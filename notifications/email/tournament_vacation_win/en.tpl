<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>%SITE_NAME%</title>

    <style type="text/css">

            .telegram-block-container,
            .social-block-icons {
                display: none !important;
            }

            .main-header-block .align-left {
                display: none !important;
            }

@media only screen and (max-device-width: 658px) {
  .body-container {
    margin: 0 3px !important;
    border-radius: 13px !important;
  }

  .body-container-content {
    border-radius: 12px !important;
    border: 1px solid #ce7613 !important;
  }

  body {
    padding: 0 !important;
  }

  .main-tape-block h3 {
    font-size: 18px !important;
  }

  .btn {
    min-width: 200px !important;
    font-size: 16px !important;
    padding: 15px !important;
  }

  .main.content {
    padding-bottom: 0 !important;
  }

  .logo {
    width: 163px !important;
    margin-bottom: 5px !important;
  }

  .main-header-block h3 {
    font-size: 12px !important;
  }

  .main-header-block a {
    font-size: 12px !important;
  }

  .header-image {
    display: none !important;
  }

  .footer {
    padding: 15px !important;
    border-bottom-left-radius: 12px !important;
    border-bottom-right-radius: 12px !important;
  }

  .main-header-block {
    padding: 7px 20px !important;
    border-top-left-radius: 11px !important;
    border-top-right-radius: 11px !important;
  }

  .main-body-block,
.footer-block-container {
    border-bottom-left-radius: 11px !important;
    border-bottom-right-radius: 11px !important;
  }

  .telegram-block {
    font-size: 12px !important;
    line-height: 1.1 !important;
  }

  .telegram-block-container {
    padding: 8px 20px !important;
  }

  .tournament-block-header {
    padding-left: 14px !important;
    padding-right: 14px !important;
  }

  .tournament-block-header-img {
    max-width: 95px !important;
  }

  .tournament-block-header-text h4 {
    margin-bottom: 8px !important;
  }

  .tournament-block-header-text h3 {
    font-size: 24px !important;
    margin-bottom: 3px !important;
  }

  .tournament-block-header-text h5 {
    font-size: 14px !important;
  }

  .tournament-block-body {
    padding-top: 14px !important;
    padding-bottom: 18px !important;
  }

  .tournament-block-body-title {
    font-size: 16px !important;
    margin-bottom: 5px !important;
  }

  .tournament-prize-pool {
    font-size: 36px !important;
  }

  .tournament-block-body-text {
    padding: 0 10% !important;
    margin-top: 15px !important;
    font-size: 14px !important;
  }

  .tournament-block-body .btn-container {
    margin-top: 16px !important;
  }

  .footer-text-primary {
    font-size: 12px !important;
  }

  .footer-text-secondary {
    font-size: 11px !important;
  }

  .footer-wrap .footer {
    padding: 10px !important;
  }

              .tournament-prize-pool-decorator img {
                  max-width: 16px !important;
              }
}
@media only screen and (max-device-width: 480px) {
  .footer-text {
    font-size: 10px !important;
  }

  .btn {
    min-width: auto !important;
    width: 90% !important;
    font-size: 12px !important;
    padding: 10px !important;
  }

  .tournament-block-header-text h4 {
    font-size: 12px !important;
    margin-bottom: 3px !important;
  }

  .tournament-block-header-text h3 {
    font-size: 14px !important;
  }

  .tournament-block-header-text h3 br {
    display: none;
  }

  .tournament-block-header-text h5 {
    font-size: 10px !important;
  }

  .main-header-block h3 {
    font-size: 10px !important;
  }

  .main-header-block a {
    font-size: 10px !important;
  }

  .tournament-prize-pool {
    font-size: 24px !important;
    padding-left: 10px !important;
    padding-right: 10px !important;
  }
}
</style>
</head>

<body style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; height: 100%; padding: 0; background: #08182f; width: 100%;">

<!-- HEADER -->
<table class="head-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                    <td style="display:none !important;
                               visibility:hidden;
                               mso-hide:all;
                               font-size:1px;
                               color:#ffffff;
                               line-height:1px;
                               max-height:0px;
                               max-width:0px;
                               opacity:0;
                               overflow:hidden;">
                      Luck is definitely on your side. You are lucky! Arrange a holiday! Thanks for your participation in the tournament.
                    </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="header container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 658px; margin: 0 auto; clear: both;">

            <div class="content" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 658px; margin: 0 auto; display: block;">
                <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                            <a href="https://%DOMAIN%/" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none;">
                                <img class="logo" src="https://%DOMAIN%/images/mail/logo.png" alt="%SITE_NAME%" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; width: 250px; margin: 18px auto 0 auto; display: block;" width="250">
                            </a>
                            <a href="https://%DOMAIN%/" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none;">
                                <div class="header-image" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 60%; margin-left: auto; margin-right: auto;">
                                    <img src="https://%DOMAIN%/images/mail/header-image.png" alt="%SITE_NAME%" style="display: block; margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%;">
                                </div>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 658px; margin: 0 auto; clear: both;">
            <div class="body-container" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border: 1px solid #ffed0e; border-radius: 25px;">
                <div class="body-container-content" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border: 3px solid #ce7613; border-radius: 24px; background: #022e4f;">
                    <div class="main-container" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                        <div class="content main" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 658px; margin: 0 auto; display: block;">
                            <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                                <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                    <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                        <!-- Main header -->
                                        <div class="main-header-block" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; padding: 10px 20px; color: #000; font-size: 14px; text-transform: uppercase; background: #ffce01; border-top-left-radius: 21px; border-top-right-radius: 21px;">
                                            <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                                                <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                                    <td class="align-left" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: left;" align="left">
                                                        <h3 style="padding: 0; font-family: 'Arial', 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; margin-bottom: 15px; color: #000; font-weight: 500; font-size: 14px; margin: 0; line-height: 1.43; display: inline-block; margin-right: 5px;">New mirror! <a class="text-underline" href="https://%CURRENT_MIRROR%/" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-decoration: underline; color: #000; font-size: 14px;">https://%CURRENT_MIRROR%/</a></h3>
                                                    </td>
                                                    <td class="align-right" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: right;" align="right">
                                                        <a class="text-underline" href="https://%DOMAIN%/login" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-decoration: underline; color: #000; font-size: 14px;">Login</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!-- /Main header -->

                                        <div class="main-body-block" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; background: #007173; border-bottom-left-radius: 21px; border-bottom-right-radius: 21px;">
                                            <!-- Telegram block -->
                                            <div class="telegram-block-container" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center; padding: 18px 10px;">
                                                <p class="telegram-block" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-bottom: 10px; font-weight: normal; font-size: 14px; color: #fff; line-height: 24px; text-align: left; display: inline-block; margin: 0;">
                                                    <img class="telegram-block-icon" src="https://%DOMAIN%/images/mail/icon-telegram.png" alt="%SITE_NAME% - Telegram channel" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; float: left; width: 24px; height: 24px; margin-right: 7px;" width="24" height="24">
                                                    The most up-to-date mirrors in telegram channel: <a class="text-underline" href="https://t.me/%TELEGRAM_CHANNEL%" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: underline;">@%TELEGRAM_CHANNEL%</a>
                                                </p>
                                            </div>
                                            <!-- /Telegram block -->

                                            <!-- Tournament block header -->
                                            <div class="tournament-block-header" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; background: #54cad4; padding: 6px 50px;">
                                                <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                                                    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                                        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center;" align="center">
                                                            <div class="tournament-block-header-text" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: left;">
                                                                <h4 style="padding: 0; font-family: 'Arial', 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; color: #dc0000; font-size: 18px; margin: 0; text-transform: uppercase; margin-bottom: 14px; font-weight: bold;">{{ place }} place in the tournament</h4>
                                                                <h3 style="padding: 0; font-family: 'Arial', 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; font-size: 32px; color: #fff; text-shadow: 0 4px 1px rgba(0, 0, 0, 0.5); margin: 0; margin-bottom: 10px; text-transform: uppercase; font-weight: bold;">
                                                                    Vacation
                                                                </h3>
                                                                <h5 style="padding: 0; font-family: 'Arial', 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; margin-bottom: 15px; color: #000; font-size: 20px; margin: 0; font-weight: normal;">Congratulations on winning!</h5>
                                                            </div>
                                                        </td>
                                                        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center;" align="center">
                                                            <img class="tournament-block-header-img" src="https://%DOMAIN%/images/mail/trophy.png" alt="{{ place }} place in the tournament. Congratulations on winning!" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; width: 100%; max-width: 190px; display: block; margin: 0 auto;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- /Tournament block header -->

                                            <!-- Tournament block body -->
                                            <div class="tournament-block-body" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; padding: 32px 10px 57px 10px; text-align: center;">
                                                <p class="tournament-block-body-title" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.6; margin: 0; margin-bottom: 19px; text-transform: uppercase; font-size: 20px; color: #edaa00;">
                                                    You Won!
                                                </p>

                                                    <div class="tournament-prize-pool-decorator" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: inline-block; vertical-align: middle;">
                                                                                                    <img src="https://%DOMAIN%/images/mail/laurel-left.png" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 24px; width: 100%;">
                                                                                                </div>
                                                                                                <div class="tournament-prize-pool" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: inline-block; text-shadow: 0 2px 2px rgba(0, 0, 0, 0.5); font-size: 48px; color: #fff; font-weight: bold; padding: 10px 30px; vertical-align: middle;">{{ prize }}</div>
                                                                                                <div class="tournament-prize-pool-decorator" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: inline-block; vertical-align: middle;">
                                                                                                    <img src="https://%DOMAIN%/images/mail/laurel-right.png" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 24px; width: 100%;">
                                                                                                </div>

                                                <div class="tournament-block-body-text" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1.25; color: #fff; padding: 0 20%; margin-top: 20px;">
                                                    Luck is definitely on your side. You are lucky!
                                                    Arrange a holiday!
                                                    Thanks for your participation in the tournament.
                                                </div>

                                                <div class="btn-container" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center; margin-top: 34px;">
                                                    <a href="https://%DOMAIN%/" target="_blank" class="btn" style="font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: inline-block; color: #ffffff; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 16px; font-weight: bold; margin: 0; padding: 15px 25px; vertical-align: top; border-radius: 100px; text-align: center; text-transform: uppercase; min-width: 282px; box-shadow: 0 3px 0 0 rgba(0, 16, 28, 0.61); border: solid 1px #ffbc30; background-color: #fa7c34; background-image: linear-gradient(to bottom, #feae00, #ff5d00 100%); text-shadow: 0 2px 2px #d14d00;">Get Your Prize</a>
                                                </div>
                                            </div>
                                            <!-- /Tournament block body -->

                                            <div class="footer-block-container" style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-bottom-left-radius: 21px; border-bottom-right-radius: 21px; background-color: rgba(0, 0, 0, 0.27); padding: 12px; text-align: center;">
                                                <div class="social-block-icons" style="margin: 0; margin-bottom: 7px; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                                    <a href="%VK_GROUP%" target="_blank" class="social-icon" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none; width: 24px; height: 24px; margin: 0 3px; display: inline-block; vertical-align: top;"><img src="https://%DOMAIN%/images/mail/icon-vk.png" alt="%SITE_NAME% | VK" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; width: 100%;"></a>
                                                    <a href="%FB_GROUP%" target="_blank" class="social-icon" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none; width: 24px; height: 24px; margin: 0 3px; display: inline-block; vertical-align: top;"><img src="https://%DOMAIN%/images/mail/icon-fb.png" alt="%SITE_NAME% | FB" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; width: 100%;"></a>
                                                    <a href="%INSTAGRAM_GROUP%" target="_blank" class="social-icon" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none; width: 24px; height: 24px; margin: 0 3px; display: inline-block; vertical-align: top;"><img src="https://%DOMAIN%/images/mail/icon-instagram.png" alt="%SITE_NAME% | INSTAGRAM" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; width: 100%;"></a>
                                                </div>
                                                <p class="footer-text footer-text-access footer-text-primary" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-bottom: 10px; font-weight: normal; margin: 0;  color: #ffffff; font-size: 14px; line-height: 1.3;">
                                                    Access problems?
                                                    <br style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                                    Download <a class="text-underline" href="https://fri-gate.org/" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: underline;">friGate</a> and stay with us.
                                                </p>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%; clear: both;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 658px; margin: 0 auto; clear: both;">
            <!-- content -->
            <div class="content footer" style="font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 658px; margin: 0 auto; display: block; color: #a8adbb; text-align: center; padding: 16px;">
                <p class="footer-text footer-text-secondary" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-bottom: 10px; font-weight: normal; line-height: 1.6; margin: 0; font-size: 12px;">&copy; %YEAR% %SITE_NAME% — All rights reserved.</p>
            </div>
            <!-- /content -->
        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table>
<!-- /FOOTER -->

</body>
</html>