<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>%SITE_NAME%</title>

    <style type="text/css">
        @media only screen and (max-device-width: 658px) {
            body {
                padding: 0 !important;
            }

            .btn {
                min-width: auto !important; font-size: 14px !important;
            }

            .main-title {
                margin-bottom: 27px !important;
            }

                      .main-title,
                       .main-title-link {
                           padding-left: 15px !important;
                           padding-right: 15px !important;
                       }

            .main-image {
                margin-bottom: 15px !important;
            }

            .main-footer {
                margin-top: 40px !important;
                margin-bottom: 20px !important;
            }

            .main-title,
            .main-title-link {
                font-size: 16px !important;
            }

            .main-promo-block {
                font-size: 26px !important;
                padding: 16px !important;
            }

              .footer {
                padding: 15px !important;
              }

                          .logo {
                              width: 136px !important;
                          }

                          .main-header-block h3 {
                              font-size: 12px !important;
                          }

                          .main-header-block { width: 100% !important;
                              margin-bottom: 18px !important;
                          }
        }
    @media only screen and (max-device-width: 480px) {            .footer-text {                font-size: 10px !important;            }    .btn { font-size: 13px !important;  }    } </style>
</head>

<body style="margin: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; height: 100%; padding: 0; background: #243054; width: 100%;">

<!-- HEADER -->
<table class="head-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="header container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; background: no-repeat url('https://%DOMAIN%/images/mail/header-bg-1.png') 0 0 / 100% 100%; -webkit-background-size: 100% 100%; background-size: 100% 100%; display: block; max-width: 658px; margin: 0 auto; clear: both;">

            <div class="content" style="font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 658px; margin: 0 auto; display: block; padding: 15px;">
                <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                            <a href="https://%DOMAIN%?ref=JXpCquWf&p1=follow_up_no_deposits&p2=logo" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none;">
                                <img class="logo" src="https://%DOMAIN%/images/mail/logo.png" alt="%SITE_NAME%" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; width: 250px; margin: 0 auto; display: block;" width="250">
                            </a>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 658px; margin: 0 auto; clear: both;">

            <div class="content main" style="font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; background: #fff; position: relative; max-width: 658px; margin: 0 auto; display: block;">
                <table style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%;" width="100%">
                    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                            <!-- Main header -->
                            <div class="main-header-block" style="margin: 0 auto; margin-bottom: 35px; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-left: auto; margin-right: auto; text-align: center; border-bottom-left-radius: 6px; border-bottom-right-radius: 6px; background-image: linear-gradient(to bottom, #feae00, #fa7c34); width: 90%;">
                                <a href="https://%CURRENT_MIRROR%?ref=JXpCquWf&p1=follow_up_no_deposits&p2=site-mirror-link" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none;">
                                    <h3 style="font-family: 'Arial', 'Helvetica Neue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; margin-bottom: 15px; font-weight: 500; margin: 0; color: #fff; text-transform: uppercase; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5); font-size: 18px; padding: 14px; line-height: 1.4; word-break: break-word;">Новое зеркало! https://%CURRENT_MIRROR%/</h3>
                                </a>
                            </div>
                            <!-- /Main header -->

                            <!-- Main title -->
                            <p class="main-title" style="margin: 0; padding: 0 30px; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #000; text-align: center; margin-top: -2px; margin-bottom: 42px; font-size: 22px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: 1.25; letter-spacing: normal;">
                                Привет, это %SITE_NAME%!
                                <br style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                                Вы зарегистрировались у нас, но так и не совершили ни одного депозита, поэтому мы дарим промокод на фриспины.
                            </p>
                            <!-- /Main title -->

                            <!-- Main image -->
                            <div class="main-image" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-bottom: 14px;">
                                <a href="https://%DOMAIN%/refill?ref=JXpCquWf&p1=follow_up_no_deposits&p2=promocode-image" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: none;">
                                    <img src="https://%DOMAIN%/images/mail/follow_up_no_deposits/main-bg.png?v1" alt="%SITE_NAME% - Промокод на фриспины" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 90%; display: block; margin: 0 auto 10px auto;">
                                    <div class="main-promo-block" style="font-family: 'Times New Roman', 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center; max-width: 360px; margin: 0 auto; border-radius: 6px; text-shadow: 0 1px 2px rgba(0, 0, 0, 0.75); font-size: 36px; font-weight: bold; color: #fff; padding: 24px; background-image: linear-gradient(to bottom, #dc0000, #b50000);">
                                        {{ promo_code }}
                                    </div>
                                </a>
                            </div>
                            <!-- /Main image -->

                            <!-- Main button -->
                            <div class="btn-container" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; text-align: center;">
                                <a href="https://%DOMAIN%/refill?ref=JXpCquWf&p1=follow_up_no_deposits&p2=promocode-btn" target="_blank" class="btn" style="font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: inline-block; color: #ffffff; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 16px; font-weight: bold; margin: 0; padding: 15px 25px; vertical-align: top; background-color: #fa7c34; background-image: linear-gradient(to right, #feae00, #fa7c34); border-radius: 25px; text-align: center; text-transform: uppercase; min-width: 282px;">Использовать промокод</a>
                            </div>
                            <!-- /Main button -->

                            <!-- Main footer -->
                            <div class="main-footer" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-top: 76px; margin-bottom: 30px;">

                                <!-- Main title -->
                                <p class="main-title" style="margin: 0; padding: 0 30px; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #000; text-align: center; margin-top: -2px; font-size: 22px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: 1.25; letter-spacing: normal; margin-bottom: 20px;">
                                    Если вы забыли пароль, то сбросьте его, скопировав ссылку в адресную строку браузера.
                                </p>
                                <a href="https://%DOMAIN%/reset-password?ref=JXpCquWf&p1=follow_up_no_deposits&p2=reset-pass-link" target="_blank" class="main-title-link" style="margin: 0; padding: 0 30px; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: 1.25; letter-spacing: normal; text-align: center; color: #3b78d2; display: block; text-decoration: underline;">https://%DOMAIN%/reset-password</a>
                                <!-- /Main title -->

                            </div>
                            <!-- /Main footer -->
                        </td>
                    </tr>
                </table>
            </div><!-- /content -->

        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; border-spacing: 0; border-collapse: collapse; width: 100%; clear: both;" width="100%">
    <tr style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
        <td class="container" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; display: block; max-width: 658px; margin: 0 auto; clear: both;">

            <!-- content -->
            <div class="content footer" style="max-width: 658px; margin: 0 auto; display: block; padding: 26px 30px; margin-top: -2px; font-family: sans-serif; background-color: #1d2640; color: #8e929f; text-align: center;">
                <div class="footer-text-block" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                    <p class="footer-text footer-text-telegram footer-text-primary" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; color: #ffffff; font-size: 12px; line-height: 1.3; margin-bottom: 12px; display: inline-block; text-align: left;">
                        <a href="https://t.me/%TELEGRAM_CHANNEL%" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"><img class="footer-text-icon" src="https://%DOMAIN%/images/mail/icons/telegram.png" alt="%SITE_NAME% - Телеграм канал" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; max-width: 100%; float: left; width: 30px; height: 30px; margin-right: 7px;" width="30" height="30"></a>
                        Самые актуальные зеркала в нашем
                        <br style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                        телеграм канале: <a class="text-underline" href="https://t.me/%TELEGRAM_CHANNEL%" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: underline;">@%TELEGRAM_CHANNEL%</a>
                    </p>
                </div>


                <p class="footer-text footer-text-access footer-text-primary" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; font-weight: normal; margin: 0; color: #ffffff; font-size: 12px; line-height: 1.3; margin-bottom: 12px;">
                    Проблемы с доступом?
                    <br style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;">
                    Скачивайте <a class="text-underline" href="https://fri-gate.org/" target="_blank" style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; color: #feae06; text-decoration: underline;">friGate</a> и оставайтесь с нами
                </p>

                <p class="footer-text footer-text-secondary" style="padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif; margin-bottom: 10px; font-weight: normal; line-height: 1.6; margin: 0; font-size: 11px;">&copy; %YEAR% %SITE_NAME% — Все права защищены.</p>
            </div>
            <!-- /content -->

        </td>
        <td style="margin: 0; padding: 0; font-family: 'Arial', 'Helvetica Neue', 'Helvetica', Arial, Helvetica, sans-serif;"></td>
    </tr>
</table>
<!-- /FOOTER -->

</body>
</html>