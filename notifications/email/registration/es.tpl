<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail</title>
    <style>
        @media only screen and (max-device-width: 620px) {
            .container {
                width: 100%;
            }
            .btn {
                width: 100%!important;
            }
            .wrapper {
                padding-left: 12px!important;
                padding-right: 12px!important;
                padding-bottom: 5px!important;
            }
        }
    </style>
</head>
<body style="font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #242f53; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="margin-top: 20px; border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #242f53;" width="100%" bgcolor="#242f53">
    <tr><td></td><td style="height:100px; background: #242f53"></td><td></td></tr>
    <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; margin: 0 auto !important; max-width: 600px;" width="600" valign="top">
            <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 600px;">
                <!-- START HEADER -->
                <div class="header" style="border-bottom: 8px solid #fb7d33; padding-top: 21px; padding-bottom: 19px; background: #19406a; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;" width="100%">
                        <tr>
                            <td class="align-center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center;" valign="top" align="center">
                                <a href="https://%DOMAIN%/" target="_blank" style="color: #3498db; text-decoration: underline;">
                                    <img src="https://%DOMAIN%/images/mail/logo.png" width="175" height="40" alt="%SITE_NAME%" align="center" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;"></a>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- END HEADER -->
                <table border="0" cellpadding="0" cellspacing="0" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff;" width="100%">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 30px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <h1 style="color: #fb7d33; font-family: Arial; font-weight: 700; line-height: 1.4; margin: 0; margin-bottom: 30px; font-size: 24px; text-align: center;">E&#8209;mail Address Confirmation</h1>
                                        <p style="font-family: Arial; font-size: 18px; font-weight: normal; margin: 0; margin-bottom: 15px; color: #4a4a4a;">
                                            You have successfully registered on casino %SITE_NAME%. To confirm your e&#8209;mail address, please, use "Confirm E&#8209;mail" button or copy a link below to your browser address bar:
                                        </p>
                                        <p style="font-family: Arial; font-size: 18px; font-weight: normal; margin: 0; margin-bottom: 25px; color: #3b78d2; text-decoration: none; text-align: center;">
                                            {{ url }}
                                        </p>
                                        <table border="0" cellpadding="0" cellspacing="0" class="hr" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; height: 17px; line-height: 20px;" valign="top">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="center" style="font-family: Arial; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
                                                    <table class="btn" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 342px;">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-family: Arial; font-size: 16px; vertical-align: top; background-image: linear-gradient(to right, #feae00, #fa7c34); border-radius: 25px; text-align: center;" valign="top" bgcolor="#3498db" align="center">
                                                                <a href={{ url }} target="_blank" style="display: inline-block; color: #ffffff; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 16px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize;">Confirm E&#8209;mail</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- END MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper section-callout" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px; background-color: #1d2640; color: #ffffff;" valign="top" bgcolor="#1d2640">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: left; color: #ffffff;" valign="top" align="left">
                                        <p style="font-size: 11px; font-family: Arial;">&copy; %YEAR% %SITE_NAME% — All rights reserved. </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
    </tr>
    <tr><td></td><td style="height:100px; background: #242f53"></td><td></td></tr>
</table>
</body>

</html>