'use strict';

module.exports = {
  tabWidth: 4,
  useTabs: false,
  semi: true,
  singleQuote: true,
  bracketSpacing: false,
  arrowParens: 'always',
  trailingComma: 'all',
  jsxBracketSameLine: false,
  arrowParens: 'always',
  printWidth: 100,
  endOfLine: 'lf',
};
