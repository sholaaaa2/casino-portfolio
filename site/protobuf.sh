#!/usr/bin/env bash
export PATH=$(npm bin):$PATH
pbjs --es6 --force-long -t static-module ./src/api/proto/messages.proto ./src/api/proto/webpush.proto ./src/api/proto/game_monitor.proto  -o src/api/proto.js
(head -n 11 src/api/proto.js; cat src/api/long-proto-init.js; tail -n +12 src/api/proto.js) > /tmp/proto.js
mv -vf /tmp/proto.js src/api/proto.js
rm -rf /tmp/proto.js
sed -i.tmp "1 s/^.*$/\/*eslint-disable*\//" src/api/proto.js
pbts src/api/proto.js -o src/api/proto.d.ts
rm -rf src/api/proto.js.tmp