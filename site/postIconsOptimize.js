const fs = require("fs");

fs.readdir("./src/assets/fonts/optimized-game-menu-icons/", (err, files) => {
  if (err) {
    throw err;
  } else if (files) {
    for (const file of files) {
      if (file.endsWith(".svg")) {
        const newFileName = file
          .replace(/a/g, "0")
          .replace(/e/g, "1")
          .replace(/i/g, "2")
          .replace(/o/g, "3")
          .replace(/u/g, "4")
          .replace(/y/g, "5");
        fs.rename(
          `./src/assets/fonts/optimized-game-menu-icons/${file}`,
          `./src/assets/fonts/optimized-icons/${newFileName}`,
          err => {
            if (err) {
              throw err;
            }
          }
        );
      }
    }
  }
});
