// replace i selector with wildcard in iconfont (since fantasticon --tag option does not work on Windows)

const fs = require('fs');
const path = require('path');

const FONTICON_CSS_PATH = path.join(
    __dirname,
    '..',
    'src',
    'assets',
    'fonts',
    'iconfont',
    'icons.css',
);
let css = fs.readFileSync(FONTICON_CSS_PATH, 'utf-8');

css = css.replace(
    'i[class^="iconfont-"]:before, i[class*=" iconfont-"]:before',
    `*[class^="iconfont-"]:before, *[class*=" iconfont-"]:before`,
);

fs.writeFile(FONTICON_CSS_PATH, css, (err) => {
    if (err) {
        throw err;
    }
});
