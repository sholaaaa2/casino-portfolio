// corrects sass files (basic cases only) to help with migration from node-sass to dart-sass

const fs = require('fs');
const path = require('path');
const readline = require('readline');

const SASS_DIR = path.join(__dirname, '..', 'src', 'assets', 'sass');

for (const filePath of walkSync(SASS_DIR)) {
    if (filePath.endsWith('.sass')) {
        processFile(filePath);
    }
}

function* walkSync(dir) {
    const files = fs.readdirSync(dir, {withFileTypes: true});
    for (const file of files) {
        if (file.isDirectory()) {
            yield* walkSync(path.join(dir, file.name));
        } else {
            yield path.join(dir, file.name);
        }
    }
}

function processFile(filePath) {
    const tmpFile = filePath + '.tmp';
    fs.writeFileSync(tmpFile, '');
    const output = fs.createWriteStream(tmpFile, {flags: 'r+', defaultEncoding: 'utf8'});
    const rl = readline.createInterface({
        input: fs.createReadStream(filePath),
        output,
    });
    let prevLine = undefined;
    rl.on('line', (line) => {
        let ln = line;
        ln = ln.replace(';', '');
        ln = replaceAll(ln, '	', '  ');
        if (prevLine !== undefined) {
            prevLine = processLine(prevLine, ln);
            output.write(prevLine + '\n');
        }
        prevLine = ln;
    }).on('close', () => {
        output.write(prevLine + '\n');
        fs.unlinkSync(filePath);
        fs.renameSync(tmpFile, filePath);
    });
}

function replaceAll(line, searchValue, replaceValue) {
    if (String.prototype.replaceAll) {
        return line.replaceAll(searchValue, replaceValue);
    }
    return line.replace(new RegExp(searchValue, 'g'), replaceValue);
}

function processLine(line, nextLine) {
    const trimmedLine = line.trimStart();
    const trimmedNextLine = nextLine.trimStart();
    if (line.length - trimmedLine.length === nextLine.length - trimmedNextLine.length) {
        if (isSelector(trimmedLine) && isSelector(trimmedNextLine)) {
            const lineTrimmedFromEnd = line.trimEnd();
            if (!lineTrimmedFromEnd.endsWith(',')) {
                return lineTrimmedFromEnd + ',';
            }
        }
    } else {
        if (line.trimEnd().endsWith(',')) {
            return line.slice(0, line.length - 1);
        }
    }
    return line;
}

function isSelector(line) {
    const firstTag = line.split('.')[0].split('#')[0];
    return (
        [
            'body',
            'header',
            'footer',
            'nav',
            'main',
            'article',
            'section',
            'div',
            'a',
            'ul',
            'li',
            'input',
            'button',
        ].includes(firstTag) ||
        line.startsWith('.') ||
        line.startsWith('#') ||
        line.startsWith('&')
    );
}
