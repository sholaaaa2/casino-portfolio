const express = require('express');
const compression = require('compression');
const openBrowser = require('react-dev-utils/openBrowser');
const path = require('path');
const proxy = require('http-proxy-middleware');
const packageJson = require('../package.json');

const app = express();
const publicPath = path.resolve(__dirname, '../', 'build');

const staticServe = express.static(publicPath);

app.use(compression());
app.use('/', staticServe);

app.use(
  proxy('/', {
    target: packageJson.proxy,
    autoRewrite: false,
    ws: true,
    changeOrigin: true,
  }),
);


app.use('*', staticServe);


app.listen(3002);
console.log('Open: http://localhost:3002/')
// openBrowser('http://localhost:3002/');
