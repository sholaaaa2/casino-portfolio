#!/usr/bin/env bash

cd build/
find . -type f -name "*.js" -print0 | xargs -0 sed -i 's/Random/\\u0052\\u0061\u006E\\u0064\\u006F\\u006D/g'
find . -type f -name "*.js" -print0 | xargs -0 sed -i 's/random/\\u0072\\u0061\\u006E\\u0064\\u006F\\u006D/g'
find . -type f -name "*.js" -print0 | xargs -0 sed -i 's/RANDOM/\\u0052\\u0041\\u004E\\u0044\\u004F\\u004D/g'
