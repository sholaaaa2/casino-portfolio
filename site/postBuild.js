const fs = require('fs');
const path = require('path');
const glob = require('glob');
const appSettings = require('./src/AppSettings.json');
const assetsTtf = glob.sync(__dirname + '/build/static/media/*.ttf')
  .filter((assetPath) => assetPath.indexOf("icomoon") !== -1)
  .map((assetPath) => {
    return path.relative(__dirname + '/build', assetPath);
  });
const assetsWoff2 = glob.sync(__dirname + '/build/static/media/*.woff2')
  .filter((assetPath) => assetPath.indexOf("open-sans-v15-latin_cyrillic-600") !== -1 || assetPath.indexOf("open-sans-v15-latin_cyrillic-700") !== -1)
  .map((assetPath) => {
    return path.relative(__dirname + '/build', assetPath);
  });
const assets = assetsTtf.concat(assetsWoff2);
const pathToEntry = './build/index.html';
const splitBy = '</title>';

console.log('Preloaded fonts:');
console.log(assets);

let indexHTML = fs.readFileSync(pathToEntry).toString();
if (appSettings.kioskLobby.enabled) {
  indexHTML = indexHTML.replace('<img src="/images/logo-full.svg" width="227" height="52" alt="%SITE_NAME%"/>', '');
}

const parts = indexHTML.split(splitBy);

const fileWithPreload = [
  parts[0],
  splitBy
];

for (const link of assets) {
  fileWithPreload.push(`<link rel="preload" href="/${link}" as="font" crossorigin>`);
}

fileWithPreload.push(parts[1]);

fs.writeFileSync(pathToEntry, fileWithPreload.join(''));