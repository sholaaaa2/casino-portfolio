#!/usr/bin/env bash
(head -n 11 src/api/proto.js; cat src/api/long-proto-init.js; tail -n +12 src/api/proto.js) > /tmp/proto.js
mv -vf /tmp/proto.js src/api/proto.js
rm -rf /tmp/proto.js
sed -i.tmp "1 s/^.*$/\/*eslint-disable*\//" src/api/proto.js
rm -rf src/api/proto.js.tmp
