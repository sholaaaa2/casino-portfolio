# Whitelabel Client

## Start Dev Server
```
cd site
npm ci
npm start
```

## Build
```
cd site
npm ci
sh protobuf.sh
npm run build
```

## SSR
```
cd site/ssr && npm ci && node index.js && cd ../../
```
