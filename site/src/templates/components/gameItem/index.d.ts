import { ReactNode } from 'react';
import {GameItemWrapper} from '../../../state/actions/GamesActions';

declare function template(content: GameItemWrapper): ReactNode;
export default template;
