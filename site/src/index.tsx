// polyfills
import './state/utils/Polyfills';
import * as React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import * as ReactDOM from 'react-dom';
import App from './App';
import AppState from './state/AppState';
import './assets/fonts/iconfont/icons.css';
import './assets/fonts/icomoon/style.css';
import './assets/sass/app.sass';
require('./assets/libs/matchMediaPolyfill.js');
require('./assets/libs/qrcode.min.js');

export const appState = new AppState();
window.appState = appState;

export enum AndroidWebViewMessageType {
    SAVE_FILE = "save-file",
    GEO_ENABLE = "geo-enable",
    PASTE_FROM_CLIPBOARD = "paste-from-clipboard",
    PICK_CONTACT = "pick-contact",
    CONTACT_LIST = "contact-list",
};
window.AndroidWebViewCB = {
    [AndroidWebViewMessageType.SAVE_FILE]: undefined,
    [AndroidWebViewMessageType.GEO_ENABLE]: undefined,
    [AndroidWebViewMessageType.PASTE_FROM_CLIPBOARD]: undefined,
    [AndroidWebViewMessageType.PICK_CONTACT]: undefined,
    [AndroidWebViewMessageType.CONTACT_LIST]: appState.user.processContactList,
};

// global declarations
declare global {
    interface Window {
        // digitain sport
        _sp: Array<(string | number | boolean)[]>;
        SportFrame: { frame: (params: Array<(string | number | boolean)[]>) => void, openPage: (pageName: string) => void  };
        onSportLogin: Function;
        onSportRegister: Function;
        Bootstrapper: {bootIframe: Function, boot: Function};

        // app state export serves debug goals only
        appState: AppState;
        // tslint:disable-next-line
        olark: any;
        // tslint:disable-next-line
        owcWidget: any;
        owcWidgetCB: () => void;
        // tslint:disable-next-line
        Tawk_API: any;
        smartsupp: any;
        _smartsupp: any;
        createQRCode: (element: HTMLElement, text: string, data: object) => void;
        destroyQRCode: () => void;

        QRCode: any;

        // tslint:disable-next-line
        ym: any;
        ReactNativeWebView?: { postMessage?: (message: string) => void };
        AndroidWebView?: { postMessage?: (message: string) => string };
        AndroidWebViewCB: Record<AndroidWebViewMessageType, ((message: string) => void) | undefined>;
        IdleDetector: IdleDetector;
    }

    interface IdleDetector {
        new(): IdleDetector;
        userState: "active" | "idle";
        addEventListener(eventType: string, callback: () => void): void;
        requestPermission(): Promise<"granted" | "denied">;
        start(options: {
            threshold: number;
            signal: AbortSignal;
        }): Promise<void>;
    }
}

ReactDOM.render(<Router><App appState={appState}/></Router>, document.getElementById('root'));
