import {action, computed, observable} from 'mobx';
import {messages, wallet} from '../../api/proto';
import AppState from '../AppState';
import {NotificationItemLevel} from '../components/Notificator';

export class ConfirmCashappPhone {
    @observable country = '';
    @observable phone = '';
    @observable error?: wallet.CashAppConfirmPhoneResponse.CashAppConfirmPhoneError = undefined;
    @observable confirmationCode = '';
    @observable isLoading = false;

    constructor(private appState: AppState) {}

    @computed
    get isSubmitButtonDisabled(): boolean {
        return (
            this.isLoading ||
            this.confirmationCode.length !== this.appState.confirmationCodeMethodState.codeLength
        );
    }

    @action.bound
    resetState() {
        this.country = '';
        this.phone = '';
        this.confirmationCode = '';
        this.error = undefined;
        this.isLoading = false;
    }

    @action.bound
    handleCodeInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.confirmationCode = e.target.value;
    }

    @action.bound
    init(country: string, phone: string): void {
        this.isLoading = true;
        this.appState.confirmationCodeMethodState.isFirstRetry = true;
        this.country = country;
        this.phone = phone;
        this.appState.confirmForm.activateResendTimer();
        this.appState.modal.showConfirmCashappPhone();
        this.appState.api.cashAppConfirmPhone(
            phone,
            country,
            this.appState.confirmationCodeMethodState.isVoiceSelected,
            undefined,
            (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                if (act.cashAppConfirmPhone?.error) {
                    this.error = act.cashAppConfirmPhone?.error;
                }
                this.isLoading = false;
            },
        );
    }

    @action.bound
    resendCode() {
        this.error = undefined;
        this.appState.confirmationCodeMethodState.isFirstRetry = false;
        this.isLoading = true;
        this.appState.api.cashAppConfirmPhone(
            this.phone,
            this.country,
            this.appState.confirmationCodeMethodState.isChoiseAvailable
                ? false
                : this.appState.confirmationCodeMethodState.isVoiceSelected,
            undefined,
            (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                this.appState.confirmForm.activateResendTimer();
                if (act.cashAppConfirmPhone?.error) {
                    this.error = act.cashAppConfirmPhone?.error;
                }
                this.isLoading = false;
            },
        );
    }

    @action.bound
    submitCode() {
        this.error = undefined;
        this.appState.api.cashAppConfirmPhone(
            this.phone,
            this.country,
            false,
            this.confirmationCode,
            (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                this.isLoading = false;
                if (act.cashAppConfirmPhone?.error) {
                    this.error = act.cashAppConfirmPhone?.error;
                }
                if (act.cashAppConfirmPhone?.ok) {
                    const confirmedPhone = this.appState.refillStore.selectedPayway?.fields.find(
                        (_) => _.id === 'cashapp-phone-confirmed',
                    );
                    if (confirmedPhone) {
                        confirmedPhone.value = this.phone;
                    }
                    const confirmPhoneBool = this.appState.refillStore.selectedPayway?.fields.find(
                        (_) => _.id === 'cashapp-confirm-phone',
                    );
                    if (confirmPhoneBool) {
                        confirmPhoneBool.value = 'false';
                    }
                    this.resetState();
                    this.appState.modal.hideModal();
                    this.appState.notificator?.addNotification({
                        level: NotificationItemLevel.SUCCESS,
                        message: this.appState.t('ui-phone-confirmed'),
                    });
                }
            },
        );
    }
}
