import {action, observable} from 'mobx';
import AppState from '../../AppState';

export default class AuthorizationBaseActions {
    @observable error?: string;
    @observable isLoading: boolean = false;

    constructor(protected appState: AppState) {
        this.onEmailInputChange = this.onEmailInputChange.bind(this);
        this.onLoginInputChange = this.onLoginInputChange.bind(this);
        this.onPasswordInputChange = this.onPasswordInputChange.bind(this);
        this.onCurrencyValueChange = this.onCurrencyValueChange.bind(this);
        this.onPhoneCountryCodeChange = this.onPhoneCountryCodeChange.bind(this);
        this.onPhoneNumberInputChange = this.onPhoneNumberInputChange.bind(this);
    }

    @action
    resetErrors() {
        this.error = undefined;
        this.appState.currencySelectState.resetError();
        this.appState.emailInputState.resetError();
        this.appState.loginInputState.resetError();
        this.appState.passwordInputState.resetError();
        this.appState.phoneInputState.resetError();
    }

    @action
    setLoading(value: boolean) {
        this.isLoading = value;
        this.appState.currencySelectState.setDisabled(value);
        this.appState.emailInputState.setDisabled(value);
        this.appState.loginInputState.setDisabled(value);
        this.appState.passwordInputState.setDisabled(value);
        this.appState.phoneInputState.setDisabled(value);
    }

    @action
    resetForm() {
        this.error = undefined;
        this.isLoading = false;
        this.appState.currencySelectState.resetForm();
        this.appState.emailInputState.resetForm();
        this.appState.loginInputState.resetForm();
        this.appState.passwordInputState.resetForm();
        this.appState.phoneInputState.resetForm();
        this.appState.acceptTermsAndConditionsState.resetForm();
    }

    @action
    onCurrencyValueChange(e: React.ChangeEvent<HTMLSelectElement>) {
        this.error = undefined;
        this.appState.currencySelectState.onValueChange(e);
    }

    @action
    onEmailInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.error = undefined;
        this.appState.emailInputState.onValueChange(e);
    }

    @action
    onLoginInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.error = undefined;
        this.appState.loginInputState.onValueChange(e);
    }

    @action
    onPasswordInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.error = undefined;
        this.appState.passwordInputState.onValueChange(e);
    }

    @action
    onPhoneCountryCodeChange(e: React.ChangeEvent<HTMLSelectElement>) {
        this.error = undefined;
        this.appState.phoneInputState.onChangeCountryCode(e);
    }

    @action
    onPhoneNumberInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.error = undefined;
        this.appState.phoneInputState.onPhoneNumberChange(e);
    }

    @action
    redirectFromAuthPages() {
        if (
            window.location.pathname.endsWith("/login") ||
            window.location.pathname.endsWith("/register") ||
            window.location.pathname.endsWith("/login-cashier") ||
            window.location.pathname.endsWith("/anonymous")
        ) {
            this.appState.router.push(this.appState.l("/"));
        }
    }

    @action.bound
    redirectToCashierLogin() {
        this.appState.router.push(this.appState.l('/login'));
    }

    @action.bound
    activatePromoCodeAfterAuth() {
        if (this.appState.refillStore.promoCodeForm.value) {
            this.appState.refillStore.promoCodeForm.tryActivatePromoCode((isSuccess: boolean) => {
                if (!isSuccess) {
                    this.appState.modal.showWrongPromoCodeModal();
                    this.appState.refillStore.promoCodeForm.clearValue();
                } else {
                    this.appState.refillStore.promoCodeForm.reset();
                }
            });
        }
    }
}
