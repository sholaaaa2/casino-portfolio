import AppState from '../../AppState';
import {messages, users} from '../../../api/proto';
import {action} from 'mobx';
import {getSignInError} from '../../../api/utils';
import {AuthorizationType} from './AuthorizationTypeState';
import AuthorizationBaseActions from './AuthorizationBaseActions';

export default class SignInActions extends AuthorizationBaseActions {

    constructor(appState: AppState) {
        super(appState);
        this.onSignInButtonClick = this.onSignInButtonClick.bind(this);
    }

    // region ---- common
    onSignInButtonClick(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        this.trySignIn();
    }

    trySignIn() {
        switch (this.appState.authorizationTypeState.type) {
            case AuthorizationType.BY_PHONE:
                this.trySignInByPhone();
                break;
            case AuthorizationType.BY_EMAIL:
                this.trySignInByEmail();
                break;
            case AuthorizationType.BY_LOGIN:
                this.trySignInByLogin();
                break;
            default:
                break;
        }
    }

    // endregion

    // region ---- login/phone/email

    trySignInByPhone() {
        const phone = this.appState.phoneInputState.getValueOrSetError();
        const password = this.appState.passwordInputState.getValueOrSetError();
        const isValid = !!(phone && password);
        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signIn('', phone!.countryCode, phone!.phoneNumber, password!, this.onSignInResponse.bind(this));
        }
    }

    trySignInByEmail() {
        const email = this.appState.emailInputState.getValueOrSetError();
        const password = this.appState.passwordInputState.getValueOrSetError();
        const isValid = !!(email && password);
        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signIn(email!, '', '', password!, this.onSignInResponse.bind(this));
        }
    }

    trySignInByLogin() {
        const login = this.appState.loginInputState.getValueOrSetError();
        const password = this.appState.passwordInputState.getValueOrSetError();
        const isValid = !!(login && password);
        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signInUsingGeneratedLogin(login!, password!, this.appState.signInActions.onSignInResponse.bind(this));
        }
    }

    onSignInResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.setLoading(false);
        this.appState.loginForm.loading = false; // todo: legacy staff

        if (act.login && act.login.ok) {
            this.processSignInSuccess(act.login);
        } else {
            this.processSignInError(act.login ? act.login.error : null);
        }
    }

    processSignInSuccess = async (response: users.ILoginResponse) => {
        const isRedirected = await this.appState.userInfo.storeAndRedirectToUrl(response.tokenLoginUrl);
        if (!isRedirected) {
            this.appState.user.hideAuthWithoutRedirect();
            this.appState.guestPopup.destroy();
            this.appState.api.startBatch();
            this.appState.games.fetchFavoriteGames();
            this.appState.games.fetchGames(true, (msg, act) => {
                this.appState.firebaseApp.initWithRequestPermission(this.appState.siteConfig.firebaseConfig);
            });
            this.appState.jpMonitorAPI.check();
            this.activatePromoCodeAfterAuth();
            this.appState.api.commit();
            this.redirectFromAuthPages();
        }
    }

    @action
    processSignInError(error: users.LoginResponse.LoginError | null | undefined) {
        const errorTerm = getSignInError(error);
        this.error = errorTerm;
        this.appState.loginForm.loginError = this.appState.t(errorTerm); // todo: legacy staff
    }

    onSignInWithGeneratedResponse = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        this.appState.processApi(msg, act);
        this.setLoading(false);
        this.appState.loginForm.loading = false; // todo: legacy staff

        if (act.login && act.login.ok) {
            this.processSignInSuccess(act.login);
        } else {
            this.processSignInWithGeneratedError(act.login ? act.login.error : null);
        }
    }

    @action
    processSignInWithGeneratedError(error: users.LoginResponse.LoginError | null | undefined) {
        const errorTerm = getSignInError(error);
        this.error = errorTerm;
        this.appState.loginForm.generatedError = this.appState.t(errorTerm); // todo: legacy staff
    }

    // endregion
}
