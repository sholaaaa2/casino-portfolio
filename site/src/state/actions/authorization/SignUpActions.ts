import AppState from '../../AppState';
import {messages, users} from '../../../api/proto';
import {getSignUpError} from '../../../api/utils';
import {NString} from '../../meta';
import {NotificationItemLevel} from '../../components/Notificator';
import {AuthorizationType} from './AuthorizationTypeState';
import AuthorizationBaseActions from './AuthorizationBaseActions';
import {action, observable, computed} from 'mobx';
import FileSaver from 'file-saver';
import { AndroidWebViewMessageType } from '../../..';

export default class SignUpActions extends AuthorizationBaseActions {
    @observable generatedLogin: NString = null;
    @observable generatedPassword: NString = null;
    @observable authorizationLink?: string;

    constructor(appState: AppState) {
        super(appState);
        this.onSignUpButtonClick = this.onSignUpButtonClick.bind(this);
        this.saveGeneratedCredentialsToFile = this.saveGeneratedCredentialsToFile.bind(this);
    }

    @computed
    get isAnonymousRegistartionEnabled(): boolean {
        return (
            this.appState.appSettings.registerForm
                .isAnonymousRegistrationWithCryptoEnabled &&
            this.appState.currencySelectState.cryptoOptions.length > 0
        );
    }

    onSignUpButtonClick(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        this.trySignUp();
    }

    // region ---- common
    trySignUp() {
        switch (this.appState.authorizationTypeState.type) {
            case AuthorizationType.BY_PHONE:
                this.trySignUpByPhone();
                break;
            case AuthorizationType.BY_EMAIL:
                this.trySignUpByEmail();
                break;
            case AuthorizationType.BY_LOGIN:
                this.trySignUpByLogin();
                break;
            case AuthorizationType.ANONYMOUS:
                this.trySignUpAnonymous();
                break;
            default:
                break;
        }
    }

    processSignUpSuccess() {
        this.appState.guestPopup.destroy();
        if (!this.appState.user.redirectToIncomingUrl()) {
            this.redirectFromAuthPages();
        }
        this.activatePromoCodeAfterAuth();
        this.appState.games.fetchGames(true, (msg, act) => {
            this.appState.firebaseApp.initWithRequestPermission(this.appState.siteConfig.firebaseConfig);
        });
        this.appState.jpMonitorAPI.check();
    }

    processSignUpError(error: users.RegisterResponse.RegisterError | null | undefined) {
        const errorTerm = getSignUpError(error);
        switch (error) {
            case users.RegisterResponse.RegisterError.phone_used:
            case users.RegisterResponse.RegisterError.country_code_required_for_phone:
            case users.RegisterResponse.RegisterError.invalid_phone:
            case users.RegisterResponse.RegisterError.unknown_country_code:
            case users.RegisterResponse.RegisterError.unknown_currency:
                this.appState.phoneInputState.error = errorTerm;
                this.appState.registerForm.phoneError = this.appState.t(errorTerm);
                break;
            case users.RegisterResponse.RegisterError.email_used:
            case users.RegisterResponse.RegisterError.forbidden_email:
            case users.RegisterResponse.RegisterError.invalid_email:
                this.appState.emailInputState.error = this.appState.t(errorTerm);
                this.appState.registerForm.emailError = this.appState.t(errorTerm);
                break;
            case users.RegisterResponse.RegisterError.password_too_short:
                this.appState.passwordInputState.error = this.appState.t(errorTerm);
                this.appState.registerForm.passwordError = this.appState.t(errorTerm);
                break;
            case users.RegisterResponse.RegisterError.invalid_fields:
                this.error = this.appState.t(errorTerm);
                this.appState.registerForm.userRequiredFieldsError = this.appState.t(errorTerm);
                break;
            case users.RegisterResponse.RegisterError.unknown_error:
            default:
                this.error = this.appState.t(errorTerm);
                this.appState.registerForm.error = this.appState.t(errorTerm);
                break;
        }
    }

    // endregion

    // region ---- phone
    trySignUpByPhone() {
        const phone = this.appState.phoneInputState.getValueOrSetError();
        const password = this.appState.passwordInputState.getValueOrSetError();
        const currency = this.appState.currencySelectState.getValueOrSetError();
        const termsAndConditions = this.appState.acceptTermsAndConditionsState.getValueOrSetError();
        const isValid = !!(phone && password && currency && termsAndConditions);
        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signUp(
                '',
                password!,
                phone!.phoneNumber,
                phone!.countryCode!,
                currency!,
                true,
                null,
                this.appState.confirmationCodeMethodState.isVoiceSelected,
                this.onSignUpByPhoneResponse.bind(this)
            );
        }
    }

    onSignUpByPhoneResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.register && act.register.ok) {
            this.appState.modal.showConfirmPhone();
            this.appState.confirmForm.activateResendTimer();
            this.processSignUpSuccess();
        } else {
            this.processSignUpError(act.register ? act.register.error : null);
        }
    }

    // endregion

    // region ---- email
    trySignUpByEmail() {
        const email = this.appState.emailInputState.getValueOrSetError();
        const password = this.appState.passwordInputState.getValueOrSetError();
        const currency = this.appState.currencySelectState.getValueOrSetError();
        const termsAndConditions = this.appState.acceptTermsAndConditionsState.getValueOrSetError();
        const isValid = !!(email && password && currency && termsAndConditions);
        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signUp(
                email!,
                password!,
                '',
                '',
                currency!,
                true,
                null,
                false,
                this.onSignUpByEmailResponse.bind(this, email!)
            );
        }
    }

    onSignUpByEmailResponse(email: string, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.register && act.register.ok) {
            this.appState.user.hideAuthWithoutRedirect();
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-email-confirmation-resend-successful', {email: email}),
                level: NotificationItemLevel.SUCCESS
            });
            this.processSignUpSuccess();
        } else {
            this.processSignUpError(act.register ? act.register.error : null);
        }
    }

    // endregion

    // region ---- one click registration
    trySignUpByLogin() {
        const currency = this.appState.currencySelectState.getValueOrSetError();
        const termsAndConditions = this.appState.acceptTermsAndConditionsState.getValueOrSetError();
        const isValid = !!(currency && termsAndConditions);

        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signUpOneClick(currency!, this.onSignUpOneClickResponse.bind(this));
        }
    }

    onSignUpOneClickResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.registerOneClick && act.registerOneClick.ok) {
            this.processSignUpSuccess();
            this.generatedLogin = act.registerOneClick.generatedLogin;
            this.generatedPassword = act.registerOneClick.generatedPassword;
            this.authorizationLink = undefined;
            this.appState.modal.showOneClickRegistrationSuccess();
        } else {
            this.processSignUpError(act.registerOneClick ? act.registerOneClick.error : null);
        }
    }

    saveGeneratedCredentialsToFile(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        e.stopPropagation();
        if (this.generatedLogin && this.generatedPassword) {
            const creds = [
                this.appState.t('ui-login-generated-login') + ': ' + this.generatedLogin,
                this.appState.t('ui-password') + ': ' + this.generatedPassword
            ];
            if (this.authorizationLink) {
                creds.push(this.appState.t('anonymous-registration-auth-url') + ': ' + this.authorizationLink)
            }
            if (this.appState.refillStore.cryptoAddressForm.address) {
                creds.push(this.appState.t('anonymous-registration-crypto-address') + ': ' + this.appState.refillStore.cryptoAddressForm.address)
            }
            const text = creds.join(', ');
            const blob = new Blob([text], {type: 'text/plain;charset=utf-8'});
            const fileName = this.appState.g('%SITE_NAME%') + '.txt';
            FileSaver.saveAs(blob, fileName);
            if (window.ReactNativeWebView?.postMessage) {
                window.ReactNativeWebView.postMessage(JSON.stringify({
                    type: AndroidWebViewMessageType.SAVE_FILE,
                    fileName,
                    text,
                }));
            } else if (window.AndroidWebView?.postMessage) {
                window.AndroidWebView.postMessage(JSON.stringify({
                    type: AndroidWebViewMessageType.SAVE_FILE,
                    fileName,
                    text,
                }));
            }
        } else {
            console.error('Unable to save generated credentials: ', this.generatedLogin, this.generatedPassword);
        }
    }

    // endregion

    // region ---- anonymous registration
    trySignUpAnonymous() {
        const currency = this.appState.currencySelectState.getValueOrSetError();
        const termsAndConditions = this.appState.acceptTermsAndConditionsState.getValueOrSetError();
        const isValid = !!(currency && termsAndConditions);

        if (isValid) {
            this.setLoading(true);
            this.resetErrors();
            this.appState.api.signUpOneClick(currency!, this.onSignUpAnonymousResponse.bind(this));
        }
    }

    onSignUpAnonymousResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.registerOneClick && act.registerOneClick.ok) {
            this.processSignUpSuccess();
            this.updateAnonymousState(act.registerOneClick);
            this.appState.modal.showAnonymousRegistrationSuccess();
            this.appState.refillStore.cryptoAddressForm.addCheckingTimeout();
        } else {
            this.processSignUpError(act.registerOneClick ? act.registerOneClick.error : null);
        }
    }

    @action.bound
    updateAnonymousState(registerOneClick: users.IRegisterOneClickResponse) {
        this.generatedLogin = registerOneClick.generatedLogin;
        this.generatedPassword = registerOneClick.generatedPassword;
        this.authorizationLink = registerOneClick.authorizationLink
            ? window.location.origin +
              registerOneClick.authorizationLink
            : undefined;
    }

    // endregion

    @action
    resetForm() {
        super.resetForm();
        this.generatedPassword = null;
        this.generatedLogin = null;
    }
}