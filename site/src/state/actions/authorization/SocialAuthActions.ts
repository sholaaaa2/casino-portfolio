import {action, observable} from 'mobx';
import AppState from '../../AppState';
import {site, messages} from '../../../api/proto';
import {extractLocationArgs} from '../../utils/BrowserUtils';
import {NotificationItemLevel} from '../../components/Notificator';
import Long from 'long';

export default class SocialAuthActions {
    private appState: AppState;
    @observable providers: site.Social.SocialProvider[];
    @observable facebookId?: string;
    @observable providerForRegistration?: number;
    @observable socialAuthError?: string;
    @observable signInLoading: boolean = false;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @action
    processProviders(providers: site.Social.SocialProvider[]) {
        if (providers) {
            this.providers = providers;
        }
    }

    get fbProviderIsActive() {
        return this.providers.indexOf(site.Social.SocialProvider.facebook) !== -1;
    }

    loginViaFb = () => {
        this.socialSignInRequest(site.Social.SocialProvider.facebook);
    }

    @action
    socialSignInRequest = (provider: site.Social.SocialProvider) => {
        this.signInLoading = true;
        this.appState.api.signInUsingSocial({
            provider,
            redirectUrl: `${window.location.origin}${`/social/${Object.keys(site.Social.SocialProvider)[provider]}`}`,
        }, this.onSocialSignInResponse);
    }

    @action
    onSocialSignInResponse = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        if (act?.login?.error && act?.login?.alternativeCredentials?.authUrl) {
            window.location.replace(act?.login?.alternativeCredentials?.authUrl);
        } else if (act?.login?.ok) {
            this.appState.signInActions.processSignInSuccess(act.login);
        }
        this.signInLoading = false;
    }

    checkRegistrationIsSocial = () => {
        const queryParams = extractLocationArgs();
        if (queryParams?.facebookId && queryParams?.provider) {
            this.facebookId = queryParams?.facebookId;
            this.providerForRegistration = Number(site.Social.SocialProvider[queryParams?.provider]);
        }
    }

    get socialRegistration() {
        return !!(this.facebookId && this.providerForRegistration);
    }

    @action
    tryRegister = (defaultCurrency?: boolean) => {
        const currency = defaultCurrency
           ? this.appState.currencySelectState.defaultValue
           : this.appState.currencySelectState.getValueOrSetError();
        if (currency && this.providerForRegistration && this.facebookId) {
            this.appState.api.signUpSocial(new Long(Number(currency)), Number(this.providerForRegistration), this.facebookId, this.onSignUpBySocialResponse)
        }
    }

    onSignUpBySocialResponse = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        this.appState.processApi(msg, act);

        if (act.register && act.register.ok) {
            this.appState.user.hideAuthWithoutRedirect();
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-social-registration-success'),
                level: NotificationItemLevel.SUCCESS
            });
            this.appState.signUpActions.processSignUpSuccess();
            this.appState.router.push(this.appState.l('/'))
        } else {
            this.appState.signUpActions.processSignUpError(act.register ? act.register.error : null);
        }
    }

    @action
    onCloseRegistration = (event?: React.MouseEvent<HTMLElement>) => {
        this.facebookId = undefined;
        this.providerForRegistration = undefined;
        this.appState.modal.hideModal(event);
        this.appState.resetForms();
        this.appState.router.push(this.appState.l('/'));
    }

    @action
    supportHandleClick = (e: React.MouseEvent<Element>) => {
        const a = (e.target as Element).closest("a");

        if (a && a.href && a.href.search("/support") !== -1) {
            e.preventDefault();
            this.appState.chat.expand();
        }
    }
}
