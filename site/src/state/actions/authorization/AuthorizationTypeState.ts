import {NString, Modal} from '../../meta';
import {action, observable} from 'mobx';
import {isValidAuthorizationType} from '../../utils/Validation';
import AppState from '../../AppState';
import {default as classNames} from 'classnames';
import { getParamsFromUrl } from '../../utils/BrowserUtils';
import { AUTH_GET_PARAMS } from '../../enums/AUTH_GET_PARAMS';

export enum AuthorizationType {
    BY_PHONE = 'phone',
    BY_EMAIL = 'email',
    BY_LOGIN = 'login',
    ANONYMOUS = 'anonymous'
}

export class AuthorizationTypeState {
    @observable type: AuthorizationType = AuthorizationType.BY_EMAIL;

    constructor(private appState: AppState) {
        this.onButtonClick = this.onButtonClick.bind(this);
    }

    onButtonClick(e: React.MouseEvent<HTMLButtonElement>) {
        const value = e.currentTarget.getAttribute('data-type');
        this.setType(value);
    }

    getTypeFromUrl() {
        const [email, phone, login] = getParamsFromUrl([
            AUTH_GET_PARAMS.EMAIL,
            AUTH_GET_PARAMS.PHONE,
            AUTH_GET_PARAMS.LOGIN
        ]);
        if (email) {
            this.setType(AuthorizationType.BY_EMAIL);
        } else if (phone) {
            this.setType(AuthorizationType.BY_PHONE);
        } else if (login) {
            this.setType(AuthorizationType.BY_LOGIN);
        }
    }

    @action.bound
    setType(value: NString) {
        if (isValidAuthorizationType(value)) {
            this.type = value as AuthorizationType;
            this.appState.signInActions.resetForm();
            this.appState.signUpActions.resetForm();

            if (value === AuthorizationType.BY_PHONE) {
                if (this.appState.modal.activeModal === Modal.LOGIN) {
                    this.appState.phoneInputState.getStateFromUrl();
                }
            } else if (value === AuthorizationType.BY_EMAIL) {
                this.appState.loginType = AuthorizationType.BY_EMAIL;
                if (this.appState.modal.activeModal === Modal.LOGIN) {
                    this.appState.emailInputState.getStateFromUrl();
                }
            } else if (value === AuthorizationType.BY_LOGIN) {
                this.appState.loginType = AuthorizationType.BY_LOGIN;
                if (this.appState.modal.activeModal === Modal.LOGIN) {
                    this.appState.loginInputState.getStateFromUrl();
                }
            }
        } else {
            console.error('Unknown authorization type: ', value);
        }
    }

    getClassName(isActive: boolean): string {
        return classNames({
            'authorization-tab-item--active': isActive,
            'authorization-tab-item--disabled': this.appState.signInActions.isLoading || this.appState.signUpActions.isLoading
        });
    }
}
