import {observable} from 'mobx';
import AppState from '../AppState';

export default class VLobbyActions {
    appState: AppState;
    @observable domain: string | null = null;

    constructor(appState: AppState) {
        this.appState = appState;
    }
}