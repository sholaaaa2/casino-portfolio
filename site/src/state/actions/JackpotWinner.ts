import * as currencies from '../utils/Currencies';
import AppState from '../AppState';
import {action, observable} from 'mobx';
import Long from 'long';
import {formatCurrencyAmount} from '../utils/Money';
import {v4 as uuid} from 'uuid';
import {NotificationAnimationClass} from '../meta';
import {updateFadeClasses} from '../../utils/common';
import {GameItem} from './GamesActions';
import { JackpotWinState } from './jackpots/JackpotWinState';

export interface JackpotWinnerData {
    playerId?: Long;
    title: string;
    jackpotCaption: string;
    formattedAmount: string;
    notificationId?: string;
}

export class JackpotWinner {
    @observable winner: JackpotWinnerData | null = null;

    appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;

        this.resetJackpotWinner = this.resetJackpotWinner.bind(this);
        this.onModalJackpotClose = this.onModalJackpotClose.bind(this);
    }

    @action
    setJackpotWinner(jackpotWinner: JackpotWinnerData | null) {
        this.winner = jackpotWinner;
    }

    onModalJackpotClose(e: React.MouseEvent<HTMLElement>) {
        const notificationId = e.currentTarget.getAttribute('data-big-win-notification-id');
        if (notificationId) {
            this.appState.bonuses.execRefuseBonus(notificationId);
        }
        this.closeJackpot();
    }

    closeJackpot() {
        const modalJackpot = this.appState.getRef('modal-jackpot');
        if (modalJackpot) {
            updateFadeClasses(false, modalJackpot, null, this.resetJackpotWinner);
        }
    }

    resetJackpotWinner() {
        this.appState.modal.hideModal();
        this.winner = null;
    }

    @action
    showYouWonJackpot(amount: Long, currency: Long) {
        this.showWin(
            this.appState.t('ui-you-won'),
            this.appState.t('ui-jackpot'),
            formatCurrencyAmount(amount, currencies.findCurrencyById(currency))
        );
    }

    @action
    showYouWonTournament(name: string, amount: string, notificationId: string) {
        this.showWin(
            this.appState.t('ui-tournament'),
            this.appState.t(name),
            amount,
            notificationId
        );
    }

    @action
    showWin(title: string, caption: string, amount: string, notificationId?: string) {
        if (this.winner) {
            this.resetJackpotWinner();
        }

        if (!this.winner) {
            this.setJackpotWinner({
                title: title,
                jackpotCaption: caption,
                formattedAmount: amount,
                notificationId
            });
            this.appState.modal.showJackpotWinner();
        }
    }

    @action
    showSomeoneWonJackpot(player: string, amount: Long, currency: Long, game?: GameItem | null) {
        const notification = {
            id: uuid(),
            caption: player,
            infoTitle: this.appState.t('ui-won'),
            infoCaption: this.appState.t('ui-jackpot'),
            infoAmount: formatCurrencyAmount(amount, currencies.findCurrencyById(currency)),
            showGameInfo: !!game,
            gameInfoTitle: this.appState.t('ui-in-game'),
            gameInfoHref: game ? this.appState.games.url(game.id!) : '',
            gameInfoImg: game ? this.appState.games.getGameImage(game) : '',
            gameInfoCaption: game ? this.appState.games.getGameName(game.id!) : '',
            animationClass: NotificationAnimationClass.QUEUED
        };

        this.appState.jackpotNotifications.addNotification(notification);
    }

    processWinNotification(win: JackpotWinState) {
        const runningGame = this.appState.runningGameStore.runningGame;
        if (runningGame?.siteJackpotSupport) {
            return;
        }
        if (win.currencyInfo?.id) {
            if (this.appState.userInfo.userId.eq(win.userId)) {
                if (runningGame && runningGame.disableJackpotWinNotification) {
                    return;
                }
                this.showYouWonJackpot(win.amount, win.currencyInfo.id);
            } else {
                this.showSomeoneWonJackpot(win.nickname, win.amount, win.currencyInfo.id, win.gameObj);
            }
        } else {
            console.error('Got jackpot with missing currencyId: ', win);
        }
    }

    // region ---- test function
    showTestSomeoneWonJackpot(hideGame?: boolean) {
        const amount = Long.fromNumber(5000);
        const currency = Long.fromNumber(840);
        const player = 'vv.pu***mail.com';
        const game = hideGame ? null : {
            id: 'ib_fc_d',
            icon: {ru: {images: [{src: '/media/thumb/300x300/ib_fc_d.jpeg'}]}}
        };

        this.showSomeoneWonJackpot(player, amount, currency, game);
    }

    showTestYouWonJackpot() {
        const amount = Long.fromNumber(5000);
        const currency = Long.fromNumber(840);
        this.showYouWonJackpot(amount, currency);
    }

    // endregion
}
