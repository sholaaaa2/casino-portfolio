import {computed, observable} from 'mobx';
import {commons, jpmonitor} from '../../../api/proto';
import {formatCurrencyAmount, formatJackpotStyle, formatCurrencyCode} from '../../utils/Money';
import AppState from '../../AppState';
import Long from 'long';

export class JackpotState extends jpmonitor.JackpotState {
    @observable loading: boolean = false;
    @observable enabled: boolean = false;
    @observable name: string;
    currencyFormattingFn: (value: number) => string = (value: number) => {
        return formatCurrencyAmount(Long.fromNumber(value), this.currencyInfo, false);
    };

    constructor(private appState: AppState, jpState: jpmonitor.IJackpotState | undefined) {
        super(jpState);
        this.enabled = !!jpState;
        this.name = jpState?.name ? jpState.name : appState.t?.('ui-jackpot');
    }

    @computed
    get style(): string {
        return formatJackpotStyle(this);
    }

    @computed
    get currencyInfo(): commons.ICurrency | null {
        return (
            this.appState.siteConfig.currencies?.supported?.find((_) => _.code === this.currency) ??
            null
        );
    }

    @computed
    get amountString(): string {
        return formatCurrencyAmount(this.value, this.currencyInfo, true);
    }

    @computed
    get amountNumber(): number {
        return this.value?.toNumber() ?? 0;
    }

    @computed
    get sourceAmount(): number {
        return this.amountNumber;
    }

    @computed
    get minBetCurrency(): string {
        return formatCurrencyCode(this.currencyInfo);
    }

    @computed
    get minBetAmount(): string {
        return formatCurrencyAmount(this.minBet, this.currencyInfo, true);
    }

    @computed
    get state(): jpmonitor.JackpotState.JackpotBlinkingState {
        return this.blinkingState;
    }
}
