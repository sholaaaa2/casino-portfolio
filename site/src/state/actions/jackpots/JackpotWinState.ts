import {computed} from 'mobx';
import {commons, jpmonitor} from '../../../api/proto';
import {formatCurrencyAmount, formatJackpotName, formatJackpotStyle} from '../../utils/Money';
import AppState from '../../AppState';
import {JackpotState} from './JackpotState';
import {LastActionType} from '../LastWinsActions';
import {GameImageSize, GameItem} from '../GamesActions';
import {mutePhoneCode} from '../../../api/utils';
import {gameNameWithoutAliases} from '../../utils/GameUtils';

export class JackpotWinState extends jpmonitor.SiteFeedMessage {
    idString: string;
    type = LastActionType.WIN;

    constructor(private appState: AppState, win: jpmonitor.ISiteFeedMessage) {
        super(win);
        this.idString = win.id?.toString() ?? '';
    }

    @computed
    get jackpot(): JackpotState | undefined {
        return this.appState.jackpots.allJackpots.find((_) => _.name === this.jackpotName);
    }

    @computed
    get jackpotStyle(): string {
        return formatJackpotStyle(this);
    }

    @computed
    get jackpotName(): string {
        return formatJackpotName(this);
    }

    @computed
    get currencyInfo(): commons.ICurrency | null {
        return (
            this.appState.siteConfig.currencies?.supported?.find((_) => _.id?.eq(this.currency)) ??
            null
        );
    }

    @computed
    get amountString(): string {
        return formatCurrencyAmount(this.amount, this.currencyInfo);
    }

    @computed
    get date(): Date {
        return new Date(this.stamp?.toNumber() ?? 0);
    }

    @computed
    get gameObj(): GameItem | null | undefined {
        return this.appState.games.findGameById(this.gameId);
    }

    @computed
    get game(): string | undefined {
        const name = this.appState.games.getGameName(this.gameObj);
        return gameNameWithoutAliases(name);
    }

    @computed
    get link(): string | undefined {
        return this.gameObj ? this.appState.games.url(this.gameId) : undefined;
    }

    @computed
    get icon(): string | undefined {
        return this.gameObj
            ? this.appState.games.getGameImage(this.gameObj, GameImageSize.SIZE_40x40)
            : '/images/jackpots/default-jackpot-image.jpg';
    }

    @computed
    get image(): string | undefined {
        return this.gameObj
            ? this.appState.games.getGameImage(this.gameObj, GameImageSize.SIZE_300x188)
            : '/images/jackpots/default-jackpot-image.jpg';
    }

    @computed
    get title(): string {
        return (
            this.appState.t('ui-game') +
            ' - ' +
            this.game +
            '\n' +
            this.appState.t('ui-player') +
            ' - ' +
            this.user +
            '\n' +
            this.appState.t('ui-prize-won') +
            ' - ' +
            this.amountString +
            '\n' +
            this.appState.t('ui-date') +
            ' - ' +
            this.appState.format(this.date, 'dd LLL yyyy HH:mm:ss')
        );
    }

    get currencyCode(): string | undefined {
        return this.currencyInfo?.code ?? undefined;
    }

    get user(): string {
        if (this.appState.appSettings.mutePhoneCodes) {
            return mutePhoneCode(this.userName) ?? this.userName;
        }
        return this.userName;
    }

    get nickname(): string {
        return this.userName;
    }

    get timestamp(): number | undefined {
        return this.stamp?.toNumber();
    }

    get name(): string {
        return this.jackpotId;
    }
}
