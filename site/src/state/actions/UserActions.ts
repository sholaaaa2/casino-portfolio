import * as currencies from '../utils/Currencies';
import Long from 'long';
import {action, computed, observable} from 'mobx';
import {ChangeEvent, ReactEventHandler, SyntheticEvent} from 'react';
import {RegisterCustomFields} from '../../api/APIConnector';
import {messages, site, users} from '../../api/proto';
import AppState from '../AppState';
import {allowedNumberKeys, parseDate} from '../utils/BrowserUtils';
import {UserBalance} from '../UserInfo';
import {NotificationItemLevel} from '../components/Notificator';
import {formatCurrencyCode} from '../utils/Money';
import {getPhoneConfirmationError} from '../../api/utils';
import {isValidEmail} from '../utils/Validation';
import { Modal, SitePages } from '../meta';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import { matchPath } from 'react-router';
import { BM_PASSWORD_LENGTH } from '../controls/PasswordInputState';
import { AndroidWebViewMessageType } from '../..';

// region authorization forms

export class LoginForm {
    @observable loginVal: string = '';
    @observable loginPassVal: string = '';

    @observable pinVal: string = '';
    @observable pinPassVal: string = '';

    @observable phoneError: string | null = null;
    @observable emailError: string | null = null;
    @observable passwordError: string | null = null;
    @observable loginError: string | null = null;
    @observable generatedLoginError: string | null = null;
    @observable generatedPasswordError: string | null = null;
    @observable generatedError: string | null = null;
    @observable tokenError: string | null = null;
    @observable loading: boolean = false;
    showCashierPasswordInputAsTel = window.CSS && window.CSS.supports ? window.CSS.supports("-webkit-text-security", "disc") : false;

    constructor() {
        this.resetField = this.resetField.bind(this);
    }

    @action
    resetErrors = () => {
        this.phoneError = null;
        this.emailError = null;
        this.passwordError = null;
        this.loginError = null;
        this.generatedLoginError = null;
        this.generatedPasswordError = null;
        this.generatedError = null;
        this.tokenError = null;
        this.loading = false;
    }

    @action
    resetValues = () => {
        this.loginVal = '';
        this.loginPassVal = '';
        this.pinVal = '';
        this.pinPassVal = '';
    }

    @action
    reset() {
        this.resetErrors();
        this.resetValues();
    }

    resetField(e: SyntheticEvent<HTMLInputElement>) {
        const field = e.currentTarget.name + 'Error';
        if ((this as Object).hasOwnProperty(field)) {
            this[field] = undefined;
        }
    }

    @action.bound
    preventNotNumberInput(e: React.KeyboardEvent<HTMLInputElement>) {
        const isAllowedKey = /^[0-9]$/i.test(e.key);
        if (!isAllowedKey && !e.metaKey && !e.ctrlKey) {
            e.preventDefault();
            e.stopPropagation();
        }
    }

    @action
    onChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.loginVal = e.currentTarget.value;
    }

    @action
    onChangeLoginPass = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.loginPassVal = e.currentTarget.value;
    }

    @action
    onChangePin = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.pinVal = e.currentTarget.value;
    }

    @action
    onChangePinPass = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.pinPassVal = e.currentTarget.value;
    }
}

export class RegisterForm {
    @observable loading: boolean = false;

    @observable phone: string | null = null;
    @observable phoneCode: string | null = null;
    @observable email: string | null = null;
    @observable password: string | null = null;
    @observable countryCode: string | null = null;
    @observable currency: Long | null = null;

    @observable error: string | null = null;
    @observable phoneError: string | null = null;
    @observable emailError: string | null = null;
    @observable passwordError: string | null = null;
    @observable currencyError: string | null = null;

    @observable userRequiredFieldsError: string | null = null;
    @observable userFields: { [key: string]: string | number | Long } = {};
    @observable userFieldsError: { [key: string]: string } = {};


    @observable loginVal: string = '';
    @observable loginPassVal: string = '';

    @action
    resetValues = () => {
        this.phone = null;
        this.email = null;
        this.password = null;
        this.countryCode = null;
        this.currency = null;
        this.userFields = {};

        this.loginVal = '';
        this.loginPassVal = '';
    }

    @action
    reset() {
        this.loading = false;

        this.resetErrors();
        this.resetValues();
    }

    @action
    setUserFieldError(name: string, value: string) {
        const errors = {...this.userFieldsError};
        errors[name] = value;
        this.userFieldsError = errors;
    }

    @action
    resetErrors() {
        this.error = null;
        this.phoneError = null;
        this.emailError = null;
        this.passwordError = null;
        this.currencyError = null;
        this.userRequiredFieldsError = null;
        this.userFieldsError = {};
    }

    getFirstStepError() {
        return this.error || this.phoneError || this.emailError || this.passwordError || this.currencyError;
    }

    getSecondStepError() {
        return this.userRequiredFieldsError || this.getFirstStepError();
    }

    buildUserFields(userFields: site.IUserFieldDescription[] | null | undefined): RegisterCustomFields | null {
        if (userFields) {
            const result = {};
            for (let field of userFields) {
                let name = field.name!;
                const value = this.userFields[name];
                if (value !== undefined) {
                    if (field.type === 'datetime') {
                        result[name] = new users.RegisterRequest.FieldValue({datetimeValue: new Date(value as number).toISOString()});
                    } else if (field.type === 'uint') {
                        result[name] = new users.RegisterRequest.FieldValue({uintValue: value as Long});
                    } else {
                        result[name] = new users.RegisterRequest.FieldValue({stringValue: value as string});
                    }
                }
            }
            return result;
        } else {
            return null;
        }
    }

    parseUserFieldValue(field: site.IUserFieldDescription, value: string): Long | number | string | null {
        if (field.type === 'uint') {
            return Long.fromString(value);
        } else if (field.type === 'datetime') {
            return parseDate(value);
        } else {
            return value.trim();
        }
    }

    @action
    onChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.loginVal = e.currentTarget.value;
    }

    @action
    onChangeLoginPass = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.loginPassVal = e.currentTarget.value;
    }
}

// endregion

// region ---- restore forms
export class RestoreForm {
    @observable phoneError: string | null = null;
    @observable emailError: string | null = null;
    @observable loading: boolean = false;
    @observable error: string | null = null;
    @observable success: boolean = false;
    @observable phone: string | null = null;
    @observable country: string | null = null;
    @observable codeError: string | null = null;
    @observable passwordError: string | null = null;
    @observable restored: boolean = false;

    @observable code: string = '';
    @observable newPassword: string = '';

    @action
    reset() {
        this.phoneError = null;
        this.emailError = null;
        this.loading = false;
        this.error = null;
        this.success = false;
        this.phone = null;
        this.country = null;
        this.codeError = null;
        this.passwordError = null;
        this.restored = false;
        this.code = '';
        this.newPassword = '';
    }

    @action
    resetErrors() {
        this.error = null;
        this.phoneError = null;
        this.emailError = null;
        this.codeError = null;
        this.passwordError = null;
        this.error = null;
    }
}

export class RestoreEmailForm {
    @observable email: string | null = null;
    @observable code: string | null = null;
    @observable newPassword: string | null = null;

    reset() {
        this.email = null;
        this.code = null;
        this.newPassword = null;
    }
}

// endregion

// region ---- confirmation forms
export class ConfirmForm {
    @observable canResend: boolean = false;
    @observable codeError: string | null = null;
    @observable loading: boolean = false;
    @observable sending: boolean = false;
    @observable error: string | null = null;
    @observable success: boolean = false;
    @observable timeLeft: number = 0;
    private appState: AppState;
    private timerId: number;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @computed
    get isResendButtonShown(): boolean {
        return !this.appState.ref.isOwnPhoneSubmitting;
    }

    @computed
    get timeLeftFormatted(): string {
        return this.appState.t('ui-you-can-request-new-code-timer', {'time': this.timeLeft});
    }

    @computed
    get resendButtonTitle(): string {
        const isResendTimerActive = this.timeLeft > 0;
        const mins = Math.floor(this.timeLeft / 60);
        const secs = this.timeLeft - mins * 60;
        return `${
            this.appState.confirmationCodeMethodState.isChoiseAvailable
                ? this.appState.confirmationCodeMethodState.isFirstRetry
                    ? this.appState.t('ui-send-confirmation-by-call')
                    : this.appState.t('ui-resend-confirmation-by-call')
                : this.appState.confirmationCodeMethodState.selectedMethod === site.ConfirmationMethod.VOICE
                    ? this.appState.t('ui-resend-confirmation-by-call')
                    : this.appState.t('ui-resend-confirmation')
        }${isResendTimerActive ? ' (' + mins + ":" + (secs < 10 ? '0' + secs : secs) + ')' : ''}`;
    }

    @action.bound
    reset() {
        this.canResend = true;
        this.codeError = null;
        this.loading = false;
        this.sending = false;
        this.error = null;
        this.success = false;
    }

    @action.bound
    activateResendTimer() {
        this.timeLeft = this.appState.appSettings.registerForm.resendConfirmationCodeTimeout;
        window.clearInterval(this.timerId);
        this.timerId = window.setInterval(this.tickTimer, 1000);
    }

    @action.bound
    tickTimer() {
        if (this.timeLeft === 0) {
            window.clearInterval(this.timerId);
        } else {
            this.timeLeft = this.timeLeft - 1;
        }
    }
}

export class ConfirmEmailForm {
    @observable code: string | null = null;
    @observable email: string | null = null;
    @observable emailError: string | null = null;
    @observable codeError: string | null = null;
    @observable error: string | null = null;
    @observable loading: boolean = false;

    reset() {
        this.emailError = null;
        this.codeError = null;
        this.error = null;
        this.loading = false;
    }
}

export class ConfirmEmailSentForm {
    @observable sending: boolean = false;
    @observable sent: boolean = false;
    @observable error: string | null = null;

    reset() {
        this.sending = false;
        this.sent = false;
        this.error = null;
    }
}

export class ConfirmRestorePasswordByMailForm {
    @observable loading: boolean = false;
    @observable success: boolean = false;
    @observable error: string | null = null;
    @observable passwordError: string | null = null;

    reset() {
        this.loading = false;
        this.success = false;
        this.error = null;
        this.passwordError = null;
    }
}

// endregion

export default class UserActions {
    private appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;

        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.resetPassword = this.resetPassword.bind(this);
        this.hideAuth = this.hideAuth.bind(this);
        this.setLoginType = this.setLoginType.bind(this);
        this.confirmationDialog = this.confirmationDialog.bind(this);
        this.setRegisterFormGender = this.setRegisterFormGender.bind(this);
        this.changeConfirmEmailCode = this.changeConfirmEmailCode.bind(this);
        this.tryLogin = this.tryLogin.bind(this);
        this.tryResetPassword = this.tryResetPassword.bind(this);
        this.restoreBack = this.restoreBack.bind(this);
        this.tryRegister = this.tryRegister.bind(this);
        this.tryConfirmPhoneRegistration = this.tryConfirmPhoneRegistration.bind(this);
        this.tryResendPhoneConfirmation = this.tryResendPhoneConfirmation.bind(this);
        this.tryResendEmailConfirmation = this.tryResendEmailConfirmation.bind(this);
        this.tryConfirmEmail = this.tryConfirmEmail.bind(this);
        this.tryConfirmRestoreByPhone = this.tryConfirmRestoreByPhone.bind(this);
        this.tryConfirmRestorePassword = this.tryConfirmRestorePassword.bind(this);
        this.closeRestoreDialog = this.closeRestoreDialog.bind(this);
        this.changeRestoreFormCode = this.changeRestoreFormCode.bind(this);
        this.changeRestoreFormNewPassword = this.changeRestoreFormNewPassword.bind(this);
        this.showRegistrationSecondStep = this.showRegistrationSecondStep.bind(this);
        this.tryLoginUsingGeneratedLogin = this.tryLoginUsingGeneratedLogin.bind(this);
        this.tryLogout = this.tryLogout.bind(this);
    }

    @computed
    get isFromKiosk(): boolean {
        return !!this.appState.rawUserInfo?.tags && this.appState.rawUserInfo.tags.includes('wl-device');
    }

    @computed
    get isKioskLobbyEnabled(): boolean {
        return (
            this.appState.appSettings.kioskLobby.enabled &&
            (!this.appState.appSettings.kioskLobby.electronOnly ||
                this.appState.siteConfig.applicationType ===
                    site.SiteConfigResponse.ApplicationType.ELECTRON_APP)
        );
    }

    // region ---- login/logout
    @action
    setLoginType(event: ChangeEvent<HTMLSelectElement>) {
        this.appState.loginType = event.target.value;
        this.appState.resetForms();
    }

    @action
    showLogin() {
        this.appState.modal.showLogin();
        this.appState.resetForms();
    }

    @action
    tryLogin(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        event.stopPropagation();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        let country = this.appState.getRef('loginCountry') ?
            (this.appState.getRef('loginCountry') as HTMLSelectElement).value
            : '';
        let phone = this.appState.getRef('loginPhone')
            ? (this.appState.getRef('loginPhone') as HTMLInputElement).value
            : '';
        let email = this.appState.getRef('loginEmail')
            ? (this.appState.getRef('loginEmail') as HTMLInputElement).value
            : '';
        let pass = this.appState.getRef('loginPassword')
            ? (this.appState.getRef('loginPassword') as HTMLInputElement).value
            : '';
        let inputType = this.appState.loginType;

        if (!phone && !email) {
            const phoneOrEmail = this.appState.getRef('loginOrPhoneOrEmail')
                ? (this.appState.getRef('loginOrPhoneOrEmail') as HTMLInputElement).value
                : '';
            if (phoneOrEmail.indexOf('@') > 0) {
                inputType = 'email';
                email = phoneOrEmail;
            } else {
                inputType = 'phone';
                phone = phoneOrEmail;
                const phoneNumber = parsePhoneNumberFromString(phoneOrEmail);
                if (phoneNumber && phoneNumber.country) {
                    country = phoneNumber.country;
                }
            }
        }

        let loginState = this.appState.loginForm;
        loginState.reset();

        let valid = true;

        if (inputType === 'phone' && (!country || !phone)) {
            loginState.phoneError = this.appState.t('error-login-phone-required');
            valid = false;
        } else if (inputType === 'email' && !email) {
            loginState.emailError = this.appState.t('error-login-email-required');
            valid = false;
        }
        if (!pass) {
            loginState.passwordError = this.appState.t('error-login-password-required');
            valid = false;
        } else if (pass.length < 6) {
            loginState.passwordError = this.appState.t('error-password-too-short');
            valid = false;
        }

        if (valid) {
            loginState.loading = true;
            this.appState.api.signIn(email, country, phone, pass, this.appState.signInActions.onSignInResponse.bind(this.appState.signInActions));
        }
    }

    @action
    tryMultiLogin = (event: SyntheticEvent<HTMLElement>) => {
        event.preventDefault();
        event.stopPropagation();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        let login = '';
        let country = '';
        let phone = '';
        let email = '';
        let pass = this.appState.getRef('loginPassword')
            ? (this.appState.getRef('loginPassword') as HTMLInputElement).value
            : '';

        const loginOrPhoneOrEmail = this.appState.getRef('loginOrPhoneOrEmail')
            ? (this.appState.getRef('loginOrPhoneOrEmail') as HTMLInputElement).value
            : '';

        if (/^\d{8}$/.test(loginOrPhoneOrEmail)) {
            login = loginOrPhoneOrEmail;
        } else if (loginOrPhoneOrEmail.indexOf('@') > 0) {
            this.appState.loginType = 'email';
            email = loginOrPhoneOrEmail;
        } else {
            this.appState.loginType = 'phone';
            phone = loginOrPhoneOrEmail;
            const phoneNumber = parsePhoneNumberFromString(loginOrPhoneOrEmail);
            if (phoneNumber && phoneNumber.country) {
                country = phoneNumber.country;
            }
        }

        let loginState = this.appState.loginForm;
        loginState.resetErrors();

        let valid = true;

        if (!login && this.appState.loginType === 'phone' && (!country || !phone)) {
            loginState.phoneError = this.appState.t('error-login-phone-required');
            valid = false;
        } else if (!login && this.appState.loginType === 'email' && !email) {
            loginState.emailError = this.appState.t('error-login-email-required');
            valid = false;
        }
        if (!pass) {
            loginState.passwordError = this.appState.t('error-login-password-required');
            valid = false;
        } else if (pass.length < 6) {
            loginState.passwordError = this.appState.t('error-password-too-short');
            valid = false;
        }

        if (valid) {
            loginState.loading = true;
            if (login) {
                this.appState.api.signInUsingGeneratedLogin(login, pass, this.appState.signInActions.onSignInResponse.bind(this.appState.signInActions));
            } else {
                this.appState.api.signIn(email, country, phone, pass, this.appState.signInActions.onSignInResponse.bind(this.appState.signInActions));
            }
        }
    }

    @action
    tryLoginUsingGeneratedLogin(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        const loginState = this.appState.loginForm;
        loginState.reset();

        const loginRef = this.appState.getRef('generatedLogin') as HTMLInputElement;
        const passwordRef = this.appState.getRef('loginPassword') as HTMLInputElement;
        const login = loginRef ? loginRef.value : '';
        const password = passwordRef ? passwordRef.value : '';

        let valid = true;

        if (!login) {
            valid = false;
            this.appState.loginForm.generatedLoginError = this.appState.t('error-generated-login-required');
        }

        if (!password || password.length < BM_PASSWORD_LENGTH) {
            this.appState.loginForm.passwordError = this.appState.t('error-login-password-required');
            valid = false;
        }

        if (valid) {
            loginState.loading = true;
            this.appState.api.signInUsingGeneratedLogin(login, password, this.appState.signInActions.onSignInResponse.bind(this.appState.signInActions));
        }
    }

    @action
    tryLoginWithGenerated = (event: SyntheticEvent<HTMLElement>) => {
        event.preventDefault();

        const loginState = this.appState.loginForm;
        loginState.resetErrors();

        const loginRef = this.appState.getRef('generatedLogin') as HTMLInputElement;
        const passwordRef = this.appState.getRef('generatedPassword') as HTMLInputElement;
        const login = loginRef ? loginRef.value.replace(/-/gim, '') : '';
        const password = passwordRef ? passwordRef.value : '';

        let valid = true;

        if (!login) {
            valid = false;
            this.appState.loginForm.generatedLoginError = this.appState.t('error-generated-login-required');
        }

        if (!password || password.length < 6) {
            this.appState.loginForm.generatedPasswordError = this.appState.t('error-login-password-required');
            valid = false;
        }

        if (valid) {
            loginState.loading = true;
            this.appState.api.signInUsingGeneratedLogin(login, password, this.appState.signInActions.onSignInWithGeneratedResponse);
        }
    }

    redirectToIncomingUrl(): boolean {
        if (this.appState.userInfo.incomingUrl) {
            const url = new URL(this.appState.userInfo.incomingUrl);
            const langRule = `/:lang${this.appState.localesGroupsRe}?`;
            const match = matchPath(url.pathname, {
                path: `${langRule}/game/:id`,
                exact: true,
                strict: false
              });
            const matchLobby = matchPath(url.pathname, {
                path: `${langRule}/:id`,
                exact: true,
                strict: false
            });
            const matchedPath = match ?? matchLobby;
            if (!!matchedPath?.params["id"] &&
                !!this.appState.games.findGameById(matchedPath.params["id"])
            ) {
                this.appState.router.push(url.pathname + url.search);
                return true;
            }
        }
        return false;
    }

    login(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        this.showLogin();
    }

    @action
    tryLogout(event?: SyntheticEvent<HTMLElement>) {
        if (event) {
            event.preventDefault();
        }

        const appState = this.appState;
        if (appState.skillWheel.isEnabled) {
            appState.skillWheel.startSkillWheel();
        } else {
            this.logout();
        }
    }

    logout(event?: SyntheticEvent<HTMLElement>, goToMainPage?: boolean, withoutIsLoadingChange?: boolean) {
        const isChangingAccountForGame = this.appState.modal.activeModal === Modal.GAME_REQUIRES_ANOTHER_CURRENCY;
        if (event) {
            event.preventDefault();
        }
        if (!withoutIsLoadingChange) this.appState.loading = true;
        this.appState.api.signOut((msg, act) => {
            this.appState.userInfo.guest = true;
            this.appState.modal.resetState();
            if (!isChangingAccountForGame) {
                this.appState.userInfo.incomingUrl = null;
            }
            this.appState.processApi(msg, act);
            if (this.isKioskLobbyEnabled) {
                this.backToLobby();
            } else if (isChangingAccountForGame) {
                this.appState.loading = false;
                this.appState.router.push(this.appState.l('/login'));
            } else if  (goToMainPage) {
                window.location.href = '/';
            } else {
                window.location.reload();
            }
        });
    }

    @action.bound
    doLogout() {
        this.appState.loading = true;
        document.location.href = this.appState.l('/');
    }

    @action.bound
    redirectToMainPageOrRefreshGamePageAfterLogout() {
        if (this.appState.page === SitePages.GAME) {
            document.location.reload();
        } else {
            this.doLogout();
        }
    }

    @action.bound
    redirectToLoginAfterLogout() {
        document.location.href = this.appState.l('/login');
    }

    // @action.bound
    // keepAlive(e?: React.MouseEvent<HTMLElement>) {
    //     this.appState.api.keepAlive(this.onKeepAliveResponse.bind(this));
    // }

    // @action.bound
    // onKeepAliveResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
    //     this.appState.modal.hideModal();
    // }

    @action.bound
    hideModalAndOpenMainPage(e?: React.MouseEvent<HTMLElement>) {
        this.appState.modal.hideModal(e);
        this.appState.router.push(this.appState.l('/'));
    }


    @action.bound
    backToLobby() {
        if (
            this.appState.page !== SitePages.KIOSK_LOBBY &&
            this.isKioskLobbyEnabled
        ) {
            this.appState.site.isKioskLobbyShown = false;
        }
    }

    @action.bound
    hideModalAndOpenWithdrawalsPage(e?: React.MouseEvent<HTMLElement>) {
        this.appState.modal.hideModal(e);
        this.appState.router.push(this.appState.l('/withdraw'));
    }

    // endregion

    // region ---- registration
    @computed
    get isSiteRegistered(): boolean {
        const isSiteRegistration = !this.appState.rawUserInfo || this.appState.rawUserInfo.registrationMethod === users.UserInfo.UserRegistrationMethod.SITE_REGISTRATION;
        const showPaywaysForAuthorized = !this.appState.siteConfig || !!this.appState.siteConfig.showPaywaysForAuthorized;
        const isAuthorized = this.appState.site.isAuthorized;
        return isSiteRegistration || (isAuthorized && showPaywaysForAuthorized);
    }

    @action
    hideAuth(event: React.MouseEvent<HTMLElement>) {
        this.appState.modal.hideModal(event);
        this.appState.resetForms();
        this.appState.router.push(this.appState.l('/'));
    }

    @action.bound
    hideAuthWithoutRedirect(event?: React.MouseEvent<HTMLElement>) {
        if (this.appState.pages.isPageWithEmbeddedContent && !this.appState.userInfo.guest) {
            window.location.reload();
        }
        this.appState.modal.hideModal(event);
        this.appState.resetForms();
    }

    @action
    setRegisterFormGender(event: ChangeEvent<HTMLSelectElement>) {
        const userFields = {...this.appState.registerForm.userFields};
        userFields['gender'] = event.target.value;
        this.appState.registerForm.userFields = userFields;
    }

    @action
    tryRegister(event?: SyntheticEvent<HTMLElement>) {
        if (event) {
            event.preventDefault();

            if ((event.target as HTMLInputElement).disabled) {
                return;
            }
        }

        const registerState = this.appState.registerForm;
        const isSecondRegistrationStepRequited = this.appState.site.hasRequiredUserFields;

        const isValid = isSecondRegistrationStepRequited ?
            this.validateRegisterSecondStepFields() :
            this.validateRegisterFirstStepFields();

        if (isValid) {
            registerState.loading = true;
            const userFields = isSecondRegistrationStepRequited ?
                registerState.buildUserFields(this.appState.siteConfig.userFields) :
                null;

            this.appState.api.signUp(
                registerState.email!,
                registerState.password!,
                registerState.phone!,
                registerState.countryCode!,
                registerState.currency!,
                true,
                userFields,
                this.appState.confirmationCodeMethodState.isVoiceSelected,
                this.onTryRegisterResponse.bind(this));
        }
    }

    onTryRegisterResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const inputType = this.appState.loginType;
        const registerState = this.appState.registerForm;
        registerState.loading = false;

        if (inputType === 'phone') {
            this.appState.signUpActions.onSignUpByPhoneResponse(msg, act);
        } else if (inputType === 'email') {
            this.appState.signUpActions.onSignUpByEmailResponse(registerState.email!, msg, act);
        }
    }

    @action.bound
    showRegistrationSecondStep(event: React.MouseEvent<HTMLElement>) {
        event.preventDefault();
        const valid = this.validateRegisterFirstStepFields();
        if (valid) {
            this.appState.modal.showRegisterSecondStep();
        }
    }

    @action
    validateRegisterFirstStepFields(): boolean {
        const registerState = this.appState.registerForm;
        registerState.resetErrors();
        this.appState.loginForm.resetErrors();

        let countryCode = this.appState.getRef('registerCountryCode') ?
            (this.appState.getRef('registerCountryCode') as HTMLSelectElement).value : '';
        let phone = this.appState.getRef('registerPhone') ?
            (this.appState.getRef('registerPhone') as HTMLInputElement).value : '';
        // phone code
        const country = this.appState.site.getCountries()!.find((c: site.ICountryInfo) => c.code === countryCode);
        const phoneCode = country ? country.phoneCode!.toString() : '';
        // end region
        let email = this.appState.getRef('registerEmail') ?
            (this.appState.getRef('registerEmail') as HTMLInputElement).value.trim() : '';
        const password = this.appState.getRef('registerPassword') ?
            (this.appState.getRef('registerPassword')  as HTMLInputElement).value : '';
        const strCurrency = this.appState.getRef('registerCurrency') ?
            (this.appState.getRef('registerCurrency') as HTMLSelectElement).value : '';

        if (!phone && !email) {
            const phoneOrEmail = this.appState.getRef('registerPhoneOrEmail')
                ? (this.appState.getRef('registerPhoneOrEmail') as HTMLInputElement).value
                : '';
            if (phoneOrEmail.indexOf('@') > 0) {
                this.appState.loginType = 'email';
                email = phoneOrEmail;
            } else {
                this.appState.loginType = 'phone';
                phone = phoneOrEmail;
                const phoneNumber = parsePhoneNumberFromString(phoneOrEmail);
                if (phoneNumber && phoneNumber.country) {
                    countryCode = phoneNumber.country;
                }
            }
        }

        let valid = true;

        if (this.appState.loginType === 'phone' && (!countryCode || !phone)) {
            registerState.phoneError = this.appState.t('error-register-phone-required');
            valid = false;
        } else if (this.appState.loginType === 'email') {
            if (!email) {
                registerState.emailError = this.appState.t('error-register-email-required');
                valid = false;
            } else if (!isValidEmail(email)) {
                registerState.emailError = this.appState.t('error-invalid-email');
                valid = false;
            }
        }

        if (!password) {
            registerState.passwordError = this.appState.t('error-register-password-required');
            valid = false;
        }

        if (password && password.length < 6) {
            registerState.passwordError = this.appState.t('error-password-too-short');
            valid = false;
        }

        const currency = strCurrency ? Long.fromString(strCurrency) : this.appState.currencies.getDefaultCurrencyId();
        if (!currency) {
            registerState.currencyError = this.appState.t('error-register-currency-required');
            valid = false;
        }

        if (!this.appState.acceptTermsAndConditionsState.getValueOrSetError()) {
            registerState.error = this.appState.t('error-register-user-age-and-rule-approval-required');
            valid = false;
        }

        if (valid) {
            registerState.countryCode = countryCode;
            registerState.phone = phone;
            registerState.email = email;
            registerState.password = password;
            registerState.currency = currency;
            registerState.phoneCode = phoneCode;
        }

        return valid;
    }

    @action
    validateRegisterSecondStepFields(): boolean {
        const registerState = this.appState.registerForm;
        const userFields = this.appState.siteConfig.userFields!;
        let isValid = true;

        for (let field of userFields) {
            const name = field.name!;
            if (name === 'gender') {
                if (field.required && !registerState.userFields['gender']) {
                    registerState.setUserFieldError('gender', this.appState.t('error-gender-required'));
                    isValid = false;
                }
            } else {
                let ref = this.appState.getRef(name) as HTMLInputElement;
                let refValue = ref ? registerState.parseUserFieldValue(field, ref.value) : '';

                if (field.required) {
                    if (refValue) {
                        registerState.userFields[name] = refValue;
                    } else {
                        registerState.setUserFieldError(name, this.appState.t('error-' + name + '-required'));
                        isValid = false;
                    }
                } else {
                    registerState.userFields[name] = refValue!;
                }
            }
        }

        return isValid;
    }

    // endregion

    //  region ---- restore actions
    @action
    resetPassword(e?: React.MouseEvent<HTMLElement>) {
        this.appState.modal.showRestore(e);
    }

    @action
    restoreBack(e?: React.MouseEvent<HTMLElement>) {
        this.appState.modal.showLogin(e);
    }

    @action
    tryResetPassword(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        let countryCode = this.appState.getRef('restoreCountryCode') ?
            (this.appState.getRef('restoreCountryCode') as HTMLSelectElement).value : '';
        let phone = this.appState.getRef('restorePhone') ?
            (this.appState.getRef('restorePhone') as HTMLSelectElement).value : '';
        let email = this.appState.getRef('restoreEmail') ?
            (this.appState.getRef('restoreEmail') as HTMLSelectElement).value : '';

        let inputType = this.appState.loginType;

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        this.appState.restoreForm.reset();
        let restoreState = this.appState.restoreForm;
        let valid = true;

        if (inputType === 'phone' && (!countryCode || !phone)) {
            restoreState.phoneError = this.appState.t('error-reset-password-phone-required');
            valid = false;
        } else if (inputType === 'email' && !email) {
            restoreState.emailError = this.appState.t('error-reset-password-email-required');
            valid = false;
        }

        if (valid) {
            restoreState.loading = true;
            this.appState.api.restorePasswordRequest(email, phone, countryCode, (msg, act) => {
                this.appState.processApi(msg, act);
                if (!act.restorePassword!.ok) {
                    this.appState.restoreForm.loading = false;
                    this.appState.restoreForm.error = this.appState.t('error-reset-password-failed');
                } else {
                    this.appState.restoreForm.success = true;
                    this.appState.restoreForm.loading = false;
                    this.appState.restoreForm.country = countryCode;
                    this.appState.restoreForm.phone = phone;
                }
            });

        }
    }

    @action
    changeRestoreFormCode(event: ChangeEvent<HTMLInputElement>) {
        this.appState.restoreForm.code = event.target.value;
    }

    @action
    changeRestoreFormNewPassword(event: ChangeEvent<HTMLInputElement>) {
        this.appState.restoreForm.newPassword = event.target.value;
    }

    @action
    closeRestoreDialog(event: React.MouseEvent<HTMLElement>) {
        this.appState.resetForms();
        this.appState.modal.hideModal(event);
        this.appState.router.push(this.appState.l('/'));
    }

    // endregion

    // region ---- confirmation actions
    @action
    confirmationDialog(event: React.MouseEvent<HTMLSelectElement>) {
        event.stopPropagation();
        this.appState.modal.showAuthorizedConfirmation(event);
        this.appState.confirmForm.reset();
    }

    @action
    changeConfirmEmailCode(event: ChangeEvent<HTMLInputElement>) {
        this.appState.confirmEmailForm.code = event.target.value;
    }

    @action
    tryConfirmPhoneRegistration(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        let code = this.appState.getRef('confirmCode') ?
            (this.appState.getRef('confirmCode') as HTMLInputElement).value : '';

        this.appState.confirmForm.reset();
        let confirmForm = this.appState.confirmForm;
        let valid = true;

        if (!code) {
            confirmForm.codeError = this.appState.t('error-register-confirm-code-required');
            valid = false;
        }

        if (valid) {
            confirmForm.loading = true;

            this.appState.api.confirmPhone(code, (msg, act) => {
                this.appState.processApi(msg, act);
                confirmForm.loading = false;
                if (!act.confirmPhone!.ok) {
                    confirmForm.error = this.appState.t(getPhoneConfirmationError(act.confirmPhone!.error));
                } else {
                    this.appState.confirmForm.reset();
                    this.appState.modal.hideModal();
                    this.appState.router.push(this.appState.l('/'));
                }
            });
        }
    }

    @action
    tryResendPhoneConfirmation(phone?: string): ReactEventHandler<HTMLElement> {
        return (event) => {
            event.preventDefault();

            if ((event.target as HTMLInputElement).disabled) {
                return;
            }

            this.appState.confirmForm.reset();
            let confirmForm = this.appState.confirmForm;

            confirmForm.sending = true;
            confirmForm.canResend = false;

            this.appState.api.resendPhoneConfirmation((msg, act) => {
                let success = act.resendPhoneConfirmation!.ok;
                confirmForm.sending = false;

                if (success) {
                    confirmForm.canResend = true;
                    confirmForm.activateResendTimer();

                    if (phone) {
                        this.appState.notificator!.addNotification({
                            message: this.appState.t('ui-phone-confirmation-resend-successful', {phone: phone}),
                            level: NotificationItemLevel.SUCCESS
                        });
                    }
                } else {
                    this.appState.notificator!.addNotification({
                        message: this.appState.t('ui-phone-confirmation-resend-failed'),
                        level: NotificationItemLevel.ERROR
                    });
                }
            });
        };
    }

    @action
    tryResendEmailConfirmation(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        this.appState.confirmEmailSentForm.reset();
        let form = this.appState.confirmEmailSentForm;

        form.sending = true;

        this.appState.api.resendEmailConfirmation((msg, act) => {
            form.sending = false;

            let success = act.resendEmailConfirmation!.ok;
            let error = null;
            if (!success) {
                switch (act.resendEmailConfirmation!.error) {
                    case users.ResendEmailConfirmationResponse.ResendEmailConfirmationError.unauthenticated:
                        error = this.appState.t('error-authorization-required');
                        break;
                    case users.ResendEmailConfirmationResponse.ResendEmailConfirmationError.already_confirmed:
                        error = this.appState.t('error-email-already-confirmed');
                        break;
                    default:
                        // TODO: use right error here
                        error = this.appState.t('error-register-unknown-error');
                        break;
                }
            }
            form.sending = false;
            form.sent = true;
            form.error = error;
        });
    }

    @action
    tryConfirmEmail(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        let email = this.appState.confirmEmailForm.email!;
        let code = this.appState.confirmEmailForm.code!;

        this.appState.confirmEmailForm.reset();
        let form = this.appState.confirmEmailForm;
        let valid = true;

        if (!email) {
            form.emailError = this.appState.t('error-email-confirmation-email-required');
            valid = false;
        }

        if (!code) {
            form.codeError = this.appState.t('error-email-confirmation-code-required');
            valid = false;
        }

        if (valid) {
            form.loading = true;
            const userInfo = this.appState.rawUserInfo!;
            if (userInfo.newEmail) {
                this.appState.api.confirmNewEmail(email, code, this.onTryConfirmEmail.bind(this));
            } else {
                this.appState.api.confirmEmail(email, code, this.onTryConfirmEmail.bind(this));
            }
        }
    }

    @action
    onTryConfirmEmail(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        const actionData = act.confirmEmail || act.confirmNewEmail;
        const form = this.appState.confirmEmailForm;
        form.loading = false;

        if (actionData && actionData.ok) {
            this.appState.confirmEmailForm.reset();
            this.appState.modal.hideModal();
            this.appState.router.push(this.appState.l('/'));
        } else {
            form.error = this.appState.t('error-email-confirmation-failed');
        }
    }

    @action
    tryConfirmRestoreByPhone(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }
        let restoreForm = this.appState.restoreForm;

        let code = restoreForm.code!;
        let newPassword = restoreForm.newPassword!;
        let valid = true;

        let {phone, country} = restoreForm;

        restoreForm.resetErrors();
        this.appState.restoreForm.success = true;

        if (!code) {
            restoreForm.codeError = this.appState.t('error-restore-code-required');
            valid = false;
        }

        if (!newPassword || newPassword.length < 6) {
            restoreForm.passwordError = this.appState.t('error-restore-new-password-too-short');
            valid = false;
        }

        if (valid) {
            restoreForm.loading = true;

            this.appState.api.restorePassword('', phone!, country!, code, newPassword, (msg, act) => {
                this.appState.processApi(msg, act);

                let success = act.restorePassword!.ok!;
                let error = '';
                if (!success) {
                    switch (act.restorePassword!.error) {
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_code:
                            error = this.appState.t('error-register-confirm-code-invalid');
                            break;
                        case users.RestorePasswordResponse.RestorePasswordError.user_not_found:
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_email:
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_phone:
                            error = this.appState.t('error-restore-failed');
                            break;
                        case users.RestorePasswordResponse.RestorePasswordError.password_too_short:
                            error = this.appState.t('error-restore-new-password-too-short');
                            break;
                        default:
                            error = this.appState.t('error-register-unknown-error');
                            break;
                    }
                }
                restoreForm.loading = false;
                restoreForm.restored = success;
                restoreForm.error = error;
            });
        }
    }

    @action
    tryConfirmRestorePassword(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        if ((event.target as HTMLInputElement).disabled) {
            return;
        }

        let {code, email} = this.appState.restoreEmailForm;

        let password = this.appState.getRef('newPassword') ?
            (this.appState.getRef('newPassword') as HTMLInputElement).value : '';

        let verifyPassword = this.appState.getRef('verifyNewPassword') ?
            (this.appState.getRef('verifyNewPassword') as HTMLInputElement).value : '';

        let valid = true;

        this.appState.confirmRestorePasswordByMailForm.reset();
        let restorePassword = this.appState.confirmRestorePasswordByMailForm;

        if (!password || password.length < 6) {
            restorePassword.passwordError = this.appState.t('error-restore-new-password-too-short');
            valid = false;
        } else if (password !== verifyPassword) {
            restorePassword.passwordError = this.appState.t('error-restore-new-passwords-not-match');
            valid = false;
        }

        if (valid) {
            restorePassword.loading = true;

            this.appState.api.restorePassword(email!, '', '', code!, password, (msg, act) => {
                this.appState.processApi(msg, act);

                let success = act.restorePassword!.ok;
                let error = null;
                if (!success) {
                    switch (act.restorePassword!.error) {
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_code:
                        case users.RestorePasswordResponse.RestorePasswordError.user_not_found:
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_email:
                        case users.RestorePasswordResponse.RestorePasswordError.invalid_phone:
                            error = this.appState.t('error-restore-failed');
                            break;
                        case users.RestorePasswordResponse.RestorePasswordError.password_too_short:
                            error = this.appState.t('error-restore-new-password-too-short');
                            break;
                        default:
                            error = this.appState.t('error-register-unknown-error');
                            break;
                    }
                }
                restorePassword.loading = false;
                restorePassword.success = success!;
                restorePassword.error = error;
            });
        }
    }

    // endregion

    // region ---- utils
    getFirstRealBalance(): UserBalance | undefined {
        return (this.appState.userInfo.balances || []).find(b => b.currencyId!.toInt() > 0);
    }

    hasRealBalance() {
        return !!this.getFirstRealBalance();
    }

    getRealBalanceCode() {
        const balance = this.getFirstRealBalance();
        if (balance) {
            return formatCurrencyCode(balance.currencyId);
        }
        return null;
    }

    getRealBalanceCurrency() {
        const balance = this.getFirstRealBalance();
        if (balance) {
            return currencies.findCurrencyById(balance.currencyId);
        }
        return null;
    }

    getRealBalanceCurrencyId(): Long | null {
        const balance = this.getFirstRealBalance();
        if (balance) {
            return balance.currencyId;
        }
        return null;
    }

    getRealBalanceId(): (Long | null) {
        const balance = this.getFirstRealBalance();
        if (balance) {
            return balance!.id;
        }
        return null;
    }

    getActiveBalance(): UserBalance | undefined {
        return (this.appState.userInfo.balances || []).find(b => b.active);
    }

    getActiveBalanceCurrencyId(): Long | null {
        const balance = this.getActiveBalance();
        if (balance) {
            return balance.currencyId;
        }
        return null;
    }

    getActiveBalanceCode() {
        const balance = this.getActiveBalance();
        if (balance) {
            return formatCurrencyCode(balance.currencyId);
        }
        return null;
    }

    preventPasswordInput(e: React.KeyboardEvent<HTMLInputElement>) {
        const notAllowedKeys = [
            32 // space
        ];

        if (notAllowedKeys.indexOf(e.keyCode) !== -1 && !e.metaKey && !e.ctrlKey) {
            e.preventDefault();
            e.stopPropagation();
        }
    }

    preventTextInput(e: React.KeyboardEvent<HTMLInputElement>) {
        const notAllowedKey = allowedNumberKeys.indexOf(e.keyCode) === -1;

        if (notAllowedKey && !e.metaKey && !e.ctrlKey) {
            e.preventDefault();
            e.stopPropagation();
        }
    }
    // endregion

    processContactList = () => {
        if (window.AndroidWebView?.postMessage) {
            const contactListJSON = window.AndroidWebView.postMessage(
                JSON.stringify({
                    type: AndroidWebViewMessageType.CONTACT_LIST,
                }),
            );
            const encodedJson = new TextEncoder().encode(contactListJSON);
            this.appState.api.dumpContacts(encodedJson, (msg, act) => {});
        }
    }
}
