import AppState from '../AppState';
import {action, computed, observable, observe} from 'mobx';
import {formatCurrencyAmount} from '../utils/Money';
import Long from 'long';
import {BonusForPage, BonusType, IPossibleBonusCode, /*Page,*/ PlayState, SitePages, SkillWheelState} from '../meta';
import {processLong, processMetaList, processNumber, processString} from '../utils/Common';
import {bonuses, messages} from '../../api/proto';
import {BonusMetaField} from './BonusAppearance';
import {getTranslateEl, isMobile} from '../utils/BrowserUtils';
import {NotificationItemLevel} from '../components/Notificator';

const DEFAULT_ANIMATION_DURATION_FOR_ONE_ITEM = 2000; // ms

export default class SkillWheelActions {
    @observable show: boolean = false;
    isShowed: boolean = false;

    // region ---- bonus state
    @observable spinsRemaining: number = 0;
    @observable prizeWon: Long = Long.fromNumber(0);
    @observable wheelItems: number[] = [];
    @observable wheelPlayState: PlayState;
    @observable isSkillWheelDisabled = false;
    timeForSpin: number = 0; // in ms
    // ---- endregion

    balance: Long = Long.fromNumber(0);
    time: number = 0; // in ms
    timerIdForClock: NodeJS.Timer | null;
    timerIdForHighlightActiveItem: NodeJS.Timer | null;
    timerIdForAnimateWheel: NodeJS.Timer | null;
    prevActiveItemIndex: number | null;
    highlightedWheelItem: HTMLElement | null;

    constructor(private appState: AppState) {
        observe(this, 'wheelPlayState', (change) => {
            switch (change.newValue) {
                case PlayState.PLAY:
                    setTimeout(() => this.playWheel(), 1000);
                    break;
                case PlayState.STOP:
                    this.stopWheel();
                    break;
                case PlayState.PAUSE:
                    this.pauseWheel();
                    break;
                default:
                    break;
            }
        });

        observe(this, 'show', (change) => {
            if (change.newValue) {
                if (appState.router) {
                    appState.router.push(this.appState.l('/skill-wheel'));
                }
            }
        });
    }

    @computed
    get isEnabled(): boolean {
        const appState = this.appState;
        const isBalanceOk = appState.userInfo.realBalance && appState.userInfo.realBalance.availableValue?.greaterThan(0);
        return !!(appState.appSettings.skillWheel.enabled && isBalanceOk && !this.isSkillWheelDisabled);
    }

    tryPlayWheel = () => {
        if (this.wheelPlayState === PlayState.PLAY) {
            this.playWheel();
        }
    }

    // region ---- play state cycles
    playWheel = () => {
        const skillWheelEl = this.appState.getRef('skill-wheel');
        if (skillWheelEl) {
            // update timer for attempt
            this.updateTimer();
            // first highlight active item
            this.appState.skillWheel.highlightActiveItem();
            // start highlight active item
            if (!this.timerIdForHighlightActiveItem) {
                this.timerIdForHighlightActiveItem = setInterval(this.appState.skillWheel.highlightActiveItem, 50);
            }
            // start wheel
            if (!this.timerIdForAnimateWheel) {
                const interval = this.getIntervalForAnimateWheel();
                if (interval) {
                    this.timerIdForAnimateWheel = setInterval(this.animateWheel, interval);
                }
            }
        }
    }

    getIntervalForAnimateWheel() {
        const skillWheelEl = this.appState.getRef('skill-wheel');
        if (skillWheelEl) {
            const childrenEl = skillWheelEl!.children[0] as HTMLElement;
            if (childrenEl) {
                const animationDurationForOneItem = this.appState.appSettings.skillWheel.animationDurationForOneItem || DEFAULT_ANIMATION_DURATION_FOR_ONE_ITEM;
                return animationDurationForOneItem / childrenEl.offsetHeight;
            }
        }
        return null;
    }

    pauseWheel = () => {
        this.resetWheelTimers();
        this.scrollToHighlightedItem();
    }

    stopWheel = () => {
        this.resetTimerId();
        this.resetWheelTimers();
        this.appState.user.logout(undefined, true, true);
    }
    // ---- endregion

    // region ---- process skill-wheel state
    @action
    fetchState = (page: string = BonusForPage.SKILLWHEEL) => {
        this.wheelPlayState = PlayState.PAUSE;
        this.appState.userBonusesStore.fetchBonuses(this.onFetchSkillWheelState.bind(this), page);
    }

    @action
    onFetchSkillWheelState = (codes: bonuses.IBonusCode[], possibleCodes: IPossibleBonusCode[]) => {
        this.isShowed = this.show;
        for (let code of possibleCodes) {
            const bonusType = processString(BonusMetaField.BONUS_TYPE, code.meta, null);
            if (this.appState.page === SitePages.SKILL_WHEEL && code.id === 'skillwheel') {
                if (!code.meta || Object.keys(code.meta).length === 0) {
                    window.location.href = '/';
                }
            }
            if (bonusType === BonusType.SKILL_WHEEL) {
                const showSkillWheel = processString(BonusMetaField.SKILL_WHEEL_STATE, code.meta, null) !== SkillWheelState.ENDED;
                const isSkillWheelDisabled = processString(BonusMetaField.SKILL_WHEEL_DISABLED, code.meta, "false") === "true";
                // hack, reload page if skill-wheel settings have been received. Game page breaks react in mobile
                const forceReload = showSkillWheel && !isSkillWheelDisabled && isMobile() && this.appState.page === SitePages.GAME;
                if (forceReload) {
                    this.resetTimerId();
                    this.resetWheelTimers();
                    window.location.reload();
                    return;
                }


                this.processData(code);
                break;
            }
        }
        //this.appState.loading = false;
        // hide skillWheel after timeout if last attempt/auth/H2d6zD
        if (this.isShowed && !this.show) {
            this.show = true;
            // add timeout to show the values ​​after the last attempt before close skill wheel
            setTimeout(() => {
                this.resetState();
                this.wheelPlayState = PlayState.STOP;
            }, 1000);
        } else {
            this.appState.loading = false;
        }
    }

    @action
    processData = (skillWheelBonus: IPossibleBonusCode) => {
        const bonusMeta = skillWheelBonus.meta!;
        const userBalance = this.appState.userInfo.realBalance?.availableValue ? this.appState.userInfo.realBalance?.availableValue?.toNumber() : 0;
        const spinsRemaining = processNumber(BonusMetaField.SKILL_WHEEL_SPINS_REMAINING, bonusMeta)!;
        const timeForSpin = processNumber(BonusMetaField.SKILL_WHEEL_TIME_FOR_SPIN, bonusMeta)!;
        const prizeWon = processLong(BonusMetaField.SKILL_WHEEL_PRIZE_WON_AMOUNT, bonusMeta)!;
        const wheelItems = processMetaList(bonusMeta[BonusMetaField.SKILL_WHEEL_VALUES]);
        const skillWheelState = processString(BonusMetaField.SKILL_WHEEL_STATE, skillWheelBonus.meta, null);
        const isSkillWheelDisabled = processString(BonusMetaField.SKILL_WHEEL_DISABLED, skillWheelBonus.meta, "false") === "true";
        // set state
        this.isSkillWheelDisabled = isSkillWheelDisabled;
        this.show = skillWheelState !== SkillWheelState.ENDED && !isSkillWheelDisabled;
        this.timeForSpin = timeForSpin;
        this.time = this.spinsRemaining !== spinsRemaining ? timeForSpin : this.time;
        this.spinsRemaining = spinsRemaining;
        this.prizeWon = prizeWon;
        this.wheelItems = this.spinsRemaining ? wheelItems.concat(wheelItems) : this.wheelItems;
        this.balance = this.spinsRemaining ? Long.fromNumber(userBalance - Number(prizeWon)) : Long.fromNumber(0);
        this.wheelPlayState = skillWheelState !== SkillWheelState.ENDED ? PlayState.PLAY : this.wheelPlayState;
        //this.wheelPlayState = skillWheelState !== SkillWheelState.ENDED ? PlayState.PLAY : skillWheelState === SkillWheelState.ENDED ? PlayState.STOP : this.wheelPlayState;
    }

    @action
    resetState = () => {
        this.show = false;
        this.isShowed = false;
        this.prizeWon = Long.fromNumber(0);
        this.balance = Long.fromNumber(0);
        this.spinsRemaining = 0;
        this.time = 0;
        this.timeForSpin = 0;
        this.wheelItems = [];
    }

    // ---- endregion

    // region ---- start skill-wheel actions
    @action
    startSkillWheel = () => {
        if (!this.appState.loading) {
            this.appState.api.activatePromoCode(`${BonusForPage.SKILLWHEEL}:start`, this.onActivateStartSkillWheel.bind(this));
        }
        this.appState.loading = true;
    }

    @action
    onActivateStartSkillWheel = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        const result = act.bonusAction ? act.bonusAction.activateCode!.result : null;
        // if start skill-wheel initialized
        if (result === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
            this.fetchState();
        } else {
            const errorToken = this.getSkillWheelActivationErrorToken(result);
            const errorMessage = this.appState.t(errorToken);
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.ERROR,
                message: errorMessage,
            });
        }
    }
    // ---- endregion

    // region ---- select skill-wheel item actions
    @action
    selectWheelItem = () => {
        if (this.wheelPlayState === PlayState.PLAY && this.highlightedWheelItem) {
            this.wheelPlayState = PlayState.PAUSE;
            const value = parseInt(this.highlightedWheelItem!.getAttribute('data-prize-won')!, 10);
            this.appState.api.activatePromoCode(`${BonusForPage.SKILLWHEEL}:${value}`, this.onSelectWheelItem);
        }
    }

    onSelectWheelItem = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        //const appState = this.appState;
        const result = act.bonusAction ? act.bonusAction.activateCode!.result : null;

        if (result !== bonuses.BonusCodeActivateResponse.Result.SUCCESS && result !== bonuses.BonusCodeActivateResponse.Result.CANT_BE_ACTIVATED_AT_THE_MOMENT) {
            const errorToken = this.getSkillWheelActivationErrorToken(result);
            const errorMessage = this.appState.t(errorToken);
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.ERROR,
                message: errorMessage,
            });
        }
        this.fetchState();
        // fetch actual state in SSE, processUpdatedBonusCodes
        // only local fetch, because SSE not working local
        if (this.appState.isLocalhost) {
          //  this.fetchState();
        }
    }
    // ---- endregion

    // region ---- UI skill-wheel utils
    animateWheel = () => {
        const skillWheelEl = this.appState.getRef('skill-wheel');
        if (skillWheelEl) {
            const middleLine = Math.ceil(skillWheelEl.offsetHeight / 2);
            const translateY = getTranslateEl(skillWheelEl)!.translateY! || 0;

            if (translateY <= middleLine) {
                skillWheelEl.style.transform = `translateY(${translateY + 1}px)`;
            } else {
                skillWheelEl.style.transform = `translateY(0px)`;
            }
        }
    }

    highlightActiveItem = () => {
        const skillWheelEl = this.appState.getRef('skill-wheel')!;
        const skillWheelContainerEl = this.appState.getRef('skill-wheel-container')!;

        if (skillWheelEl && skillWheelContainerEl && this.wheelPlayState === PlayState.PLAY) {
            const childrenEl = skillWheelEl.children[0] as HTMLElement;
            if (childrenEl) {
                const translateY = getTranslateEl(skillWheelEl!)!.translateY! || 0;
                const activeItemIndex = skillWheelEl.childElementCount / 2 - Math.round((translateY - (skillWheelContainerEl.offsetHeight / 2) + (childrenEl.offsetHeight / 2)) / childrenEl.offsetHeight);
                const activeItem = skillWheelEl.children[activeItemIndex] as HTMLElement;
                const duplicateActiveItemIndex = activeItemIndex < skillWheelEl.childElementCount / 2 ? activeItemIndex + skillWheelEl.childElementCount / 2 : activeItemIndex - skillWheelEl.childElementCount / 2;
                const duplicateActiveItem = skillWheelEl.children[duplicateActiveItemIndex] as HTMLElement;

                if (activeItemIndex !== this.prevActiveItemIndex) {
                    this.prevActiveItemIndex = activeItemIndex;

                    [].forEach.call(skillWheelEl.children, function (item: HTMLElement) {
                        item.classList.remove('wheel-item--active');
                    });

                    activeItem.classList.add('wheel-item--active');
                    duplicateActiveItem.classList.add('wheel-item--active');

                    this.highlightedWheelItem = activeItem;
                }
            }
        }
    }

    scrollToHighlightedItem = () => {
        const skillWheelEl = this.appState.getRef('skill-wheel');
        const skillWheelContainerEl = this.appState.getRef('skill-wheel-container');

        if (skillWheelEl && skillWheelContainerEl && this.highlightedWheelItem) {
            const containerBounding = skillWheelContainerEl.getBoundingClientRect();
            const middleLine = containerBounding.top + containerBounding.height / 2;
            const itemBounding = this.highlightedWheelItem!.getBoundingClientRect();
            const itemMiddleLine = itemBounding.top + itemBounding.height / 2;
            const multiplier = Math.round(skillWheelContainerEl.offsetHeight / containerBounding.height * 100) / 100;
            const offset = Math.round((middleLine - itemMiddleLine) * multiplier);
            const translateY = getTranslateEl(skillWheelEl)!.translateY || 0;
            skillWheelEl.style.transform = `translateY(${translateY + offset}px)`;
        }
    }

    resetWheelTimers = () => {
        clearInterval(this.timerIdForHighlightActiveItem!);
        this.timerIdForHighlightActiveItem = null;

        clearInterval(this.timerIdForAnimateWheel!);
        this.timerIdForAnimateWheel = null;
    }

    // ---- endregion

    // region ---- skill-wheel prizes
    @computed
    get prizeWonWithCurrency(): string {
        return formatCurrencyAmount(this.prizeWon, this.appState.userInfo.realBalance?.currencyId!);
    }

    // ---- endregion

    // region ---- wheel timer
    resetTimerId() {
        clearInterval(this.timerIdForClock!);
        this.timerIdForClock = null;
    }

    @action
    updateTimer = () => {
        if (!this.timerIdForClock) {
            this.timerIdForClock = setInterval(this.countdown.bind(this), 1000);
        }
        this.updateTimerInDom();
    }

    @action
    countdown = () => {
        if (this.wheelPlayState !== PlayState.PAUSE) {
            if (this.time === 0) {
                this.selectWheelItem();
            } else {
                this.time = this.time - 1000; // ms
                this.updateTimerInDom();
            }
        }
    }

    updateTimerInDom = () => {
        const dateEl = document.getElementById('skill-wheel-timer');
        if (dateEl) {
            dateEl.textContent = this.appState.t('ui-time-remaining', {time: (this.time / 1000)!.toString()});
        }
    }

    // ---- endregion

    // region --- errors

    getSkillWheelActivationErrorToken(error?: bonuses.BonusCodeActivateResponse.Result | undefined | null) {
        return 'error-skill-wheel-promo-code-cant-be-activated-at-the-moment';
    }

    // endregion
}
