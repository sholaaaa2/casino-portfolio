import {bonuses, commons} from "../../api/proto";
//import {processParsedMetaList} from "../../api/utils";
import AppState from "../AppState";
import Long from 'long';
import {formatCurrencyAmount} from "../utils/Money";
//import {formatDateTime} from '../utils/Datetime';
import { format } from 'date-fns';

interface Entry {
    minRefill?: number;
    maxRefill?: number;
    bonusPercentFromRefill?: number;
    bonusWager?: number;
    code?: string;
    currentBalance?: number;
    dynamicValues?: DynamicValues;
    freeGame?: string;
    freeGameBetPerSpin?: number;
    freeGameCount?: number;
    freeGameCurrency?: number;
}

interface DynamicValues {
    available?: boolean;
    lastRefill?: number;
    activationExpirationStamp?: number;
    bonusAmount?: number;
    balanceAfterBonusAccept?: number;
    minClosingAmount?: number;
    maxClosingAmount?: number;
    unavailabilityReasons?: UnavailabilityReasons;
    wagerAmount?: number;
}

interface UnavailabilityReasons {
    refillIsRequired?: boolean;
    bonusAlreadyActivated?: boolean;
    timeForActivationHasExpired?: boolean;
    refillAmountIsOutOfBoundaries?: boolean;
}

export class BalanceWagerBonusProcessor {
    constructor(private appState: AppState) {
    }

    process(
        bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode | bonuses.IBonusCode
    ): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        const originMeta = bonus.meta;
        if (originMeta?.entries) {
            const entries = this.parseEntries(originMeta.entries);
            if (entries?.length) {
                const processedEntries = [];
                for (const entry of entries) {
                    const processedEntry = this.processEntry(bonus, entry, originMeta);
                    processedEntries.push(processedEntry);
                }
                return processedEntries;
            }
        }
        if (originMeta?.active_entry) {
            const entry = JSON.parse(originMeta.active_entry);
            if (entry) {
                const processedEntry = this.processEntry(bonus, entry, originMeta);
                return [processedEntry];
            }
        }
        return [];
    }

    processEntry(
        bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode | bonuses.IBonusCode,
        entry: Entry,
        originMeta: { [k: string]: string }
    ): bonuses.UserBonusesListResponse.IPossibleBonusCode {
        return {
            ...bonus,
            id: entry.code,
            meta: this.getCaseMeta(originMeta, entry)
        };
    }


    private parseEntries(entries: string): Entry[] | null {
        try {
            return JSON.parse(entries)
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.error("Wager Bonus: Wager bonus rules are invalid JSON");
                console.error(e);
            } else {
                throw e;
            }
            return null;
        }
    }

    private getCaseMeta(originMeta: { [k: string]: string }, entry: Entry) {
        const dateTime = Number(entry.dynamicValues?.activationExpirationStamp) ? new Date(Number(entry.dynamicValues?.activationExpirationStamp)) : null;
        const currency = this.appState.userInfo.guest ? new Long(Number(originMeta.entries_data_currency || 0)) : this.appState.user.getRealBalanceCurrency();
        let freeGameBetPerSpinFormatted = undefined;
        if (entry.freeGameBetPerSpin && entry.freeGameCurrency) {
            freeGameBetPerSpinFormatted = this.formatAmount(entry.freeGameBetPerSpin, new Long(entry.freeGameCurrency));
        }
        let caseMeta: any = {
            ...originMeta,
            ...entry,
            ...entry.dynamicValues,
            currentBalanceFormatted: this.formatAmount(entry.currentBalance, currency),
            lastRefillFormatted: this.formatAmount(entry.dynamicValues?.lastRefill, currency),
            balanceAfterBonusAcceptFormatted: this.formatAmount(entry.dynamicValues?.balanceAfterBonusAccept, currency),
            maxClosingAmountFormatted: this.formatAmount(entry.dynamicValues?.maxClosingAmount, currency),
            minRefillFormatted: this.formatAmount(entry.minRefill, currency),
            maxRefillFormatted: this.formatAmount(entry.maxRefill, currency),
            wagerAmountFormatted: this.formatAmount(entry.dynamicValues?.wagerAmount, currency),
            timeToLeftActivation: dateTime
                ? format(dateTime, 'dd.LL.yyyy') + '\n' + format(dateTime, 'HH:mm:ss')
                : null,
            freeGameBetPerSpinFormatted,

        };
        caseMeta.descriptionFormatted = this.processDescription(caseMeta.entry_description, caseMeta, currency);
        caseMeta.notification = this.processDescription(caseMeta.notification, caseMeta, currency);

        delete caseMeta.entries;
        return caseMeta;
    }

    processDescription = (description?: string, caseMeta?: any, currency?: Long | commons.ICurrency | null) => {
        if (description) {
            return description.replace(/{[A-z]*}/gi, (match: string) => {
                const key = match.replace('{','').replace('}','');
                if (caseMeta[key] !== undefined) {
                    if (key === 'bonusPercentFromRefill' || key === 'bonusWager' || key === 'freeGameCount') {
                        return caseMeta[key];
                    }
                    if (key === 'freeGame') {
                        const gameName = this.appState.games.getGameName(caseMeta[key]);
                        if (gameName) {
                            return gameName;
                        }
                    }
                    return this.formatAmount(caseMeta[key], currency);
                }
                return match;
            });
        }
        return '';
    }
    freeGame?: string;
    freeGameBetPerSpin?: number;
    freeGameCount?: number;
    freeGameCurrency?: number;

    private formatAmount(value?: number, currency?: Long | commons.ICurrency | null): string {
        if (value && value !== 0) {
            return formatCurrencyAmount(
                Long.fromNumber(value),
                currency ? currency : this.appState.user.getRealBalanceCurrency()
            );
        }
        return '—';
    }

    getIssueNotificationTitle(meta: { [k: string]: string }): string | null {
        if (meta && meta?.bonus_title)
            return meta?.bonus_title;
        return null;
    }

    getIssueNotification(bonus: bonuses.IBonusCode, meta: { [k: string]: string }): string | null {
      if (meta && meta?.notification && meta.active_entry) {
          const entry = JSON.parse(meta.active_entry);

          if (entry) {
              const processedEntry = this.processEntry(bonus, entry, meta);
              return processedEntry?.meta?.notification ? processedEntry?.meta?.notification : meta.notification;
          }

      }
      return meta?.notification ? meta?.notification : '';
    }

}
