import AppState from '../AppState';
import * as React from 'react';

export default class NumPad {
    private appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;

        this.numPadCancel = this.numPadCancel.bind(this);
        this.numPadBackspace = this.numPadBackspace.bind(this);
        this.numPadTriggerKey = this.numPadTriggerKey.bind(this);
    }

    resetInputError() {
        const input = document.querySelector('input:focus');
        const inputLogin = this.appState.getRef('generatedLogin');
        const inputPassword = this.appState.getRef('loginPassword');

        if (input === inputLogin) {
            this.appState.loginForm.generatedLoginError = null;
        } else if (input === inputPassword) {
            this.appState.loginForm.passwordError = null;
        }
    }

    numPadTriggerKey(event: React.SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        this.resetInputError();

        const key = event.currentTarget.getAttribute('data-key')!;
        const focusedInput = this.numPadGetFocusedInput();
        this.numPadInsertText(focusedInput, key);
    }

    numPadInsertText(input: HTMLInputElement, text: string) {
        if (input === null) {
            return;
        }

        const maxLength = parseInt(input.getAttribute('maxlength')!, 10);
        if (!isNaN(maxLength) && input.value.length + 1 > maxLength) {
            return;
        }

        const selectionStart = input.selectionStart!;
        const selectionEnd = input.selectionEnd!;
        const front = (input.value).substring(0, selectionStart);
        const back = (input.value).substring(selectionEnd, input.value.length);
        const scrollPos = input.scrollTop;

        input.value = front + text + back;
        input.selectionStart = selectionStart + text.length;
        input.selectionEnd = selectionStart + text.length;
        input.scrollTop = scrollPos;

        setTimeout(() => input.focus(), 0);
    }

    numPadBackspace(event: React.SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        this.resetInputError();

        const input = this.numPadGetFocusedInput();
        if (input === null) {
            return;
        }
        const selectionStart = input.selectionStart!;
        const selectionEnd = input.selectionEnd!;
        const value = input.value;

        if (selectionStart !== 0 || (selectionStart === 0 && selectionEnd !== 0)) {
            if (selectionStart === selectionEnd) {
                input.value = value.substr(0, selectionStart - 1) + value.substr(selectionStart);
                input.selectionStart = selectionStart - 1;
                input.selectionEnd = selectionStart - 1;
            } else {
                input.value = value.substr(0, selectionStart) + value.substr(selectionEnd);
                input.selectionStart = selectionStart;
                input.selectionEnd = selectionStart;
            }

            input.scrollTop = selectionStart;
            setTimeout(() => input.focus(), 0);
        }
    }

    numPadCancel(event: React.SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        this.resetInputError();

        const input = this.numPadGetFocusedInput();
        if (input === null) {
            return;
        }
        input.value = '';

        setTimeout(() => input.focus(), 0);
    }

    numPadGetFocusedInput(): HTMLInputElement {
        return document.querySelector('input:focus') as HTMLInputElement;
    }
}