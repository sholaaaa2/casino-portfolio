import {action, computed, observable} from 'mobx';
import {jpmonitor, site} from '../../api/proto';
import {replaceZeros} from '../utils/Money';
import AppState from '../AppState';
import {AMOUNT_OF_ITEMS_ON_MAIN_PAGE} from './LastWinsActions';
import {SitePages} from '../meta';
import {JackpotState} from './jackpots/JackpotState';
import {JackpotWinState} from './jackpots/JackpotWinState';

const JACKPOT_IMG_PATH_MOBILE = '/images/jackpots/mobile';
const JACKPOT_IMG_PATH_MOBILE_BLINKING = '/images/jackpots/mobile/blinking';
const JACKPOT_IMG_PATH_MOBILE_NEW = '/images/jackpots/new_jackpot_banner';
const JACKPOT_IMG_PATH_MOBILE_NEW_BLINKING = '/images/jackpots/new_jackpot_banner/blinking';
const JACKPOT_IMG_PATH = '/images/jackpots/desktop';
const JACKPOT_IMG_PATH_BLINKING = '/images/jackpots/desktop/blinking';
const JACKPOT_IMG_PATH_NOBANNERS = '/images/jackpots/desktop-no-banner';
const JACKPOT_IMG_PATH_NOBANNERS_BLINKING = '/images/jackpots/desktop-no-banner/blinking';
const JACKPOT_TITLE_IMG_PATH = '/images/jackpots/titles';
const WIN_NOTIFICATION_INTERVAL = 3000;

export class JPMonitorActions {
    allJackpots = observable<JackpotState>([]);
    winners = observable<JackpotWinState>([]);

    constructor(private appState: AppState) {}

    @computed
    get winnersOptimized() {
        return this.appState.page === SitePages.MAIN
            ? this.winners.slice(0, AMOUNT_OF_ITEMS_ON_MAIN_PAGE)
            : this.winners;
    }

    @computed
    get titleImgLink(): string | null {
        let theme = 'idle';
        if (this.theme === 'win' || this.theme === 'idle') {
            theme = this.theme;
        } else if (
            this.appState.jackpotsList.some(
                (j) => j.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING,
            )
        ) {
            theme = 'win';
        }
        if (theme && this.title) {
            return `${JACKPOT_TITLE_IMG_PATH}/${theme}/${this.title}.svg`;
        }
        return null;
    }

    get theme(): boolean | string {
        return this.appState.appSettings.jackpots?.theme || false;
    }

    get title(): string | null {
        return this.appState.appSettings.jackpots?.title || null;
    }

    get themeColors(): string[] {
        return this.appState.appSettings.jackpots?.themeColors || [];
    }

    get jackpotsMaxNumber(): number {
        return this.appState.appSettings.jackpots?.maxNumber || 4;
    }

    @action.bound
    handleResponse(response: jpmonitor.GameExtrasServerResponse) {
        if (response?.jackpots) {
            this.processJackpots(response.jackpots);
        }
        if (response?.siteFeed?.jpLastWins) {
            this.processLastWins(response.siteFeed.jpLastWins);
        }
        if (response?.siteFeed?.lastWins) {
            this.appState.lastWinsActions.processLastWins(response.siteFeed.lastWins);
        }
        if (response?.siteFeed?.topWins) {
            this.appState.lastWinsActions.processTopWins(response.siteFeed.topWins);
        }
        if (response?.siteFeed?.withdrawals || response?.siteFeed?.deposits) {
            this.appState.lastWinsActions.processSiteBillingNotifications(
                response.siteFeed.withdrawals ?? [],
                response.siteFeed.deposits ?? [],
            );
        }
        if (response?.tournaments) {
            this.appState.tournamentsInfoActions.handleTournaments(response.tournaments);
        }
    }

    @action.bound
    processJackpots(jackpots: jpmonitor.IJackpotState[]) {
        this.allJackpots.replace(
            jackpots.map((jackpot) => {
                return new JackpotState(this.appState, jackpot);
            }),
        );
        this.appState.jackpotsList = this.allJackpots.slice(0, this.jackpotsMaxNumber);
    }

    @action.bound
    processLastWins(lastWins: jpmonitor.ISiteFeedMessage[]): void {
        const currentTime = new Date().getTime();
        const winners: JackpotWinState[] = [];
        const currentWinsIds = this.winners.map((_) => _.idString);
        lastWins?.forEach((win) => {
            const id = win.id?.toString();
            if (!!id && !currentWinsIds.includes(id)) {
                winners.push(new JackpotWinState(this.appState, win));
            }
        });
        const winnersSorted = winners.sort((a, b) => (a.timestamp ?? 0) - (b.timestamp ?? 0));
        winnersSorted.forEach((_) => {
            this.winners.unshift(_);
            if (_.timestamp) {
                if (
                    currentTime - _.timestamp <
                    WIN_NOTIFICATION_INTERVAL +
                        (this.appState.jpMonitorAPI.queryDelayInMilliseconds ?? 0)
                ) {
                    this.appState.jackpotWinners.processWinNotification(_);
                }
            }
        });
    }

    @action
    zeroReplacingFn: (value: number) => string = (value: number) => {
        return replaceZeros(value);
    };

    findJackpot(name: string, jackpots: site.IJackpotProto[]): site.IJackpotProto | null {
        return jackpots.find((j: site.IJackpotProto) => j.name === name) || null;
    }

    getJackpotMobileSize(size?: number) {
        const jackpotsListLength = size || this.appState.jackpotsList.length;

        switch (jackpotsListLength) {
            case 1:
                return '310x79';
            case 2:
                return '154x46';
            case 3:
                return '102x46';
            default:
                return '76x46';
        }
    }

    getJackpotSize() {
        const jackpotsListLength = this.appState.jackpotsList.length;
        switch (jackpotsListLength) {
            case 1:
                return '249x310';
            case 2:
                return '249x156';
            case 3:
                return '249x101';
            default:
                return '249x78';
        }
    }

    getJackpotNoBannersSize(size?: number) {
        const jackpotsListLength = size || this.appState.jackpotsList.length;
        switch (jackpotsListLength) {
            case 1:
                return '1034x153';
            case 2:
                return '636x155';
            case 3:
                return '431x105';
            default:
                return '319x79';
        }
    }

    getJackpotColor(jp: JackpotState) {
        const index = this.appState.jackpotsList.indexOf(jp);
        switch (index) {
            case 0:
                return 'Platinum';
            case 1:
                return 'Diamond';
            case 2:
                return 'Gold';
            case 3:
                return 'Silver';
            default:
                return 'Platinum';
        }
    }

    getJackpotIndex(jp: JackpotState) {
        const index = this.appState.jackpotsList.indexOf(jp);

        if (this.themeColors.length && this.themeColors[index]) {
            switch (this.themeColors[index]) {
                case 'black':
                    return 0;
                case 'blue':
                    return 1;
                case 'gold':
                    return 2;
                case 'green':
                    return 3;
                case 'violet':
                    return 4;
                case 'red':
                    return 5;
                default:
                    return 0;
            }
        }
        if (index > this.jackpotsMaxNumber - 1) {
            return this.jackpotsMaxNumber - 1;
        }
        return index;
    }

    getThemeColor(jp: JackpotState) {
        const index = this.appState.jackpotsList.indexOf(jp);
        if (this.themeColors.length && this.themeColors[index]) {
            return this.themeColors[index];
        }
        return 'default';
    }

    getMobileJackpotLink(jp: JackpotState, size?: number) {
        if (this.theme) {
            const theme =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? 'win'
                    : 'idle';
            return `${JACKPOT_IMG_PATH_MOBILE}/${theme}/Jackpot_${this.getJackpotIndex(
                jp,
            )}_Mob_${this.getJackpotMobileSize(size || 2)}.svg`;
        }
        let path = '';
        if (this.appState.appSettings.newMobileJackpots) {
            path =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? JACKPOT_IMG_PATH_MOBILE_NEW_BLINKING
                    : JACKPOT_IMG_PATH_MOBILE_NEW;
        } else {
            path =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? JACKPOT_IMG_PATH_MOBILE_BLINKING
                    : JACKPOT_IMG_PATH_MOBILE;
        }

        return `${path}/Jackpot_${this.getJackpotColor(jp)}_Mob_${this.getJackpotMobileSize(
            size,
        )}.svg`;
    }

    getJackpotLink(jp: JackpotState) {
        if (this.theme) {
            const theme =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? 'win'
                    : 'idle';
            return `${JACKPOT_IMG_PATH}/${theme}/Jackpot_${this.getJackpotIndex(
                jp,
            )}_Desktop_${this.getJackpotSize()}.svg`;
        }
        const path =
            jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                ? JACKPOT_IMG_PATH_BLINKING
                : JACKPOT_IMG_PATH;
        return `${path}/Jackpot_${this.getJackpotColor(jp)}_Desktop_${this.getJackpotSize()}.svg`;
    }

    getJackpotLinkWithoutSlider(jp: JackpotState, size?: number) {
        if (this.theme) {
            const theme =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? 'win'
                    : 'idle';
            return `${JACKPOT_IMG_PATH_NOBANNERS}/${theme}/Jackpot_${this.getJackpotIndex(
                jp,
            )}_Desktop_${this.getJackpotNoBannersSize(size)}.svg`;
        }
        const path =
            jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                ? JACKPOT_IMG_PATH_NOBANNERS_BLINKING
                : JACKPOT_IMG_PATH_NOBANNERS;
        return `${path}/Jackpot_${this.getJackpotColor(jp)}_Desktop_${this.getJackpotNoBannersSize(
            size,
        )}.svg`;
    }

    getGameJackpotLink(jp: JackpotState) {
        if (this.theme) {
            const theme =
                jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                    ? 'win'
                    : 'idle';
            return `${JACKPOT_IMG_PATH_MOBILE}/${theme}/Jackpot_${this.getJackpotIndex(
                jp,
            )}_Mob_154x46.svg`;
        }
        const path =
            jp.state === jpmonitor.JackpotState.JackpotBlinkingState.IS_BLINKING
                ? JACKPOT_IMG_PATH_MOBILE_BLINKING
                : JACKPOT_IMG_PATH_MOBILE;
        return `${path}/Jackpot_${this.getJackpotColor(jp)}_Mob_154x46.svg`;
    }

    getJackpotClass() {
        const jackpotsListLength = this.appState.jackpotsList.length;
        switch (jackpotsListLength) {
            case 1:
                return 'large';
            case 2:
                return 'middle';
            case 3:
                return 'small';
            default:
                return 'xs';
        }
    }
}
