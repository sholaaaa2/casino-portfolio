import {action, observable, computed} from 'mobx';
import React from 'react';
import {bonuses, messages} from '../../../api/proto';
import {BonusType, getBonusActivationErrorToken} from '../../../api/utils';
import AppState from '../../AppState';
import {NotificationItemLevel} from '../../components/Notificator';

export class ManualPromoCode {
    @observable value: string;
    @observable isSubmitting = false;
    bonusType: BonusType;

    constructor(private appState: AppState, value: string, bonusType: BonusType) {
        this.value = value;
        this.bonusType = bonusType;
    }

    @computed
    get isActivationDisabled(): boolean {
        return this.value.length === 0 || this.isSubmitting;
    }

    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.value = e.target.value;
    }

    @action.bound
    activate() {
        if (!this.isSubmitting && !!this.value) {
            const promocode = this.value;
            this.isSubmitting = true;
            this.appState.api.activatePromoCode(
                promocode,
                (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                    this.appState.processApi(msg, act);
                    const result = act.bonusAction?.activateCode?.result;
                    const meta = act.bonusAction?.activateCode?.meta;
                    if (result === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                        this.appState.notificator?.addNotification({
                            level: NotificationItemLevel.SUCCESS,
                            message: this.appState.t('promo-code-success', {promocode}),
                        });
                        if (this.bonusType === BonusType.DEPOSIT_BY_CODE) {
                            this.appState.router.push(this.appState.l('/refill'));
                        } else {
                            this.appState.profileBonuses.tryLoadBonusesData();
                        }
                    } else {
                        const error = meta?.error
                            ? this.appState.t('error-promo-code-conditions-not-met', {
                                  reason: this.appState.t(meta.error),
                              })
                            : this.appState.t(getBonusActivationErrorToken(result));
                        this.appState.notificator?.addNotification({
                            level: NotificationItemLevel.ERROR,
                            message: error,
                        });
                    }
                    this.isSubmitting = false;
                },
            );
        }
    }
}
