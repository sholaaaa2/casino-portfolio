import AppState from '../../AppState';

const MEASURE_WHEEL_ITEM_ID = 'js-measure-wheel-item';

export default class UISkillWheel {
    constructor(private appState: AppState) {
    }

    // calc font size for wheel items
    calcFontSizeForWheelItems = () => {
        const skillWheelEl = this.appState.getRef('skill-wheel');
        if (skillWheelEl) {
            const skillWheelItemEl = skillWheelEl.children[0];
            if (!skillWheelItemEl) {
                return;
            }

            const itemStyles = window.getComputedStyle(skillWheelItemEl);
            const maxWidth = skillWheelEl.offsetWidth;
            const gap = 275;
            const maxFontSize = 300;
            let fontSize = parseFloat(itemStyles.fontSize || '0');
            let {textWidth, text} = this.getMaxWidthText(this.appState.skillWheel.wheelItems.slice(), itemStyles.fontSize!);

            if (textWidth + gap > maxWidth) {
                while (textWidth + gap > maxWidth) {
                    --fontSize;
                    textWidth = this.getMaxWidthText([text], `${fontSize}px`).textWidth;
                }
            } else {
                while (textWidth + gap < maxWidth && fontSize < maxFontSize) {
                    ++fontSize;
                    textWidth = this.getMaxWidthText([text], `${fontSize}px`).textWidth;
                }
            }

            skillWheelEl.style.fontSize = `${fontSize}px`;

            this.removeMeasureItem();
        }
    }

    getMaxWidthText = (items: number[] | string[], fontSize: string): { textWidth: number, text: string } => {
        let width = 0;
        let text = '';

        let spanEl = document.getElementById(MEASURE_WHEEL_ITEM_ID) as HTMLElement;
        if (!spanEl) {
            spanEl = this.appendMeasureItem();
        }

        for (let i = 0; i < items.length; i++) {
            const item = items[i].toString();
            spanEl.innerHTML = item;
            spanEl.style.fontSize = fontSize;

            if (spanEl.offsetWidth > width) {
                width = spanEl.offsetWidth;
                text = item;
            }
        }

        return {
            textWidth: width,
            text: text
        };
    }

    appendMeasureItem = (): HTMLElement => {
        const span = document.createElement('span');
        span.setAttribute('id', MEASURE_WHEEL_ITEM_ID);
        document.body.appendChild(span);
        return span;
    }

    removeMeasureItem = () => {
        const span = document.getElementById(MEASURE_WHEEL_ITEM_ID);
        if (span) {
            span.remove();
        }
    }
}
