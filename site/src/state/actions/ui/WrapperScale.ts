import AppState from '../../AppState';
import {getTranslateEl, ScreenSize} from '../../utils/BrowserUtils';

export default class WrapperScale {
    appState: AppState;
    prevViewportWidth: number;
    prevViewportHeight: number;
    page: string | null;

    constructor(appState: AppState) {
        this.appState = appState;
        this.page = null;

        this.calcNotificationsScale = this.calcNotificationsScale.bind(this);
    }

    // wrapper scale
    calcWrapperScale(contentWrapper?: HTMLElement | null, alwaysUpdate?: boolean, stretchVertical?: boolean) {
        const appState = this.appState;
        const {viewport, page} = appState;

        const wrapper = contentWrapper || appState.getRef('wrapper') as HTMLElement;
        const updateIsRequired = this.page !== page || (this.prevViewportWidth !== viewport.width || this.prevViewportHeight !== viewport.height);

        if (!appState.loading && wrapper && (updateIsRequired || alwaysUpdate)) {
            // save prev values
            this.page = page;
            this.prevViewportWidth = viewport.width;
            this.prevViewportHeight = viewport.height;

            let verticalOffset = viewport.height / 10;
            let horizontalOffset = viewport.width / 10;

            // if mobile and viewport > ScreenSize.MOBILE and this is login page, than reset scale to 1
            const preventUpdateScale = appState.isMobileOnly && page === 'generated-login';

            if ((viewport.width >= ScreenSize.MOBILE && !preventUpdateScale) || alwaysUpdate) {
                const deltaByWidth = (viewport.width - horizontalOffset) / wrapper.clientWidth;
                const deltaByHeight = (viewport.height - verticalOffset) / wrapper.clientHeight;
                const scale = Math.min(deltaByHeight, deltaByWidth);
                const translateEl = getTranslateEl(wrapper);
                const prevScale = translateEl ? Number(translateEl.scale) : null;

                if ((prevScale !== null && scale.toFixed(2) !== prevScale.toFixed(2)) || prevScale === null) {
                    wrapper.style.top = '0px';
                    wrapper.style.left = '0px';
                    if (stretchVertical) {
                        wrapper.style.webkitTransform = `scaleX(${deltaByWidth}) scaleY(${deltaByHeight})`;
                        wrapper.style.transform = `scaleX(${deltaByWidth}) scaleY(${deltaByHeight})`;
                    } else {
                        wrapper.style.webkitTransform = 'scale(' + scale + ')';
                        wrapper.style.transform = 'scale(' + scale + ')';
                    }

                    const bounding = wrapper.getBoundingClientRect();
                    wrapper.style.top = -bounding.top + (viewport.height - bounding.height) / 2 + 'px';
                    wrapper.style.left = -bounding.left + (viewport.width - bounding.width) / 2 + 'px';
                }
            } else if (wrapper) {
                wrapper.style.top = '0px';
                wrapper.style.left = '0px';
                wrapper.style.webkitTransform = 'scale(1)';
                wrapper.style.transform = 'scale(1)';
            }
        }
    }

    // end region

    // notifications scale
    calcNotificationsScale() {
        const cards = document.getElementsByClassName('js-card-popup') as HTMLCollection;
        for (let i = 0; i < cards.length; i++) {
            this.initNotificationScale(cards[i] as HTMLElement);
        }
    }

    initNotificationScale(card: HTMLElement) {
        const appState = this.appState;

        if (!appState.loading && card) {
            const viewport = appState.viewport;
            const cardContainerStyles = window.getComputedStyle(card.parentElement!);
            const vOffset = 12;
            const top = parseInt(cardContainerStyles.top || '0', 10);
            const right = parseInt(cardContainerStyles.right || '0', 10);
            const width = parseInt(cardContainerStyles.width || '0', 10);
            const height = parseInt(cardContainerStyles.height || '0', 10);
            const isViewportWidthLessCardWidth = viewport.width < card.offsetWidth + right * 2;
            const isViewportHeightLessCardHeight = viewport.height < card.offsetHeight + top + vOffset;
            const verticalOffset = top + vOffset;
            const horizontalOffset = right * 2;

            if (card.offsetWidth <= width && (isViewportWidthLessCardWidth || isViewportHeightLessCardHeight)) {
                const deltaByWidth = (viewport.width - horizontalOffset) / card.clientWidth;
                const deltaByHeight = (viewport.height - verticalOffset) / card.clientHeight;
                const scale = Math.min(deltaByHeight, deltaByWidth);
                const translateEl = getTranslateEl(card);
                const prevScale = translateEl ? Number(translateEl.scale) : null;

                if ((prevScale !== null && scale.toFixed(2) !== prevScale.toFixed(2)) || prevScale === null) {
                    card.style.webkitTransform = 'scale(' + scale + ')';
                    card.style.transform = 'scale(' + scale + ')';
                    card.style.top = -((height - height * scale) / 2) + 'px';
                    card.style.right = -((width - width * scale) / 2) + 'px';
                }
            } else {
                card.style.top = '0px';
                card.style.right = '0px';
                card.style.webkitTransform = 'scale(1)';
                card.style.transform = 'scale(1)';
            }
        }
    }

    // end region
}
