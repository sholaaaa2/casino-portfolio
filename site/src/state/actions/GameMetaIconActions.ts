import {action, observable} from 'mobx';
import AppState from '../AppState';
import {GameItem} from './GamesActions';
import {proto} from '../../api/proto';
import { SitePages } from '../meta';
import {debounce} from '../utils/BrowserUtils';

export default class GameMetaIconActions {
    private appState: AppState;
    @observable fetchingStarted: boolean = false;
    @observable gamesWithMeta: GameItem[] = [];
    @observable gamesMeta: {
      [gameId: string]: proto.icon.IMetaDataEntry[];
    } | {} = {};
    intervalForChangesMeta: number;
    gamesUpdateTimeouts: {
      [gameId: string]: number;
    } | {} = {};

    gamesUpdateTimeoutsMs: {
      [gameId: string]: number;
    } | {} = {};

    gamesFetchStatus: {
      [gameId: string]: boolean;
    } | {} = {};

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @action
    gamesMetaInit = (games: GameItem[]) => {
        if (!games || !games.length) return;
        this.gamesWithMeta = games;
        if (this.isGamesListPage()) {
            this.fetchingStarted = true;
            this.gamesMetaFetch(games);
        }
        const updateTime = this.appState.appSettings.games.gameIconAnimationTimeMs
            ? this.appState.appSettings.games.gameIconAnimationTimeMs
            : 5000;
        this.intervalForChangesMeta = window.setInterval(() => this.changeMeta(games), updateTime);
        this.appState.router.listen((location, action) => {
            if (this.isGamesListPage()) {
                if (!this.fetchingStarted) {
                    this.gamesMetaInit(games);
                } else {
                    setTimeout(this.gamesVisibilityCheck, 0);
                }
            } else {
                this.stopFetching(true);
            }
        });
        if (!this.appState.appSettings.games.useOptimizedGamesGrid) {
            window.addEventListener('scroll', this.listener);
        }
        window.addEventListener('visibilitychange', this.visibilityChangeListener);
    }

    visibilityChangeListener = () => {
        if (document.hidden) {
            this.stopFetching(false);
        } else if (!this.fetchingStarted && this.gamesWithMeta) {
            this.gamesMetaInit(this.gamesWithMeta);
        }
    }

    stopFetching = (fullReset: boolean) => {
        this.fetchingStarted = false;
        this.resetData(fullReset);
        clearInterval(this.intervalForChangesMeta);
    }

    listener = debounce(() => {
        this.gamesVisibilityCheck();
    },500)

    gamesVisibilityCheck = () => {
        Object.keys(this.gamesFetchStatus).forEach(key => {
            if (this.isElementVisible("game_" + key)) {
                this.startFetchingForGame(key);
            } else {
                this.stopFetchingForGame(key);
            }
        });
    }

    stopFetchingForGame = (gameId: string) => {
        this.gamesFetchStatus[gameId] = false;
        if (this.gamesUpdateTimeouts[gameId]) {
            clearTimeout(this.gamesUpdateTimeouts[gameId]);
        }
    }

    startFetchingForGame = (gameId: string) => {
        if (!this.gamesFetchStatus[gameId]) {
            this.gamesFetchStatus[gameId] = true;
            const game = this.appState.games.findGameById(gameId);
            if (game) this.metaLoad(game);
        }
    }

    @action
    removeListeners = (fullReset: boolean) => {
        window.removeEventListener('scroll', this.listener);
        if (fullReset) window.removeEventListener('visibilitychange', this.visibilityChangeListener);
    };

    @action
    resetData = (fullReset: boolean) => {
        this.removeListeners(fullReset);
        this.gamesFetchStatus = {};
        Object.values(this.gamesUpdateTimeouts).forEach(timeout => clearTimeout(timeout));
        this.gamesUpdateTimeouts = {};
        this.gamesUpdateTimeoutsMs = {};
    }

    @action
    gamesMetaFetch = async (games: GameItem[]) => {
        await games.forEach(async(game) => this.metaLoad(game, true))
    };

    @action
    metaLoad = async(game: GameItem, firstLoad?: boolean) => {
        if (firstLoad) {
            this.gamesFetchStatus[game.id!] = true;
            setTimeout(() => {
              if (!this.isElementVisible("game_" + game.id!)) {
                  this.gamesFetchStatus[game.id!] = false;
              }
            }, 1000);
        }

        if (game.iconMetaUrl && this.gamesFetchStatus[game.id!]) {
            const response = await this.appState.api.iconMetaUrlRequest(game.iconMetaUrl);
            if (response) {
                const message = proto.icon.IconMetaData.decode(response);
                if (message.entries) {
                    game.meta = message.entries;
                    const metaForShow = this.processMetaForShow(game.meta, game.id!);
                    if (firstLoad) {
                        game.metaForShow = metaForShow;
                        this.setGamesMeta(game.id!, metaForShow);
                    }
                    this.appState.games.setGameWithMeta(game);
                }
                if (message.nextQuerySeconds && this.fetchingStarted) {
                    const timeout = message.nextQuerySeconds.toNumber() * 1000;
                    this.gamesUpdateTimeoutsMs[game.id!] = timeout;
                    this.gamesUpdateTimeouts[game.id!] = setTimeout(() => this.metaLoad(game), timeout);
                }

            } else {
                if (this.gamesUpdateTimeoutsMs[game.id!]) {
                    this.gamesUpdateTimeouts[game.id!] = setTimeout(() => this.metaLoad(game), this.gamesUpdateTimeoutsMs[game.id!]);
                }
            }
        }
    }

    @action
    processMetaForShow = (meta: proto.icon.IMetaDataEntry[], gameId: string) => {
        const arrayForSlice = meta.length > 1 && this.gamesMeta[gameId]
          ? meta.filter(meta => meta.key !== this.gamesMeta[gameId][0]?.key)
          : meta;

        return this.shuffleArray(arrayForSlice).slice(0,1).map(entry => {
            if (entry.localizedKey && this.appState.language && entry.localizedKey[this.appState.language]) {
                return {
                  ...entry,
                  key: entry.localizedKey[this.appState.language],
                }
            }
            return entry
        });
    }

    getMaxKeyLength = (meta: proto.icon.IMetaDataEntry[]) => {
        let maxLength = 0;
        meta.forEach(entry => {
            if (entry.key?.length && entry.key?.length > maxLength) {
                maxLength = entry.key?.length;
            }
        });
        return maxLength;
    }

    shuffleArray(array: proto.icon.IMetaDataEntry[]) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }

    @action
    changeMeta = (games: GameItem[]) => {
        this.appState.games.gamesList = this.appState.games.gamesList.map(game => {
            if (game.meta) {
                const metaForShow = this.processMetaForShow(game.meta, game.id!);
                this.setGamesMeta(game.id!, metaForShow);
                return {
                  ...game,
                  metaForShow,
                }
            }
            return game;
        });
    };

    @action
    setGamesMeta(gameId: string, meta: proto.icon.IMetaDataEntry[]) {
        const oldMeta = this.gamesMeta;
        this.gamesMeta = {...oldMeta, [gameId]: meta,}
    }

    gameShowMeta(game: GameItem) {
        return this.isGamesListPage() && !!this.gamesMeta[game.id!];
    }

    isGamesListPage() {
        if (window.location.pathname &&
           (window.location.pathname.search('/games/') !== -1 ||
            (this.appState.site.homePageName === SitePages.MAIN && (
              window.location.pathname === this.appState.l('/') ||
              window.location.pathname === '/'
            )))) {
            return true;
        }
        return false;
    }

    isElementVisible(id: string) {
      const target = document.getElementById(id);
      if (!target) return false;
      let targetPosition = {
          top: window.pageYOffset + target.getBoundingClientRect().top,
          left: window.pageXOffset + target.getBoundingClientRect().left,
          right: window.pageXOffset + target.getBoundingClientRect().right,
          bottom: window.pageYOffset + target.getBoundingClientRect().bottom
      };

      let windowPosition = {
          top: window.pageYOffset,
          left: window.pageXOffset,
          right: window.pageXOffset + document.documentElement.clientWidth,
          bottom: window.pageYOffset + document.documentElement.clientHeight
      };

      if (targetPosition.bottom > windowPosition.top &&
          targetPosition.top < windowPosition.bottom &&
          targetPosition.right > windowPosition.left &&
          targetPosition.left < windowPosition.right) {
          return true;
      } else if (window.location.pathname.search('/games/favorites')) {
          return true;
      } else {
          return false;
      };
    }
}
