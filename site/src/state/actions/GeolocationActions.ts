import { action, observable } from "mobx";
import { messages, usersLocations } from "../../api/proto";
import AppState from "../AppState";


const geoOptions = {
  enableHighAccuracy: true,
  maximumAge: 0
};

interface GeolocationPositionError {
	code: number;
	message: string;
}

interface GeolocationPosition {
  coords?: {
    accuracy?: number;
    altitude?: number | null;
    altitudeAccuracy?: number | null;
    heading?: number | null;
    latitude?: number;
    longitude?: number;
    speed?: number | null;
  }
  timestamp?: number;
}

export type Callback = (isSuccess: boolean, error?: string) => void;

export class GeolocationActions {

    constructor(protected appState: AppState) {}

    @observable coords?: {
      accuracy?: number;
      latitude?: number;
      longitude?: number;
    } = {};

    @action
    sendGeolocation = (cb: Callback) => {
        if (this.appState.isApk && window.ReactNativeWebView?.postMessage) {
            document.addEventListener("message", (event: any) => {
              if (event.data) {
                  const response = JSON.parse(event.data);
                  if (response?.type && response?.type === 'geo-enable') {
                      if (response?.isSuccess) {
                          navigator.geolocation.getCurrentPosition(
                              (position: GeolocationPosition) => this.geoSuccess(position, cb),
                              (error: GeolocationPositionError) => this.geoError(error, cb),
                              geoOptions
                          );
                      } else {
                          cb(false, response?.errText ? response?.errText : 'Get geolocation error.');
                      }
                  }
              }
            });
            window.ReactNativeWebView.postMessage(JSON.stringify({
                type: 'geo-enable',
            }));

        } else {
            navigator.geolocation.getCurrentPosition(
                (position: GeolocationPosition) => this.geoSuccess(position, cb),
                (error: GeolocationPositionError) => this.geoError(error, cb),
                geoOptions
            );
        }
    }

    @action
    geoSuccess = (position: GeolocationPosition, cb: Callback) => {
        const {coords} = position;

        if (coords && coords.latitude && coords.longitude && coords.accuracy) {
            this.coords = {
              latitude: coords.latitude,
              longitude: coords.longitude,
              accuracy: coords.accuracy,
            };
            this.appState.api.sendUserGeolocation(
              {
                lat: coords.latitude,
                long: coords.longitude,
                accuracy: coords.accuracy,
              },
              (msg: messages.IServerResponse, act: messages.IClientActionResponse) => this.onGeoLocationSend(msg, act, cb)
            )
        }
    }

    onGeoLocationSend = (msg: messages.IServerResponse, act: messages.IClientActionResponse, cb: Callback) => {
        if (act?.addUserLocationResponse?.result && act?.addUserLocationResponse?.result === usersLocations.AddUserGeoLocationResponse.ResultStatus.internal_error) {
            console.debug(act?.addUserLocationResponse?.result, 'error')
            cb(false, 'send-error');
        } else {
            cb(true);
        }
    }

    @action
    geoError = (error: GeolocationPositionError, cb: Callback) => {
        console.debug(error);
        cb(false, 'Get geolocation error');
    }
}
