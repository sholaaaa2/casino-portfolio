import * as React from 'react';
import {ChangeEvent} from 'react';
import {action, computed, observable} from 'mobx';
import AppState from '../AppState';
import {isValidEmail} from '../utils/Validation';
import {messages, site, users} from '../../api/proto';
import {
    clearPhoneNumber,
    formatInputDate,
    getEmailChangeError,
    getPhoneChangeError,
    getPhoneCodeByCountry,
    getPhoneConfirmationError,
    getPhoneParams,
    isValidDate,
} from '../../api/utils';
import Long from 'long';
import {RegisterCustomFields} from '../../api/APIConnector';
import {NotificationItemLevel} from '../components/Notificator';
import {NString, UserFieldType, UserProfileGroup, UserProfileGroupIconStatusClass} from '../meta';
import {NumberFormatProps} from 'react-number-format';
import {getPhoneMask} from '../utils/getPhoneMask';
import { BM_PASSWORD_LENGTH } from '../controls/PasswordInputState';

export class ProfileEditActions {
    appState: AppState;

    @observable inputEmail: string | null = null;
    @observable emailError: string | null = null;
    @observable isEmailLoading: boolean = false;

    @observable inputPassword: string | null = null;
    @observable inputPasswordConfirmation: string | null = null;
    @observable passwordError: string | null = null;
    @observable passwordConfirmationError: string | null = null;
    @observable isPasswordLoading: boolean = false;

    @observable showPhoneConfirmationInput: boolean = false;
    @observable inputPhoneNumber: string | null = null;
    @observable newPhoneNumber: string | null = null;
    @observable inputPhoneCode: string | null = null;
    @observable inputPhoneCountry: string | null = null;
    @observable newPhoneCountry: string | null = null;
    @observable phoneError: string | null = null;
    @observable phoneConfirmationCodeError: string | null = null;
    @observable isPhoneLoading: boolean = false;
    @observable isPhoneConfirmationLoading: boolean = false;
    @observable confirmCode: string = '';
    @observable successPhoneConfirm: boolean = false;
    @observable successEmailConfirm: boolean = false;
    @observable successPasswordChange: boolean = false;

    @observable userFields: { [key: string]: string | number | Long } = {};
    @observable userFieldsOld: { [key: string]: string | number | Long } = {};
    @observable userFieldsError: { [key: string]: string } = {};
    @observable isUserFieldsLoading: boolean = false;

    @observable passwordEyeInputPasswordIsOpen: boolean = false;
    @observable passwordEyeInputPasswordConfirmationIsOpen: boolean = false;

    @observable resendIntervalChangePhone?: number;
    changePhoneTimeout?: NodeJS.Timer;

    constructor(state: AppState) {
        this.appState = state;

        this.showPhoneInput = this.showPhoneInput.bind(this);
        this.showConfirmationCodeInput = this.showConfirmationCodeInput.bind(this);

        this.tryChangePhone = this.tryChangePhone.bind(this);
        this.tryChangeEmail = this.tryChangeEmail.bind(this);
        this.tryConfirmPhone = this.tryConfirmPhone.bind(this);
        this.tryChangePassword = this.tryChangePassword.bind(this);
        this.tryChangeUserFields = this.tryChangeUserFields.bind(this);
        this.tryResendEmailConfirmation = this.tryResendEmailConfirmation.bind(this);
        this.tryReSendPhoneConfirmation = this.tryReSendPhoneConfirmation.bind(this);

        this.onEmailInputChange = this.onEmailInputChange.bind(this);
        this.onPhoneInputChange = this.onPhoneInputChange.bind(this);
        this.onChangeUserFields = this.onChangeUserFields.bind(this);
        this.onPasswordInputChange = this.onPasswordInputChange.bind(this);
        this.onInputUserFieldChange = this.onInputUserFieldChange.bind(this);
        this.onPasswordConfirmationInputChange = this.onPasswordConfirmationInputChange.bind(this);
        this.onPhoneConfirmationCodeInputChange = this.onPhoneConfirmationCodeInputChange.bind(this);

        this.resetFormErrors = this.resetFormErrors.bind(this);
    }

    // region ---- phone
    @computed
    get canShowPhoneConfirmationInput() {
        if (this.appState.rawUserInfo) {
            return (
                (!!this.appState.rawUserInfo.phone && !this.appState.rawUserInfo.phoneConfirmed) ||
                !!this.appState.rawUserInfo.newPhone
            );
        } else {
            return false;
        }
    }

    @action
    showPhoneInput(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.showPhoneConfirmationInput = false;
    }

    @action
    showConfirmationCodeInput(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.showPhoneConfirmationInput = true;
    }

    @action.bound
    goToPhoneEdit(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.successPhoneConfirm = false;
        this.appState.router.push(this.appState.l('/profile/edit/phone'))
    }

    @action.bound
    goToEmailEdit(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.successEmailConfirm = false;
        this.appState.router.push(this.appState.l('/profile/edit/email'))
    }

    @action.bound
    goToPasswordEdit(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.successPasswordChange = false;
        this.appState.router.push(this.appState.l('/profile/edit/password'))
    }

    @action.bound
    isFullWidthField(fieldName?: string) {
        if (fieldName && (
            fieldName === "first-name" ||
            fieldName === "last-name" ||
            fieldName === "country" ||
            fieldName === "city")) {
            return false;
        }
        return true;
    }

    isPhoneMatchToInput(phone: string): boolean {
        const phoneParams = getPhoneParams(phone, this.appState.site.getCountries());
        return (
            !!phoneParams &&
            clearPhoneNumber(this.inputPhoneNumber) === phoneParams.cleanPhoneNumber &&
            this.inputPhoneCode === phoneParams.phoneCode
        );
    }

    eyePasswordInputToggle = (e: React.MouseEvent<HTMLElement>) => {
        e?.preventDefault();
        this.passwordEyeInputPasswordIsOpen = !this.passwordEyeInputPasswordIsOpen;
    }

    eyePasswordInputConfirmationToggle = (e: React.MouseEvent<HTMLElement>) => {
        e?.preventDefault();
        this.passwordEyeInputPasswordConfirmationIsOpen = !this.passwordEyeInputPasswordConfirmationIsOpen;
    }

    @action
    onPhoneInputChange(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        if (e.target instanceof HTMLSelectElement) {
            const country = this.appState.site.getCountries()!.find((c: site.ICountryInfo) => c.code === e.target.value);
            if (country) {
                this.inputPhoneCode = country.phoneCode!.toString();
                this.inputPhoneCountry = country.code!;
            }
        }

        this.phoneError = null;
        this.phoneConfirmationCodeError = null;
    }

    @computed
    get phoneMask(): string | undefined {
        return getPhoneMask(this.inputPhoneCountry!, this.appState.site.countries);
    }

    @action.bound
    updatePhone(phone: NumberFormatProps | string): void {
        if (typeof phone === 'string') {
            this.inputPhoneNumber = phone;
        } else {
            this.inputPhoneNumber = phone.formattedValue;
        }
        //this.phoneError = null;
        this.phoneConfirmationCodeError = null;
    }

    @action
    onPhoneConfirmationCodeInputChange() {
        this.phoneConfirmationCodeError = null;
    }

    @action.bound
    onPhoneFocus(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        const button = document.getElementsByClassName('bottom-button')[0];
        if (this.appState.isMobileOnly && button) {
            button.scrollIntoView();
        }
    }

    @action.bound
    onPhoneCodeFocus(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        if (e.target.id) {
            const id = e.target.id;
            const num = Number(id.split('-')[1] ? id.split('-')[1] : '1');
            if (num > this.confirmCode.length + 1) {
                document.getElementById(`codechar-${this.confirmCode.length + 1}`)?.focus();
            }
        }
    }

    @action.bound
    onPhoneCodeChange(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        if (e.target.id) {
            const id = e.target.id;
            const num = Number(id.split('-')[1] ? id.split('-')[1] : '1');
            if (e?.target?.value && /^[0-9]+$/.test(e?.target?.value) && num <= this.appState.confirmationCodeMethodState.codeLength && this.confirmCode?.length < this.appState.confirmationCodeMethodState.codeLength) {
                let insertValue = '';
                let idForFocus = num;
                if (e?.target?.value?.length > 2) {
                    const availableChars = this.appState.confirmationCodeMethodState.codeLength - this.confirmCode.length;
                    insertValue = e?.target?.value.slice(0, availableChars);
                    idForFocus += insertValue.length;
                } else {
                    insertValue = e?.target?.value?.length > 1 ? e?.target?.value.slice(1, 2) : e?.target?.value;
                    idForFocus ++;
                }
                if (num < this.confirmCode?.length) {
                    this.confirmCode = this.confirmCode.slice(0, num) + insertValue + this.confirmCode.slice(num);
                } else {
                    this.confirmCode += insertValue;
                }
                document.getElementById(`codechar-${idForFocus}`)?.focus();
                if (this.confirmCode.length === this.appState.confirmationCodeMethodState.codeLength) {
                    this.tryConfirmPhone();
                }
            }
        }
    }

    @action.bound
    onKeyDownPhoneCode(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 8 && (e.target as HTMLElement).id) {
            const id = (e.target as HTMLElement).id;
            const num = Number(id.split('-')[1] ? id.split('-')[1] : '1');
            if (num <= this.confirmCode.length) {
                this.confirmCode = this.confirmCode ? this.confirmCode.slice(0, num-1) + this.confirmCode.slice(num) : '';
            }
            document.getElementById(`codechar-${num-1}`)?.focus();
        }
    }

    @action
    clearPhoneErrors() {
        this.phoneError = null;
        this.phoneConfirmationCodeError = null;
        setTimeout(() => {
            this.confirmCode = '';
        }, 500);
    }

    @action
    tryReSendPhoneConfirmation(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        const userInfo = this.appState.rawUserInfo!;
        if (userInfo.newPhone) {
            this.isPhoneLoading = true;
            this.appState.api.resendNewPhoneConfirmation(this.onReSendPhoneConfirmation.bind(this));
        } else if (userInfo.phone && !userInfo.phoneConfirmed) {
            this.isPhoneLoading = true;
            this.appState.api.resendPhoneConfirmation(this.onReSendPhoneConfirmation.bind(this));
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('error-refill-unknown-error'),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    onReSendPhoneConfirmation(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.isPhoneLoading = false;
        const userInfo = this.appState.rawUserInfo!;
        const phone = userInfo.newPhone || userInfo.phone;
        const actionData = act.resendPhoneConfirmation || act.resendNewPhoneConfirmation;

        if (actionData && actionData.ok) {
            this.showPhoneConfirmationInput = true;
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-phone-confirmation-resend-successful', {phone: phone}),
                level: NotificationItemLevel.SUCCESS
            });
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-phone-confirmation-resend-failed'),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    @action
    tryChangePhone(e?: React.MouseEvent<HTMLButtonElement>) {
        e?.preventDefault();

        if (this.inputPhoneCode && this.inputPhoneCountry && this.inputPhoneNumber) {
            this.clearPhoneErrors();
            this.isPhoneLoading = true;
            this.appState.api.changePhone(
                this.inputPhoneNumber,
                this.inputPhoneCountry,
                this.appState.confirmationCodeMethodState.isVoiceSelected,
                this.onChangePhone.bind(this)
            );
        } else {
            this.phoneError = this.appState.t('error-register-phone-required');
        }
    }

    onChangePhone(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isPhoneLoading = false;
        this.appState.processApi(msg, act);

        if (act.changePhone && act.changePhone.ok) {
            const phone = this.inputPhoneCode + ' ' + this.inputPhoneNumber;
            this.showPhoneConfirmationInput = true;
            this.newPhoneNumber = this.inputPhoneNumber;
            this.newPhoneCountry = this.inputPhoneCountry;
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-phone-confirmation-resend-successful', {phone: phone}),
                level: NotificationItemLevel.SUCCESS
            });
            if (act.changePhone.timeOutToNextRetry) {
                this.resendIntervalChangePhone = act.changePhone.timeOutToNextRetry.toNumber() / 1000;
                this.changePhoneTimeout = setTimeout(this.onChangePhoneTimeout, 1000);
            }
        } else {
            const error = act.changePhone ? act.changePhone.error : null;
            if (error && error === users.ChangePhoneResponse.ChangePhoneError.retry_too_fast && act.changePhone?.timeOutToNextRetry) {
                this.phoneError = this.appState.t('error-change-phone-retry-too-fast-with-value', {time: act.changePhone.timeOutToNextRetry.divide(1000).toString()})
            } else {
                this.phoneError = this.appState.t(getPhoneChangeError(error));
            }
        }
    }

    @action
    onChangePhoneTimeout = () => {
        if (this.resendIntervalChangePhone) {
            this.resendIntervalChangePhone = this.resendIntervalChangePhone - 1;
            if (this.resendIntervalChangePhone <= 0) {
                if (this.changePhoneTimeout) clearTimeout(this.changePhoneTimeout);
                this.resendIntervalChangePhone = undefined;
            } else {
                this.changePhoneTimeout = setTimeout(this.onChangePhoneTimeout, 1000);
            }
        }
    }

    @action
    tryConfirmPhone(e?: React.MouseEvent<HTMLButtonElement>) {
        e?.preventDefault();

        const code = this.confirmCode;
        if (code) {
            if (this.appState.rawUserInfo!.newPhone) {
                this.isPhoneConfirmationLoading = true;
                this.clearPhoneErrors();
                document.getElementById(`codechar-5`)?.blur();
                this.appState.api.confirmNewPhone(code, this.onConfirmPhone.bind(this));
            } else if (this.appState.rawUserInfo!.phone && !this.appState.rawUserInfo!.phoneConfirmed) {
                this.isPhoneConfirmationLoading = true;
                this.clearPhoneErrors();
                document.getElementById(`codechar-5`)?.blur();
                this.appState.api.confirmPhone(code, this.onConfirmPhone.bind(this));
            } else {
                this.phoneConfirmationCodeError = this.appState.t('error-refill-unknown-error');
            }
        } else {
            this.phoneConfirmationCodeError = this.appState.t('error-register-confirm-code-required');
        }
    }

    @action
    onConfirmPhone(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isPhoneConfirmationLoading = false;
        this.appState.processApi(msg, act);

        if (act.confirmPhone) {
            if (!act.confirmPhone.ok) {
                this.phoneConfirmationCodeError = this.appState.t(getPhoneConfirmationError(act.confirmPhone!.error));
            } else {
                this.successPhoneConfirm = true;
            }
        } else if (act.confirmNewPhone) {
            if (!act.confirmNewPhone.ok) {
                this.phoneConfirmationCodeError = this.appState.t(getPhoneConfirmationError(act.confirmNewPhone!.error));
            } else {
                this.successPhoneConfirm = true;
            }
        } else {
            this.phoneConfirmationCodeError = this.appState.t(getPhoneConfirmationError());
        }
    }

    // endregion

    // region ---- email
    @action
    onEmailInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.inputEmail = e.currentTarget.value;
        this.emailError = '';
    }

    @action
    tryChangeEmail(e?: React.MouseEvent<HTMLButtonElement>) {
        e?.preventDefault();

        const email = this.inputEmail ? this.inputEmail.trim() : undefined;
        let isValid = true;

        if (!email) {
            isValid = false;
            this.emailError = this.appState.t('error-email-required');
        } else if (!isValidEmail(email)) {
            isValid = false;
            this.emailError = this.appState.t('error-email-invalid');
        }

        if (isValid) {
            this.isEmailLoading = true;
            this.appState.api.changeEmail(email!, this.onChangeEmail.bind(this, email));
        }
    }

    @action
    onChangeEmail(email: NString, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isEmailLoading = false;
        this.appState.processApi(msg, act);

        if (act.changeEmail && act.changeEmail.ok) {
            this.successEmailConfirm = true;
        } else {
            const error = act.changeEmail ? act.changeEmail.error : null;
            this.emailError = this.appState.t(getEmailChangeError(error));
        }
    }

    @action
    tryResendEmailConfirmation(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        const userInfo = this.appState.rawUserInfo!;

        if (userInfo.newEmail) {
            this.isEmailLoading = true;
            this.appState.api.resendNewEmailConfirmation(this.onReSendEmailConfirmation.bind(this));
        } else if (userInfo.email) {
            this.appState.api.resendEmailConfirmation(this.onReSendEmailConfirmation.bind(this));
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('error-refill-unknown-error'),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    onReSendEmailConfirmation(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.isEmailLoading = false;

        const userInfo = this.appState.rawUserInfo!;
        const actionData = act.resendEmailConfirmation || act.resendNewEmailConfirmation;
        const email = userInfo.newEmail || userInfo.email;

        if (actionData && actionData.ok) {
            this.appState.router.push('/profile/edit/email');
            this.successEmailConfirm = true;
            this.inputEmail = email as string | null;
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-email-confirmation-resend-failed', {email: email}),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    // endregion

    // region ---- password
    @action
    onPasswordInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.passwordError = null;
        this.inputPassword = e.currentTarget.value;
    }

    @action
    onPasswordConfirmationInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.passwordConfirmationError = null;
        this.inputPasswordConfirmation = e.currentTarget.value;
    }

    @action
    tryChangePassword(e?: React.MouseEvent<HTMLButtonElement>) {
        e?.preventDefault();

        const passwordInput = this.appState.getRef('profile-edit-password') as HTMLInputElement;
        const passwordConfirmationInput = this.appState.getRef('profile-edit-password-confirmation') as HTMLInputElement;
        const password = passwordInput ? passwordInput.value : '';
        const passwordConfirmation = passwordConfirmationInput ? passwordConfirmationInput.value : '';
        let isValid = true;

        if (!password) {
            isValid = false;
            this.passwordError = this.appState.t('error-login-password-required');
            if (this.appState.isMobileOnly) document.getElementsByClassName('bottom-buttons-container')[0]?.scrollIntoView();
        } else if (password.length < 6) {
            isValid = false;
            this.passwordError = this.appState.t('error-password-too-short');
            if (this.appState.isMobileOnly) document.getElementsByClassName('bottom-buttons-container')[0]?.scrollIntoView();
        }

        if (isValid && password !== passwordConfirmation) {
            isValid = false;
            this.passwordConfirmationError = this.appState.t('error-passwords-not-match');
            if (this.appState.isMobileOnly) document.getElementsByClassName('bottom-buttons-container')[0]?.scrollIntoView();
        }

        if (isValid) {
            this.isPasswordLoading = true;
            this.appState.api.changePassword(password, this.onChangePassword.bind(this));
        }
    }

    @action.bound
    onChangePassword(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isPasswordLoading = false;
        this.appState.processApi(msg, act);

        if (act.changePassword) {
            if (act.changePassword.ok) {
                this.successPasswordChange = true;
                this.resetPasswordFields();
            } else {
                this.appState.notificator!.addNotification({
                    message: this.appState.t('error-password-change-failed'),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('error-password-change-failed'),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    resetPasswordFields() {
        const passwordInput = this.appState.getRef('profile-edit-password') as HTMLInputElement;
        const passwordConfirmationInput = this.appState.getRef('profile-edit-password-confirmation') as HTMLInputElement;
        passwordInput.value = '';
        passwordConfirmationInput.value = '';

        this.inputPassword = null;
        this.inputPasswordConfirmation = null;
    }

    // endregion

    // region ---- user fields
    @action
    onInputUserFieldChange(event: ChangeEvent<HTMLSelectElement>) {
        const newUserFields = Object.assign({}, this.userFields);
        const name = event.target.name;
        newUserFields[name] = event.target.value;
        this.userFields = newUserFields;
    }

    @action.bound
    onKeyDownUserField(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 13 && (e.target as HTMLElement).id) {
            const id = (e.target as HTMLElement).id;
            const num = Number(id.split('-')[1] ? id.split('-')[1] : '1');
            document.getElementById(`personalDataField-${num+1}`)?.focus();
        }
    }

    @action.bound
    onPasswordKeyDown(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 13 && (e.target as HTMLElement).id) {
            const id = (e.target as HTMLElement).id;
            if (id === 'password') {
                document.getElementById(`password-confirmation`)?.focus();
            } else if (id === 'password-confirmation') {
                this.tryChangePassword();
            }
        }
    }

    @action.bound
    onPasswordPaste(e: React.ClipboardEvent<HTMLInputElement>) {
        const fieldText = e.currentTarget.value;
        const pastedText = e.clipboardData.getData('Text');
        if (
            this.appState.userInfo.isBmUser &&
            (!/^\d+$/.test(pastedText) || fieldText.length + pastedText.length > BM_PASSWORD_LENGTH)
        ) {
            e?.preventDefault();
        }
    }

    @action.bound
    onPhoneKeyDown(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 13 && this.isSendPhoneButtonActive) {
            e?.preventDefault();
            this.tryChangePhone();
        }
    }

    @action.bound
    onEmailKeyDown(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 13 && this.isSendEmailButtonActive) {
            e?.preventDefault();
            this.tryChangeEmail();
        }
    }

    // endregion

    // region ---- save handlers
    @action
    tryChangeUserFields(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.userFieldsError = {};

        if (this.validateUserFields()) {
            const fields = this.buildUserFields(this.appState.siteConfig.userFields);
            this.changeUserFields(fields!);
        }
    }

    @action
    validateUserFields(): boolean {
        const userFields = this.appState.siteConfig.userFields!;
        let isValid = true;
        let scrolled = false;

        for (let field of userFields) {
            if (!field.name) {
                console.error('Unable to process user field: ', field);
                continue;
            }
            const name = field.name;
            const ref = this.appState.getRef(name) as HTMLInputElement;
            const refValue = ref ? this.appState.registerForm.parseUserFieldValue(field, ref.value) : '';
            if (field.type === UserFieldType.DATE && ((refValue === null && ref.value) || (refValue && !isValidDate(new Date(Number(refValue)))))) {
                this.userFieldsError[name] = this.appState.t('error-' + name + '-invalid');
                isValid = false;
                if (!scrolled && this.appState.isMobileOnly) {
                    console.log('scroll')
                    document.getElementById((ref as any)?.props?.id)?.scrollIntoView();
                    window.scrollBy(0, -50);
                    scrolled = true;
                }
            }

            if (field.required) {
                if (!refValue) {
                    this.userFieldsError[name] = this.appState.t('error-' + name + '-required');
                    isValid = false;
                    if (!scrolled && this.appState.isMobileOnly) {
                        document.getElementById(ref.id)?.scrollIntoView();
                        window.scrollBy(0, -50);
                        scrolled = true;
                    }
                }
            }
        }

        return isValid;
    }

    @action
    changeUserFields(fields: { [k: string]: users.RegisterRequest.IFieldValue }) {
        this.isUserFieldsLoading = true;
        this.appState.api.changeUserFields(fields, this.onChangeUserFields);
    }

    @action
    onChangeUserFields(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isUserFieldsLoading = false;
        this.appState.processApi(msg, act);

        if (act.changeFields) {
            if (act.changeFields.ok) {
                this.userFieldsOld = this.userFields;
                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-user-fields-change-successful'),
                    level: NotificationItemLevel.SUCCESS
                });
            } else {
                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-user-fields-change-failed'),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-user-fields-change-failed'),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    @computed
    get savePersonalDataButtonIsDisabled() {
        let isDisabled = true;
        Object.keys(this.userFields).forEach(key => {
            if (!this.userFieldsOld[key] && this.userFields[key]) {
                isDisabled = false;
            }
            if (this.userFieldsOld[key] && this.userFields[key] !== this.userFieldsOld[key]) {
                isDisabled = false;
            }
        })
        return isDisabled;
    }

    buildUserFields(userFields: site.IUserFieldDescription[] | null | undefined): RegisterCustomFields | null {
        if (userFields) {
            const result = {};
            for (let field of userFields) {
                const name = field.name!;
                const value = this.userFields[name];
                if (value !== undefined) {
                    if (field.type === UserFieldType.DATE) {
                        if (value) {
                            const date = (value as string).split('.');
                            const year = +date[2];
                            // -1, because jan === 0
                            const month = +date[1] - 1;
                            const day = +date[0];

                            result[name] = new users.RegisterRequest.FieldValue({
                                datetimeValue: new Date(Date.UTC(year, month, day)).toISOString(),
                            });
                        } else {
                            result[name] = new users.RegisterRequest.FieldValue({stringValue: ''});
                        }
                    } else if (field.type === UserFieldType.UINT) {
                        result[name] = new users.RegisterRequest.FieldValue({uintValue: value as Long});
                    } else {
                        result[name] = new users.RegisterRequest.FieldValue({stringValue: value.toString().trim()});
                    }
                }
            }
            return result;
        } else {
            return null;
        }
    }

    // endregion

    // region ---- Status icon classes
    getProfileGroupStatusIconClass(group: UserProfileGroup): string {
        switch (group) {
            case UserProfileGroup.PHONE:
                if (this.phoneError) {
                    return UserProfileGroupIconStatusClass.WARNING;
                } else if (this.showPhoneConfirmationInput && this.canShowPhoneConfirmationInput) {
                    return UserProfileGroupIconStatusClass.LOADING;
                } else if (this.appState.rawUserInfo && this.appState.rawUserInfo.phoneConfirmed && !this.appState.rawUserInfo.newPhone) {
                    return UserProfileGroupIconStatusClass.SUCCESS;
                } else {
                    return UserProfileGroupIconStatusClass.WARNING;
                }
            case UserProfileGroup.EMAIL:
                if (this.emailError) {
                    return UserProfileGroupIconStatusClass.WARNING;
                } else if (this.isShowEmailLoadingStatus()) {
                    return UserProfileGroupIconStatusClass.LOADING;
                } else if (this.appState.rawUserInfo && this.appState.rawUserInfo.emailConfirmed && !this.appState.rawUserInfo.newEmail) {
                    return UserProfileGroupIconStatusClass.SUCCESS;
                } else {
                    return UserProfileGroupIconStatusClass.WARNING;
                }
            case UserProfileGroup.USER_FIELDS:
                if (Object.keys(this.userFieldsError).length) {
                    return UserProfileGroupIconStatusClass.WARNING;
                } else if (this.isShowUserFieldsSuccessStatus()) {
                    return UserProfileGroupIconStatusClass.SUCCESS;
                } else {
                    return UserProfileGroupIconStatusClass.WARNING;
                }
            default:
                break;
        }

        return '';
    }
    // ---- endregion

    // region ---- Status icon title
    getProfileGroupStatusIconTitle(group: UserProfileGroup): string {
        switch (group) {
            case UserProfileGroup.PHONE:
                if (this.appState.rawUserInfo && this.appState.rawUserInfo.phoneConfirmed && !this.appState.rawUserInfo.newPhone) {
                    // success title
                    return this.appState.t('ui-phone-confirmed');
                } else {
                    // warning title
                    if (this.appState.rawUserInfo && (this.appState.rawUserInfo.phone || this.appState.rawUserInfo.newPhone)) {
                        return this.appState.t('ui-phone-not-confirmed');
                    } else {
                        return this.appState.t('ui-phone-not-specified');
                    }
                }
            case UserProfileGroup.EMAIL:
                if (this.appState.rawUserInfo && this.appState.rawUserInfo.emailConfirmed && !this.appState.rawUserInfo.newEmail) {
                    // success title
                    return this.appState.t('ui-email-confirmed');
                } else {
                    // warning title
                    if (this.appState.rawUserInfo && (this.appState.rawUserInfo.email || this.appState.rawUserInfo.newEmail)) {
                        return this.appState.t('ui-email-not-confirmed');
                    } else {
                        return this.appState.t('ui-email-not-specified');
                    }
                }
            case UserProfileGroup.USER_FIELDS:
                if (this.isShowUserFieldsSuccessStatus()) {
                    // success title
                    return this.appState.t('ui-personal-data-specified');
                } else {
                    // warning title
                    if (!Object.keys(this.userFields).length) {
                        return this.appState.t('ui-personal-data-not-specified');
                    } else {
                        return this.appState.t('ui-personal-data-not-complete');
                    }
                }
            default:
                break;
        }

        return '';
    }

    isShowEmailLoadingStatus() {
        const rawUserInfo = this.appState.rawUserInfo;
        return rawUserInfo && (
            (!rawUserInfo.email && !!rawUserInfo.newEmail) ||
            (rawUserInfo.email && !rawUserInfo.newEmail && !rawUserInfo.emailConfirmed) ||
            (!!rawUserInfo.email && !!rawUserInfo.newEmail));
    }

    isShowUserFieldsSuccessStatus() {
        const userInfo = this.appState.rawUserInfo!;
        const userFieldsInfo = userInfo && userInfo.fields;
        const userFields = this.appState.siteConfig.userFields!;
        if (Object.keys(this.userFields).length === userFields.length && userFieldsInfo) {
            for (let field of userFields) {
                const name = field.name!;
                if (!this.userFields[name] || !(userFieldsInfo[name] && userFieldsInfo[name][userFieldsInfo[name]['value']])) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    // ---- endregion

    @computed
    get isSendPhoneButtonActive() {
        if (this.appState.rawUserInfo?.phoneConfirmed && (this.appState.rawUserInfo?.phone || this.appState.rawUserInfo?.newPhone)) {
            const phoneParams = this.appState.rawUserInfo?.phone ? getPhoneParams(this.appState.rawUserInfo?.phone, this.appState.site.getCountries()) : undefined;
            const phoneNewParams = this.appState.rawUserInfo?.newPhone ? getPhoneParams(this.appState.rawUserInfo?.newPhone, this.appState.site.getCountries()) : undefined;
            if (phoneParams?.cleanPhoneNumber === clearPhoneNumber(this.inputPhoneNumber) && phoneParams?.phoneCountry === this.inputPhoneCountry) {
                return false;
            }
            if (phoneNewParams?.cleanPhoneNumber === clearPhoneNumber(this.inputPhoneNumber) && phoneNewParams?.phoneCountry === this.inputPhoneCountry) {
                return false;
            }
        }
        if (this.resendIntervalChangePhone) {
            return false;
        }
        return true;
    }

    @computed
    get isSendEmailButtonActive() {
        if (this.appState.rawUserInfo?.email && this.appState.rawUserInfo?.email === this.inputEmail) {
            return false;
        }
        return true;
    }

    getUserField = (fieldName?: string) => {
        if (fieldName) {
            if (fieldName === 'gender') {
                if (this.userFields[fieldName]) {
                    return this.userFields[fieldName] === 'male' ? this.appState.t("ui-male") : this.appState.t("ui-female");
                } else {
                    return '—';
                }
            }
            return this.userFields[fieldName] ? this.userFields[fieldName] : '—';
        }
        return '—';
    }

    // region ---- initial values
    @action
    setCurrentValues() {
        const userInfo = this.appState.rawUserInfo!;
        this.successPasswordChange = false;
        // email
        this.inputEmail = userInfo.email || userInfo.newEmail || '';

        // phone
        const phone = userInfo.newPhone || userInfo.phone || null;
        const phoneParams = getPhoneParams(phone, this.appState.site.getCountries());
        if (phoneParams) {
            this.inputPhoneCode = phoneParams.phoneCode;
            this.inputPhoneNumber = phoneParams.phoneNumber;
            if (phoneParams.phoneCountry) {
                this.inputPhoneCountry = phoneParams.phoneCountry;
            }
        }

        // new phone
        const newPhone = userInfo.newPhone || null;
        const newPhoneParams = getPhoneParams(newPhone, this.appState.site.getCountries());
        if (newPhoneParams) {
            this.newPhoneNumber = newPhoneParams.phoneNumber;
            if (newPhoneParams.phoneCountry) {
                this.newPhoneCountry = newPhoneParams.phoneCountry;
            }
        }

        // phone country
        if (!this.inputPhoneCountry) {
            if (userInfo && userInfo.country) {
                this.inputPhoneCountry = userInfo.country;
            } else {
                this.inputPhoneCountry = this.appState.siteConfig.countries!.defaultCountry!;
            }
        }

        // phone code
        if (!this.inputPhoneCode) {
            const autoPhoneCode = getPhoneCodeByCountry(this.inputPhoneCountry, this.appState.siteConfig.countries!);
            if (autoPhoneCode) {
                this.inputPhoneCode = autoPhoneCode.toString();
            } else if (this.appState.site.getCountries().length) {
                this.inputPhoneCode = this.appState.site.getCountries()[0].phoneCode!.toString();
            }
        }

        // phone confirmation input
        this.showPhoneConfirmationInput = this.canShowPhoneConfirmationInput;

        // user fields
        const userFieldsInfo = userInfo && userInfo.fields;
        if (userFieldsInfo) {
            const userFields = this.appState.siteConfig.userFields!;

            for (let field of userFields) {
                const name = field.name!;
                if (userFieldsInfo[name] && userFieldsInfo[name][userFieldsInfo[name]['value']] && field.type) {
                    if (field.type === UserFieldType.DATE) {
                        const selectedDate = new Date(userFieldsInfo[name][userFieldsInfo[name]['value']]);
                        this.userFields[name] = formatInputDate(selectedDate);
                    } else {
                        this.userFields[name] = userFieldsInfo[name][userFieldsInfo[name]['value']];
                    }
                }
            }
            this.userFieldsOld = this.userFields;

        }
    }

    // endregion

    // region ---- common
    @action
    resetFormErrors() {
        this.emailError = null;
        this.passwordError = null;
        this.passwordConfirmationError = null;
        this.phoneError = null;
        this.phoneConfirmationCodeError = null;
        this.userFieldsError = {};

    }
    // ---- endregion
}
