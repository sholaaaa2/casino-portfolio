import {action, observable} from 'mobx';
import {FUN_CURRENCY_ID} from '../utils/Currencies';
import AppState from '../AppState';

export default class LiveCasinoActions {
    @observable isLoading: boolean = false;
    scriptLoaded: boolean = false;

    constructor(private appState: AppState) {
    }

    @action
    setIsLoading(value: boolean) {

        this.isLoading = value;
    }

    isCurrencySupported() {
        // fun isn't supported
        const currency = this.appState.user.getActiveBalanceCurrencyId();
        return currency && currency.notEquals(FUN_CURRENCY_ID);
    }
}
