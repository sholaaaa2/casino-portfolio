import {action, computed, observable} from 'mobx';
import Long from 'long';
import {bonuses, commons} from '../../../api/proto';
import AppState from '../../AppState';
import {BonusType} from '../../../api/utils';
import {formatCurrencyAmount, formatCurrencyCode, normalizeCurrency} from '../../utils/Money';
import { format } from 'date-fns';
import { NLong } from '../../meta';

export enum TimelineCashbackLevelId {
    CL_BRONZE = 'CL_BRONZE',
    CL_SILVER = 'CL_SILVER',
    CL_GOLD = 'CL_GOLD',
    CL_DIAMOND = 'CL_DIAMOND',
    CL_IMPERIAL = 'CL_IMPERIAL',
    CL_IRON = 'CL_IRON',
    CL_PLATINUM = 'CL_PLATINUM',
    CL_RUBY = 'CL_RUBY',
}

interface ITimelineCashbackLevel {
    minPoints?: number;
    maxPoints?: number;
    percent?: number;
    id?: TimelineCashbackLevelId;
}

interface ITimelineCashbackSettings {
    levels?: ITimelineCashbackLevel[];
    currentLevelIndex?: number;
    currentCashback?: string;
    endTime?: string;
    currentCashbackPercent?: number;
    currentPoints?: number;
    progressPercent?: number;
    currency?: string;
    pointsAccumulating?: boolean;
    accumulationBlockingBonusesNames?: string[];
}

export default class TimelineCashbackBonus {
    @observable settings?: ITimelineCashbackSettings;

    constructor(public appState: AppState) {}

    @computed
    get visibleOnMainPage() {
        return this.appState.appSettings.timelineCashbackVisibility
            ? (this.appState.appSettings.timelineCashbackVisibility === 'all'
              || this.appState.appSettings.timelineCashbackVisibility === 'main-page-only')
            : false;
    }

    @computed
    get visibleOnProfileBonusesPage() {
        return this.appState.appSettings.timelineCashbackVisibility
            ? this.appState.appSettings.timelineCashbackVisibility === 'all'
              || this.appState.appSettings.timelineCashbackVisibility === 'profile-only'
            : false;
    }

    @action
    processPossibleBonuses = (
        possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[],
    ) => {
        const timelineCashbackBonus = possibleBonuses.find(bonus => {
            if (bonus.meta?.bonus_type! === BonusType.TIMELINE_CASHBACK && bonus.meta?.enabled === "true") return true;
            return false;
        })

        if (timelineCashbackBonus) {
            setTimeout(() => this.processBonus(timelineCashbackBonus), 0)
        }
    };

    processBonus(timelineCashbackBonus: bonuses.UserBonusesListResponse.IPossibleBonusCode) {
        if (timelineCashbackBonus?.meta?.ui_state) {
            let settings = JSON.parse(timelineCashbackBonus.meta.ui_state);
            const levels = settings?.levels?.length ?
              settings.levels.map((level: ITimelineCashbackLevel) => {
                  return {
                      minPoints: level.minPoints ? Number(level.minPoints) : 0,
                      maxPoints: level.maxPoints ? Number(level.maxPoints) : 0,
                      percent: Number(level.percent || "0"),
                      id: level.id
                  }
              })
              : undefined;
            const currency = this.appState.userInfo.guest
               ? timelineCashbackBonus.meta.currency ? Long.fromNumber(Number(timelineCashbackBonus.meta.currency)) : this.appState.userInfo.currency
               : this.appState.userInfo.currency;
            const timeFormat = this.appState.appSettings.timelineCashbackCustomTimeFormat ? 'eee dd/LL h:mm a' : 'dd.MM.yyyy HH:mm';
            const pointsAccumulating = !settings?.accumulationBlockingBonuses?.length && !this.appState.userInfo.guest;
            let accumulationBlockingBonusesNames: string[] = [];
            if (settings?.accumulationBlockingBonuses?.length) {
                settings.accumulationBlockingBonuses.forEach((bonusId: number) => {
                    const bonus = this.appState.userBonusesStore.codes?.find((bonus) => bonus.id?.toNumber() === bonusId);
                    if (bonus?.meta?.bonus_title && !accumulationBlockingBonusesNames.includes(bonus.meta.bonus_title)) {
                        accumulationBlockingBonusesNames.push(bonus.meta.bonus_title);
                    }
                })
            }
            
            this.settings = {
               levels,
               currentLevelIndex: settings.currentIndex !== undefined && settings.currentIndex !== null ? Number(settings.currentIndex) : undefined,
               currentCashback: settings.currentCashback ?
                   this.getCashbackCurrencyAmount(
                       Long.fromNumber(Number(settings.currentCashback) || 0),
                       currency!
                   )
                   : undefined,
               endTime: settings.endTime && Number(settings.endTime) ? format(Number(settings.endTime), timeFormat) : '-',
               currentPoints: settings.currentPoints ? Number(settings.currentPoints) : 0,
               currentCashbackPercent: settings.currentIndex && levels ? levels[Number(settings.currentIndex) || 0].percent : 0,
               progressPercent: settings.currentPoints && settings.currentIndex !== undefined && levels
                  ? this.processProgressPercent(levels[Number(settings.currentIndex) || 0], Number(settings.currentPoints))
                  : 0,
               currency: formatCurrencyCode(currency!),
               pointsAccumulating,
               accumulationBlockingBonusesNames,
            };
        }
    }

    getCashbackCurrencyAmount = (amount: Long, currency: NLong | commons.ICurrency) => {
        const normilizedCurrency = normalizeCurrency(currency);
        if (this.appState.appSettings.timelineCashbackCurrentCashbackCurrencyCustomFormat && normilizedCurrency?.id.eq(840)) {
            return `${formatCurrencyAmount(amount, currency, true)} $B`;
        }
        return formatCurrencyAmount(amount, currency);
    }

    processProgressPercent = (level: ITimelineCashbackLevel, currentPoints: number) => {
        if (!currentPoints) return 0;
        if (level.maxPoints && level.minPoints !== undefined) {
            const availablePointsInLevel = level.maxPoints - level.minPoints;
            const progress = Math.round((currentPoints - level.minPoints) * 100 / availablePointsInLevel);
            return progress && progress > 0 ? progress : 0;
        } else if (level.minPoints && !level.maxPoints) {
            return 100;
        }
        return 0;
    }

    @computed
    get pointsAccumulatingStatusTitle() {
        if (this.settings?.pointsAccumulating) {
            return this.appState.t('your-cashback-successfully-credited');
        } else if (this.settings?.accumulationBlockingBonusesNames?.length) {
            return this.appState.t('reason-bonuses-wagering')
        } else if (this.appState.userInfo.guest) {
            return this.appState.t('auth-for-get-cashback')
        }
        return '';
    }

    @computed
    get levelsForCashbackProgress() {
        if (this.settings?.levels) {
            return this.settings.levels.map((level, i) => ({
                ...level,
                isComplete: this.settings?.currentLevelIndex !== undefined && this.settings.currentLevelIndex > i,
                isCurrent: this.settings?.currentLevelIndex !== undefined && this.settings.currentLevelIndex === i,
            }))
        }
        return []
    }

    @computed
    get maxPointsInLevel() {
        if (this.settings?.currentLevelIndex !== undefined && this.settings?.levels && this.settings.levels[this.settings?.currentLevelIndex]) {
            return this.settings.levels[this.settings.currentLevelIndex].maxPoints ?
                this.settings.levels[this.settings.currentLevelIndex].maxPoints
                : 0
        }
        return 0
    }

    @computed
    get progressPercent() {
        if (this.settings?.progressPercent) {
            return `${this.settings?.progressPercent}%`;
        }
        return '0%';
    }

    @computed
    get currentPointsInLevel() {
        if (this.currentLevel?.minPoints !== undefined && this.settings?.currentPoints) {
            const currentPoints = this.settings.currentPoints >= this.currentLevel.minPoints ? this.settings.currentPoints - this.currentLevel.minPoints : 0;
            return currentPoints;
        }
        return this.settings?.currentPoints ? this.settings?.currentPoints : 0;
    }

    @computed
    get availablePointsInLevel() {
        if (this.currentLevel?.maxPoints && this.currentLevel?.minPoints !== undefined) {
            return this.currentLevel.maxPoints - this.currentLevel.minPoints;
        }
        return undefined;
    }

    @computed
    get circleProgressDeg() {
        if (this.settings?.progressPercent !== undefined) {
            return (360 * this.settings.progressPercent / 100) + 180;
        }
        return 0;
    }

    @computed
    get circleProgressRightStyle() {
        return `rotate(${String(this.circleProgressDeg)}deg)`;
    }

    @computed
    get circleProgressClass() {
        if (this.settings?.progressPercent! >= 50) {
            return 'over_50';
        }
        return '';
    }

    @computed
    get circleImageSrc() {
        if (this.cashbackLevelType) {
            return `/images/timeline-cashback/${this.cashbackLevelType}.png`;
        }
        return '';
    }

    @computed
    get cashbackPageLevelTypeImageSrc() {
        if (this.cashbackLevelType) {
            return `/images/timeline-cashback/${this.cashbackLevelType}-2.png`;
        }
        return '';
    }

    @computed
    get cashbackLevelType() {
        if (this.currentLevel?.id) {
            return this.currentLevel.id.toLowerCase().substring(3);
        }
        return 'bronze';
    }

    @computed
    get currentLevel() {
        if (this.settings?.currentLevelIndex !== undefined && this.settings.levels![this.settings.currentLevelIndex]) {
            return this.settings.levels![this.settings.currentLevelIndex];
        }
        return undefined;
    }

    @computed
    get currentLevelPercent() {
        if (this.currentLevel?.percent) {
            return this.currentLevel.percent;
        }
        return undefined;
    }
}
