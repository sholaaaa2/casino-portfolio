import {action, computed, observable} from 'mobx';
import AppState from '../../AppState';
import {
    BonusCodeNotificationFreeBets,
    BonusCodeNotificationFreeSpins,
    BonusCodeNotificationFunds,
} from '../BonusesActions';
import {NotificationAnimationClass, NString, Routes} from '../../meta';
import {BonusMeta, BonusType, processBoolean} from '../../../api/utils';
import {UsaRefInviteByCashierBonusMeta} from '../BonusAppearance';
import {NotificationItemLevel} from '../../components/Notificator';
import React from 'react';
import {bonuses} from '../../../api/proto';
import getBonusImageId from '../../utils/getBonusImageId';

export class BonusCodeNotification {
    appState: AppState;

    // common props
    notificationId: string;
    title: NString;
    message: string;
    bonusCodeInfo: bonuses.IBonusCodeInfo;

    // visual props
    canActivate: boolean;
    image: string;
    animationClass = NotificationAnimationClass.QUEUED;
    gradientClass: string;

    // bonus props
    fundsBonus?: BonusCodeNotificationFunds;
    freeSpinsBonuses?: BonusCodeNotificationFreeSpins[];
    freeBetsBonuses?: BonusCodeNotificationFreeBets[];
    cashierInviteMeta?: UsaRefInviteByCashierBonusMeta;

    @observable isAddToMyBonusesClicked = false;
    @observable isAccepted = false;

    constructor(
        params: Pick<
            BonusCodeNotification,
            | 'appState'
            | 'bonusCodeInfo'
            | 'notificationId'
            | 'title'
            | 'message'
            | 'image'
            | 'gradientClass'
            | 'cashierInviteMeta'
        >,
    ) {
        Object.assign(this, params);
        this.canActivate = processBoolean(
            BonusMeta.RECEIVE_NOTIFICATION_CAN_ACTIVATE,
            params.bonusCodeInfo.meta,
            true,
        );
    }

    @computed
    get bonusCodeMeta(): Record<string, string> {
        return this.bonusCodeInfo.meta ?? {};
    }

    @computed
    get type(): BonusType | undefined {
        const bonusType = this.bonusCodeMeta.bonus_type ?? undefined;
        if (Object.values(BonusType).includes(bonusType as BonusType)) {
            return bonusType as BonusType;
        }
        console.error('unknown bonus type:', this.bonusCodeInfo);
        return undefined;
    }

    @computed
    get bonusImageId(): string | number | undefined {
        if (
            this.type === BonusType.MANUAL_ACTIVATION_REGULAR_TOURNAMENT ||
            this.type === BonusType.REGULAR_TOURNAMENT
        ) {
            return getBonusImageId(this.bonusId);
        }
        return undefined;
    }

    @computed
    get isSingleGameInFreeSpins(): boolean {
        return this.freeSpinsBonuses?.length === 1;
    }

    @computed
    get isOpenGameAfterAccept(): boolean {
        return this.isSingleGameInFreeSpins && this.canActivate;
    }

    @computed
    get bonusId(): string {
        return this.bonusCodeInfo.bonusTypeId ?? '';
    }

    @computed
    get code(): string {
        return this.bonusCodeInfo.code ?? '';
    }

    @computed
    get isLinkToBonusRulesShown(): boolean {
        return !this.appState.appSettings.hideBonusPolicyInNotification;
    }

    @computed
    get isDefault(): boolean {
        return this.type
            ? ![
                  BonusType.USA_REF_INVITEE_BY_CASHIER,
                  BonusType.USA_REF_INVITEE,
                  BonusType.USA_REF_INVITER,
                  BonusType.NO_DEPOSIT_BY_CODE_BALANCE_WAGER,
                  BonusType.APK_BONUS,
                  BonusType.EXTERNAL_PROMO,
              ].includes(this.type)
            : true;
    }

    @computed
    get shouldPersistAfterAccept(): boolean {
        return this.type ? (this.isDefault && (this.freeSpinsBonuses?.length ?? 0) > 1) || this.type === BonusType.TIMELINE_CASHBACK : false;
    }

    @computed
    get isGoToGameButtonShownInFreespins(): boolean {
        return !this.canActivate && this.type !== BonusType.APK_BONUS;
    }

    @action.bound
    addToMyBonuses() {
        this.appState.notificator!.addNotification({
            level: NotificationItemLevel.SUCCESS,
            message: this.appState.t('added-to-bonuses-successfully'),
            lifetime: 1,
        });
        this.isAddToMyBonusesClicked = true;
    }

    @action.bound
    goToMyBonuses(e: React.MouseEvent<HTMLButtonElement>) {
        this.appState.bonuses.refuseBonus(e);
        this.appState.router.push(this.appState.l('/profile/bonuses'));
    }

    @action.bound
    openGame(gameId: string) {
        this.appState.router.push(this.appState.l(Routes.GAME.replace(':id', gameId)));
    }

    @action.bound
    onAcceptSuccess() {
        this.isAccepted = true;
        if (this.isDefault && this.freeSpinsBonuses?.length === 1) {
            this.openGame(this.freeSpinsBonuses[0].gameId);
        }
    }

    @action.bound
    close(e: React.MouseEvent<HTMLElement>) {
        if (this.isAccepted) {
            this.appState.bonuses.hideNotification(this.notificationId);
        } else {
            this.appState.bonuses.refuseBonus(e);
        }
    }
}
