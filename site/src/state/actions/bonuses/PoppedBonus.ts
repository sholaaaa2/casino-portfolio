import {action, computed} from 'mobx';
import Long from 'long';
import {bonuses, commons} from '../../../api/proto';
import AppState from '../../AppState';
import {SitePages, NLong} from '../../meta';
import {BonusType} from '../../../api/utils';
import {DisplayedBonus} from '../UserBonusesStore';
import {getGameImage} from '../../../state/utils/GameUtils';
import {formatCurrencyAmount} from '../../utils/Money';
import {IDict, IDictValue} from '../../utils/dict';
import {isAndroid} from '../../utils/BrowserUtils';

export const POPUP_BONUS_TYPES: string[] = [BonusType.APK_ANDROID_BONUS];

interface BonusFreeSpin {
    gameName: string;
    gameImage: string;
    spinsCount: number;
    line?: number;
    bet?: number;
    gameId: string;
}

export default class PoppedBonus {
    id?: string | null = null;
    meta?: IDict | null = null;
    popup: IDict & {fs: IDict[]} | null = null;
    freeSpins: BonusFreeSpin[] = [];
    amountFormatted: string | null = null;

    get title(): string {
        return this.meta && this.meta.bonus_title ? this.meta.bonus_title.toString() : '';
    }

    get bonusCode() {
        return this.id ? this.appState.userBonusesStore.findBonusCode(this.id) : null;
    }

    get desc(): string {
        if (!this.popup) {
            return '';
        }
        return this.appState.isApk ? this.popup.apk_desc.toString() : this.popup.desc.toString();
    }

    get apkLink(): string {
        if (!this.appState.isApk && this.popup && this.popup.apk_link) {
            return this.popup.apk_link.toString();
        }
        if (!this.appState.isApk && this.appState.siteConfig?.apps?.length) {
            return this.appState.siteConfig?.apps[0];
        }
        return '';
    }

    constructor(
        public appState: AppState,
        bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode,
    ) {
        try {
            this.id = bonus.id;
            this.meta = bonus.meta;
            if (bonus.meta && bonus.meta.popup) {
                this.popup = JSON.parse(bonus.meta.popup);
            }
            if (this.popup && this.popup.fs) {
                this.freeSpins = this.popup.fs.map((fs) => ({
                    gameName: this.appState.games.getGameName(fs.game.toString()),
                    gameImage: getGameImage(fs.game.toString()) || '',
                    spinsCount: parseInt(fs.cnt.toString(), 10),
                    bet: parseInt(fs.bet.toString(), 10),
                    betFormatted: this.format(fs.bet, this.popup ? this.popup.currency : undefined),
                    line: parseInt(fs.lines.toString(), 10),
                    gameId: fs.game.toString(),
                }));
            }
            if (this.popup && this.popup.possible_amount) {
                this.amountFormatted = this.format(this.popup.possible_amount, this.popup.currency);
            }
        } catch (e) {
            console.log(e);
        }
    }

    format = (val: IDictValue, currencyId?: IDictValue): string => {
        let currencyOrCurrencyId: NLong | commons.ICurrency = null;
        if (currencyId) {
            currencyOrCurrencyId = Long.fromNumber(typeof currencyId == 'number' ? currencyId : parseFloat(currencyId.toString()));
        }
        if (!currencyOrCurrencyId && this.popup?.currency) {
            currencyOrCurrencyId = Long.fromNumber(typeof this.popup?.currency == 'number' ? this.popup?.currency : parseFloat(this.popup?.currency.toString()));
        }
        if (!currencyOrCurrencyId) {
            currencyOrCurrencyId = this.appState.userInfo.currency;
        }
        if (this.appState.userInfo.guest && this.bonusCode) {
            currencyOrCurrencyId = this.bonusCode.currency;
        }
        return formatCurrencyAmount(
            Long.fromNumber(typeof val == 'number' ? val : parseFloat(val.toString())),
            currencyOrCurrencyId,
        );
    };

    get subType(): string {
        return this.meta && this.meta.bonus_subtype ? this.meta.bonus_subtype.toString() : '';
    }

    get bonusPopupDelay() {
        const delay = this.popup && this.popup.delay_on_close_modal_in_hours ? parseFloat(this.popup.delay_on_close_modal_in_hours.toString()) : 2;
        return delay * 60 * 60 * 1000;
    }

    get currentBonusToken(): string {
        return 'bonus-popup-close-time-' + this.id;
    }

    @computed
    get popupCloseTime(): number {
        let closeTimeStr: string | null = null;
        try {
            closeTimeStr = window.localStorage.getItem(this.currentBonusToken);
        } catch (e) {
            console.log(e);
        }
        return closeTimeStr ? parseFloat(closeTimeStr) : 0;
    }

    setPopupCloseTime = () => {
        window.localStorage.setItem(this.currentBonusToken, new Date().getTime().toString());
    };

    get isAvailable() {
        return (
            this.popupCloseTime === 0 ||
            this.bonusPopupDelay + this.popupCloseTime < new Date().getTime()
        );
    }

    @action
    closeModal = (e?: React.MouseEvent<HTMLElement>) => {
        this.setPopupCloseTime();
        this.appState.modal.hideModal(e);
        this.appState.poppedBonus.runReOpenTimeout();
    };
}

export class PoppedBonusProcessor {
    list: PoppedBonus[] = [];
    currentBonus?: PoppedBonus | null = null;

    checkAvailability = (bonus: PoppedBonus) => {
        // visibility conditions of popup
        if (!bonus.popup || !bonus.isAvailable || Object.keys(bonus.popup).length === 0) {
            return false;
        }
        if (bonus.meta && bonus.meta.bonus_mod === BonusType.APK_ANDROID_BONUS &&
            (!isAndroid() || (this.appState.isApk && this.appState.userInfo.guest))) {
            return false;
        }
        return true;
    };

    constructor(public appState: AppState) {}

    @action
    processPossibleBonuses = (
        possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[],
    ) => {
        this.list = possibleBonuses.filter(
            (bonus) =>
                bonus.id &&
                bonus.meta &&
                bonus.meta.enabled === 'true' &&
                bonus.meta.popup,
        ).map((b) => new PoppedBonus(this.appState, b)).filter(
            (bonus) =>
                this.checkAvailability(bonus)
        );

        const availableBonus = this.list.find((b) => b.popup);

        if (availableBonus) {
            this.currentBonus = availableBonus;
            this.showModal(availableBonus);
        }
    };

    @action
    showModal = (bonus: PoppedBonus) => {
        if (this.appState.pages.isPageWithEmbeddedContent || this.appState.page === SitePages.PROFILE_BONUSES) {
            return;
        }
        // kind of popup
        if (bonus.meta && bonus.meta.bonus_mod === BonusType.APK_ANDROID_BONUS) {
            this.appState.modal.showApkAndroidBonusModal();
        }
        if (bonus.meta && bonus.meta.bonus_subtype && bonus.meta.bonus_subtype === BonusType.BONUS_BY_CODE_WITH_REQ_DEPS) {
            this.appState.modal.showBonusByCodeWithReqDepsModal();
        }
    };

    @action
    closeModal = (e?: React.MouseEvent<HTMLElement>) => {
        this.currentBonus && this.currentBonus.setPopupCloseTime();
        this.appState.modal.hideModal(e);
        this.runReOpenTimeout();
    };

    @action
    handleOpenProfile = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();

        window.location.href = this.appState.l('/profile/bonuses');
    };

    @action
    handleOpenRules = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();

        const entryId = e.currentTarget.getAttribute('data-entry-id');
        const bonusesPageEntry = this.appState.userBonusesStore.displayedBonuses!.find(
            (b) => b.id === entryId,
        ) as DisplayedBonus;

        this.appState.modal.hideModal();
        this.appState.page = SitePages.BONUSES;
        this.appState.site.onPageOpen(SitePages.BONUSES);

        setTimeout(() => {
            bonusesPageEntry && bonusesPageEntry.toggle();
            const els = window.document.getElementsByClassName(`bonuses-page-block--${entryId}`);
            const el = els.length ? (els[0] as HTMLElement) : null;
            if (el) {
                window.scrollTo(0, el.offsetTop + 216);
            }
        }, 200);
    };

    @action
    handleGameClick = () => {
        this.appState.modal.hideModal();
    };

    reOpenTimeout: NodeJS.Timeout | null = null;

    @action
    runReOpenTimeout = () => {
        if (this.reOpenTimeout) {
            clearTimeout(this.reOpenTimeout);
        }

        const delay = this.currentBonus ? this.currentBonus.bonusPopupDelay : 2 * 60 * 60 * 1000;

        this.reOpenTimeout = setTimeout(() => {
            this.reOpenTimeout = null;
            this.appState.userBonusesStore.fetchBonuses();
        }, delay);
    };
}
