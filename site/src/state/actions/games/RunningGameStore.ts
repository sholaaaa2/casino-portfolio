import {GameItem} from '../GamesActions';
import {computed, observable} from 'mobx';
import {Size} from '../../utils/BrowserUtils';
import AppState from '../../AppState';

export default class RunningGameStore {
    @observable runningGame: GameItem | null = null;
    @observable rawHTML: string | null = null;
    @observable iframe: string | null = null;
    @observable gameScreenSize: Size | null = null;

    constructor(private appState: AppState) {}

    @computed
    get gameBlockClassName() {
        const classNames = [];
        if (this.appState.isMobile) {
            classNames.push('block-game-mobile');
        }
        if (!!this.appState.appSettings.guestGamePage && this.appState.userInfo.guest) {
            classNames.push('fun-game-page');
        }
        if (this.runningGame?.provider) {
            classNames.push(`block-game--${this.runningGame.provider}`);
        }
        return classNames.join(' ');
    }
}
