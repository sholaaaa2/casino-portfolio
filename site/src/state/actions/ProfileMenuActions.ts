import AppState from '../AppState';
import {action, computed, observable} from 'mobx';
import { SitePages } from '../meta';

export interface ProfileMenuItemData {
    id?: string | null;
    name: string;
    href: string;
    page?: SitePages;
    hasWarnSign?: boolean;
    onClick?: () => void;
    iconPath?: string;
}

export class ProfileMenuActions {
    appState: AppState;

    @observable profileMenuIsOpened: boolean = true;

    constructor(state: AppState) {
        this.appState = state;

        this.toggleMenuOpen = this.toggleMenuOpen.bind(this);
    }

    @action
    toggleMenuOpen(event: MouseEvent) {
        this.profileMenuIsOpened = !this.profileMenuIsOpened;

        const navHeader = event.currentTarget as HTMLElement;

        const navMenu = navHeader.nextElementSibling as HTMLElement;

        const isCollapsible = navMenu && navMenu.classList.contains('collapse');

        if (!isCollapsible) { return; }

        const nextHeight = this.profileMenuIsOpened ? navMenu.scrollHeight : 0;

        navMenu.style.height = `${nextHeight}px`;
    }

    @computed
    get profileContentIsVisible() {
        if (this.appState.isMobileOnly) {
            return !this.profileMenuIsOpened;
        }
        return true;
    }

    @computed
    get profileMenuItems(): ProfileMenuItemData[] {
        const items = [];
        if (this.appState.site.isProfileEditAllowed) {
            items.push(
                {
                    name: this.appState.t('ui-edit-profile'),
                    href: '/profile/edit',
                    page: SitePages.PROFILE_EDIT,
                    iconPath: '/images/icons/profile-edit.svg',
                }
            );
        }
        if (this.appState.site.isAffiliateProgramAllowed) {
            items.push(
                {name: this.appState.t('ui-affiliate-program'), href: '/profile/affiliate-program', page: SitePages.AFFILIATE_PROGRAM, iconPath: "/images/icons/profile-ref-system.svg"}
            );
        }
        if (this.appState.site.isRefSystemAllowed) {
            items.push(
                {name: this.appState.t('ui-ref-system'), href: `/${SitePages.PROFILE_REF_SYSTEM}`, page: SitePages.PROFILE_REF_SYSTEM, iconPath: "/images/icons/profile-ref-system-2.svg"}
            );
        }
        if (this.appState.site.isProfileBonusesAllowed) {
            items.push(
                {name: this.appState.t('ui-my-bonuses'), href: '/profile/bonuses', page: SitePages.PROFILE_BONUSES, iconPath: "/images/icons/profile-my-bonuses.svg"}
            );
        }
        if (this.appState.site.isProfileBonusesAllowed && this.appState.appSettings.showBonusBalances) {
            items.push({
                name: this.appState.t('ui-bonus-balances'),
                href: '/profile/bonus-balances',
                page: SitePages.PROFILE_BONUS_BALANCES,
                iconPath: "/images/icons/profile-bonus-balances.svg"
            });
        }
        if (this.appState.site.isProfileEditAllowed) {
            items.push({
                name: this.appState.t("ui-profile-documents"),
                href: "/profile/documents",
                page: SitePages.PROFILE_DOCUMENTS,
                hasWarnSign: this.appState.userInfo.restrictions.length > 0,
                iconPath: "/images/icons/profile-documents-verification.svg"
            });
        }

        // check if profile is enabled at all
        if (items.length && this.appState.logoutAvailable) {
            items.push(
                {name: this.appState.t('ui-logout'), href: '/', page: SitePages.MAIN, onClick: this.appState.user.tryLogout, iconPath: '/images/icons/exit-profile.svg'}
            );
        }

        return items;
    }

    @action.bound
    openSection(item: ProfileMenuItemData): void {
        this.profileMenuIsOpened = false;
        if (item.onClick) {
            item.onClick();
        }
        this.appState.router.push(this.appState.l(item.href));
    }
}
