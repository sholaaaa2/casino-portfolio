import AppState from '../AppState';
import {isDef} from '../utils/Common';
import {BonusType} from '../meta';
import { users } from '../../api/proto';

export const NOTIFICATION_CARDS = ["first-card", "second-card", "third-card"] as const;
export type SELECTED_CARD = typeof NOTIFICATION_CARDS[number];

export interface UsaRefInviteByCashierBonusMeta extends users.invitations.IInvitationMessageMeta {
    className: string;
}

export type BonusMeta = { [k: string]: string };

export enum BonusMetaField {
    BONUS_TYPE = 'bonus_type',

    ENABLED = 'enabled',

    // skill-wheel fields
    SKILL_WHEEL_SPINS_REMAINING = 'skillwheel_spins_remaining',
    SKILL_WHEEL_TIME_FOR_SPIN = 'skillwheel_time_for_spin',
    SKILL_WHEEL_PRIZE_WON_AMOUNT = 'skillwheel_price_won_amount',
    SKILL_WHEEL_STATE = 'skillwheel_state',
    SKILL_WHEEL_VALUES = 'skillwheel_values',
    SKILL_WHEEL_DISABLED = 'disabled',

    RECEIVE_NOTIFICATION_CAN_ACTIVATE = 'receive_notification_can_activate',

    // deprecated
    BONUS_TITLE = 'bonus_title',
    NEW_BONUS_META = 'new_bonus_meta',
    AVAILABILITY_NOTIFICATION_TITLE = 'availability_notification_title',
    RECEIVE_NOTIFICATION_TITLE_PARAMS = 'receive_notification_title_params',
    RECEIVE_NOTIFICATION_TITLE = 'receive_notification_title',
    RECEIVE_NOTIFICATION_SUBTITLE = 'receive_notification_subtitle',
    RECEIVE_NOTIFICATION_SUBTITLE_PARAMS = 'receive_notification_subtitle_params',
    AVAILABILITY_NOTIFICATION_SUBTITLE = 'availability_notification_subtitle',
    AVAILABILITY_NOTIFICATION_SUBTITLE_PARAMS = 'availability_notification_subtitle_params',
    BONUS_TITLE_PARAMS = 'bonus_title_params',
    BONUS_SUBTITLE_PARAMS = 'bonus_subtitle_params',
    BONUS_SUBTITLE = 'bonus_subtitle',
    BONUS_IMAGE = 'bonus_image',
    BONUS_GRADIENT = 'bonus_gradient',
}

export class BonusAppearance {
    appState: AppState;
    title?: string;
    subTitle?: string;
    message?: string;
    image: string;
    gradient: string;
    canActivate: boolean;
    canShow?: boolean;
    isEnabled: boolean;
    runConfettiAnimation?: boolean;
    bonusesSchedule: object[] = [];
    type?: BonusType;

    constructor(appState: AppState, meta: BonusMeta | null | undefined) {
        this.appState = appState;

        // default settings
        this.title = this.appState.t('ui-bonus-card-default-title');
        this.image = '/images/bonuses/coins.svg';
        this.gradient = 'card-popup-bonus--gradient-coins';
        this.canActivate = true;
        this.canShow = true;
        this.runConfettiAnimation = true;
        this.isEnabled = true;

        // set additional settings
        if (meta) {
            this.setLegacySettings(meta);
        }
    }

    setLegacySettings(meta: BonusMeta) {
        // process common settings
        this.title = this.processText(BonusMetaField.BONUS_TITLE, BonusMetaField.BONUS_TITLE_PARAMS, meta, this.title);
        this.subTitle = this.processText(BonusMetaField.BONUS_SUBTITLE, BonusMetaField.BONUS_SUBTITLE_PARAMS, meta, this.subTitle);
        this.gradient = this.processString(BonusMetaField.BONUS_GRADIENT, meta, this.gradient);
        this.image = this.processString(BonusMetaField.BONUS_IMAGE, meta, this.image);
        this.title = this.processText(BonusMetaField.RECEIVE_NOTIFICATION_TITLE, BonusMetaField.RECEIVE_NOTIFICATION_TITLE_PARAMS, meta, this.title);
        this.subTitle = this.processText(BonusMetaField.RECEIVE_NOTIFICATION_SUBTITLE, BonusMetaField.RECEIVE_NOTIFICATION_SUBTITLE_PARAMS, meta, this.subTitle);
        this.canActivate = this.processBoolean(BonusMetaField.RECEIVE_NOTIFICATION_CAN_ACTIVATE, meta);
        const bonusType = this.processString(BonusMetaField.BONUS_TYPE, meta, '');
        this.type = bonusType ? bonusType as BonusType : undefined;
    }

    processText(valueKey: string, paramsKey: string, settings: BonusMeta, defaultVal: string | undefined): string | undefined {
        if (isDef(settings[valueKey])) {
            const params = settings[paramsKey] ? settings[paramsKey].split('|') : '';
            return this.appState.t(settings[valueKey]!, params);
        } else {
            return defaultVal;
        }
    }

    processString(key: string, settings: BonusMeta, defaultVal: string): string {
        return isDef(settings[key]) ? settings[key] : defaultVal;
    }

    processBoolean(key: string, settings: BonusMeta) {
        return settings[key] === 'true';
    }
}
