import AppState from "../AppState";
import {action, observable} from "mobx";
import {bonuses, messages} from "../../api/proto";
import {SitePages} from "../meta";
import React from "react";
import {NotificationItemLevel} from "../components/Notificator";
import {DisplayedWagerBonus} from "./UserBonusesStore";
import {BonusMeta, getBonusCancelErrorToken, processBoolean, processString} from "../../api/utils";
import {formatCurrencyAmount, formatPercent} from "../utils/Money";
import {calcPercent} from "../utils/Math";
import {CaseType} from "./WagerBonusProcessor";
import Long from 'long';

interface WagerBonusEntry {
    id: string;
    title: string;
    amount: string;
    redeem: string;
    type: string;
    isAvailable: boolean;
    freeSpinsGameName?: string;
    freeSpinsGameBet?: string;
}

interface ActiveWagerBonus {
    id: string
    title: string | null;
    bonusCode?: string | null;
    redeem?: string | null;
    amount?: string | null;
    redeemAmount: string | null;
    description: string | null;
    freeSpins?: {
        game: string;
        fsCount: Long;
        fsCountInitial: Long;
        percent: string;
    } | null;
    canCancel: boolean;
}

export default class WagerBonusActions {
    @observable entries: WagerBonusEntry[] = [];
    @observable active: ActiveWagerBonus | null = null;
    @observable waitingAnyActions = false;
    @observable cancelCorrection: bonuses.BonusCancelPreviewResponse.ICorrection | null = null;
    @observable confirmingActivation = false;
    @observable confirmationEntry?: WagerBonusEntry | null = null;

    activationCode: string = '';
    retryCount: number;

    constructor(private appState: AppState) {
    }

    @action.bound
    processActiveWagerBonus(wagerBonus: bonuses.IBonusCode) {
        const id = wagerBonus.id as unknown as string;
        const title = processString(BonusMeta.BONUS_TITLE, wagerBonus.meta, null);
        const res: ActiveWagerBonus = {
            id: id,
            bonusCode: wagerBonus.code,
            amount: processString(BonusMeta.WAGER_BONUS_AMOUNT, wagerBonus.meta, null),
            redeem: processString(BonusMeta.WAGER_BONUS_REDEEM, wagerBonus.meta, null),
            title: title,
            description: processString(BonusMeta.BONUSES_PAGE_DESCRIPTION, wagerBonus.meta, null),
            canCancel: !processBoolean(BonusMeta.CANT_CANCEL_BONUS, wagerBonus.meta),
            redeemAmount: processString(BonusMeta.BONUS_REDEEM_AMOUNT, wagerBonus.meta, null),
            freeSpins: null,
        };
        if (wagerBonus.freeSpins?.length) {
            const freeSpins = wagerBonus.freeSpins[0];
            if (freeSpins) {
                if (freeSpins.count && freeSpins.countInitial && freeSpins.gameId) {
                    res.freeSpins = {
                        game: freeSpins.gameId,
                        fsCount: freeSpins.count,
                        fsCountInitial: freeSpins.countInitial,
                        percent: formatPercent(calcPercent(freeSpins.count, freeSpins.countInitial))
                    }
                }
            }
        }
        this.active = res;
    }

    @action.bound
    processWagerBonuses(possibleWagerBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) {
        this.entries = [];
        for (const wagerBonus of possibleWagerBonuses) {
            if (
                wagerBonus.id &&
                wagerBonus.meta?.bonus_title &&
                wagerBonus.meta?.redeem &&
                wagerBonus.meta?.is_available
            ) {
                var res: WagerBonusEntry = {
                    id: wagerBonus.id,
                    type: wagerBonus.meta.type,
                    title: wagerBonus.meta.bonus_title,
                    amount: wagerBonus.meta.amount,
                    redeem: wagerBonus.meta.redeem,
                    isAvailable: wagerBonus.meta.is_available === 'true'
                };
                if (wagerBonus.meta.type === CaseType.FREESPINS) {
                    res.freeSpinsGameBet = wagerBonus.meta.bet_per_line;
                    res.freeSpinsGameName = wagerBonus.meta.game_name;
                }
                this.entries.push(res);
            }
        }
    }

    computeRedeemAmount() {
        var balance = this.appState.user.getFirstRealBalance();
        if (this.confirmationEntry && balance?.availableValue && balance.currencyId) {
            const redeemAmount = balance.availableValue.multiply(Long.fromString(this.confirmationEntry.redeem));
            return formatCurrencyAmount(redeemAmount, balance.currencyId);
        }
        return '';
    }

    @action.bound
    fetchData(shouldShowModal?: boolean) {
        setTimeout(() => {
            this.appState.userBonusesStore.fetchBonuses(() => {
                if (this.appState.page === SitePages.GAME && !this.appState.appSettings.wagerBonusOpenModalOnGamePage) {
                    shouldShowModal = false;
                }
                if (shouldShowModal) {
                    this.showModal();
                }
            });
        }, 0);
    }

    @action.bound
    tryActivatePromocode() {
        if (this.retryCount < 5) {
            this.retryCount++;
            this.appState.api.activatePromoCode(this.activationCode, this.onPromocodeActivateResponse)
        } else {
            const message = this.appState.t('error-refill-promo-code-not-found');
            this.waitingAnyActions = false;
            const errorNotification = {
                level: NotificationItemLevel.ERROR,
                message: message
            };
            this.appState.notificator!.addNotification(errorNotification);
        }
    }

    @action.bound
    onPromocodeActivateResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.bonusAction?.activateCode) {
            const activationResult = act.bonusAction.activateCode.result;

            if (activationResult) {
                if (activationResult === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                    this.waitingAnyActions = false;
                    this.appState.modal.hideModal();
                    this.appState.profileBonuses.onPromoCodeActivation();
                } else {
                    setTimeout(this.tryActivatePromocode, 1000);
                }
            } else {
                setTimeout(this.tryActivatePromocode, 1000);
            }
        }
    }

    @action.bound
    onClickModalTryApply(e: React.MouseEvent<HTMLElement>) {
        if (this.requireActivationConfirmation) {
            this.showConfirmation(e);
        } else {
            this.modalOnClickApply(e);
        }
    }

    @action.bound
    onClickBonusesPageTryApply(e: React.MouseEvent<HTMLElement>) {
        if (this.requireActivationConfirmation) {
            this.showConfirmationModal(e);
        } else {
            this.modalOnClickApply(e);
        }
    }

    @action.bound
    modalOnClickApply(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        const entryId = e.currentTarget.getAttribute('data-entry-id');
        this.retryCount = 0;
        if (entryId) {
            this.activationCode = entryId;
            this.waitingAnyActions = true;
            this.tryActivatePromocode();
        }
    }

    @action.bound
    showConfirmation(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        const entryId = e.currentTarget.getAttribute('data-entry-id');
        this.confirmingActivation = true;
        this.confirmationEntry = this.entries.find(e => e.id === entryId);
    }

    @action.bound
    showConfirmationModal(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        const entryId = e.currentTarget.getAttribute('data-entry-id');
        this.confirmingActivation = true;
        this.confirmationEntry = this.entries.find(e => e.id === entryId);
        this.appState.modal.showConfirmApplyingWagerBonus();
    }

    @action.bound
    cancelConfirmation(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        this.confirmingActivation = false;
        this.confirmationEntry = null;
    }

    @action.bound
    cancelConfirmationModal(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        this.appState.modal.hideModal();
        this.confirmingActivation = false;
        this.confirmationEntry = null;
    }

    @action.bound
    tryCancelBonus(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        if (this.active?.bonusCode) {
            this.waitingAnyActions = true;
            this.appState.api.cancelBonus(this.active.bonusCode, this.onTryCancelBonus);
        }
    }

    @action.bound
    onTryCancelBonus(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.bonusAction?.cancelCode) {
            const result = act.bonusAction.cancelCode.result;
            this.waitingAnyActions = false;
            if (result === bonuses.BonusCancelResponse.Result.SUCCESS) {
                this.active = null;
                this.appState.profileBonuses.tryLoadBonusesData();

                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-notification-bonus-reject-description'),
                    level: NotificationItemLevel.SUCCESS
                });
            } else {
                const bonusActivationErrorToken = getBonusCancelErrorToken(result);
                this.appState.notificator!.addNotification({
                    message: this.appState.t(bonusActivationErrorToken),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            const bonusActivationErrorToken = getBonusCancelErrorToken();
            this.appState.notificator!.addNotification({
                message: this.appState.t(bonusActivationErrorToken),
                level: NotificationItemLevel.ERROR
            });
        }
        this.appState.modal.hideModal();
    }

    @action.bound
    onClickCancelBonus(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        if (this.active?.bonusCode) {
            this.waitingAnyActions = true;
            this.appState.api.cancelBonusPreview(this.active.bonusCode, this.onCancelBonusPreview)
        }
    }

    @action.bound
    onCancelBonusPreview(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.bonusAction?.previewCancelCode) {
            const result = act.bonusAction.previewCancelCode.result;
            this.waitingAnyActions = false;
            if (result === bonuses.BonusCancelPreviewResponse.Result.SUCCESS && act.bonusAction.previewCancelCode.correction) {
                this.cancelCorrection = act.bonusAction.previewCancelCode.correction;
            }
            this.appState.modal.showConfirmRefuseWagerBonus();
        }
    }

    @action.bound
    modalOnClickRules(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        const entryId = e.currentTarget.getAttribute('data-entry-id');
        const bonusesPageEntry = this.appState.userBonusesStore.displayedBonuses!.find(b => b.id === entryId) as DisplayedWagerBonus;
        this.appState.modal.hideModal();
        this.appState.page = SitePages.BONUSES;
        this.appState.site.onPageOpen(SitePages.BONUSES);
        setTimeout(() => {
            bonusesPageEntry.open();
        }, 200);
    }

    @action.bound
    onClickRules(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        if (this.active?.id) {
            const bonusesPageEntry = this.appState.userBonusesStore.displayedBonuses!.find((b) => {
                return b.id === this.active!.id;
            }) as DisplayedWagerBonus;
            if (bonusesPageEntry) {
                bonusesPageEntry.open();
            }
        }
    }

    @action.bound
    showModal() {
        this.appState.modal.showWagerBonudModal();
    }

    getWidthClass() {
        return `modal-width-${Math.ceil(this.entries.length / 4)}`;
    }

    get isPossibleWagerBonusesExists(): boolean {
        return this.entries.length !== 0;
    }

    get openModalOnLogin(): boolean {
        return !!this.appState.appSettings.wagerBonusOpenModalOnLogin;
    }

    get openModalOnInit(): boolean {
        return !!this.appState.appSettings.wagerBonusOpenModalOnInit;
    }

    get requireActivationConfirmation(): boolean {
        return !!this.appState.appSettings.wagerBonusRequireActivationConfirmation;
    }
};