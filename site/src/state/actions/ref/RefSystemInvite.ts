import {action, autorun, computed, observable, runInAction} from 'mobx';
import copyToClipboard from 'copy-to-clipboard';
import AppState from '../../AppState';
import {users} from '../../../api/proto';
import {formatSMSHref} from '../../utils/formatSMSHref';

const COPIED_INVITE_MESSAGE_TIMEOUT = 1000;

export class RefSystemInvite {
    @observable isTimeoutOn = true;
    timer: ReturnType<typeof setTimeout>;
    autorunDisposer: () => void;
    @observable isCopiedToClipboard = false;
    copiedTimer: ReturnType<typeof setTimeout>;

    @computed
    get isResendButtonVisible(): boolean {
        return (
            !this.isTimeoutOn &&
            this.opts.invitationStatus === users.invitations.InvitationBusinessStatus.IN_DELIVERY &&
            !this.appState.ref.isManualSMSMode
        );
    }

    @computed
    get smsLink(): string {
        return formatSMSHref(this.opts);
    }

    get isAccepted(): boolean {
        return this.opts.invitationStatus === users.invitations.InvitationBusinessStatus.ACCEPTED;
    }

    constructor(private opts: users.invitations.IUserInvitation, private appState: AppState) {
        this.updateTimer();
        this.autorunDisposer = autorun(() => {
            if (this.appState.ref.bonusInfo?.resentIntervalMs) {
                this.updateTimer();
            }
        });
    }

    cleanUp() {
        this.autorunDisposer();
        clearTimeout(this.timer);
        clearTimeout(this.copiedTimer);
    }

    @action.bound
    updateTimer() {
        clearTimeout(this.timer);
        if (
            this.appState.ref.bonusInfo?.resentIntervalMs &&
            this.opts.lastSent &&
            this.opts.invitationStatus === users.invitations.InvitationBusinessStatus.IN_DELIVERY
        ) {
            const currentDate = Date.now();
            const inviteDate = new Date(this.opts.lastSent.toNumber());
            const timeLeft =
                inviteDate.getTime() +
                this.appState.ref.bonusInfo?.resentIntervalMs?.toNumber() -
                currentDate;
            if (timeLeft <= 0) {
                this.isTimeoutOn = false;
            } else {
                this.timer = setTimeout(() => {
                    runInAction(() => {
                        this.isTimeoutOn = false;
                    });
                }, timeLeft);
            }
        } else {
            this.isTimeoutOn = true;
        }
    }

    @action.bound
    resend() {
        if (this.opts.id) {
            this.isTimeoutOn = true;
            this.appState.api.refResendInviteRequest(this.opts.id, (msg, act) => {
                const response = act.resendInvitationResponse;
                if (response && !response.status) {
                    if (response.invitation) {
                        this.opts = response.invitation;
                    }
                    this.updateTimer();
                } else {
                    this.isTimeoutOn = false;
                }
            });
        }
    }

    @action.bound
    copyLink() {
        if (this.opts.loginLink) {
            clearTimeout(this.copiedTimer);
            copyToClipboard(this.opts.loginLink);
            this.isCopiedToClipboard = true;
            this.copiedTimer = setTimeout(() => {
                runInAction(() => {
                    this.isCopiedToClipboard = false;
                });
            }, COPIED_INVITE_MESSAGE_TIMEOUT);
        }
    }
}
