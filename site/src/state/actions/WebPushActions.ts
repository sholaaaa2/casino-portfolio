import {action, computed, observable} from 'mobx';
import {webpush} from '../../api/proto';
import AppState from '../AppState';

const SOUND_PATH = '/audio/web-push-notification.mp3';

export class WebPushNotification extends webpush.ServerEvent {
    @observable timeLeft?: number = undefined;
    timeout?: number;
    tickInterval?: number;

    constructor(event: webpush.ServerEvent, private appState: AppState) {
        super(event);
        this.checkExpiration();
        this.checkAndPlayAudioIfNeeded();
    }

    @computed
    get id(): string {
        return new TextDecoder().decode(this.eventId);
    }

    @computed
    get buttonText(): string | null {
        if (this.message?.actionText) {
            if (this.timeLeft) {
                return `${this.message.actionText} (${this.timeLeft}s)`;
            }
            return this.message.actionText;
        }
        return null;
    }

    @action.bound
    checkExpiration() {
        if (this.expiresAtMs) {
            const timeout = this.expiresAtMs.subtract(new Date().getTime());
            if (timeout.greaterThan(0)) {
                if (this.message?.screenTimeInMs) {
                    const screenTime = this.message.screenTimeInMs;
                    const timeLeft = screenTime.toNumber();
                    this.timeLeft = Math.floor(timeLeft / 1000);
                    this.timeout = window.setTimeout(() => {
                        this.close();
                    }, timeLeft);
                    this.tickInterval = window.setInterval(() => {
                        this.tickTimer();
                    }, 1000);
                }
            } else {
                this.close();
            }
        }
        
    }

    @action.bound
    checkAndPlayAudioIfNeeded() {
        if (this.message?.sound) {
            const audio = new Audio(SOUND_PATH);
            audio.play();
        }
    }

    @action.bound
    tickTimer() {
        if (!this.timeLeft) {
            window.clearInterval(this.tickInterval);
        } else {
            this.timeLeft = this.timeLeft - 1;
        }
    }

    @action.bound
    close(shouldPost = true) {
        if (shouldPost) {
            this.appState.webPushAPI?.post({
                webPushClosed: new webpush.WebPushClose({
                    eventId: this.eventId,
                }),
            });
        }
        window.clearTimeout(this.timeout);
        window.clearInterval(this.tickInterval);
        this.appState.webPushActions?.items.remove(this);
    }
}

export class WebPushActions {
    items = observable<WebPushNotification>([]);

    constructor(private appState: AppState) {}

    @action.bound
    onMessage(event: webpush.ServerEvent) {
        if (this.appState.userInfo.userId.eq(event.userId)) {
            if (event.message) {
                this.appState.webPushAPI.post({
                    webPushReceived: new webpush.WebPushReceived({eventId: event.eventId}),
                });
                const id = new TextDecoder().decode(event.eventId);
                if (this.items.every((item) => item.id !== id)) {
                    const notification = new WebPushNotification(event, this.appState);
                    this.items.unshift(notification);
                }
            }
            if (event.webPushClose?.eventId) {
                const id = new TextDecoder().decode(event.webPushClose.eventId);
                const notification = this.items.find((_) => _.id === id);
                notification?.close(false);
            }
        }
    }
}
