import Long from "long";
import { bonuses } from "../../../api/proto";
import AppState from "../../AppState";
import { action, observable, computed } from "mobx";
import { Modal, SitePages } from "../../meta";
import { Currency, findCurrencyById } from "../../utils/Currencies";
import { formatCurrencyAmount, formatCurrencyCode } from "../../utils/Money";
import { findGame, getGameImage } from '../../utils/GameUtils';
import { GameItem, GameImageSize } from '../GamesActions';
import { BonusEntryWager } from '../ProfileBonusesActions';
import { timeStampToHHMMSS } from '../../utils/Datetime';

const DAILY_BONUS_TYPE = "daily-bonus";


type TextOrLangMap = string | { [lang: string]: string };
interface Bonus {
    fromDeposit: number;
    toDeposit: number;
    blockTitle: TextOrLangMap;
    amount: {
        percent: number;
        fixed: number;
    };
    descTitle: TextOrLangMap;
    title: TextOrLangMap;
    description: TextOrLangMap;
    wager: number;
    subtitle?: string;
    fromDepositFormatted: string;
    toDepositFormatted: string;
}
interface TimedBonus extends NotLocalizedBonus {
    fromHour: number;
    toHour: number;
    days: number[];
}
interface NotLocalizedBonus extends Bonus {
    blockTitle: string;
    descTitle: string;
    title: string;
    description: string;
}

enum ParticipationStatus {
    playing = 'playing',
    waiting = 'waiting',
    not_participating = 'not-participating'
}

export interface IDailyBonusRules {
    currency: number;
    participation_status: ParticipationStatus;
    current_day: number;
    remaining_time: number;
    daily_bonuses_list: IBonusDay[];
}

enum ConditionTypes {
    EMPTY = "empty",
    BALANCE = "balance",
    RANDOM = "random",
    REFILLS_FOR_LIFETIME = "refillsForLifetime",
    REFILLS_FOR_PERIOD = "refillsForPeriod",
    SPINS_FOR_PERIOD = "spinsForPeriod",
}

interface Condition {
    type: ConditionTypes,
    filter: {
        amount?: number;
        minAmount?: number;
        minSpins?: number;
        minCount?: number;
        periodInHours?: number;
    }
}

interface IBonusDay {
    index: number;
    amount: {
      fixed?: number;
      fixedFormatted?: string;
      fixedWager?: number;
      freeBetsWager?: number;
      freeBetsMaxWin?: number;
      freeSpinsWager?: number;
      freeSpinsMaxWin?: number;
      withdrawalLimit?: number;
      withdrawalMin?: number;
      freeSpins?: {[currency: string]: {
        bet?: number;
        cnt?: number;
        lines?: number;
        game?: string;
      }[]};
      freeBets?: {[currency: string]: {
        bet?: number;
        cnt?: number;
        lines?: number;
        game?: string;
      }[]};
      freeSpinsFormatted?: {
        bet?: number;
        betFormatted?: string;
        cnt?: number;
        lines?: number;
        game?: string;
        gameObj?: GameItem | null;
        gameName?: string;
        gameImage?: string;
      }[];
      freeBetsFormatted?: {
        bet?: number;
        betFormatted?: string;
        cnt?: number;
        lines?: number;
        game?: string;
        gameObj?: GameItem | null;
        gameName?: string;
        gameImage?: string;
      }[];
    };
    meta: {
      title?: string;
      description?: string;
    };
    condition: string | null;
}



export class DailyBonus {
    @observable selectedDayIndex: number;
    @observable selectedDayInterval: Interval | null = null;
    @observable bonuses: Bonus[] = [];
    @observable timedBonuses: TimedBonus[] = [];
    @observable days: number[] = [];
    @observable bonusAmount?: string;
    @observable bonusAmountTitle?: string;
    @observable dailyBonusRules?: IDailyBonusRules = undefined;
    @observable daysBonuses?: IBonusDay[] = [];
    @observable dailyBonusCode?: string;
    @observable currentBonusWager?: BonusEntryWager | null;
    @observable showWager: boolean = false;
    @observable remainingTime?: number = undefined;
    @observable formattedRemainingTime?: {hh:string, mm:string, ss:string} = {hh:'00', mm:'00', ss:'00'};
    @observable dailyBonusState?: bonuses.BonusCode.State;
    @observable isLoading: boolean = false;
    interval: number;

    shouldShowModal = false;

    constructor(private appState: AppState) {}

    @computed
    get finalBonus(): Bonus | undefined {
        return this.bonuses.length > 0 ? this.bonuses[0] : undefined;
    }

    componentDidUnmount() {
        clearTimeout(this.interval);
    }

    @action.bound
    fetchData(shouldShowModal?: boolean) {
        this.shouldShowModal = !!shouldShowModal;

        setTimeout(() => {
          if (this.appState.page === SitePages.GAME && !this.appState.appSettings.dailyBonusOpenModalOnGamePage) {
              this.shouldShowModal = false;
          }
          this.appState.userBonusesStore.fetchBonuses(
              this.processData.bind(this)
          );
        }, 0);

    }

    @action.bound
    processData(
        codes: bonuses.IBonusCode[],
        possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[]
    ) {
        this.appState.profileBonuses.onTryLoadBonusesData(undefined, codes, possibleBonuses);
        let dailyBonus: bonuses.IBonusCode | bonuses.UserBonusesListResponse.IPossibleBonusCode | undefined;
        dailyBonus = codes.find(
            (bonus) =>
                bonus.id &&
                bonus.meta &&
                bonus.meta.bonus_type === DAILY_BONUS_TYPE
                && bonus.state && bonus.state < 3
        )
        if (!dailyBonus) {
            dailyBonus = codes.find(
                (bonus) =>
                    bonus.id &&
                    bonus.meta &&
                    bonus.meta.bonus_type === DAILY_BONUS_TYPE
                    && !bonus.state
            )
        }

        if (!dailyBonus) {
            dailyBonus = possibleBonuses.find(
                (bonus) =>
                    bonus.id &&
                    bonus.meta &&
                    bonus.meta.bonus_type === DAILY_BONUS_TYPE
            )
        }


        if (dailyBonus?.meta) {
            let db = dailyBonus as bonuses.IBonusCode;
            if (db.code) {
                this.dailyBonusCode = db.code;
                this.currentBonusWager = this.appState.profileBonuses.processBonusWager(db);
            }
            if (db.state) {
                this.dailyBonusState = db.state;
                this.showWager = db.state < 3;
            }

            this.processDailyBonus(
                dailyBonus.meta
            );
        }
    }

    @computed
    get showBonusInfoButton(): boolean {
        return !!(this.daysBonuses && this.daysBonuses.length > 0);
    }

    @action
    setBonuses = (bonuses: Bonus[]) => {
        this.bonuses = bonuses;
        if (this.bonusAmount && bonuses.length > 1) {
            this.setBonusAmountTitle();
        }
    };

    @action
    setBonusAmountTitle() {
        this.bonusAmountTitle = `${this.appState.t(
            "bonus-final-yours"
        )} <strong>${this.bonusAmount}</strong>`;
    }

    showDialog = () => {
        if (this.daysBonuses) {
            this.appState.modal.show(Modal.DAILY_BONUS);
        }
    };

    @action
    onActivateBonus = () => {
        if (this.dailyBonusCode) {
            this.isLoading = true;
            this.appState.profileBonuses.tryAcceptBonus(this.dailyBonusCode, () => this.fetchData());
        }
    }

    @action
    processDailyBonus(meta: { [k: string]: string }) {
        const rules: IDailyBonusRules = JSON.parse(meta.daily_rules);
        this.dailyBonusRules = {
          ...rules,
        };


        if (rules.daily_bonuses_list && rules.daily_bonuses_list.length) {
            let currency = rules.currency;
            if (!this.appState.userInfo.guest && this.appState.userInfo.currency?.id){
                currency = this.appState.userInfo.currency.id.toNumber();
            }
            const currencyCode = formatCurrencyCode(Long.fromNumber(currency));
            this.daysBonuses = rules.daily_bonuses_list.map((dayBonus, i) => {
                if (!this.appState.userInfo.guest && rules.current_day === i) {
                    if ((dayBonus.amount.freeSpins && Object.keys(dayBonus.amount.freeSpins as {}).length && !dayBonus.amount.freeSpins[currencyCode])
                        || (dayBonus.amount.freeBets && Object.keys(dayBonus.amount.freeBets as {}).length && !dayBonus.amount.freeBets[currencyCode])) {
                          this.shouldShowModal = false;
                    }
                }
                let currencyForFS = formatCurrencyCode(Long.fromNumber(currency));
                let freeSpins = dayBonus.amount.freeSpins && dayBonus.amount.freeSpins[currencyCode];
                if (this.appState.userInfo.guest && !freeSpins && Object.keys(dayBonus.amount.freeSpins as {}).length) {
                    currencyForFS = Object.keys(dayBonus.amount.freeSpins as {})[0];
                    freeSpins = dayBonus.amount.freeSpins && dayBonus.amount.freeSpins[Object.keys(dayBonus.amount.freeSpins)[0]];
                }
                let freeBets = dayBonus.amount.freeBets && dayBonus.amount.freeBets[currencyCode];
                if (this.appState.userInfo.guest && !freeBets && Object.keys(dayBonus.amount.freeBets as {}).length) {
                    currencyForFS = Object.keys(dayBonus.amount.freeBets as {})[0];
                    freeBets = dayBonus.amount.freeBets && dayBonus.amount.freeBets[Object.keys(dayBonus.amount.freeBets)[0]];
                }
                let condition = null;
                if (dayBonus.condition) {
                    condition = this.processDayCondition(dayBonus.condition as unknown as Condition, rules.currency);
                }
                return {
                  index: i,
                  amount: {
                    ...dayBonus.amount,
                    fixedFormatted: dayBonus.amount.fixed
                        ? formatCurrencyAmount(Long.fromNumber(dayBonus.amount.fixed), Long.fromNumber(rules.currency))
                        : undefined,
                    freeSpinsFormatted: freeSpins
                        ? freeSpins.map((freeSpin) => {
                            const gameName = this.appState.games.getGameName(freeSpin.game);
                            return {
                              ...freeSpin,
                              betFormatted: freeSpin.bet
                                ? `${formatCurrencyAmount(Long.fromNumber(freeSpin.bet), Long.fromNumber(rules.currency), true)} ${currencyForFS}`
                                : undefined,
                              gameObj: findGame(this.appState, freeSpin.game),
                              gameName: gameName,
                              gameImage: getGameImage(gameName, GameImageSize.SIZE_300x300)
                            }
                          })
                        : undefined,
                    freeBetsFormatted: freeBets
                        ? freeBets.map((freeSpin) => {
                            const gameName = this.appState.games.getGameName(freeSpin.game);
                            return {
                              ...freeSpin,
                              betFormatted: freeSpin.bet
                                ? `${formatCurrencyAmount(Long.fromNumber(freeSpin.bet), Long.fromNumber(rules.currency), true)} ${currencyForFS}`
                                : undefined,
                              gameObj: findGame(this.appState, freeSpin.game),
                              gameName: gameName,
                              gameImage: getGameImage(gameName, GameImageSize.SIZE_300x300)
                            }
                          })
                        : undefined,
                  },
                  meta: {
                    title: this.checkAndTranslateTextField(dayBonus.meta.title),
                    description: this.checkAndTranslateTextField(dayBonus.meta.description),
                  },
                  condition,
                }
            });
        }

        this.isLoading = false;

        if (!this.appState.userInfo.guest && this.daysBonuses?.length) {
            if (rules.remaining_time !== undefined) {
                this.remainingTime = Number(rules.remaining_time);
                this.interval = window.setInterval(this.computedTimer, 1000);
            }
            if (this.shouldShowModal) {
                this.shouldShowModal = false;
                setTimeout(() => {
                    this.showDialog();
                }, 1000);
            }
        }
    }

    computedTimer = () => {
      if (this.remainingTime && this.remainingTime > 0) {
          this.remainingTime -= 1000;
          if (this.remainingTime < 0) this.remainingTime = 0;
          this.formattedRemainingTime = timeStampToHHMMSS(this.remainingTime);
      }
  }

    processBonus(
        bonus: Bonus,
        currencyId: Long,
        isMultipleBonuses: boolean
    ): Bonus {
        const currency = findCurrencyById(currencyId);
        const { amount } = bonus;

        bonus.blockTitle = this.checkAndTranslateTextField(bonus.blockTitle);
        bonus.descTitle = this.checkAndTranslateTextField(bonus.descTitle);
        bonus.title = this.checkAndTranslateTextField(bonus.title);
        bonus.description = this.checkAndTranslateTextField(bonus.description);

        bonus.toDepositFormatted = bonus.toDeposit
            ? formatCurrencyAmount(Long.fromNumber(bonus.toDeposit), currency)
            : "";
        bonus.fromDepositFormatted = bonus.fromDeposit
            ? formatCurrencyAmount(Long.fromNumber(bonus.fromDeposit), currency)
            : "0";
        if (this.bonusAmount !== undefined && isMultipleBonuses === false) {
            bonus.title = `${bonus.title} <span class="final-bonuses__bonus-amount">${this.bonusAmount}</span>`;
        } else if (amount.fixed) {
            bonus.title = `${bonus.title} ${formatCurrencyAmount(
                Long.fromNumber(amount.fixed),
                currency
            )}`;
        } else if (amount.percent) {
            bonus.title = `${bonus.title} ${amount.percent}%`;
        }
        return bonus as NotLocalizedBonus;
    }

    processDayCondition(condition: Condition, bonusCurrencyId: number): string | null {
        if (condition.type === ConditionTypes.EMPTY) return null;
        if (condition.type === ConditionTypes.RANDOM) return null;

        const currency = this.appState.currencies.findCurrencyById(Long.fromNumber(bonusCurrencyId));
        if (condition.type === ConditionTypes.BALANCE)
            return this.getConditionRequiredBalanceAmountMessage(condition, currency);
        if (condition.type === ConditionTypes.SPINS_FOR_PERIOD)
            return this.getConditionSpinsForPeriodMessage(condition);
        if (condition.type === ConditionTypes.REFILLS_FOR_LIFETIME)
            return this.getConditionRefillsForLifetimeMessage(condition, currency);
        if (condition.type === ConditionTypes.REFILLS_FOR_PERIOD)
            return this.getConditionRefillsForPeriodMessage(condition, currency);
        return null;
    }

    getConditionRequiredBalanceAmountMessage(condition: Condition, currency: Currency | null) {
        if (!condition.filter.amount || !currency) return null;
        const amount = formatCurrencyAmount(Long.fromNumber(condition.filter.amount), currency);
        return this.appState.t('daily-bonus-condition-balance-text', { amount });
    }

    getConditionSpinsForPeriodMessage(condition: Condition) {
        if (!condition.filter.minSpins || !condition.filter.periodInHours) return null;
        return this.appState.t(
            'daily-bonus-condition-spins-for-period',
            { minSpins: condition.filter.minSpins, period: condition.filter.periodInHours}
        );
    }

    getConditionRefillsForLifetimeMessage(condition: Condition, currency: Currency | null) {
        if ((!condition.filter.minAmount && !condition.filter.minCount) || !currency) return null;
        let amount;
        if (condition.filter.minAmount)
            amount = formatCurrencyAmount(Long.fromNumber(condition.filter.minAmount), currency);
        if (amount && condition.filter.minCount)
            return this.appState.t(
                'daily-bonus-condition-refills-lifetime-amount-count-text',
                { amount, count: condition.filter.minCount }
            );
        if (amount)
            return this.appState.t(
                'daily-bonus-condition-refills-lifetime-amount-text',
                { amount }
            );
        if (condition.filter.minCount)
            return this.appState.t(
                'daily-bonus-condition-refills-lifetime-count-text',
                { count: condition.filter.minCount }
            );
        return null;
    }

    getConditionRefillsForPeriodMessage(condition: Condition, currency: Currency | null) {
        if ((!condition.filter.minAmount && !condition.filter.minCount) || !condition.filter.periodInHours || !currency) return null;
        let amount;
        if (condition.filter.minAmount) amount = formatCurrencyAmount(Long.fromNumber(condition.filter.minAmount), currency);
        if (amount && condition.filter.minCount)
            return this.appState.t(
                'daily-bonus-condition-refills-period-amount-count-text', // todo: add poeditor
                { amount, count: condition.filter.minCount, period: condition.filter.periodInHours }
            );
        if (amount)
            return this.appState.t(
                'daily-bonus-condition-refills-period-amount-text', // todo: add poeditor
                { amount, period: condition.filter.periodInHours }
            );
        if (condition.filter.minCount)
            return this.appState.t(
                'daily-bonus-condition-refills-period-count-text',
                { count: condition.filter.minCount, period: condition.filter.periodInHours }
            );
        return null;
    }

    toggleCardHelp = (e: any) => {
        const currentTarget = e.currentTarget;
        if (e.target.classList.contains("toggle-help")) {
            if (currentTarget.classList.contains("show-help")) {
                currentTarget.classList.remove("show-help");
                setTimeout(() => {
                    // hack for safari
                    currentTarget.classList.remove("hide-button");
                }, 500);
            } else {
                currentTarget.classList.add("show-help", "hide-button");
            }
        }
    };

    hideAllCardHelp() {
        document.querySelectorAll(".show-help").forEach((e) => {
            e.setAttribute("style", "transition: 0.001s");
            e.classList.remove("show-help");
            setTimeout(() => {
                e.removeAttribute("style");
            }, 0);
        });
    }

    checkAndTranslateTextField = (field: string | TextOrLangMap | undefined): string => {
        if (typeof field === "string") return field;
        return field ? (field[this.appState.language] || field["default"] || "") : "";
    };

    get openModalOnLogin(): boolean {
        return !!this.appState.appSettings.dailyBonusOpenModalOnLogin;
    }

    get openModalOnInit(): boolean {
        return !!this.appState.appSettings.dailyBonusOpenModalOnInit;
    }

    get currentDay() {
        if (this.daysBonuses) {
            const currentDay = this.daysBonuses.find(dayBonus => dayBonus.index === this.dailyBonusRules?.current_day);
            return currentDay ? currentDay : undefined;
        }
        return undefined;
    }

    getClassForDay(dayBonus: IBonusDay) {
        if (this.dailyBonusRules?.current_day !== undefined && dayBonus.index === this.dailyBonusRules?.current_day) {
            return 'current'
        }
        if (this.dailyBonusRules?.current_day !== undefined && dayBonus.index < this.dailyBonusRules?.current_day) {
            return 'past'
        }
        return '';
    }

    getImageForDay(dayBonus: IBonusDay) {
        if (this.dailyBonusRules?.current_day !== undefined && dayBonus.index === this.dailyBonusRules?.current_day) {
            return '/images/bonuses/daily/day-bg-03.svg'
        }
        if (this.dailyBonusRules?.current_day !== undefined && dayBonus.index < this.dailyBonusRules?.current_day) {
            return '/images/bonuses/daily/day-bg-01.svg'
        }
        return '/images/bonuses/daily/day-bg-02.svg';
    }

    isChecked(dayBonus: IBonusDay) {
        return (this.dailyBonusRules?.current_day !== undefined && dayBonus.index < this.dailyBonusRules?.current_day)
          || (this.dailyBonusRules?.participation_status === ParticipationStatus.waiting && dayBonus.index === this.dailyBonusRules?.current_day);
    }

    showGetButton() {
        return !this.dailyBonusState && this.dailyBonusRules?.participation_status === ParticipationStatus.playing && this.dailyBonusCode;
    }
}
