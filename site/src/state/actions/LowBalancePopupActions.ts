import {action, computed, observable} from 'mobx';
import AppState from '../AppState';
import {site} from '../../api/proto';
import {SitePages} from '../meta';

export default class LowBalancePopupActions {
    private appState: AppState;
    @observable config?: site.ILowBalancePopup;
    @observable isShowed: boolean = false;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @action
    showIfNeeded = () => {
        const {appState} = this;
        if (!this.isShowed && appState.page && appState.site.isAuthorized &&
            this.config?.limit && appState.userInfo?.balance?.available &&
            (this.config?.limit.toNumber() / 100) >= Number(appState.userInfo?.balance?.available) &&
            appState.page !== SitePages.GAME &&
            appState.page !== SitePages.LIVE_CASINO &&
            appState.page !== SitePages.LOBBY &&
            appState.page !== SitePages.LOBBY_GAME) {
              appState.modal.showLowBalancePopup();
              this.isShowed = true;
        }
    }

    @action.bound
    setConfig(config: site.ILowBalancePopup) {
        this.config = config;
        this.showIfNeeded();
    }

    @computed
    get content() {
        return this.config!;
    }

    @action
    goToButtonUrl = (url: string) => {
        this.appState.modal.hideModal();
        this.appState.router.push(this.appState.l(url));
    }
}
