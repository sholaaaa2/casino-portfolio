import {bonuses} from "../../api/proto";
import {processParsedMetaList} from "../../api/utils";
import AppState from "../AppState";
import Long from 'long';
import {formatCurrencyAmount} from "../utils/Money";

interface Rules {
    is_available?: boolean;
    min_deposit?: number;
    min_closing_amount?: number;
    cases?: Case[];
    currency?: number;
    bonus_redeem_amount?: number;
    free_spins_redeem?: string;
    free_spins_multiplier?: string;
    current_redeem?: number;
}

interface Case {
    promocode?: string;
    caseSettings?: CaseSettings;
    meta: {
        bonus_title: string,
        issue_rules: string,
        wagering_rules: string,
        withdrawal_rules: string,
        html_description: string,
        bonuses_page_description: string,
        notification: string
    };
}

interface CaseSettings {
    type?: CaseType;
    redeem?: number;
    multiplier?: number;
    ui?: {
        bannerUrl?: string;
    }
    fs?: {
        [currency: string]: FreeSpinsParams
    }
}

interface FreeSpinsParams {
    bet: number;
    cnt: number;
    lines: number;
    game: string;
}

export enum CaseType {
    FREESPINS = 'freespins',
    FUNDS = 'funds'
}

export class WagerBonusProcessor {
    constructor(private appState: AppState) {
    }

    process(
        bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode | bonuses.IBonusCode
    ): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        const originMeta = bonus.meta;
        if (originMeta?.rules) {
            const rules = this.parseRules(originMeta.rules);
            if (rules?.cases) {
                const cases = processParsedMetaList(rules.cases as any);
                const processedCases = [];
                for (const caseEntry of cases) {
                    const processedCase = this.processCase(bonus, rules, caseEntry, originMeta);
                    if (Object.keys(processedCase.meta as Object).length > 0) {
                        if (processedCase.meta?.type === CaseType.FREESPINS) {
                            if (!!this.appState.games.findGameById(processedCase.meta?.gameId)) {
                                processedCases.push(processedCase);
                            }
                        } else {
                            processedCases.push(processedCase);
                        }
                    }
                }
                return processedCases;
            }
        }
        return [];
    }

    processCase(
        bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode | bonuses.IBonusCode,
        rules: Rules,
        caseEntry: Case,
        originMeta: { [k: string]: string }
    ): bonuses.UserBonusesListResponse.IPossibleBonusCode {
        return {
            ...bonus,
            id: caseEntry.promocode,
            meta: this.getCaseMeta(originMeta, caseEntry, rules)
        };
    }

    getIssueNotificationTitle(meta: { [k: string]: string }): string | null {
        const rules = this.parseRules(meta.rules);
        if (rules?.cases && rules.cases[0]?.meta.bonus_title)
            return rules.cases[0].meta.bonus_title;
        return null;
    }

    getIssueNotification(meta: { [k: string]: string }): string | null {
        const rules = this.parseRules(meta.rules);
        if (rules?.cases && rules.cases[0]?.meta.notification)
            return rules.cases[0].meta.notification;
        return null;
    }

    private parseRules(rules: string): Rules | null {
        try {
            return JSON.parse(rules)
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.error("Wager Bonus: Wager bonus rules are invalid JSON");
                console.error(e);
            } else {
                throw e;
            }
            return null;
        }
    }

    private getCaseMeta(originMeta: { [k: string]: string }, caseEntry: Case, rules: Rules) {
        if (this.isRequiredFieldsPresent(caseEntry, rules)) {
            const bonusRedeemAmount = this.formatBonusRedeemAmount(rules.bonus_redeem_amount);
            let amount = "0";
            let gameName: string | undefined;
            let gameId: string | undefined;
            let betPerLine;
            if (caseEntry.caseSettings!.type === CaseType.FREESPINS) {
                const freeSpinsSettings = this.getFreeSpinsParams(caseEntry.caseSettings!, +originMeta.currency);
                if (freeSpinsSettings) {
                    amount = freeSpinsSettings.cnt.toString();
                    gameId = freeSpinsSettings.game;
                    gameName = this.appState.games.getGameName(freeSpinsSettings.game);
                    betPerLine = this.getFreeSpinsBet(caseEntry.caseSettings!, +originMeta.currency);
                    if (!gameName || !betPerLine) {
                        console.warn(`Wager Bonus: Freespins no such game(${freeSpinsSettings.game}) is available to user`);
                        return {};
                    }
                } else {
                    console.warn('Wager Bonus: no fs settings in Freespins case');
                    return {};
                }
                caseEntry.meta = this.replaceGameIdInMeta(caseEntry.meta);
            } else {
                const multiplier = caseEntry.caseSettings!.multiplier;
                amount = multiplier !== undefined ? multiplier.toString() : "0";
            }

            const caseMeta = {
                ...originMeta,
                ...caseEntry.meta,
                is_available: rules.is_available!.toString(),
                type: caseEntry.caseSettings!.type!.toString(),
                redeem: caseEntry.caseSettings!.redeem!.toString(),
                bet_per_line: betPerLine,
                game_name: gameName,
                gameId,
                amount: amount,
                bonus_redeem_amount: bonusRedeemAmount,
                free_spins_redeem: rules?.free_spins_redeem,
                free_spins_multiplier: rules?.free_spins_multiplier,
                current_redeem: rules?.current_redeem,
                bannerUrl: caseEntry.caseSettings?.ui?.bannerUrl ? caseEntry.caseSettings.ui.bannerUrl : originMeta.bannerUrl || undefined,
            };

            // @ts-ignore:
            delete caseMeta.rules;
            return caseMeta;
        }
        console.warn('Wager Bonus: Bonus case is present but some required data is missing');
        return {};
    }

    private isRequiredFieldsPresent(caseEntry: Case, rules: Rules): boolean {
        return (
            caseEntry !== undefined &&
            caseEntry.caseSettings?.type !== undefined  &&
            caseEntry.caseSettings?.redeem !== undefined  &&
            rules.min_deposit !== undefined  &&
            rules.min_closing_amount !== undefined  &&
            rules.is_available !== undefined
        );
    }

    private formatBonusRedeemAmount(bonusRedeemAmount?: number): string {
        if (bonusRedeemAmount && bonusRedeemAmount !== 0) {
            return formatCurrencyAmount(
                Long.fromNumber(bonusRedeemAmount),
                this.appState.user.getRealBalanceCurrency()
            );
        }
        return 'null';
    }

    private replaceGameIdInMeta(meta: Case["meta"]): Case["meta"] {
        const newMeta = {};
        for (const metaFieldName in meta) {
            newMeta[metaFieldName] = this.findAndReplaceGameId(meta[metaFieldName]);
        }
        return newMeta as Case["meta"];
    }

    private findAndReplaceGameId(metaField: string) {
        const match = metaField.match(/{gameId\((.+)\)}/);
        if (match && match[1]) {
            const gameName = this.appState.games.getGameName(match[1]);
            if (gameName) {
                return metaField.replace(/{gameId\(.+\)}/g, gameName);
            }
        }
        return metaField;
    }

    private getFreeSpinsParams(caseSettings: CaseSettings, defaultCurrency: number): FreeSpinsParams | null {
        if (caseSettings.fs) {
            if (this.appState.userInfo.guest) {
                const currency = this.appState.currencies.findCurrencyById(Long.fromNumber(defaultCurrency));
                if (currency) return caseSettings.fs[currency.code];
            } else if (!this.appState.userInfo.guest) {
                const currency = this.appState.user.getRealBalanceCurrency();
                if (currency) return caseSettings.fs[currency.code];
            }
        }
        console.warn('Wager Bonus: FreeSpins for default site currency or user currency is missing');
        return null;
    }

    private getFreeSpinsBet(caseSettings: CaseSettings, defaultCurrency: number): string | null {
        if (caseSettings.fs) {
            if (this.appState.userInfo.guest) {
                const currency = this.appState.currencies.findCurrencyById(Long.fromNumber(defaultCurrency));
                if (currency && caseSettings.fs[currency.code].bet) {
                    return formatCurrencyAmount(Long.fromNumber(caseSettings.fs[currency.code].bet), currency);
                }
            } else if (!this.appState.userInfo.guest) {
                const currency = this.appState.user.getRealBalanceCurrency();
                if (currency && caseSettings.fs[currency.code].bet) {
                    return formatCurrencyAmount(Long.fromNumber(caseSettings.fs[currency.code].bet), currency);
                }
            }
        }
        console.warn('Wager Bonus: FreeSpins bet per line is missing');
        return null;
    }
}
