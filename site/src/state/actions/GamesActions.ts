import * as currencies from '../utils/Currencies';
import {action, computed, observable} from 'mobx';
import {commons, games, messages, site, users, proto} from '../../api/proto';
import AppState from '../AppState';
import {NotificationItemLevel} from '../components/Notificator';
import {GameGroupId, POSSIBLE_LOBBY_GAMES, NString, LobbyGame} from '../meta';
import {isMobile} from '../utils/BrowserUtils';
import {
    applyGamesFilters,
    fillGamesToCount,
    filterEmptyIds,
    FilterListItem,
    findGroupById,
    GameFilterMeta,
    getGameImage,
    hasHTMLVersion,
    isBelongsToGroup,
    isGameSuitableForAge,
    isGameSuitableForCurrency,
    isMeetsAppSettings,
    matchSearchQuery,
    sortGames,
    isProvidersGroup,
    sortGamesAlphabetically,
    getRandGameIndex,
    swapGamesByIndex,
} from '../utils/GameUtils';
import { differenceInYears } from 'date-fns';
import { Callback } from '../../api/APIConnector';

const FAVORITES_TAG = 'player_favorite_games';
const MIN_CATEGORY_LIMIT = 25;

const VIEW_OPTIONS = ['view-full', 'view-half', 'view-one-third'] as const;
export type ViewOption = typeof VIEW_OPTIONS[number];

export interface GameItem extends games.IGame {
    isFavorite?: boolean;
    isFavoriteActionLoading?: boolean;
    meta?: proto.icon.IMetaDataEntry[];
    metaForShow?: proto.icon.IMetaDataEntry[];
}

export interface GameItemWrapper {
    content: AppState;
    game: GameItem;
    ref?: (e: HTMLDivElement) => void;
    loadImage?: boolean;
}

interface GamesCategory {
    categoryId?: number;
}

export enum GameImageSize {
    SIZE_300x300 = '300x300',
    SIZE_300x188 = '300x188',
    SIZE_40x40 = '40x40',
}

export default class GamesActions {
    @observable gamesList: GameItem[] = [];
    @observable groupId: string | null = null;

    @observable isTopGamesLoading: boolean = false;
    @observable topGames: GameItem[] | null = null;
    @observable topGamesStats: users.UserGamesStatsResponse.IGameStats[] | null = null;
    @observable topGamesStatsMap: { [key: string]: users.UserGamesStatsResponse.IGameStats } | null = null;
    randomGamesCache: {[key: string]: GameItem[]} | null = null;

    @observable launchGameError: string | null = null;
    @observable launchGameProviderError: string | null = null;
    @observable showSupportedCurrenciesForGame: boolean = false;
    providers: Set<string>;

    @observable heightSport2: number = 0;

    @observable gameIndexToShow: number = 20;

    viewOptions = VIEW_OPTIONS;
    @observable view?: ViewOption = "view-half";

    isAfterLogin: boolean = false;

    private gamesMapById: { [key: string]: GameItem } = {};
    private appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;
        this.onToggleGameFavorites = this.onToggleGameFavorites.bind(this);
        if (appState.appSettings.games?.showMoreGames?.enabled && appState.appSettings.games?.showMoreGames?.gamesForLoad) {
            this.gameIndexToShow = appState.appSettings.games.showMoreGames.gamesForLoad;
        }
        if (appState.appSettings.games?.defaultGroup) {
            this.groupId = appState.appSettings.games?.defaultGroup;
        }
    }

    @computed
    get openGameInTheSameTab(): boolean {
        return !this.appState.isMobile || !!this.appState.appSettings.games.openGameInTheSameTabOnMobile;
    }

    @computed
    get prevPageUrl(): string {
        if (window.location.href === document.referrer || !document.referrer.includes(window.location.origin)) {
            return window.location.origin;
        }
        return document.referrer;
    }

    @computed
    get availableGames(): GameItem[] {
        const filtersList: FilterListItem[] = [filterEmptyIds];
        const gamesList = this.gamesList || [];
        const filtersMeta: GameFilterMeta = {
            enabled: this.appState.appSettings.games.enabled,
            disabled: this.appState.appSettings.games.disabled
        };
        if (isMobile()) {
            filtersList.push(hasHTMLVersion);
        }

        // filter by currency
        const userInfo = this.appState.userInfo;
        const isRealBalance = userInfo.balance && userInfo.balance.currencyId!.toNumber() !== 0;
        if (isRealBalance) {
            filtersMeta.currencyCode = userInfo.realBalance!.currencyCode;
            filtersList.push(isGameSuitableForCurrency);
        }

        const rawUserInfo = this.appState.rawUserInfo;
        if (rawUserInfo) {
            const fields = rawUserInfo.fields;
            const key = 'date-of-birth';
            let value = fields && fields[key] ? fields[key] : null;

            if (value && value.datetimeValue) {
                const birthday = new Date(value.datetimeValue);
                filtersMeta.userAge = differenceInYears(Date.now(), birthday);
            }
        }
        // filter by user age
        filtersList.push(isGameSuitableForAge);

        // filter by app settings
        filtersList.push(isMeetsAppSettings);

        // apply filters
        return applyGamesFilters(gamesList, filtersList, filtersMeta);
    }

    @action
    showMoreGames = () => {
        if (this.appState.appSettings.games?.showMoreGames?.gamesForLoad) {
            this.gameIndexToShow += this.appState.appSettings.games?.showMoreGames?.gamesForLoad;
        }
    }

    @computed
    get filteredGames(): GameItem[] {
        const gamesSorting = this.appState.appSettings.games.sorting;
        const filtersList: FilterListItem[] = [filterEmptyIds];
        const availableGames = this.availableGames || [];
        const filtersMeta: GameFilterMeta = {
            enabled: this.appState.appSettings.games.enabled,
            disabled: this.appState.appSettings.games.disabled
        };

        // filter by group
        const group = findGroupById(this.appState.siteConfig?.gameMenu, this.groupId);
        const groupId = group ? group.id : null;
        if (group) {
            filtersMeta.group = group;
            filtersList.push(isBelongsToGroup);
        }

        // apply filters
        let result = applyGamesFilters(availableGames, filtersList, filtersMeta);

        // popular games tab can't contain less than limit(default 25) games
        if (group && group.id === GameGroupId.POPULAR && result.length < MIN_CATEGORY_LIMIT) {
            fillGamesToCount(result, availableGames, gamesSorting, MIN_CATEGORY_LIMIT, group.id, filtersMeta);
            if (this.appState.searchValue) {
                result = applyGamesFilters(result, [matchSearchQuery], filtersMeta);
            }
        }

        // sort games
        if (gamesSorting.providersCategoryAlphabetically && isProvidersGroup(this.appState.siteConfig.gameMenu, group)) {
            sortGamesAlphabetically(result, this.appState.language);
        } else {
            sortGames(result, gamesSorting, group ? group.id : 'all');
        }

        // randomize games at indexes
        const indexes = this.appState.appSettings.games.randomizeIndexes;
        const isMainPage = !groupId || groupId === GameGroupId.ALL;
        if (isMainPage && !this.appState.searchValue && indexes && indexes.length) {
            result = this.randomizeGames(result, group ? group.id : null, indexes);
        }

        if (this.isAfterLogin) {
            this.isAfterLogin = false;
        }

        return result;
    }

    @computed
    get displayedGames(): GameItem[] {
        const displayedGames = this.filteredGames || [];
        if (this.appState.searchValue) {
            const filtersList: FilterListItem[] = [];
            const filtersMeta: GameFilterMeta = {};
            try {
                filtersMeta.searchRe = new RegExp(this.appState.searchValue, 'i');
                filtersList.push(matchSearchQuery);
            } catch (e) {
                console.debug('Wrong symbol in search query: ', this.appState.searchValue);
            }
            return applyGamesFilters(displayedGames, filtersList, filtersMeta);
        } else {
            return displayedGames;
        }
    }

    @computed
    get popularGames(): GameItem[] {
        const gamesSorting = this.appState.appSettings.games.sorting;
        const filtersList: FilterListItem[] = [filterEmptyIds];
        const availableGames = this.availableGames || [];
        const filtersMeta: GameFilterMeta = {
            enabled: this.appState.appSettings.games.enabled,
            disabled: this.appState.appSettings.games.disabled
        };

        // apply filters
        let result = applyGamesFilters(availableGames, filtersList, filtersMeta);

        fillGamesToCount(result, availableGames, gamesSorting, MIN_CATEGORY_LIMIT, GameGroupId.POPULAR, filtersMeta);

        // sort games
        sortGames(result, gamesSorting, GameGroupId.POPULAR);

        return result;
    }

    @computed
    get popularAvailableGames(): GameItem[] {
        return this.popularGames.filter((g) => !g.disableIfActiveBonus);
    }

    findGameById(gameId: string | null | undefined): GameItem | null {
        return gameId && this.gamesMapById[gameId] ? this.gamesMapById[gameId] : null;
    }

    findGameIndexById(gameId: string | null | undefined): number {
        return gameId ? this.gamesList.findIndex((g: GameItem) => g.id === gameId) : -1;
    }

    gameUrl(slug: string): string {
        return `/game/${slug}`;
    }

    isLobbyPage(slug: string): boolean {
        if (POSSIBLE_LOBBY_GAMES.includes(slug as LobbyGame)) {
            return true;
        }
        switch (slug) {
            case 'tvbet':
            case 'sport':
            case 'pilot':
                return true;
        }
        return false;
    }

    url(slug: string): string {
        if (this.isLobbyPage(slug)) {
            return this.appState.l(`/${slug}`);
        }
        return this.appState.l(`/game/${slug}`);
    }

    getGameImage(game: GameItem | null | undefined, size?: GameImageSize): string | undefined {
        return getGameImage(game, size);
    }

    getGameName(gameOrGameId: GameItem | string | null | undefined): string {
        if (gameOrGameId) {
            let game = null;
            if (typeof gameOrGameId === 'string') {
                game = this.findGameById(gameOrGameId);
            } else {
                game = gameOrGameId as GameItem;
            }

            return game && game.name && this.appState.language in game.name ? game.name[this.appState.language] : '';
        } else {
            return '';
        }
    }

    @computed
    get mapOfGamesByCategory(): GamesCategory {
        const appState = this.appState;
        const categories = appState.siteConfig.gameMenu!;
        const availableGames = this.availableGames;
        let mapOfGamesByCategory: GamesCategory = {};

        // set the number of games for category
        availableGames.forEach(game => {
            categories.forEach((group: site.IGameMenuEntry) => {
                if (group.id) {
                    let filtersMeta: GameFilterMeta = {
                        group: group
                    };

                    if (group.children && group.children.length) {
                        group.children.forEach(children => {
                            if (children && children.id) {
                                filtersMeta.group = children as site.IGameMenuEntry;
                                if (isBelongsToGroup(game, filtersMeta)) {
                                    if (mapOfGamesByCategory[children.id]) {
                                        mapOfGamesByCategory[children.id] += 1;
                                    } else {
                                        mapOfGamesByCategory[children.id] = 1;
                                    }

                                    if (mapOfGamesByCategory[group.id!]) {
                                        mapOfGamesByCategory[group.id!] += 1;
                                    } else {
                                        mapOfGamesByCategory[group.id!] = 1;
                                    }
                                }
                            }
                        });
                    } else {
                        const limit = (group && group.limit) || MIN_CATEGORY_LIMIT;
                        if (group.id === GameGroupId.POPULAR || group.id === GameGroupId.FAVORITES) {
                            // popular/favorites category
                            mapOfGamesByCategory[group.id] = limit;
                        } else if (isBelongsToGroup(game, filtersMeta)) {
                            // other categories
                            if (mapOfGamesByCategory[group.id]) {
                                mapOfGamesByCategory[group.id] += 1;
                            } else {
                                mapOfGamesByCategory[group.id] = 1;
                            }
                        }
                    }
                }
            });
        });

        return mapOfGamesByCategory;
    }

    // region ---- games
    @action
    fetchGames(isAfterLogin?: boolean, cb?: Callback) {
        this.appState.api.fetchGames((msg, act) => {
            this.onFetchGames(msg, act, isAfterLogin);
            cb && cb(msg, act);
        });
    }

    @action.bound
    onFetchGames(msg: messages.IServerResponse, act: messages.IClientActionResponse, isAfterLogin?: boolean) {
        if (act.siteConfig) {
            this.appState.processApi(msg, act, isAfterLogin);
            this.isAfterLogin = isAfterLogin || false;
            let gamesWithMeta: games.IGame[] = [];
            this.appState.games.gamesList.forEach(game => {
                if (game.id) {
                    this.gamesMapById[game.id] = game;
                }
                if (game.iconMetaUrl) {
                    gamesWithMeta.push(game);
                }
            });
            if (this.appState.gameMetaIconActions.fetchingStarted) {
                this.appState.gameMetaIconActions.fetchingStarted = false;
                this.appState.gameMetaIconActions.resetData(true);
                setTimeout(() => this.appState.gameMetaIconActions.gamesMetaInit(gamesWithMeta), 1000);
            } else {
                this.appState.gameMetaIconActions.gamesMetaInit(gamesWithMeta)
            }
        }
    }
    // ---- endregion

    // region ---- favorites games
    @action
    fetchFavoriteGames() {
        this.appState.api.fetchFavoriteGames(this.onFetchFavoriteGames.bind(this));
    }

    @action
    onFetchFavoriteGames(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        if (act.favGames) {
            this.processFavGames(act.favGames.favGames);
        }
    }

    @action
    processFavGames(favGames: string[] | null | undefined) {
        if (favGames) {
            this.gamesList = this.updateGamesListWithFavorites(this.gamesList, favGames);

            const randomGamesCache = this.randomGamesCache;
            if (randomGamesCache) {
                const cacheKeys = Object.keys(randomGamesCache);
                cacheKeys.forEach((key: string) => {
                    if (randomGamesCache[key]) {
                        randomGamesCache[key] = this.updateGamesListWithFavorites(randomGamesCache[key], favGames);
                    }
                })
            }

            if (this.appState.runningGameStore.runningGame && this.appState.runningGameStore.runningGame.id) {
                this.appState.runningGameStore.runningGame.isFavorite = favGames.indexOf(this.appState.runningGameStore.runningGame.id) !== -1;
            }
        }
    }

    updateGamesListWithFavorites(gamesList: GameItem[], favGames: string[]): GameItem[] {
        return gamesList.map(function (game: GameItem) {
            const isFavorite = !!(game.id && favGames.indexOf(game.id) !== -1);
            game.isFavorite = isFavorite;

            if (game.tags) {
                const tagIndex = game.tags.indexOf(FAVORITES_TAG);
                if (isFavorite && tagIndex === -1) {
                    game.tags.push(FAVORITES_TAG);
                } else if (!isFavorite && tagIndex !== -1) {
                    game.tags.splice(tagIndex, 1);
                }
            } else if (isFavorite) {
                game.tags = [FAVORITES_TAG];
            }
            return game;
        });
    }

    @action
    addGameToFavorites(gameId: string) {
        const game = this.findGameById(gameId);

        if (game) {
            this.setGameFavoritesActionLoading(game, true);

            setTimeout(() => {
              this.setGameFavoritesActionLoading(game, false);
            }, 2000)
            this.appState.api.addGameToFavorites(gameId, this.onAddGameToFavorites.bind(this, game));
        }
    }

    onAddGameToFavorites(game: GameItem, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);


        if (act.favGames && act.favGames.addToFav && act.favGames.addToFav.ok) {
            this.processFavGames(act.favGames.favGames);
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-add-game-to-favorites-success', {'game-name': this.getGameName(game)})
            });
        } else {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.ERROR,
                message: this.appState.t('ui-add-game-to-favorites-error', {'game-name': this.getGameName(game)})
            });
        }
    }

    @action
    removeGameFromFavorites(gameId: string) {
        const game = this.findGameById(gameId);
        if (game) {
            this.setGameFavoritesActionLoading(game, true);
            setTimeout(() => {
              this.setGameFavoritesActionLoading(game, false);
            }, 2000)
            this.appState.api.removeGameFromFavorites(gameId, this.onRemoveGameToFavorites.bind(this, game));
        }
    }

    onRemoveGameToFavorites(game: GameItem, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);


        if (act.favGames && act.favGames.removeFromFav && act.favGames.removeFromFav.ok) {
            this.processFavGames(act.favGames.favGames);
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-remove-game-from-favorites-success', {'game-name': this.getGameName(game)})
            });
        } else {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.ERROR,
                message: this.appState.t('ui-remove-game-from-favorites-error', {'game-name': this.getGameName(game)})
            });
        }
    }

    @action
    setGameFavoritesActionLoading(game: GameItem, value: boolean) {
        game.isFavoriteActionLoading = value;
        this.gamesList = this.gamesList.slice();
        if (this.appState.runningGameStore.runningGame && this.appState.runningGameStore.runningGame.id === game.id) {
            this.appState.runningGameStore.runningGame = {...this.appState.runningGameStore.runningGame, isFavoriteActionLoading: value};
        }
    }

    @action
    onToggleGameFavorites(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        e.stopPropagation();

        const gameId = e.currentTarget.getAttribute('data-id');
        const game = this.findGameById(gameId);

        if (game && !game.isFavoriteActionLoading) {
            if (game.isFavorite) {
                this.removeGameFromFavorites(gameId!);
            } else {
                this.addGameToFavorites(gameId!);
            }
        }
    }

    // endregion

    // region ---- top games/recommended games
    @action
    fetchPlayerTopGames() {
        this.isTopGamesLoading = true;
        this.appState.api.fetchPlayerTopGames(25, this.onFetchPlayerTopGames.bind(this));
    }

    @action
    onFetchPlayerTopGames(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isTopGamesLoading = false;
        this.appState.processApi(msg, act);

        if (act.gamesStats && act.gamesStats.top) {
            const result = [];
            const map = {};

            for (let entry of act.gamesStats.top) {
                let game = this.findGameById(entry.gameId!);
                if (game) {
                    map[game.id!] = entry;
                    result.push(game);
                }
            }
            this.topGames = result;
            this.topGamesStats = act.gamesStats.top;
            this.topGamesStatsMap = map;
        } else {
            this.topGames = [];
            this.topGamesStats = [];
            this.topGamesStatsMap = {};
        }
    }

    @computed
    get recommendedToFavoritesGames(): GameItem[] {
        const minGamesCount = 10;
        const maxGamesCount = 25;

        // first  - try to get recommended games from top user games
        let result = this.topGames ? this.topGames.filter((g: GameItem) => !g.isFavorite) : [];

        // sort games by bets count
        const topGamesStatsMap = this.topGamesStatsMap;
        if (topGamesStatsMap) {
            result.sort((a: games.IGame, b: games.IGame) => {
                const aStats = topGamesStatsMap[a.id!];
                const bStats = topGamesStatsMap[b.id!];

                const aValue = aStats.betsCount ? aStats.betsCount.toNumber() : 0;
                const bValue = bStats.betsCount ? bStats.betsCount.toNumber() : 0;

                return bValue - aValue;
            });
        }

        // second  - try to fill recommended games from popular
        // sort popular games using popular games sorting rules
        if (result.length < minGamesCount) {
            const popularGames = this.gamesList.filter((g: GameItem) => {
                return !g.isFavorite && g.popularity24h && g.popularity24h > 0 && result.find((rg: GameItem) => rg.id === g.id) == null;
            });
            sortGames(popularGames, this.appState.appSettings.games.sorting, GameGroupId.FAVORITES);
            result = result.concat(popularGames.slice(result.length, minGamesCount));
        }

        // third - if there is not enough popular games, any games
        // sort games using new games sorting rules
        if (result.length < minGamesCount) {
            const anyGames = this.gamesList.filter((g: GameItem) => {
                return !g.isFavorite && result.find((rg: GameItem) => rg.id === g.id) == null;
            });
            sortGames(anyGames, this.appState.appSettings.games.sorting, GameGroupId.NEW);
            result = result.concat(anyGames.slice(result.length, minGamesCount));
        }

        return result.slice(0, maxGamesCount);
    }

    @computed
    get supportedCurrenciesForRunningGame(): string[] | null {
        if (this.showSupportedCurrenciesForGame) {
            return this.getSupportedCurrenciesForGame(this.appState.runningGameStore.runningGame);
        }

        return null;
    }

    getSupportedCurrenciesForGame(game: games.IGame | null | undefined): string[] | null {
        const siteCurrencies = currencies.getSupportedCurrencies();
        const gameCurrencies = game ? game.currencies : null;
        let supportedCurrencies: string[] = [];

        if (siteCurrencies && gameCurrencies) {
            siteCurrencies.forEach(function (currency: commons.ICurrency) {
                if (currency.id!.toNumber() !== 0 && gameCurrencies.indexOf(currency.code!) !== -1) {
                    supportedCurrencies.push(currency.code!);
                }
            });
        }

        return supportedCurrencies.length ? supportedCurrencies : null;
    }

    @computed
    get showGamesListSearchMsg(): boolean {
        return this.appState.searchValue.length !== 0 && this.displayedGames.length === 0;
    }

    getGameItemClasses = (game: games.IGame, inactive?: boolean): string => {
        const classes = [];

        if (inactive) {
            classes.push('game-item--inactive')
        }

        if (game.disableIfActiveBonus) {
            classes.push('game-item--showPreview');
        }

        return classes.join(' ');
    }

    getPreviewGameText = (game: games.IGame) => {
        if (game.disableIfActiveBonus) {
            return this.appState.t('error-game-disable-by-active-bonus');
        }

        return '';
    }

    @action.bound
    setGroupId(groupId: string | null) {
        this.appState.search.close();
        this.groupId = groupId;
        if (this.appState.appSettings.games?.showMoreGames?.enabled && this.appState.appSettings.games?.showMoreGames?.gamesForLoad) {
            this.gameIndexToShow = this.appState.appSettings.games.showMoreGames.gamesForLoad;
        }
    }

    getFrameHeight() {
        const screenHeight = window.innerHeight;

        return document.getElementsByClassName('site-header')[0]
          ? screenHeight - document.getElementsByClassName('site-header')[0].clientHeight
          : screenHeight;
    }
    // endregion

    @action
    setGameWithMeta(gameWithMeta: GameItem) {
        this.gamesList = this.gamesList.map((game) => {
            if (game.id === gameWithMeta.id) {
                return gameWithMeta;
            }
            return game;
        });
    }

    randomizeGames(games: GameItem[], groupId: NString, indexes: number[]): GameItem[] {
       const cacheKey = groupId || GameGroupId.ALL;
       const cache = this.randomGamesCache ? this.randomGamesCache[cacheKey] : null;
       if (!cache || this.isAfterLogin) {
           const result = games.slice();
           const length = result.length;

           // set random games for indexes
           indexes.forEach(function(index: number) {
               const game = result[index];
               if (game) {
                   const rand = getRandGameIndex(index, length);
                   swapGamesByIndex(result, index, rand);
               }
           });

           // cache result
           if (!this.randomGamesCache) {
               this.randomGamesCache = {};
           }
           this.randomGamesCache[cacheKey] = result;
           return result;
       } else {
           return cache;
       }
    }

    @action.bound
    updateViewOption(value: ViewOption) {
        if (VIEW_OPTIONS.indexOf(value) + 1 >= VIEW_OPTIONS.length) {
            this.view = VIEW_OPTIONS[0];
        } else {
            this.view = VIEW_OPTIONS[VIEW_OPTIONS.indexOf(value) + 1]
        }
    }
}
