import { action, runInAction, computed } from 'mobx';
import AppState from '../AppState';
import { SitePages } from '../meta';

export default class PagesActions {

    constructor(private appState: AppState) {
        this.appState = appState;
    }

    @action
    load(pageId: string, pageType?: SitePages, disallowNoMatchPage?: boolean) {
        this.appState.page = pageType || SitePages.STATIC;

        if (this.appState.staticPageId === pageId) {
            return;
        }

        this.appState.staticPageLoading = true;
        this.appState.staticPageId = pageId;

        this.appState.api.loadStaticPage(pageId, (msg, act) => {
            const found = act.staticPage!.found!;
            if (found === false) {
                runInAction(() => {
                    this.appState.staticPageLoading = false;
                    if (!disallowNoMatchPage) {
                        this.appState.pages.triggerNoMatchPage();
                    }
                });
            } else {
                runInAction(() => {
                    this.appState.staticPageLoading = false;
                    this.appState.staticPageContent = {__html: act.staticPage!.content!};
                });
            }
        });
    }

    @action
    triggerNoMatchPage() {
        this.appState.page = SitePages.ERROR_404;
        this.appState.routeParams = null;
        this.appState.site.onPageOpen('404');
    }

    @action.bound
    handleClicks(e: React.MouseEvent<Element>) {
        const a = (e.target as Element).closest("a");
        if (a && a.href) {
            e.preventDefault();
            const url: string = a.href;
            if (
                url.indexOf(window.location.origin) !== -1 &&
                a.target !== "_blank"
            ) {
                this.appState.router.push(
                    this.appState.l(url.replace(window.location.origin, ""))
                );
            } else {
                window.open(url, "_blank");
            }
        }
    }

    @computed
    get isPageWithEmbeddedContent(): boolean {
        return this.appState.page !== null ? [
            SitePages.GAME,
            SitePages.SPORT,
            SitePages.LIVE_CASINO,
            SitePages.LOBBY,
            SitePages.PILOT,
            SitePages.LOBBY_GAME,
        ].includes(this.appState.page as SitePages) : false;
    }
}