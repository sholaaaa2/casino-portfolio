import AppState from "../../AppState";
import { IChat } from "./Chat";
import { initTawk } from "../../utils/ThirdParty";

export default class TawkChat implements IChat {
    private currentUserLogin?: string;

    constructor(
        public appState: AppState,
        chatId: string,
        chatId2: string,
        startVisible: boolean
    ) {
        try {
            initTawk(chatId, chatId2);
        } catch(error) {
            console.error(error);
        }

        window.Tawk_API = window.Tawk_API || {};
        window.Tawk_API.onLoad = () => {
            try {
                if (startVisible) {
                    window.Tawk_API.showWidget();
                } else {
                    window.Tawk_API.hideWidget();
                }
            } catch(error) {
                console.error(error);
            }
        };
    }

    updateInfo() {
        const { userInfo, rawUserInfo } = this.appState;
        if (window.Tawk_API && window.Tawk_API.setAttributes && this.currentUserLogin !== userInfo.login) {
            const userId = userInfo.userId.toString();
            const userEmail =
                rawUserInfo && rawUserInfo.email
                    ? rawUserInfo.email
                    : `${userId}@${this.appState.site.siteDomain}`;
            const attrParams = {
                name: `Login:${userInfo.login} ID:${userId}`,
                hash: userId,
                email: userEmail
            };
            try {
                window.Tawk_API.setAttributes(attrParams, (error: any) => {
                    if (error) {
                        console.error(error);
                    }
                });
            } catch(error) {
                console.error(error);
            }
            this.currentUserLogin = userInfo.login;
        }
    }

    expand() {
        if (window.Tawk_API && window.Tawk_API.maximize && window.Tawk_API.showWidget) {
            try {
                window.Tawk_API.showWidget();
                window.Tawk_API.maximize();
            } catch(error) {
                console.error(error);
            }
        }
    }
}
