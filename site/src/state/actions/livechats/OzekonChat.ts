import AppState from "../../AppState";
import { IChat } from "./Chat";
import { formatRegistrationLevel } from "../../utils/BrowserUtils";
import { initOzekon } from "../../utils/ThirdParty";
import { action, observable } from "mobx";

export default class OzekonChat implements IChat {
    constructor(
        public appState: AppState,
        chatUrl: string,
        chatId: string,
        startVisible: boolean
    ) {
        window.owcWidgetCB = () => {
            try {
                window.owcWidget.set("box.onShow", this.updateInfo);
                window.owcWidget.set("box.onExpand", this.updateInfo);
                window.owcWidget.set("chat.onBeginConversation", this.updateInfo);
                window.owcWidget.set("chat.onMessageToOperator", this.updateInfo);
                if (!startVisible) {
                    window.owcWidget.set("box.hide");
                    window.owcWidget.set("box.onShrink", () => {
                        window.owcWidget.set("box.hide");
                    });
                }
                this.updateInfo();
                window.owcWidget.set("chat.onInit", this.handleChatStatusUpdate);
                window.owcWidget.set("chat.onUpdate", this.handleChatStatusUpdate);
            } catch (error) {
                console.error(error);
            }
        }
        initOzekon(chatUrl, chatId, startVisible);
    }

    updateInfo = () => {
        const owcWidget = window.owcWidget;
        const userInfo = this.appState.userInfo;
        const user = this.appState.rawUserInfo;

        try {
            if (owcWidget) {
                const metaData: string[] = [
                    "Project: WagerGun",
                    "Domain: " + window.location.hostname
                ];
                const userData: {
                    userId?: string | null;
                    phoneNumber?: string | null;
                    email?: string | null;
                    metadata?: string | object | null;
                    firstName?: string | null;
                    lastName?: string | null;
                } = {};

                if (userInfo) {
                    userData.userId = userInfo.userId.toString();
                    userData.firstName = `wl_${userInfo.login}_${userInfo.userId}`;
                    metaData.push("User ID: " + userInfo.userId);
                }

                if (user) {
                    metaData.push(
                        "Registration level: " +
                        formatRegistrationLevel(user.registrationLevel)
                    );

                    if (user.phone && user.phone !== "none") {
                        userData.phoneNumber = user.phone;
                        metaData.push("Phone: " + user.phone);
                        metaData.push(
                            "Phone status: " +
                            (user.phoneConfirmed ? "Confirmed" : "Unconfirmed")
                        );
                    }

                    if (user.email && user.email !== "none") {
                        userData.email = user.email;
                        metaData.push("Email: " + user.email);
                        metaData.push(
                            "Email status: " +
                            (user.emailConfirmed ? "Confirmed" : "Unconfirmed")
                        );
                    }
                }

                userData.metadata = metaData;
                owcWidget.set("visitor.update", userData);
            }
        } catch (error) {
            console.error(error);
        }
    };

    expand() {
        try {
            if (window.owcWidget) {
                window.owcWidget.set("box.expand");
            }
        } catch (error) {
            console.error(error);
        }
    }

    @observable unreadCount: number = 0;

    @action handleChatStatusUpdate = (e: { unreadCount: number }) => {
        this.unreadCount = e.unreadCount || 0;
    };
}
