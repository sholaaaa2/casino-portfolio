import AppState from "../../AppState";
import { IChat } from "./Chat";
import { formatRegistrationLevel } from "../../utils/BrowserUtils";
import { initOlark } from "../../utils/ThirdParty";

export default class OlarkChat implements IChat {
    constructor(
        public appState: AppState,
        chatId: string,
        startVisible: boolean
    ) {
        const groupId = this.appState.g('%CHAT_ID_OLARK_GROUP%');
        initOlark(chatId, startVisible, groupId);
        if (window.olark) {
            window.olark("api.box.onShow", this.updateInfo);
            window.olark("api.box.onExpand", this.updateInfo);
            window.olark("api.chat.onBeginConversation", this.updateInfo);
            window.olark("api.chat.onMessageToOperator", this.updateInfo);
            if (!startVisible) {
                // window.olark("api.box.hide");
                // window.olark("api.box.onShrink", () => {---------------------
                //     window.olark("api.box.hide");
                // });
            }
        }
    }

    updateInfo = () => {
        const olark = window.olark;
        const userInfo = this.appState.userInfo;
        const user = this.appState.rawUserInfo;

        if (olark) {
            const data: string[] = [
                "Project: WagerGun",
                "Domain: " + window.location.hostname
            ];

            if (userInfo) {
                olark("api.visitor.updateFullName", {
                    fullName: `wl_${userInfo.login}_${userInfo.userId}`
                });
                data.push("User ID: " + userInfo.userId);
            }

            if (user) {
                data.push(
                    "Registration level: " +
                        formatRegistrationLevel(user.registrationLevel)
                );
                if (user.phone && user.phone !== "none") {
                    olark("api.visitor.updatePhoneNumber", {
                        phoneNumber: user.phone
                    });
                    data.push("Phone: " + user.phone);
                    data.push(
                        "Phone status: " +
                            (user.phoneConfirmed ? "Confirmed" : "Unconfirmed")
                    );
                }

                if (user.email && user.email !== "none") {
                    olark("api.visitor.updateEmailAddress", {
                        emailAddress: user.email
                    });
                    data.push("Email: " + user.email);
                    data.push(
                        "Email status: " +
                            (user.emailConfirmed ? "Confirmed" : "Unconfirmed")
                    );
                }
            }
            olark("api.chat.updateVisitorStatus", {
                snippet: data
            });
        }
    };

    expand() {
        if (window.olark) {
            window.olark("api.box.expand");
        }
    }
}
