import AppState from "../../AppState";
import { isGPSE } from "../../utils/BrowserUtils";
import OlarkChat from "./OlarkChat";
import TawkChat from "./TawkChat";
import SmartsuppChat from "./SmartsuppChat";
import OzekonChat from "./OzekonChat";

export interface IChat {
    appState: AppState;
    updateInfo: () => void;
    expand: () => void;
    unreadCount?: number;
}

export class Chat implements IChat {
    protected active?: OlarkChat | TawkChat | OzekonChat | SmartsuppChat;

    constructor(public appState: AppState) {}

    init() {
        if (!isGPSE()) {
            const CHAT_ID_OLARK = this.appState.g('%CHAT_ID_OLARK%');
            const CHAT_ID_TAWK = this.appState.g('%CHAT_ID_TAWK%');
            const CHAT_ID_2_TAWK = this.appState.g('%CHAT_ID_2_TAWK%') ? this.appState.g('%CHAT_ID_2_TAWK%') : 'default';
            const CHAT_ID_SMARTSUPP = this.appState.g('%CHAT_ID_SMARTSUPP%');
            const CHAT_ID_OZEKON = this.appState.g('%CHAT_ID_OZEKON%');
            const CHAT_START_VISIBLE = !!this.appState.g('%CHAT_START_VISIBLE%');
            if (CHAT_ID_OLARK) {
                this.active = new OlarkChat(this.appState, CHAT_ID_OLARK, CHAT_START_VISIBLE);
            } else if (CHAT_ID_TAWK) {
                this.active = new TawkChat(this.appState, CHAT_ID_TAWK, CHAT_ID_2_TAWK, CHAT_START_VISIBLE);
            } else if (CHAT_ID_SMARTSUPP) {
                this.active = new SmartsuppChat(this.appState, CHAT_ID_SMARTSUPP, CHAT_START_VISIBLE)
            } else if (CHAT_ID_OZEKON) {
                const CHAT_LOADER_URL_OZEKON = this.appState.g('%CHAT_LOADER_URL_OZEKON%');
                this.active = new OzekonChat(this.appState, CHAT_LOADER_URL_OZEKON, CHAT_ID_OZEKON, CHAT_START_VISIBLE);
            }
        }
    }

    updateInfo() {
        if (this.active) {
            this.active.updateInfo();
        }
    }

    expand() {
        if (this.active) {
            this.active.expand();
        }
    }

    get unreadCount() {
        if (this.active instanceof OzekonChat) {
            return this.active?.unreadCount || 0;
        }
        return 0;
    }
}
