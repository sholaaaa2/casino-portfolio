import AppState from "../../AppState";
import { IChat } from "./Chat";
import { initSmartsupp } from "../../utils/ThirdParty";

export default class SmartsuppChat implements IChat {

    constructor(
        public appState: AppState,
        chatId: string,
        startVisible: boolean,
    ) {
        initSmartsupp(chatId);
        window.smartsupp('on', 'startup', () => {
            if (startVisible) {
              window.smartsupp('chat:open');
            } else {
              window.smartsupp('chat:close');
            }
          })
    }

    updateInfo() {
        const { userInfo, rawUserInfo } = this.appState;
        const smart = window.smartsupp;
        smart("language", this.appState.language); // Chat language
        smart('name', userInfo.login);
        smart('email', rawUserInfo!.email ? rawUserInfo!.email : `${userInfo.userId.toString()}@${window.location.hostname}`);
        smart('variables', {
            userId: {
                label: 'User ID ',
                value: userInfo.userId.toString()
            },
            role: {
                label: 'Подтверждение',
                value: rawUserInfo!.registrationLevel === 2 ? ': Да' : ': Hет'
            },
            orderedPrice: {
                label: 'Баланс',
                value: `${userInfo.balanceStringWithoutCurrency} ${userInfo.currencyString}`
            }
        });
    }

    expand() {
        if (window.smartsupp) {
            window.smartsupp('chat:open');
        }
    }
}
