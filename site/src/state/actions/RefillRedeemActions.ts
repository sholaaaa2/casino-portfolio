import { action, computed, observable } from "mobx";
import AppState from "../AppState";

enum REFILL_REDEEM_LOGOUT_STATE {
  LOGOUT_BTN,
  CONFIRMATION
}

export class RefillRedeemActions {

    constructor(protected appState: AppState) {}

    @observable refillRedeemLogoutState: REFILL_REDEEM_LOGOUT_STATE = REFILL_REDEEM_LOGOUT_STATE.LOGOUT_BTN;

    @action
    refillRedeemLogoutChangeStateToConfirm = () => {
        this.refillRedeemLogoutState = REFILL_REDEEM_LOGOUT_STATE.CONFIRMATION;
    }

    @action
    refillRedeemLogoutChangeStateToShowButton = () => {
        this.refillRedeemLogoutState = REFILL_REDEEM_LOGOUT_STATE.LOGOUT_BTN;
    }

    @computed
    get refillRedeemLogoutIsShowConfirm() {
        return this.refillRedeemLogoutState === REFILL_REDEEM_LOGOUT_STATE.CONFIRMATION;
    }

    @action
    openRefillRedeemDialog = () => {
        this.appState.modal.showRefillRedeemModal();
        this.appState.geo.sendGeolocation((isSuccess: boolean, error?: string) => {});
    }

    @computed
    get isEnabled() {
        return this.appState.appSettings.refillAndRedeemInstructionModal &&
          this.appState.site.isAuthorized &&
          this.appState.isMobileOnly &&
          this.appState.rawUserInfo?.displayName
    }
}
