import * as currencies from '../utils/Currencies';
import {action, observable, observe} from 'mobx';
import {bonuses, messages} from '../../api/proto';
import {formatCurrencyAmount, formatCurrencyCode} from '../utils/Money';
import {calcWagerMultiplier} from '../utils/Math';
import Long from 'long';
import {BonusMeta, BonusMetaObj, BonusType, getBonusActivationErrorToken, processString, BonusFamily} from '../../api/utils';
import {NotificationItemLevel} from '../components/Notificator';
import {v4 as uuid} from 'uuid';
import {runBonusConfetti} from '../../utils/effects';
import {AnimationModes, Modal, NotificationAnimationClass, NString, SitePages} from '../meta';
import {findGame} from '../utils/GameUtils';
import AppState from '../AppState';
import {TournamentMeta} from './tournaments/TournamentsActions';
import { getSelectedCardClassName } from '../utils/getSelectedCardClassName';
import { UsaRefInviteByCashierBonusMeta } from './BonusAppearance';
import { BonusCodeNotification } from './bonuses/BonusCodeNotification';

const bonusesImages = [
    '/images/bonuses/777.svg',
    '/images/bonuses/Cards.svg',
    '/images/bonuses/Casino.svg',
    '/images/bonuses/Chips.svg',
    '/images/bonuses/Cigare.svg',
    '/images/bonuses/Coins.svg',
    '/images/bonuses/Crown.svg',
    '/images/bonuses/Crupie.svg',
    '/images/bonuses/Diamond.svg',
    '/images/bonuses/dice.svg',
    '/images/bonuses/Girl.svg',
    '/images/bonuses/Jocker.svg',
    '/images/bonuses/Money.svg',
    '/images/bonuses/Pocket.svg',
    '/images/bonuses/Wine.svg'
];

export type BonusNotificationAppearance = { gradientClass: string, image: string, title: string};

export interface BonusCodeNotificationFunds {
    amount: string;
    currency: string;
    amountWithCurrency: string;
    wagerLeft?: string;
    wagerMultiplier?: string;
}

export interface BonusCodeNotificationFreeSpins {
    gameName: string;
    gameImage: string;
    spinsCount: number;
    line?: number;
    bet?: number;
    gameId: string;
}

export interface BonusCodeNotificationFreeBets {
    gameName: string;
    gameImage: string;
    spinsCount: number;
    bet?: number;
    gameId: string;
}

export interface BonusCodeMeta {
    // fields used by free_spins_partial_bonus
    amount?: string;
    currency?: string;
    game?: string;
    type?: string;
    bonus_type?: string;
    popup_meta_need_notify?: string;
    wagerAmount?: string;
    wagerMultiplier?: string;

    // fields used by balance correction
    balanceCorrection?: string;
    balanceBonusAmount?: string;
    freeSpinsWinAmount?: string;
    bonusMaxWinLimit?: string;
    refillAmountForWithdraw?: string;
    newBalance?: string;
    invite_meta?: string;
}

export interface BalanceCorrectionNotificationMessageParams {
    correction?: string;
    balanceBonus?: string;
    freeSpinsBonus?: string;
    maxBonusWinAmount?: string;
    refillAmountForWithdraw?: string;
    newBalance?: string;
}

interface BonusModalNotification {
    title: string;
    description: string;
}

export class BonusesState {
    @observable playedString: string | null = null;
    @observable totalString: string | null = null;
    @observable currencyCode: string | null = null;
    @observable needPlay: boolean = false;
    @observable percentString: string | null = null;

    @observable notificationLoading: boolean = false;
    notifications = observable<BonusCodeNotification>([], { deep: false });
    private isFirstNotificationReceived: boolean = false;
    public bonusModalNotification: BonusModalNotification | null = null;

    constructor(protected appState: AppState) {
        observe(this, 'totalString', () => {
            if (this.isFirstNotificationReceived) {
                this.appState.games.fetchGames();
            } else {
                this.isFirstNotificationReceived = true;
            }
        });
    }

    removeNotification(id: string) {
        const notification = this.notifications.find(_ => _.notificationId === id);
        if (notification) {
            this.notifications.remove(notification);
        }
    }
}

export default class BonusesActions {

    private appState: AppState;

    static getRandomBonusImage() {
        const randomIndex = Math.floor(Math.random() * (bonusesImages.length - 1));
        return bonusesImages[randomIndex];
    }

    static getRandomBonusGradient() {
        const randomIndex = Math.floor((Math.random() * 19) + 1);
        return 'card-popup-bonus--gradient-' + randomIndex;
    }

    constructor(appState: AppState) {
        this.appState = appState;

        this.acceptBonus = this.acceptBonus.bind(this);
        this.refuseBonus = this.refuseBonus.bind(this);
        this.onBonusAcceptResponse = this.onBonusAcceptResponse.bind(this);
        this.showNextNotification = this.showNextNotification.bind(this);
        this.onBonusRefuseResponse = this.onBonusRefuseResponse.bind(this);
        this.setNotificationAnimationClassById = this.setNotificationAnimationClassById.bind(this);
    }

    @action
    processBonusesState(played: Long, total: Long) {
        const balance = this.appState.userInfo.balances.find(b => b.currencyId!.greaterThan(Long.ZERO));

        if (balance) {
            const currency = balance.currency;
            if (currency) {
                this.appState.bonusesInfo.playedString = formatCurrencyAmount(played, currency.id, true);
                this.appState.bonusesInfo.totalString = formatCurrencyAmount(total, currency.id, true);
                this.appState.bonusesInfo.currencyCode = formatCurrencyCode(currency.id);
                this.appState.bonusesInfo.needPlay = total.greaterThan(played);

                if (total.greaterThan(Long.ZERO)) {
                    const percent = Math.floor(played.toNumber() / total.toNumber() * 100).toString();
                    this.appState.bonusesInfo.percentString = parseInt(percent, 10).toString();
                } else {
                    this.appState.bonusesInfo.percentString = '0';
                }
            }
        }
    }

    @action
    processBonusesNotifications(notifications: bonuses.IBonusCodeNotification[]) {
        for (let notification of notifications) {
            if (notification.id && notification.code) {
                if (this.appState.pages.isPageWithEmbeddedContent) {
                    return;
                }

                const appState = this.appState;
                const id = notification.id;
                const bonusCode = notification.code;
                const bonusCodeMeta = bonusCode.meta ?? {};
                let cashierInviteMeta: UsaRefInviteByCashierBonusMeta | undefined;
                // let isUsaRefInviteeBonus = false;

                if (bonusCode?.code && bonusCode?.code === "REFILL_NOTIFICATION") {
                    appState.balanceWager.showModalBonusNotificationId = id;
                    appState.balanceWager.fetchData(true);
                    break;
                }

                // check if bonus code meta contains special
                if (bonusCodeMeta) {
                    if (bonusCodeMeta.type === 'free-spins-game-result' || bonusCodeMeta.type === 'free-bets-game-result') {
                        setTimeout(this.processFreeSpinsResult.bind(this, id, bonusCodeMeta), 2000);
                        return;
                    } else if (bonusCodeMeta.type === 'balance-correction') {
                        setTimeout(this.processBalanceCorrectionResult.bind(this, id, bonusCodeMeta), 2000);
                        return;
                    } else if (
                        bonusCodeMeta.bonus_type === BonusType.REG_WAGER_FORCE_REDEEM &&
                        bonusCodeMeta.popup_meta_need_notify === "true" &&
                        this.appState.modal.activeModal !== Modal.POSITIVE_REG_WAGER_BONUS_CLOSING
                    ) {
                        setTimeout(this.processPositiveRegWagerBonusClosing.bind(this, bonusCodeMeta, id), 2000);
                        return;
                    } else if (bonusCodeMeta[BonusMeta.BONUS_TYPE] === BonusType.TOURNAMENT) {
                        const fundsBonus = this.processBonusNotificationsFunds(bonusCode);
                        if (this.appState.tournamentsActions.isValidTournamentMeta(bonusCode.meta) && fundsBonus) {
                            const tournamentMeta = bonusCodeMeta as TournamentMeta;
                            const tournament = this.appState.tournamentsActions.processTournament(tournamentMeta);
                            this.appState.jackpotWinners.showYouWonTournament(
                                tournament.name,
                                fundsBonus.amountWithCurrency,
                                id
                            );
                        } else {
                            console.error('Unable to show tournament win notifications, missing required params', bonusCode);
                        }
                        return;
                    } else if (bonusCodeMeta[BonusMeta.BONUS_TYPE] === BonusType.USA_REF_INVITEE ||
                        bonusCodeMeta[BonusMeta.BONUS_TYPE] === BonusType.USA_REF_INVITER ||
                        bonusCodeMeta[BonusMeta.BONUS_TYPE] === BonusType.USA_REF_INVITEE_BY_CASHIER) {
                        if (bonusCodeMeta.invite_meta) {
                            const parsedInviteMeta = JSON.parse(bonusCodeMeta.invite_meta);
                            if (parsedInviteMeta) {
                                parsedInviteMeta.className = getSelectedCardClassName(parsedInviteMeta.selectedCard);
                                cashierInviteMeta = parsedInviteMeta;
                            }
                         }
                    }
                }

                // there can be a situation when we receive bonus notification twice
                if (this.appState.bonusesInfo.notifications.every(_ => _.notificationId !== id)) {
                    const bonusId = bonusCode.bonusTypeId || '';
                    if (bonusId === BonusType.SKILLWHEEL) {
                        return;
                    }

                    let message = bonusCode.message ? bonusCode.message[appState.language] : null;
                    const appearance = this.getBonusAppearance(bonusId, bonusCode.meta);

                    if (bonusCode.meta?.bonus_type === BonusType.WAGER_BONUS) {
                        const issueNotification = this.appState.wagerBonusProcessor.getIssueNotification(bonusCode.meta);
                        const issueNotificationTitle = this.appState.wagerBonusProcessor.getIssueNotificationTitle(bonusCode.meta);
                        if (issueNotification) message = issueNotification;
                        if (issueNotificationTitle) appearance.title = issueNotificationTitle;
                    } else if (bonusCode.meta?.bonus_family && bonusCode.meta?.bonus_family === BonusFamily.REFILLS) {
                        const issueNotification = this.appState.balanceWagerProcessor.getIssueNotification(bonusCode, bonusCode.meta);
                        const issueNotificationTitle = this.appState.balanceWagerProcessor.getIssueNotificationTitle(bonusCode.meta);
                        if (issueNotification) message = issueNotification;
                        if (issueNotificationTitle) appearance.title = issueNotificationTitle;
                    }  else if (
                        bonusCode.meta?.bonus_type === BonusType.USA_REF_INVITEE ||
                        bonusCode.meta?.bonus_type === BonusType.USA_REF_INVITER ||
                        bonusCode.meta?.bonus_type === BonusType.EXTERNAL_REG_BONUS ||
                        bonusCode.meta?.bonus_type === BonusType.REG_WAGER_BONUS ||
                        bonusCode.meta?.bonus_type === BonusType.USA_REF_INVITEE_BY_CASHIER ||
                        bonusCode.meta?.bonus_type === BonusType.REG_WAGER_FORCE_REDEEM ||
                        bonusCode.meta?.bonus_type === BonusType.NO_DEPOSIT_BY_CODE_BALANCE_WAGER ||
                        bonusCode.meta?.bonus_type === BonusType.APK_BONUS ||
                        bonusCode.meta?.bonus_type === BonusType.EXTERNAL_PROMO
                    )  {
                        const metaNotify = processString('notification', bonusCode.meta, '') || '';
                        const inviteeLogin = processString('invitee_login', bonusCode.meta, '') || '';
                        const redeemAmount = processString('redeem_amount', bonusCode.meta, '') || '0';
                        const currentRedeemMultiplier = processString('current_redeem_multiplier', bonusCode.meta, '') || '';
                        const amountFormated = bonusCode.amount && bonusCode.currency ? formatCurrencyAmount(
                            bonusCode.amount,
                            bonusCode.currency,
                            false
                        ) : "";
                        const redeemAmountFormatted = formatCurrencyAmount(
                            Long.fromString(redeemAmount.split('.')[0]),
                            bonusCode.currency,
                            false
                        );
                        message = metaNotify
                            .replace(/{friendLogin}/g, inviteeLogin)
                            .replace(/{bonusAmount}/g, amountFormated.replace('.00', ''))
                            .replace(/{redeemAmount}/g, redeemAmountFormatted.replace('.00', ''))
                            .replace(/{currentRedeemMultiplier}/g, currentRedeemMultiplier);
                    } else if (bonusCode.meta?.bonus_type === BonusType.PIN_WAGER && message && bonusCodeMeta!.redeem) {
                        message = message.replace(/{{redeem}}/g, bonusCodeMeta!.redeem)
                            .replace(/{{bonusAmount}}/g, formatCurrencyAmount(bonusCode.amount, bonusCode.currency, true).replace('.00', ''));
                    }

                    if (bonusCode.meta?.receive_notification_can_activate === "false" &&
                        (
                            bonusCode.meta?.bonus_type === BonusType.NO_DEPOSIT_BY_CODE_BALANCE_WAGER ||
                            bonusCode.meta?.bonus_type === BonusType.APK_BONUS ||
                            bonusCode.meta?.bonus_type === BonusType.EXTERNAL_PROMO
                        )
                    ) {
                        message = bonusCode.meta.cant_activate_reason;
                    }

                    if (!message && bonusCode.meta?.notification) {
                        message = bonusCode.meta.notification;
                    }

                    // common bonus props
                    const bonusNotification = new BonusCodeNotification({
                        appState: this.appState,
                        bonusCodeInfo: bonusCode,
                        notificationId: id,
                        message: message ? appState.t(message) : '',
                        title: appearance.title,
                        image: appearance.image,
                        gradientClass: appearance.gradientClass,
                        cashierInviteMeta,
                    });

                    // funds bonus props
                    bonusNotification.fundsBonus = this.processBonusNotificationsFunds(bonusCode);

                    // free spins bonus props
                    if (bonusCode.freeSpins && bonusCode.freeSpins.length) {
                        bonusNotification.freeSpinsBonuses = [];
                        for (let fs of bonusCode.freeSpins) {
                            if (fs.gameId && fs.count) {
                                let game = findGame(this.appState, fs.gameId);
                                if (game) {
                                    bonusNotification.freeSpinsBonuses.push({
                                        gameName: this.appState.games.getGameName(game)!,
                                        gameImage: this.appState.games.getGameImage(game)!,
                                        spinsCount: fs.count.toNumber(),
                                        bet: fs.bet ? fs.bet.toNumber() : undefined,
                                        line: fs.line ? fs.line.toNumber() : undefined,
                                        gameId: fs.gameId,
                                    });
                                } else {
                                    console.error('Free spins bonus notification - unable to find game with id: ', fs.gameId);
                                }
                            }
                        }
                    }

                    // free bets bonus props
                    if (bonusCode.freeBets && bonusCode.freeBets.length) {
                        bonusNotification.freeBetsBonuses = [];
                        for (let fs of bonusCode.freeBets) {
                            if (fs.gameId && fs.count) {
                                let game = findGame(this.appState, fs.gameId);
                                if (game) {
                                    bonusNotification.freeBetsBonuses.push({
                                        gameName: this.appState.games.getGameName(game)!,
                                        gameImage: this.appState.games.getGameImage(game)!,
                                        spinsCount: fs.count.toNumber(),
                                        bet: fs.bet ? fs.bet.toNumber() : undefined,
                                        gameId: fs.gameId,
                                    });
                                } else {
                                    console.error('Free bets bonus notification - unable to find game with id: ', fs.gameId);
                                }
                            }
                        }
                    }

                    this.appState.bonusesInfo.notifications.push(bonusNotification);
                    setTimeout(() => {
                      this.appState.games.fetchGames();
                    }, 1000);
                    // we need to render notification with queued status before set active status
                    // assume 100ms is enough to render notification
                    setTimeout(this.showNextNotification, 100);
                }
            }
        }
    }

    processBonusNotificationsFunds(bonusCode: bonuses.IBonusCodeInfo): BonusCodeNotificationFunds | undefined {
        const bonusType = processString(BonusMeta.BONUS_TYPE, bonusCode.meta, null);

        if (bonusCode.currency && bonusCode.amount && bonusCode.amount.greaterThan(Long.ZERO) && bonusType !== BonusType.PIN_WAGER) {
            const currency = currencies.findCurrencyById(bonusCode.currency);
            const result: BonusCodeNotificationFunds = {
                amount: formatCurrencyAmount(bonusCode.amount, bonusCode.currency, true),
                currency: formatCurrencyCode(currency),
                amountWithCurrency: formatCurrencyAmount(bonusCode.amount, currency)
            };
            if (bonusCode.wagerLeft) {
                result.wagerLeft = formatCurrencyAmount(bonusCode.wagerLeft, currency);
                result.wagerMultiplier = calcWagerMultiplier(bonusCode.wagerLeft, bonusCode.amount).toString();
            }

            return result;
        } else {
            return undefined;
        }
    }

    processUpdatedBonusCodes(updatedBonusCodes: bonuses.IBonusCodeUpdateNotification[]) {
        for (let updatedBonus of updatedBonusCodes) {
            const id = updatedBonus.id;
            const bonusNotification = this.appState.bonusesInfo.notifications.find((n: BonusCodeNotification) => n.notificationId === id);
            if (bonusNotification) {
                if (
                    !(
                        bonusNotification.shouldPersistAfterAccept &&
                        updatedBonus.action === bonuses.BonusCodeUpdateNotification.Action.ACCEPT
                    )
                ) {
                    this.hideNotification(bonusNotification.notificationId);
                }
            } else if (document.querySelector('[data-big-win-notification-id]')) {
                this.appState.jackpotWinners.closeJackpot();
                window.setTimeout(this.showNextNotification, 100);
            }
        }
    }

    processFreeSpinsResult(notificationId: string, bonusCodeMeta: BonusCodeMeta) {
        const spinsResultAmount = Long.fromString(bonusCodeMeta.amount!);
        const spinsResultCurrencyId = Long.fromString(bonusCodeMeta.currency!);
        const spinsResultCurrency = currencies.findCurrencyById(spinsResultCurrencyId);
        const spinsResultAmountWithCurrency = formatCurrencyAmount(spinsResultAmount, spinsResultCurrency);
        const spinsResultWagerMultiplier = 'x' + bonusCodeMeta.wagerMultiplier;

        const message = this.appState.t('ui-free-spins-result-notification', {
            amount: spinsResultAmountWithCurrency,
            wager: spinsResultWagerMultiplier
        });
        this.appState.notificator!.addNotification({
            message: message,
            level: NotificationItemLevel.SUCCESS,
            lifetime: 10
        });
        this.appState.api.closePromoCodeNotification(notificationId, bonuses.BonusCodeNotificationActionRequest.Action.HIDE, () => null);
    }

    processPositiveRegWagerBonusClosing(bonusCodeMeta: BonusCodeMeta, notificationId?: string) {
        const bonusMetaTitle = processString("popup_title", bonusCodeMeta as BonusMetaObj, null);
        const bonusMetaDescription = processString("popup_description", bonusCodeMeta as BonusMetaObj, null);
        if (bonusMetaTitle && bonusMetaDescription) {
            this.appState.bonusesInfo.bonusModalNotification = {
                title: bonusMetaTitle,
                description: bonusMetaDescription,
            }
            this.appState.modal.showPositiveRegWagerBonusClosing();
            if (notificationId) {
                this.appState.api.closePromoCodeNotification(notificationId, bonuses.BonusCodeNotificationActionRequest.Action.HIDE, () => null)
            }
        }
    }

    processBalanceCorrectionResult(notificationId: string, bonusCodeMeta: BonusCodeMeta) {
        const currencyId = Long.fromString(bonusCodeMeta.currency!);
        const currency = currencies.findCurrencyById(currencyId);

        const newBalance = Long.fromString(bonusCodeMeta.newBalance!);
        const newBalanceWithCurrency = formatCurrencyAmount(newBalance, currency);

        const bonusMaxWinLimit = Long.fromString(bonusCodeMeta.bonusMaxWinLimit!);
        const bonusMaxWinLimitWithCurrency = formatCurrencyAmount(bonusMaxWinLimit, currency);

        let params: BalanceCorrectionNotificationMessageParams = {
            maxBonusWinAmount: bonusMaxWinLimitWithCurrency,
            newBalance: newBalanceWithCurrency
        };
        let token = 'ui-balance-correction-notification';

        const message = this.appState.t(token, params);

        this.appState.notificator!.addNotification({
            message: message,
            level: NotificationItemLevel.SUCCESS,
            lifetime: 15
        });
        this.appState.api.closePromoCodeNotification(notificationId, bonuses.BonusCodeNotificationActionRequest.Action.HIDE, () => null);
    }

    getBonusAppearance(bonusId: string, meta: BonusMetaObj): BonusNotificationAppearance {
        const bonusType = processString(BonusMeta.BONUS_TYPE, meta, null);
        switch (bonusType) {
            case BonusType.CASHBACK:
                return {image: '/images/bonuses/Coins.svg', gradientClass: 'card-popup-bonus--gradient-18', title: 'ui-cash-back-bonus-title'};
            case BonusType.FINAL_BONUS:
                return {image: '/images/bonuses/Pocket.svg', gradientClass: 'card-popup-bonus--gradient-2', title: 'ui-final-bonus-title'};
            case BonusType.PIN_WAGER:
                return {
                    image: processString(BonusMeta.BONUS_IMAGE, meta, null) || BonusesActions.getRandomBonusImage(),
                    gradientClass: processString(BonusMeta.BONUS_GRADIENT, meta, null) || BonusesActions.getRandomBonusGradient(),
                    title: '',
                };
            default:
                return {
                    image: processString(BonusMeta.BONUS_IMAGE, meta, null) || BonusesActions.getRandomBonusImage(),
                    gradientClass: processString(BonusMeta.BONUS_GRADIENT, meta, null) || BonusesActions.getRandomBonusGradient(),
                    title: processString(BonusMeta.BONUS_TITLE, meta, 'ui-bonus')!
                };
        }
    }

    @action
    showNextNotification() {
        const appState = this.appState;
        const hasActiveNotification = this.appState.bonusesInfo.notifications.some(
            _ => _.animationClass === NotificationAnimationClass.ACTIVE,
        );
        const animation = appState.appSettings.animations && appState.appSettings.animations.bonusReceived;
        if (!hasActiveNotification) {
            const queuedNotification = this.appState.bonusesInfo.notifications.find((n: BonusCodeNotification) => n.animationClass === NotificationAnimationClass.QUEUED);
            if (queuedNotification) {
                if (animation === AnimationModes.CONFETTI && this.appState.page !== SitePages.GAME) {
                    runBonusConfetti();
                }
                queuedNotification.animationClass = NotificationAnimationClass.ACTIVE;
                // add timeout, to calc scale after append card html
                setTimeout(this.appState.ui.scale.calcNotificationsScale, 0);
            }
        }
    }

    @action
    refuseBonus(e: React.MouseEvent<HTMLElement>) {
        const bonusElement = e.currentTarget.closest('.js-card-popup-bonus');
        const notificationId = bonusElement ? bonusElement.getAttribute('id') : null;

        if (notificationId) {
            this.silentlyRefuseBonus(notificationId);
        } else {
            console.error('Unable to find notification element or notification id');
        }
    }

    @action
    execRefuseBonus(notificationId: string) {
        this.appState.bonusesInfo.notificationLoading = true;
        const notificationAction = this.appState.site.isProfileBonusesAllowed ?
            bonuses.BonusCodeNotificationActionRequest.Action.HIDE :
            bonuses.BonusCodeNotificationActionRequest.Action.DECLINE;
        this.appState.api.closePromoCodeNotification(notificationId, notificationAction, (msg, act) => {
            this.onBonusRefuseResponse(msg, act);
            if (notificationAction === bonuses.BonusCodeNotificationActionRequest.Action.DECLINE && act?.bonusNotificationAction?.ok) {
                this.appState.profileBonuses.onPromoCodeDecline();
            }
        });
    }

    @action
    onBonusRefuseResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const appState = this.appState;
        appState.bonusesInfo.notificationLoading = false;

        if (!act.bonusNotificationAction || !act.bonusNotificationAction.ok) {
            const message = appState.t('error-bonus-notification-action');
            const errorNotification = {
                level: NotificationItemLevel.ERROR,
                message: message
            };
            appState.notificator!.addNotification(errorNotification);
        }
    }

    @action.bound
    silentlyRefuseBonus(notificationId: string) {
        this.hideNotification(notificationId);
        const notificationAction = this.appState.site.isProfileBonusesAllowed ?
            bonuses.BonusCodeNotificationActionRequest.Action.HIDE :
            bonuses.BonusCodeNotificationActionRequest.Action.DECLINE;
        this.appState.api.closePromoCodeNotification(notificationId, notificationAction, (msg, act) => {
            this.onBonusRefuseResponse(msg, act);
            if (notificationAction === bonuses.BonusCodeNotificationActionRequest.Action.DECLINE && act?.bonusNotificationAction?.ok) {
                this.appState.profileBonuses.onPromoCodeDecline();
            }
        }, true);
    }

    @action
    acceptBonus(e: React.MouseEvent<HTMLElement>) {
        const bonusElement = e.currentTarget.closest('.js-card-popup-bonus');
        const notificationId = bonusElement ? bonusElement.getAttribute('id') : null;
        const notification = this.appState.bonusesInfo.notifications.find((n: BonusCodeNotification) => !!(n.notificationId && n.notificationId === notificationId));

        if (notificationId && notification) {
            this.appState.bonusesInfo.notificationLoading = true;
            this.appState.api.activatePromoCode(notification.code, this.onBonusAcceptResponse.bind(this, notification), true);
        } else {
            console.error('Unable to find notification element or notification id');
        }
    }

    @action
    onBonusAcceptResponse(notification: BonusCodeNotification | undefined, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const appState = this.appState;
        appState.bonusesInfo.notificationLoading = false;
        if (notification?.shouldPersistAfterAccept === false) {
            this.hideNotification(notification.notificationId);
        }

        const result = act.bonusAction ? act.bonusAction.activateCode!.result : null;
        if (result !== bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
            const errorToken = getBonusActivationErrorToken(result);
            const message = appState.t(errorToken);
            const errorNotification = {
                level: NotificationItemLevel.ERROR,
                message: message
            };
            appState.notificator!.addNotification(errorNotification);
        } else {
            notification?.onAcceptSuccess();
            this.appState.profileBonuses.onPromoCodeActivation();
        }
    }

    @action
    refuseBonusAndOpenGame(notificationId: string, gameId: string) {
        const notification = this.appState.bonusesInfo.notifications.find((n: BonusCodeNotification) => !!(n.notificationId && n.notificationId === notificationId));

        if (notification) {
            this.appState.bonusesInfo.notificationLoading = true;
            const notificationAction = this.appState.site.isProfileBonusesAllowed ?
                bonuses.BonusCodeNotificationActionRequest.Action.HIDE :
                bonuses.BonusCodeNotificationActionRequest.Action.DECLINE;
            this.appState.api.closePromoCodeNotification(notificationId, notificationAction, (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                this.onBonusRefuseResponse(msg, act);
                if (act.bonusNotificationAction?.ok) {
                    window.location.href = this.appState.games.url(gameId);
                }
            });
        } else {
            console.error('Unable to find notification element or notification id');
        }
    }

    @action
    hideNotification(id: string) {
        this.setNotificationAnimationClassById(id, NotificationAnimationClass.HIDE);
        window.setTimeout(() => {
            this.appState.bonusesInfo.removeNotification(id);
            this.showNextNotification();
        }, 500);
    }

    @action
    setNotificationAnimationClassById(id: NString, status: NotificationAnimationClass) {
        const notification = this.appState.bonusesInfo.notifications.find((n: BonusCodeNotification) => n.notificationId === id);

        if (notification) {
            notification.animationClass = status;

            if (status === NotificationAnimationClass.HIDE) {
                setTimeout(() => {
                    notification.animationClass = NotificationAnimationClass.HIDDEN;
                }, 500);
            }
        }
    }

    @action
    showTestBonusNotification(fundsBonus?: boolean, freeSpins?: boolean) {
        const bonusNotification = new BonusCodeNotification({
            appState: this.appState,
            bonusCodeInfo: {},
            notificationId: uuid(),
            title: 'ui-bonus',
            message: '',
            image: BonusesActions.getRandomBonusImage(),
            gradientClass: 'card-popup-bonus--gradient-2',
        });

        if (fundsBonus) {
            bonusNotification.fundsBonus = {
                amount: '1,000.00',
                currency: 'RUB',
                amountWithCurrency: '1,000.00 RUB',
                wagerMultiplier: '60',
                wagerLeft: '20,000.00 RUB'
            };
        }

        if (freeSpins) {
            bonusNotification.freeSpinsBonuses = [
                {gameName: 'Aladdin\'s Lamp', gameImage: '/media/thumb/300x300/ib_al.jpeg', spinsCount: 100, bet: 10, line: 10, gameId: 'ib_al'}
            ];
        }
        this.appState.bonusesInfo.notifications.push(bonusNotification);
        setTimeout(this.showNextNotification, 100);
    }
}
