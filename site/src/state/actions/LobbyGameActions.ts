import {computed} from 'mobx';
import AppState from '../AppState';

export default class LobbyGameActions {
    appState: AppState;
    gameId?: string;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @computed
    get isHeaderShown(): boolean {
        return (
            !this.appState.isMobile || this.gameId === 'bc-sport' || this.gameId === 'sports2win'
        );
    }

    @computed
    get pageClassName(): string {
        const className = [];
        if (this.isHeaderShown) {
            className.push('with-header');
        }
        return className.join(' ');
    }

    @computed
    get footerIsShow() {
        return this.appState.appSettings.showFooterInLobbyGamePage;
    }
}
