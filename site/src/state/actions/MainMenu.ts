import AppState from '../AppState';
import {site} from '../../api/proto';
import {action, computed, observable} from 'mobx';
import {SitePages} from '../meta';
import {getMobileOSType} from '../utils/BrowserUtils';

export class MainMenuItem extends site.MenuItem {
    children: MainMenuItem[] = [];

    constructor(private appState: AppState, gameMenuEntry: site.IGameMenuEntry) {
        super();
        Object.assign(this, gameMenuEntry);
    }

    @computed
    get displayedChildren(): MainMenuItem[] {
        return this.children.filter((_) => _.isVisible);
    }

    @computed
    get isWithChildren(): boolean {
        return this.displayedChildren.length > 0;
    }

    @computed
    get isVisible(): boolean {
        if (!this.name || !(this.name[this.appState.language] || this.id)) {
            return false;
        }
        if (
            this.appState.userInfo.guest &&
            (this.id === 'payments' || this.id === 'withdraw' || this.id === 'logout')
        ) {
            return false;
        }
        if (
            !this.appState.user.isSiteRegistered &&
            (this.id === 'payments' || this.id === 'withdraw')
        ) {
            return false;
        }
        if (this.id === 'logout' && !this.appState.logoutAvailable) {
            return false;
        }
        if (this.id === 'android' && (getMobileOSType() === 'iOS' || this.appState.isApk)) {
            return false;
        }
        if (this.id === 'tournaments-info' && !this.appState.tournamentsInfoActions.tournaments?.length) {
            return false;
        }
        return true;
    }

    @computed
    get isUrlStartsWithHttp(): boolean {
        return this.url.startsWith('http');
    }

    @computed
    get isNew(): boolean {
        return (
            this.appState.appSettings.mainMenu?.newEntries?.includes(this.id) ||
            (!this.appState.isMobileOnly &&
                this.isWithChildren &&
                this.children.some((_) => _.isNew))
        );
    }

    @computed
    get isActive(): boolean {
        const {currentPathName} = this.appState.site;
        let isActive = false;
        if (this.url) {
            if (this.url === '/') {
                isActive = currentPathName === this.appState.l('/') || currentPathName === '/';
                if (this.appState.siteConfig.mainPageType === site.MainPageType.DEFAULT_PAGE) {
                    isActive = currentPathName.startsWith(this.appState.l('/games'));
                }
            } else {
                isActive = currentPathName.startsWith(this.appState.l(this.url));
            }
        }
        return (
            isActive ||
            (!this.appState.isMobileOnly &&
                this.isWithChildren &&
                this.children.some((_) => _.isActive))
        );
    }

    @computed
    get isHovered(): boolean {
        return this.id === this.appState.mainMenu.activeItemId;
    }

    @computed
    get classNames(): string {
        const classes: string[] = ['main-menu__item'];
        if (this.isActive) {
            classes.push('active');
        }
        if (this.isWithChildren) {
            classes.push('main-menu__item--with-children');
        }
        return classes.join(' ');
    }

    @computed
    get counter(): null | number {
        if (this.id === 'bonuses') {
            if (this.appState.appSettings.mainMenu?.showAvailableBonusesCount) {
                return this.appState.profileBonuses.possibleBonuses.length || null;
            } else if (
                !!this.appState.siteConfig.pendingBonuses &&
                !this.appState.siteConfig.pendingBonuses.isZero()
            ) {
                return this.appState.siteConfig.pendingBonuses.toNumber();
            }
        }
        if (this.id === 'support' && this.appState.chat.unreadCount) {
            return this.appState.chat.unreadCount;
        }
        if (!this.appState.isMobileOnly && this.isWithChildren) {
            return (
                this.children.reduce((prev, current) => prev + (current.counter ?? 0), 0) || null
            );
        }
        if (this.id === 'tournaments-info') {
            return this.appState.tournamentsInfoActions.tournaments?.length || 0;
        }
        return null;
    }

    onClick = (event: React.MouseEvent<HTMLElement>) => {
        if (this.isWithChildren) {
            event.stopPropagation();
            event.preventDefault();
            if (this.appState.mainMenu.activeItemId === this.id) {
                this.clearActiveItem();
            } else {
                this.setActiveItem(event);
            }
        } else {
            if (this.id === 'support') {
                event.preventDefault();
                this.appState.chat.expand();
            } else if (this.id === 'logout') {
                this.appState.user.tryLogout(event);
            } else if (this.id === SitePages.PILOT) {
                window.setTimeout(this.appState.site.openGameIframeInFullscreenOnMobile, 0);
            } else if (this.id === 'back-to-lobby') {
                this.appState.user.backToLobby();
                event.preventDefault();
            }
            this.appState.site.doCloseMobileMenu();
        }
    };

    @action.bound
    onMouseEnter(e: React.MouseEvent<HTMLElement>) {
        this.setActiveItem(e);
    }

    @action.bound
    onMouseLeave() {
        this.clearActiveItem();
    }

    @action.bound
    onChildClick(e: React.MouseEvent<HTMLElement>) {
        e.stopPropagation();
        this.setActiveItem(e);
    }

    @action.bound
    setActiveItem(e: React.MouseEvent<HTMLElement>) {
        this.appState.mainMenu.activeItemId = this.id;
    }

    @action.bound
    clearActiveItem() {
        this.appState.mainMenu.activeItemId = null;
    }
}

export class MainMenu {
    items = observable<MainMenuItem>([]);
    itemsFlattened = observable<MainMenuItem>([], {deep: false});
    @observable activeItemId: string | null = null;

    constructor(private appState: AppState) {}

    @computed
    get displayedItems(): MainMenuItem[] {
        const filteredItems = this.items.filter((_) => _.isVisible);
        if (this.appState.appSettings.kioskLobby.enabled && this.appState.user.isFromKiosk) {
            filteredItems.push(
                new MainMenuItem(this.appState, {
                    id: 'back-to-lobby',
                    name: {[this.appState.language]: this.appState.t('ui-back-to-lobby')},
                }),
            );
        }
        return filteredItems;
    }

    @action.bound
    upgrade() {
        if (this.appState.siteConfig.topMenu) {
            const itemsFlattened: MainMenuItem[] = [];
            this.items.replace(
                this.appState.siteConfig.topMenu.map((item) => {
                    const mainMenuItem = new MainMenuItem(this.appState, item);
                    if (item.children) {
                        mainMenuItem.children = item.children.map((child) => {
                            return new MainMenuItem(this.appState, child);
                        });
                    }
                    itemsFlattened.push(mainMenuItem);
                    itemsFlattened.push(...mainMenuItem.children);
                    return mainMenuItem;
                }),
            );
            this.itemsFlattened.replace(itemsFlattened);
        }
    }
}
