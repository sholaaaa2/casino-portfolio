import AppState from '../AppState';

export default class GameListState {
    isScrolling: boolean = false;
    loadingCache = {};
    visibleState: string[];
    notLoadedImages = {};
    appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    setScrollingState(state: boolean) {
        this.isScrolling = state;
    }

    getScrollingState() {
        return this.isScrolling;
    }

    setLoadingState(id: string, state: boolean) {
        this.loadingCache[id] = state;
    }

    getLoadingState(id: string) {
        return this.loadingCache[id];
    }

    setVisibleState(arr: string[]) {
        this.visibleState = arr;
    }

    setNotLoadedImages(id: string) {
        this.notLoadedImages[id] = true;
    }

    isImageNotLoaded(id: string) {
        return this.notLoadedImages[id];
    }

    isNeedRender(id: string) {
        if (!this.appState.appSettings.games.useOptimizedGamesGrid) {
            return true;
        } else {
            return this.visibleState ? this.visibleState.indexOf(id) !== -1 : false;
        }

    }
}