import AppState from '../AppState';
import { site } from '../../api/proto';
import { action, computed, observable } from 'mobx';
import { SitePages } from '../meta';

export class GameMenuItem extends site.GameMenuEntry {
    private appState: AppState;

    constructor(gameMenuEntry: site.IGameMenuEntry, appState: AppState) {
        super();
        Object.assign(this, gameMenuEntry);
        this.children = [];
        this.appState = appState;
    }

    @computed
    get classNames(): string {
        const targetId = this.appState.games.groupId;
        const classes: string[] = [];
        if (this.id === 'tournaments-info' && this.appState.page === SitePages.TOURNAMENTS_INFO_LIST_PAGE) {
            classes.push('active');
        } else if (this.children && this.children.length > 0) {
            classes.push('hasChild');
            if (targetId) {
                if (
                    targetId === this.id ||
                    this.children.find(e => e.id === targetId) !== undefined
                ) {
                    classes.push('active');
                }
            }
            if (this.appState.gameMenu.activeItemId === this.id) {
                classes.push('visible');
            }
        } else {
            if (
                ((targetId && targetId === this.id) ||
                (!targetId && this.id === 'all')) && this.appState.page !== SitePages.TOURNAMENTS_INFO_LIST_PAGE
            ) {
                classes.push('active');
            }

            if (this.appState.appSettings.suppliersMenu.useProvidersIcons) {
                classes.push(`iconfont-${this.encodeProviderId(this.id)}`);
            }
        }
        return classes.join(' ');
    }

    encodeProviderId(providerId: string): string {
        return providerId.replace(/a/g, '0').replace(/e/g, '1').replace(/i/g, '2').replace(/o/g, '3').replace(/u/g, '4').replace(/y/g, '5');
    }

    @action.bound
    onClick(e: React.MouseEvent<HTMLElement>) {
        e.stopPropagation();
        e.preventDefault();
        if (this.appState.gameMenu.activeItemId === this.id!) {
            this.clearActiveItem();
        } else {
            this.setActiveItem(e);
        }
    }

    @action.bound
    onMouseEnter(e: React.MouseEvent<HTMLElement>) {
        this.setActiveItem(e);
    }

    @action.bound
    onMouseLeave() {
        this.clearActiveItem();
    }

    @action.bound
    onChildClick(e: React.MouseEvent<HTMLElement>) {
        e.stopPropagation();
        this.setActiveItem(e);
    }

    @action.bound
    setActiveItem(e: React.MouseEvent<HTMLElement>) {
        this.appState.gameMenu.activeItemId = this.id!;
        if (
            this.appState.appSettings.suppliersMenu.useHorizontalMenu &&
            this.children.length
        ) {
            this.recalculateSubmenuHeightForIOS(e);
        }
    }

    @action.bound
    clearActiveItem() {
        this.appState.gameMenu.activeItemId = null;
        if (this.appState.appSettings.suppliersMenu.useHorizontalMenu) {
            this.resetSubmenuHeightForIOS();
        }
    }

    @computed
    get isVisible() {
        if (this.id === 'tournaments-info' && !this.appState.tournamentsInfoActions.tournaments?.length) {
            return false;
        }
        return true;
    }

    recalculateSubmenuHeightForIOS(e: React.MouseEvent<HTMLElement>) {
        const targetEl = e.target as HTMLElement;
        const gameMenuEl = this.appState.getRef('gameMenu') as HTMLElement;
        const submenuEl = targetEl.nextElementSibling as HTMLElement;
        if (gameMenuEl && submenuEl) {
            submenuEl.style.display = 'flex';
            if (submenuEl.offsetHeight) {
                gameMenuEl.style.height = `${submenuEl.offsetHeight +
                    submenuEl.offsetTop}px`;
            }
            submenuEl.style.display = '';
        }
    }

    resetSubmenuHeightForIOS() {
        const gameMenuEl = this.appState.getRef('gameMenu') as HTMLElement;
        if (gameMenuEl) {
            gameMenuEl.style.height = 'auto';
        }
    }
}

export class GameMenu {
    public appState: AppState;
    @observable public items: GameMenuItem[] = [];
    @observable public activeItemId: string | null = null;

    private readonly SLIDING_INTERVAL = 500;
    private touchTimeout?: number;
    private touchInterval?: number;
    private itemsMap: { [key: string]: GameMenuItem } = {};

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @action.bound
    upgrade() {
        if (this.appState.siteConfig.gameMenu) {
            this.items = this.appState.siteConfig.gameMenu.map(item => {
                const gameMenuItem = new GameMenuItem(item, this.appState);
                this.itemsMap[gameMenuItem.id] = gameMenuItem;
                if (item.children) {
                    gameMenuItem.children = item.children.map(child => {
                        return new GameMenuItem(child, this.appState);
                    });
                }
                return gameMenuItem;
            });
        }
    }

    @action.bound
    scrollGameMenuLeftStart(event: React.SyntheticEvent<HTMLElement>) {
        this.scrollGameMenuEnd();
        this.touchTimeout = window.setTimeout(this.scrollLeftOneByOne, 10);
        this.touchInterval = window.setInterval(
            this.scrollLeftOneByOne,
            this.SLIDING_INTERVAL
        );
    }

    @action.bound
    scrollLeftOneByOne() {
        const container = this.appState.getRef('gameMenu');
        if (container) {
            const items: Element[] = Array.from(container.children).reverse();
            let containerOffsetLeft = container.offsetLeft;
            if (this.appState.appSettings.suppliersMenu.useHorizontalMenu) {
                const navArrow = container.parentElement!.querySelector(
                    '.navi-arr'
                );
                const arrowWidth = navArrow ? navArrow.clientWidth : 0;
                containerOffsetLeft += arrowWidth;
            }

            for (const item of items) {
                const htmlItem = item as HTMLElement;

                if (
                    htmlItem.offsetLeft <
                    containerOffsetLeft + container.scrollLeft - 1
                ) {
                    if (
                        this.appState.appSettings.suppliersMenu
                            .useHorizontalMenu === false &&
                        item.classList.contains('hasChild')
                    ) {
                        const children = Array.from(
                            item.querySelectorAll('li')
                        ).reverse();
                        let jumpedToNextChild = false;
                        for (const child of children) {
                            const htmlChild = child as HTMLElement;
                            const childWithOffset =
                                htmlItem.offsetLeft + htmlChild.offsetLeft;
                            if (
                                childWithOffset <
                                containerOffsetLeft + container.scrollLeft - 1
                            ) {
                                container.scrollLeft =
                                    childWithOffset - containerOffsetLeft;
                                jumpedToNextChild = true;
                                break;
                            }
                        }
                        if (jumpedToNextChild === false) {
                            const prevParent = htmlItem.previousSibling as HTMLElement;
                            if (prevParent) {
                                container.scrollLeft =
                                    prevParent.offsetLeft - containerOffsetLeft;
                            }
                        }
                    } else {
                        container.scrollLeft =
                            htmlItem.offsetLeft - containerOffsetLeft;
                    }
                    break;
                }
            }
        }
    }

    @action.bound
    scrollGameMenuRightStart(event: React.SyntheticEvent<HTMLElement>) {
        this.scrollGameMenuEnd();
        this.touchTimeout = window.setTimeout(this.scrollRightOneByOne, 10);
        this.touchInterval = window.setInterval(
            this.scrollRightOneByOne,
            this.SLIDING_INTERVAL
        );
    }

    @action.bound
    scrollRightOneByOne() {
        const container = this.appState.getRef('gameMenu');
        if (container) {
            let containerViewportWidthWithOffset =
                container.clientWidth + container.offsetLeft;
            if (this.appState.appSettings.suppliersMenu.useHorizontalMenu) {
                const navArrow = container.parentElement!.querySelector(
                    '.navi-arr'
                );
                const arrowWidth = navArrow ? navArrow.clientWidth : 0;
                containerViewportWidthWithOffset =
                    containerViewportWidthWithOffset - arrowWidth;
            }
            const items: Element[] = Array.from(container.children);

            for (const item of items) {
                const htmlItem = item as HTMLElement;
                const itemWidthWithOffset =
                    htmlItem.offsetLeft + htmlItem.clientWidth;

                if (
                    itemWidthWithOffset >
                    containerViewportWidthWithOffset + container.scrollLeft + 1
                ) {
                    if (
                        this.appState.appSettings.suppliersMenu
                            .useHorizontalMenu === false &&
                        item.classList.contains('hasChild')
                    ) {
                        const children = Array.from(
                            item.querySelectorAll('li')
                        );
                        let jumpedToNextChild = false;
                        for (const child of children) {
                            const htmlChild = child as HTMLElement;
                            const childWithOffset =
                                htmlItem.offsetLeft +
                                htmlChild.offsetLeft +
                                htmlChild.clientWidth;
                            if (
                                childWithOffset >
                                containerViewportWidthWithOffset +
                                    container.scrollLeft +
                                    1
                            ) {
                                container.scrollLeft =
                                    childWithOffset -
                                    containerViewportWidthWithOffset;
                                jumpedToNextChild = true;
                                break;
                            }
                        }
                        if (jumpedToNextChild === false) {
                            const nextParent = htmlItem.nextSibling as HTMLElement;
                            if (nextParent) {
                                container.scrollLeft =
                                    nextParent.offsetLeft +
                                    nextParent.clientWidth -
                                    containerViewportWidthWithOffset;
                            }
                        }
                    } else {
                        container.scrollLeft =
                            itemWidthWithOffset -
                            containerViewportWidthWithOffset;
                    }
                    break;
                }
            }
        }
    }

    @action.bound
    scrollGameMenuEnd() {
        if (this.touchTimeout) {
            clearTimeout(this.touchTimeout);
        }
        if (this.touchInterval) {
            clearInterval(this.touchInterval);
            this.touchInterval = undefined;
        }
    }

    get contentNaviClasses() {
        const classes = [];
        if (this.appState.appSettings.suppliersMenu.useHorizontalMenu) {
            classes.push('content-navi--horizontal-submenu');
        }
        if (this.appState.appSettings.games.showGameItemsSizeOptions) {
            classes.push('with-game-size-options');
            if (this.appState.appSettings.games.gamesSizeOptionsButtonPosition &&
                this.appState.appSettings.games.gamesSizeOptionsButtonPosition === 'game-menu-right') {
                classes.push('game-size-button-right');
            }
        }
        return classes.join(' ');
    }
}
