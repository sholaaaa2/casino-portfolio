import { action, observable, computed } from 'mobx';
import { GuestPopupId, Modal } from '../meta';
import { WallettecInvoiceHTML } from '../payment-systems/providers/Walletec';
import { WecashupModalData } from '../payment-systems/providers/WeCashUp';
import AppState from '../AppState';
import { AuthorizationType } from '../actions/authorization/AuthorizationTypeState';
import { CoinPaymentsFiatForm } from '../payment-systems/providers/CoinPayments';
import { SwitchInvoiceHTML } from "../payment-systems/providers/Switch";

const AUTH_MODALS = [
    Modal.LOGIN,
    Modal.LOGIN_BY_PHONE,
    Modal.REGISTER,
    Modal.REGISTRATION_SECOND_STEP,
    Modal.RESTORE,
    Modal.SESSION_STARTED_ON_DIFFERENT_DEVICE,
    Modal.ONE_CLICK_REGISTRATION_SUCCESS,
    Modal.ANONYMOUS_REGISTRATION_SUCCESS,
    Modal.AUTHORIZED_CONFIRMATION,
    Modal.CONFIRM_PHONE,
    Modal.CONFIRM_EMAIL,
    Modal.DO_CONFIRM_EMAIL,
    Modal.CONFIRM_CASHAPP_PHONE,
];
const BONUS_MODALS = [
    Modal.FINAL_BONUSES,
    Modal.FINAL_BONUS,
    Modal.DAILY_BONUS,
    Modal.WAGER_BONUS,
    Modal.REF_INVITE_FORM,
    Modal.JACKPOT_WINNER,
    Modal.BALANCE_WAGER_BONUS,
    Modal.APK_ANDROID_BONUS,
    GuestPopupId.BONUSES,
    GuestPopupId.JACKPOT,
    GuestPopupId.WINNERS,
    Modal.WRONG_PROMO_CODE,
    Modal.LOW_BALANCE_POPUP,
    Modal.BONUS_BY_CODE_WITH_REQ_DEPS,
];

interface IModalContext {
    actionCallback?: (() => void) | null;
    closeCallback?: (() => void) | null;
    data?: any | null;
}

export class ModalActions {
    @observable _activeModal: Modal | GuestPopupId | null = null;
    @computed get activeModal(): Modal | GuestPopupId | null {
        return this._activeModal;
    }
    set activeModal(val: Modal | GuestPopupId | null) {
        const root = document.getElementById('root');
        if (root) {
            root.classList.remove(`modal-${this._activeModal}`);
            if (val) {
                root.classList.add(`modal-${val}`);
            }
        }
        this._activeModal = val;
    }
    @observable walletecModalData: WallettecInvoiceHTML[] | null = null;
    @observable switchModalData: SwitchInvoiceHTML[] | null = null;
    @observable wecashupModalData: WecashupModalData | null = null;
    @observable coinPaymentsFiatFormData: CoinPaymentsFiatForm | null = null;
    modalsQueue: { modal: (Modal | GuestPopupId); context?: IModalContext | null; }[] = [];

    private appState: AppState;
    gameRequiresAnotherCurrencyCallback?: () => void;

    context?: IModalContext | null;

    triggerActionCallback = () => {
        this.context?.actionCallback && this.context.actionCallback();
    };

    triggerCloseCallback = () => {
        this.context?.closeCallback && this.context.closeCallback();
    };

    constructor(appState: AppState) {
        this.appState = appState;
        if (appState.desktopAuthMode && appState.screenSaverMode) {
            this.activeModal = Modal.LOGIN;
        }
    }

    resetState() {
        this.modalsQueue = [];
    }

    @action.bound
    public show(modal: Modal | GuestPopupId, e?: React.MouseEvent<HTMLElement> | null, context?: IModalContext | null) {
        e?.preventDefault();

        if (
            !!this.activeModal &&
            ((AUTH_MODALS.includes(this.activeModal as Modal) &&
              BONUS_MODALS.includes(modal)) || (
              BONUS_MODALS.includes(this.activeModal as Modal) &&
              BONUS_MODALS.includes(modal)
            ))
        ) {
            if (!this.modalsQueue.some((m) => m.modal === modal)) {
                this.modalsQueue.push({ modal, context });
            }
        } else {
            this.activeModal = modal;
            this.context = context;
        }
    }

    @action.bound
    public hideModal(e?: React.MouseEvent<HTMLElement>) {
        e?.preventDefault();
        const m = this.modalsQueue.shift() ?? null;
        this.activeModal = m?.modal ?? null;
        this.context = m?.context || null;
    }

    @action.bound
    public closeModal(e?: React.MouseEvent<HTMLElement>) {
        this.triggerCloseCallback();
        this.hideModal(e);
    }

    @action.bound
    public showError500(code?: number, message?: string) {
        this.show(Modal.ERROR_500);
    }

    @action.bound
    public showRegister(e?: React.MouseEvent<HTMLElement>) {
        e?.preventDefault();
        if (this.appState.site.isWeblimaMode) {
            this.appState.signUpActions.redirectToCashierLogin();
        } else {
            this.appState.resetForms();
            this.show(Modal.REGISTER);
            if (this.appState.site.isWlAndWeblimaMode) {
                this.appState.authorizationTypeState.type = AuthorizationType.BY_EMAIL;
            } else if (
                this.appState.signUpActions.isAnonymousRegistartionEnabled &&
                !this.appState.appSettings.registerForm.isOneClickRegistrationEnabled &&
                this.appState.authorizationTypeState.type === this.appState.enums.authorizationType.BY_LOGIN
            ) {
                this.appState.authorizationTypeState.type = AuthorizationType.ANONYMOUS;
            }
        }
    }

    @action.bound
    public showRegisterSecondStep(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.REGISTRATION_SECOND_STEP, e);
    }

    @action.bound
    public showLogin(e?: React.MouseEvent<HTMLElement>) {
        e?.preventDefault();
        if (this.appState.site.isWeblimaMode) {
            this.appState.signInActions.redirectToCashierLogin();
        } else {
            this.appState.resetForms();
            this.show(Modal.LOGIN);
            if (this.appState.authorizationTypeState.type === this.appState.enums.authorizationType.ANONYMOUS) {
                this.appState.authorizationTypeState.type = this.appState.enums.authorizationType.BY_LOGIN;
            }
            this.appState.authorizationTypeState.getTypeFromUrl();
        }
    }

    @action.bound
    public showLoginByPhone(e?: React.MouseEvent<HTMLElement>) {
        this.appState.resetForms();
        this.show(Modal.LOGIN_BY_PHONE, e);
    }

    @action.bound
    public showRestore(e?: React.MouseEvent<HTMLElement>) {
        this.appState.resetForms();
        this.show(Modal.RESTORE, e);
    }

    @action.bound
    public showAuthorizedConfirmation(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.AUTHORIZED_CONFIRMATION, e);
    }

    @action.bound
    public showJackpotWinner(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.JACKPOT_WINNER, e);
    }

    @action.bound
    public showConfirmRefuseBonus(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.CONFIRM_REFUSE_BONUS, e);
    }

    @action.bound
    public showNonTakenSpinsGames(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.NON_TAKEN_SPINS_GAMES, e);
    }

    @action.bound
    public showConfirmRefuseWagerBonus(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.CONFIRM_REFUSE_WAGER_BONUS, e);
    }

    @action.bound
    public showConfirmApplyingWagerBonus(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.WAGER_BONUS_CONFIRMATION, e);
    }

    @action.bound
    public showWagerBonudModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.WAGER_BONUS, e);
    }

    @action.bound
    public showGameEmbedError(e?: React.MouseEvent<HTMLElement>) {
        if (this.appState.appSettings.redirectFrom404ToMainPage) {
            const route = this.appState.userInfo.guest ? '/login' : '/';
            this.appState.router.push(this.appState.l(route));
        } else {
            this.show(Modal.GAME_EMBED_ERROR, e);
        }
    }

    @action.bound
    public showConfirmPhone(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.CONFIRM_PHONE, e);
    }

    @action.bound
    public showDoConfirmEmail(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.DO_CONFIRM_EMAIL, e);
    }

    @action.bound
    public showOneClickRegistrationSuccess(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.ONE_CLICK_REGISTRATION_SUCCESS, e);
    }

    @action.bound
    public showAnonymousRegistrationSuccess(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.ANONYMOUS_REGISTRATION_SUCCESS, e);
    }

    @action.bound
    public showRestorePasswordByMail(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.DO_RESTORE_PASSWORD_BY_MAIL, e);
    }

    @action.bound
    public showRefillWallettecInfo(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.REFILL_WALLETTEC_INFO, e);
    }

    @action.bound
    public showRefillSwitchInfo(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.REFILL_SWITCH_INFO, e);
    }

    @action.bound
    public showPaywayWecashupRefillForm(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.PAYWAY_WECASHUP_REFILL_FORM, e);
    }

    @action.bound
    public showCoinPaymentsCryptoForm(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.PAYWAY_COIN_PAYMENTS_CRYPTO_REFILL_FORM, e);
    }

    @action.bound
    showSessionStartedOnDifferentDevice(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.SESSION_STARTED_ON_DIFFERENT_DEVICE, e);
    }

    @action.bound
    showAutoLogoutAfterIdle(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.AUTO_LOGOUT_AFTER_IDLE, e);
    }

    @action.bound
    showKeepAlivePing(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.KEEP_ALIVE_PING, e);
    }

    @action.bound
    showFreespinBuy(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.FREESPIN_BUY, e);
    }

    @action.bound
    public showApkAndroidBonusModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.APK_ANDROID_BONUS, e);
    }

    @action.bound
    showRefInviteWasSent(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.REF_INVITE_WAS_SENT, e);
    }

    @action.bound
    showRefInviteForm(e?: React.MouseEvent<HTMLElement>) {
        this.appState.ref.onRefModalOpen();
        this.show(Modal.REF_INVITE_FORM, e);
    }

    @action.bound
    showGameRequiresAnotherCurrency(callback: () => void) {
        this.gameRequiresAnotherCurrencyCallback = callback;
        this.show(Modal.GAME_REQUIRES_ANOTHER_CURRENCY);
    }

    @action.bound
    showGameRequiresLogin(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.GAME_REQUIRES_LOGIN, e);
    }

    @action.bound
    showPositiveRegWagerBonusClosing(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.POSITIVE_REG_WAGER_BONUS_CLOSING, e);
    }

    @action.bound
    showRefillRedeemModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.REFILL_REDEEM, e);
    }

    @action.bound
    showLowBalancePopup(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.LOW_BALANCE_POPUP, e);
    }

    @action.bound
    showWithdrawalSkycryptoLinkModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.WITHDRAWAL_LINK_MODAL_SKYCRYPTO, e);
    }

    @action.bound
    public showbalanceWagerModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.BALANCE_WAGER_BONUS, e);
    }

    @action.bound
    showWrongPromoCodeModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.WRONG_PROMO_CODE, e);
    }

    @action.bound
    showConfirmCashappPhone(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.CONFIRM_CASHAPP_PHONE, e);
    }

    @action.bound
    showBonusByCodeWithReqDepsModal(e?: React.MouseEvent<HTMLElement>) {
        this.show(Modal.BONUS_BY_CODE_WITH_REQ_DEPS, e);
    }
}
