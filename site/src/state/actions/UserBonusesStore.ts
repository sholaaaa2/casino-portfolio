import AppState from '../AppState';
import {bonuses, messages, users} from '../../api/proto';
import {observable, action, computed} from 'mobx';
import {processBonusPercentValue, processString, BonusMeta, processFreeSpinsAmount, processBonusFreeSpinsTotalAmountString, BonusType, BonusFamily} from '../../api/utils';
import {BonusEntryWager, BonusConditionsGroup} from './ProfileBonusesActions';
import {SitePages} from '../meta';
import {formatCurrencyAmount} from '../utils/Money';
import Long from 'long';
import PoppedBonus from './bonuses/PoppedBonus';

interface BonusBalance {
    name: string;
    isShownInPopup: boolean;
    description?: string;
    formattedPossibleAmount?: string;
    formattedAvailableAmount?: string;
    wagered?: string;
    wagerLeftInitial?: string;
    afterCorrection?: string;
    wagerEndTime?: string;
}

export type SuccessCallback = (codes: bonuses.IBonusCode[],
                               possibleCodes: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) => void;

export class DisplayedBonus {
    @observable public isDetailsOpened: boolean = false;
    public id: string;
    public meta: { [k: string]: string };
    public poppedBonus: PoppedBonus | null = null;
    public type: string;
    public valueString: string;
    public isActive: boolean;
    public wager: BonusEntryWager | null;
    public conditions: BonusConditionsGroup[];
    public freeSpins: any | null;

    public issueRules?: string | null | undefined;
    public wageringRules?: string | null | undefined;
    public withdrawalRules?: string | null | undefined;
    public htmlDescription?: string | null | undefined;

    constructor(public appState: AppState, bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode) {
        this.id = bonus.id!;
        this.meta = bonus.meta!;
        if (bonus.meta && bonus.meta.popup) {
            this.poppedBonus = new PoppedBonus(this.appState, bonus);
        }
        this.valueString = bonus.meta!.balance_bonus_amount_exact_percent || processBonusPercentValue(bonus.meta, this.appState) || '';
        this.isActive = bonus.meta!.can_be_reactivated === 'true';
        this.wager = this.appState.profileBonuses.processBonusWager(
            bonus as bonuses.IBonusCode
        );
        const freeSpinsRaw = processFreeSpinsAmount(BonusMeta.FREE_SPINS_BONUS_AMOUNT, bonus.meta);
        if (freeSpinsRaw && !this.valueString) {
            this.valueString = processBonusFreeSpinsTotalAmountString(appState, freeSpinsRaw);
            this.freeSpins = freeSpinsRaw.map(freeSpin => {
                return {
                    ...freeSpin,
                    game: appState.games.findGameById(freeSpin.gameId)
                }
            }).filter(freeSpin => freeSpin.game);
        }
        this.conditions = [
            ...this.appState.profileBonuses.processBonusReceiveConditions(bonus as bonuses.IBonusCode),
            ...this.appState.profileBonuses.processBonusWageringConditions(bonus as bonuses.IBonusCode, this.wager),
            ...this.appState.profileBonuses.processBonusWithdrawalConditions(bonus as bonuses.IBonusCode)
        ];

        this.issueRules = bonus.meta && bonus.meta["issue_rules"];
        this.wageringRules = bonus.meta && bonus.meta["wagering_rules"];
        this.withdrawalRules = bonus.meta && bonus.meta["withdrawal_rules"];
        this.htmlDescription = bonus.meta && bonus.meta["bonuses_page_description"];
    }

    @action.bound
    toggle(e?: React.MouseEvent<HTMLElement>) {
        e?.preventDefault();
        this.isDetailsOpened = !this.isDetailsOpened;
    }
}

export class DisplayedWagerBonus extends DisplayedBonus {
    public isAvailable: boolean;

    constructor(public appState: AppState, bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode) {
        super(appState, bonus);
        this.type = bonus.meta!.bonus_type;
        this.isAvailable = bonus.meta!.is_available === 'true';
    }

    @action.bound
    open() {
        const thisInDOM = document.getElementById(`wager-bonus-${this.id}`);
        if (thisInDOM) {
            window.scrollTo(0,thisInDOM.offsetTop + 216);
        }
        if (!this.isDetailsOpened) {
            this.isDetailsOpened = true;
        }
    }
}

export class DisplayedBalanceWager extends DisplayedBonus {
    public isAvailable: boolean;

    constructor(public appState: AppState, bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode) {
        super(appState, bonus);
        this.type = bonus.meta!.bonus_type;
        this.isAvailable = !!bonus.meta!.available;
    }

    @action.bound
    open() {
        const thisInDOM = document.getElementById(`balance-wager-refills-${this.id}`);
        if (thisInDOM) {
            window.scrollTo(0,thisInDOM.offsetTop + 216);
        }
        if (!this.isDetailsOpened) {
            this.isDetailsOpened = true;
        }
    }
}

export default class UserBonusesStore {
    isLoading: boolean = false;
    callbacks: SuccessCallback[] = [];
    @observable public codes: bonuses.IBonusCode[] = [];
    @observable public displayedBonuses?: bonuses.UserBonusesListResponse.IPossibleBonusCode[];
    @observable public displayedUnactiveBonuses?: bonuses.UserBonusesListResponse.IPossibleBonusCode[];
    refInviterBonuses = observable<bonuses.IBonusCode>([]);
    @observable isBonusBalanceLoading = false;
    @observable bonusBalances?: BonusBalance[];
    @observable isBonusBalanceInfoVisible = false;
    timeToLeftWageringInterval?: ReturnType<typeof setTimeout>;

    constructor(private appState: AppState) {
    }

    findBonusCode = (bonusId: string): bonuses.IBonusCode | null => {
        return this.codes.find((b) => b.bonusId === bonusId) || null;
    };

    @computed
    get bonusBalancesWithTotalWager(): BonusBalance[] {
        const bonusBalances: BonusBalance[] = this.bonusBalances ? [...this.bonusBalances] : [];
        return this.appState.bonusesInfo.needPlay && this.appState.bonusesInfo.playedString && this.appState.bonusesInfo.totalString ?
            bonusBalances.concat({
                name: this.appState.t(`bonus-balance-total-wager`),
                wagered: this.appState.bonusesInfo.playedString,
                wagerLeftInitial: this.appState.bonusesInfo.totalString,
                isShownInPopup: true
            }) :
            bonusBalances;
    }

    @computed
    get bonusBalancesInPopup(): BonusBalance[] {
        return this.bonusBalancesWithTotalWager.filter(bonusBalance => bonusBalance.isShownInPopup);
    }

    @computed
    get showBonuses(): boolean {
        return this.appState.games.groupId === 'adult' || this.appState.page === SitePages.BONUSES;
    }

    @computed
    get displayedBonusesLine1(): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        if (!this.displayedBonuses) {
            return [];
        } else {
            return this.displayedBonuses.slice(0, Math.ceil(this.displayedBonuses.length / 2));
        }
    }

    @computed
    get displayedBonusesLine2(): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        if (!this.displayedBonuses) {
            return [];
        } else {
            return this.displayedBonuses.slice(Math.ceil(this.displayedBonuses.length / 2));
        }
    }

    @computed
    get displayedUnactiveBonusesLine1(): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        if (!this.displayedUnactiveBonuses) {
            return [];
        } else {
            return this.displayedUnactiveBonuses.slice(0, Math.ceil(this.displayedUnactiveBonuses.length / 2));
        }
    }

    @computed
    get displayedUnactiveBonusesLine2(): bonuses.UserBonusesListResponse.IPossibleBonusCode[] {
        if (!this.displayedUnactiveBonuses) {
            return [];
        } else {
            return this.displayedUnactiveBonuses.slice(Math.ceil(this.displayedUnactiveBonuses.length / 2));
        }
    }

    fetchBonuses(callback?: SuccessCallback, page?: string) {
        if (callback) {
            this.callbacks.push(callback);
        }

        if (!this.isLoading) {
            this.isLoading = true;
            this.appState.api.getUserBonuses(this.onFetchBonusesResponse.bind(this), page);
        }
    }

    onFetchBonusesResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isLoading = false;
        this.appState.processApi(msg, act);

        if (act.userBonuses) {
            this.execCallbacks(act.userBonuses.codes || [], act.userBonuses.possibleCodes || []);
            if (act.userBonuses.possibleCodes) {
                this.processPossibleBonuses(act.userBonuses.possibleCodes);
            }
            if (act.userBonuses.codes) {
                this.processActivatedBonuses(act.userBonuses.codes);
            }
        } else {
            console.error('Unable to fetch user bonuses (bonuses.UserBonusesListRequest)');
            this.execCallbacks([], []);
        }
    }

    execCallbacks(codes: bonuses.IBonusCode[], possibleCodes: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) {
        const callbacks = this.callbacks.slice();
        this.callbacks = [];
        for (let cb of callbacks) {
            cb(codes, possibleCodes);
        }
    }

    fetchBonusBalances() {
        if (!this.isBonusBalanceLoading) {
            this.isBonusBalanceLoading = true;
            this.appState.api.getBonusBalance(this.onFetchBonusBalancesResponse.bind(this));
        }
    }

    onFetchBonusBalancesResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isBonusBalanceLoading = false;
        this.appState.processApi(msg, act);

        if (act.userBonusBalances && act.userBonusBalances) {
            this.processBonusBalances(act.userBonusBalances);
        } else {
            console.error('Unable to fetch bonus balance (user.UserBonusBalancesRequest)');
        }
    }

    @action.bound
    processBonusBalances(response: users.IUserBonusBalancesResponse) {
        if (this.timeToLeftWageringInterval) clearInterval(this.timeToLeftWageringInterval);
        const bonusBalances: BonusBalance[] = [];
        if (response.availableWithdrawal) {
            bonusBalances.push({
                name: this.appState.t('bonus-balance-available-for-withdrawal:title'),
                description: this.appState.t('bonus-balance-available-for-withdrawal:description'),
                formattedAvailableAmount: formatCurrencyAmount(response.availableWithdrawal, this.appState.userInfo.realCurrency),
                isShownInPopup: true
            });
        }
        let timerEnable = false;
        if (response.bonusBalances) {
            response.bonusBalances.forEach(bonus => {
                const hasPossibleAmount = !!bonus.possibleAmount && !bonus.possibleAmount.isZero();
                let formattedAvailableAmount = bonus.availableWithdrawal && !bonus.availableWithdrawal.isZero() ?
                    formatCurrencyAmount(bonus.availableWithdrawal, bonus.currencyId) :
                    undefined;
                if (!bonus.canClose) {
                    formattedAvailableAmount = formatCurrencyAmount(Long.ZERO, bonus.currencyId);
                }
                const timeNow = Date.now();
                const timeToLeftWagering = bonus.wagerEndTime && bonus.wagerEndTime.toNumber() > timeNow ? bonus.wagerEndTime.toNumber() - timeNow : undefined;
                if (bonus.redeemAmount && !bonus.redeemAmount.isZero()) {
                    bonusBalances.push({
                        name: bonus.title ? bonus.title : this.appState.t(`bonus-balance:${bonus.bonusId}:title`),
                        description: this.appState.t(
                            'ui-bonus-balances-wager-bonus-description',
                            {redeemAmount: formatCurrencyAmount(bonus.redeemAmount, bonus.currencyId)}
                        ),
                        isShownInPopup: true,
                        wagerEndTime: timeToLeftWagering ? timeToLeftWagering.toString() : undefined,
                        afterCorrection: bonus.afterCorrection && !bonus.afterCorrection.isZero() ? formatCurrencyAmount(bonus.afterCorrection, bonus.currencyId, false) : undefined,
                    });
                } else {
                    bonusBalances.push({
                        name: bonus.title ? bonus.title : (bonus.bonusId ? this.appState.t(`bonus-balance:${bonus.bonusId}:title`) : ""),
                        formattedPossibleAmount: hasPossibleAmount ?
                            formatCurrencyAmount(bonus.possibleAmount, bonus.currencyId) :
                            undefined,
                        formattedAvailableAmount,
                        wagered: bonus.wagerLeftInitial && !bonus.wagerLeftInitial.isZero() ? formatCurrencyAmount(bonus.wagered, bonus.currencyId, true) : undefined,
                        wagerLeftInitial: bonus.wagerLeftInitial && !bonus.wagerLeftInitial.isZero() ? formatCurrencyAmount(bonus.wagerLeftInitial, bonus.currencyId, true) : undefined,
                        isShownInPopup: hasPossibleAmount || !!timeToLeftWagering,
                        wagerEndTime: timeToLeftWagering ? timeToLeftWagering.toString() : undefined,
                        afterCorrection: bonus.afterCorrection && !bonus.afterCorrection.isZero() ? formatCurrencyAmount(bonus.afterCorrection, bonus.currencyId, false) : undefined,
                    });
                }
                if (timeToLeftWagering) timerEnable = true;
            });
        }
        this.bonusBalances = bonusBalances;
        if (timerEnable) {
            this.timeToLeftWageringInterval = setInterval(() => {
                this.bonusBalances = this.bonusBalances?.map(bonus => {
                    if (bonus.wagerEndTime) {
                        return {
                            ...bonus,
                            wagerEndTime: Number(bonus.wagerEndTime) >= 1000 ? String(Number(bonus.wagerEndTime) - 1000) : '0',
                        }
                    }
                    return bonus;
                })
            }, 1000)
        }
    }

    @action.bound
    processPossibleBonuses(
        possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[]
    ) {
        this.appState.balanceWager.entries = [];
        this.displayedBonuses = possibleBonuses.filter(bonus => {
            return (
                bonus.id &&
                bonus.meta &&
                bonus.meta.enabled === 'true' &&
                bonus.meta.should_show_in_possible_bonuses === 'true' &&
                bonus.meta.can_be_reactivated === 'true'
            );
        }).reduce((acc: bonuses.UserBonusesListResponse.IPossibleBonusCode[], bonus) => {
            if (bonus.meta!.bonus_type === BonusType.WAGER_BONUS) {
                const wagerBonuses = this.appState.wagerBonusProcessor.process(bonus);
                this.appState.wagerBonus.processWagerBonuses(wagerBonuses);
                const processedWagerBonuses = wagerBonuses.map((bonus) => new DisplayedWagerBonus(this.appState, bonus));
                return [...acc, ...processedWagerBonuses];
            } else if (bonus.meta!.bonus_family && bonus.meta!.bonus_family === BonusFamily.REFILLS) {
                const balanceWagerBonuses = this.appState.balanceWagerProcessor.process(bonus);
                this.appState.balanceWager.processBalanceWageres(balanceWagerBonuses);
                const proccessesBalanceWagerBonuses = balanceWagerBonuses.map((bonus: any) => new DisplayedBalanceWager(this.appState, bonus));
                return [...acc, ...proccessesBalanceWagerBonuses];
            } else {
                return [...acc, new DisplayedBonus(this.appState, bonus)];
            }
        }, []);
        this.displayedUnactiveBonuses = possibleBonuses.filter(bonus => {
            return (
                bonus.id &&
                bonus.meta &&
                bonus.meta.enabled === 'true' &&
                bonus.meta.should_show_in_possible_bonuses === 'true' &&
                bonus.meta.can_be_reactivated === 'false'
            );
        }).map(bonus => {
            return new DisplayedBonus(this.appState, bonus);
        });

        this.appState.poppedBonus.processPossibleBonuses(possibleBonuses);
        this.appState.timelineCashbackBonus.processPossibleBonuses(possibleBonuses);
    }

    @action.bound
    processActivatedBonuses(activatedBonuses: bonuses.IBonusCode[]) {
        this.codes = activatedBonuses;
        const wagerBonuses = activatedBonuses.filter((b) => {
            const type = processString(BonusMeta.BONUS_TYPE, b.meta, null);
            return type === BonusType.WAGER_BONUS;
        }).map((b) => {
            return this.appState.wagerBonusProcessor.process(b)[0];
        }) as bonuses.IBonusCode[];
        const activeWagerBonus = wagerBonuses.find((b) => {
            return b ? b.state === bonuses.BonusCode.State.BS_WAGERING : false;
        });
        if (activeWagerBonus) this.appState.wagerBonus.processActiveWagerBonus(activeWagerBonus);
        this.refInviterBonuses.replace(activatedBonuses.filter(bonus => {
            return (
                bonus.id &&
                bonus.meta?.enabled === 'true' &&
                bonus.meta?.bonus_type === BonusType.USA_REF_INVITER &&
                bonus.state === bonuses.BonusCode.State.BS_PENDING
            );
        }));
    }

    @action.bound
    toggleBonusBalanceInfo(e?: React.MouseEvent<HTMLElement>) {
        if (e) {
            e.stopPropagation();
        }
        this.isBonusBalanceInfoVisible = !this.isBonusBalanceInfoVisible;
        if (this.isBonusBalanceInfoVisible) {
            this.appState.changeUserHeaderVisible = false;
            if (this.appState.page !== SitePages.PROFILE_BONUS_BALANCES) {
                this.fetchBonusBalances();
            }
        }
    }

    @action.bound
    resetBonuses() {
        if (this.showBonuses) {
            this.fetchBonuses();
        } else {
            this.displayedBonuses = undefined;
        }
    }

    @action.bound
    bonusClick(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        if (this.appState.userInfo.guest) {
            this.appState.modal.showRegister();
        } else {
            this.appState.router.push(this.appState.l('/refill'));
        }
    }
}
