import React, {createRef} from 'react';
import {action, computed, observable, runInAction} from 'mobx';
import {CountryCode, parsePhoneNumberFromString} from 'libphonenumber-js';
import copyToClipboard from 'copy-to-clipboard';
import AppState from '../AppState';
import {bonuses, messages, users} from '../../api/proto';
import Long from 'long';
import {formatCurrencyAmount} from '../utils/Money';
import {
    getPhoneChangeError,
    getPhoneCodeByCountry,
    getPhoneConfirmationError,
} from '../../api/utils';
import {getPhoneMask} from '../utils/getPhoneMask';
import {NumberFormatProps} from 'react-number-format';
import {SitePages} from '../meta';
import {NotificationItemLevel} from '../components/Notificator';
import {formatSMSHref} from '../utils/formatSMSHref';
import {NOTIFICATION_CARDS, UsaRefInviteByCashierBonusMeta} from './BonusAppearance';
import {getSelectedCardClassName} from '../utils/getSelectedCardClassName';
import {RefSystemInvite} from './ref/RefSystemInvite';
import { androidPickContact } from '../utils/androidPickContact';

const COPIED_INVITE_MESSAGE_TIMEOUT = 1000;
const SMS_APP_WAITING_FOR_LAUNCH_TIMEOUT = 3000;

type ContactsPickerAPIOptions = 'name' | 'tel' | 'email' | 'address' | 'icon';

interface ShareData {
    text?: string;
    title?: string;
    url?: string;
}

interface NavigatorWithContactsAndShare extends Navigator {
    contacts: {
        select: (options: ContactsPickerAPIOptions[]) => Promise<
            {
                [key in ContactsPickerAPIOptions]: string[];
            }[]
        >;
        getProperties: () => Promise<ContactsPickerAPIOptions[]>;
    };
    share: (data?: ShareData) => Promise<void>;
}

interface OTPOptions {
    transport: string[];
}

interface CredentialRequestOptionsWithOTP extends CredentialRequestOptions {
    otp: OTPOptions;
}

const MOBILE_STEPS = ['inviter-name', 'guest-name', 'location', 'message', 'final'] as const;

export class RefSystemActions {
    readonly INVITES_PER_PAGE = 10;
    @observable isBonusInfoLoaded = false;
    @observable isInvitesInfoLoaded = false;
    @observable isInvitesLoading = true;
    @observable isSendingInvite = false;
    savedNewPhone = '';
    @observable phone = '';
    @observable pickedContactName = '';
    @observable countryCode = '';
    @observable totalCount = Long.ZERO;
    @observable totalObtainedBonusesAmount = Long.ZERO;
    @observable activePageNumber = 0;
    @observable isContactPicked = false;
    @observable isContactPickerAccessDenied = false;
    invites = observable<RefSystemInvite>([], {
        deep: false,
    });
    @observable bonusInfo?: users.invitations.IInvitationBonusesInfoResponse;
    @observable inviteErrorCode?: users.invitations.InvitationManagementStatus;
    @observable isRulesLoaded = false;
    @observable rules?: {__html: string};
    @observable ownPhoneError?: string;
    @observable isOwnPhoneSubmitting: boolean = false;
    @observable ownConfirmCode = '';
    @observable isOwnPhoneInputShown = true;
    @observable lastInviteInfo?: users.invitations.IUserInvitation;
    @observable isCopiedToClipboard = false;
    @observable copiedTimer: ReturnType<typeof setTimeout>;
    @observable isBonusesManualActivation = false;
    @observable isBonusActivating = false;
    smsButtonRef: React.RefObject<HTMLAnchorElement> = createRef();
    invitesListRef: React.RefObject<HTMLDivElement> = createRef();
    @observable mobileCurrentStep?: typeof MOBILE_STEPS[number];
    @observable inviterName = '';
    @observable location = '';
    @observable message = '';
    @observable selectedCardIndex = 0;
    webOtpSignal?: AbortController;

    @computed
    get isInviteFormWithSteps(): boolean {
        return this.appState.appSettings.sendSMSInvitesWithCard;
    }

    @computed
    get isPhoneValid(): boolean {
        const phoneNumbersLength = this.phone.replace(/\D/g, '').length;
        const maskLength = this.phoneMask?.match(new RegExp('#', 'g'))?.length ?? 0;
        return (
            (this.isContactPicked && phoneNumbersLength >= 6) ||
            (!this.isContactPicked && phoneNumbersLength >= maskLength)
        );
    }

    @computed
    get isInviteSendButtonDisabled(): boolean {
        return !this.isPhoneValid || this.isSendingInvite;
    }

    @computed
    get isAddOwnPhoneButtonDisabled(): boolean {
        return !this.isPhoneValid || this.isOwnPhoneSubmitting;
    }

    @computed
    get isOwnPhoneConfirmButtonDisabled(): boolean {
        return (
            this.isOwnPhoneSubmitting ||
            this.ownConfirmCode.length !== this.appState.confirmationCodeMethodState.codeLength
        );
    }

    @computed
    get cardInvitesMeta(): UsaRefInviteByCashierBonusMeta[] {
        return NOTIFICATION_CARDS.map((card) => {
            return {
                selectedCard: card,
                inviterName: this.inviterName,
                inviteeName: this.pickedContactName,
                locationAddress: this.location,
                textFromLocation: this.message,
                locationName: this.inviterName,
                locationPhone: this.appState.rawUserInfo?.phone ?? undefined,
                className: getSelectedCardClassName(card),
            };
        });
    }

    @computed
    get isEnabledInBonuses(): boolean {
        return (
            (this.appState.siteConfig.userProfile?.refSystem &&
                this.appState.site.showInvitationBonusesInfoInBonuses) ??
            false
        );
    }

    @computed
    get isReady(): boolean {
        return this.isBonusInfoLoaded && this.isInvitesInfoLoaded;
    }

    @computed
    get isAnyInvites(): boolean {
        return !this.totalCount.isZero();
    }

    @computed
    get isPaginatorShown(): boolean {
        return this.totalCount.greaterThan(this.INVITES_PER_PAGE);
    }

    @computed
    get numberOfPages(): number {
        return Math.ceil(this.totalCount.toNumber() / this.INVITES_PER_PAGE);
    }

    @computed
    get paginationPages(): number[] {
        const pages: number[] = [];
        let numberOfPages = this.numberOfPages;
        while (numberOfPages !== 0) {
            pages.push(numberOfPages);
            numberOfPages--;
        }
        return pages.reverse();
    }

    @computed
    get formattedTotalObtainedBonusesAmount() {
        return formatCurrencyAmount(
            this.totalObtainedBonusesAmount,
            this.appState.userInfo.realBalance?.currency,
            false,
        );
    }

    @computed
    get formattedGuestBonusAmount() {
        return formatCurrencyAmount(
            this.bonusInfo?.guestBonusAmount,
            this.appState.userInfo.realBalance?.currency,
            false,
        );
    }

    @computed
    get formattedGuestMinDepositAmount() {
        return formatCurrencyAmount(
            this.bonusInfo?.guestMinDepositAmount,
            this.appState.userInfo.realBalance?.currency,
            false,
        );
    }

    @computed
    get formattedInviterBonusAmount() {
        return formatCurrencyAmount(
            this.bonusInfo?.inviterBonusAmount,
            this.appState.userInfo.realBalance?.currency,
            false,
        );
    }

    @computed
    get phoneMask(): string | undefined {
        return getPhoneMask(this.countryCode, this.appState.site.countries);
    }

    @computed
    get phonePlaceholder(): string | undefined {
        return this.phoneMask
            ? this.phoneMask.split('#').join('_')
            : this.appState.t('ui-phone-number');
    }

    @computed
    get canShowPhoneConfirmationInput() {
        if (this.appState.rawUserInfo) {
            return (
                (!!this.appState.rawUserInfo.phone && !this.appState.rawUserInfo.phoneConfirmed) ||
                !!this.appState.rawUserInfo.newPhone
            );
        } else {
            return false;
        }
    }

    @computed
    get isPhoneSubmitted(): boolean {
        return this.canShowPhoneConfirmationInput && !this.isOwnPhoneInputShown;
    }

    @computed
    get isPhoneConfirmed(): boolean {
        return this.appState.rawUserInfo?.phoneConfirmed ?? false;
    }

    @computed
    get smsLink(): string | undefined {
        return this.lastInviteInfo ? formatSMSHref(this.lastInviteInfo) : undefined;
    }

    @computed
    get isFormDisabled(): boolean {
        return (
            this.isSendingInvite ||
            (this.lastInviteInfo?.invitationStatus ===
                users.invitations.InvitationBusinessStatus.MANUAL_SENDING &&
                this.isManualSMSMode)
        );
    }

    @computed
    get refInviterBonusesCount(): number {
        return (this.appState.userBonusesStore.refInviterBonuses ?? []).length;
    }

    @computed
    get refInviterBonusAvailableForActivation(): bonuses.IBonusCode | undefined {
        return this.appState.userBonusesStore.refInviterBonuses?.find(
            (_) => _.meta?.can_activate === 'true',
        );
    }

    @computed
    get submitPhoneTitle(): string {
        if (this.appState.page === SitePages.FORCED_PHONE_CONFIRMATION) {
            if (
                !this.appState.confirmationCodeMethodState.isChoiseAvailable &&
                this.appState.confirmationCodeMethodState.selectedMethod ===
                    this.appState.proto.site.ConfirmationMethod.VOICE
            ) {
                return this.formattedGuestBonusAmount
                    ? this.appState.t('verify-phone-voice-and-get-bonus-with-real-amount', {
                          guestBonusAmount: this.formattedGuestBonusAmount,
                      })
                    : this.appState.t('verify-phone-voice-and-get-bonus');
            } else {
                return this.formattedGuestBonusAmount
                    ? this.appState.t('verify-phone-and-get-bonus-with-real-amount', {
                          guestBonusAmount: this.formattedGuestBonusAmount,
                      })
                    : this.appState.t('verify-phone-and-get-bonus');
            }
        } else {
            if (
                !this.appState.confirmationCodeMethodState.isChoiseAvailable &&
                this.appState.confirmationCodeMethodState.selectedMethod ===
                    this.appState.proto.site.ConfirmationMethod.VOICE
            ) {
                return this.appState.t('please-add-your-phone-before-inviting-voice');
            } else {
                return this.appState.t('please-add-your-phone-before-inviting');
            }
        }
    }

    @computed
    get confirmCodeTitle(): string {
        if (this.appState.page === SitePages.FORCED_PHONE_CONFIRMATION) {
            if (this.appState.confirmationCodeMethodState.isChoiseAvailable) {
                if (!this.appState.confirmationCodeMethodState.isFirstRetry) {
                    return this.formattedGuestBonusAmount
                        ? this.appState.t(
                              'confirmation-call-started-and-get-bonus-with-real-amount',
                              {
                                  guestBonusAmount: this.formattedGuestBonusAmount,
                              },
                          )
                        : this.appState.t('confirmation-call-started-and-get-bonus');
                }
            } else if (
                this.appState.confirmationCodeMethodState.selectedMethod ===
                this.appState.proto.site.ConfirmationMethod.VOICE
            ) {
                return this.formattedGuestBonusAmount
                    ? this.appState.t('confirmation-call-started-and-get-bonus-with-real-amount', {
                          guestBonusAmount: this.formattedGuestBonusAmount,
                      })
                    : this.appState.t('confirmation-call-started-and-get-bonus');
            }
            return this.formattedGuestBonusAmount
                ? this.appState.t('verify-phone-and-get-bonus-with-real-amount', {
                      guestBonusAmount: this.formattedGuestBonusAmount,
                  })
                : this.appState.t('verify-phone-and-get-bonus');
        } else {
            if (this.appState.confirmationCodeMethodState.isChoiseAvailable) {
                if (!this.appState.confirmationCodeMethodState.isFirstRetry) {
                    return this.appState.t('confirmation-call-started');
                }
            } else if (
                this.appState.confirmationCodeMethodState.selectedMethod ===
                this.appState.proto.site.ConfirmationMethod.VOICE
            ) {
                return this.appState.t('confirmation-call-started');
            }
            return this.appState.t('ui-sms-sent-to-number');
        }
    }

    get isManualSendFromAndroid(): boolean {
        return (
            this.appState.appSettings.sendSMSInvitesFromClientOnAndroid &&
            /(android)/i.test(navigator.userAgent)
        );
    }

    get isManualSMSMode(): boolean {
        return (
            this.isManualSendFromAndroid || this.appState.appSettings.sendAllSMSInvitesFromClient
        );
    }

    get offset(): Long {
        return Long.fromNumber(this.activePageNumber).multiply(this.INVITES_PER_PAGE);
    }

    get isContactsPickerSupported(): boolean {
        return 'contacts' in navigator && 'ContactsManager' in window;
    }

    get isWebShareSupported(): boolean {
        const nav = navigator as NavigatorWithContactsAndShare;
        return 'share' in nav;
    }

    constructor(private appState: AppState) {
        this.pickContact = this.pickContact.bind(this);
        this.getSubmitCodeFromWebOtp = this.getSubmitCodeFromWebOtp.bind(this);
        this.shareLastInviteLink = this.shareLastInviteLink.bind(this);
    }

    @action.bound
    clearState(): void {
        this.isInvitesLoading = true;
        this.isSendingInvite = false;
        this.phone = '';
        this.totalCount = Long.ZERO;
        this.totalObtainedBonusesAmount = Long.ZERO;
        this.activePageNumber = 0;
        this.invites.forEach((_) => _.cleanUp());
        this.invites.clear();
        if (!this.appState.site.showInvitationBonusesInfoInBonuses) {
            this.bonusInfo = undefined;
            this.isBonusInfoLoaded = false;
        }
        this.isInvitesInfoLoaded = false;
        this.inviteErrorCode = undefined;
        this.isContactPicked = false;
        this.isContactPickerAccessDenied = false;
        this.ownPhoneError = undefined;
        this.isOwnPhoneSubmitting = false;
        this.ownConfirmCode = '';
        this.isOwnPhoneInputShown = false;
        this.lastInviteInfo = undefined;
        clearTimeout(this.copiedTimer);
        this.pickedContactName = '';
    }

    @action.bound
    updateCountry(event: React.ChangeEvent<HTMLSelectElement>) {
        this.countryCode = event.target.value;
    }

    @action.bound
    updatePhone(phone: NumberFormatProps | string): void {
        if (typeof phone === 'string') {
            this.phone = phone;
        } else {
            this.phone = phone.formattedValue;
        }
    }

    @action.bound
    updatePhoneUnformatted(event: React.ChangeEvent<HTMLInputElement>): void {
        this.phone = event.target.value;
        this.pickedContactName = '';
    }

    async pickContact() {
        try {
            if (this.appState.isApk) {
                const contact = await androidPickContact();
                const contactParsed = JSON.parse(contact);
                runInAction(() => {
                    if ("phone" in contactParsed && contactParsed.phone.length > 0) {
                        this.isContactPickerAccessDenied = false;
                        this.isContactPicked = true;
                        this.phone = contactParsed.phone;
                        this.pickedContactName = contactParsed.name ?? "";
                    } else if ("error" in contactParsed) {
                        if (contactParsed.error === "PERMISSION_DENIED") {
                            this.isContactPickerAccessDenied = true;
                        }
                        console.error("pickContact", contactParsed);
                    }
                });
            } else if (this.isContactsPickerSupported) {
                const nav = navigator as NavigatorWithContactsAndShare;
                const supportedProperties = await nav.contacts.getProperties();
                if (!supportedProperties.includes('tel')) {
                    runInAction(() => {
                        this.isContactPickerAccessDenied = true;
                    });
                } else {
                    const contacts = await nav.contacts.select(['tel', 'name']);
                    runInAction(() => {
                        this.isContactPickerAccessDenied = false;
                        if (contacts.length > 0) {
                            const {tel, name} = contacts[0];
                            if (tel && tel.length > 0) {
                                this.phone = tel[0];
                                this.isContactPicked = true;
                            }
                            if (name && name.length > 0) {
                                this.pickedContactName = name[0];
                            } else {
                                this.pickedContactName = '';
                            }
                        }
                    });
                }
            }
        } catch (error) {
            runInAction(() => {
                this.isContactPickerAccessDenied = true;
            });
            console.error(error);
        }
    }

    @action.bound
    loadBonusInfo() {
        this.appState.api.refInvitationBonusesInfoRequest((msg, act) => {
            const response = act.invitationBonusesInfoResponse;
            runInAction(() => {
                if (response) {
                    if (response.bonusesManualActivation) {
                        this.appState.userBonusesStore.fetchBonuses();
                    }
                    this.isBonusesManualActivation = response.bonusesManualActivation ?? false;
                    this.preparePhone();
                    this.bonusInfo = response;
                    this.isBonusInfoLoaded = true;
                }
            });
        });
    }

    @action.bound
    loadInvites(trigger?: 'SSE') {
        const numberOfAcceptedInvites = this.invites.filter((_) => _.isAccepted).length;
        const offset = this.offset;
        this.isInvitesLoading = true;
        this.appState.api.refListInvitationsRequest(this.INVITES_PER_PAGE, offset, (msg, act) => {
            const response = act.listInvitationsResponse;
            if (response && offset.eq(this.offset)) {
                this.invites.forEach((_) => _.cleanUp());
                this.invites.replace(
                    response.entries?.map((entry) => new RefSystemInvite(entry, this.appState)) ??
                        [],
                );
                this.totalCount = response.totalCount ?? Long.ZERO;
                this.totalObtainedBonusesAmount = response.totalObtainedBonusesAmount ?? Long.ZERO;
                this.isInvitesLoading = false;
                this.isInvitesInfoLoaded = true;
                if (trigger === 'SSE') {
                    const newNumberOfAcceptedInvites = this.invites.filter(
                        (_) => _.isAccepted,
                    ).length;
                    if (
                        numberOfAcceptedInvites !== newNumberOfAcceptedInvites &&
                        this.isBonusesManualActivation
                    ) {
                        this.appState.userBonusesStore.fetchBonuses();
                    }
                }
            }
        });
    }

    @action.bound
    sendInvite() {
        if (!this.isSendingInvite) {
            this.lastInviteInfo = undefined;
            this.inviteErrorCode = undefined;
            this.isSendingInvite = true;
            if (this.phone) {
                const params: users.invitations.ISendInvitationRequest =
                    this.isContactPicked || !this.countryCode
                        ? {phone: this.phone}
                        : {
                              phone: `+${getPhoneCodeByCountry(
                                  this.countryCode,
                                  this.appState.siteConfig!.countries!,
                              )}${this.phone}`,
                              countryCode: this.countryCode,
                          };
                params.manual = this.isManualSMSMode;
                params.optionalName = this.pickedContactName || undefined;
                if (this.isInviteFormWithSteps) {
                    params.messageMeta = {
                        type: users.invitations.InvitationMessageMeta.InvitationSenderType
                            .FROM_PLAYER,
                        inviterName: this.inviterName || undefined,
                        inviteeName: this.pickedContactName || undefined,
                        locationAddress: this.location || undefined,
                        textFromLocation: this.message || undefined,
                        selectedCard:
                            this.cardInvitesMeta.length > this.selectedCardIndex
                                ? this.cardInvitesMeta[this.selectedCardIndex].selectedCard
                                : undefined,
                        locationName: this.inviterName || undefined,
                        locationPhone: this.appState.rawUserInfo?.phone ?? undefined,
                    };
                }
                this.appState.api.refSendInviteRequest(params, (msg, act) => {
                    const response = act.sendInvitationResponse;
                    if (response && !response.status) {
                        this.lastInviteInfo = response.invitation ?? undefined;
                        if (
                            response.invitation?.invitationStatus ===
                            users.invitations.InvitationBusinessStatus.MANUAL_SENDING
                        ) {
                            if (this.isManualSendFromAndroid) {
                                setTimeout(() => {
                                    this.smsButtonRef.current?.click();
                                }, 0);
                            } else {
                                this.afterInvite();
                            }
                        } else {
                            this.afterInvite();
                        }
                    } else {
                        this.mobileCurrentStep = undefined;
                        this.inviteErrorCode = response?.status ?? undefined;
                    }
                    this.isSendingInvite = false;
                });
            }
        }
    }

    @action.bound
    afterInvite() {
        this.appState.modal.showRefInviteWasSent();
        this.isContactPicked = false;
        this.phone = '';
        this.loadInvites();
    }

    @action.bound
    afterSMSInvite() {
        setTimeout(() => {
            this.afterInvite();
        }, SMS_APP_WAITING_FOR_LAUNCH_TIMEOUT);
    }

    @action.bound
    goToPrevPage() {
        if (this.activePageNumber !== 0) {
            this.openPage(this.activePageNumber);
        }
    }

    @action.bound
    goToNextPage() {
        if (this.activePageNumber !== this.numberOfPages - 1) {
            this.openPage(this.activePageNumber + 2);
        }
    }

    @action.bound
    openPage(page: number) {
        this.activePageNumber = page - 1;
        this.loadInvites();
        this.invitesListRef.current?.scrollIntoView(true);
    }

    @action.bound
    handleStatusChangeEvent() {
        if (this.isInvitesInfoLoaded) {
            this.loadInvites('SSE');
        }
    }

    @action.bound
    showRefInviteForm() {
        if (this.isPhoneConfirmed) {
            this.phone = '';
        }
        this.appState.modal.showRefInviteForm();
    }

    @action.bound
    showInviteAnotherFriend() {
        this.isContactPicked = false;
        this.phone = '';
        this.pickedContactName = '';
        this.inviteErrorCode = undefined;
        this.lastInviteInfo = undefined;
        this.inviterName = '';
        this.location = '';
        this.message = '';
        this.mobileCurrentStep = undefined;
        this.selectedCardIndex = 0;
        this.appState.ref.showRefInviteForm();
    }

    @action.bound
    hideModal() {
        this.isContactPicked = false;
        this.phone = '';
        this.pickedContactName = '';
        this.inviterName = '';
        this.location = '';
        this.message = '';
        this.mobileCurrentStep = undefined;
        this.selectedCardIndex = 0;
        this.inviteErrorCode = undefined;
        this.lastInviteInfo = undefined;
        this.webOtpSignal?.abort();
        this.appState.modal.hideModal();
    }

    @action.bound
    submitOwnPhone() {
        if (!this.isOwnPhoneSubmitting) {
            this.isOwnPhoneSubmitting = true;
            this.ownPhoneError = undefined;
            this.getSubmitCodeFromWebOtp();
            if (
                this.phone === this.savedNewPhone &&
                this.appState.page !== this.appState.enums.sitePages.FORCED_PHONE_CONFIRMATION &&
                !this.appState.confirmationCodeMethodState.isChoiseAvailable
            ) {
                this.appState.api.resendNewPhoneConfirmation(this.processSubmitOwnPhoneResponse);
            } else {
                this.appState.api.changePhone(
                    this.phone,
                    this.countryCode,
                    this.appState.confirmationCodeMethodState.isChoiseAvailable
                        ? false
                        : this.appState.confirmationCodeMethodState.isVoiceSelected,
                    this.processSubmitOwnPhoneResponse,
                );
            }
        }
    }

    @action.bound
    processSubmitOwnPhoneResponse(
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) {
        this.appState.processApi(msg, act);
        if (!!act.changePhone?.ok || !!act.resendNewPhoneConfirmation?.ok) {
            this.appState.confirmationCodeMethodState.isFirstRetry = true;
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-phone-confirmation-resend-successful', {
                    phone: this.appState.rawUserInfo?.newPhone,
                }),
                level: NotificationItemLevel.SUCCESS,
            });
            this.isOwnPhoneInputShown = false;
            this.appState.confirmForm.activateResendTimer();
        } else {
            this.ownPhoneError = this.appState.t(getPhoneChangeError(act.changePhone?.error));
        }
        this.isOwnPhoneSubmitting = false;
    }

    @action.bound
    updateOwnCode(code: NumberFormatProps): void {
        this.ownConfirmCode = `${code.value}` ?? '';
        const enteredCodeLength = this.ownConfirmCode.replace(/\D/g, '').length;
        if (enteredCodeLength === this.appState.confirmationCodeMethodState.codeLength) {
            this.confirmOwnCode();
        }
    }

    @action.bound
    confirmOwnCode() {
        if (!this.isOwnPhoneSubmitting) {
            if (this.appState.page === SitePages.FORCED_PHONE_CONFIRMATION) {
                this.appState.modal.hideModal();
            }
            this.isOwnPhoneSubmitting = true;
            this.ownPhoneError = undefined;
            this.appState.api.confirmNewPhone(this.ownConfirmCode, (msg, act) => {
                this.appState.processApi(msg, act);
                if (act.confirmNewPhone && act.confirmNewPhone.ok) {
                    this.ownConfirmCode = '';
                    this.phone = '';
                    this.appState.confirmForm.activateResendTimer();
                } else {
                    this.ownPhoneError = this.appState.t(
                        getPhoneConfirmationError(act.confirmNewPhone?.error),
                    );
                }
                this.isOwnPhoneSubmitting = false;
            });
        }
    }

    @action.bound
    resendOwnConfirmCode() {
        this.ownConfirmCode = '';
        this.ownPhoneError = undefined;
        this.isOwnPhoneSubmitting = true;
        this.getSubmitCodeFromWebOtp();
        if (
            this.appState.confirmationCodeMethodState.isFirstRetry &&
            this.appState.confirmationCodeMethodState.isChoiseAvailable
        ) {
            this.appState.api.changePhone(
                this.phone,
                this.countryCode,
                true,
                (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                    if (act.changePhone?.ok) {
                        this.processResendOwnConfirmCode(msg, act);
                    } else {
                        this.ownPhoneError = this.appState.t(
                            getPhoneChangeError(act.changePhone?.error),
                        );
                    }
                },
            );
        } else {
            this.appState.api.resendNewPhoneConfirmation(
                (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                    if (act.resendNewPhoneConfirmation?.ok) {
                        this.processResendOwnConfirmCode(msg, act);
                    } else {
                        this.ownPhoneError = this.appState.t(
                            `error-resend-new-phone-confirmation-${
                                act.resendNewPhoneConfirmation?.error ??
                                users.ResendPhoneConfirmationResponse.ResendConfirmPhoneError
                                    .unauthenticated
                            }`,
                        );
                    }
                },
            );
        }
    }

    @action.bound
    processResendOwnConfirmCode(
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) {
        this.appState.processApi(msg, act);
        this.appState.confirmForm.activateResendTimer();
        this.isOwnPhoneSubmitting = false;
        this.appState.confirmationCodeMethodState.isFirstRetry = false;
    }

    async getSubmitCodeFromWebOtp(): Promise<void> {
        if ('OTPCredential' in window) {
            try {
                this.webOtpSignal?.abort();
                this.webOtpSignal = new AbortController();
                if (navigator.credentials) {
                    const getOTPCreds = navigator.credentials.get as (
                        params: CredentialRequestOptionsWithOTP,
                    ) => Promise<{code?: string}>;
                    const otp = await getOTPCreds({
                        signal: this.webOtpSignal.signal,
                        otp: {transport: ['sms']},
                    });
                    runInAction(() => {
                        if (otp?.code) {
                            this.ownConfirmCode = otp.code;
                            this.confirmOwnCode();
                        }
                    });
                }
            } catch (error) {
                console.error(error);
            }
        }
    }

    @action.bound
    backToOwnPhoneInput() {
        this.webOtpSignal?.abort();
        this.ownConfirmCode = '';
        this.ownPhoneError = undefined;
        this.isOwnPhoneInputShown = true;
    }

    @action.bound
    openProfileRefPage() {
        this.hideModal();
        this.appState.site.doCloseMobileMenu();
        this.appState.router.push(`/${SitePages.PROFILE_REF_SYSTEM}`);
    }

    @action.bound
    copyLastInviteLink() {
        if (this.lastInviteInfo?.loginLink) {
            clearTimeout(this.copiedTimer);
            copyToClipboard(this.lastInviteInfo.loginLink);
            this.isCopiedToClipboard = true;
            this.copiedTimer = setTimeout(() => {
                runInAction(() => {
                    this.isCopiedToClipboard = false;
                });
            }, COPIED_INVITE_MESSAGE_TIMEOUT);
        }
    }

    async shareLastInviteLink() {
        try {
            if (this.lastInviteInfo?.loginLink) {
                const nav = navigator as NavigatorWithContactsAndShare;
                if ('share' in nav) {
                    await nav.share({
                        url: this.lastInviteInfo.loginLink,
                        text: this.lastInviteInfo.messageText ?? undefined,
                    });
                }
            }
        } catch (error) {
            console.error(error);
        }
    }

    @action.bound
    updateInviterName(event: React.ChangeEvent<HTMLSelectElement>) {
        if (event.target.value !== ' ') {
            this.inviterName = event.target.value;
        }
    }

    @action.bound
    updatePickedContactName(event: React.ChangeEvent<HTMLSelectElement>) {
        if (event.target.value !== ' ') {
            this.pickedContactName = event.target.value;
        }
    }

    @action.bound
    updateLocation(event: React.ChangeEvent<HTMLSelectElement>) {
        if (event.target.value !== ' ') {
            this.location = event.target.value;
        }
    }

    @action.bound
    updateMessage(event: React.ChangeEvent<HTMLSelectElement>) {
        if (event.target.value !== ' ') {
            this.message = event.target.value;
        }
    }

    @action.bound
    goToPrevStep() {
        switch (this.mobileCurrentStep) {
            case 'inviter-name':
                this.mobileCurrentStep = undefined;
                break;
            case 'guest-name':
                this.mobileCurrentStep = 'inviter-name';
                break;
            case 'location':
            case 'message':
                this.mobileCurrentStep = 'final';
                break;
            case 'final':
                this.mobileCurrentStep = 'guest-name';
                break;
            default:
                break;
        }
    }

    @action.bound
    goToNextStep() {
        switch (this.mobileCurrentStep) {
            case undefined:
                this.mobileCurrentStep = 'inviter-name';
                break;
            case 'inviter-name':
                this.mobileCurrentStep = 'guest-name';
                break;
            case 'guest-name':
            case 'location':
            case 'message':
                this.mobileCurrentStep = 'final';
                break;
        }
    }

    @action.bound
    goToLocationStep() {
        this.mobileCurrentStep = 'location';
    }

    @action.bound
    goToMessageStep() {
        this.mobileCurrentStep = 'message';
    }

    @action.bound
    handleKeyPressOnMobileSteps(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.charCode === 13) {
            this.goToNextStep();
        }
    }

    @action.bound
    handleKeyPressSubmitOwnPhone(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.charCode === 13 && !this.isAddOwnPhoneButtonDisabled) {
            this.submitOwnPhone();
        }
    }

    @action.bound
    handleKeyPressSubmitPhone(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.charCode === 13 && !this.isInviteSendButtonDisabled) {
            if (this.isInviteFormWithSteps) {
                this.goToNextStep();
            } else {
                this.sendInvite();
            }
        }
    }

    @action.bound
    updateSelectedCardIndex(index: number) {
        this.selectedCardIndex = index;
    }

    @action.bound
    activateRefInviterBonus() {
        if (this.refInviterBonusAvailableForActivation?.code) {
            const bonusValue = this.appState.profileBonuses.processBonusValue(
                this.refInviterBonusAvailableForActivation,
                null,
            );
            this.isBonusActivating = true;
            this.appState.profileBonuses.tryAcceptBonus(
                this.refInviterBonusAvailableForActivation.code,
                this.afterAcceptBonus,
                bonusValue,
            );
        }
    }

    @action.bound
    afterAcceptBonus() {
        this.isBonusActivating = false;
    }

    @action.bound
    preparePhone() {
        let countryCode = this.appState.siteConfig.countries?.phoneDetectedCountry;
        if (this.appState.siteConfig.countries?.countries?.every((_) => _.code !== countryCode)) {
            countryCode = this.appState.siteConfig.countries?.phoneDefaultCountry;
            if (
                this.appState.siteConfig.countries?.countries?.every((_) => _.code !== countryCode)
            ) {
                countryCode = this.appState.siteConfig.countries?.defaultCountry;
                if (
                    this.appState.siteConfig.countries?.countries?.every(
                        (_) => _.code !== countryCode,
                    ) &&
                    this.appState.siteConfig.countries?.countries.length > 0
                ) {
                    countryCode = this.appState.siteConfig.countries?.countries[0].code;
                }
            }
        }
        if (this.appState.rawUserInfo?.newPhone && !this.isPhoneConfirmed) {
            const parsedPhone = parsePhoneNumberFromString(
                this.appState.rawUserInfo.newPhone,
                countryCode as CountryCode,
            );
            if (parsedPhone) {
                const nationalNumber = parsedPhone?.nationalNumber.toString() ?? '';
                this.savedNewPhone = nationalNumber;
                this.phone = nationalNumber;
            }
        }
        if (this.isPhoneConfirmed) {
            this.phone = '';
        }
        this.countryCode = countryCode ?? '';
    }

    @action.bound
    onForcedConfirmationOpen() {
        this.appState.confirmForm.activateResendTimer();
        this.appState.ref.loadBonusInfo();
        if (this.appState.confirmationCodeMethodState.isVoiceEnabled) {
            if (this.appState.confirmationCodeMethodState.isChoiseAvailable) {
                this.appState.confirmationCodeMethodState.isFirstRetry = true;
                this.preparePhone();
            } else {
                this.isOwnPhoneInputShown = true;
            }
        }
    }

    @action.bound
    onRefModalOpen() {
        this.isOwnPhoneInputShown = true;
        this.appState.confirmationCodeMethodState.isFirstRetry = true;
        this.preparePhone();
    }
}
