import { Long } from 'long';
import {action, computed, observable, runInAction} from 'mobx';
import { jpmonitor } from '../../../api/proto';
import AppState from '../../AppState';
import { timeStampToDDHHMMSS } from '../../utils/Datetime';
import { formatCurrencyAmount } from '../../utils/Money';

export interface ITournamentInfo extends jpmonitor.ITournamentState {
    prizePlacesFormatted?: {[key: number]: string};
    timeToPrizeFormatted?: string;
    tournamentId?: string;
}

export class TournamentInfo implements ITournamentInfo {
    @observable prizePlaces?: jpmonitor.TournamentState.IPrizePlace[] | null | undefined;
    @observable prizePlacesFormatted?: { [key: number]: string; } | undefined;
    @observable timeToPrize?: Long | null | undefined;
    @observable timeToPrizeFormatted?: string | undefined;
    @observable current?: jpmonitor.TournamentState.IScoreBoardShortEntry | null | undefined;
    @observable tournamentId?: string;

    constructor(props: ITournamentInfo) {
        Object.assign(this, props)
    }
}

export default class TournamentsInfoActions {
    @observable tournaments?: ITournamentInfo[] = [];
    @observable tournamentUuid?: string;

    interval?: number;

    constructor(public appState: AppState) {
    }

    handleTournaments = (tournaments: jpmonitor.ITournamentState[]) => {
        window.clearInterval(this.interval);
        window.setTimeout(() => {
            this.appState.tournamentsInfoActions.processTournaments(tournaments);
        }, 0);
    }

    @action.bound
    processTournaments(tournaments: jpmonitor.ITournamentState[]) {
        this.tournaments = tournaments.map(tournament => {
            const prizePlacesFormatted = {};
            if (tournament.prizePlaces) {
                tournament.prizePlaces.forEach((place) => {
                    if (place.place && place.possibleWin) {
                        prizePlacesFormatted[place.place] = formatCurrencyAmount(place.possibleWin, this.appState.userInfo.currency, false, true);
                    }
                })
            }
            return new TournamentInfo({
                ...tournament,
                prizePlacesFormatted,
                timeToPrizeFormatted: tournament.timeToPrize ? timeStampToDDHHMMSS(tournament.timeToPrize) : '0',
                tournamentId: tournament.uuid ? String(new TextDecoder().decode(tournament.uuid)) : '',
            })
        });
        if (this.tournaments?.length) {
            this.interval = window.setInterval(() => {
                runInAction(() => {
                    this.tournaments = this.tournaments?.map((tournament) => ({
                        ...tournament,
                        timeToPrize: tournament.timeToPrize ? tournament.timeToPrize.subtract(1000) : undefined,
                        timeToPrizeFormatted: tournament.timeToPrize ? timeStampToDDHHMMSS(tournament.timeToPrize) : '0',
                    }))
                });
            }, 1000)
        }
    }

    @computed
    get selectedTournamentGames() {
        if (this.tournamentUuid && this.tournaments?.length) {
            const tournament = this.tournaments.find(tournament=>tournament.tournamentId === this.tournamentUuid);
            if (tournament?.tournamentMeta?.games?.length) {
                return tournament.tournamentMeta.games.map(game => {
                    return this.appState.games.findGameById(game)
                }).sort((a, b) => {
                    if (a && b) {
                        if (a.id === 'gmz_kings_of_africa_nudge' || b.id === 'gmz_kings_of_africa_nudge') {
                            if (a.id === 'gmz_kings_of_africa_nudge') return -1;
                            else return 1;
                        }
                        if (a.tags?.includes('double_nudge') && !b.tags?.includes('double_nudge')) {
                            return -1;
                        }
                        if (!a.tags?.includes('double_nudge') && b.tags?.includes('double_nudge')) {
                            return 1;
                        }
                    }
                    return 0;
                });
            }
        }
        return [];
    }

    @computed
    get selectedTournament() {
        if (this.tournamentUuid && this.tournaments?.length) {
            const tournament = this.tournaments.find(tournament=>tournament.tournamentId === this.tournamentUuid);
            return tournament;
        }
        return undefined;
    }

    @action.bound
    goToTournamentGames(id?: string) {
        if (id) {
            this.appState.router.push(this.appState.l('/tournaments-list'))
            this.tournamentUuid = id;
        }
    }

    @action.bound
    goToTournamentsPage() {
        this.tournamentUuid = undefined;
    }

    @computed
    get tournamentsList() {
        return this.tournamentUuid ? [this.selectedTournament] : this.tournaments;
    }

    
}