import {action, computed, observable} from 'mobx';
import Long from 'long';
import AppState from '../../AppState';
import {bonuses, messages} from '../../../api/proto';
import {GameItem} from '../GamesActions';
import {ActiveTournamentState, NString} from '../../meta';
import {format} from 'date-fns';
import {formatCurrencyAmount, formatScoresCurrency, formatCurrencyCode, isScoreCurrencyId, NASymbol} from '../../utils/Money';
import {
    BonusMeta,
    BonusType,
    checkRequiredFieldsPresent,
    muteString,
    processMetaList,
    processString,
    mutePhoneCode,
} from '../../../api/utils';

const tournamentsAppearance = require('./tournaments-appearance-settings.json');

export enum TournamentStatus {
    ACTIVE = 'active',
    FINISHED = 'finished',
    UPCOMING = 'upcoming'
}

export enum TournamentHeaderLayout {
    POSITION_RIGHT = 'position-right'
}

export enum TournamentType {
    BATTLE_OF_SLOTS = 'battle_of_slots',
    EGYPT_TREASURES = 'egypt_treasures',
    GOLDEN_FEVER = 'golden_fever',
    HOLIDAYS_ARE_COMING = 'holidays_are_coming',
    KENOMANIA = 'kenomania',
    LOVE_DAY = 'love_day',
    XMAS_HOLIDAYS = 'xmas_holidays',
    DEFENDER_OF_THE_FATHERLAND = 'defender_of_the_fatherland',
    WOMENS_DAY = 'womens_day',
    VACATION = 'vacation',
    GOLD_RUSH = 'gold-rush',
    WEEKLY = 'weekly'
}

export interface TournamentLeader {
    index: number;
    name: string;
    email?: NString;
    phone?: NString;
    login?: NString;
    amount: Long;
    currency: Long;
    amountStr: string;
}

export class Tournament {
    id: string;
    type: string;
    headerLayout?: string;
    name: string;
    promoText?: string;
    rules: string;
    startAt: Long;
    finishAt: Long;
    timezone: string;
    status: TournamentStatus;

    prizePool: Long;
    prizePoolCurrency: Long;
    prizePoolStr: string;
    prizePoolCurrencyCode: string;
    prizePoolCaption?: string;
    prizePoolDistribution: string[];
    prizePoolParticipationPercent: string;
    progressivePrizePool: boolean;

    betsParticipationMinAmount: string;
    betsParticipationMaxAmount: string;
    spinsParticipationMinAmount: string;
    spinsParticipationMaxAmount: string;

    confirmedEmailRequired?: boolean;
    confirmedPhoneRequired?: boolean;

    games: GameItem[];
    leaders: TournamentLeader[];
    winners: TournamentLeader[];
    previousStartAt: string;
    previousFinishAt: string;

    mainPageClass?: string;
    tournamentPageClass?: string;
    tournamentListClass?: string;

    minDepositsCountToParticipate?: number;
    minDepositsAmount?: string;
}

type TourmanentMetaLeader = {
    userId?: string;
    amount?: string | number;
    currency?: string | number;
    email?: string;
    login?: string;
    phone?: string;
};

const requiredFields = [
    'tournament_id',
    'tournament_start_date',
    'tournament_end_date',
    'tournament_currency',
    'tournament_prize_pool'
];

export type TournamentMeta = {
    tournament_id: string;
    tournament_type: string;
    tournament_prize_pool: string;
    tournament_currency: string;
    tournament_data: string;
    tournament_games: string;
    tournament_rules: string;
    tournament_start_date: string;
    tournament_end_date: string;

    previous_tournament_winners: string;
    previous_tournament_start_date: string;
    previous_tournament_end_date: string;

    prize_pool_distribution: string
    prize_poll_accumulation_participation_percent: string

    prize_poll_accumulation_bets_allowed?: string;
    prize_poll_accumulation_bets_currency?: string;
    prize_poll_accumulation_bets_max_bet_amount?: string;
    prize_poll_accumulation_bets_min_bet_amount?: string;

    prize_poll_accumulation_spins_allowed?: string;
    prize_poll_accumulation_spins_amount_multiplier?: string;
    prize_poll_accumulation_spins_currency?: string;
    prize_poll_accumulation_spins_max_spin_amount?: string;
    prize_poll_accumulation_spins_min_spin_amount?: string;

    receive_conditions_email_confirmation_required?: string;
    receive_conditions_phone_confirmation_required?: string;

    tournament_enabled?: string;
    tournament_top_players_count?: string;
    
    tournament_required_deposits_min_deposit_amount?: string;
    tournament_required_deposits_count_to_participate?: string;

    // deprecated
    tournament_name?: string;
    prize_pool_caption?: string;
    main_page_class?: string;
    tournament_list_class?: string;
    tournament_page_class?: string;
};

export enum TournamentRulesArgs {
    SITE_NAME = '{site-name}',

    START_DATE = '{start-date}',
    END_DATE = '{end-date}',
    TIMEZONE = '{timezone}',
    CURRENCY = '{currency}',

    SPIN_MIN_AMOUNT = '{spin-min-amount}',
    SPIN_MAX_AMOUNT = '{spin-max-amount}',
    BET_MIN_AMOUNT = '{bet-min-amount}',
    BET_MAX_AMOUNT = '{bet-max-amount}',

    GAMES_LIST = '{games-list}',
    PRIZE_POOL_PERCENT = '{prize-pool-percent}',
    PRIZE_POOL_DISTRIBUTION = '{prize-pool-distribution}',
    PRIZE_POOL_DISTRIBUTION_LENGTH = '{prize-pool-distribution-length}',
    PARTICIPATION_CONDITIONS = '{participation-conditions}',

    MIN_DEPOSITS_COUNT_TO_PARTICIPATE = '{min-deposits-count-to-participate}',
    MIN_AMOUNT_PER_DEPOSIT = '{min-amount-per-deposit}',
}

export type TournamentAppearance = {
    name: string,
    promoText?: string,
    prizePoolCaption: string,
    headerLayout?: string;

    mainPageClass?: string,
    tournamentPageClass?: string,
    tournamentListClass?: string,
};

export default class TournamentActions {
    @observable isLoading: boolean = false;
    @observable tournaments: Tournament[] | null = null;
    @observable tournamentId: string | null = null;
    @observable tournamentRules: { [key: string]: string } = {};
    tournamentsFilterTypes = ['active', 'finished'];
    @observable tournamentsFilterType: string = 'active';

    constructor(public appState: AppState) {
    }

    fetchTournaments() {
        this.isLoading = true;
        this.appState.userBonusesStore.fetchBonuses(this.onFetchTournaments.bind(this));
    }

    onFetchTournaments(codes: bonuses.IBonusCode[], possibleCodes: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) {
        this.isLoading = false;

        const result: Tournament[] = [];

        for (let possibleCode of possibleCodes) {
            const bonusType = processString(BonusMeta.BONUS_TYPE, possibleCode.meta, null);

            if (bonusType === BonusType.TOURNAMENT) {
                if (this.isValidTournamentMeta(possibleCode.meta)) {
                    result.push(this.processTournament(possibleCode.meta as TournamentMeta));
                }
            }
        }

        this.tournaments = result.sort((a: Tournament, b: Tournament) => {
            return a.finishAt.toNumber() - b.finishAt.toNumber();
        });

        const query = new URLSearchParams(window.location.search);
        if (query.get('active-tournament') === ActiveTournamentState.LAST_ACTIVE) {
            const lastTournament = this.activeTournaments[0];

            if (lastTournament) {
                // redirect to last active tournament
                this.appState.router.push(`/tournaments/${lastTournament.id}`);
            }
        }

        this.fetchRules(this.tournamentId);
    }

    processTournament(meta: TournamentMeta): Tournament {
        const type = meta.tournament_type;
        const prizePool = Long.fromString(meta.tournament_prize_pool);
        const prizePoolCurrency = Long.fromString(meta.tournament_currency);
        const prizePoolAmount = formatCurrencyAmount(prizePool, prizePoolCurrency, false);
        const currencyCode = formatCurrencyCode(prizePoolCurrency);

        const enabled = meta.tournament_enabled === 'true';
        const startDate = +meta.tournament_start_date;
        const endDate = +meta.tournament_end_date;
        const status = this.processStatus(enabled, startDate, endDate);

        const gamesIds = processMetaList(meta.tournament_games) as string[];
        const games: GameItem[] = gamesIds
            .map((id: string) => this.appState.games.findGameById(id), this)
            .filter((g: GameItem | null) => !!g) as GameItem[];

        const metaLeaders = processMetaList(meta.tournament_data) as TourmanentMetaLeader[];
        const leaders = metaLeaders
            .map((ml: TourmanentMetaLeader, index: number) => this.processLeader(index, ml, meta), this)
            .filter((m: TournamentLeader | null) => !!m) as TournamentLeader[];

        const metaWinners = processMetaList(meta.previous_tournament_winners) as TourmanentMetaLeader[];
        const winners = metaWinners
            .map((ml: TourmanentMetaLeader, index: number) => this.processLeader(index, ml, meta), this)
            .filter((m: TournamentLeader | null) => !!m) as TournamentLeader[];
        const previousStartDate = Long.fromNumber(+meta.previous_tournament_start_date);
        const previousEndDate = Long.fromNumber(+meta.previous_tournament_end_date);

        const prizePoolDistribution = processMetaList(meta.prize_pool_distribution)
            .map((ratio: number) => parseFloat((ratio * 100).toFixed(2)) + '%');

        const tz = new Date().getTimezoneOffset() / 60;
        const sign = tz <= 0 ? '+' : '-';
        const timezone = 'GMT ' + sign + Math.abs(tz);

        const appearance = this.processTournamentAppearance(type, meta);

        const minDepositsAmount = meta.tournament_required_deposits_min_deposit_amount
            ? formatCurrencyAmount(Long.fromString(meta.tournament_required_deposits_min_deposit_amount), prizePoolCurrency, false)
            : undefined;
        const minDepositsCountToParticipate = meta.tournament_required_deposits_count_to_participate
            ? Number(meta.tournament_required_deposits_count_to_participate)
            : undefined;

        return {
            id: meta.tournament_id,
            type: meta.tournament_type,
            rules: meta.tournament_rules,
            status: status,

            name: appearance.name,
            promoText: appearance.promoText,
            prizePoolCaption: appearance.prizePoolCaption,
            headerLayout: appearance.headerLayout,
            mainPageClass: appearance.mainPageClass,
            tournamentPageClass: appearance.tournamentPageClass,
            tournamentListClass: appearance.tournamentListClass,

            prizePool: prizePool,
            prizePoolCurrency: prizePoolCurrency,
            prizePoolStr: prizePoolAmount,
            prizePoolCurrencyCode: currencyCode,

            prizePoolDistribution: prizePoolDistribution,
            prizePoolParticipationPercent: meta.prize_poll_accumulation_participation_percent,
            progressivePrizePool: !!meta.prize_poll_accumulation_participation_percent && meta.prize_poll_accumulation_participation_percent !== '0%',

            confirmedEmailRequired: meta.receive_conditions_email_confirmation_required === 'true',
            confirmedPhoneRequired: meta.receive_conditions_phone_confirmation_required === 'true',

            betsParticipationMinAmount: this.processAmount(meta.prize_poll_accumulation_bets_allowed, meta.prize_poll_accumulation_bets_min_bet_amount, meta.prize_poll_accumulation_bets_currency),
            betsParticipationMaxAmount: this.processAmount(meta.prize_poll_accumulation_bets_allowed, meta.prize_poll_accumulation_bets_max_bet_amount, meta.prize_poll_accumulation_bets_currency),
            spinsParticipationMinAmount: this.processAmount(meta.prize_poll_accumulation_spins_allowed, meta.prize_poll_accumulation_spins_min_spin_amount, meta.prize_poll_accumulation_spins_currency),
            spinsParticipationMaxAmount: this.processAmount(meta.prize_poll_accumulation_spins_allowed, meta.prize_poll_accumulation_spins_max_spin_amount, meta.prize_poll_accumulation_spins_currency),

            startAt: Long.fromNumber(startDate),
            finishAt: Long.fromNumber(endDate),
            timezone: timezone,

            games: games,
            leaders: leaders,
            winners: winners,
            previousStartAt: this.appState.format(new Date(previousStartDate.toNumber()), 'dd.MM.yyyy HH:mm:ss'),
            previousFinishAt: this.appState.format(new Date(previousEndDate.toNumber()), 'dd.MM.yyyy HH:mm:ss'),

            minDepositsAmount,
            minDepositsCountToParticipate,
        };
    }

    processStatus(enabled: boolean, startDate: number, endDate: number): TournamentStatus {
        const now = new Date().getTime();
        if (enabled) {
            if (now > endDate) {
                return TournamentStatus.FINISHED;
            } else if (now < startDate) {
                return TournamentStatus.UPCOMING;
            } else {
                return TournamentStatus.ACTIVE;
            }
        } else {
            return TournamentStatus.FINISHED;
        }
    }

    processLeader(index: number, leaderMeta: TourmanentMetaLeader, tournamentMeta: TournamentMeta): TournamentLeader | null {
        const email = leaderMeta.email ? muteString(leaderMeta.email) : null;
        let phone = leaderMeta.phone ? muteString(leaderMeta.phone) : null;
        const login = leaderMeta.login ? muteString(leaderMeta.login) : null;

        if (this.appState.appSettings.mutePhoneCodes) {
            phone = mutePhoneCode(phone) || null;
        }

        if (!email && !phone && !login) {
            console.error('Unable to process leader, missing email, phone and login', leaderMeta, tournamentMeta);
            return null;
        }

        if (typeof leaderMeta.amount === 'undefined' || !leaderMeta.currency) {
            console.error('Unable to process leader, missing amount or currency', leaderMeta, tournamentMeta);
            return null;
        }

        const amount = this.processLong(leaderMeta.amount);
        if (amount.lessThan(1)) {
            return null;
        }

        const name = phone || email || login || NASymbol;
        const currency = this.processLong(leaderMeta.currency);
        const amountStr = isScoreCurrencyId(currency) ?
            formatScoresCurrency(amount) :
            formatCurrencyAmount(amount, currency, false);

        return {
            index: index + 1,
            name: name,
            email: email,
            phone: phone,
            amount: amount,
            login: login,
            currency: currency,
            amountStr: amountStr
        };
    }

    processLong(value: string | number): Long {
        if (typeof value === 'number') {
            return Long.fromNumber(value);
        } else {
            return Long.fromString(value);
        }
    }

    processAmount(enabled?: string, amountStr?: string, currencyStr?: string): string {
        if (enabled === 'true') {
            const amount = amountStr ? Long.fromString(amountStr) : null;
            const currency = currencyStr ? Long.fromString(currencyStr) : null;
            if (amount && currency) {
                return formatCurrencyAmount(amount, currency, false);
            } else {
                return NASymbol;
            }
        } else {
            return NASymbol;
        }
    }

    isValidTournamentMeta(meta: { [k: string]: string } | null | undefined): boolean {
        const errors = checkRequiredFieldsPresent(requiredFields, meta);
        if (errors.length) {
            console.error('Unable to process tournament, missing required fields: ', errors, meta);
            return false;
        } else {
            return true;
        }
    }

    fetchRules(tournamentId: NString) {
        if (tournamentId && this.tournamentRules[tournamentId] === undefined) {
            const tournament = this.findTournament(tournamentId);
            if (tournament && tournament.rules) {
                this.setTournamentRulesValue(tournamentId, '');
                this.appState.api.loadStaticPage(tournament.rules, this.onFetchRulesResponse.bind(this, tournamentId));
            }
        }
    }

    onFetchRulesResponse(tournamentId: string, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        if (act.staticPage && act.staticPage.found && act.staticPage.content) {
            this.setTournamentRulesValue(tournamentId, act.staticPage.content);
        }
    }

    @action
    setTournamentRulesValue(id: string, html: string) {
        const tournament = this.findTournament(id);
        if (tournament) {
            const games = tournament.games.map((g: GameItem) => this.appState.games.getGameName(g), this).join(', ');
            const rules = {...this.tournamentRules};
            const place = this.appState.t('ui-place');
            const prizePoolDistribution = '<ul>' + tournament.prizePoolDistribution.map((percent: string, index: number) => '<li>' + (index + 1) + '. ' + place + ' - ' + percent + '</li>').join(' ') + '</ul>';

            let participationConditions = '';
            if (tournament.confirmedEmailRequired || tournament.confirmedPhoneRequired) {
                participationConditions += '<ul>';
                if (tournament.confirmedEmailRequired) {
                    participationConditions += '<li>' + this.appState.t('ui-condition-email') + '</li>';
                }
                if (tournament.confirmedPhoneRequired) {
                    participationConditions += '<li>' + this.appState.t('ui-condition-phone') + '</li>';
                }

                participationConditions += '</ul>';
            }

            rules[id] = html
                .replace(new RegExp(TournamentRulesArgs.SITE_NAME, 'g'), this.appState.g('%SITE_NAME%'))
                .replace(new RegExp(TournamentRulesArgs.START_DATE, 'g'), this.appState.format(new Date(tournament.startAt.toNumber()), 'yyyy.LL.dd HH:mm:ss'))
                .replace(new RegExp(TournamentRulesArgs.END_DATE, 'g'), this.appState.format(new Date(tournament.finishAt.toNumber()), 'yyyy.LL.dd HH:mm:ss'))
                .replace(new RegExp(TournamentRulesArgs.TIMEZONE, 'g'), tournament.timezone)
                .replace(new RegExp(TournamentRulesArgs.CURRENCY, 'g'), tournament.prizePoolCurrencyCode)
                .replace(new RegExp(TournamentRulesArgs.SPIN_MIN_AMOUNT, 'g'), tournament.spinsParticipationMinAmount)
                .replace(new RegExp(TournamentRulesArgs.PARTICIPATION_CONDITIONS, 'g'), participationConditions)
                .replace(new RegExp(TournamentRulesArgs.SPIN_MAX_AMOUNT, 'g'), tournament.spinsParticipationMaxAmount)
                .replace(new RegExp(TournamentRulesArgs.BET_MIN_AMOUNT, 'g'), tournament.betsParticipationMinAmount)
                .replace(new RegExp(TournamentRulesArgs.BET_MAX_AMOUNT, 'g'), tournament.betsParticipationMaxAmount)
                .replace(new RegExp(TournamentRulesArgs.GAMES_LIST, 'g'), games)
                .replace(new RegExp(TournamentRulesArgs.PRIZE_POOL_PERCENT, 'g'), tournament.prizePoolParticipationPercent)
                .replace(new RegExp(TournamentRulesArgs.PRIZE_POOL_DISTRIBUTION, 'g'), prizePoolDistribution)
                .replace(new RegExp(TournamentRulesArgs.PRIZE_POOL_DISTRIBUTION_LENGTH, 'g'), tournament.prizePoolDistribution.length.toString())
                .replace(new RegExp(TournamentRulesArgs.MIN_DEPOSITS_COUNT_TO_PARTICIPATE, 'g'), tournament.minDepositsCountToParticipate?.toString() || '')
                .replace(new RegExp(TournamentRulesArgs.MIN_AMOUNT_PER_DEPOSIT, 'g'), tournament.minDepositsAmount || '')
                .replace(new RegExp(String.fromCharCode(8232), 'g'), '');

            this.tournamentRules = rules;
        } else {
            console.error('Unable to process tournament for id: ', id);
        }
    }

    getLocaleTournamentSchedule(id: string) {
        const {startAt, finishAt} = this.tournaments!.find(t => t.id === id)!;
        const dateFormat = this.appState.appSettings.formatDatesForUS ? 'LL/dd/yyyy HH:mm:ss' : 'dd.LL.yyyy HH:mm:ss';

        const start = format(new Date(startAt.toNumber()), dateFormat);
        const end = format(new Date(finishAt.toNumber()), dateFormat);

        return `${start} — ${end}`;
    }

    processTournamentAppearance(type: NString, meta: TournamentMeta): TournamentAppearance {
        return type && tournamentsAppearance[type] ? tournamentsAppearance[type] : {
            name: meta.tournament_name || '',
            prizePoolCaption: meta.prize_pool_caption || 'ui-progressive-prize-pool',
            mainPageClass: meta.main_page_class,
            tournamentPageClass: meta.tournament_page_class,
            tournamentListClass: meta.tournament_list_class
        };
    }

    @computed
    get activeTournaments(): Tournament[] {
        return this.tournaments ? this.tournaments.filter((t: Tournament) => t.status === TournamentStatus.ACTIVE) : [];
    }

    @computed
    get upcomingTournaments(): Tournament[] {
        return this.tournaments ? this.tournaments.filter((t: Tournament) => t.status === TournamentStatus.UPCOMING) : [];
    }

    @computed
    get finishedTournaments(): Tournament[] {
        const weekly = this.tournaments ? this.tournaments.filter((t: Tournament) => t.status !== TournamentStatus.FINISHED && t.type === TournamentType.WEEKLY)
            .map((t: Tournament) => {
                return { ...t, status: TournamentStatus.FINISHED };
            }) : [];
        return this.tournaments ? this.tournaments.filter((t: Tournament) => t.status === TournamentStatus.FINISHED).concat(weekly).sort((a: Tournament, b: Tournament) => {
            return b.finishAt.toNumber() - a.finishAt.toNumber();
        }) : [];
    }

    @computed
    get tournamentPageEntry(): Tournament | null {
        return this.findTournament(this.tournamentId);
    }

    @computed
    get tournamentPageEntryRules(): string | null {
        return this.tournamentId ? this.tournamentRules[this.tournamentId] || null : null;
    }

    findTournament(id: NString): Tournament | null {
        return id && this.tournaments ? this.tournaments.find((t: Tournament) => t.id === id) || null : null;
    }

    @action.bound
    changeFilterType(type: string): void {
        this.tournamentsFilterType = type;
    }

    getTournamentClasses(tournament: Tournament) {
        const classes = [tournament.tournamentPageClass];
        if (tournament.headerLayout) {
            classes.push(`tournament-layout--header-${tournament.headerLayout}`);
        }
        return classes.join(' ');
    }
}