import * as currencies from '../utils/Currencies';
import { action, computed, observable } from 'mobx';
import { jpmonitor, wallet } from '../../api/proto';
import {
    findGame,
    findGameConsideringDeviceAndCapabilites,
    gameNameWithoutAliases
} from '../utils/GameUtils';
import {
    formatCurrencyAmount,
} from '../utils/Money';
import AppState from '../AppState';
import { ActiveBlockWinners, NString, SitePages } from '../meta';
import { GameImageSize, GameItem } from './GamesActions';
import * as React from 'react';
import { PaywayLogo } from '../payment-systems/utils';
import Long from 'long';

export const AMOUNT_OF_ITEMS_ON_MAIN_PAGE = 6;

export enum LastActionType {
    WIN,
    BILLING,
}

class LastWinEntry {
    id: string;
    title?: string;
    link?: string;
    amountString: string;
    currencyCode: string;
    game?: NString;
    icon?: string;
    gameId?: NString;
    date: Date;
    userId?: string;
    user: string;
    jackpotName?: string | null;
    jackpotStyle?: string | null;
    image?: string;
    gameObj?: GameItem | null;
    type?: LastActionType;
    billingTypeString?: string;
    nickname?: string;
    timestamp?: number;
    stamp: Long;

    constructor(id: string) {
        this.id = id;
    }
}

export class LastWinsState {
    @observable public loading: boolean = false;
    @observable public winners: LastWinEntry[] = [];
    public refName: string;

    private appState: AppState;
    private readonly amountOfItemsOnMainPage: number = AMOUNT_OF_ITEMS_ON_MAIN_PAGE;

    constructor(appState: AppState, refName: string) {
        this.refName = refName;
        this.appState = appState;
    }

    @computed
    get winnersOptimized() {
        return this.appState.page === SitePages.MAIN
            ? this.winners.slice(0, this.amountOfItemsOnMainPage)
            : this.winners;
    }
}

export class LastWinsActions {
    private readonly maxWinnerListLength: number = 20;

    private appState: AppState;
    private isFirstLoad: boolean = true;

    constructor(appState: AppState) {
        this.appState = appState;
        this.toggleWinnersBlock = this.toggleWinnersBlock.bind(this);
    }

    @computed
    get lastWinsWithBillingActions() {
        if (this.appState.lastWins.winners?.length && this.appState.billingActions.winners?.length) {
            return this.appState.lastWins.winners.concat(this.appState.billingActions.winners)
                .sort((a, b) => {
                    if (a.timestamp! < b.timestamp!) return 1;
                    if (a.timestamp! > b.timestamp!) return -1;
                    return 0;
                })
                .slice(0, AMOUNT_OF_ITEMS_ON_MAIN_PAGE);
        }
        return this.appState.lastWins.winnersOptimized;
    }

    @action
    processLastWins(lastWins: jpmonitor.ISiteFeedMessage[]) {
        const ref = this.appState.getRef(this.appState.lastWins.refName);
        if (ref || this.isFirstLoad) {
            this.isFirstLoad = false;
            this.prepareLastWins(lastWins, this.appState.lastWins);
        }
    }

    @action
    processTopWins(topWins: jpmonitor.ISiteFeedMessage[]) {
        this.prepareTopWins(topWins, this.appState.topWins);
    }

    @action
    processSiteBillingNotifications(
        withdrawals: jpmonitor.ISiteFeedMessage[],
        deposits: jpmonitor.ISiteFeedMessage[],
    ) {
        const withdrawalsActions = withdrawals
            .filter((billingAction) => {
                const provider = billingAction.providerId
                    ? parseInt(billingAction.providerId, 10)
                    : undefined;
                return (
                    provider === wallet.WithdrawProvider.CASHAPP ||
                    provider === wallet.WithdrawProvider.BTCPAYSERVER
                );
            })
            .map((billingAction) => this.createBillingActionEntry(billingAction, true));
        const depositsActions = deposits
            .filter((billingAction) => {
                const provider = billingAction.providerId
                    ? parseInt(billingAction.providerId, 10)
                    : undefined;
                return (
                    provider === wallet.WithdrawProvider.CASHAPP ||
                    provider === wallet.WithdrawProvider.BTCPAYSERVER
                );
            })
            .map((billingAction) => this.createBillingActionEntry(billingAction, false));
        const actions = [...withdrawalsActions, ...depositsActions].sort((a, b) =>
            b.stamp?.greaterThan(a.stamp ?? Long.ZERO) ? 1 : -1,
        );
        if (
            actions.length === 0 ||
            (this.appState.billingActions.winners.length > 0 &&
                actions.length > 0 &&
                this.appState.billingActions.winners[0].stamp.greaterThanOrEqual(
                    actions[0].stamp ?? Long.ZERO,
                ))
        ) {
            return;
        }
        this.appState.billingActions.winners = actions;
        window.setTimeout(() => this.animateWinner(this.appState.billingActions), 0);
    }

    @action
    createBillingActionEntry(billingAction: jpmonitor.ISiteFeedMessage, isWithdraw: boolean): LastWinEntry {
        const winId = billingAction.id!.toString();
        const currency = currencies.findCurrencyById(billingAction.currency);
        const entry = new LastWinEntry(winId);
        const provider = parseInt(billingAction.providerId!, 10);
        entry.amountString = formatCurrencyAmount(
            billingAction.amount,
            currency
        );
        entry.type = LastActionType.BILLING;
        entry.icon = '';
        if (provider === wallet.WithdrawProvider.CASHAPP) {
            entry.icon = PaywayLogo.CASHAPP;
        } else if (provider === wallet.WithdrawProvider.BTCPAYSERVER) {
            entry.icon = PaywayLogo.BTCPAYSERVER;
        }
        entry.billingTypeString = isWithdraw
            ? this.appState.t('ui-redeemed')
            : this.appState.t('ui-refill');
        entry.nickname = billingAction.userName ? `${this.appState.t('ui-tag')}: ${billingAction.userName}` : "";
        entry.timestamp = billingAction.stamp ? billingAction.stamp?.toNumber() * 1000 : 0;
        entry.stamp = billingAction.stamp ?? Long.ZERO;
        entry.userId = entry.userId?.toString() ?? undefined;
        return entry;
    }

    @action.bound
    prepareLastWins(wins: jpmonitor.ISiteFeedMessage[], winsState: LastWinsState) {
        const winsSorted = wins
            .filter(
                (_) =>
                    winsState.winners.length === 0 ||
                    winsState.winners.every((win) => _.id?.notEquals(win.id)),
            )
            .sort((a, b) =>
                b.stamp?.greaterThan(a.stamp ?? Long.ZERO)
                    ? 1
                    : b.stamp?.eq(a.stamp ?? Long.ZERO)
                    ? 0
                    : -1,
            );
        if (
            winsState.winners.length > 0 &&
            winsSorted.length > 0 &&
            winsState.winners[0].stamp.greaterThanOrEqual(winsSorted[0].stamp ?? Long.ZERO)
        ) {
            return;
        }
        const newWinsConsideringDeviceAndCapabilites =
            this.reduceWinsConsideringDeviceAndCapabilites(winsSorted, ActiveBlockWinners.LAST);
        const winsLeft =
            this.maxWinnerListLength -
            (newWinsConsideringDeviceAndCapabilites.length + winsState.winners.length);
        const otherWins = winsSorted.filter((_) =>
            newWinsConsideringDeviceAndCapabilites.every((win) => _.id?.notEquals(win.id)),
        );
        const newWins =
            winsLeft > 0
                ? this.reduceWinsDumb(otherWins, ActiveBlockWinners.LAST, winsLeft)
                      .slice(0, winsLeft)
                      .concat(newWinsConsideringDeviceAndCapabilites)
                : newWinsConsideringDeviceAndCapabilites;
        if (newWins.length) {
            newWins.forEach((win) => {
                if (winsState.winners.unshift(win) > this.maxWinnerListLength) {
                    winsState.winners.pop();
                }
            });
            window.setTimeout(() => this.animateWinner(winsState), 0);
        }
    }

    @action
    prepareTopWins(wins: jpmonitor.ISiteFeedMessage[], winsState: LastWinsState) {
        const topWins = this.reduceWinsConsideringDeviceAndCapabilites(
            wins.reverse(),
            ActiveBlockWinners.TOP
        );
        if (topWins.every(_ => winsState.winners.some(win => _.id?.toString() === win.id))) {
            return;
        }
        if (topWins.length) {
            winsState.winners = topWins;
            window.setTimeout(() => this.animateWinner(winsState), 0);
        }
    }

    reduceWinsConsideringDeviceAndCapabilites(
        wins: jpmonitor.ISiteFeedMessage[],
        type: ActiveBlockWinners
    ) {
        return wins.reduce(
            (accumulator: LastWinEntry[], winInfo: jpmonitor.ISiteFeedMessage) => {
                const winId = winInfo.id?.toString();
                if (!!winId) {
                    if (accumulator.length !== this.maxWinnerListLength) {
                        const game = findGameConsideringDeviceAndCapabilites(
                            this.appState,
                            winInfo.gameId!
                        );
                        if (game) {
                            accumulator.unshift(
                                this.createWinEntry(winId, winInfo, game, type)
                            );
                        }
                    }
                }
                return accumulator;
            },
            []
        );
    }

    reduceWinsDumb(
        wins: jpmonitor.ISiteFeedMessage[],
        type: ActiveBlockWinners,
        winsLeft: number
    ) {
        return wins.reduce(
            (accumulator: LastWinEntry[], winInfo: jpmonitor.ISiteFeedMessage) => {
                const winId = winInfo.id?.toString();
                if (!!winId) {
                    const game = findGame(this.appState, winInfo.gameId!);
                    if (game && accumulator.length < winsLeft) {
                        accumulator.unshift(
                            this.createWinEntry(winId, winInfo, game, type)
                        );
                    }
                }
                return accumulator;
            },
            []
        );
    }

    createWinEntry(
        winId: string,
        winInfo: jpmonitor.ISiteFeedMessage,
        game: GameItem,
        type: ActiveBlockWinners
    ): LastWinEntry {
        const winEntry = new LastWinEntry(winId);
        const name = this.appState.games.getGameName(game);
        const currency = currencies.findCurrencyById(winInfo.currency);
        const timestamp = winInfo.stamp ? winInfo.stamp.toNumber() * 1000 : 0;
        winEntry.amountString = formatCurrencyAmount(winInfo.amount, currency);
        winEntry.game = name ? gameNameWithoutAliases(name) : '';
        winEntry.gameId = winInfo.gameId;
        winEntry.user = winInfo.userName ?? "";
        winEntry.userId = winInfo.userId?.toString() ?? undefined;
        winEntry.icon = this.appState.games.getGameImage(
            game,
            GameImageSize.SIZE_40x40
        )!;
        winEntry.date = new Date(timestamp);
        winEntry.title = this.generateWinnerTitle(winEntry);
        winEntry.link = winEntry.gameId ? this.appState.games.url(winEntry.gameId) : undefined;
        winEntry.type = LastActionType.WIN;
        winEntry.stamp = winInfo.stamp ?? Long.ZERO;
        winEntry.timestamp = timestamp;
        return winEntry;
    }

    animateWinner(winnersState: LastWinsState): void {
        const winners = this.appState.getRef(winnersState.refName);
        const winnerAnimationDuration = 1000;
        const winnerEl = winners && winners.children[0];
        if (winnerEl) {
            winnerEl.classList.add('winner-new');
            // remove animation class after animate
            setTimeout(() => {
                winnerEl.classList.remove('winner-new');
            }, winnerAnimationDuration);
        }
    }

    generateWinnerTitle(winner: LastWinEntry): string {
        return (
            this.appState.t('ui-game') +
            ' - ' +
            winner.game +
            '\n' +
            this.appState.t('ui-player') +
            ' - ' +
            winner.user +
            '\n' +
            this.appState.t('ui-user-id') +
            ' - ' +
            winner.userId +
            '\n' +
            this.appState.t('ui-prize-won') +
            ' - ' +
            winner.amountString +
            '\n' +
            this.appState.t('ui-date') +
            ' - ' +
            this.appState.format(winner.date, 'dd LLL yyyy HH:mm:ss')
        );
    }

    @action
    toggleWinnersBlock(e: React.MouseEvent<HTMLDivElement>) {
        const activeBlockWinners = (e.currentTarget as HTMLDivElement).getAttribute(
            'data-active-winners'
        );
        const appState = this.appState;
        if (activeBlockWinners) {
            appState.activeBlockWinners = activeBlockWinners as ActiveBlockWinners;
        } else {
            console.error('Unable to change winners tab: ', activeBlockWinners);
        }
    }

    @computed
    get isJackpotWinsEnabled(): boolean {
        return this.appState.jackpots.winners.length > 0;
    }
}
