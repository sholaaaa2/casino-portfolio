import AppState from "../AppState";
import {action, observable, computed} from "mobx";
import {bonuses, messages} from "../../api/proto";
import {SitePages} from "../meta";
import React from "react";
import {NotificationItemLevel} from "../components/Notificator";
import {BonusType, BonusMeta} from "../../api/utils";

interface balanceWagerEntry {
    id?: string;
    minRefill?: number;
    maxRefill?: number;
    bonusPercentFromRefill?: number;
    bonusWager?: number;
    code?: string;
    currentBalance?: number;
    available?: boolean;
    lastRefill?: number;
    activationExpirationStamp?: number;
    bonusAmount?: number;
    balanceAfterBonusAccept?: number;
    minClosingAmount?: number;
    maxClosingAmount?: number;
    unavailabilityReasons?: UnavailabilityReasons;
    bonus_type?: BonusType;
    freeGame?: string;
    freeGameBetPerSpin?: number;
    freeGameCount?: number;
    freeGameCurrency?: number;
    bonusImageUrl?: string;
}

interface UnavailabilityReasons {
    refillIsRequired?: boolean;
    bonusAlreadyActivated?: boolean;
    timeForActivationHasExpired?: boolean;
    refillAmountIsOutOfBoundaries?: boolean;
}

export default class BalanceWagerBonusActions {
    @observable entries: balanceWagerEntry[] = [];
    //@observable active: ActiveWagerBonus | null = null;
    @observable waitingAnyActions = false;
    @observable cancelCorrection: bonuses.BonusCancelPreviewResponse.ICorrection | null = null;
    @observable confirmingActivation = false;
    @observable selectedEntryId?: string = undefined;

    activationCode: string = '';
    retryCount: number;
    isShowModal: boolean = false;
    showModalBonusNotificationId?: string | null = null;

    constructor(private appState: AppState) {
    }

    @action.bound
    processBalanceWageres(possiblebalanceWageres: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) {
        this.selectedEntryId = undefined;
        //this.entries = [];
        for (const balanceWager of possiblebalanceWageres) {
            if (balanceWager.id) {
                const res: balanceWagerEntry = {
                    id: balanceWager.id,
                    bonusImageUrl: balanceWager.meta?.[BonusMeta.BANNER_URL] ? balanceWager.meta[BonusMeta.BANNER_URL] : undefined,
                    ...balanceWager.meta,
                };
                if (!this.selectedEntryId && balanceWager.meta?.available) {
                    this.selectedEntryId = balanceWager.id;
                }

                this.entries.push(res);
            }
        }
        if (this.entries?.length) {
            this.entries = this.entries.sort((a: balanceWagerEntry, b: balanceWagerEntry) => {
                return a.bonusPercentFromRefill && b.bonusPercentFromRefill ? a.bonusPercentFromRefill - b.bonusPercentFromRefill : 0;
            })
        }
        if (!this.selectedEntryId) {
            this.selectedEntryId = this.entries[0]?.id;
        }
        if (this.entries?.length && this.isShowModal && this.appState.page !== SitePages.GAME) {
            this.showModal();
            setTimeout(() => document.getElementById(this.selectedEntryId!)?.scrollIntoView(), 0)
            this.isShowModal = false;
            if (this.showModalBonusNotificationId) {
                this.appState.bonuses.execRefuseBonus(this.showModalBonusNotificationId);
                this.showModalBonusNotificationId = null;
            }
        }
        console.log(this.entries)
    }

    @action.bound
    fetchData(shouldShowModal?: boolean) {
        this.waitingAnyActions = true;
        if (this.appState.page === SitePages.GAME) {
            shouldShowModal = false;
        }
        if (shouldShowModal) {
            this.isShowModal = true;
        }
        setTimeout(() => {
            this.appState.userBonusesStore.fetchBonuses(() => {
                this.waitingAnyActions = false;
            });
        }, 0);
    }

    @action.bound
    tryActivatePromocode() {
        if (this.retryCount < 3) {
            this.retryCount++;
            this.appState.api.activatePromoCode(this.activationCode, this.onPromocodeActivateResponse)
        } else {
            const message = this.appState.t('error-refill-promo-code-not-found');
            this.waitingAnyActions = false;
            const errorNotification = {
                level: NotificationItemLevel.ERROR,
                message: message
            };
            this.appState.notificator!.addNotification(errorNotification);
        }
    }

    @action.bound
    onPromocodeActivateResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.bonusAction?.activateCode) {
            const activationResult = act.bonusAction.activateCode.result;
            const meta = act.bonusAction.activateCode.meta;
            if (meta && meta.ok === 'false' && meta.error) {
                const message = this.appState.t(meta.error);
                this.waitingAnyActions = false;
                const errorNotification = {
                    level: NotificationItemLevel.ERROR,
                    message: message
                };
                this.appState.notificator!.addNotification(errorNotification);
            } else {
                if (activationResult) {
                    if (activationResult === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                        this.waitingAnyActions = false;
                        this.appState.modal.hideModal();
                        this.appState.profileBonuses.onPromoCodeActivation();
                    } else if (activationResult === bonuses.BonusCodeActivateResponse.Result.SERVER_ERROR) {
                        setTimeout(this.tryActivatePromocode, 1000);
                    } else if (activationResult === bonuses.BonusCodeActivateResponse.Result.CANT_BE_ACTIVATED_AT_THE_MOMENT) {
                        const message = this.appState.t('error-has-restricted-bonuses');
                        this.waitingAnyActions = false;
                        const errorNotification = {
                            level: NotificationItemLevel.ERROR,
                            message: message
                        };
                        this.appState.notificator!.addNotification(errorNotification);
                    }
                } else {
                    setTimeout(this.tryActivatePromocode, 1000);
                }
            }
        }
    }

    @action.bound
    onChangeSelectedBonus(id?: string) {
        if (id) {
            this.selectedEntryId = id;
            document.getElementById(id)?.scrollIntoView();
        }
    }

    @computed
    get selectedEntry() {
        if (this.selectedEntryId && this.entries) {
            const entry = this.entries.find(entry => entry.id === this.selectedEntryId);
            if (entry) {
                return entry;
            }
        }
        return null;
    }

    @computed
    get unaviabilityReasonForSelectedEntry() {
        if (this.selectedEntry && this.selectedEntry.unavailabilityReasons) {
            if (this.selectedEntry.unavailabilityReasons.bonusAlreadyActivated) {
                return this.appState.t('bonus-already-activated')
            }
            if (this.selectedEntry.unavailabilityReasons.refillAmountIsOutOfBoundaries) {
                return this.appState.t('refill-amount-is-out-of-boundaries')
            }
            if (this.selectedEntry.unavailabilityReasons.refillIsRequired) {
                return this.appState.t('refill-is-required')
            }
            if (this.selectedEntry.unavailabilityReasons.timeForActivationHasExpired) {
                return this.appState.t('time-for-activation-expired')
            }
        }
        return null;
    }


    @action.bound
    modalOnClickApply(e?: React.MouseEvent<HTMLElement>) {
        e?.preventDefault();
        const entryId = this.selectedEntryId;
        this.retryCount = 0;
        if (entryId) {
            if (Number(this.selectedEntry?.activationExpirationStamp)) {
                if (Number(this.selectedEntry?.activationExpirationStamp) - Date.now() > 0) {
                    this.activationCode = entryId;
                    this.waitingAnyActions = true;
                    this.tryActivatePromocode();
                } else {
                    this.fetchData();
                }
            } else {
                this.activationCode = entryId;
                this.waitingAnyActions = true;
                this.tryActivatePromocode();
            }
        }
    }

    @action.bound
    scrollListLeftStart() {
        const selectedEntryIndex = this.getEntrySelectedIndex();
        if (selectedEntryIndex !== undefined && selectedEntryIndex > 0) {
            const prevEntry = this.entries[selectedEntryIndex - 1];
            if (prevEntry?.id) {
                this.selectedEntryId = prevEntry?.id;
                document.getElementById(prevEntry.id)?.scrollIntoView();
            }
        }
    }

    getEntrySelectedIndex() {
        let entryIndex = 0;
        this.entries.forEach((entry, i) => {
            if (entry.id === this.selectedEntryId) {
                entryIndex = i;
            }
        })
        return entryIndex;
    }

    @action.bound
    scrollListRightStart() {
        const selectedEntryIndex = this.getEntrySelectedIndex();
        if (selectedEntryIndex !== undefined && selectedEntryIndex < this.entries.length) {
            const nextEntry = this.entries[selectedEntryIndex + 1];
            if (nextEntry?.id) {
                this.selectedEntryId = nextEntry?.id;
                document.getElementById(nextEntry.id)?.scrollIntoView();
            }
        }
    }

    @action.bound
    clickApplyOnProfile(code?: string) {
        if (code) {
            this.selectedEntryId = code;
            this.modalOnClickApply();
        }
    }

    @action.bound
    showIfNeededAfterRefill() {
        setTimeout(() => this.fetchData(true),2000);
    }

    @action.bound
    showModal() {
        this.appState.modal.showbalanceWagerModal();
    }

    @action.bound
    showModalAndSelectEntry(id?: string) {
        if (id) {
            this.selectedEntryId = id;
            setTimeout(() => document.getElementById(this.selectedEntryId!)?.scrollIntoView(), 0)
        }
        this.appState.modal.showbalanceWagerModal();
    }

    getWidthClass() {
        return `modal-width-${Math.ceil(this.entries.length / 4)}`;
    }

    get isPossibleWagerBonusesExists(): boolean {
        return this.entries.length !== 0;
    }

    get requireActivationConfirmation(): boolean {
        return !!this.appState.appSettings.wagerBonusRequireActivationConfirmation;
    }

    getEntryListTitle(entry?: balanceWagerEntry) {
        if (entry) {
            if (entry.bonus_type === BonusType.BALANCE_WAGER_BONUS || entry.bonus_type === BonusType.BETS_WAGER_REFILLS_BONUS) {
                return `${this.appState.t('ui-bonus')} ${entry.bonusPercentFromRefill || ''}%`;
            } else if ((entry.bonus_type === BonusType.FS_BETS_WAGER_REFILLS_BONUS || entry.bonus_type === BonusType.FS_BALANCE_WAGER_REFILLS_BONUS) && entry.freeGame && entry.freeGameCount) {
                const gameName = this.appState.games.getGameName(entry.freeGame);
                return `${gameName} | ${entry.freeGameCount}`;
            }
        }
        return this.appState.t('ui-bonus');
    }

    getGameIfExist(entry?: balanceWagerEntry) {
        return entry?.freeGame ? this.appState.games.findGameById(entry.freeGame) : undefined;
    }

    closeModalAndOpenGame = (url: string) => {
        this.appState.modal.hideModal();
        this.appState.router.push(url);
    }
};
