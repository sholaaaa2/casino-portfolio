import {action, computed, observable} from 'mobx';
import {messages, shop} from '../../api/proto';
import AppState from '../AppState';
import {GameImageSize, GameItem} from './GamesActions';
import {formatCurrencyAmount, formatCurrencyCode, getFreeSpinsToken} from '../utils/Money';
import Long from 'long';
import {timeStampToDDHHMMSS} from '../utils/Datetime';
import copyToClipboard from 'copy-to-clipboard';
import React from 'react';

const CHECK_TIMEOUT_MS = 30000;
const STORAGE_SELECTED_PRODUCTS = 'wl:freeSpinsShop:selectedProducts';
const FILTERS = ['all', 'selected'] as const;
type FilterOption = typeof FILTERS[number];
const SORT_OPTIONS = [
    'sort-wager-max',
    'sort-wager-min',
    'sort-price-max',
    'sort-price-min',
    'sort-sale-max',
    'sort-sale-min',
    'sort-freespins-max',
    'sort-freespins-min',
] as const;
type SortOption = typeof SORT_OPTIONS[number];
const VIEW_OPTIONS = ['view-full', 'view-half', 'view-one-third'] as const;
type ViewOption = typeof VIEW_OPTIONS[number];
const NUMBER_OF_DIGITS_IN_PROMOCODE = 6;

const RUB_CODE = 643;
const KZT_CODE = 398;

class FreespinsShopItem extends shop.Product {
    @observable timeLeftComputed?: number;
    @observable isSelected = false;
    @observable quantityLeft: number;

    constructor(private appState: AppState, product: shop.IProduct, isSelected: boolean) {
        super(product);
        this.isSelected = isSelected;
        this.tickTimer();
    }

    @computed
    get game(): GameItem | null {
        return this.appState.games.findGameById(this.freeSpin?.gameId);
    }

    @computed
    get gameName(): string | undefined {
        return this.game?.name ? this.game.name[this.appState.language] : undefined;
    }

    @computed
    get gameImage(): string | undefined {
        return this.appState.games.getGameImage(this.game, GameImageSize.SIZE_300x188);
    }

    @computed
    get squareImage(): string | undefined {
        return this.appState.games.getGameImage(this.game, GameImageSize.SIZE_300x300);
    }

    @computed
    get wagerString(): string | undefined {
        return this.wager ? String(this.wager.toNumber() / 100) : undefined;
    }

    @computed
    get timeLeftStringFormatted(): string | undefined {
        return timeStampToDDHHMMSS(Long.fromNumber(this.timeLeftComputed ?? 0));
    }

    @computed
    get timeLeftStringFormattedStripped(): string | undefined {
        return this.timeLeftStringFormatted?.startsWith('00:')
            ? this.timeLeftStringFormatted.substring(3)
            : this.timeLeftStringFormatted;
    }

    @computed
    get discountString(): string | undefined {
        return this.discount ? `${this.discount / 100}%` : undefined;
    }

    @computed
    get originalPriceString(): string | undefined {
        return this.originalPrice
            ? formatCurrencyAmount(this.originalPrice, this.currencyId, true, this.isIntegerPrices)
            : undefined;
    }

    @computed
    get oldPriceFormatted(): string {
        return formatCurrencyAmount(this.originalPrice, this.currencyId, false, this.isIntegerPrices);
    }

    @computed
    get priceString(): string | undefined {
        return this.price ? formatCurrencyAmount(this.price, this.currencyId, true, this.isIntegerPrices) : undefined;
    }

    @computed
    get priceFormatted(): string {
        return formatCurrencyAmount(this.price, this.currencyId, false, this.isIntegerPrices);
    }

    @computed
    get currencyString(): string | undefined {
        return formatCurrencyCode(this.currencyId);
    }

    @computed
    get betString(): string | undefined {
        return this.freeSpin?.bet
            ? formatCurrencyAmount(this.freeSpin.bet, this.currencyId, true, this.isIntegerPrices)
            : undefined;
    }

    @computed
    get freeSpinsInfo(): string {
        const fsAmount = this.freeSpin?.count ?? 0;
        return `${fsAmount} ${this.appState.t(getFreeSpinsToken(fsAmount))}`;
    }

    @computed
    get isIntegerPrices(): boolean {
        return this.appState.freespinsShopActions.isMinimalTheme && (this.currencyId.equals(RUB_CODE) || this.currencyId.equals(KZT_CODE));
    }

    @action.bound
    tickTimer(): void {
        const currentDate = new Date();
        const timeLeft = this.availableUntil.toNumber() - currentDate.getTime();
        this.timeLeftComputed = timeLeft;
    }

    @action.bound
    toggleSelected(): void {
        this.isSelected = !this.isSelected;
        const id = this.id.toString();
        let selectedProducts = this.appState.freespinsShopActions.getSelectedProducts();
        if (this.isSelected) {
            selectedProducts.push(id);
        } else {
            selectedProducts = selectedProducts.filter((_: string) => _ !== id);
        }
        window.localStorage.setItem(STORAGE_SELECTED_PRODUCTS, JSON.stringify(selectedProducts));
    }
}

interface IPromoCodeInfo {
    price?: string;
    priceFormatted?: string;
    priceDiscount?: number;
    wagerString?: number;
    wagerDiscount?: number;
    statusOk?: boolean;
    errorMessage?: string;
}

export default class FreespinsShopActions {
    private appState: AppState;
    @observable isFreespinsLoading = false;
    @observable buyRequestLoading = false;
    freespinsShopItems = observable<FreespinsShopItem>([]);
    @observable freespinItemSelected: FreespinsShopItem | null | undefined = undefined;
    @observable modalErrorMessage: string | null | undefined = undefined;
    @observable modalBuyIsSuccess: boolean = false;
    @observable promoCodeValue?: string;
    @observable promoCodeInfo?: IPromoCodeInfo;
    interval?: number = undefined;
    checkTimeout?: number = undefined;
    filters = FILTERS;
    @observable filter: FilterOption = 'all';
    sortOptions = SORT_OPTIONS;
    @observable sort?: SortOption = undefined;
    viewOptions = VIEW_OPTIONS;
    @observable view?: ViewOption = undefined;
    @observable isFilterFixed = false;
    @observable isFilterSelectShown = false;
    ref: React.RefObject<HTMLDivElement> = React.createRef();
    filterRef: React.RefObject<HTMLDivElement> = React.createRef();
    isMounted = false;

    @observable copyToClipboardTooltipId?: number;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    @computed
    get isMinimalTheme(): boolean {
        return this.appState.appSettings.freespinsShop?.theme === 'minimal';
    }

    @computed
    get isFiltersShown(): boolean {
        return this.isMinimalTheme;
    }

    @computed
    get selectedItems(): FreespinsShopItem[] {
        return this.freespinsShopItems.filter((_) => _.isSelected);
    }

    @computed
    get filteredItems(): FreespinsShopItem[] {
        let items: FreespinsShopItem[] = this.freespinsShopItems;
        if (this.filter === 'selected') {
            items = this.selectedItems;
        }
        if (this.isMinimalTheme) {
            if (this.sort === 'sort-wager-min') {
                items = items.slice().sort((a, b) => (a.wager.greaterThan(b.wager) ? 1 : -1));
            } else if (this.sort === 'sort-wager-max') {
                items = items.slice().sort((a, b) => (a.wager.lessThan(b.wager) ? 1 : -1));
            } else if (this.sort === 'sort-price-min') {
                items = items.slice().sort((a, b) => (a.price.greaterThan(b.price) ? 1 : -1));
            } else if (this.sort === 'sort-price-max') {
                items = items.slice().sort((a, b) => (a.price.lessThan(b.price) ? 1 : -1));
            } else if (this.sort === 'sort-sale-min') {
                items = items.slice().sort((a, b) => a.discount - b.discount);
            } else if (this.sort === 'sort-sale-max') {
                items = items.slice().sort((a, b) => b.discount - a.discount);
            } else if (this.sort === 'sort-freespins-min') {
                items = items
                    .slice()
                    .sort((a, b) =>
                        a.freeSpin?.count?.greaterThan(b.freeSpin?.count ?? 0) ? 1 : -1,
                    );
            } else if (this.sort === 'sort-freespins-max') {
                items = items
                    .slice()
                    .sort((a, b) => (a.freeSpin?.count?.lessThan(b.freeSpin?.count ?? 0) ? 1 : -1));
            }
        }
        return items;
    }

    @action.bound
    resetState() {
        window.removeEventListener('scroll', this.handleScroll);
        document.removeEventListener('click', this.handleClickOutside);
        window.clearInterval(this.interval);
        window.clearTimeout(this.checkTimeout);
        this.modalErrorMessage = undefined;
        this.freespinItemSelected = undefined;
        this.promoCodeInfo = undefined;
        this.promoCodeValue = undefined;
        this.isFilterFixed = false;
        this.isFilterSelectShown = false;
        this.isMounted = false;
    }

    @action.bound
    init(shopItemId?: string) {
        this.isMounted = true;
        window.addEventListener('scroll', this.handleScroll);
        document.addEventListener('click', this.handleClickOutside);
        this.interval = window.setInterval(this.computedTimer, 1000);
        this.fetchFreespinsForShop(shopItemId);
    }

    @action
    fetchFreespinsForShop(shopItemId?: string, isWithLoader = true) {
        if (isWithLoader) {
            this.isFreespinsLoading = true;
        }
        if (shopItemId === undefined) {
            window.clearTimeout(this.checkTimeout);
        }
        this.appState.api.fetchFreespinsForShop(
            (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                this.onFetchFreespinsForShop(msg, act, shopItemId);
            },
        );
    }

    @action
    onFetchFreespinsForShop(
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
        shopItemId?: string,
    ) {
        this.isFreespinsLoading = false;
        this.appState.processApi(msg, act);
        if (this.isMounted && act.shopItems?.products && act.shopItems?.products.length > 0) {
            const selectedProducts = this.getSelectedProducts();
            this.freespinsShopItems.replace(
                act.shopItems.products
                    .sort((a: shop.IProduct, b: shop.IProduct) => {
                        const aTimeLeft = a?.timeLeft?.toNumber() || 0;
                        const bTimeLeft = b?.timeLeft?.toNumber() || 0;

                        return aTimeLeft - bTimeLeft;
                    })
                    .map((product) => {
                        const isSelected = selectedProducts.includes(product?.id?.toString() ?? '');
                        return new FreespinsShopItem(this.appState, product, isSelected);
                    }),
            );
            this.checkTimeout = window.setTimeout(() => {
                this.fetchFreespinsForShop(undefined, false);
            }, CHECK_TIMEOUT_MS);
            if (shopItemId) {
                const fsItem = this.freespinsShopItems.find(
                    (item) => item.id?.toNumber() === Number(shopItemId),
                );
                if (fsItem) {
                    this.onOpenModalClick(fsItem);
                } else {
                    this.appState.router.push(this.appState.l(`/freespins-shop`));
                }
            }
        }
    }

    @action.bound
    handleScroll() {
        const {current} = this.ref;
        if (current && this.isFiltersShown) {
            if (current.getBoundingClientRect().top < 0) {
                this.isFilterFixed = true;
            } else {
                this.isFilterFixed = false;
            }
        }
    }

    @action.bound
    handleClickOutside(event: MouseEvent) {
        if (!this.filterRef.current?.contains(event.target as Node)) {
            this.isFilterSelectShown = false;
        }
    }

    @action.bound
    computedTimer() {
        const initialItemsCount = this.freespinsShopItems?.length ?? 0;
        this.freespinsShopItems?.forEach((item) => {
            item.tickTimer();
        });
        this.freespinsShopItems.replace(
            this.freespinsShopItems.filter((_) => _.timeLeftComputed === undefined || _.timeLeftComputed > 0),
        );
        window.setTimeout(() => {
            const finalItemsCount = this.freespinsShopItems?.length ?? 0;
            if (initialItemsCount !== finalItemsCount && finalItemsCount === 0 && !this.isFreespinsLoading) {
                this.fetchFreespinsForShop();
            }
        }, 5000);
    }

    @action
    buyRequest = () => {
        if (this.freespinItemSelected?.id) {
            this.buyRequestLoading = true;
            this.appState.api.buyFreespin(
                this.freespinItemSelected.id,
                this.onBuyRequest,
                this.promoCodeValue?.replace('-', '').replace('_', ''),
            );
        }
    };

    @action.bound
    onBuyRequest(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.buyRequestLoading = false;
        if (act.shopPayment) {
            if (act.shopPayment.status === shop.SiteShopPaymentResponse.PaymentResponseStatus.ok) {
                if (this.freespinItemSelected) {
                    this.freespinItemSelected.quantityLeft -= 1;
                }
                this.modalBuyIsSuccess = true;
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.not_enough_money
            ) {
                this.modalErrorMessage = this.appState.t('freespin-shop-error-not-enough-money');
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.not_spent_item_exists
            ) {
                this.modalErrorMessage = this.appState.t(
                    'freespin-shop-error-not-spent-item-exists',
                );
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.item_overdue
            ) {
                this.modalErrorMessage = this.appState.t('freespin-shop-error-item-overdue');
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.item_not_found
            ) {
                this.modalErrorMessage = this.appState.t('freespin-shop-error-item-not-found');
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.not_allowed_for_bonus_money
            ) {
                this.modalErrorMessage = this.appState.t(
                    'freespin-shop-error-not-allowed-for-bonus-money',
                );
            } else if (
                act.shopPayment.status ===
                shop.SiteShopPaymentResponse.PaymentResponseStatus.user_not_authorized
            ) {
                this.modalErrorMessage = this.appState.t('freespin-shop-error-user-not-authorized');
            }
        }
    }

    @action
    onOpenModalClick = (freespinShopItem: FreespinsShopItem) => {
        if (!this.appState.userInfo.guest && freespinShopItem.quantityLeft > 0) {
            this.freespinItemSelected = freespinShopItem;
            this.appState.router.push(
                this.appState.l(`/freespins-shop/${freespinShopItem.id?.toNumber()}`),
            );
            this.appState.modal.showFreespinBuy();
        }
    };

    @action
    closeModal = () => {
        this.freespinItemSelected = undefined;
        this.modalErrorMessage = undefined;
        this.promoCodeInfo = undefined;
        this.promoCodeValue = undefined;
        this.modalBuyIsSuccess = false;
        this.appState.router.push(this.appState.l(`/freespins-shop`));
        this.appState.modal.hideModal();
    };

    goToGamePage = () => {
        if (this.freespinItemSelected?.freeSpin?.gameId) {
            document.location.href = this.appState.l(
                `/game/${this.freespinItemSelected?.freeSpin?.gameId}`,
            );
        }
    };

    @action
    onPromoCodeValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (
            e.target.value.replace('-', '').replace('_', '').length > NUMBER_OF_DIGITS_IN_PROMOCODE
        ) {
        } else if (
            e.target.value.replace('-', '').replace('_', '').length ===
            NUMBER_OF_DIGITS_IN_PROMOCODE
        ) {
            this.promoCodeValue = e.target.value;
            if (this.freespinItemSelected?.id)
                this.appState.api.freeSpinPromocodeRequest(
                    this.freespinItemSelected.id as Long,
                    this.promoCodeValue.replace('-', '').replace('_', '') as string,
                    this.onPromoCodeResponse.bind(this),
                );
        } else {
            this.promoCodeValue = e.target.value;
        }
    };

    @action
    onPromoCodeResponse = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        if (act.shopCoupon) {
            this.promoCodeInfo = {
                statusOk: false,
            };
            if (
                act.shopCoupon.status ===
                shop.SiteShopCouponDiscountResponse.CouponApplyingStatus.ok
            ) {
                this.promoCodeInfo = {
                    price:
                        act.shopCoupon.price && this.freespinItemSelected?.currencyId
                            ? formatCurrencyAmount(
                                  act.shopCoupon.price,
                                  this.freespinItemSelected.currencyId,
                                  true,
                              )
                            : undefined,
                    priceFormatted: formatCurrencyAmount(
                        act.shopCoupon.price,
                        this.freespinItemSelected?.currencyId ?? Long.ZERO,
                    ),
                    priceDiscount: act.shopCoupon.priceDiscount
                        ? act.shopCoupon.priceDiscount / 100
                        : undefined,
                    wagerString: act.shopCoupon.wager
                        ? act.shopCoupon.wager.toNumber() / 100
                        : undefined,
                    wagerDiscount: act.shopCoupon.wagerDiscount
                        ? act.shopCoupon.wagerDiscount / 100
                        : undefined,
                    statusOk: true,
                };
            } else if (
                act.shopCoupon.status ===
                shop.SiteShopCouponDiscountResponse.CouponApplyingStatus.coupon_not_found
            ) {
                this.promoCodeInfo.errorMessage = this.appState.t(
                    'freespin-shop-promocode-error-coupon-not-found',
                );
            } else if (
                act.shopCoupon.status ===
                shop.SiteShopCouponDiscountResponse.CouponApplyingStatus.coupon_overdue
            ) {
                this.promoCodeInfo.errorMessage = this.appState.t(
                    'freespin-shop-promocode-error-coupon-overdue',
                );
            } else if (
                act.shopCoupon.status ===
                shop.SiteShopCouponDiscountResponse.CouponApplyingStatus.item_not_found
            ) {
                this.promoCodeInfo.errorMessage = this.appState.t(
                    'freespin-shop-promocode-error-item-not-found',
                );
            }
        }
    };

    @action
    onShareClick = (id?: Long) => {
        if (id) {
            const url = `${window.location?.origin!}/freespins-shop/${id}`;
            if (navigator.share) {
                navigator
                    .share({
                        title: window.location?.host,
                        text: this.appState.t('share-freespin-text'),
                        url: url,
                    })
                    .then(() => console.log('Successful share'))
                    .catch((error) => console.log('Error sharing', error));
            } else {
                copyToClipboard(url);
                this.copyToClipboardTooltipId = id.toNumber();
                setTimeout(() => {
                    if (this.copyToClipboardTooltipId === id.toNumber()) {
                        this.copyToClipboardTooltipId = undefined;
                    }
                }, 1500);
            }
        }
    };

    @action.bound
    updateFilter(value: FilterOption) {
        this.filter = value;
    }

    @action.bound
    updateSort(value: SortOption) {
        this.sort = value;
    }

    @action.bound
    updateViewOption(value: ViewOption) {
        if (this.view === value) {
            this.view = undefined;
        } else {
            this.view = value;
        }
    }

    @action.bound
    getSelectedProducts(): string[] {
        const selectedProductsJSON = window.localStorage.getItem(STORAGE_SELECTED_PRODUCTS) ?? '[]';
        return JSON.parse(selectedProductsJSON);
    }

    @action.bound
    toggleFilterSelect() {
        this.isFilterSelectShown = !this.isFilterSelectShown;
    }
}
