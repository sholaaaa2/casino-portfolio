import AppState from '../AppState';
import {action, observable} from 'mobx';
import {AnimationModes, NotificationAnimationClass} from '../meta';
import {runJackpotConfettiForNotification} from '../../utils/effects';

export interface JackpotNotification {
    id: string;
    caption?: string;

    infoTitle?: string;
    infoCaption?: string;
    infoAmount?: string;

    showGameInfo?: boolean;
    gameInfoHref?: string;
    gameInfoTitle?: string;
    gameInfoCaption?: string;
    gameInfoImg?: string;

    animationClass?: NotificationAnimationClass;
}

export class JackpotNotifications {
    appState: AppState;

    @observable notifications: JackpotNotification[] = [];
    @observable activeNotification: JackpotNotification | null = null;

    constructor(appState: AppState) {
        this.appState = appState;

        this.hideActiveNotification = this.hideActiveNotification.bind(this);
        this.tryToShowNextNotification = this.tryToShowNextNotification.bind(this);
    }

    @action
    addNotification(notification: JackpotNotification) {
        if (!this.isNotificationExists(notification.id)) {
            this.notifications.push(notification);
            this.tryToShowNextNotification();
        }
    }

    @action
    removeNotification() {
        const id = this.activeNotification!.id;
        const index = this.notifications.findIndex((n: JackpotNotification) => n.id === id);
        if (index !== -1) {
            const notification = this.notifications[index];
            const notifications = this.notifications.slice();
            notifications.splice(index, 1);
            this.notifications = notifications;

            if (this.activeNotification === notification) {
                this.activeNotification = null;
            }

            setTimeout(this.tryToShowNextNotification, 250);
        }
    }

    @action
    tryToShowNextNotification() {
        if (this.notifications.length) {
            const appSettings = this.appState.appSettings;
            const animation = appSettings.animations && appSettings.animations.someoneWinJP;
            if (!this.activeNotification || this.activeNotification.id !== this.notifications[0].id) {
                if (animation === AnimationModes.CONFETTI) {
                    runJackpotConfettiForNotification();
                }
            }
            this.activeNotification = this.notifications[0];
            this.activeNotification.animationClass = NotificationAnimationClass.ACTIVE;
            // add timeout, to calc scale after append card html
            setTimeout(this.appState.ui.scale.calcNotificationsScale, 0);
        }
    }

    isNotificationExists(id: string) {
        return !!this.findNotification(id);
    }

    findNotification(id: string) {
        return this.notifications.find((n: JackpotNotification) => n.id === id) || null;
    }

    hideActiveNotification(): string | null {
        const notificationEl = this.appState.getRef('card-popup-jp');
        if (notificationEl) {
            notificationEl.classList.add(NotificationAnimationClass.HIDE);

            const onTimeout = this.removeNotification.bind(this);
            setTimeout(onTimeout, 500);
        }
        return null;
    }
}