import React from 'react';
import {action} from 'mobx';
import {ChangeEvent, KeyboardEvent, SyntheticEvent} from 'react';
import {debounce} from '../utils/BrowserUtils';
import AppState from '../AppState';

export default class SearchActions {
    desktopSearchInputRef: React.RefObject<HTMLInputElement> = React.createRef();

    constructor(private appState: AppState) {
        this.onChange = debounce(this.onChange, 300);
    }

    @action.bound
    proceedCtrlF(event: Event) {
        const { current } = this.desktopSearchInputRef;
        if (current) {
            current.focus();
            event.preventDefault();
        }
    }

    @action
    show = (event: SyntheticEvent<HTMLElement>) => {
        event.preventDefault();
        this.appState.searchOpened = true;
    }

    @action
    close = (event?: SyntheticEvent<HTMLElement>) => {
        if (event) {
            event.preventDefault();
        }

        this.clearSearchInputs();
        this.appState.searchValue = '';
        this.appState.searchOpened = false;
    }

    @action
    onChange = (event: ChangeEvent<HTMLInputElement>) => {
        const searchInput = document.getElementById('search-games-input') as HTMLInputElement;
        const searchInputXs = document.getElementById('search-games-input-xs') as HTMLInputElement;
        let searchValue = '';

        // update 2 search-input elements in DOM
        if (document.activeElement === searchInput) {
            searchValue = searchInput.value;
            // update search-input for desktop
            if (searchInputXs) {
                searchInputXs.value = searchValue;
            }
        } else if (document.activeElement === searchInputXs) {
            searchValue = searchInputXs.value;
            // update search-input for mobile
            if (searchInput) {
                searchInput.value = searchValue;
            }
        }

        // update searchValue in AppState
        this.appState.searchValue = searchValue;
    }

    @action
    onKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Escape') {
            this.clearSearchInputs();
            this.appState.searchValue = '';
            this.appState.searchOpened = false;
        }
    }

    clearSearchInputs = () => {
        const searchInput = document.getElementById('search-games-input') as HTMLInputElement;
        const searchInputXs = document.getElementById('search-games-input-xs') as HTMLInputElement;
        if (searchInput) {
            searchInput.value = '';
        }
        if (searchInputXs) {
            searchInputXs.value = '';
        }
    }
}