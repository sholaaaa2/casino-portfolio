import TextSlider from "./TextSlider";
import AppState from "../../AppState";
import { action, computed } from "mobx";

const CONTAINER_ID = "final-bonuses-time-intervals";

export default class TimeIntervalsSlider extends TextSlider {
    constructor(private appState: AppState) {
        super(CONTAINER_ID);
    }

    // region ---- slider actions
    @action
    afterChange = (index: number) => {
        this.appState.finalBonuses.setSelectedDayInterval(
            this.appState.finalBonuses.timeIntervalsForDay[index]
        );
        this.activeIndex = index;
    };

    // ---- endregion

    // region ---- pagination arrows state
    @computed
    get isDisabledNextIcon() {
        return (
            this.activeIndex ===
            this.appState.finalBonuses.timeIntervalsForDay.length - 1
        );
    }

    // ---- endregion

    @action
    updateAndGetActiveSlideIndex = () => {
        const selectedDayInterval = this.appState.finalBonuses
            .selectedDayInterval || { from: 0, to: 0 };
        this.activeIndex = this.appState.finalBonuses.timeIntervalsForDay.findIndex(
            (interval) =>
                interval.from === selectedDayInterval.from &&
                interval.to === selectedDayInterval.to
        );
        return this.activeIndex;
    };
}
