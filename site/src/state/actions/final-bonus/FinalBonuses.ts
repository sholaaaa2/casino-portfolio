import Long from "long";
import { bonuses } from "../../../api/proto";
import NotificationsSlider from "./NotificationsSlider";
import AppState from "../../AppState";
import DaysSlider from "./DaysSlider";
import { action, observable, computed } from "mobx";
import { Modal } from "../../meta";
import TimeIntervalsSlider from "./TimeIntervalsSlider";
import { findCurrencyById } from "../../utils/Currencies";
import { formatCurrencyAmount } from "../../utils/Money";

const FINAL_BONUS_TYPE = "updated_final_bonus";

enum BonusAmountModes {
    VISIBLE = "VISIBLE",
    HIDDEN = "HIDDEN",
}

enum SubTypes {
    LOWBALANCE = "lowbalance_cashback",
}

interface Interval {
    from: number;
    to: number;
    title: string;
}

interface FinalBonusRules {
    issueMode: "time-based" | "always";
    title: string;
    description: string;
    minBalance: number;
    period: number;
    deposits?: { [key: string]: Bonus };
    timeRanges?: { [key: string]: TimedBonus };
    timeZone: number;
    timeZoneName?: string;
}

interface TimedBonus extends NotLocalizedBonus {
    fromHour: number;
    toHour: number;
    days: number[];
}

type TextOrLangMap = string | { [lang: string]: string };
interface Bonus {
    fromDeposit: number;
    toDeposit: number;
    blockTitle: TextOrLangMap;
    amount: {
        percent: number;
        fixed: number;
    };
    descTitle: TextOrLangMap;
    title: TextOrLangMap;
    description: TextOrLangMap;
    wager: number;
    subtitle?: string;
    fromDepositFormatted: string;
    toDepositFormatted: string;
}

interface NotLocalizedBonus extends Bonus {
    blockTitle: string;
    descTitle: string;
    title: string;
    description: string;
}

export class FinalBonuses {
    notificationsSlider: NotificationsSlider = new NotificationsSlider(
        this.appState
    );
    daysSlider: DaysSlider = new DaysSlider(this.appState);
    timeIntervalsSlider: TimeIntervalsSlider = new TimeIntervalsSlider(
        this.appState
    );

    lowbalanceCashback: boolean;
    timeBased: boolean;
    timeZone?: number;
    timeZoneName?: string;
    timeInfo?: {
        day: number;
        hour: number;
    };

    @observable selectedDayIndex: number;
    @observable selectedDayInterval: Interval | null = null;
    @observable bonuses: Bonus[] = [];
    @observable timedBonuses: TimedBonus[] = [];
    @observable days: number[] = [];
    @observable bonusAmount?: string;
    @observable bonusAmountTitle?: string;
    shouldShowModal = false;

    constructor(private appState: AppState) {}

    @computed
    get finalBonus(): Bonus | undefined {
        return this.bonuses.length > 0 ? this.bonuses[0] : undefined;
    }

    @action.bound
    fetchData(shouldShowModal?: boolean) {
        this.shouldShowModal = !!shouldShowModal;
        this.appState.userBonusesStore.fetchBonuses(
            this.processData.bind(this)
        );
    }

    @action.bound
    processData(
        codes: bonuses.IBonusCode[],
        possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[]
    ) {
        const finalBonus = possibleBonuses.find(
            (bonus) =>
                bonus.id &&
                bonus.meta &&
                bonus.meta.bonus_type === FINAL_BONUS_TYPE
        );
        if (finalBonus?.meta) {
            this.processFinalBonus(
                finalBonus.meta,
                Long.fromString(finalBonus.meta.currency)
            );
        } else if (this.shouldShowModal) {
            this.showDialog();
        }
    }

    @action
    setBonuses = (bonuses: Bonus[]) => {
        this.bonuses = bonuses;
        if (this.bonusAmount && (bonuses.length > 1 || this.appState.ref.isEnabledInBonuses)) {
            this.setBonusAmountTitle();
        }
    };

    @action
    setBonusAmountTitle() {
        this.bonusAmountTitle = `${this.appState.t(
            "bonus-final-yours"
        )} <strong>${this.bonusAmount}</strong>`;
    }

    @action
    setSelectedDayInterval(value: Interval | null) {
        this.selectedDayInterval = value;
        this.hideAllCardHelp();
        this.notificationsSlider.slideToInitialIndex();
    }

    @action
    setTimedBonuses = (bonuses: TimedBonus[]) => {
        this.timedBonuses = bonuses;
        if (this.bonusAmount && bonuses.length > 0) {
            this.setBonusAmountTitle();
        }
    };

    @action
    setSelectedDayIndex = (value: number) => {
        this.selectedDayIndex = value;
        this.hideAllCardHelp();
        this.selectCurrentDayInterval();
    };

    @action
    setDays = (days: number[]) => {
        const currentDay = this.timeInfo
            ? this.timeInfo.day
            : this.getServerDate().getDay();
        this.days = days;
        this.selectBonusDay(currentDay);
    };

    get bonusesByDay() {
        return this.timedBonuses.filter(
            (bonus) =>
                bonus.days.indexOf(this.days[this.selectedDayIndex]) !== -1
        );
    }

    get bonusesByTime() {
        return this.bonusesByDay.filter(
            (bonus) =>
                bonus.toHour === this.selectedDayInterval!.to &&
                bonus.fromHour === this.selectedDayInterval!.from
        );
    }

    get timeIntervalsForDay(): Interval[] {
        const memIntervals: { [key: string]: boolean } = {};
        return this.bonusesByDay.reduce((intevals, bonus) => {
            if (!memIntervals[bonus.fromHour]) {
                memIntervals[bonus.fromHour] = true;
                intevals.push({
                    from: bonus.fromHour,
                    to: bonus.toHour,
                    title: bonus.blockTitle,
                });
            }
            return intevals;
        }, new Array<Interval>());
    }

    showDialog = () => {
        if (this.timeBased) {
            if (this.bonuses.length > 0 || this.timedBonuses.length > 0) {
                const currentDay = this.timeInfo
                    ? this.timeInfo.day
                    : this.getServerDate().getDay();
                this.selectCurrentDayInterval();
                this.selectBonusDay(currentDay);
                this.appState.modal.show(Modal.FINAL_BONUSES);
            }
        } else if (this.bonuses.length > 1) {
            this.appState.modal.show(Modal.FINAL_BONUSES);
        } else if (this.bonuses.length) {
            if (this.appState.ref.isEnabledInBonuses) {
                this.appState.modal.show(Modal.FINAL_BONUSES);
            } else {
                this.appState.modal.show(Modal.FINAL_BONUS);
            }
        } else if (this.appState.ref.isEnabledInBonuses) {
            if (this.appState.appSettings.bonusButtonInvite) {
                this.appState.modal.show(Modal.FINAL_BONUSES);
            } else {
                this.appState.ref.showRefInviteForm();
            }
        }
    };

    @action
    closeDialog = () => {
        this.appState.modal.hideModal();
        this.appState.userBonusesStore.fetchBonuses();
    };

    @action
    selectBonusDay(day: number) {
        if (this.days.indexOf(day) !== -1) {
            this.setSelectedDayIndex(this.days.indexOf(day));
        } else {
            this.setSelectedDayIndex(0);
        }
        this.selectCurrentDayInterval();
    }

    @action
    processFinalBonus(meta: { [k: string]: string }, currencyId: Long) {
        const rules: FinalBonusRules = JSON.parse(meta.rules);
        if (meta.timeInfo) {
            this.timeInfo = JSON.parse(meta.timeInfo);
        }

        if (
            meta.bonusAmountMode &&
            meta.bonusAmountMode === BonusAmountModes.VISIBLE &&
            meta.bonusAmount &&
            meta.bonusAmount !== "0"
        ) {
            this.bonusAmount = formatCurrencyAmount(
                Long.fromString(meta.bonusAmount),
                this.appState.userInfo.currency!
            );
        } else {
            this.bonusAmount = undefined;
        }

        this.lowbalanceCashback = meta?.bonus_subtype === SubTypes.LOWBALANCE;
        if (rules.issueMode === "time-based" && rules.timeRanges) {
            const finalBonuses: TimedBonus[] = [];
            const timeRanges = Object.keys(rules.timeRanges);
            const days = timeRanges
                .reduce((bonusesDays: number[], timeRangeKey) => {
                    const bonus = rules.timeRanges![timeRangeKey];
                    this.processBonus(bonus, currencyId, true);
                    if (bonus.days && bonus.days.length > 0) {
                        bonus.days = Object.keys(bonus.days).map(
                            (key) => bonus.days[key]
                        );
                    } else {
                        bonus.days = [0, 1, 2, 3, 4, 5, 6];
                    }
                    finalBonuses.push(bonus);
                    return bonusesDays.concat(bonus.days);
                }, [])
                .sort();
            this.setTimedBonuses(finalBonuses);
            this.setDays(Array.from(new Set(days)));
            this.timeBased = true;
            this.timeZone = rules.timeZone;
            if (rules.timeZoneName) {
                this.timeZoneName = rules.timeZoneName.replace(/_/g, " ");
            }
        } else if (rules.deposits) {
            const deposits = Object.keys(rules.deposits);
            const finalBonuses = deposits.map((key) => {
                const bonus = rules.deposits![key];
                return this.processBonus(
                    bonus,
                    currencyId,
                    deposits.length > 1
                );
            });
            this.setBonuses(finalBonuses);
            this.timeBased = false;
        }
        if (!this.appState.userInfo.guest && this.shouldShowModal) {
            this.shouldShowModal = false;
            this.showDialog();
        }
    }

    @computed
    get hasTimeOrFinalBonuses(): boolean {
        return this.timedBonuses.length > 0 || this.bonuses.length > 0;
    }

    @computed
    get showBonusInfoButton(): boolean {
        return this.hasTimeOrFinalBonuses || this.appState.ref.isEnabledInBonuses;
    }

    processBonus(
        bonus: Bonus,
        currencyId: Long,
        isMultipleBonuses: boolean
    ): Bonus {
        const currency = findCurrencyById(currencyId);
        const { amount } = bonus;

        bonus.blockTitle = this.checkAndTranslateTextField(bonus.blockTitle);
        bonus.descTitle = this.checkAndTranslateTextField(bonus.descTitle);
        bonus.title = this.checkAndTranslateTextField(bonus.title);
        bonus.description = this.checkAndTranslateTextField(bonus.description);

        bonus.toDepositFormatted = bonus.toDeposit
            ? formatCurrencyAmount(Long.fromNumber(bonus.toDeposit), currency)
            : "";
        bonus.fromDepositFormatted = bonus.fromDeposit
            ? formatCurrencyAmount(Long.fromNumber(bonus.fromDeposit), currency)
            : "0";
        if (this.bonusAmount !== undefined && isMultipleBonuses === false && !this.appState.ref.isEnabledInBonuses) {
            bonus.title = `${bonus.title} <span class="final-bonuses__bonus-amount">${this.bonusAmount}</span>`;
        } else if (amount.fixed) {
            bonus.title = `${bonus.title} ${formatCurrencyAmount(
                Long.fromNumber(amount.fixed),
                currency
            )}`;
        } else if (amount.percent) {
            bonus.title = `${bonus.title} ${amount.percent}%`;
        }
        return bonus as NotLocalizedBonus;
    }

    toggleCardHelp = (e: any) => {
        const currentTarget = e.currentTarget;
        if (e.target.classList.contains("toggle-help")) {
            if (currentTarget.classList.contains("show-help")) {
                currentTarget.classList.remove("show-help");
                setTimeout(() => {
                    // hack for safari
                    currentTarget.classList.remove("hide-button");
                }, 500);
            } else {
                currentTarget.classList.add("show-help", "hide-button");
            }
        }
    };

    hideAllCardHelp() {
        document.querySelectorAll(".show-help").forEach((e) => {
            e.setAttribute("style", "transition: 0.001s");
            e.classList.remove("show-help");
            setTimeout(() => {
                e.removeAttribute("style");
            }, 0);
        });
    }

    @action
    selectDayInterval = (interval: Interval) => {
        return () => {
            this.setSelectedDayInterval(interval);
        };
    };

    isSelectedDayInterval = (from: number, to: number) => {
        return (
            this.selectedDayInterval &&
            this.selectedDayInterval.from === from &&
            this.selectedDayInterval.to === to
        );
    };

    @action
    selectCurrentDayInterval = () => {
        let currentHour: number = 0;
        if (this.timeInfo) {
            currentHour = this.timeInfo.hour;
        } else {
            const date = this.getServerDate();
            currentHour = date.getHours() + date.getMinutes() / 100;
        }
        this.selectedDayInterval =
            this.timeIntervalsForDay.find((interval) => {
                return (
                    interval.from <= currentHour && interval.to > currentHour
                );
            }) || this.timeIntervalsForDay[0];
    };

    formatTimeIntervals = ({ from, to, title }: Interval) => {
        if (title) {
            return title;
        } else {
            const formattedIntervalFrom = this.formatInterval(from);
            const formattedIntervalTo = this.formatInterval(to);
            return `${formattedIntervalFrom} - ${formattedIntervalTo}`;
        }
    };

    formatInterval = (interval: number): string => {
        const time = new Date();
        time.setHours(interval);
        time.setMinutes(0);
        return time.toLocaleString("en-US", {
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        });
    };

    checkAndTranslateTextField = (field: string | TextOrLangMap | undefined): string => {
        if (typeof field === "string") return field;
        return field ? (field[this.appState.language] || field["default"] || "") : "";
    };

    @computed
    get timeZoneDescription(): string {
        if (this.timeZoneName) {
            return this.appState.t("bonus-final-timezonename-description", {
                timezone: this.timeZoneName,
            });
        } else if (this.timeZone) {
            return this.appState.t("ui-bonus-final-timezone-description", {
                time: this.formatTimeZone(this.timeZone),
            });
        } else {
            return "";
        }
    }

    formatTimeZone = (timeZone: number): string => {
        const formattedTimeString = timeZone / 60 + ":00";
        return `: ${
            timeZone >= 0 ? `+${formattedTimeString}` : `${formattedTimeString}`
        }`;
    };

    getServerDate() {
        const userDate = new Date();
        const utcTimeInMs =
            userDate.getTime() + userDate.getTimezoneOffset() * 60000;
        const offsetInMinutes = this.timeZone ? this.timeZone : 0;
        return new Date(utcTimeInMs + 60000 * offsetInMinutes);
    }

    get openModalOnLogin(): boolean {
        return !!this.appState.appSettings.finalBonusOpenModalOnLogin;
    }
}
