import AppState from "../../AppState";
import { action, observable, observe } from "mobx";
import Slider from "react-slick";
import { isDef } from "../../../api/utils";

const CARD_WIDTH = 400;
const CARD_OFFSET = 12;
const SUMMARY_CARD_WIDTH = CARD_WIDTH + CARD_OFFSET * 2;
const CONTAINER_OFFSET = 60;

export default class NotificationsSlider {
    @observable activeIndex: number;
    @observable reInitSlider: boolean = false;

    slider?: Slider;
    responsiveSettings = [
        {
            breakpoint: SUMMARY_CARD_WIDTH * 6 + CONTAINER_OFFSET * 2,
            settings: {
                slidesToShow: 5,
            },
        },
        {
            breakpoint: SUMMARY_CARD_WIDTH * 5 + CONTAINER_OFFSET * 2,
            settings: {
                slidesToShow: 5,
            },
        },
        {
            breakpoint: SUMMARY_CARD_WIDTH * 4 + CONTAINER_OFFSET * 2,
            settings: {
                slidesToShow: 4,
            },
        },
        {
            breakpoint: SUMMARY_CARD_WIDTH * 3 + CONTAINER_OFFSET * 2 + 100,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: SUMMARY_CARD_WIDTH * 2 + CONTAINER_OFFSET * 2 - 200,
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: SUMMARY_CARD_WIDTH + CONTAINER_OFFSET * 2 + 100,
            settings: {
                slidesToShow: 1,
            },
        },
    ];

    constructor(private appState: AppState) {
        observe(this.appState, "viewport", this.checkActiveIndexPagination);
    }

    checkActiveIndexPagination = () => {
        const pagination = document.getElementsByClassName(
            "js-slider-pagination"
        )[0] as HTMLElement;
        if (
            this.slider &&
            pagination &&
            this.activeIndex >= pagination.childElementCount
        ) {
            this.slider.slickGoTo(pagination.childElementCount - 1, true);
        }
    };

    @action
    doReInitSlider = () => {
        // hack for reInit slider with zero slideIndex, because slickGoTo not working correct (https://bugs.dvmntr.com/issue/WT-1395)
        if (isDef(this.activeIndex) && this.activeIndex !== 0) {
            this.activeIndex = 0;
            this.reInitSlider = true;
            setTimeout(() => {
                this.reInitSlider = false;
            }, 0);
        }
    };

    @action
    slideToInitialIndex() {
        if (this.slider) {
            this.activeIndex = 0;
            this.slider.slickGoTo(0, true);
        }
    }

    // region ---- slider actions

    sliderRef = (slider: Slider) => {
        this.slider = slider;
    };

    @action
    afterChange = (index: number) => {
        this.activeIndex = index;
    };

    // ---- endregion
}
