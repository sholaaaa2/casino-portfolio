import { computed } from "mobx";
import AppState from "../../AppState";
import TextSlider from "./TextSlider";

interface Day {
    index: number;
    title: string;
}

const CONTAINER_ID = "final-bonuses-days-list";

export default class DaysSlider extends TextSlider {
    constructor(private appState: AppState) {
        super(CONTAINER_ID);
    }

    getDayNameByIndex(index: number): string {
        switch (index) {
            case 0:
                return "ui-sunday";
            case 1:
                return "ui-monday";
            case 2:
                return "ui-tuesday";
            case 3:
                return "ui-wednesday";
            case 4:
                return "ui-thursday";
            case 5:
                return "ui-friday";
            case 6:
                return "ui-saturday";
            default:
                return "";
        }
    }

    afterChange = (index: number) => {
        this.appState.finalBonuses.setSelectedDayIndex(index);
        this.appState.finalBonuses.timeIntervalsSlider.updateAndGetActiveSlideIndex();
    };

    @computed
    get isDisabledNextIcon() {
        const { finalBonuses } = this.appState;
        const { days } = finalBonuses;
        return finalBonuses.selectedDayIndex === days.length - 1;
    }

    @computed
    get isDisabledPrevIcon() {
        return this.appState.finalBonuses.selectedDayIndex === 0;
    }

    get daysList() {
        let result: Day[] = [];
        for (let i = 0; i < 7; i++) {
            result.push({
                index: i,
                title: this.appState.t(this.getDayNameByIndex(i)),
            });
        }
        return result;
    }

    get availableDaysList() {
        let result: Day[] = [];
        this.appState.finalBonuses.days.forEach((day) => {
            result.push(this.daysList[day]);
        });
        return result;
    }
}
