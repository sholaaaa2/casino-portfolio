import { action, computed, observable } from "mobx";

export enum Direction {
    LEFT = "left",
    RIGHT = "right",
}

export default class TextSlider {
    containerId: string;

    @observable activeIndex: number = 0;

    constructor(containerId: string) {
        this.containerId = containerId;
    }

    // region ---- slider actions

    @action
    afterChange = (index: number) => {
        this.activeIndex = index;
    };

    beforeChange = (oldIndex: number, newIndex: number) => {
        const direction =
            newIndex > oldIndex ? Direction.RIGHT : Direction.LEFT;
        if (newIndex !== oldIndex) {
            this.animateArrowIcon(direction);
        }
    };

    animateArrowIcon = (direction: string) => {
        const containerEl = document.getElementById(this.containerId);
        if (containerEl) {
            containerEl.classList.add(`animate-pagination__arrow_${direction}`);
            setTimeout(() => {
                if (containerEl) {
                    containerEl.classList.remove(
                        `animate-pagination__arrow_${direction}`
                    );
                }
            }, 200);
        }
    };

    // ---- endregion

    // region ---- pagination arrows state
    @computed
    get isDisabledPrevIcon() {
        return this.activeIndex === 0;
    }

    // ---- endregion
}
