import * as currencies from '../utils/Currencies';
import AppState from '../AppState';
import {action, observable} from 'mobx';
import {messages, users} from '../../api/proto';
import {Size} from '../utils/BrowserUtils';
import {SyntheticEvent} from 'react';
import {formatCurrencyAmount, NASymbol} from '../utils/Money';
import copyToClipboard from "copy-to-clipboard";

export interface ReferralResource {
    id: string;
    label?: string;
    title?: { [key: string]: string };
    titleText?: string;
    img?: { [key: string]: string };
    imgUrl?: string;
    size?: Size;
}

export class AffiliateProgramActions {
    appState: AppState;

    @observable isRefCodeLoading: boolean = false;
    @observable refCodeError: string | null = null;

    @observable isRefInfoLoading: boolean = false;
    @observable refInfo: users.IGetRefInfoResponse | null = null;
    @observable refInfoError: string | null = null;

    promoBanners: ReferralResource[];

    @observable linkCopied = '';
    @observable linksShowingBanner = [''];

    constructor(state: AppState) {
        this.appState = state;
        this.isCopied = this.isCopied.bind(this);
        this.loadRefInfo = this.loadRefInfo.bind(this);
        this.createRefCode = this.createRefCode.bind(this);
    }

    @action
    tryLoadPromoLinks() {
        this.promoBanners = this.appState.appSettings.promoMaterials.promoBanners;
        this.processPromoBanners();
    }

    @action
    processPromoBanners() {
        const lang = this.appState.language;
        const defaultLang = this.appState.siteConfig.defaultLanguage;
        const siteDomain = this.appState.site.siteDomain;
        const {protocol} = window.location;

        this.promoBanners.forEach((item: ReferralResource) => {
            // img by language
            const imgUrl = item.img ? item.img[lang] || (defaultLang && item.img[defaultLang]) || item.img['en'] : null;
            item.imgUrl = imgUrl ? `${protocol}//${siteDomain}` + imgUrl : '';

            // title by language
            const titleText = item.title ? item.title[lang] || (defaultLang && item.title[defaultLang]) || item.title['en'] : null;
            item.titleText = titleText || '';

            // localized label
            const sizeString = this.getImgSizeString(item.size!);
            item.label = this.appState.t('ui-promo-banner', {size: sizeString});
        });
    }

    @action.bound
    handleCopy(value: string, id: string) {
        copyToClipboard(value);

        this.linkCopied = id;

        setTimeout(() => {
            this.linkCopied = '';
        }, 1000);
    }

    getReferralLinkContent() {
        const link = this.createReferralLink();

        return {
            link,
            handleCopy: () => this.handleCopy(link, 'referral'),
        };
    }

    isCopied(id: string) {
        return this.linkCopied.includes(id);
    }

    isImageShown(id: string) {
        return this.linksShowingBanner.some(t => t === id);
    }

    getPromoLinkContent(resource: ReferralResource) {
        const markup = this.createReferralBannerMarkup(resource);

        return {
            markup,
            handleToggleImage: () => this.toggleImage(resource.id),
            handleCopy: () => this.handleCopy(markup, resource.id)
        };
    }

    @action.bound
    toggleImage(id: string) {
        const current = this.linksShowingBanner;
        const isShown = current.some(t => t === id);

        this.linksShowingBanner = isShown
            ? current.filter(n => n !== id)
            : current.concat(id);
    }

    createReferralLink = (append: string = '') => {
        const {protocol} = window.location;

        const {site: {siteDomain}, rawUserInfo} = this.appState;

        return `${protocol}//${siteDomain}/?ref=${rawUserInfo!.refCode}${append}`;
    }

    createReferralBannerMarkup({titleText, imgUrl, size, id}: ReferralResource) {
        const linkElement = document.createElement('a');

        linkElement.href = this.createReferralLink(`&p1=${id}`);

        if (titleText) {
            linkElement.title = titleText;
        }

        const imgElement = document.createElement('img');

        if (imgUrl) {
            imgElement.src = imgUrl;
        }

        if (size) {
            imgElement.width = size.width;
            imgElement.height = size.height;
        }

        linkElement.appendChild(imgElement);

        return linkElement.outerHTML.replace(/&amp;/g, '&');
    }

    referralLink(resource?: ReferralResource) {
        const protocol = window.location.protocol;
        const resourceParam = resource ? '&p1=' + resource.id : '';
        return protocol + '//' + this.appState.site.siteDomain + '/?ref=' + this.appState.rawUserInfo!.refCode + resourceParam;
    }

    @action
    createRefCode() {
        this.isRefCodeLoading = true;
        this.appState.api.createRefCode(this.onCreateRefCode.bind(this));
    }

    @action
    onCreateRefCode(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);
        this.isRefCodeLoading = false;

        if (act.createRefCode && act.createRefCode.ok) {
            this.loadRefInfo();
        } else {
            this.refCodeError = this.appState.t('error-create-ref-code');
        }
    }

    @action
    loadRefInfo() {
        if (this.appState.rawUserInfo && this.appState.rawUserInfo.refCode) {
            this.appState.api.getRefInfo(this.onLoadRefInfo.bind(this));
        }
    }

    onLoadRefInfo(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.getRefInfo && act.getRefInfo.ok) {
            this.refInfo = act.getRefInfo;
        } else {
            this.refInfoError = this.appState.t('error-load-ref-info');
        }
    }

    getImgSizeString(size: Size) {
        return size.width + 'x' + size.height;
    }

    onRequestWithdrawal = (event: SyntheticEvent<HTMLElement>) => {
        event.preventDefault();
        this.appState.chat.expand();
    };

    getRefInfoBalance() {
        if (this.refInfo) {
            const currency = currencies.findCurrencyById(this.refInfo.refCurrency);
            return formatCurrencyAmount(this.refInfo.refProfit, currency);
        } else {
            return NASymbol;
        }
    }
}