import AppState from '../AppState';
import {GuestPopupShowMode, GuestPopupId, GuestPopupItem} from '../meta';
import {action} from 'mobx';
import {cookie, getLanguagefromUrl} from '../utils/BrowserUtils';
import {isDef} from '../../api/utils';

export default class GuestPopup {
    // guest popup settings
    list: GuestPopupItem[] = [];
    mode: GuestPopupShowMode = GuestPopupShowMode.BY_INACTIVITY;

    timeoutId: NodeJS.Timer | null = null;

    private appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;

        this.onTimeout = this.onTimeout.bind(this);
        this.hidePopup = this.hidePopup.bind(this);
        this.startTimer = this.startTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    init() {
        const guestPopupSettings = this.appState.appSettings.guestPopups;
        const enabled = guestPopupSettings && guestPopupSettings.enabled;
        const list = guestPopupSettings && guestPopupSettings.list;
        const mode = guestPopupSettings && guestPopupSettings.mode;

        // if all settings are set
        if (enabled && mode && list && list.length) {
            // set settings
            this.list = list.filter((item) => isDef(item.enabled) ? item.enabled !== false : true);
            this.mode = mode;

            // remove shown popups
            const shownPopupsIds = this.getShownPopups(this.list);
            for (let id of shownPopupsIds) {
                const index = this.list.map((item: GuestPopupItem) => item.id).indexOf(id as GuestPopupId);
                if (index !== -1) {
                    this.list.splice(index, 1);
                }
            }

            // setup timers
            this.setupTimersByMode();
        }
    }

    destroy() {
        this.destroyTimer();

        if (this.mode === GuestPopupShowMode.BY_INACTIVITY) {
            this.removeEventListeners();
        }
    }

    setupTimersByMode() {
        if (!this.isAllPopupsShown()) {
            // inactivity mode
            if (this.mode === GuestPopupShowMode.BY_INACTIVITY) {
                this.setupEventListeners();
                this.startTimer();
            }

            // timeout mode
            if (this.mode === GuestPopupShowMode.BY_TIMEOUT) {
                this.startTimer();
            }
        }
    }

    // region ---- inactivity/timeout mode

    setupEventListeners() {
        document.addEventListener('mousemove', this.resetTimer, false);
        document.addEventListener('mousedown', this.resetTimer, false);
        document.addEventListener('keypress', this.resetTimer, false);
        document.addEventListener('touchmove', this.resetTimer, false);
    }

    removeEventListeners() {
        document.removeEventListener('mousemove', this.resetTimer);
        document.removeEventListener('mousedown', this.resetTimer);
        document.removeEventListener('keypress', this.resetTimer);
        document.removeEventListener('touchmove', this.resetTimer);
    }

    startTimer() {
        const popup = this.getPopupToShow();
        if (popup) {
            this.timeoutId = setTimeout(this.onTimeout, popup.timeout);
        }
    }

    onTimeout() {
        const popup = this.getPopupToShow();
        const isExcludeUrl = this.checkExcludeUrlList();

        if (isExcludeUrl || this.appState.modal.activeModal) {
            this.resetTimer();
            return;
        }

        if (popup) {
            this.showPopup(popup);
            this.markPopupAsShown(popup.id);

            if (this.mode === GuestPopupShowMode.BY_INACTIVITY) {
                this.removeEventListeners();
            }
        }
    }

    checkExcludeUrlList(): boolean {
        const guestPopupSettings = this.appState.appSettings.guestPopups;
        const excludeUrls = guestPopupSettings && guestPopupSettings.excludeUrls;
        const lang = getLanguagefromUrl();
        const pathname = window.location.pathname;

        if (excludeUrls && excludeUrls.length) {
            for (let i = 0; i < excludeUrls.length; i++) {
                let url = excludeUrls[i];
                if (lang === this.appState.language) {
                    url = `/${lang}` + url;
                }

                if (url === pathname) {
                    return true;
                }
            }
        }

        return false;
    }

    resetTimer() {
        this.destroyTimer();
        this.startTimer();
    }

    destroyTimer() {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
            this.timeoutId = null;
        }
    }

    // endregion

    // region ---- common
    getPopupToShow(): GuestPopupItem | null {
        const shownListIds = this.getShownPopups(this.list);
        for (let i = 0; i < this.list.length; i++) {
            if (shownListIds.indexOf(this.list[i].id) === -1) {
                return this.list[i];
            }
        }

        return null;
    }

    isAllPopupsShown(): boolean {
        const guestPopupSettings = this.appState.appSettings.guestPopups;
        const shownListIds = this.getShownPopups(this.list);
        return shownListIds.length === guestPopupSettings.list.length;
    }

    // endregion

    // region ---- guest popup actions
    @action
    showPopup(popup: GuestPopupItem) {
        this.appState.modal.show(popup.id);
    }

    @action
    hidePopup() {
        this.appState.modal.hideModal();
        this.onHidePopup();
    }

    onHidePopup() {
        this.setupTimersByMode();
    }

    // endregion

    // region ---- cookies
    getShownPopups(list: GuestPopupItem[]): string[] {
        const result = [];
        for (let item of list) {
            const cookieKey = `wlguestpopup:${window.location.host}:${item.id}`;
            if (cookie(cookieKey)) {
                result.push(item.id);
            }
        }
        return result;
    }

    markPopupAsShown(id: string) {
        const cookieKey = `wlguestpopup:${window.location.host}:${id}`;
        cookie(cookieKey, 'shown', 30 * 60 * 60 * 365, '/');
    }
}