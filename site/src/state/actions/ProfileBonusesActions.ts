import AppState from '../AppState';
import {action, computed, observable} from 'mobx';
import {bonuses, goapi, messages} from '../../api/proto';
import {formatCurrencyAmount, formatCurrencyCode, formatPercent, getFreeSpinsToken} from '../utils/Money';
import {calcPercent, calcWagerMultiplier, maxLongValue} from '../utils/Math';
import {dateFromLongSec, formatDateTime, msToTime} from '../utils/Datetime';
import {NLong, NString, SitePages} from '../meta';
import {NotificationItemLevel} from '../components/Notificator';
import {
    BonusMeta,
    BonusMetaObj,
    BonusType,
    formatBonusState,
    getBonusActivationErrorToken,
    getBonusCancelErrorToken,
    isDef,
    isNullDate,
    processBonusPercentValue,
    processBoolean,
    processFreeSpinsAmount,
    processLong,
    processNumber,
    processString,
    BonusFamily,
} from '../../api/utils';
import {GameItem} from './GamesActions';
import Long from 'long';
import { convertStringToLong } from '../utils/convertStringToLong';
import { CaseType } from './WagerBonusProcessor';
import { IDailyBonusRules } from './daily-bonus/DailyBonus';
import { ManualPromoCode } from './ui/ManualPromoCode';

const HISTORY_ITEMS_LIMIT = 5;

interface BonusStats {
    caption: string;
    value: string;
}

interface BonusTags {
    name: string;
    color: string;
    textColor: string;
}

export interface BonusEntryFreeSpins {
    amountStr: string;
    games: GameItem[];
    count: number;
    countInitial: number;
    progressPercent: string;
    state: bonuses.FreeSpinsState;
}

export interface BonusEntryWager {
    initialValue?: string;
    initialAmount?: string;
    leftValue?: string;
    leftAmount?: string;
    wageredValue?: string;
    wageredAmount?: string;
    progressPercent?: string;
    multiplier?: NString;
    currencyCode?: string;
}

export interface BonusConditions {
    value: string;
    name?: string;
    help?: string;
}

export interface BonusConditionsGroup {
    caption?: string;
    conditions: BonusConditions[];
}

export interface BonusEntry {
    id: NString;
    bonusType: string | null;
    bonusFamily?: string | null;
    bonusId: string;
    name: string;
    state: bonuses.BonusCode.State;
    stateStr: string;

    value: NString;
    freeSpins: BonusEntryFreeSpins[] | null;
    wager: BonusEntryWager | null;
    wageringStrategy?: bonuses.BonusCode.WageringStrategy | null;

    isCanceled: boolean;
    canCancel: boolean;
    canBeReactivated: boolean;
    promoCode: string;

    createdAt: string;
    createdAtStamp: number | null;
    activatedAt: NString;
    activatedAtStamp: number | null;
    wageredAt: NString;
    wageredAtStamp: number | null;

    stats?: BonusStats[];
    receiveConditions?: BonusConditionsGroup[];
    wageringConditions?: BonusConditionsGroup[];
    withdrawalConditions?: BonusConditionsGroup[];

    isRulesAvailable: boolean;
    issueRules?: string;
    wageringRules?: string;
    withdrawalRules?: string;
    htmlDescription?: string;
    pageDescription?: string;
    hideWagerMultiplier?: boolean;
    amount?: string;
    redeemAmount?: string;
    redeemMultiplier?: string;
    activeFreeSpinsAmount?: string;
    isDefaultBonusImage: boolean;
    bonusImageUrl: string;
    isAvailable: boolean;
    depositMultiplier?: string;
    maxPossibleFixedBonusAmount?: [string, string];
    maxPossibleCashbackPercent?: string;
    manualPromoCode?: ManualPromoCode;
    freespinsWager?: string;
    bonusTags?: BonusTags[];
    meta: { [k: string]: string };
    tournamentType?: string;
    isWeeklyTournament?: boolean;
    timeToLeftWagering?: number;
    balanceAfterTimeExpired?: string;
    displayPriority?: number;
    fsWin?: string;
    gameItem?: GameItem | null;
    allowedWithdrawal?: string;
}

export enum BonusLoadingType {
    ACCEPT = 'accept',
    REFUSE = 'refuse'
}

export class ProfileBonusesActions {
    appState: AppState;

    @observable isLoading: boolean = false;
    @observable actionLoadingMap: { [key: string]: string } = {};
    @observable userBonuses: BonusEntry[] = [];
    @observable possibleBonusesList: BonusEntry[] = [];
    @observable bonusToConfirmAction: BonusEntry | null = null;
    @observable bonusCorrection: bonuses.BonusCancelPreviewResponse.ICorrection | null = null;
    @observable processingError: string | null = null;
    openedRulesInBonuses = observable<string>([]);
    @observable historyLimit = HISTORY_ITEMS_LIMIT;
    codes: bonuses.IBonusCode[] = [];
    possibleCodes: bonuses.UserBonusesListResponse.IPossibleBonusCode[] = [];
    timeToLeftWageringInterval?: ReturnType<typeof setTimeout>;

    @observable nonTakenSpinsGames?: (GameItem | null)[] = [];

    constructor(state: AppState) {
        this.appState = state;

        this.onAcceptBonus = this.onAcceptBonus.bind(this);
        this.onRefuseBonus = this.onRefuseBonus.bind(this);

        this.showRefuseModal = this.showRefuseModal.bind(this);
        this.hideRefuseModal = this.hideRefuseModal.bind(this);

        this.isAnyActionLoading = this.isAnyActionLoading.bind(this);
        this.setBonusActionLoading = this.setBonusActionLoading.bind(this);

        this.tryLoadBonusesData = this.tryLoadBonusesData.bind(this);

        this.hasActiveBonus = this.hasActiveBonus.bind(this);
        this.hasClosedBonusThatCantBeReactivated = this.hasClosedBonusThatCantBeReactivated.bind(this);
    }

    @action.bound
    resetStateOnUnmount() {
        this.historyLimit = HISTORY_ITEMS_LIMIT;
    }

    // region ---- data loading and processing
    @action
    tryLoadBonusesData(optCallback?: Function) {
        this.isLoading = true;
        this.processingError = null;
        this.appState.userBonusesStore.fetchBonuses(this.onTryLoadBonusesData.bind(this, optCallback));
    }

    @action
    onTryLoadBonusesData(optCallback: Function | null | undefined, codes: bonuses.IBonusCode[], possibleCodes: bonuses.UserBonusesListResponse.IPossibleBonusCode[]) {
        this.isLoading = false;
        this.codes = codes;
        this.possibleCodes = possibleCodes;
        if (this.timeToLeftWageringInterval) clearInterval(this.timeToLeftWageringInterval);

        codes.sort(function (a: bonuses.IBonusCode, b: bonuses.IBonusCode) {
            const aCreated = a.createdAt ? a.createdAt.toNumber() : 0;
            const bCreated = b.createdAt ? b.createdAt.toNumber() : 0;
            return bCreated - aCreated;
        });
        this.userBonuses = codes
            .filter((b: bonuses.IBonusCode) => {
                const type = processString(BonusMeta.BONUS_TYPE, b.meta, null);
                return type !== BonusType.TOURNAMENT &&
                    type !== BonusType.FOLLOW_UP &&
                    type !== BonusType.SKILLWHEEL &&
                    type !== BonusType.WITHDRAWALS_INSPECTOR;
            })
            .map((b: bonuses.IBonusCode) => {
                const type = processString(BonusMeta.BONUS_TYPE, b.meta, null);
                if (type === BonusType.WAGER_BONUS && b.state === bonuses.BonusCode.State.BS_WAGERING) {
                    const processedWagerBonus = this.appState.wagerBonusProcessor.process(b)[0] as bonuses.IBonusCode;
                    if (processedWagerBonus) {
                        this.appState.wagerBonus.processActiveWagerBonus(processedWagerBonus);
                        return processedWagerBonus;
                    }
                    return b;
                }
                const family = processString(BonusMeta.BONUS_FAMILY, b.meta, null);
                if (family === BonusFamily.REFILLS) {
                    const processedbalanceWager = this.appState.balanceWagerProcessor.process(b)[0] as bonuses.IBonusCode;
                    if (processedbalanceWager) {
                        //this.appState.wagerBonus.processActiveWagerBonus(processedWagerBonus);
                        return processedbalanceWager;
                    }
                    return b;
                }
                return b;
            })
            .map(this.processBonusCode, this);

        if (this.userBonuses.find(bonus => bonus.timeToLeftWagering)) {
            this.timeToLeftWageringInterval = setInterval(() => {
                this.userBonuses = this.userBonuses.map(bonus => {
                    if (bonus.timeToLeftWagering) {
                        return {
                            ...bonus,
                            timeToLeftWagering: bonus.timeToLeftWagering >= 1000 ? bonus.timeToLeftWagering - 1000 : 0,
                        }
                    }
                    return bonus;
                })
            }, 1000)
        }

        this.possibleBonusesList = this.processPossibleBonuses(possibleCodes);

        if (optCallback) {
            optCallback();
        }
    }

    processBonusCode(bonusCode: bonuses.IBonusCode): BonusEntry {
        const { wageringStrategy } = bonusCode;
        const freeSpins = this.processBonusFreeSpins(bonusCode);
        const wager = this.processBonusWager(bonusCode);
        const stats = this.processBonusStats(bonusCode);
        const wageringConditions = this.processBonusWageringConditions(bonusCode, wager);
        const receiveConditions = this.processBonusReceiveConditions(bonusCode);
        const withdrawalConditions = this.processBonusWithdrawalConditions(bonusCode);
        const issueRules = bonusCode.meta?.issue_rules;
        const wageringRules = bonusCode.meta?.wagering_rules;
        const withdrawalRules = bonusCode.meta?.withdrawal_rules;
        const value = this.processBonusValue(bonusCode, freeSpins);
        const canBeReactivated = processBoolean(BonusMeta.CAN_BE_REACTIVATED, bonusCode.meta);
        const canCancel = (bonusCode.state === bonuses.BonusCode.State.BS_WAGERING ||
            bonusCode.state === bonuses.BonusCode.State.BS_WAGERED) &&
            !!bonusCode.meta && isDef(bonusCode.meta[BonusMeta.CANT_CANCEL_BONUS]) && !processBoolean(BonusMeta.CANT_CANCEL_BONUS, bonusCode.meta);
        const bonusType = processString(BonusMeta.BONUS_TYPE, bonusCode.meta, null);
        const isCanceled = bonusCode.state === bonuses.BonusCode.State.BS_MANUALLY_CLOSED;
        const amount = this.processBonusAmount(bonusCode, bonusType);
        const redeemAmount = this.processBonusRedeemAmount(bonusCode);
        const redeemMultiplier = this.processBonusRedeemMultiplier(bonusCode);
        const activeFreeSpinsAmount = this.processBonusFreeSpinsAmount(bonusCode);
        const { bonusImageUrl, isDefaultBonusImage } = this.processBonusImageUrl(bonusCode, bonusType);
        const depositMultiplier = this.processBonusDepositMultiplier(bonusCode);
        const maxPossibleFixedBonusAmount = this.processMaxPossibleFixedBonusAmount(bonusCode);
        const maxPossibleCashbackPercent = this.processMaxPossibleCashbackPercent(bonusCode);
        const isRulesAvailable = !!issueRules || !!wageringRules || !!withdrawalRules || wageringConditions.length > 0 || receiveConditions.length > 0 || (withdrawalConditions.length > 0 && withdrawalConditions[0].conditions.length > 1);
        const manualPromoCode = this.processManualPromoCode(bonusCode);
        const freespinsWager = this.processFreespinsWager(bonusCode);
        const bonusTags = this.processBonusTags(bonusCode);
        const dateFormat = this.appState.appSettings.formatDatesForUS ? 'LL/dd/yyyy HH:mm:ss' : 'dd.LL.yyyy HH:mm:ss';
        const tournamentType = bonusCode.meta?.tournament_type || '';
        const isWeeklyTournament = (bonusType === BonusType.REGULAR_TOURNAMENT || bonusType === BonusType.MANUAL_ACTIVATION_REGULAR_TOURNAMENT) && tournamentType === 'weekly';
        const wageringFinishTime = processString(BonusMeta.WAGERING_FINISH_TIME, bonusCode.meta, null);
        const timeNow = Date.now();
        const timeToLeftWagering = wageringFinishTime && Number(wageringFinishTime) > timeNow ? Number(wageringFinishTime) - timeNow : undefined;
        const balanceAfterTimeExpiredRaw = processString(BonusMeta.BALANCE_AFTER_TIME_EXPIRED, bonusCode.meta, null);
        const bonusFamily = processString(BonusMeta.BONUS_FAMILY, bonusCode.meta, null);
        const displayPriority = processNumber(BonusMeta.DISPLAY_PRIORITY, bonusCode.meta);
        const fsWin = this.processFsWin(bonusCode);
        const gameItem = this.processGameItem(bonusCode);
        const allowedWithdrawal = this.processAllowedWithdrawal(bonusCode, bonusType);

        return {
            id: bonusCode.id ? bonusCode.id.toString() : null,
            bonusType,
            bonusFamily,
            bonusId: bonusCode.bonusId!,
            value,
            name: this.processBonusName(bonusCode),
            state: bonusCode.state ?? bonuses.BonusCode.State.BS_PENDING,
            stateStr: this.appState.t(formatBonusState(bonusCode.state, isCanceled)),
            freeSpins,
            wager,
            stats,
            isRulesAvailable,
            receiveConditions,
            wageringConditions,
            withdrawalConditions,
            isCanceled,
            canCancel,
            canBeReactivated,
            wageringStrategy,

            promoCode: bonusCode.code!,
            createdAt: formatDateTime(bonusCode.createdAt, true, dateFormat),
            createdAtStamp: dateFromLongSec(bonusCode.createdAt),
            activatedAt: formatDateTime(bonusCode.activatedAt, true, dateFormat),
            activatedAtStamp: dateFromLongSec(bonusCode.activatedAt),
            wageredAt: formatDateTime(bonusCode.wageredAt, true, dateFormat),
            wageredAtStamp: dateFromLongSec(bonusCode.wageredAt),

            issueRules,
            wageringRules,
            withdrawalRules,
            htmlDescription: bonusCode.meta?.html_description,
            pageDescription: bonusCode.meta?.bonuses_page_description,
            hideWagerMultiplier: bonusCode.meta?.hide_wager_multiplier === "true",
            isAvailable: bonusCode.meta?.is_available === 'true',
            isDefaultBonusImage,
            bonusImageUrl,
            amount,
            redeemAmount,
            redeemMultiplier,
            activeFreeSpinsAmount,
            depositMultiplier,
            maxPossibleFixedBonusAmount,
            maxPossibleCashbackPercent,
            manualPromoCode,
            freespinsWager,
            bonusTags,
            meta: bonusCode.meta || {},
            tournamentType,
            isWeeklyTournament,
            timeToLeftWagering: timeToLeftWagering ? Number(timeToLeftWagering) : undefined,
            balanceAfterTimeExpired: balanceAfterTimeExpiredRaw ? formatCurrencyAmount(new Long(Number(balanceAfterTimeExpiredRaw)), bonusCode.currency, false) : undefined,
            displayPriority: displayPriority ? displayPriority : 0,
            fsWin,
            gameItem,
            allowedWithdrawal,
        };
    }

    processBonusWager(bonusCode: bonuses.IBonusCode): BonusEntryWager | null {
        if (bonusCode.wagerLeftInitial && bonusCode.wagerLeftInitial.greaterThan(0)) {
            const currency = bonusCode.currency;
            const wagerLeft = maxLongValue(bonusCode.wagerLeft, Long.fromNumber(0)) || Long.fromNumber(0);
            const wagered = maxLongValue(bonusCode.wagerLeftInitial.subtract(wagerLeft), Long.fromNumber(0));
            let multiplier = null;
            if (bonusCode.amount && bonusCode.amount.greaterThan(0)) {
                multiplier = 'x' + calcWagerMultiplier(bonusCode.wagerLeftInitial, bonusCode.amount).toString();
            } else if (bonusCode.meta && bonusCode.meta[BonusMeta.FREE_SPINS_BONUS_WAGER_MULTIPLIER]) {
                multiplier = 'x' + bonusCode.meta[BonusMeta.FREE_SPINS_BONUS_WAGER_MULTIPLIER];
            } else if (bonusCode.meta && bonusCode.meta[BonusMeta.BALANCE_BONUS_WAGER_MULTIPLIER]) {
                multiplier = 'x' + bonusCode.meta[BonusMeta.BALANCE_BONUS_WAGER_MULTIPLIER];
            }
            return {
                initialValue: formatCurrencyAmount(bonusCode.wagerLeftInitial, currency, true),
                initialAmount: formatCurrencyAmount(bonusCode.wagerLeftInitial, currency, false),
                wageredValue: formatCurrencyAmount(wagered, currency, true),
                wageredAmount: formatCurrencyAmount(wagered, currency, false),
                leftValue: formatCurrencyAmount(wagerLeft, currency, true),
                leftAmount: formatCurrencyAmount(wagerLeft, currency, false),
                progressPercent: formatPercent(calcPercent(wagerLeft, bonusCode.wagerLeftInitial)),
                multiplier: multiplier,
                currencyCode: formatCurrencyCode(currency)
            };
        } else if (bonusCode.meta) {
            if (bonusCode.meta[BonusMeta.BALANCE_BONUS_WAGER_MULTIPLIER]) {
                return {multiplier: 'x' + bonusCode.meta[BonusMeta.BALANCE_BONUS_WAGER_MULTIPLIER]};
            }
            if (bonusCode.meta[BonusMeta.FREE_SPINS_BONUS_WAGER_MULTIPLIER]) {
                return {multiplier: 'x' + bonusCode.meta[BonusMeta.FREE_SPINS_BONUS_WAGER_MULTIPLIER]};
            }
            if (!!bonusCode.meta[BonusMeta.AMOUNT_WAGER] && (!!bonusCode.meta[BonusMeta.BONUS_MAX_POSSIBLE_PERCENT] || !!bonusCode.meta[BonusMeta.MAX_POSSIBLE_FIXED_BONUS_AMOUNT])) {
                if ([BonusType.FIRST_DEPOSIT_BONUS, BonusType.FOLLOWING_DEPOSITS_BONUS, BonusType.DEPOSIT_BY_CODE].includes(bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType)) {
                    return {multiplier: `${this.appState.t('promo-up-to')} x${bonusCode.meta[BonusMeta.AMOUNT_WAGER].split('.')[0]}`};
                } else {
                    return {multiplier: `x${bonusCode.meta[BonusMeta.AMOUNT_WAGER].split('.')[0]}`};
                }
            }
        }
        return null;
    }

    processBonusFreeSpins(bonusCode: bonuses.IBonusCode): BonusEntryFreeSpins[] | null {
        const freeSpins = bonusCode.freeSpins || [];
        const freeBets = this.convertFreeBetsAsFreeSpinsRepresentation(bonusCode.freeBets || []);
        const mergedFreeActions = freeSpins.concat(freeBets);
        if (mergedFreeActions.length > 0) {
            const appState = this.appState;
            const isGameSelected = mergedFreeActions.some(_ => _.state === this.appState.enums.bonusFreeSpinsState.OTHER_GAME_SELECTED);
            return mergedFreeActions
                .filter((f: bonuses.IFreeSpins) => !!(
                    f.count &&
                    f.countInitial?.greaterThan(0) &&
                    appState.games.findGameById(f.gameId) &&
                    (!isGameSelected || f.state !== this.appState.enums.bonusFreeSpinsState.OTHER_GAME_SELECTED)
                ), this)
                .map((f: bonuses.IFreeSpins): BonusEntryFreeSpins => {
                    return {
                        games: [appState.games.findGameById(f.gameId)!],
                        amountStr: f.countInitial!.toString() + ' ' + this.appState.t(getFreeSpinsToken(f.countInitial!)),
                        count: f.countInitial!.toNumber() - f.count!.toNumber(),
                        countInitial: f.countInitial!.toNumber(),
                        progressPercent: formatPercent(calcPercent(f.count!, f.countInitial!)),
                        state: f.state!
                    };
                });
        } else if (!!bonusCode.meta) {
            if (bonusCode.meta[BonusMeta.POSSIBLE_FREE_SPINS]) {
                const games = bonusCode.meta[BonusMeta.POSSIBLE_FREE_SPINS].split(':').map(_ => this.appState.games.findGameById(_)).filter(_ => !!_);
                return games.length > 0 ? games.map(_ => ({
                    games: [_] as GameItem[],
                    amountStr: "0",
                    count: 0,
                    countInitial: 0,
                    progressPercent: "",
                    state: bonuses.FreeSpinsState.CREATED,
                })) : null;
            } else if (bonusCode.meta[BonusMeta.GAME_ID]) {
                const game = this.appState.games.findGameById(bonusCode.meta[BonusMeta.GAME_ID]);
                return game ? [{
                    games: [game],
                    amountStr: "0",
                    count: 0,
                    countInitial: 0,
                    progressPercent: "",
                    state: bonuses.FreeSpinsState.CREATED,
                }] : null;
            }

        }
        return null;
    }

    processBonusStats(bonusCode: bonuses.IBonusCode): BonusStats[] {
        const result = [];
        const dateFormat = this.appState.appSettings.formatDatesForUS ? 'LL/dd/yyyy' : 'dd.LL.yyyy';

        if (!isNullDate(bonusCode.createdAt)) {
            result.push({caption: 'ui-received-date-short', value: formatDateTime(bonusCode.createdAt, true, dateFormat)});
        }

        if (!isNullDate(bonusCode.activatedAt)) {
            result.push({caption: 'ui-activation-date-short', value: formatDateTime(bonusCode.activatedAt, true, dateFormat)});
        }

        return result;
    }

    processFsWin(bonusCode: bonuses.IBonusCode) {
        if (bonusCode.meta?.bonus_type! === BonusType.SAFE_REGISTRATION_BONUS &&
            (bonusCode.state === bonuses.BonusCode.State.BS_WAGERING ||
            bonusCode.state === bonuses.BonusCode.State.BS_WAGERED) && bonusCode.meta?.popup) {
            const popup = JSON.parse(bonusCode.meta.popup);
            if (popup.fsWin && popup.fsWin > 0 && popup.currency) {
                return formatCurrencyAmount(Long.fromNumber(popup.fsWin), Long.fromNumber(popup.currency));
            }
        }
        return undefined;
    }

    processBonusValue(bonusCode: bonuses.IBonusCode, freeSpins: BonusEntryFreeSpins[] | null): string | null {
        const amount = bonusCode.amount && bonusCode.amount.greaterThan(0) ?
            formatCurrencyAmount(bonusCode.amount, bonusCode.currency, false) :
            null;
        const totalFreeSpinsCount = freeSpins ?
            freeSpins.reduce((result: number, f: BonusEntryFreeSpins) => result + f.countInitial, 0) :
            null;
        const freeSpinsCountStr = totalFreeSpinsCount ?
            totalFreeSpinsCount.toString() + ' ' + this.appState.t(getFreeSpinsToken(totalFreeSpinsCount)) :
            null;
        const percentValue = processBonusPercentValue(bonusCode.meta, this.appState);
        const amountStr = amount || percentValue;

        if (amountStr && freeSpinsCountStr) {
            return this.appState.t('ui-and', {
                val1: amountStr,
                val2: freeSpinsCountStr
            });
        } else if (amountStr) {
            return amountStr;
        } else if (freeSpinsCountStr) {
            return freeSpinsCountStr;
        }
        return null;
    }

    processBonusName(bonusCode: bonuses.IBonusCode): string {
        if (bonusCode.meta && bonusCode.meta[BonusMeta.BONUS_TITLE]) {
            return this.appState.t(bonusCode.meta[BonusMeta.BONUS_TITLE]);
        } else {
            return this.appState.t(bonusCode.bonusId!.toString());
        }
    }

    processBonusWithdrawalConditions(bonusCode: bonuses.IBonusCode): BonusConditionsGroup[] {
        const result: BonusConditionsGroup[] = [];
        const hasMeta = bonusCode.meta && Object.keys(bonusCode.meta).length > 0;

        if (hasMeta) {
            const group: BonusConditionsGroup = {conditions: []};
            const currency = processLong(BonusMeta.WITHDRAWAL_CONDITIONS_CURRENCY, bonusCode.meta);
            const maxWinLimit = processLong(BonusMeta.WITHDRAWAL_CONDITIONS_MAX_WIN_LIMIT, bonusCode.meta);
            const minDepositAmount = processLong(BonusMeta.WITHDRAWAL_CONDITIONS_MIN_DEPOSITS_SUM, bonusCode.meta);

            if (currency && maxWinLimit) {
                group.conditions.push({
                    name: this.appState.t('ui-max-win-amount'),
                    value: formatCurrencyAmount(maxWinLimit, currency, false)
                });
            }

            if (currency && minDepositAmount) {
                group.conditions.push({
                    name: this.appState.t('ui-deposit-condition'),
                    value: formatCurrencyAmount(minDepositAmount, currency, false)
                });
            }

            if (!minDepositAmount && !maxWinLimit) {
                group.conditions.push({value: this.appState.t('ui-without-restrictions')});
            }

            if (group.conditions.length) {
                result.push(group);
            }
        }

        return result;
    }

    processBonusReceiveConditions(bonusCode: bonuses.IBonusCode): BonusConditionsGroup[] {
        const result: BonusConditionsGroup[] = [];
        const hasMeta = bonusCode.meta && Object.keys(bonusCode.meta).length > 0;

        if (hasMeta) {
            const group: BonusConditionsGroup = {conditions: []};
            const isPhoneConfirmationRequired = processBoolean(BonusMeta.RECEIVE_CONDITIONS_PHONE_CONFIRMATION_REQUIRED, bonusCode.meta);
            const isEmailConfirmationRequired = processBoolean(BonusMeta.RECEIVE_CONDITIONS_EMAIL_CONFIRMATION_REQUIRED, bonusCode.meta);
            const isPromoCodeRequired = processBoolean(BonusMeta.RECEIVE_CONDITIONS_PROMO_CODE_REQUIRED, bonusCode.meta);
            const issuePeriod = this.processBonusIssuePeriod(bonusCode.meta);

            if (isPhoneConfirmationRequired && isEmailConfirmationRequired) {
                group.conditions.push({value: this.appState.t('ui-condition-email-and-phone')});
            } else if (isPhoneConfirmationRequired) {
                group.conditions.push({value: this.appState.t('ui-condition-phone')});
            } else if (isEmailConfirmationRequired) {
                group.conditions.push({value: this.appState.t('ui-condition-email')});
            }

            const promoCode = processString(BonusMeta.RECEIVE_CONDITIONS_PROMO_CODES, bonusCode.meta, null);
            if (promoCode) {
                group.conditions.push({name: this.appState.t('page-refill-promo'), value: promoCode});
            }

            const currency = processLong(BonusMeta.RECEIVE_CONDITIONS_CURRENCY, bonusCode.meta);
            const depositMinAmount = processLong(BonusMeta.RECEIVE_CONDITIONS_MIN_DEPOSIT, bonusCode.meta);
            const depositMinAmountSum = processLong(BonusMeta.RECEIVE_CONDITIONS_MIN_DEPOSIT_SUM, bonusCode.meta);
            const depositMaxAmount = processLong(BonusMeta.RECEIVE_CONDITIONS_MAX_DEPOSIT, bonusCode.meta);

            if (currency && depositMinAmount && depositMaxAmount) {
                group.conditions.push({
                    name: this.appState.t('ui-deposit-amount'),
                    value: this.appState.t('ui-from-to', {
                        from: formatCurrencyAmount(depositMinAmount, currency, true),
                            to: formatCurrencyAmount(depositMaxAmount, currency, true),
                        currency: formatCurrencyCode(currency)
                    })
                });
            } else if (currency && depositMinAmount) {
                group.conditions.push({
                    name: this.appState.t('ui-deposit-amount-not-less-than'),
                    value: formatCurrencyAmount(depositMinAmount, currency, false)
                });
            } else if (currency && depositMaxAmount) {
                group.conditions.push({
                    name: this.appState.t('ui-deposit-amount-not-greater-than'),
                    value: formatCurrencyAmount(depositMaxAmount, currency, false)
                });
            } else if (currency && depositMinAmountSum) {
                group.conditions.push({
                    name: this.appState.t('ui-min-deposits-amount-sum'),
                    value: formatCurrencyAmount(depositMinAmountSum, currency, false)
                });
            }

            if (isPromoCodeRequired) {
                group.conditions.push({
                    value: this.appState.t('ui-by-promo-code')
                });
            }

            if (issuePeriod) {
                group.conditions.push({
                    name: this.appState.t('ui-issue-period'),
                    value: this.appState.t(issuePeriod)
                });
            }

            if (group.conditions.length) {
                result.push(group);
            }
        }

        return result;
    }

    processBonusIssuePeriod(meta: BonusMetaObj): string | null {
        const strPeriod = processString(BonusMeta.RECEIVE_CONDITIONS_ISSUE_PERIOD, meta, null);
        const numPeriod = parseInt(strPeriod!, 10);

        if (numPeriod === 1000 * 60 * 60 * 24 || strPeriod === 'day') {
            return 'ui-every-day';
        } else if (numPeriod === 1000 * 60 * 60 * 24 * 7 || strPeriod === 'week') {
            const startCashBackDateString = processString(BonusMeta.RECEIVE_CONDITIONS_ISSUE_PERIOD_START, meta, null);
            const startCashBackDay = startCashBackDateString ? new Date(startCashBackDateString).getDay() : null;
            switch (startCashBackDay) {
                case 0:
                    return 'ui-every-sunday';
                case 1:
                    return 'ui-every-monday';
                case 2:
                    return 'ui-every-tuesday';
                case 3:
                    return 'ui-every-wednesday';
                case 4:
                    return 'ui-every-thursday';
                case 5:
                    return 'ui-every-friday';
                case 6:
                    return 'ui-every-saturday';
                default:
                    return null;
            }
        } else if (strPeriod === 'month') {
            return 'ui-every-month';
        } else {
            return null;
        }
    }

    processBonusWageringConditions(bonusCode: bonuses.IBonusCode, wager: BonusEntryWager | null): BonusConditionsGroup[] {
        const result: BonusConditionsGroup[] = [];
        const hasMeta = bonusCode.meta && Object.keys(bonusCode.meta).length > 0;

        if (hasMeta && wager && wager.multiplier && wager.multiplier !== 'x0') {
            // spins wagering conditions
            const spinsWageringGroup: BonusConditionsGroup = {conditions: []};
            const isSpinsWageringAllowed = processBoolean(BonusMeta.WAGERING_SPINS_ALLOWED, bonusCode.meta);
            if (isSpinsWageringAllowed) {
                spinsWageringGroup.conditions.push({
                    name: this.appState.t('game_menu_slots'),
                    value: this.appState.t('ui-wagering-allowed')
                });
                this.fillWageringConditionsGroup(
                    spinsWageringGroup,
                    processLong(BonusMeta.WAGERING_SPINS_CURRENCY, bonusCode.meta),
                    processLong(BonusMeta.WAGERING_SPINS_MIN_SPIN_AMOUNT, bonusCode.meta),
                    processLong(BonusMeta.WAGERING_SPINS_MAX_SPIN_AMOUNT, bonusCode.meta),
                    processNumber(BonusMeta.WAGERING_SPINS_AMOUNT_MULTIPLIER, bonusCode.meta)
                );
            } else {
                spinsWageringGroup.conditions.push({
                    name: this.appState.t('game_menu_slots'),
                    value: this.appState.t('ui-wagering-forbidden')
                });
            }

            // bets wagering conditions
            const betsWageringGroup: BonusConditionsGroup = {conditions: []};
            const isBetsWageringAllowed = processBoolean(BonusMeta.WAGERING_BETS_ALLOWED, bonusCode.meta);
            if (isBetsWageringAllowed) {
                betsWageringGroup.conditions.push({name: this.appState.t('ui-bets'), value: this.appState.t('ui-wagering-allowed')});
                this.fillWageringConditionsGroup(
                    betsWageringGroup,
                    processLong(BonusMeta.WAGERING_BETS_CURRENCY, bonusCode.meta),
                    processLong(BonusMeta.WAGERING_BETS_MIN_BET_AMOUNT, bonusCode.meta),
                    processLong(BonusMeta.WAGERING_BETS_MAX_BET_AMOUNT, bonusCode.meta),
                    processNumber(BonusMeta.WAGERING_BETS_AMOUNT_MULTIPLIER, bonusCode.meta)
                );
            } else {
                betsWageringGroup.conditions.push({
                    name: this.appState.t('ui-bets'),
                    value: this.appState.t('ui-wagering-forbidden')
                });
            }

            result.push(spinsWageringGroup);
            result.push(betsWageringGroup);
        }

        return result;
    }

    fillWageringConditionsGroup(group: BonusConditionsGroup, currency: NLong, minAmount: NLong, maxAmount: NLong, amountMultiplier: number | null) {
        if (minAmount) {
            group.conditions.push({
                name: this.appState.t('ui-min-bet'),
                value: formatCurrencyAmount(minAmount, currency, false)
            });
        }
        if (maxAmount) {
            group.conditions.push({
                name: this.appState.t('ui-max-bet'),
                value: formatCurrencyAmount(maxAmount, currency, false)
            });
        }
        if (amountMultiplier && amountMultiplier !== 1) {
            group.conditions.push({
                name: this.appState.t('ui-wagering-participation'),
                value: formatPercent(amountMultiplier)
            });
        }
    }

    processBonusFreeSpinsConditions(freeSpins: bonuses.IFreeSpins[] | null): BonusConditionsGroup[] {
        const result: BonusConditionsGroup[] = [];

        return result;
    }

    convertFreeBetsAsFreeSpinsRepresentation(freeBets: bonuses.IFreeBets[]): bonuses.IFreeSpins[] {
        return freeBets.map(f  => {
            return {
                bet: f.bet,
                currencyId: f.currencyId,
                gameId: f.gameId,
                line: null,
                count: f.count,
                countInitial: f.countInitial,
                state: (state => { switch(state) {
                    case bonuses.FreeBets.FreeBetsState.ACTIVATED: return bonuses.FreeSpinsState.ACTIVATED
                    case bonuses.FreeBets.FreeBetsState.BONUS_CLOSED: return bonuses.FreeSpinsState.BONUS_CLOSED
                    case bonuses.FreeBets.FreeBetsState.CREATED: return bonuses.FreeSpinsState.CREATED
                    case bonuses.FreeBets.FreeBetsState.OTHER_GAME_SELECTED: return bonuses.FreeSpinsState.OTHER_GAME_SELECTED
                    case bonuses.FreeBets.FreeBetsState.PLAYED: return bonuses.FreeSpinsState.PLAYED
                    case bonuses.FreeBets.FreeBetsState.STARTED: return bonuses.FreeSpinsState.STARTED
                    case bonuses.FreeBets.FreeBetsState.WAITING_FOR_CLOSE: return bonuses.FreeSpinsState.WAITING_FOR_CLOSE
                    default: return null
                }}) (f.state)
            } as bonuses.IFreeSpins
        })
    }

    processPossibleBonuses(possibleBonuses: bonuses.UserBonusesListResponse.IPossibleBonusCode[]): BonusEntry[] {
        const result: BonusEntry[] = [];
        for (let i = 0, count = possibleBonuses.length; i < count; i++) {
            const bonus = possibleBonuses[i];
            const isBonusEnabled = processBoolean(BonusMeta.ENABLED, bonus.meta);
            const shouldShowInPossibleBonuses =
                processBoolean(BonusMeta.SHOULD_SHOW_IN_POSSIBLE_BONUSES, bonus.meta) ||
                (bonus.meta?.bonus_type === BonusType.DEPOSIT_BY_CODE &&
                    processBoolean(BonusMeta.PROMOCODE_ACTIVATED, bonus.meta));
            if (bonus.meta?.bonus_type === BonusType.DAILY_BONUS && !this.appState.userInfo.guest) {
                const currencyCode = this.appState.userInfo.realCurrency?.code;
                if (currencyCode) {
                    const rules: IDailyBonusRules = JSON.parse(bonus.meta?.daily_rules);
                    if (!!rules.daily_bonuses_list && rules.daily_bonuses_list.every(day => !!day?.amount?.freeSpins && !(currencyCode in day?.amount?.freeSpins))) {
                        continue;
                    }
                }
            }
            if (bonus.id && bonus.meta && isBonusEnabled && shouldShowInPossibleBonuses) {
                const bonusCode: bonuses.IBonusCode = {
                    id: Long.fromNumber(i),
                    userId: null,
                    state: -1,
                    bonusId: bonus.id,
                    createdAt: null,
                    activatedAt: null,
                    wageredAt: null,
                    wagerLeft: null,
                    wagerLeftInitial: null,
                    amount: processLong(BonusMeta.BALANCE_BONUS_AMOUNT, bonus.meta),
                    currency: processLong(BonusMeta.BALANCE_BONUS_CURRENCY, bonus.meta),
                    freeSpins: processFreeSpinsAmount(BonusMeta.FREE_SPINS_BONUS_AMOUNT, bonus.meta),
                    meta: bonus.meta,
                    bannerUrl: bonus.bannerUrl,
                };
                if (processString(BonusMeta.BONUS_TYPE, bonusCode.meta, '') === BonusType.WAGER_BONUS) {
                    const wagerBonuses = this.appState.wagerBonusProcessor.process(bonusCode as bonuses.UserBonusesListResponse.IPossibleBonusCode);
                    for (const wagerBonus of wagerBonuses) {
                        result.push(this.processBonusCode(wagerBonus as bonuses.IBonusCode));
                    }
                } else if (processString(BonusMeta.BONUS_FAMILY, bonusCode.meta, '') === BonusFamily.REFILLS) {
                    const balanceWageres = this.appState.balanceWagerProcessor.process(bonusCode as bonuses.UserBonusesListResponse.IPossibleBonusCode);
                    for (const balanceWager of balanceWageres) {
                        result.push(this.processBonusCode(balanceWager as bonuses.IBonusCode));
                    }
                } else {
                    result.push(this.processBonusCode(bonusCode));
                }
            }
        }
        return result;
    }

    @computed
    get activeBonuses(): BonusEntry[] {
        return this.userBonuses.filter((bonus: BonusEntry) => {
            return (
                bonus.state === bonuses.BonusCode.State.BS_WAGERING ||
                bonus.state === bonuses.BonusCode.State.BS_WAGERED
            )
        }).sort(this.sortBonusesByDisplayPriority);;
    }

    @computed
    get activeBonusesWithWageringTypeExclusive() {
        return this.activeBonuses.filter((bonus: BonusEntry) => bonus.wageringStrategy === bonuses.BonusCode.WageringStrategy.EXCLUSIVE);
    }

    @computed
    get uniqueActiveBonusesWithoutTournaments(): BonusEntry[] {
        const uniqueActiveBonuses: BonusEntry[] = [];
        this.activeBonusesWithoutTournaments.forEach((bonus) => {
            if (!uniqueActiveBonuses.find(b => b.bonusId === bonus.bonusId)) {
                uniqueActiveBonuses.push(bonus);
            }
        })
        return uniqueActiveBonuses;
    }

    @computed
    get activeBonusesWithoutTournaments(): BonusEntry[] {
        return this.activeBonuses.filter((b: BonusEntry) => {
            return b.bonusType !== BonusType.REGULAR_TOURNAMENT &&
                b.bonusType !== BonusType.MANUAL_ACTIVATION_REGULAR_TOURNAMENT
        })
    }

    @computed
    get pendingBonuses(): BonusEntry[] {
        return this.userBonuses
            .filter((bonus: BonusEntry) => bonus.state === bonuses.BonusCode.State.BS_PENDING && bonus.bonusType !== BonusType.USA_REF_INVITER)
            .sort(this.sortBonusesByDisplayPriority);;
    }

    @computed
    get possibleBonuses(): BonusEntry[] {
        return this.possibleBonusesList.filter((b: BonusEntry) => {
            return !this.hasActiveBonus(b.bonusId) && !this.hasClosedBonusThatCantBeReactivated(b.bonusId);
        }).sort(this.sortBonusesByDisplayPriority);
    }

    @computed
    get alreadyReceivedBonuses(): BonusEntry[] {
        return this.possibleBonusesList.filter((b: BonusEntry) => {
            return this.hasClosedBonusThatCantBeReactivated(b.bonusId);
        }).sort(this.sortBonusesByDisplayPriority);
    }

    sortBonusesByDisplayPriority = (bonusA: BonusEntry, bonusB: BonusEntry) => {
        if (bonusA.displayPriority !== undefined && bonusB.displayPriority !== undefined) {
            return bonusB.displayPriority - bonusA.displayPriority;
        }
        return 0;
    }

    @computed
    get bonusesHistory(): BonusEntry[] {
        const history = this.userBonuses.filter((bonus: BonusEntry) => (
            bonus.state === bonuses.BonusCode.State.BS_CLOSED ||
            bonus.state === bonuses.BonusCode.State.BS_MANUALLY_CLOSED ||
            bonus.state === bonuses.BonusCode.State.BS_DECLINED
        ));
        history.sort(function (a: BonusEntry, b: BonusEntry) {
            const aCreated = a.wageredAtStamp || 0;
            const bCreated = b.wageredAtStamp || 0;
            return bCreated - aCreated;
        });
        return history;
    }

    @computed
    get bonusesHistoryPartial(): BonusEntry[] {
        return this.bonusesHistory.slice(0, this.historyLimit);
    }

    @computed
    get hasData(): boolean {
        return !!(this.activeBonuses.length || this.pendingBonuses.length || this.possibleBonuses.length || this.bonusesHistory.length);
    }

    hasActiveBonus(bonusId: string): boolean {
        return !!this.userBonuses.find((ub: BonusEntry) => {
            return ub.bonusId === bonusId && ub.state !== bonuses.BonusCode.State.BS_CLOSED  && ub.state !== bonuses.BonusCode.State.BS_MANUALLY_CLOSED && ub.state !== bonuses.BonusCode.State.BS_DECLINED;
        });
    }

    hasClosedBonusThatCantBeReactivated(bonusId: string): boolean {
        return !!this.userBonuses.find((ub: BonusEntry) => {
            return ub.bonusId === bonusId && !ub.canBeReactivated;
        });
    }

    // endregion

    // region ---- bonus action loading state
    @action
    setBonusActionLoading(code: string, type: BonusLoadingType | null) {
        const map = {...this.actionLoadingMap};
        if (type === null) {
            delete map[code];
        } else {
            map[code] = type;
        }
        this.actionLoadingMap = map;
    }

    isBonusActionLoading(code: string, type: BonusLoadingType): boolean {
        return this.actionLoadingMap[code] === type;
    }

    isAnyActionLoading(): boolean {
        return Object.keys(this.actionLoadingMap).length > 0;
    }

    // endregion

    // region ---- bonus accept button action
    onAcceptBonus(e: React.MouseEvent<HTMLElement>) {
        const code = e.currentTarget.getAttribute('data-value')!;
        this.tryAcceptBonus(code);
    }

    @action
    tryAcceptBonus(code: string, cb?: Function, bonusValue?: NString) {
        this.setBonusActionLoading(code, BonusLoadingType.ACCEPT);
        this.appState.api.activatePromoCode(code, this.onTryAcceptBonus.bind(this, {code, bonusValue, cb}));
    }

    @action
    onTryAcceptBonus(params: {code: string, bonusValue?: NString, cb?: Function}, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const { code } = params;
        const bonus = this.findBonusByCode(code);
        const bonusValue = params.bonusValue ?? bonus?.value;
        this.setBonusActionLoading(code, null);
        this.appState.processApi(msg, act);

        if (act.bonusAction && act.bonusAction.activateCode) {
            const result = act.bonusAction.activateCode.result;
            if (result === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                if (params.cb) {
                    params.cb();
                }

                if (bonus?.isWeeklyTournament) {
                    this.appState.notificator?.addNotification({
                        message: this.appState.t('ui-notification-tournament-bonus-accepted'),
                        level: NotificationItemLevel.SUCCESS
                    });
                } else {
                    this.appState.notificator?.addNotification({
                        message: this.appState.t('ui-notification-bonus-deposited', { amount: bonusValue }),
                        level: NotificationItemLevel.SUCCESS
                    });
                }
                this.onPromoCodeActivation();
            } else {
                const bonusActivationErrorToken = getBonusActivationErrorToken(result);
                this.appState.notificator?.addNotification({
                    message: this.appState.t(bonusActivationErrorToken),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            const bonusActivationErrorToken = getBonusActivationErrorToken(bonuses.BonusCodeActivateResponse.Result.SERVER_ERROR);
            this.appState.notificator?.addNotification({
                message: this.appState.t(bonusActivationErrorToken),
                level: NotificationItemLevel.ERROR
            });
        }
    }

    // endregion

    // region ---- bonus refuse button action
    @action
    showRefuseModal(e: React.MouseEvent<HTMLElement>) {
        const code = e.currentTarget.getAttribute('data-value');
        const bonus = this.findBonusByCode(code);

        if (bonus) {
            this.checkNonTakenSpins(code!, () => {
                this.isLoading = true;
                this.bonusToConfirmAction = bonus;
                this.appState.api.cancelBonusPreview(
                    bonus.promoCode,
                    this.onCancelPreview.bind(this, bonus.promoCode),
                );
            });
        }
    }

    @action
    checkNonTakenSpins(code: string, cb: () => void) {
        this.setBonusActionLoading(code, BonusLoadingType.REFUSE);
        this.appState.api.checkNonTakenSpinsGames((msg, act) => this.onCheckNonTakenSpins(msg, act, code, cb))
    }

    @action
    onCheckNonTakenSpins(msg: messages.IServerResponse, act: goapi.IServerActionResponse, code: string, cb: () => void) {
        this.setBonusActionLoading(code, null);
        if (act.games?.checkNonTaken?.pendingGames?.length) {
            const gameIds = act.games?.checkNonTaken?.pendingGames;

            this.nonTakenSpinsGames = gameIds.map((id) => this.appState.games.findGameById(id));
            this.appState.modal.showNonTakenSpinsGames();
        } else {
            cb();
        }
    }

    hideRefuseModal() {
        this.appState.modal.hideModal();
        this.bonusToConfirmAction = null;
    }

    onRefuseBonus(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        e.stopPropagation();

        if (this.bonusToConfirmAction) {
            this.tryRefuseBonus(this.bonusToConfirmAction.promoCode);
        }
    }

    @action
    tryRefuseBonus(code: string) {
        this.setBonusActionLoading(code, BonusLoadingType.REFUSE);
        this.appState.api.cancelBonus(code, this.onTryRefuseBonus.bind(this, code));
    }

    @action
    onTryRefuseBonus(code: string, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.setBonusActionLoading(code, null);

        if (act.bonusAction && act.bonusAction.cancelCode) {
            const result = act.bonusAction.cancelCode.result;

            if (result === bonuses.BonusCancelResponse.Result.SUCCESS) {
                this.tryLoadBonusesData();

                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-notification-bonus-reject-description'),
                    level: NotificationItemLevel.SUCCESS
                });
            } else {
                const bonusActivationErrorToken = getBonusCancelErrorToken(result);
                this.appState.notificator!.addNotification({
                    message: this.appState.t(bonusActivationErrorToken),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            const bonusActivationErrorToken = getBonusCancelErrorToken();
            this.appState.notificator!.addNotification({
                message: this.appState.t(bonusActivationErrorToken),
                level: NotificationItemLevel.ERROR
            });
        }

        this.hideRefuseModal();
    }

    @action
    onCancelPreview(code: string, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.bonusAction && act.bonusAction.previewCancelCode) {
            const result = act.bonusAction.previewCancelCode.result;

            if (result === bonuses.BonusCancelPreviewResponse.Result.SUCCESS && act.bonusAction.previewCancelCode.correction) {
                this.bonusCorrection = act.bonusAction.previewCancelCode.correction;
            }
        }
        this.appState.modal.showConfirmRefuseBonus();
        this.isLoading = false;
    }

    // endregion

    // region ---- utils
    findBonusByCode(code: NString): BonusEntry | null {
        return code ? this.userBonuses.find((b: BonusEntry) => b.promoCode === code) || null : null;
    }

    getActionButtonClasses(code: string, type: BonusLoadingType): string | undefined {
        if (this.isBonusActionLoading(code, type)) {
            return 'btn--loading';
        } else if (this.isAnyActionLoading()) {
            return 'btn--disabled';
        } else if (type === BonusLoadingType.ACCEPT && this.activeBonusesWithWageringTypeExclusive.length) {
            return 'btn--disabled';
        } else {
            return undefined;
        }
    }

    @computed
    get historyTableData() {
        return this.bonusesHistoryPartial.map(function (this: ProfileBonusesActions, bonus: BonusEntry) {
            return [
                {
                    type: 'bonus-date',
                    createdAt: bonus.createdAt,
                    activatedAt: bonus.activatedAt,
                    wageredAt: bonus.isCanceled ? null : bonus.wageredAt,
                    canceledAt: bonus.isCanceled ? bonus.wageredAt : null
                },
                {value: bonus.name, className: 'table-history-bonus-value'},
                {value: bonus.value},
                {type: 'bonus-wager', value: bonus.wager ? bonus.wager.multiplier : null},
                {value: bonus.stateStr},
            ];
        }, this);
    }

    processBonusAmount(bonusCode: bonuses.IBonusCode, bonusType: string | null): string | undefined {
        if (bonusCode.amount?.greaterThan(Long.ZERO) && bonusType !== this.appState.enums.bonusType.WAGER_BONUS) {
            return formatCurrencyAmount(bonusCode.amount, bonusCode.currency, false);
        }
        return undefined;
    }

    processAllowedWithdrawal(bonusCode: bonuses.IBonusCode, bonusType: string | null): string | undefined {
        if (bonusCode.meta?.allowed_withdrawal && bonusType === this.appState.enums.bonusType.LEVER_BONUS_BY_CODE) {
            return formatCurrencyAmount(Long.fromNumber(Number(bonusCode.meta.allowed_withdrawal)), bonusCode.currency, false);
        }
        return undefined;
    }

    processBonusRedeemAmount(bonusCode: bonuses.IBonusCode): string | undefined {
        if (bonusCode.meta) {
            const currency = bonusCode.currency ?? convertStringToLong(bonusCode.meta.currency);
            if (BonusMeta.BONUS_REDEEM_AMOUNT in bonusCode.meta &&
                bonusCode.meta[BonusMeta.BONUS_REDEEM_AMOUNT] !== "null") {
                return bonusCode.meta[BonusMeta.BONUS_REDEEM_AMOUNT];
            }
            if (BonusMeta.WAGER_BONUS_REDEEM_AMOUNT in bonusCode.meta &&
                bonusCode.meta[BonusMeta.WAGER_BONUS_REDEEM_AMOUNT] !== "null") {
                if (Number(bonusCode.meta[BonusMeta.WAGER_BONUS_REDEEM_AMOUNT])) {
                    return formatCurrencyAmount(Long.fromNumber(Number(bonusCode.meta[BonusMeta.WAGER_BONUS_REDEEM_AMOUNT])), currency, false);
                }
                return undefined;
            }
        }
        return undefined;
    }

    processBonusRedeemMultiplier(bonusCode: bonuses.IBonusCode): string | undefined {
        if (bonusCode.meta) {
            if (bonusCode.meta[BonusMeta.CURRENT_REDEEM]) {
                return `X${bonusCode.meta[BonusMeta.CURRENT_REDEEM]}`;
            }
            if (bonusCode.meta.type === CaseType.FREESPINS && !!bonusCode.meta[BonusMeta.WAGER_BONUS_FREE_SPINS_REDEEM]) {
                return bonusCode.meta[BonusMeta.WAGER_BONUS_FREE_SPINS_REDEEM];
            }
            if (BonusMeta.WAGER_BONUS_REDEEM in bonusCode.meta) {
                return `X${bonusCode.meta[BonusMeta.WAGER_BONUS_REDEEM]}`;
            }
        }
        return undefined;
    }

    processBonusFreeSpinsAmount(bonusCode: bonuses.IBonusCode): string | undefined {
        if (bonusCode.meta && (bonusCode.meta[BonusMeta.BONUS_TYPE] !== BonusType.WAGER_BONUS || bonusCode.state !== bonuses.BonusCode.State.BS_WAGERING)) {
            if (bonusCode.meta[BonusMeta.POSSIBLE_FREESPINS_COUNT]) {
                if ([BonusType.FIRST_DEPOSIT_BONUS, BonusType.FOLLOWING_DEPOSITS_BONUS, BonusType.DEPOSIT_BY_CODE].includes(bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType)) {
                    return `${this.appState.t('promo-up-to')} ${bonusCode.meta[BonusMeta.POSSIBLE_FREESPINS_COUNT]}`;
                } else {
                    return bonusCode.meta[BonusMeta.POSSIBLE_FREESPINS_COUNT];
                }
            }
            if (bonusCode.meta.type === CaseType.FREESPINS) {
                if (!!bonusCode.meta[BonusMeta.WAGER_BONUS_FREE_SPINS_MULTIPLIER]) {
                    return bonusCode.meta[BonusMeta.WAGER_BONUS_FREE_SPINS_MULTIPLIER];
                }
                if (BonusMeta.WAGER_BONUS_AMOUNT in bonusCode.meta) {
                    return bonusCode.meta[BonusMeta.WAGER_BONUS_AMOUNT];
                }
            }
        }
        const freeSpinsAmount = bonusCode.freeSpins?.filter(_ => {
            return _.state !== bonuses.FreeSpinsState.OTHER_GAME_SELECTED
        }).reduce((acc, _) => {
            return acc + (_.countInitial?.toNumber() ?? 0);
        }, 0);
        return freeSpinsAmount ? freeSpinsAmount.toString() : undefined;
    }

    processBonusDepositMultiplier(bonusCode: bonuses.IBonusCode): string | undefined {
        if (bonusCode.meta) {
            if (BonusMeta.WAGER_BONUS_AMOUNT in bonusCode.meta && bonusCode.meta.type === CaseType.FUNDS) {
                return `X${bonusCode.meta[BonusMeta.WAGER_BONUS_AMOUNT]}`;
            } else if (!!bonusCode.meta[BonusMeta.BONUS_MAX_POSSIBLE_PERCENT]) {
                if ([BonusType.FIRST_DEPOSIT_BONUS, BonusType.FOLLOWING_DEPOSITS_BONUS, BonusType.DEPOSIT_BY_CODE].includes(bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType)) {
                    return `${this.appState.t('promo-up-to')} ${bonusCode.meta[BonusMeta.BONUS_MAX_POSSIBLE_PERCENT]}%`;
                } else {
                    return `${bonusCode.meta[BonusMeta.BONUS_MAX_POSSIBLE_PERCENT]}%`;
                }
            }
        }
        return undefined;
    }

    processMaxPossibleFixedBonusAmount(bonusCode: bonuses.IBonusCode): [string, string] | undefined {
        if (bonusCode.meta && !!bonusCode.meta[BonusMeta.MAX_POSSIBLE_FIXED_BONUS_AMOUNT]) {
            const maxAmount = bonusCode.meta[BonusMeta.MAX_POSSIBLE_FIXED_BONUS_AMOUNT].split(':');
            if (maxAmount.length === 2) {
                const amount = Long.fromString(maxAmount[0]);
                const currencyId = Long.fromString(maxAmount[1]);
                const currency = this.appState.currencySelectState.currencies?.supported?.find(_ => _.id?.eq(currencyId));
                if (!currency) {
                    console.error("processMaxPossibleFixedBonusAmount: Bonus currency is not available in siteConfig")
                } else {
                    const formattedAmout = formatCurrencyAmount(amount, currency, true);
                    const currencyCode = currency.code ?? "";
                    if (!!bonusCode.meta && [BonusType.FIRST_DEPOSIT_BONUS, BonusType.FOLLOWING_DEPOSITS_BONUS, BonusType.DEPOSIT_BY_CODE].includes(bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType)) {
                        return [`${this.appState.t('promo-up-to')} ${formattedAmout}`, currencyCode]
                    } else {
                        return [formattedAmout, currencyCode];
                    }
                }
            }
        }
        return undefined;
    }

    processMaxPossibleCashbackPercent(bonusCode: bonuses.IBonusCode): string | undefined {
        if (bonusCode.meta && !!bonusCode.meta[BonusMeta.MAX_POSSIBLE_CASHBACK_PERCENT]) {
            return `${this.appState.t('promo-up-to')} ${bonusCode.meta[BonusMeta.MAX_POSSIBLE_CASHBACK_PERCENT]}%`;
        }
        return undefined;
    }

    processManualPromoCode(bonusCode: bonuses.IBonusCode): ManualPromoCode | undefined {
        if (bonusCode.meta && bonusCode.meta[BonusMeta.PUBLIC_PROMO_CODE] === 'true' && !!bonusCode.meta[BonusMeta.PROMO_CODE]) {
            return new ManualPromoCode(this.appState, bonusCode.meta[BonusMeta.PROMO_CODE], bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType);
        }
        return undefined;
    }

    processFreespinsWager(bonusCode: bonuses.IBonusCode): string | undefined {
        if (!!bonusCode.meta && !!bonusCode.meta[BonusMeta.FREESPINS_WAGER] && !!bonusCode.meta[BonusMeta.POSSIBLE_FREESPINS_COUNT]) {
            if ([BonusType.FIRST_DEPOSIT_BONUS, BonusType.FOLLOWING_DEPOSITS_BONUS, BonusType.DEPOSIT_BY_CODE].includes(bonusCode.meta[BonusMeta.BONUS_TYPE] as BonusType)) {
                return `${this.appState.t('promo-up-to')} x${bonusCode.meta[BonusMeta.FREESPINS_WAGER].split('.')[0]}`;
            } else {
                return `x${bonusCode.meta[BonusMeta.FREESPINS_WAGER].split('.')[0]}`;
            }
        }
        return undefined;
    }

    processBonusTags(bonusCode: bonuses.IBonusCode): BonusTags[] | undefined {
        if (!!bonusCode.meta && !!bonusCode.meta[BonusMeta.BONUS_TAGS]) {
            const tags: BonusTags[] = JSON.parse(bonusCode.meta[BonusMeta.BONUS_TAGS]);
            if (tags && Array.isArray(tags)) {
                return tags.length > 0 ? tags : undefined;
            }
        }
        return undefined;
    }

    processGameItem(bonusCode: bonuses.IBonusCode) {
      const bonusType = processString(BonusMeta.BONUS_TYPE, bonusCode.meta, null);
      if ((bonusType === BonusType.FS_BETS_WAGER_REFILLS_BONUS || bonusType === BonusType.FS_BALANCE_WAGER_REFILLS_BONUS) && bonusCode?.meta?.freeGame) {
        return this.appState.games.findGameById(bonusCode.meta?.freeGame);
      }
      if (bonusType === BonusType.WAGER_BONUS && bonusCode?.meta?.gameId) {
        return this.appState.games.findGameById(bonusCode.meta.gameId);
      }
      return undefined;
    }

    processBonusImageUrl(
        bonusCode: bonuses.IBonusCode,
        bonusType: string | null,
    ): {bonusImageUrl: string; isDefaultBonusImage: boolean} {
        let isDefaultBonusImage = false;
        let bonusImageUrl = '';

        if (bonusCode.meta?.[BonusMeta.BANNER_URL]) {
            bonusImageUrl = bonusCode.meta[BonusMeta.BANNER_URL];
        } else if (bonusCode.bannerUrl) {
            bonusImageUrl = bonusCode.bannerUrl;
        } else {
            isDefaultBonusImage = true;
            const bonusImageId = bonusType
                ? ((bonusCode.bonusId
                      ?.split('')
                      .map((_) => _.charCodeAt(0))
                      .reduce((acc, val) => acc + val, 0) ?? 0) %
                      6) +
                  1
                : 1;
            bonusImageUrl = `/images/bonuses/block/bonus${bonusImageId}.jpg`;
        }
        return {bonusImageUrl, isDefaultBonusImage};
    }

    formatMsToTime(value: number | undefined) {
        if (value) {
            return msToTime(value);
        }
        return null;
    }

    @action.bound
    showRules(bonusId: string) {
        if (!this.openedRulesInBonuses.includes(bonusId)) {
            this.openedRulesInBonuses.push(bonusId);
        } else {
            this.hideRules(bonusId);
        }
    }

    @action.bound
    hideRules(bonusId: string) {
        this.openedRulesInBonuses.remove(bonusId);
    }

    @action.bound
    showMoreHistory() {
        this.historyLimit += HISTORY_ITEMS_LIMIT;
    }

    @action.bound
    showFinalBonusInfo() {
        this.appState.finalBonuses.processData(this.codes, this.possibleCodes);
        this.appState.finalBonuses.showDialog();
    }

    @action.bound
    showDailyBonusInfo() {
        this.appState.dailyBonus.processData(this.codes, this.possibleCodes);
        this.appState.dailyBonus.showDialog();
    }

    // endregion

    onBonusNotification() {
        if (this.appState.page === SitePages.PROFILE_BONUSES || this.appState.page === SitePages.BONUSES) {
            this.tryLoadBonusesData();
        }
    }

    onPromoCodeActivation() {
        if (this.appState.page === SitePages.PROFILE_BONUSES || this.appState.page === SitePages.BONUSES) {
            this.tryLoadBonusesData();
        }
    }

    onPromoCodeDecline() {
        if (this.appState.page === SitePages.PROFILE_BONUSES || this.appState.page === SitePages.BONUSES) {
            this.tryLoadBonusesData();
        }
    }

    getBonusBlockWagerTitle = (bonus: BonusEntry) => {
        if (bonus.bonusType && (bonus.bonusType === BonusType.BALANCE_WAGER_BONUS || bonus.bonusType === BonusType.FS_BALANCE_WAGER_REFILLS_BONUS)) {
            return this.appState.t('ui-bonus-redeem');
        }
        return this.appState.t('ui-wager');
    }
}
