import React from 'react';
import {action, computed, autorun, observable, runInAction} from 'mobx';
import {site} from '../../api/proto';
import {ReactEventHandler, SyntheticEvent} from 'react';
import AppState from '../AppState';
import {setPageMeta, scrollToTheTop, getMobileOSType, isSafari, isIPhone} from '../utils/BrowserUtils';
import { SitePages } from '../meta';
import { Slide } from '../components/Slider';

const DEFAULT_TITLE = document.title;
const STORAGE_KEY_IOS_SAFARI_ADD_TO_HOMEPAGE = 'wl:ios-safari-add-to-homepage';

export default class SiteActions {
    @observable public isFavoritesGamesAllowed: boolean = true;
    @observable public isFunAllowed: boolean = true;
    @observable fullscreenGameRef: React.RefObject<HTMLIFrameElement> = React.createRef();
    @observable currentPathName = window.location.pathname;
    @observable isIOSAddToHomescreenActive = false;
    @observable isKioskLobbyShown = true;

    constructor(protected appState: AppState) {

        this.showMobileMenu = this.showMobileMenu.bind(this);
        this.closeMobileMenu = this.closeMobileMenu.bind(this);
        this.doCloseMobileMenu = this.doCloseMobileMenu.bind(this);

        autorun(() => {
            if (!this.isKioskLobbyShown && appState.appSettings.kioskLobby.enabled) {
                appState.router.push(appState.l('/kiosk-lobby'));
            } else if (this.isAccessRestricted && this.appState.page !== SitePages.GENERATED_LOGIN) {
                appState.router.push(appState.l('/login'));
            } else if (this.forcedPhoneConfirmation && this.appState.page !== SitePages.FORCED_PHONE_CONFIRMATION) {
                appState.router.push(appState.l('/forced-phone-confirmation'));
            }
            this.isFavoritesGamesAllowed =
                !this.appState.siteConfig ||
                this.isMenuItemEnabled(
                    'favorites',
                    appState.siteConfig?.gameMenu
                );
        });
    }

    @computed
    get homePageName(): SitePages {
        const { siteConfig } = this.appState;
        if (siteConfig?.mainPageType === site.MainPageType.SPORT_PAGE) {
            return SitePages.SPORT;
        }

        if (siteConfig && siteConfig.mainPageType === site.MainPageType.SP2_PAGE) {
            return SitePages.SP2_PAGE;
        }
        if (siteConfig?.mainPageType === site.MainPageType.PILOT_PAGE) {
            return SitePages.PILOT;
        }
        return SitePages.MAIN;
    }

    @computed
    get fullscreenGameHeight(): number {
        const headerHeight = this.fullscreenGameRef.current ? this.fullscreenGameRef.current.getBoundingClientRect().top : 0;
        if (this.appState.isMobileOnly && this.appState.viewport.height < this.appState.viewport.width) {
            return this.appState.viewport.height;
        }
        return this.appState.viewport.height - headerHeight;
    }

    @action.bound
    openGameIframeInFullscreenOnMobile() {
        if (this.appState.isMobileOnly && !this.appState.userInfo.guest) {
            const { current } = this.fullscreenGameRef;
            if (current) {
                current.requestFullscreen();
            }
        }
    }

    getCountries(): site.ICountryInfo[] {
        if (!this.appState.siteConfig.countries) {
            return [];
        }
        return this.appState.siteConfig.countries!.countries!;
    }

    @computed
    get countries(): site.ICountryInfo[] {
        return this.appState.siteConfig.countries?.countries ?? [];
    }

    @computed
    get bannersForSlider(): Slide[] {
        if (!this.appState.siteConfig.banners) {
            return [];
        }
        return this.appState.siteConfig.banners
            .filter((banner) => {
                if (banner.lang) {
                    return banner.lang === this.appState.language;
                }
                if (banner.disabledLanguages) {
                    return !banner.disabledLanguages.includes(this.appState.language);
                }
                return true;
            })
            .map((banner) => {
                const isExternalLink = banner.link?.includes('://') ?? false;
                const target = isExternalLink ? '_blank' : undefined;
                return {image: banner.href ?? undefined, href: banner.link ?? undefined, target};
            });
    }

    getDefaultCountry(): string | null {
        if (!this.appState.siteConfig.countries) {
            return null;
        }
        return this.appState.siteConfig.countries!.defaultCountry!;
    }

    getTitle(path: string): string {
        const seo = this.appState.siteConfig ? this.appState.siteConfig.seo : null;
        if (seo) {
            const title = seo[path] ? seo[path].title : null;
            const result = title ? title[this.appState.language] : null;
            return result ? this.appState.t(result, this.getSeoMetaData()) : DEFAULT_TITLE;
        } else {
            return DEFAULT_TITLE;
        }
    }

    getDescription(path: string): string {
        const seo = this.appState.siteConfig ? this.appState.siteConfig.seo : null;
        if (seo) {
            const description = seo[path] ? seo[path].description : null;
            const result = description ? description[this.appState.language] : null;
            return result ? this.appState.t(result, this.getSeoMetaData()) : '';
        } else {
            return '';
        }
    }

    get siteName() {
        return this.appState.g('%SITE_NAME%');
    }

    get siteLogo() {
        return this.appState.g('%LOGO%');
    }

    get siteDomain() {
        return this.appState.g('%DOMAIN%');
    }

    getSeoMetaData() {
        return {
            'site-name': this.siteName,
            'domain': this.siteDomain,
            'top-five-popular-games': this.appState.siteConfig.games!
                .slice(0, 5)
                .map(g => g.name![this.appState.language])
                .join(', '),
        };
    }

    onPageOpen(path: string) {
        // set page meta
        this.setPageMetaByPath(path);
        const canonical = this.buildCanonicalUrl(path);
        document.querySelector('link[rel=\'canonical\']')!.setAttribute('href', canonical);

        // build alternate links
        const languages = this.appState.siteConfig ? this.appState.siteConfig.languages : null;
        if (languages) {
            const alternate = document.querySelectorAll('link[rel=\'alternate\']');
            const alternateCount = Math.max(alternate.length, languages.length);

            for (let i = 0; i < alternateCount; i++) {
                let alternateLink = alternate[i];
                let lang = languages[i];

                if (!alternateLink) {
                    alternateLink = document.createElement('link');
                    document.head.appendChild(alternateLink);
                }
                alternateLink.setAttribute('rel', 'alternate');
                alternateLink.setAttribute('href', this.buildCanonicalUrl(path, lang.code!));
                alternateLink.setAttribute('hreflang', lang.code!);
            }
        }
    }

    setPageMetaByPath(path: string) {
        const title = this.getTitle(path);
        const description = this.getDescription(path);
        setPageMeta(this.appState, title, description);
    }

    buildCanonicalUrl(path: string, lang?: string) {
        const canonicalPath = (path === 'index' || path === 'main') ?
            '/' :
            '/' + (path.endsWith('/') ? path.substr(0, path.length - 1) : path);
        const lCanonicalPath = this.appState.l(canonicalPath, lang);
        return lCanonicalPath !== '/' ?
            `https://${window.location.host}${lCanonicalPath}` :
            `https://${window.location.host}`;
    }

    @action
    changeLanguage(content: AppState, language: string): ReactEventHandler<HTMLElement> {
        return (event) => {
            event.preventDefault();
            const href = event.currentTarget.getAttribute('href')!;
            content.api.changeLanguage(language, () => {
                window.location.href = href;
            });
        };
    }

    supportBtnClick = (event: Event) => {
        event.preventDefault();
        this.appState.chat.expand();
    }

    reloadPage(event?: Event): void {
        if (event) {
            event.preventDefault();
        }
        window.location.reload();
    }

    topMenuItemClick(content: AppState, item: site.IMenuItem): ReactEventHandler<HTMLElement> {
        return (event) => {
            if (item.id === 'support') {
                event.preventDefault();
                this.appState.chat.expand();
            } else if (item.id === 'logout') {
                this.appState.user.tryLogout(event);
            }
        };
    }

    isTopMenuActive(entry: site.IMenuItem) {
        const path = window.location.pathname;
        const url = entry.url;
        let result = false;
        if (url) {
            if (url === '/') {
                result = path === this.appState.l('/') || path === '/';
                if (this.appState.siteConfig.mainPageType === site.MainPageType.DEFAULT_PAGE) {
                    result = path.startsWith(this.appState.l('/games'));
                }
            } else {
                result = path.startsWith(this.appState.l(url));
            }
        }
        return result;
    }

    mobileMenuItemClick(item?: site.IMenuItem): ReactEventHandler<HTMLElement> {
        return (event) => {
            if (item) {
                if (item.id === 'support') {
                    event.preventDefault();
                    this.appState.chat.expand();
                } else if (item.id === 'logout') {
                    this.appState.user.tryLogout(event);
                } else if (item.id === SitePages.PILOT) {
                    window.setTimeout(this.openGameIframeInFullscreenOnMobile, 0);
                }
            }

            this.doCloseMobileMenu();
        };
    }

    @action
    showMobileMenu(event: SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        if ((event?.target as any)?.className?.search('header-top-line__menu-button__tournaments') === -1) {
            this.appState.mobileMenuVisible = true;
        }
    }

    @action
    doCloseMobileMenu() {
        this.appState.mobileMenuVisible = false;
    }

    closeMobileMenu(event: SyntheticEvent<HTMLElement>) {
        this.doCloseMobileMenu();
    }

    @action.bound
    closeMobileMenuAndShowLoginModal(event: React.MouseEvent<HTMLElement>) {
        this.doCloseMobileMenu();
        this.appState.modal.showLogin(event);
    }

    shouldRenderTopMenu(entry: site.IMenuItem) {
        if (entry) {
            // name not present
            if (!entry.name || !(entry.name[this.appState.language] || entry.id)) {
                return false;
            }

            // entries that should not be displayed for guests
            if (this.appState.userInfo.guest && (entry.id === 'payments' || entry.id === 'withdraw' || entry.id === 'logout')) {
                return false;
            }

            // entries that should be displayed for site registered users only
            if (!this.appState.user.isSiteRegistered && (entry.id === 'payments' || entry.id === 'withdraw')) {
                return false;
            }

            // entries that should not be displayed for tag
            if (entry.id === 'logout' && !this.appState.logoutAvailable) {
                return false;
            }

            if (entry.id === 'android' && (getMobileOSType() === 'iOS' || this.appState.isApk)) {
                return false
            }

            return true;
        } else {
            return false;
        }
    }

    isNewTopMenuEntry(entry: site.IMenuItem): boolean {
        const mainMenu = this.appState.appSettings.mainMenu;
        return !!(entry.id && mainMenu && mainMenu.newEntries && mainMenu.newEntries.indexOf(entry.id) !== -1);
    }

    @computed
    get gamesCategoryTitle(): string | null {
        const id = this.appState.routeParams ? this.appState.routeParams.id : null;
        if (id) {
            const term = `games-${id}-h1`;
            const title = this.appState.t(term, this.getSeoMetaData());
            return term !== title ? title : null;
        }
        return null;
    }

    @computed
    get gamesCategoryDescription() {
        const id = this.appState.routeParams ? this.appState.routeParams.id : null;
        if (id) {
            const term = `games-${id}-seotext`;
            const desc = this.appState.t(term, this.getSeoMetaData());
            return term !== desc ? desc : null;
        }
        return null;
    }

    @computed
    get gamesCategoryPlayOtherText() {
        const id = this.appState.routeParams ? this.appState.routeParams.id : null;
        if (id) {
            const term = `ui-games-${id}-list-play-other`;
            const desc = this.appState.t(term, this.getSeoMetaData());
            return term !== desc ? desc : null;
        }
        return null;
    }

    @action.bound
    scrollToTheTop() {
        scrollToTheTop();
    }

    getgetGamesCategoryTitle(): string | null {
        const id = this.appState.routeParams ? this.appState.routeParams.id : null;
        if (id) {
            const term = 'seo-games-' + id + '-title';
            const title = this.appState.t(term, this.getSeoMetaData());
            return title ? title.split(' - ')[1] : null;
        }
        return null;
    }

    getGamesCategoryDescription() {
        const id = this.appState.routeParams ? this.appState.routeParams.id : null;
        if (id) {
            const term = 'seo-games-' + id + '-description';
            return this.appState.t(term, this.getSeoMetaData());
        }
        return null;
    }

    @computed
    get hasRequiredUserFields() {
        const hasUserFields = this.appState.siteConfig &&
            this.appState.siteConfig.userFields &&
            this.appState.siteConfig.userFields.length;
        if (hasUserFields) {
            for (let i = 0, count = this.appState.siteConfig.userFields!.length; i < count; i++) {
                if (this.appState.siteConfig.userFields![i].required) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    @computed
    get isAuthorized(): boolean {
        return this.appState.userInfo && !this.appState.userInfo.guest;
    }

    @computed
    get isElectronicPaymentsAllowed(): boolean {
        return this.appState.user.isSiteRegistered;
    }

    @computed
    get isProfileEditAllowed(): boolean {
        const siteConfig = this.appState.siteConfig;
        const isAllowed = !siteConfig || !!(siteConfig.userProfile && siteConfig.userProfile.editProfile);
        return this.isAuthorized && isAllowed;
    }

    @computed
    get isAffiliateProgramAllowed(): boolean {
        const siteConfig = this.appState.siteConfig;
        const isAllowed = !siteConfig || !!(siteConfig.userProfile && siteConfig.userProfile.affiliateProgram);
        return this.isAuthorized && isAllowed;
    }

    @computed
    get isProfileBonusesAllowed(): boolean {
        const siteConfig = this.appState.siteConfig;
        const isAllowed = !siteConfig || !!(siteConfig.userProfile && siteConfig.userProfile.bonuses);
        return this.isAuthorized && isAllowed;
    }

    @computed
    get isRefSystemAllowed(): boolean {
        const siteConfig = this.appState.siteConfig;
        const isAllowed = !siteConfig || !!(siteConfig.userProfile && siteConfig.userProfile.refSystem);
        return this.isAuthorized && isAllowed;
    }

    @computed
    get isSportEnabled(): boolean {
        return this.appState.mainMenu.itemsFlattened?.length ? this.appState.mainMenu.itemsFlattened.some(_ => _.id === 'sport') : true;
    }

    @computed
    get isLobbyEnabled(): boolean {
        return this.appState.mainMenu.itemsFlattened.some(_ => _.id === 'lobby');
    }

    @computed
    get isWeblimaMode(): boolean {
        return !!(
            this.appState.siteConfig &&
            (this.appState.siteConfig.allowGeneratedLogin ||
                this.appState.siteConfig.siteType === site.SiteType.WEBLIMA ||
                this.appState.siteConfig.siteType === site.SiteType.CLOSED_WEBLIMA)
        );
    }

    @computed
    get isClosedWeblimaMode(): boolean {
        return !!(
            this.appState.siteConfig &&
            this.appState.siteConfig.siteType === site.SiteType.CLOSED_WEBLIMA
        );
    }

    @computed
    get isWlAndWeblimaMode(): boolean {
        return !!(
            this.appState.siteConfig &&
            this.appState.siteConfig.siteType === site.SiteType.WL_AND_WEBLIMA
        );
    }

    @computed
    get isAccessRestricted() {
        return (this.isClosedWeblimaMode && this.appState.userInfo.guest)
         || (this.appState.appSettings.onlyAuthUserAccess && this.appState.userInfo.guest);
    }

    @computed
    get hidePhoneCodeSelectWhenSingleCountry(): boolean {
        return !!this.appState.appSettings.hidePhoneCodeSelectWhenSingleCountry && this.countries.length === 1;
    }

    @computed
    get showInvitationBonusesInfoInBonuses(): boolean {
        return !!this.appState.appSettings.showInvitationBonusesInfoInBonuses;
    }

    @computed
    get isInvitedUser(): boolean {
        return !!(this.appState.rawUserInfo?.tags && this.appState.rawUserInfo?.tags.includes('ref_system_invitee_force_confirm'));
    }

    @computed
    get isPhoneConfirmed(): boolean {
        return !!this.appState.rawUserInfo?.phoneConfirmed;
    }

    @computed
    get forcedPhoneConfirmation(): boolean {
        return this.isRefSystemAllowed && this.isInvitedUser && !this.isPhoneConfirmed;
    }

    isMenuItemEnabled(id: string, menu: (site.IMenuItem | site.IGameMenuEntry)[] | null | undefined): boolean {
        if (menu) {
            return !!menu.find((m: site.IMenuItem | site.IGameMenuEntry) => m.id === id);
        } else {
            return false;
        }
    }

    getGameItemFavoriteClasses(isFavoriteActionLoading: boolean, isMobile: boolean): String {
        if (isFavoriteActionLoading || isMobile) {
            const result = [];
            if (isFavoriteActionLoading) {
                result.push('game-item__favorites--loading');
            }
            if (isMobile) {
                result.push('game-item__favorites--mobile');
            }
            return result.join(' ');
        } else {
            return '';
        }
    }

    @computed
    get showSeoText(): boolean {
        const isMainPage = this.appState.page === SitePages.MAIN;
        const groupId = this.appState.games.groupId;
        return isMainPage && !groupId;
    }

    @action.bound
    changeUserCurrency(currencyId: Long, callback: Function) {
        this.appState.api.changeCurrency(currencyId, (msg, act) => {
            this.appState.processApi(msg, act);
            callback();
        });
    }

    @action
    hideFunCurrency(callback: Function) {
        const { userInfo, currencies } = this.appState;
        if (!userInfo.guest) {
            if (!userInfo.currency || currencies.isFunCurrency(userInfo.currency.id!)) {
                const realCurrency = (userInfo.balances || []).find(b => !currencies.isFunCurrency(b.currencyId!));
                if (realCurrency) {
                    this.appState.api.changeCurrency(realCurrency.currencyId!, (msg, act) => {
                        this.appState.processApi(msg, act);
                        callback();
                    });
                }
            } else {
                callback();
            }
            this.isFunAllowed = false;
        } else {
            callback();
        }
    }

    @action
    showFunCurrency() {
        this.isFunAllowed = true;
    }

    @action.bound
    showChat(event: Event) {
        event.preventDefault();
        this.appState.chat.expand();
    }

    reloadStateForNewSession() {
        this.appState.userBonusesStore.resetBonuses();
        this.appState.withdrawalStore.reloadPayways();
        this.appState.refillStore.reloadPayways();
    }

    @action.bound
    updateCurrentPathName(pathname: string) {
        this.currentPathName = pathname;
    }

    showIOSAddToHomescreenBlockIfPossible = async () => {
        const navigator = window.navigator as unknown;
        const isStandalone = (navigator as {standalone: boolean}).standalone;
        if (
            !!this.appState.appSettings.iOSSafariAddToHomeScreen &&
            isIPhone() &&
            isSafari &&
            !isStandalone
        ) {
            const userIdString = this.appState.userInfo.userId.toString();
            const storedUsers =
                (await window.localStorage.getItem(STORAGE_KEY_IOS_SAFARI_ADD_TO_HOMEPAGE)) ?? '{}';
            const users: Record<string, Date> = JSON.parse(storedUsers);
            if (!(userIdString in users)) {
                runInAction(() => {
                    this.isIOSAddToHomescreenActive = true;
                });
                window.addEventListener(
                    'visibilitychange',
                    this.handleVisibilityChangeForIOSAddToHomescreenBlock,
                    false,
                );
                users[userIdString] = new Date();
                await window.localStorage.setItem(
                    STORAGE_KEY_IOS_SAFARI_ADD_TO_HOMEPAGE,
                    JSON.stringify(users),
                );
            }
        }
    };

    @action.bound
    handleVisibilityChangeForIOSAddToHomescreenBlock() {
        if (document.hidden) {
            this.hideIOSAddToHomescreenBlock()
        }
    }

    @action.bound
    hideIOSAddToHomescreenBlock() {
        window.removeEventListener('load', this.handleVisibilityChangeForIOSAddToHomescreenBlock);
        this.isIOSAddToHomescreenActive = false;
    }

    @computed
    get infoSliderElementsCounter() {
        let counter = 0;
        if (!this.infoSliderShowOnlyTournaments && this.appState.timelineCashbackBonus.visibleOnMainPage &&
            this.appState.timelineCashbackBonus.settings &&
            this.appState.timelineCashbackBonus.settings.levels &&
            this.appState.timelineCashbackBonus.settings.currentLevelIndex !== undefined) {
            counter++;
        }
        if (this.appState.tournamentsInfoActions.tournamentsList && this.appState.tournamentsInfoActions.tournamentsList.length) {
            counter += this.appState.tournamentsInfoActions.tournamentsList.length;
        }
        return counter;
    }

    @computed
    get infoSliderShowOnlyTournaments() {
        return this.appState.page === SitePages.TOURNAMENTS_INFO_LIST_PAGE;
    }

    @computed
    get mobileMenuButtonClasses() {
        let classNames: string[] = [];
        if (this.appState.tournamentsInfoActions.tournaments?.length) {
            classNames.push('tournaments-counter')
        }
        return classNames.join(' ');
    }

    get apkLink(): string {
        if (!this.appState.isApk && this.appState.siteConfig?.apps?.length) {
            return this.appState.siteConfig?.apps[0];
        }
        return '';
    }

    get appInfoPageIsAllowed(): boolean {
        if (this.appState.mainMenu.items.find(item => item.url === '/app-info')) {
            return true;
        }
        return false;
    }
}
