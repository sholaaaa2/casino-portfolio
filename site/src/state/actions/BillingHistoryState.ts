import * as currencies from '../utils/Currencies';
import AppState from '../AppState';
import {action, computed, observable} from 'mobx';
import {formatCurrencyAmount} from '../utils/Money';
import Long from 'long';
import {messages, wallet} from '../../api/proto';
import {formatErrorStatus} from '../../api/formatErrorStatus';
import {Modal} from '../meta';
import {NotificationItemLevel} from '../components/Notificator';
import { PaywayFileUpload } from '../payment-systems/PaywayFileUpload';

export class UserBillingFormattedOperation {
    id?: (string | null);
    currency?: string;
    refill?: (wallet.IUserBillingRefillOperation|null);
    jackpot?: (wallet.IUserBillingJackpotOperation | null);
    @observable withdrawal?: (wallet.IUserBillingWithdrawOperation | null);
    credentials?: string | null;
    withdrawByAdmin?: boolean;
    royalty?: string;
    status?: string;
    stamp: number = 0;
    minusAmount?: (Long|null);
    formattedAmount?: string;
    sourceAmount?: Long;
    admin?: (wallet.IUserBillingAdminOperation | null);
    @observable.shallow fileUpload?: PaywayFileUpload;
    @observable isFilesUploading = false;
    @observable isDetailsShown = false;

    constructor(private appState: AppState, private billingHistoryState: BillingHistoryState, operation: wallet.IUserBillingOperation) {
        if (operation.refill?.provider === wallet.WithdrawProvider.WG_PAYWAYS) {
            this.fileUpload = new PaywayFileUpload();
        }
    }

    @computed
    get statusText(): string | null {
        if (this.refill) {
            if (this.refill.isBonus) {
                return this.appState.t('ui-bonus');
            } else {
                switch (this.refill.status) {
                    case wallet.UserBillingRefillOperation.RefillStatus.PROCESSING:
                        return this.appState.t('ui-refill-status-processing');
                    case wallet.UserBillingRefillOperation.RefillStatus.SUCCESS:
                        return this.appState.t('ui-refill-status-success');
                    case wallet.UserBillingRefillOperation.RefillStatus.FAILED:
                        return this.appState.t('ui-refill-status-cancelled');
                    default:
                        return this.appState.t('ui-refill-status-unknown');
                }
            }
        } else if (this.jackpot) {
            return this.appState.t('ui-jackpot');
        }

        if (this.withdrawal && this.withdrawal.status !== undefined) {
            switch (this.withdrawal.status) {
                case wallet.UserBillingWithdrawOperation.WithdrawalStatus.FAILED:
                    return this.appState.t('ui-withdrawal-status-cancelled');
                case wallet.UserBillingWithdrawOperation.WithdrawalStatus.ROLLBACK:
                    return this.appState.t('ui-withdrawal-status-rollback');
                case wallet.UserBillingWithdrawOperation.WithdrawalStatus.SUCCESS:
                    return this.appState.t('ui-withdrawal-status-success');
                case wallet.UserBillingWithdrawOperation.WithdrawalStatus.WAITING_FOR_APPROVAL:
                    return this.appState.t('ui-withdrawal-status-waiting-for-approval');
                case wallet.UserBillingWithdrawOperation.WithdrawalStatus.WAITING_FOR_PROCESSING:
                    return this.appState.t('ui-withdrawal-status-waiting-for-processing');
                default:
                    return this.appState.t('ui-withdrawal-status-unknown');
            }
        } else if (this.admin) {
            if (this.minusAmount && this.minusAmount.notEquals(Long.ZERO)) {
                return this.appState.t('ui-by-administrator');
            } else {
                return this.appState.t('ui-by-administrator');
            }
        }

        return null;
    };

    @computed
    get revokeAvailable(): boolean {
        const allowedByServer = this.withdrawal?.revokeAvailable === true;
        if (allowedByServer) {
            return !this.withdrawal?.status;
        } else  {
            return false;
        }
    };

    @computed
    get isCashierCommentShown(): boolean {
        return (
            (this.refill?.status === wallet.UserBillingRefillOperation.RefillStatus.FAILED &&
                this.refill.provider === wallet.WithdrawProvider.WG_PAYWAYS &&
                !!this.refill?.comment) ||
            (this.withdrawal?.status ===
                wallet.UserBillingWithdrawOperation.WithdrawalStatus.FAILED &&
                this.withdrawal.provider === wallet.WithdrawProvider.WG_PAYWAYS &&
                !!this.withdrawal?.comment)
        );
    }

    @computed
    get attachmentsByCashier(): wallet.IAttachedData[] {
        if (this.refill) {
            return this.refill?.attaches?.filter(_ => !_.uploadByPlayer) ?? [];
        } else {
            return this.withdrawal?.attaches?.filter(_ => !_.uploadByPlayer) ?? [];
        }
    }

    @computed
    get attachmentsByPlayer(): wallet.IAttachedData[] {
        if (this.refill) {
            return this.refill?.attaches?.filter(_ => _.uploadByPlayer) ?? [];
        } else {
            return this.withdrawal?.attaches?.filter(_ => _.uploadByPlayer) ?? [];
        }
    }

    @computed
    get isAdditionalAttachmentsRequired(): boolean {
        return this.refill?.status === wallet.UserBillingRefillOperation.RefillStatus.FAILED &&
            this.refill.provider === wallet.WithdrawProvider.WG_PAYWAYS &&
            this.fileUpload !== undefined;
    }

    @computed
    get cardNumberWithCredentials() {
        if (this.withdrawal?.provider && this.withdrawal.provider === wallet.WithdrawProvider.PINSALE) {
            const pinNumSearch = this.withdrawal.credentials ? /\d+/.exec(this.withdrawal.credentials) : '';
            const pinNum = pinNumSearch && pinNumSearch[0] ? pinNumSearch[0].match(/\d{3}(?=\d{2,3})|\d+/g)?.join("-") : '';
            return `${this.withdrawal.payway || ''}(Pin: <span class="pin-sale-number">${pinNum}</span>)`
        }
        return this.appState.t('ui-withdrawal-info', this.withdrawal || {})
    }

    reloadHistory = async () => {
        this.billingHistoryState.loadHistory();
    };

    showRevokeConfirmation = () => {
        this.appState.modal.show(Modal.REVOKE_WITHDRAWAL_CONFIRMATION, null, {
            actionCallback: this.revoke,
            closeCallback: this.reloadHistory,
            data: this,
        });
    };

    @observable isLoading: boolean = false;

    @action setLoading = (loading: boolean) => {
        this.isLoading = loading;
    };

    revoke = () => {
        if (!this.id || this.isLoading) {
            return;
        }
        this.setLoading(true);
        this.appState.api.revokeWithdrawal(this.id, this.processRevokeRes);
    };

    processRevokeRes = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        this.setLoading(false);
        this.appState.processApi(msg, act);
        if (msg.errorStatus) {
            const error = formatErrorStatus(msg.errorStatus);
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.ERROR,
                message: error,
            });
            this.reloadHistory();
        } else {
            if (this.withdrawal) {
                this.withdrawal.status = wallet.UserBillingWithdrawOperation.WithdrawalStatus.FAILED;
            }

            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-withdrawal-canceled-successfully'),
            });
        }

        this.appState.modal.hideModal();
    };

    @action.bound
    uploadFiles() {
        if (this.fileUpload && this.id) {
            this.isFilesUploading = true;
            const attaches: wallet.IAttachDataRequest[] = this.fileUpload.preparedFiles.map(
                (file) => ({
                    bytes: file.bytes,
                    fileExtension: file.name.substring(file.name.lastIndexOf('.') + 1),
                }),
            );
            this.appState.api.uploadAttachmentsForPayway(
                this.id,
                attaches,
                (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                    this.isFilesUploading = false;
                    if (act.attachBillingData?.ok) {
                        if (this.refill) {
                            this.refill.status = wallet.UserBillingRefillOperation.RefillStatus.PROCESSING;
                        }
                        this.fileUpload = undefined;
                        this.billingHistoryState.loadHistory();
                    }
                }
            );
        }
    }

    @action.bound
    showDetails() {
        this.isDetailsShown = true;
    }
}

export default class BillingHistoryState {
    @observable loading: boolean = false;

    @action setLoading = (loading: boolean) => {
        this.loading = loading;
    };

    @observable refills: UserBillingFormattedOperation[] = [];
    @observable withdrawals: UserBillingFormattedOperation[] = [];

    private appState: AppState;

    constructor(appState: AppState) {
        this.appState = appState;
    }

    loadHistory = () => {
        this.setLoading(true);
        this.appState.api.getBillingHistory(this.processHistoryRes);
    }

    @action.bound
    processHistoryRes(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.setLoading(false);
        this.appState.processApi(msg, act);

        const ops = act.billingHistory?.operations?.map(op => {
            const res = new UserBillingFormattedOperation(this.appState, this, op);
            res.id = op.id

            let { amount, currency } = op;
            if (currency === null) {
                currency = Long.ZERO;
            }

            let currencyInfo = currencies.findCurrencyById(currency);
            res.formattedAmount = formatCurrencyAmount(amount, currencyInfo);
            res.sourceAmount = amount!;
            res.currency = currencyInfo!.code!;
            res.stamp = op.stamp!.toNumber();
            res.admin = op.admin;
            res.minusAmount = op.minusAmount;

            if (op.refill) {
                res.refill = op.refill;
                res.credentials = op.refill.credentials!;
            } else if (op.jackpot) {
                res.jackpot = op.jackpot;
            }

            if (op.withdrawal && op.withdrawal.status !== undefined) {
                if (op.withdrawal.royaltyAmount && op.withdrawal.royaltyAmount.notEquals(Long.ZERO) && op.withdrawal.royaltyCurrency) {
                    res.royalty = formatCurrencyAmount(op.withdrawal.royaltyAmount, op.withdrawal.royaltyCurrency, false);
                }
                res.withdrawal = op.withdrawal;
            } else if (op.admin) {
                if (op.minusAmount && op.minusAmount.notEquals(Long.ZERO)) {
                    res.formattedAmount = formatCurrencyAmount(op.minusAmount, currencyInfo);
                }
            } else {
                res.withdrawal = undefined;
            }
            return res;
        }) || [];

        this.appState.billingHistory.refills = ops
            .filter(op => !op.withdrawal && !op.withdrawByAdmin)
            .sort((a, b) => b.stamp - a.stamp);
        this.appState.billingHistory.withdrawals = ops
            .filter(op => op.withdrawal || op.withdrawByAdmin)
            .sort((a, b) => b.stamp - a.stamp);
    };
}
