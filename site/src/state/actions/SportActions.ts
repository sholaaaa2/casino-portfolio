import {action, observable} from 'mobx';
import AppState from '../AppState';
import {BTC_CURRENCY, isCryptoCurrencyId} from '../utils/Currencies';

export default class SportActions {
    @observable isLoading: boolean = false;
    scriptLoaded: boolean = false;
    mobileFrameLoaded: boolean = false;

    constructor(private appState: AppState) {
    }

    @action
    setIsLoading(value: boolean) {

        this.isLoading = value;
    }

    isCurrencySupported() {
        // fun, all fiat currencies and BTC currency is supported
        const currency = this.appState.user.getRealBalanceCurrencyId();
        return !currency || !isCryptoCurrencyId(currency) || currency.equals(BTC_CURRENCY);
    }
}
