import * as React from "react";
import { action, observable, computed } from "mobx";
import AppState from "../AppState";
import { messages, users } from "../../api/proto";

interface IFileError {
    fileName?: string;
    status: users.UserDocumentsResponse.Status;
}

enum DOCUMENTS_TABS {
    REQUIRED = 'required',
    OPTIONAL = 'optional',
    HISTORY = 'history',
}

export class ProfileDocumentsPreparedFile {
    constructor(
        private docType: ProfileDocumentsType,
        public info: users.UserDocumentsRequest.IFile
    ) {}

    @action.bound
    remove() {
        this.docType.preparedFiles.remove(this);
    }
}

class ProfileDocumentsType {
    private appState: AppState;
    private readonly MAX_DOCUMENTS_NUMBER = 5;
    readonly id: number;
    readonly name: string;
    fileInputRef = React.createRef();
    isNumberExceededErrorShown: boolean = false;

    @observable filesToUpload?: FileList;
    @observable preparedFiles = observable<ProfileDocumentsPreparedFile>([]);
    @observable isDraggingFiles: boolean = false;
    @observable isSecondStep: boolean = false;
    @observable isUploading: boolean = false;
    @observable errors: IFileError[] = [];
    @observable isSuccessUpload: boolean = false;

    constructor(appState: AppState, id: number, name: string) {
        this.appState = appState;
        this.id = id;
        this.name = name;
    }

    @computed
    get isRequired(): boolean {
        return (
            !(
                this.id in this.appState.profileDocuments.documentsByType &&
                this.appState.profileDocuments.documentsByType[this.id].find(
                    doc => doc.status !== users.UserDocumentStatus.UDS_REJECTED
                )
            ) &&
            this.appState.profileDocuments.requiredDocuments.includes(this.id)
        );
    }

    @computed
    get addButtonClassNames(): string {
        const classNames = ["profile-documents-block__btn--add-more"];
        if (this.preparedFiles.length) {
            classNames.push("btn-secondary");
        } else {
            classNames.push("btn-pr");
        }

        if (this.isUploading) {
            classNames.push("btn-secondary--disabled");
        }
        return classNames.join(" ");
    }

    @computed
    get uploadButtonClassNames(): string {
        const classNames = [];
        if (this.isSecondStep === false || this.preparedFiles.length === 0) {
            classNames.push("btn-secondary btn-secondary--disabled");
        } else {
            classNames.push("btn-pr");
        }

        if (this.isUploading) {
            classNames.push("btn--loading");
        }
        return classNames.join(" ");
    }

    @computed
    get isShown(): boolean {
        return (
            this.appState.profileDocuments.documentsByType[this.id].filter(
                (_) => _.status !== users.UserDocumentStatus.UDS_REJECTED,
            ).length < this.MAX_DOCUMENTS_NUMBER
        );
    }

    @action
    resetState() {
        this.filesToUpload = undefined;
        this.preparedFiles.clear();
        this.isUploading = false;
        this.isSecondStep = false;
    }

    @action.bound
    showFileSelectionDialog() {
        if (this.isUploading === false) {
            const fileInput = this.fileInputRef.current as HTMLInputElement;
            if (fileInput) {
                fileInput.value = "";
                fileInput.click();
            }
        }
    }

    @action.bound
    onDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
    };

    @action.bound
    onDragEnter = (event: React.DragEvent<HTMLDivElement>) => {
        this.isDraggingFiles = true;
    };

    @action.bound
    onDragLeave = (event: React.DragEvent<HTMLDivElement>) => {
        this.isDraggingFiles = false;
    };

    @action.bound
    fileDropHandler(event: React.DragEvent<HTMLDivElement>) {
        event.preventDefault();
        this.processFiles(event.dataTransfer.files);
    }

    @action.bound
    handleSelectedFile(event: React.ChangeEvent<HTMLInputElement>) {
        this.processFiles(event.target.files);
    }

    @action
    processFiles(files: FileList | null) {
        if (files && files.length) {
            this.clearErrors();
            this.filesToUpload = files;
            this.prepareFilesForUpload();
        }
    }

    @action
    validateSelectedFile(file: File): boolean {
        if (file.size > this.appState.profileDocuments.sizeLimit) {
            this.errors.push({
                fileName: file.name,
                status: users.UserDocumentsResponse.Status.FILE_TOO_BIG
            });
            return false;
        } else if (
            !this.appState.profileDocuments.mimeTypes.includes(file.type)
        ) {
            this.errors.push({
                fileName: file.name,
                status: users.UserDocumentsResponse.Status.FAILED
            });
            return false;
        }
        return true;
    }

    @action
    clearErrors() {
        this.errors = [];
    }

    @action.bound
    prepareFilesForUpload() {
        if (this.filesToUpload) {
            this.isSecondStep = true;
            [].forEach.call(this.filesToUpload, (file: File) => {
                const reader = new FileReader();
                reader.onload = this.addPreparedFile.bind(this, file);
                reader.onerror = this.errorUploadingFailed;
                reader.readAsArrayBuffer(file);
            });
        } else {
            this.errorUploadingFailed();
        }
    }

    @action.bound
    addPreparedFile(file: File, event: ProgressEvent<FileReader>) {
        if (event.target && event.target.result) {
            const fileContent = new Uint8Array(
                event.target.result as ArrayBuffer
            );
            if (
                this.validateSelectedFile(file) &&
                !this.preparedFiles.find(
                    preparedFile => preparedFile.info.name === file.name
                )
            ) {
                const preparedFile = new ProfileDocumentsPreparedFile(this, {
                    name: file.name,
                    content: fileContent,
                    type: this.id
                });
                const numberOfFiles =
                    this.preparedFiles.length +
                    this.appState.profileDocuments.documentsByType[this.id]
                        .filter(
                            (_) => _.status !== users.UserDocumentStatus.UDS_REJECTED,
                        )
                        .length;
                if (numberOfFiles < this.MAX_DOCUMENTS_NUMBER) {
                    this.preparedFiles.push(preparedFile);
                } else if (this.isNumberExceededErrorShown === false) {
                    this.errors.push({ status: users.UserDocumentsResponse.Status.NUMBER_EXCEEDED });
                    this.isNumberExceededErrorShown = true;
                }
            }
        } else {
            this.errorUploadingFailed();
        }
    }

    @action.bound
    errorUploadingFailed() {
        this.isUploading = false;
        this.errors.push({ status: users.UserDocumentsResponse.Status.FAILED });
    }

    @action.bound
    uploadFiles() {
        if (this.isUploading === false && this.preparedFiles.length !== 0) {
            this.isUploading = true;
            this.errors = [];
            this.appState.api.uploadDocuments(
                this.preparedFiles,
                (
                    msg: messages.IServerResponse,
                    act: messages.IClientActionResponse
                ) => {
                    if (act.userDocuments) {
                        if (act.userDocuments.status) {
                            this.processResponseError(act.userDocuments.status);
                        } else {
                            this.appState.profileDocuments.updateDocuments(act);
                            this.resetState();
                            this.isSuccessUpload = true;
                        }
                    } else {
                        this.errorUploadingFailed();
                    }
                }
            );
        }
    }

    @action
    processResponseError(status: users.UserDocumentsResponse.Status) {
        this.isUploading = false;
        this.errors.push({ status });
    }
}

export class ProfileDocumentsActions {
    appState: AppState;
    fileInputRef = React.createRef();
    documentTypesNames: { [type: number]: string } = {};
    @observable documentTypes: ProfileDocumentsType[];
    DOCUMENT_STATUS = users.UserDocumentStatus;

    @observable documents: users.IUserDocument[] = [];
    @observable documentsByType: { [type: number]: users.IUserDocument[] };
    @observable mimeTypes: string[] = [];
    @observable extensions: string[] = [];
    @observable sizeLimit: number = 0;
    @observable requiredDocuments: users.UserDocumentType[] = [];
    @observable isLoading: boolean = true;
    @observable isOptionalOpened: boolean = false;
    @observable selectedTab: DOCUMENTS_TABS = DOCUMENTS_TABS.REQUIRED;
    @observable currentDocTypeFromUrl?: users.UserDocumentType;

    constructor(appState: AppState) {
        this.appState = appState;
        Object.keys(users.UserDocumentType).forEach(key => {
            this.documentTypesNames[users.UserDocumentType[key]] = key;
        });
        this.documentTypes = Object.keys(users.UserDocumentType)
            .filter(key => {
                return (
                    users.UserDocumentType[key] !==
                    users.UserDocumentType.UNKNOWN
                );
            })
            .map(key => {
                return new ProfileDocumentsType(
                    this.appState,
                    users.UserDocumentType[key],
                    key
                );
            });
    }

    @action
    setCurrentDocType = (type?: string) => {
        if (type) {
            this.currentDocTypeFromUrl = users.UserDocumentType[type.toUpperCase()];
            const typeCurrent = this.documentTypes?.find(type => type.id as users.UserDocumentType === this.currentDocTypeFromUrl);
            if (typeCurrent) {
                typeCurrent.isSuccessUpload = false;
            }
        }
    }

    @computed
    get currentDocType(): ProfileDocumentsType | undefined {
        if (this.currentDocTypeFromUrl) {
            return this.documentTypes.find(type => type.id as users.UserDocumentType === this.currentDocTypeFromUrl);
        }
        return this.documentTypes.length > 0 ? this.documentTypes[0] : undefined;
    }

    isActiveTab = (tab: DOCUMENTS_TABS): boolean => {
        return this.selectedTab === tab;
    }

    @action.bound
    changeTab = (tab: DOCUMENTS_TABS) => {
        this.selectedTab = tab;
    }

    @computed
    get sizeLimitInMB(): number {
        return this.sizeLimit / (1024 * 1024);
    }

    @computed
    get extensionsJoined(): string {
        return this.extensions.join(", ").toUpperCase();
    }

    @computed
    get requiredDocumentTypes(): ProfileDocumentsType[] {
        return this.documentTypes.filter(
            docType => docType.isRequired && docType.isShown
        );
    }

    @computed
    get optionalDocumentTypes(): ProfileDocumentsType[] {
        return this.documentTypes.filter(
            docType => docType.isRequired === false && docType.isShown
        );
    }

    getDocNameById = (id?: users.UserDocumentType) => {
        if (id) {
            return users.UserDocumentType[id].toLowerCase();
        }
        return '';
    }

    @action
    updateDocuments(response: messages.IClientActionResponse) {
        this.documentsByType = {};
        Object.keys(users.UserDocumentType).forEach(key => {
            this.documentsByType[users.UserDocumentType[key]] = [];
        });
        if (response.userDocuments && response.userDocuments.documents) {
            this.documents = response.userDocuments.documents.reverse();
            response.userDocuments.documents.reverse().forEach(document => {
                if (document.type) {
                    this.documentsByType[document.type].push(document);
                }
            });
        }
    }

    @action
    loadDocumentsAndRestrictions() {
        this.isLoading = true;
        this.appState.api.fetchUserDocuments(
            (
                msg: messages.IServerResponse,
                act: messages.IClientActionResponse
            ) => {
                if (act.userDocuments && act.userDocuments.limitations) {
                    if (act.userDocuments.limitations.mimeTypes) {
                        this.mimeTypes =
                            act.userDocuments.limitations.mimeTypes;
                    }
                    if (act.userDocuments.limitations.sizeInBytes) {
                        this.sizeLimit = act.userDocuments.limitations.sizeInBytes.toNumber();
                    }
                    if (act.userDocuments.limitations.extensions) {
                        this.extensions =
                            act.userDocuments.limitations.extensions;
                    }
                    if (act.userDocuments.requiredDocuments) {
                        this.requiredDocuments =
                            act.userDocuments.requiredDocuments;
                    }
                    this.updateDocuments(act);
                    this.isLoading = false;
                }
            }
        );
    }

    @action.bound
    toggleOptionalBlocks() {
        this.isOptionalOpened = !this.isOptionalOpened;
    }

    @action.bound
    removeDocument(id: Long) {
        if (
            window.confirm(
                this.appState.t("ui-profile-documents-confirm-document-removal")
            )
        ) {
            this.appState.api.removeDocument(
                id,
                (
                    msg: messages.IServerResponse,
                    act: messages.IClientActionResponse
                ) => {
                    if (act.userDocuments) {
                        if (act.userDocuments.status) {
                            // this.processResponseError(act.userDocuments.status);
                        } else {
                            this.updateDocuments(act);
                        }
                    }
                }
            );
        }
    }

    @action.bound
    handleOfflineEvent() {
        this.appState.modal.showError500();
    }
}
