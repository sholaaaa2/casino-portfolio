import AppState from "../AppState";
import { LOBBY_ITEMS } from "../../lobby";
import { action, computed, observable } from "mobx";
import { Routes } from "../meta";

class LobbyItem {
  id: string;
  name: string;
  icon: string;
  url: string;
  pressTimer: number;
  withLongPress: boolean;
  isPressed = false;

  constructor(
    private appState: AppState,
    data: {
      id: string;
      name: string;
      icon: string;
      url: string;
      withLongPress?: boolean;
    }
  ) {
    this.id = data.id;
    this.name = data.name;
    this.icon = data.icon;
    this.url = data.url;
    this.withLongPress = data.withLongPress ?? false;
  }

  @action.bound
  onPress() {
    this.appState.lobby.startIdleDetection();
    this.isPressed = true;
    if (this.pressTimer) {
      window.clearTimeout(this.pressTimer);
    }
    this.pressTimer = window.setTimeout(() => {
      this.isPressed = false;
      if (this.appState.lobby.pinLength > 0) {
        this.appState.lobby.showPinModal();
      } else {
        this.appState.lobby.proceedToRoot();
      }
    }, this.appState.appSettings.kioskLobby.longPressTimeoutSeconds * 1000);
  }

  @action.bound
  onRelease() {
    if (this.isPressed) {
      document.location.href = this.url;
      window.clearTimeout(this.pressTimer);
      this.isPressed = false;
    }
  }
}

export class LobbyState {
  @observable pin = "";
  @observable isPinModalActive = false;
  items: LobbyItem[] = [];
  isIdleDetectorRunning = false;

  constructor(private appState: AppState) {
    this.items = LOBBY_ITEMS.map((_) => new LobbyItem(appState, _));
  }

  @computed
  get kioskPin(): string {
    return this.appState.siteConfig.clientSettings?.kioskPin ?? "";
  }

  @computed
  get pinLength(): number {
    return this.kioskPin.length;
  }

  @computed
  get pinStars(): string[] {
    return this.pin
      .replace(/./g, "*")
      .split("")
      .concat(new Array(this.pinLength - this.pin.length).fill(""));
  }

  startIdleDetection = async () => {
    if (!this.isIdleDetectorRunning) {
      try {
        await window.IdleDetector.requestPermission();
      } catch(error) {
        console.error(error);
      }
      try {
        const controller = new AbortController();
        const signal = controller.signal;
        const idleDetector = new window.IdleDetector();
        idleDetector.addEventListener("change", () => {
          if (idleDetector.userState === "idle") {
            this.appState.user.backToLobby();
          }
        });

        await idleDetector.start({
          threshold:
            this.appState.appSettings.kioskLobby.idleTimeoutSeconds * 1000,
          signal,
        });
        this.isIdleDetectorRunning = true;
      } catch (error) {
        console.error(error);
      }
    }
  };

  @action.bound
  showPinModal() {
    this.isPinModalActive = true;
  }

  @action.bound
  hidePinModal() {
    this.pin = "";
    this.isPinModalActive = false;
  }

  @action.bound
  proceedToRoot() {
    this.appState.site.isKioskLobbyShown = true;
    this.appState.router.push(Routes.ROOT);
  }

  @action.bound
  onPinInput(pin: string) {
    if (this.pin.length < this.pinLength) {
      this.pin += pin;
    }
    if (this.pin === this.kioskPin) {
      this.hidePinModal();
      this.proceedToRoot();
    }
  }

  @action.bound
  onPinBackspace() {
    this.pin = this.pin.slice(0, -1);
  }

  @action.bound
  onPinClear() {
    this.pin = "";
  }
}
