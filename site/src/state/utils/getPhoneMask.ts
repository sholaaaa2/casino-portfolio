import {site} from '../../api/proto';

export const getPhoneMask = (
    countryCode: string,
    countries: site.ICountryInfo[],
): string | undefined => {
    return (
        countries
            .find((_) => _.code === countryCode)
            ?.mask?.split('9')
            .join('#') ?? undefined
    );
};
