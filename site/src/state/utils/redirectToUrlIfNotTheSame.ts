export const redirectToUrlIfNotTheSame = (url: string): boolean => {
    const redirectUrl = new URL(url);
    if (redirectUrl.hostname !== window.location.hostname) {
        window.location.href = url;
        return true;
    }
    return false;
};
