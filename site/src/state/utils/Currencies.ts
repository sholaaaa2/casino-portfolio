import {commons, site} from '../../api/proto';
import {CurrencySettings, NLong, NString} from '../meta';
import Long from 'long';
import {isDef} from '../../api/utils';

export const FUN_CURRENCY_ID = Long.ZERO;
export const SCORE_CURRENCY_ID = -1;
export const BTC_CURRENCY = 90002;
let currenciesInfo: SiteCurrencies = {supported: [], defaultCurrencyId: Long.ZERO};

/**
 * Overrides commons.ICurrency, marks id, code, format, denomination and spinDenomination as required fields.
 */
export interface Currency extends commons.ICurrency {
    id: Long;
    code: string;
    format: string;
    formattingCode: NString;
    denomination: number;
    spinDenomination: number;
    isCrypto: boolean;
}

/**
 * Overrides site.ISiteCurrencies, marks supported and defaultCurrencyId as required fields.
 */
export interface SiteCurrencies extends site.ISiteCurrencies {
    supported: Currency[];
    defaultCurrencyId: Long;
}

export function setCurrenciesInfo(value: site.ISiteCurrencies, currencySettings?: CurrencySettings) {
    if (value.supported && value.defaultCurrencyId) {
        // set currencies info
        currenciesInfo = {
            supported: filterSupportedCurrencies(value.supported),
            defaultCurrencyId: value.defaultCurrencyId
        };

        // update currencies with settings from AppSettings.json
        if (currencySettings) {
            applyCurrenciesSettings(currencySettings);
        }
    } else {
        console.error('Missing supported or default currency, please, check site config.');
    }
}

export function findCurrencyById(id: NLong): Currency | null {
    return id ? currenciesInfo.supported.find((c: Currency) => c.id.equals(id)) || null : null;
}

export function findCurrencyByCode(code: NString) {
    return code ? currenciesInfo.supported.find((c: Currency) => c.code === code) || null : null;
}

export function isCurrencySupported(id: NLong): boolean {
    return !!findCurrencyById(id);
}

export function getSupportedCurrencies(): Currency[] {
    return currenciesInfo.supported;
}

export function getSupportedNonFunCurrencies(): Currency[] {
    return currenciesInfo.supported.filter((c) => c.id.notEquals(FUN_CURRENCY_ID));
}

export function getSupportedRegistrationCurrencies(): Currency[] {
    return currenciesInfo.supported.filter((c) => c.id.notEquals(FUN_CURRENCY_ID) && !c.blockRefill);
}

export function getDefaultCurrencyId(): Long {
    return currenciesInfo.defaultCurrencyId;
}

export function getDefaultCurrency(): Currency | null {
    return findCurrencyById(getDefaultCurrencyId());
}

export function isCryptoCurrencyId(currencyId: NLong): boolean {
    const currency = findCurrencyById(currencyId);
    return currency ? currency.isCrypto : false;
}

function applyCurrenciesSettings(settings: CurrencySettings) {
    const currencyCodes = Object.keys(settings);
    for (let code of currencyCodes) {
        const currencySettings = settings[code];
        const currency = findCurrencyByCode(code);
        if (currency) {
            if (currencySettings.format) {
                currency.format = currencySettings.format;
            }
            if (currencySettings.code) {
                currency.formattingCode = currencySettings.code;
            }
        } else {
            console.error('Unable to find currency with code: ', code);
        }
    }
}

function filterSupportedCurrencies(currencies: commons.ICurrency[]): Currency[] {
    const result: Currency[] = [];

    for (let currency of currencies) {
        if (currency.id && currency.code && currency.format && isDef(currency.denomination) && isDef(currency.spinDenomination)) {
            const convertedCurrency: Currency = currency as Currency;
            convertedCurrency.isCrypto = currency.isFiat === false && currency.id.compare(FUN_CURRENCY_ID) !== 0;
            result.push(convertedCurrency);
        } else {
            console.error('Missing one of the required params: id, code, format, denomination, spinDenomination in currency: ', currency);
        }
    }

    return result;
}

export function isFunCurrency(currencyId: Long): boolean {
    return currencyId.compare(FUN_CURRENCY_ID) === 0;
}