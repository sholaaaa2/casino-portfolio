import {NLong} from '../meta';

export function calcPercent(val1: NLong, val2: NLong): number {
    const numVal1 = val1 ? val1.toNumber() : 0;
    const numVal2 = val2 ? val2.toNumber() : 0;
    if (numVal2 === 0) {
        return 0;
    } else {
        return 100 - (numVal1 / numVal2 * 100);
    }
}

export function calcWagerMultiplier(wagerLeftInitial: NLong, bonusAmount: NLong): number {
    const numAmount = bonusAmount ? bonusAmount.toNumber() : 0;
    const numWagerLeftInitial = wagerLeftInitial ? wagerLeftInitial.toNumber() : 0;

    if (numAmount !== 0 && numWagerLeftInitial !== 0) {
        return Math.ceil((numWagerLeftInitial / numAmount) * 10) / 10;
    } else {
        return 0;
    }
}

export function maxLongValue(val1: NLong, val2: NLong): NLong {
    if (!val1 && !val2) {
        return null;
    } else if (val1 && !val2) {
        return val1;
    } else if (!val1 && val2) {
        return val2;
    } else {
        if (val1!.toSigned().greaterThan(val2!.toSigned())) {
            return val1;
        } else {
            return val2;
        }
    }
}