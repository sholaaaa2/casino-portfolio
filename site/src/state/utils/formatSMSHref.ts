import { users } from "../../api/proto";

export const formatSMSHref = (
    invite: users.invitations.IUserInvitation
): string => {
    return `sms:${invite.guestPhone?.split(" ").join("")}?body=${escape(
        invite.messageText ?? ""
    )}`;
};
