import {AndroidWebViewMessageType} from '../..';
import AppState from '../AppState';

export const pasteFromClipboard = (appState: AppState): Promise<string> => {
    if (appState.isApk) {
        return androidPasteFromClipboard();
    }
    return navigator.clipboard.readText();
};

const androidPasteFromClipboard = (): Promise<string> => {
    return new Promise((resolve, reject) => {
        if (window.AndroidWebView?.postMessage) {
            const timeout = window.setTimeout(() => {
                window.AndroidWebViewCB[AndroidWebViewMessageType.PASTE_FROM_CLIPBOARD] = undefined;
                reject(new Error('AndroidWebView paste cancelled: time out'));
            }, 1000);
            window.AndroidWebViewCB[AndroidWebViewMessageType.PASTE_FROM_CLIPBOARD] = (
                message: string,
            ) => {
                window.clearTimeout(timeout);
                window.AndroidWebViewCB[AndroidWebViewMessageType.PASTE_FROM_CLIPBOARD] = undefined;
                resolve(message);
            };
            window.AndroidWebView.postMessage(
                JSON.stringify({
                    type: AndroidWebViewMessageType.PASTE_FROM_CLIPBOARD,
                }),
            );
        } else {
            reject(new Error('AndroidWebView is undefined'));
        }
    });
};
