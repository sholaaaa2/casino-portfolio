import {SELECTED_CARD} from '../actions/BonusAppearance';

export const getSelectedCardClassName = (cardName: SELECTED_CARD): string => {
    return `card-popup-bonus--invite-${cardName}`;
};
