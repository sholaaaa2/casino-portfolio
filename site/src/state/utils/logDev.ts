export const logDev = (...args: any): void => {
    if (!!(window as any).isDev || process.env.NODE_ENV === 'development') {
        console.log(...args);
    }
};
