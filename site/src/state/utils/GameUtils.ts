import {commons, games, site} from '../../api/proto';
import {getRandomInt, hasFlash, isMobile, Size} from './BrowserUtils';
import AppState from '../AppState';
import {GameGroupId, GamesSetSettings, GamesSortingSettings} from '../meta';
import {GameImageSize, GameItem} from '../actions/GamesActions';

export interface GameFilterMeta {
    searchRe?: RegExp | null;
    enabled?: GamesSetSettings;
    disabled?: GamesSetSettings;
    group?: site.IGameMenuEntry | null;
    currencyCode?: string | null;
    userAge?: number;
}

export type FilterListItem = (game: GameItem | null | undefined, meta: GameFilterMeta | null) => boolean;

export function isGameExists(gamesList: GameItem[], gameId: string): boolean {
    return gamesList && gamesList.filter(game => game.id === gameId).length > 0;
}

export const getGameSortingWeight = function (game: GameItem, sortingSettings: GamesSortingSettings, categorySorting: { [key: string]: number } | null, groupId: string | null | undefined): number {
    const gameTagsWeight = game.tags ? game.tags.reduce(function (acc: number, tag: string) {
        return acc + (sortingSettings.byGameTag[tag] || 0);
    }, 0) : 0;
    const gameIdWeight = (sortingSettings.byGameId[game.id!] || 0);
    const popularityWeight = (game.popularity24h || 0);
    const categorySettingsWeight = (categorySorting ? categorySorting[game.id!] || 0 : 0);

    if (groupId && groupId === 'new' && sortingSettings.sortByWeightInNewCategory) {
        return categorySettingsWeight;
    } else {
        return gameTagsWeight + gameIdWeight + popularityWeight + categorySettingsWeight;
    }
};

export function sortGames(gamesList: GameItem[], gamesSorting: GamesSortingSettings, groupId: string | null | undefined) {
    const categorySortSettings = groupId ? gamesSorting.inCategory[groupId] : null;
    
    if (groupId && groupId === 'new' && gamesSorting.sortByWeightInNewCategory) {
        gamesList = sortByAddedAt(gamesList);
    }

    gamesList.sort(function (a: GameItem, b: GameItem): number {
        return getGameSortingWeight(b, gamesSorting, categorySortSettings, groupId) - getGameSortingWeight(a, gamesSorting, categorySortSettings, groupId);
    });

    if (groupId && groupId === 'new' && !gamesSorting.sortByWeightInNewCategory) {
        gamesList = sortByAddedAt(gamesList);
    }

    // sort by available
    gamesList.sort(function (a: GameItem, b: GameItem): number {
        const aGameNotAvailable = a.disableIfActiveBonus;
        const bGameNotAvailable = b.disableIfActiveBonus;
        const aVal = (aGameNotAvailable ? 1 : 0);
        const bVal = (bGameNotAvailable ? 1 : 0);
        return aVal - bVal;
    });
}

function sortByAddedAt(gamesList: GameItem[]) {
    // additionally sort by date for new category
    // games with unset addedAt date will remain unchanged and sorted by default sorting rules
    return gamesList.sort(function (a: GameItem, b: GameItem): number {
        const aVal = (a.addedAt ? a.addedAt.toNumber() : 0);
        const bVal = (b.addedAt ? b.addedAt.toNumber() : 0);
        return bVal - aVal;
    });
}

export function sortGamesAlphabetically(gamesList: GameItem[], language: string) {
    gamesList.sort((a: GameItem, b: GameItem): number => {
        if ((a.name ? a.name[language] : '') < (b.name ? b.name[language] : '')) {
            return -1;
        }
        if ((a.name ? a.name[language] : '') > (b.name ? b.name[language] : '')) {
            return 1;
        }
        return 0;
    });
}

export function hasHTMLVersion(game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    if (game) {
        return !!(game.capabilities && game.capabilities.indexOf(commons.DeviceCapabilities.dc_html5full) > -1);
    } else {
        return false;
    }
}

export function filterEmptyIds(game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    return !!(game && game.id);
}

export function isMeetsAppSettings(game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    const disabled = meta ? meta.disabled : null;
    const enabled = meta ? meta.enabled : null;
    const disabledByTag = disabled ? disabled.byGameTag : null;
    const disabledById = disabled ? disabled.byGameId : null;
    const enabledById = enabled ? enabled.byGameId : null;

    if (game) {
        if (enabledById && enabledById.indexOf(game.id!) >= 0) {
            return true;
        } else {
            return (
                !!(disabledById && disabledById.indexOf(game.id!) === -1) &&
                !!(disabledByTag && !game.tags!.some(t => disabledByTag.indexOf(t) !== -1))
            );
        }
    } else {
        return false;
    }
}

export function hasMobileVersion(game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    if (game) {
        return !!(game.devices && game.devices.indexOf(commons.DeviceType.dt_mobile) > -1);
    } else {
        return false;
    }
}

export const matchSearchQuery = function (game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    const re = meta ? meta.searchRe : null;
    if (re && game) {
        if (Object.keys(game.name!).map(k => game.name![k]).some(n => n.search(re) !== -1)) {
            return true;
        }
        if (Object.keys(game.desc!).map(k => game.desc![k]).some(v => v.search(re) !== -1)) {
            return true;
        }
        if (game.tags!.some(n => n.search(re) !== -1)) {
            return true;
        }
        return false;
    } else {
        return true;
    }
};

export const isBelongsToGroup = function (game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    const group = meta ? meta.group : null;
    if (game && group) {
        if (group.id === GameGroupId.POPULAR) {
            const minPopularity = group.minPopularity || 0;
            return game.popularity24h ? game.popularity24h >= minPopularity : false;
        } else {
            const groupTags = group.tagsInclude;
            if (groupTags) {
                if (groupTags.indexOf('*') >= 0) {
                    return true;
                } else {
                    return game.tags!.some(t => groupTags!.indexOf(t) !== -1);
                }
            } else {
                return true;
            }
        }
    } else {
        return true;
    }
};

export const isGameSuitableForAge = function (game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    let age = meta && meta.userAge ? meta.userAge : null;
    if (game && game.tags && game.tags.indexOf('tomhorn') !== -1 && age) {
        if (age < 21 && game.tags.indexOf('tomhorn-catb') !== -1) {
            return true;
        } else if (age >= 21 && game.tags.indexOf('tomhorn-cata') !== -1) {
            return true;
        } else {
            return false;
        }
    }
    return true;
};

export const isGameSuitableForCurrency = function (game: GameItem | null | undefined, meta: GameFilterMeta | null): boolean {
    const currencyCode = meta ? meta.currencyCode : null;
    if (game && currencyCode) {
        const gameCurrencies = game.currencies;
        if (gameCurrencies) {
            return gameCurrencies.indexOf(currencyCode) !== -1;
        }
    }

    return true;
};

export const applyGamesFilters = function (gamesList: GameItem[], filtersList: FilterListItem[], filtersMeta: GameFilterMeta): GameItem[] {
    return gamesList.filter(function (game: GameItem): boolean {
        if (game.disabled) {
            return false;
        }
        for (let i = 0, count = filtersList.length; i < count; i++) {
            const filter: FilterListItem = filtersList[i];
            if (!filter(game, filtersMeta)) {
                return false;
            }
        }
        return true;
    });
};

export const findGroupById = function (groups?: site.IGameMenuEntry[] | null, id?: string | null): site.IGameMenuEntry | null {
    let group = groups ? groups.filter(g => g.id === id)[0] : null;
    if (group == null && groups) {
        for (let i = 0; i < groups.length; i++) {
            if (groups[i].children != null) {
                let subGroup = groups[i].children!.find(g => g.id === id);
                if (subGroup !== undefined) {
                    group = subGroup;
                    break;
                }
            }
        }
    }
    return group;
};

export const isProvidersGroup = function (groups?: site.IGameMenuEntry[] | null, currentGroup?: site.IGameMenuEntry | null): boolean {
    let providersGroup = groups ? groups.find(g => g.id === 'providers') || null : null;
    if (providersGroup && providersGroup.children && currentGroup) {
        return providersGroup.children.some((c) => c.id === currentGroup.id);
    }
    return false;
};

export const removeDuplicates = function (gamesList: GameItem[]) {
    const map = {};
    const result = [];
    for (let i = 0, count = gamesList.length; i < count; i++) {
        let game = gamesList[i];
        if (game.id && !map[game.id]) {
            map[game.id] = true;
            result.push(game);
        }
    }
    return result;
};

export const fillGamesToCount = function (result: GameItem[], gamesList: GameItem[], gamesSorting: GamesSortingSettings, count: number, categoryId: string, meta: GameFilterMeta | null) {
    gamesList = gamesList.slice();
    sortGames(gamesList, gamesSorting, categoryId);
    for (let i = 0, gamesCount = gamesList.length; i < gamesCount; i++) {
        if (result.length >= count) {
            return;
        } else {
            const game = gamesList[i];
            if (!isGameExists(result, game.id!) && isGameSuitableForCurrency(game, meta)) {
                result.push(game);
            }
        }
    }
};

export function findGame(appState: AppState, id?: string | null): (GameItem | null) {
    return appState.games.findGameById(id);
}

export function findGameConsideringDeviceAndCapabilites(appState: AppState, gameId: string): (GameItem | null) {
    if (!appState.siteConfig) {
        return null;
    }

    const game = appState.games.findGameById(gameId);

    if (!game) {
        return null;
    }

    if (!hasFlash() && !hasHTMLVersion(game, null)) {
        return null;
    }

    if (isMobile() && !hasMobileVersion(game, null)) {
        return null;
    }

    return game;
}

export function embedScriptCode(state: AppState, embed: games.IGameEmbedResponse, game?: GameItem | null): string {
    const lang = state.siteConfig.languages!.filter(l => l.code === state.language)[0];
    const locale = lang.code;
    const currency = state.userInfo.currency!.code!;

    let f = embed.launchMethod!.div!.initFunction!;
    f = f.replace('{{denomination|json|safe}}', getSpinDenom(state).toString());
    f = f.replace('{{session|json|safe}}', `"${state.userInfo.sid}"`);
    f = f.replace('{{currency|json|safe}}', `"${currency}"`);
    f = f.replace('{{lang|json|safe}}', `"${locale}"`);
    f = f.replace('");', '"});');
    console.debug('Launch game call', f);
    return f;
}

function getSpinDenom(content: AppState): number {
    return content.userInfo.currency!.spinDenomination!;
}

export function gameNameWithoutAliases(name: string): string {
    return name.split(/\s*\(/)[0];
}

export function getGameSeoDescription(appState: AppState, game: GameItem, defaultValue: string): string {
    const { language } = appState;

    if (game && game.seo && game.seo.description && language in game.seo.description) {
        return game.seo.description[language];
    } else {
        return defaultValue;
    }
}

export function getGameSeoTitle(appState: AppState, game: GameItem, defaultValue: string): string {
    const { language } = appState;

    if (game && game.seo && game.seo.title && language in game.seo.title) {
        return game.seo.title[language];
    } else {
        return defaultValue;
    }
}

/**
 * Calculates size from baseSize depend on proportion,
 * where proportion might be set in pixels, like 1200x600 or in ration like 2x1.
 */
export function calcProportionSize(baseSize: Size, proportion: Size): Size {
    const result = {width: 0, height: 0};

    let ratio = 0;
    let width = proportion.width * 100000000;
    let height = proportion.height * 100000000;

    if (width > baseSize.width) {
        ratio = baseSize.width / width;
        result.width = baseSize.width;
        result.height = height * ratio;
        height = height * ratio;
        width = width * ratio;
    }

    if (height > baseSize.height) {
        ratio = baseSize.height / height;
        result.width = width * ratio;
        result.height = baseSize.height;
    }

    return result;
}

export function getGameImage(gameOrGameId: GameItem | string | null | undefined, size?: GameImageSize): string | undefined {
    const gameId = typeof gameOrGameId === 'object' ? (gameOrGameId as GameItem).id : gameOrGameId as string;
    if (gameId) {
        const imageId = GamesImageToReplicaMap[gameId] || gameId;
        switch (size) {
            default:
            case GameImageSize.SIZE_300x300:
                return `/media/thumb/300x300/${imageId}.jpeg`;
            case GameImageSize.SIZE_300x188:
                return `/media/thumb/300x188/${imageId}.jpeg`;
            case GameImageSize.SIZE_40x40:
                return `/media/thumb/winners/${imageId}.jpeg.jpg`;
        }
    } else {
        return undefined;
    }
}

export function addGamePageClass() {
    const root = document.getElementById('root');
    if (root) {
        root.classList.add('root-game-page');
    }
}

export function removeGamePageClass() {
    const root = document.getElementById('root');
    if (root) {
        root.classList.remove('root-game-page');
    }
}

export function getRandGameIndex(indexToReplace: number, length: number): number {
    let result = indexToReplace;

    if (length > 2) {
        do {
            result = getRandomInt(0, length - 1);
        } while (indexToReplace === result);
        return result;
    } else {
        return result;
    }
}


export function swapGamesByIndex(games: GameItem[], index1: number, index2: number):GameItem[] {
    const game1 = games[index1];
    const game2 = games[index2];
    if (game1 && game2) {
        games[index1] = game2;
        games[index2] = game1;
    }
    return games;
}

export const GamesImageToReplicaMap = {
    g_hoc: 'y-novomatic-hannibal-of-carthago',
    k_g: 'y-table-golf-keno',
    tg_bg: 'y-amatic-billys-game',
    g_fl: 'y-novomatic-fruitilicious',
    g_jj_d: 'y-novomatic-just-jewels',
    g_jj: 'y-novomatic-just-jewels-classic',
    g_bgb: 'y-novomatic-bananas-go-bahamas',
    g_bs: 'y-novomatic-banana-splash',
    g_bor: 'y-novomatic-book-of-ra-classic',
    g_bor_d: 'y-novomatic-book-of-ra-deluxe',
    g_col: 'y-novomatic-columbus-classic',
    g_coldl: 'y-novomatic-columbus-deluxe',
    g_dd: 'y-novomatic-dazzling-diamonds',
    g_dap: 'y-novomatic-dolphins-pearl-classic',
    g_dapdl: 'y-novomatic-dolphins-pearl',
    g_ga: 'y-novomatic-golden-ark',
    g_mp: 'y-novomatic-marco-polo',
    g_sh: 'y-novomatic-sizzling-hot-classic',
    g_sh_d: 'y-novomatic-sizzling-hot-deluxe',
    g_atl: 'y-novomatic-attila',
    g_koc: 'y-novomatic-king-of-cards',
    g_ec: 'y-novomatic-emperors-of-china',
    g_loo: 'y-novomatic-lord-of-the-ocean',
    g_llc: 'y-novomatic-lucky-lady-charm-classic',
    g_llcdl: 'y-novomatic-lucky-ladys-charm',
    g_mg: 'y-novomatic-the-money-game',
    g_mjp: 'y-novomatic-mega-joker',
    g_pf: 'y-novomatic-polar-fox',
    g_pg2: 'y-novomatic-pharaohs-gold-ii',
    g_pg3: 'y-novomatic-pharaohs-gold-iii',
    g_pot2: 'y-novomatic-plenty-on-twenty-ii-hot',
    g_sph: 'y-novomatic-supra-hot',
    g_rl: 'y-novomatic-red-lady',
    g_rt: 'y-novomatic-royal-treasures',
    g_sf: 'y-novomatic-secret-forest',
    g_um: 'y-novomatic-unicorn-magic',
    g_wf: 'y-novomatic-wonderful-flute',
    g_d7: 'bb-diamond-7',
    g_gg_d: 'bb-gryphons-gold-deluxe'
};
