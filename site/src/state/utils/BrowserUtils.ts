import MobileDetect from 'mobile-detect';
import {commons, users} from '../../api/proto';
import {NString} from '../meta';
import AppState from '../AppState';

const mobileDetect = new MobileDetect(window.navigator.userAgent);

export interface Size {
    width: number;
    height: number;
}

export enum ScreenSize {
    MOBILE = 768
}

export function hasFlash(): boolean {
    const nav = window.navigator;
    if (nav.plugins !== undefined) {
        const plugins = nav.plugins;
        for (let i = 0; i < plugins.length; i++) {
            if (-1 < plugins[i].name.indexOf('Shockwave Flash')) {
                return true;
            }
        }
    }

    return false;
}

// returns true if device type is mobile or tablet
export function isMobile(): boolean {
    const isNewIpad =
        window.navigator.userAgent.match(/Mac/) &&
        window.navigator.maxTouchPoints &&
        window.navigator.maxTouchPoints > 2;
    return !!mobileDetect.mobile() || !!isNewIpad;
}

// returns true if device type is mobile
export function isMobileOnly(): boolean {
    return !!mobileDetect.phone();
}

export function getMobileOSType(): string {
    return mobileDetect.os();
}

export function isAndroid(): boolean {
    return mobileDetect.os() === 'AndroidOS';
}

export function getOSType(): string {
    const nav = window.navigator;

    if (nav.appVersion.indexOf('Win') !== -1) {
        return 'win';
    }

    if (nav.appVersion.indexOf('Mac') !== -1) {
        return 'mac';
    }

    if (nav.appVersion.indexOf('X11') !== -1) {
        return 'x11';
    }

    if (nav.appVersion.indexOf('Linux') !== -1) {
        return 'linux';
    }

    return 'unknown';
}

export function getBrowser(): string {
  return mobileDetect.userAgent();
}

export function getCapabilities(): commons.DeviceCapabilities[] {
    let capabilities = [commons.DeviceCapabilities.dc_html5full];
    if (hasFlash()) {
        capabilities.push(commons.DeviceCapabilities.dc_flashplayer);
    }
    return capabilities;
}

export function getDeviceType(gameId: NString): commons.DeviceType {
    if (gameId) {
        if (gameId.startsWith('onetouch-')) {
            return isMobileOnly() ? commons.DeviceType.dt_mobile :  commons.DeviceType.dt_desktop;
        } else {
            return isMobile() ? commons.DeviceType.dt_mobile :  commons.DeviceType.dt_desktop;
        }
    } else {
        return isMobile() ? commons.DeviceType.dt_mobile :  commons.DeviceType.dt_desktop;
    }
}

export function cookie(name: string, value: string = '', ttl: number | undefined = undefined,
                       path: NString = undefined, domain: NString = undefined,
                       secure: boolean = false) {
    // set cookie
    if (arguments.length > 1) {
        return document.cookie = name + '=' + encodeURIComponent(value) +
            (ttl ? '; expires=' + new Date(+new Date() + (ttl * 1000)).toUTCString() : '') +
            (path ? '; path=' + path : '') +
            (domain ? '; domain=' + domain : '') +
            (secure ? '; secure' : '');
    }

    // get cookie
    return decodeURIComponent((('; ' + document.cookie).split('; ' + name + '=')[1] || '').split(';')[0]);
}

export function extractLocationArgs(): { [k: string]: string } {
    let query = window.location.search;

    if (query) {
        return query.substr(1).split('&').reduce((res, entry) => {
            let [k, v] = entry.split('=');
            res[decodeURIComponent(k)] = decodeURIComponent(v);
            return res;
        }, {});
    }
    return {};
}

export function getReferrer(): NString {
    let referrer;
    // referrer is computed from any of these rules, in order of appearance
    // 1. window.location.search if it is containing `ref=`
    // 2. window.top.document.referrer
    // 3. window.parent.document.referrer
    // 4. document.referrer

    // the result will be in one of forms
    // 1. 'ref:xxx' if rules 1 or 2 matched
    // 2. 'direct' if none of rules matched
    // 3. '' if some rule matched but referrer is this site or not matching url
    // 4. url of search-engine or some other site
    try {
        if (window.location.search.indexOf('ref=') !== -1) {
            // reflink `?ref=somerefname`
            return 'ref:' + window.location.search.match(/ref=([a-z0-9-_]+)/i)![1];
        } else {
            referrer = '';
        }
    } catch (e) {
        referrer = '';
    }

    try {
        referrer = window.top?.document.referrer ?? "";
    } catch (e1) {
        if (window.parent) {
            try {
                referrer = window.parent.document.referrer;
            } catch (e2) {
                referrer = '';
            }
        }
    }

    if (referrer === '') {
        referrer = document.referrer;
    }

    if (referrer === '') {
        referrer = 'direct';
    } else {
        if (0 === referrer
                .replace(window.location.protocol + '//', '')
                .indexOf(window.location.hostname)) {
            // inner navigation
            referrer = '';
        } else {
            // search engine / other
            try {
                return referrer
                    .match(/^(http|https):\/\/(www\.|)(.+\..+)\//i)!
                    .pop();
            } catch (e3) {
                // referrer is not matching a pattern, do nothing
            }
        }
    }

    return referrer;
}

export function getLegacyReferrer(): string {
    let newReferrer = getReferrer();
    let referrer: string | null | undefined = cookie('wlref');
    if (newReferrer && newReferrer !== '' && newReferrer !== referrer) {
        // only save referrer if not internal and changed
        cookie('wlref', newReferrer, 60 * 60 * 365, '/');
        referrer = newReferrer;
    }
    return referrer || '';
}

export function getLanguagefromUrl(): string | null {
    const firstPathItem = window.location.pathname.split('/')[1];
    return firstPathItem && firstPathItem.match(/[a-z]{2}/) ? firstPathItem : null;
}

export function getExtendedParams(): { [k: string]: string } | null {
    let search = window.location.search;

    // tries to find p1-4 in window.location.search
    const re = /^(p[1-4])=(.+)$/i;
    search = -1 === search.indexOf('?') ? search : search.substr(1);

    // convert to array of strings 'name=value', get only p[1-4]
    // convert to [name, value] and reduce to single object
    const result = search
        .split('&')
        .filter(v => re.exec(v))
        .map(v => v.split('='))
        .reduce((acc, v) => {
            acc[v[0]] = decodeURIComponent(v[1]);
            return acc;
        }, {});

    const keys = Object.keys(result);
    if (keys.length > 0) {
        for (let key of keys) {
            let name = key.replace('p', 'wlrefp');
            if (!cookie(name)) {
                cookie(name, result[key], 60 * 60 * 365, '/');
            }
        }
        return result;
    } else {
        return null;
    }
}

export function parseDate(dateString: string): number | null {
    // First check for the pattern
    if (!/^\d{1,2}\.\d{1,2}\.\d{4}$/.test(dateString)) {
        return null;
    }

    // Parse the date parts to integers
    const parts = dateString.split('.');
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10);
    const year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month === 0 || month > 12) {
        return null;
    }

    const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
        monthLength[1] = 29;
    }

    // Check the range of the day
    if (!(day > 0 && day <= monthLength[month - 1])) {
        return null;
    }

    return Date.UTC(year, month - 1, day);
}

export function formatRegistrationLevel(level: users.UserInfo.UserRegistrationLevel | null | undefined) {
    switch (level) {
        case users.UserInfo.UserRegistrationLevel.GUEST:
            return 'Guest';
        case users.UserInfo.UserRegistrationLevel.UNCONFIRMED:
            return 'Unconfirmed';
        case users.UserInfo.UserRegistrationLevel.REGISTERED:
            return 'Registered';
        default:
            return 'Unknown';
    }
}

export function getViewportSize(): Size {
    return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight
    };
}

export const allowedNumberKeys = [
    8, // backspace
    9, // tab
    13, // enter
    16, // shift
    17, // ctrl
    18, // alt
    37, // left
    39, // right
    46, // delete
    91, // meta

    96, // numpad 0
    97, // numpad 1
    98, // numpad 2
    99, // numpad 3
    100, // numpad 4
    101, // numpad 5
    102, // numpad 6
    103, // numpad 7
    104, // numpad 8
    105, // numpad 9
    48, // digit 0
    49, // digit 1
    50, // digit 2
    51, // digit 3
    52, // digit 4
    53, // digit 5
    54, // digit 6
    55, // digit 7
    56, // digit 8
    57, // digit 9
    57, // digit 9
    190, // dot
];

export interface TranslateEl {
    translateX: number | null;
    translateY: number | null;
    scale: number | null;
}

export function getTranslateEl(el: HTMLElement): TranslateEl | undefined {
    let transResult: TranslateEl = {
        translateX: null,
        translateY: null,
        scale: null
    };

    if (!window.getComputedStyle) {
        return;
    }
    let style = getComputedStyle(el),
        transform = style.transform || style.webkitTransform;
    const matrix = transform!.match(/^matrix\((.+)\)$/);
    if (matrix) {
        transResult = {
            translateX: parseFloat(matrix[1].split(', ')[4]),
            translateY: parseFloat(matrix[1].split(', ')[5]),
            scale: parseFloat(matrix[1].split(', ')[0])
        };
    }

    return transResult;
}

export function convertCamelCase(str: string): string {
    const arrayString = str.replace(/([a-z])([A-Z])/g, '$1 $2')
        .toLowerCase()
        .split(' ');

    return arrayString.map((item) => {
        return item[0].toUpperCase() + item.substr(1);
    }).join(' ');
}

export function getSelectValue(appState: AppState, id: string): string {
    const codeInput = appState.getRef(id) as HTMLInputElement;
    return codeInput ? codeInput.value : '';
}

export function getCleanedInputValue(appState: AppState, id: string): string {
    const codeInput = appState.getRef(id) as HTMLInputElement;
    return codeInput ? codeInput.value.replace(/[\s-+()_]/g, '') : '';
}

export function getInputValue(appState: AppState, id: string): string {
    const codeInput = appState.getRef(id) as HTMLInputElement;
    return codeInput?.value ?? '';
}

export function getInputValueWithoutSpaces(appState: AppState, id: string): string {
    const codeInput = appState.getRef(id) as HTMLInputElement;
    return codeInput ? codeInput.value.replace(/[\s]/g, '') : '';
}

export function clearInputValue(appState: AppState, id: string): void {
    const codeInput = appState.getRef(id) as HTMLInputElement;
    if (codeInput) {
        codeInput.value = '';
    }
}

export function validateByFormat(value: string, format: NString): boolean {
    try {
        return format ? !!value.match(new RegExp(format, 'i')) : true;
    } catch (e) {
        console.error(e);
        return false;
    }
}

export function setPageMeta(appState: AppState, title: string, description: string, ogImage?: string) {
    document.title = title;
    setMetaTagContent('property', 'og:title', title);
    setMetaTagContent('name', 'description', description);
    setMetaTagContent('property', 'og:description', description);

    if (ogImage) {
        setMetaTagContent('property', 'og:image', ogImage);
    } else {
        setMetaTagContent('property', 'og:image', appState.g('%DEFAULT_OG_IMAGE%'));
    }
}

export function setMetaTagContent(paramName: string, paramValue: string, content: string) {
    const tag = document.querySelector(`meta[${paramName}="${paramValue}"]`);
    if (tag) {
        tag.setAttribute('content', content);
    }
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export function debounce(func: (...params: any[]) => void, wait: number, immediate?: boolean) {
    let timeout: NodeJS.Timer | null;
    return function(this: any) {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) {
                func.apply(context, args as any);
            }
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout!);
        timeout = setTimeout(later, wait);
        if (callNow) { 
            func.apply(context, args as any);
        }
    };
}

export function scrollToTheTop() {
    window.scrollTo(0, 0);
}

export function addCryptoDenominationToParams(params: { [k: string]: string } | null): { [k: string]: string } {
    const cryptoModeParam = {
        'crypto_mode': 'with_denomination'
    };
    if (params === null) {
        return cryptoModeParam;
    } else {
        return {
            ...params,
            ...cryptoModeParam
        };
    }
}

export function isGPSE() {
    return window.navigator.userAgent.includes('Chrome-Lighthouse');
}

export const isSafari = mobileDetect.userAgent() === 'Safari';

export function isIPhone(){
    return mobileDetect.is('iPhone');
}

export function getParamsFromUrl(params: string[]): (string | null)[] {
    const url = new URL(window.location.href);
    return params.map(param => url.searchParams.get(param));
}

export function getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
