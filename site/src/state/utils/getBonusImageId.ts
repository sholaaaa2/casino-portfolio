export const getBonusImageId = (bonusId?: string | null): number | string => {
    return bonusId ? (bonusId?.split('').map(_ => _.charCodeAt(0)).reduce((acc, val) => acc + val, 0) ?? 0) % 6 + 1 : 1;
};

export default getBonusImageId;
