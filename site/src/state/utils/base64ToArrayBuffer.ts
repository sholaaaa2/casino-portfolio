export const base64ToArrayBuffer = function (base64: string): Uint8Array {
    try {
        const binaryString = window.atob(base64);
        const len = binaryString.length;
        let bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes;
    } catch (e) {
        console.error('Failed to decode SSE notification: ', e);
        return new Uint8Array(0);
    }
};
