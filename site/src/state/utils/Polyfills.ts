// String.endsWith
if (!String.prototype.endsWith) {
    // eslint-disable-next-line no-extend-native
    String.prototype.endsWith = function (search: string, endPosition?: number): boolean {
        if (endPosition === undefined || endPosition > this.length) {
            endPosition = this.length;
        }
        return this.substring(endPosition - search.length, endPosition) === search;
    };
}

// String.startsWith
if (!String.prototype.startsWith) {
    // eslint-disable-next-line no-extend-native
    String.prototype.startsWith = function (search: string, endPosition?: number): boolean {
        endPosition = endPosition || 0;
        return this.indexOf(search, endPosition) === endPosition;
    };
}

// Console.debug
if (!console.debug) {
    console.debug = function (name: string, value: any) {
        console.warn('DEBUG: ' + name + '==' + value);
    };
}

// Find in array
if (!Array.prototype.find) {
    // eslint-disable-next-line no-extend-native
    Array.prototype.find = function (predicate: any) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        let list = Object(this);
        let thisArg = arguments[1];
        let value;

        for (let i = 0; i < list.length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

// Find index array
if (!Array.prototype.findIndex) {
    // eslint-disable-next-line no-extend-native
    Array.prototype.findIndex = function(predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.findIndex called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        let list = Object(this);
        let length = list.length >>> 0;
        let thisArg = arguments[1];
        let value;

        for (let i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return i;
            }
        }
        return -1;
    };
}

// ArrayBuffer slice
(function () {

    function clamp(val: number, length: number) {
        val = (val | 0) || 0;

        if (val < 0) {
            return Math.max(val + length, 0);
        }

        return Math.min(val, length);
    }

    if (!ArrayBuffer.prototype.slice) {
        // eslint-disable-next-line no-extend-native
        ArrayBuffer.prototype.slice = function (from: number, to: number) {
            let length = this.byteLength;
            let begin = clamp(from, length);
            let end = length;

            if (to !== undefined) {
                end = clamp(to, length);
            }

            if (begin > end) {
                return new ArrayBuffer(0);
            }

            let num = end - begin;
            let target = new ArrayBuffer(num);
            let targetArray = new Uint8Array(target);

            let sourceArray = new Uint8Array(this, begin, num);
            targetArray.set(sourceArray);

            return target;
        };
    }
})();

export default {};