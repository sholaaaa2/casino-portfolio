import Long from 'long';

export const convertStringToLong = (str?: string): Long | undefined => {
    return str ? Long.fromString(str.split('.')[0]) : undefined;
};
