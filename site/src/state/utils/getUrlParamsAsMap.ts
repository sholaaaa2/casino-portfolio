export function getUrlParamsAsMap(): {[k: string]: string} {
    const currentUrl = new URL(window.location.href);
    const extraParams = {};
    currentUrl.searchParams.forEach((value, key) => {
        extraParams[key] = value;
    });
    return extraParams;
}
