export default (array: any[], size: number, firstChunkSmall?: number) => {
    const chunks = [];
    for (let i = 0; i < array.length; i++) {
        const last = chunks[chunks.length - 1];
        if (!last || last.length === size || (firstChunkSmall && chunks.length === 1 && last.length === (array.length % size))) {
            chunks.push([array[i]]);
        } else {
            last.push(array[i]);
        }
    }
    return chunks;
};
