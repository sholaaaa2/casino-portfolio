import {NString} from '../meta';
import {AuthorizationType} from '../actions/authorization/AuthorizationTypeState';

// region ---- enums
export function isValidStrEnumValue(enumObj: object, value: NString): boolean {
    if (value) {
        const keys = Object.keys(enumObj);
        for (let key of keys) {
            if (enumObj[key] === value) {
                return true;
            }
        }
    }
    return false;
}

export function isValidAuthorizationType(value: NString): boolean {
    return isValidStrEnumValue(AuthorizationType, value);
}

// endregion

// region ---- inputs
// eslint-disable-next-line no-control-regex
const emailRe = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

export function isValidEmail(value: NString): string | null {
    return value && value.toLowerCase().match(emailRe) ? value : null;
}

const numbersRe = /^[0-9]*$/;

export function isNumberString(value: string): boolean {
    return !!value.toLowerCase().match(numbersRe);
}
// endregion