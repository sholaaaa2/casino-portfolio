const pluralize = require('pluralize');
const pluralizeRu = require('pluralize-ru');

export function pluralizeWord(word: string, amount: number, lang?: string, pluralMap?: string[]) {
    if (lang && pluralMap) {
        if (lang.toLowerCase() === 'ru') {
            return formatPluralByRU(amount, pluralMap);
        }
    }

    return `${amount} ${pluralize(word, amount)}`;
}

function formatPluralByRU(amount: number, pluralMap: string[]) {
    if (amount) {
        return `${amount} ${pluralizeRu(amount, ...pluralMap)}`;
    } else {
        return pluralizeRu(amount, ...pluralMap);
    }
}