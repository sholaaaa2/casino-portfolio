import {BonusMetaObj, NLong, NString} from '../meta';
import {BonusMetaField} from '../actions/BonusAppearance';
import Long from 'long';

interface IUpdateFadeClasses {
    show: boolean;
    el: HTMLElement;
    onShowCallback?: () => void;
    onShowedCallback?: () => void;
    onHiddenCallback?: () => void;
    animationDuration?: number; // ms
}


export function updateFadeClasses(params: IUpdateFadeClasses) {
    const {show, el, onShowCallback, onShowedCallback, onHiddenCallback, animationDuration = 250} = params;
    if (!el) {
        return;
    }
    if (show) {
        el.classList.add('in');

        setTimeout(function () {
            el.classList.add('show');
            if (onShowCallback) {
                onShowCallback();
            }
        }, 0);

        if (onShowedCallback) {
            setTimeout(onShowedCallback, animationDuration);
        }
    } else if (el.classList.contains('show') && el.classList.contains('in')) {

        el.classList.remove('show');

        setTimeout(function () {
            el.classList.remove('in');

            if (onHiddenCallback) {
                onHiddenCallback();
            }
        }, animationDuration);
    }
}

export function getTextWidth(txt: string, font: string) {
    const element = document.createElement('canvas');
    let context = element.getContext('2d')!;
    context.font = font;

    return context.measureText(txt).width;
}

// tslint:disable-next-line
export function isDef(value: any): boolean {
    return value !== null && value !== undefined;
}

export function processString(key: string, meta: BonusMetaObj, defaultVal: string | null): string | null {
    if (meta && isDef(meta[key]) && meta[key] !== 'null') {
        return meta[key];
    } else {
        return defaultVal;
    }
}

export function processBoolean(key: BonusMetaField, meta: BonusMetaObj, defaultVal?: boolean): boolean {
    if (meta && isDef(meta[key])) {
        return meta[key] === 'true';
    } else {
        return isDef(defaultVal) ? defaultVal! : false;
    }
}

export function processLong(key: BonusMetaField, meta: BonusMetaObj): NLong {
    if (meta && meta[key]) {
        return Long.fromString(meta[key]);
    } else {
        return null;
    }
}

export function processNumber(key: BonusMetaField, meta: BonusMetaObj): number | null {
    if (meta && meta[key]) {
        const value = parseFloat(meta[key]);
        return isNaN(value) ? null : value;
    } else {
        return null;
    }
}

// tslint:disable-next-line
export function processMetaList(str: NString): any[] {
    if (str) {
        try {
            const obj = JSON.parse(str);
            const keys = Object.keys(obj);
            return keys.map(function (k: string) {
                return obj[k];
            });
        } catch (e) {
            console.error('Unable to process bonus meta list', e);
            return [];
        }
    } else {
        return [];
    }
}
