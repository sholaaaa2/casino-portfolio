import * as currencies from './Currencies';
import * as localization from './Localization';
import * as accounting from 'accounting';
import { commons } from '../../api/proto';
import Long from 'long';
import { NLong } from '../meta';
import { pluralizeWord } from './Pluralize';
import { isDef } from '../../api/utils';
import { isCryptoCurrencyId } from './Currencies';
import { JackpotState } from '../actions/jackpots/JackpotState';
import { JackpotWinState } from '../actions/jackpots/JackpotWinState';
const appSettings = require('../../AppSettings.json');

export const NASymbol = '—';

export function convertAmountToApi(value: Long | number | string, currency: commons.ICurrency): Long | null {
    const denom = currency.denomination!;
    let strValue = value.toString();

    if (strValue === '0') {
        return Long.ZERO;
    }

    //TS doesn't support isNaN(arg: string)
    if (isNaN(strValue as any) || value === "") {
        return null;
    }
    if (strValue.indexOf('.') >= 0) {
        let points = strValue.length - strValue.indexOf('.') - 1;
        if (points <= denom) {
            strValue += Array(denom - points).fill('0').join('');
        } else {
            strValue = strValue.substr(0, strValue.indexOf('.') + denom + 1);
        }
        strValue = strValue.replace('.', '');
    } else {
        strValue += Array(denom).fill('0').join('');
    }
    return Long.fromString(strValue);
}

export function formatPercent(value: number): string {
    return value ? value.toFixed(1) + '%' : '0%';
}

export function formatCurrencyCode(currencyOrCurrencyId: NLong | commons.ICurrency): string {
    const currency = normalizeCurrency(currencyOrCurrencyId);

    return currency ? getFormattingCurrencyCode(currency) : '';
}

export function formatCurrencyAmount(value: NLong, currencyOrCurrencyId: NLong | commons.ICurrency, removeCurrencyCode?: boolean, displayIntegerCurrencies?: boolean): string {
    const currency = normalizeCurrency(currencyOrCurrencyId);
    if (value && currency) {
        const amountIsInteger = appSettings.displayIntegerCurrencies || displayIntegerCurrencies;
        return isCryptoCurrencyId(currency.id) ?
            formatCryptoCurrency(value, currency, removeCurrencyCode, amountIsInteger) :
            formatFiatCurrency(value, currency, removeCurrencyCode, amountIsInteger);
    } else {
        return NASymbol;
    }
}
export function replaceZeros(value: number): string {
    const result = value.toString().includes('.00') ? value.toString().replace('.00', '') :
        value.toString().substring(0, value.toString().length - 1)
    return result;
}

export function isScoreCurrencyId(currencyId: NLong): boolean {
    return !!currencyId && currencyId.equals(currencies.SCORE_CURRENCY_ID);
}

export function formatScoresCurrency(amount: NLong): string {
    if (amount) {
        return pluralizeWord(
            localization.translate('ui-scores'),
            amount.toNumber(),
            localization.getCurrentLanguage(),
            [
                localization.translate('ui-no-scores'),
                localization.translate('ui-score-single'),
                localization.translate('ui-scores-single'),
                localization.translate('ui-scores-multi')
            ]);
    } else {
        return NASymbol;
    }
}

export function denominateValue(value: NLong, currencyOrCurrencyId: NLong | commons.ICurrency): string {
    const currency = normalizeCurrency(currencyOrCurrencyId);

    if (!isDef(value) || !currency) {
        return '0';
    }

    let denom = currency.denomination!;
    let strValue = value!.toString();

    if (strValue === '0') {
        return '0';
    }

    if (strValue.length <= denom) {
        const zerosCount = denom - strValue.length + 1;
        for (let i = 0; i < zerosCount; i++) {
            strValue = '0' + strValue;
        }
    }
    return strValue.substr(0, strValue.length - denom) + '.' + strValue.substr(strValue.length - denom);
}

function formatCryptoCurrency(value: Long, currency: currencies.Currency, removeCurrencyCode?: boolean, amountIsInteger?: boolean): string {
    const currencyCode = getFormattingCurrencyCode(currency);
    if (value.equals(Long.ZERO)) {
        return removeCurrencyCode ? '0' : `0 ${currencyCode}`;
    } else {
        const denomValue = denominateValue(value, currency);
        const denomination = amountIsInteger ? 0 : currency.denomination
        const formatted = clearEndZeros(accounting.formatMoney(
            parseFloat(denomValue),
            currencyCode,
            denomination,
            ',',
            '.',
            '%v'
        ));
        return removeCurrencyCode ?
            formatted :
            currency.format.replace('%s', formatted);
    }
}

function formatFiatCurrency(value: Long, currency: currencies.Currency, removeCurrencyCode?: boolean, amountIsInteger?: boolean): string {
    const denomValue = denominateValue(value, currency);
    const format = removeCurrencyCode ? '%v' : currency.format.replace('%s', '%v');
    const currencyCode = getFormattingCurrencyCode(currency);
    const denomination = amountIsInteger ? 0 : currency.denomination
    return accounting.formatMoney(
        parseFloat(denomValue),
        currencyCode,
        denomination,
        ',',
        '.',
        format
    );
}

export function normalizeCurrency(currencyOrCurrencyId: NLong | commons.ICurrency): currencies.Currency | null {
    return Long.isLong(currencyOrCurrencyId) ?
        currencies.findCurrencyById(currencyOrCurrencyId as Long) :
        currencyOrCurrencyId as currencies.Currency;
}

export function getFormattingCurrencyCode(currency: currencies.Currency): string {
    return currency.formattingCode || currency.code;
}

function clearEndZeros(val: string): string {
    let char = val.charAt(val.length - 1);
    if (val.length === 1) {
        return val;
    } else if (char === '0') {
        return clearEndZeros(val.substr(0, val.length - 1));
    } else if (char === '.' || char === ',') {
        return val.substr(0, val.length - 1);
    } else {
        return val;
    }
}


// todo: rework with in normal plural way ----------------------------------------------------------------
/** need to move this somewhere */
export function getFreeSpinsToken(value: Long | number): string {
    const strValue = value.toString();
    if (strValue.match(/1[0-9]$/)) {
        return 'ui-free-spins-value-other';
    } else if (strValue.endsWith('1')) {
        return 'ui-free-spins-value-one';
    } else if (strValue.match(/[2-4]$/)) {
        return 'ui-free-spins-value-two';
    } else {
        return 'ui-free-spins-value-other';
    }
}

export function getFreeBetsToken(value: Long | number): string {
    const strValue = value.toString();
    if (strValue.match(/1[0-9]$/)) {
        return 'ui-free-bets-value-other';
    } else if (strValue.endsWith('1')) {
        return 'ui-free-bets-value-one';
    } else if (strValue.match(/[2-4]$/)) {
        return 'ui-free-bets-value-two';
    } else {
        return 'ui-free-bets-value-other';
    }
}

/** need to move this somewhere */
export function formatJackpotName(jackpot: JackpotState | JackpotWinState | null | undefined): string {
    // use this function to adjust jackpot name
    // e.g Gold Jackpot or Silver Jackpot
    return '';
}

/** need to move this somewhere */
export function formatJackpotStyle(jackpot: JackpotState | JackpotWinState | null | undefined): string {
    // use this function to adjust jackpot name
    // e.g jackpot-page-type-item-orange
    return '';
}

// LEGACY END --------------------------------------------------------------------------------------------
