let translations: { [k: string]: string } | null = null;
let language: string = '';

export function setTranslations(value: { [k: string]: string }) {
    translations = value;
}

export function translate(term: string, ...rest: {}[]): string {
    let res = translations ? translations[term] : null;
    if (!res) {
        res = term;
    }
    return formatString(res, ...rest);
}

export function getCurrentLanguage(): string {
    return language;
}

export function setCurrentLanguage(value: string) {
    language = value;
}

function formatString(str: string, ...params: {}[]): string {
    if (params.length) {
        const type = typeof params[0];
        const args = ('string' === type || 'number' === type) ? params : params[0];
        for (let key in args) {
            if (args[key]) {
                str = str.replace(new RegExp('\\{' + key + '\\}', 'gi'), args[key]);
            }
        }
    }
    return str;
}