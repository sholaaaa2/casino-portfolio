
export type IDictValue = string | number | boolean;

export interface IDict {
    [k: string]: IDictValue;
}
