/* eslint-disable */
export function initGoogleAnalytics(counterId: string) {
    (function(i: Window, s: Document, o: string, g: string, r: string) {
        let a: HTMLScriptElement;
        let m: Element;
        i['GoogleAnalyticsObject'] = r;
        (i[r] =
            i[r] ||
            function() {
                (i[r].q = i[r].q || []).push(arguments);
            }),
            (i[r].l = new Date().getTime());
        (a = s.createElement('script')), (m = s.getElementsByTagName(o)[0]);
        a.async = true;
        a.src = g;
        m.parentNode!.insertBefore(a, m);
    })(
        window,
        document,
        'script',
        'https://www.google-analytics.com/analytics.js',
        'ga'
    );

    ga('create', counterId, 'auto');
    ga('send', 'pageview');
}

export function initYandexMetrika(counterId: string) {
    (function(m: Window, e: Document, t: string, r: string, i: string) {
        let k: HTMLScriptElement;
        let a: Element;
        m[i] =
            m[i] ||
            function() {
                (m[i].a = m[i].a || []).push(arguments);
            };
        m[i].l = new Date().getTime();
        (k = e.createElement('script')),
            (a = e.getElementsByTagName(t)[0]),
            (k.async = true),
            (k.src = r),
            a.parentNode!.insertBefore(k, a);
    })(window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym');

    window.ym(counterId, 'init', {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true,
        trackHash: true
    });
}

export function initOlark(id: string, startVisible: boolean, groupId: string) {
    (function(o: Window, l: Document, a: string) {
        if (o.olark) { return; }
        const s = l.createElement('script');
        const r = l.getElementsByTagName('script')[0];
        s.async = true;
        s.src = '//' + a;
        r.parentNode!.insertBefore(s, r);
        // tslint:disable-next-line
        let k: any;
        // tslint:disable-next-line
        const y: any = o.olark = function() {
            k.s.push(arguments);
            k.t.push(+new Date());
        };
        y.extend = function(i: string, j: string | boolean) {
            y('extend', i, j);
        };
        y.identify = function(i: string) {
            y('identify', (k.i = i));
        };
        y.configure = function(i: string, j: string) {
            y('configure', i, j);
            k.c[i] = j;
        };
        k = y._ = { s: [], t: [+new Date()], c: {}, l: a };
    })(window, document, 'static.olark.com/jsclient/loader.js');
    /* Add configuration calls below this comment */
    window.olark.identify(id);
    window.olark.configure('system.hb_detached', true);
    window.olark.configure('system.hb_dark_theme', true);
    if (groupId !== '') {
        window.olark.configure('system.group', groupId);
    }
    // window.olark.configure('box.start_hidden', !startVisible);------------------
}

export function initTawk(id: string, id2: string) {
    (function(o: Window, l: Document) {
        if (o.Tawk_API) {
            return;
        }
        const s1 = l.createElement("script");
        const s0 = l.getElementsByTagName("script")[1];
        s1.async = true;
        s1.src = `https://embed.tawk.to/${id}/${id2}`;
        s1.charset = "UTF-8";
        s1.setAttribute("crossorigin", "*");
        s0.parentNode!.insertBefore(s1, s0);
    })(window, document);
}

export function initSmartsupp(id: string) {
    window._smartsupp = window._smartsupp || {};
    window._smartsupp.key = id;
    window.smartsupp||(function(d) {
        var s,c,o: any = window.smartsupp=function(){ o._.push(arguments)};o._=[];
        window.smartsupp('chat:open');//Open support
        window.smartsupp('chat:close'); //Close support
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode!.insertBefore(c,s);
    })(document);
}

export function initOzekon(url: string, id: string, startVisible: boolean) {
    if (!url) { return; }
    (function (o: Window, l: Document, a: string) {
        if (o.owcWidget) { return; }
        if (a) {
            const s = l.createElement('script');
            const r = l.getElementsByTagName('script')[0];
            s.async = true;
            s.src = '//' + a;
            r.parentNode?.insertBefore(s, r);
        }

        // tslint:disable-next-line
        const y: any = o.owcWidget = function () { };
        y.configure = function (i: string, j: string) {
            y.c[i] = j;
        };
        y.set = function () {
            y.s.push(arguments);
        };
        y.c = {};
        y.s = [];
    })(window, document, `${url}${id}`);
    /* Configuration */
    window.owcWidget.configure('box.hidden', !startVisible);
}
