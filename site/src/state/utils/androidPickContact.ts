import {AndroidWebViewMessageType} from '../..';

export const androidPickContact = (): Promise<string> => {
    return new Promise((resolve, reject) => {
        if (window.AndroidWebView?.postMessage) {
            const timeout = window.setTimeout(() => {
                window.AndroidWebViewCB[AndroidWebViewMessageType.PICK_CONTACT] = undefined;
                reject(new Error('AndroidWebView paste cancelled: time out'));
            }, 120000);
            window.AndroidWebViewCB[AndroidWebViewMessageType.PICK_CONTACT] = (message: string) => {
                window.clearTimeout(timeout);
                window.AndroidWebViewCB[AndroidWebViewMessageType.PICK_CONTACT] = undefined;
                resolve(message);
            };
            window.AndroidWebView.postMessage(
                JSON.stringify({
                    type: AndroidWebViewMessageType.PICK_CONTACT,
                }),
            );
        } else {
            reject(new Error('AndroidWebView is undefined'));
        }
    });
};
