import { format } from 'date-fns';
import {NASymbol} from './Money';
import {NLong} from '../meta';

export const formatDateTime = function (value: NLong, isUNIX?: boolean, dateFormat?: string | null, dateTimeFormat?: string | null): string {
    if (value) {
        // * 1000, because Javascript uses milliseconds internally, while normal UNIX timestamps are usually in seconds
        const timestamp = isUNIX ? value.toNumber() * 1000 : value.toNumber();
        const m = new Date(timestamp);

        if (dateFormat && dateTimeFormat) {
            return format(m, dateFormat) + '\n' + format(m, dateTimeFormat);
        }

        if (dateFormat) {
            return format(m, dateFormat);
        }

        if (dateTimeFormat) {
            return format(m, dateTimeFormat);
        }

        return format(m, 'dd.LL.yyyy') + '\n' + format(m, 'HH:mm:ss');
    } else {
        return NASymbol;
    }
};

export const dateFromLongSec = function (value: NLong): null | number {
    return value ? value.toNumber() * 1000 : null;
};

export const timeStampToHHMMSS = function(value: number | undefined | null) {
    let hhmmss = {
        hh: '00',
        mm: '00',
        ss: '00',
    }
    if (value) {
        let sec_num = value / 1000;
        let days: number | string    = Math.floor(sec_num / 86400);
        let hours: number | string   = Math.floor((sec_num - (days * 86400)) / 3600);
        let minutes: number | string = Math.floor((sec_num - (days * 86400) - (hours * 3600)) / 60);
        let seconds: number | string = Math.floor(sec_num - (days * 86400) - (hours * 3600) - (minutes * 60));

        if (days    < 10) {days    = "0"+days.toString();}
        if (hours   < 10) {hours   = "0"+hours.toString();}
        if (minutes < 10) {minutes = "0"+minutes.toString();}
        if (seconds < 10) {seconds = "0"+seconds.toString();}
        hhmmss = {
            hh: hours.toString(),
            mm: minutes.toString(),
            ss: seconds.toString(),
        }
    }
    return hhmmss;
}


export const timeStampToDDHHMMSS = function(value: NLong | undefined | null) {
    if (value && value.toSigned().greaterThan(0)) {
        let sec_num = value.toNumber() / 1000;
        let days: number | string    = Math.floor(sec_num / 86400);
        let hours: number | string   = Math.floor((sec_num - (days * 86400)) / 3600);
        let minutes: number | string = Math.floor((sec_num - (days * 86400) - (hours * 3600)) / 60);
        let seconds: number | string = Math.floor(sec_num - (days * 86400) - (hours * 3600) - (minutes * 60));

        if (days    < 10) {days    = "0"+days.toString();}
        if (hours   < 10) {hours   = "0"+hours.toString();}
        if (minutes < 10) {minutes = "0"+minutes.toString();}
        if (seconds < 10) {seconds = "0"+seconds.toString();}
        return days+':'+hours+':'+minutes+':'+seconds;
    }
    return '00:00:00:00'
}

export const msToTime = function(s: number) {
    function pad(n: number) {
       return ('00' + n).slice(-2);
    }

    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;

    return hrs + ':' + pad(mins) + ':' + pad(secs);
}
