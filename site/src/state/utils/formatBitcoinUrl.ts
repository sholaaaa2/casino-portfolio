import { isAndroid } from "./BrowserUtils";

export const formatBitcoinUrl = (
    wallet: string,
    amount?: string,
): string => {
    if (isAndroid()) {
        if (amount) {
            return `bitcoin://${wallet}?amount=${amount}`;
        } else {
            return `bitcoin://${wallet}`;
        }
    } else {
        if (amount) {
            return `bitcoin:${wallet}?amount=${amount}`;
        } else {
            return `bitcoin:${wallet}`;
        }
    }
};
