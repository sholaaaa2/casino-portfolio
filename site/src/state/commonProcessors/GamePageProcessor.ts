import AppState from "../AppState";

// Common game page callback processor. Please use VendorGamePageProcessor for your specific logic
export default class GamePageProcessor {
    processCommonFrameCallbacks(appState?: AppState) {
        if (appState?.runningGameStore?.iframe?.includes("betsy") && !appState?.appSettings?.disabledFrameProcessors?.includes("betsy")) {
            window.addEventListener('message', ({data}) => {
                try {
                    const messageData = JSON.parse(data);
                    if (messageData.type === "clickPlaceBet" && appState.userInfo.guest) {
                        appState.router.push(appState.l('/login'));
                    }
                } catch {
                }
            });
        }
    }
}