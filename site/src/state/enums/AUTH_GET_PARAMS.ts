export enum AUTH_GET_PARAMS {
    LOGIN = "login",
    EMAIL = "email",
    PHONE = "phone",
    COUNTRY = "country",
}
