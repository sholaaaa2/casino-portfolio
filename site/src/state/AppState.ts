import {format} from 'date-fns';
import {enGB as enLocale, ru as ruLocale} from 'date-fns/locale';
import * as H from 'history';
import * as currencies from './utils/Currencies';
import * as localization from './utils/Localization';
import Long from 'long';
import {action, computed, observable, runInAction} from 'mobx';
import {Link} from 'react-router-dom';
import APIConnector from '../api/APIConnector';
import proto, {bonuses, messages, site, users, wallet} from '../api/proto';
import BonusesActions, {BonusesState} from './actions/BonusesActions';
import GamesActions from './actions/GamesActions';
import RunningGameStore from './actions/games/RunningGameStore';
import TournamentActions, {TournamentHeaderLayout, TournamentType} from './actions/tournaments/TournamentsActions';
import { JPMonitorActions } from './actions/JPMonitorActions';
import { JPMonitorAPI } from '../api/JPMonitorAPI';
import PagesActions from './actions/PagesActions';
import SearchActions from './actions/SearchActions';
import SiteActions from './actions/SiteActions';
import SportActions from './actions/SportActions';
import UserActions, {ConfirmEmailForm, ConfirmEmailSentForm, ConfirmForm, ConfirmRestorePasswordByMailForm, LoginForm, RegisterForm, RestoreEmailForm, RestoreForm} from './actions/UserActions';
import ReactInputMask from 'react-input-mask';
import Slider from './components/Slider';
import {NotificationItemLevel, Notificator} from './components/Notificator';
import UserInfo, {UserBalance} from './UserInfo';
import {
    cookie,
    debounce,
    extractLocationArgs,
    getExtendedParams,
    getLanguagefromUrl,
    getLegacyReferrer,
    getOSType,
    getViewportSize,
    isMobile,
    isMobileOnly,
    ScreenSize,
    Size,
    addCryptoDenominationToParams,
    getMobileOSType,
} from './utils/BrowserUtils';
import * as money from './utils/Money';
import chunkArray from './utils/chunkArray';
import {formatCurrencyAmount, formatCurrencyCode, NASymbol} from './utils/Money';
import Notifications from '../api/Notification';
import JackpotCounter from './components/JackpotCounter';
import {ActiveBlockWinners, AgeOfTheRegisteredUser, AppSettings, GameGroupId, GuestPopupId, Modal, NLong, NString, RegisterFormRuleApprovalMode, UserProfileGroup, ComponentNames, SitePages, POSSIBLE_LOBBY_GAMES} from './meta';
import BillingHistoryState from './actions/BillingHistoryState';
import * as React from 'react';
import {ReactEventHandler} from 'react';
import {JackpotNotifications} from './actions/JackpotNotifications';
import {JackpotWinner} from './actions/JackpotWinner';
import {ModalActions} from './actions/ModalActions';
import {Redirect} from 'react-router';
import Countdown from './components/Countdown';
import ModalJackpot from './components/ModalJackpot';
import {Chat} from './actions/livechats/Chat';
import {ProfileEditActions} from './actions/ProfileEditActions';
import ResendConfirmCode from './components/profile/ResendConfirmCode';
import {ProfileMenuActions} from './actions/ProfileMenuActions';
import {AffiliateProgramActions} from './actions/AffiliateProgramActions';
import GuestPopup from './actions/GuestPopup';
import OptimizedGamesGrid from './components/OptimizedGamesGrid';
import NumPad from './actions/NumPad';
import WrapperScale from './actions/ui/WrapperScale';
import {ProfileBonusesActions} from './actions/ProfileBonusesActions';
import GameListState from './actions/GameListState';
import {GameImageWrapper} from './components/OptimizedGameImage';
import GameScreen from '../ui/GameScreen/GameScreen';
import RefillStore from './payment-systems/RefillStore';
import WithdrawalStore from './payment-systems/WithdrawalStore';
import Paysera from './payment-systems/providers/Paysera';
import GameImageWrap from './components/GameImage';
import {LastWinsActions, LastWinsState, LastActionType} from './actions/LastWinsActions';
import ExpandableContainer from './components/ExpandableContainer';
import VLobbyActions from './actions/VLobbyActions';
import CurrencySelectState from './controls/CurrencySelectState';
import PhoneInputState from './controls/PhoneInputState';
import PasswordInputState from './controls/PasswordInputState';
import EmailInputState from './controls/EmailInputState';
import SignUpActions from './actions/authorization/SignUpActions';
import SignInActions from './actions/authorization/SignInActions';
import LoginInputState from './controls/LoginInputState';
import {AuthorizationType, AuthorizationTypeState} from './actions/authorization/AuthorizationTypeState';
import AcceptTermsAndConditionsState from './controls/AcceptTermsAndConditionsState';
import UserBonusesStore from './actions/UserBonusesStore';
import {PageProps} from '../App';
import LiveCasinoActions from './actions/LiveCasinoActions';
import {GameMenu} from './actions/GameMenu';
import {Bonuses} from './components/Bonuses';
import {Clock} from './components/Clock';
import {ProfileDocumentsActions} from './actions/ProfileDocumentsActions';
import NumberFormat from 'react-number-format';
import classNames from 'classnames';
import {default as ReactSlick} from 'react-slick';
import ReactSlickWithCustomNav from './components/final-bonus/ReactSlickWithCustomNav';
import { FinalBonuses } from './actions/final-bonus/FinalBonuses';
import { DailyBonus } from './actions/daily-bonus/DailyBonus';
import SkillWheelActions from './actions/SkillWheelActions';
import UISkillWheel from './actions/ui/SkillWheel';
import FreespinsShopActions from './actions/FreespinsShopActions';
import {WagerBonusProcessor, CaseType} from "./actions/WagerBonusProcessor";
import {BonusGroups, BonusType, BonusFamily} from "../api/utils";
import WagerBonusActions from "./actions/WagerBonusActions";
import {PoppedBonusProcessor} from './actions/bonuses/PoppedBonus';
import {RefSystemActions} from "./actions/RefSystemActions";
import { initUrlTracking } from '../utils/url-tracker';
import { AHref } from './components/AHref';
import GameMetaIconActions from './actions/GameMetaIconActions';
import GameMetaIcon from './components/GameMetaIcon';
import SocialAuthActions from './actions/authorization/SocialAuthActions';
import SmsNumberInputState from './controls/SmsNumberInputState';
import {RefillRedeemActions} from './actions/RefillRedeemActions';
import {GeolocationActions} from './actions/GeolocationActions';
import FirebaseAppStore from './firebase/FirebaseAppStore';
import PinCodeInputState from './controls/PinCodeInputState';
import LowBalancePopupActions from './actions/LowBalancePopupActions';
import {BalanceWagerBonusProcessor} from './actions/BalanceWagerBonusProcessor';
import BalanceWagerBonusActions from './actions/BalanceWagerBonusActions';
import {PaywayFieldType} from './payment-systems/fields/PaywayField';
import { MainMenu } from './actions/MainMenu';
import { ConfirmationCodeMethodState } from './controls/ConfirmationCodeMethodState';
import { Balance } from './components/Balance';
import LobbyGameActions from './actions/LobbyGameActions';
import { ConfirmCashappPhone } from './actions/ConfirmCashappPhone';
import TimelineCashbackBonus from './actions/bonuses/TimelineCashback';
import {LobbyState} from './actions/LobbyActions';
import { WebPushAPI } from '../api/WebPushAPI';
import { WebPushActions } from './actions/WebPushActions';
import WebpushNotifications from './components/NotificationWebpush';
import TournamentsInfoActions from './actions/tournaments/TournamentsInfoActions';
import { JackpotState } from './actions/jackpots/JackpotState';
import { ApiErrorLinkType } from './payment-systems/forms/PaywayForm';

const appSettings = require('../AppSettings.json') as AppSettings;
const ImagesLoader = require('images-loader');

const SUPPORTED_LOCALES = {
    ru: ruLocale,
    en: enLocale
};

export default class AppState {
    appSettings: AppSettings = appSettings;
    proto = proto;

    // actions
    chat: Chat = new Chat(this);
    site: SiteActions = new SiteActions(this);
    vlobbyActions: VLobbyActions = new VLobbyActions(this);
    search: SearchActions = new SearchActions(this);
    pages: PagesActions = new PagesActions(this);
    jackpots: JPMonitorActions = new JPMonitorActions(this);
    lastWinsActions: LastWinsActions = new LastWinsActions(this);
    jackpotWinners: JackpotWinner = new JackpotWinner(this);
    jackpotNotifications: JackpotNotifications = new JackpotNotifications(this);
    gameList: GameListState = new GameListState(this);
    games: GamesActions = new GamesActions(this);
    bonuses: BonusesActions = new BonusesActions(this);
    user: UserActions = new UserActions(this);
    sport: SportActions = new SportActions(this);
    profileMenu: ProfileMenuActions = new ProfileMenuActions(this);
    profileEdit: ProfileEditActions = new ProfileEditActions(this);
    profileBonuses: ProfileBonusesActions = new ProfileBonusesActions(this);
    affiliateProgram: AffiliateProgramActions = new AffiliateProgramActions(this);
    profileDocuments: ProfileDocumentsActions = new ProfileDocumentsActions(this);
    modal: ModalActions = new ModalActions(this);
    numPad: NumPad = new NumPad(this);
    expandableContainer: ExpandableContainer = new ExpandableContainer(this);
    guestPopup: GuestPopup = new GuestPopup(this);
    tournamentsActions: TournamentActions = new TournamentActions(this);
    tournamentsInfoActions: TournamentsInfoActions = new TournamentsInfoActions(this);
    signUpActions: SignUpActions = new SignUpActions(this);
    signInActions: SignInActions = new SignInActions(this);
    authorizationTypeState: AuthorizationTypeState = new AuthorizationTypeState(this);
    userBonusesStore: UserBonusesStore = new UserBonusesStore(this);
    runningGameStore: RunningGameStore = new RunningGameStore(this);
    liveCasino: LiveCasinoActions = new LiveCasinoActions(this);
    mainMenu = new MainMenu(this);
    gameMenu: GameMenu = new GameMenu(this);
    freespinsShopActions: FreespinsShopActions = new FreespinsShopActions(this);
    ref = new RefSystemActions(this);
    gameMetaIconActions: GameMetaIconActions = new GameMetaIconActions(this);
    social = new SocialAuthActions(this);
    refillRedeem: RefillRedeemActions = new RefillRedeemActions(this);
    geo: GeolocationActions = new GeolocationActions(this);
    firebaseApp = new FirebaseAppStore(this);
    lowBalancePopup = new LowBalancePopupActions(this);
    lobbyGameActions = new LobbyGameActions(this);
    confirmCashappPhone = new ConfirmCashappPhone(this);
    webPushActions: WebPushActions = new WebPushActions(this);

    skillWheel: SkillWheelActions = new SkillWheelActions(this);

    // payment systems v2
    refillStore: RefillStore = new RefillStore(this);
    withdrawalStore: WithdrawalStore = new WithdrawalStore(this);

    // additional for payments
    successPaymentProvider?: string;

    // states
    billingHistory: BillingHistoryState = new BillingHistoryState(this);

    api: APIConnector = new APIConnector(this);
    webPushAPI: WebPushAPI = new WebPushAPI(
        this,
        this.webPushActions.onMessage
    );
    notifications: Notifications = new Notifications(
        this,
        this.onSSENotification.bind(this)
    );
    jpMonitorAPI: JPMonitorAPI = new JPMonitorAPI(
        this,
        this.jackpots.handleResponse
    );
    imagesLoadingMap: { [key: string]: Size } = {};

    money = money;
    currencies = currencies;

    isLocalhost: boolean = window.location.hostname === 'localhost';
    lobby = new LobbyState(this);

    // ui
    ui = {
        scale: new WrapperScale(this),
        skillWheel: new UISkillWheel(this),
    };

    // mobile or tablet
    isMobile = isMobile();
    // only mobile
    isMobileOnly = isMobileOnly() || navigator.platform === 'iPhone';

    OSType: string = getOSType();
    mobileOSType?: string = getMobileOSType();
    notificator: Notificator | null = null;
    router: H.History;
    Link = Link;
    Redirect = Redirect;
    GameImage = GameImageWrap;
    OptimizedGameImage = GameImageWrapper;
    OptimizedGamesGrid = OptimizedGamesGrid;
    ResendConfirmCode = ResendConfirmCode;
    Slider = Slider;
    ModalJackpot = ModalJackpot;
    InputMask = ReactInputMask;
    GameScreen = GameScreen;
    ReactSlick = ReactSlick;
    ReactSlickWithCustomNav = ReactSlickWithCustomNav;
    GameMetaIcon = GameMetaIcon;
    classNames = classNames;

    WebpushNotifications = WebpushNotifications;

    Countdown = Countdown;
    JackpotCounter = JackpotCounter;
    Bonuses = Bonuses;
    Clock = Clock;
    NumberFormat = NumberFormat;
    AHref = AHref;
    Balance = Balance;

    // enums
    enums = {
        modal: Modal,
        guestPopup: GuestPopupId,
        userProfileGroup: UserProfileGroup,
        activeBlockWinners: ActiveBlockWinners,
        gameGroupId: GameGroupId,
        withdrawProvider: wallet.WithdrawProvider,
        refillType: wallet.RefillProvidersResponse.RefillType,
        bonusState: bonuses.BonusCode.State,
        bonusFreeSpinsState: bonuses.FreeSpinsState,
        ageOfTheRegisteredUser: AgeOfTheRegisteredUser,
        registerFormRuleApprovalMode: RegisterFormRuleApprovalMode,
        authorizationType: AuthorizationType,
        tournamentHeaderLayout: TournamentHeaderLayout,
        tournamentType: TournamentType,
        sitePages: SitePages,
        wagerBonusCaseType: CaseType,
        bonusType: BonusType,
        bonusGroups: BonusGroups,
        bonusFamily: BonusFamily,
        paywayFieldType: PaywayFieldType,
        lastActionType: LastActionType,
        apiErrorLinkType: ApiErrorLinkType,
    };

    langRe: RegExp = /lang\.([a-z-]+)/i;
    initialized: boolean = false;
    @observable language: string = '';
    @observable siteConfig: site.ISiteConfigResponse;
    @observable userInfo: UserInfo = new UserInfo(this);
    @observable rawUserInfo: users.IUserInfo | null = null;

    @computed
    get userTags(): string[] {
        if (this.rawUserInfo && this.rawUserInfo.tags) {
            return this.rawUserInfo.tags;
        }
        return [];
    }

    @computed
    get logoutAvailable(): boolean {
        return this.userTags.indexOf('wl-device') === -1;
    }

    @observable fatal: string | null = null;
    @observable loading: boolean = true;
    @observable initialSSELoading: boolean = true;
    @observable mobileMenuVisible: boolean = false;

    // search
    @observable searchOpened: boolean = false;
    @observable searchValue: string = '';

    // static pages
    @observable staticPageLoading = false;
    @observable staticPageId: string | null = null;
    @observable staticPageContent: { __html: null | string } = {__html: null};

    @observable page: SitePages | null = null;
    @observable routeParams: { [k: string]: string | undefined } | null = null;

    // billing
    @observable changeUserHeaderVisible: boolean = false;

    // controls
    @observable phoneInputState: PhoneInputState = new PhoneInputState(this);
    @observable loginInputState: LoginInputState = new LoginInputState(this);
    @observable emailInputState: EmailInputState = new EmailInputState(this);
    @observable passwordInputState: PasswordInputState = new PasswordInputState(this);
    @observable currencySelectState: CurrencySelectState = new CurrencySelectState(this);
    @observable acceptTermsAndConditionsState: AcceptTermsAndConditionsState = new AcceptTermsAndConditionsState(this);
    @observable smsNumberInputState: SmsNumberInputState = new SmsNumberInputState(this);
    @observable pinCodeInputState: PinCodeInputState = new PinCodeInputState(this);
    confirmationCodeMethodState = new ConfirmationCodeMethodState(this);

    // user
    @observable loginType: string = 'email';
    @observable loginForm: LoginForm = new LoginForm();
    @observable registerForm: RegisterForm = new RegisterForm();
    @observable restoreForm: RestoreForm = new RestoreForm();
    @observable confirmForm: ConfirmForm = new ConfirmForm(this);
    @observable confirmEmailForm: ConfirmEmailForm = new ConfirmEmailForm();
    @observable confirmEmailSentForm: ConfirmEmailSentForm = new ConfirmEmailSentForm();
    @computed
    get desktopAuthMode(): boolean {
        return !!(
            this.appSettings.registerForm.desktopAuthMode &&
            this.viewport.width >= 960 &&
            (
                this.appSettings.registerForm.desktopAuthPIN ||
                this.appSettings.registerForm.desktopAuthLogin ||
                this.appSettings.registerForm.desktopAuthRegistration
            )
        );
    }
    @computed
    get screenSaverMode(): boolean {
        return !!this.appSettings.registerForm.screenSaverMode;
    }

    @observable confirmRestorePasswordByMailForm: ConfirmRestorePasswordByMailForm = new ConfirmRestorePasswordByMailForm();
    @observable restoreEmailForm: RestoreEmailForm = new RestoreEmailForm();

    // top wins
    @observable activeBlockWinners: ActiveBlockWinners | null = this.appSettings.winners.defaultTab || null;
    topWins: LastWinsState = new LastWinsState(this, 'winners-top');
    lastWins: LastWinsState = new LastWinsState(this, 'winners-last');
    billingActions: LastWinsState = new LastWinsState(this, 'winners-last');

    // jackpots
    jp: JackpotState = new JackpotState(this, undefined);
    @observable jackpotsList: JackpotState[] = [];
    // bonuses
    @observable bonusesInfo: BonusesState = new BonusesState(this);
    finalBonuses: FinalBonuses = new FinalBonuses(this);
    dailyBonus: DailyBonus = new DailyBonus(this);
    wagerBonusProcessor: WagerBonusProcessor = new WagerBonusProcessor(this);
    wagerBonus: WagerBonusActions = new WagerBonusActions(this);
    poppedBonus: PoppedBonusProcessor = new PoppedBonusProcessor(this);
    balanceWagerProcessor: BalanceWagerBonusProcessor = new BalanceWagerBonusProcessor(this);
    balanceWager: BalanceWagerBonusActions = new BalanceWagerBonusActions(this);
    timelineCashbackBonus: TimelineCashbackBonus = new TimelineCashbackBonus(this);

    @observable viewport: Size = getViewportSize();

    public refs: { [k: string]: HTMLElement } = {};

    constructor() {
        this.onPageClick = this.onPageClick.bind(this);
        this.changeCurrency = this.changeCurrency.bind(this);
        this.closeUserHeaderDialog = this.closeUserHeaderDialog.bind(this);
        this.toggleChangeUserHeaderDialog = this.toggleChangeUserHeaderDialog.bind(this);
    }

    format(date: Date | null | undefined, formatStr: string): string {
        if (date) {
            let locale = SUPPORTED_LOCALES.en;
            if (this.language === 'ru') {
                locale = SUPPORTED_LOCALES.ru;
            }
            return format(date, formatStr, {locale: locale});
        } else {
            return NASymbol;
        }
    }

    t = localization.translate;

    l(link: string, language: string = this.language): string {
        if (link.indexOf('http') === 0) {
            return link;
        }
        let result = link;

        // clear lang if already set
        const localesGroupRe = this.localesGroupsRe;

        // remove lang from url like /ru/refill
        let match = link.match('^/' + localesGroupRe + '(/(.*))'); // e.g ^(en|es|ru)(/(.*))
        if (match) {
            result = match[2] ? match[2] : '/';
        } else {
            match = link.match('^/' + localesGroupRe + '$'); // e.g ^(en|es|ru)
            if (match) {
                result = '/';
            }
        }

        // return cleared link if site config not loaded or current language is default
        if (!this.siteConfig || language === this.siteConfig.defaultLanguage) {
            return result;
        }

        // special case for /
        if (result === '/') {
            return '/' + language;
        }

        // return localized url
        return '/' + language + result;
    }

    g(key: string): string {
        if (this.appSettings.globalVars[key]) {
            return this.appSettings.globalVars[key];
        } else {
            if (this.isLocalhost) {
                console.warn('Missing global variable for key: ', key, ' please, check AppSettings.json file.');
            }
            return '';
        }
    }

    chunkArray = chunkArray;

    startInitialize(props: PageProps, onFinish: Function, componentName?: ComponentNames) {
        this.routeParams = props.match.params;
        this.router = props.history;


        if (this.site.isAccessRestricted && componentName !== ComponentNames.LOGIN_PAGE) {
            this.router.push(this.l('/login'));
        } else if (!this.initialized) {
            // start batch initialization request
            this.api.startBatch();
            initUrlTracking(props);

            // change language from url, this is required due to API bug
            const lang = getLanguagefromUrl();
            if (lang) {
                this.api.changeLanguage(lang, () => null);
            }

            // initialize app
            const params = addCryptoDenominationToParams(getExtendedParams());
            const referrer = getLegacyReferrer();
            this.api.initialize(referrer, params, this.onInitialize.bind(this, onFinish));
        } else {
            onFinish();
        }
    }

    finishInitialize() {
        if (!this.initialized) {
            this.initialized = true;

            // commit initialization request
            this.api.commit();

            // todo: rework this with route
            if (window.location.pathname.endsWith('/restore')) {
                this.showRestore();
            }

            // preload important images if any
            const imagesLoadingList = this.appSettings.preloadImages || [];
            const loader = new ImagesLoader();
            for (let image of imagesLoadingList) {
                loader.load(image, this.onImageLoaded.bind(this, image));
            }
        }
    }

    @action
    onInitialize(onFinish: Function, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.processInitialSiteConfig(msg, act);
        this.setRefHit();
        this.processApi(msg, act);
        this.chat.init();
        this.notifications.init();
        this.webPushAPI.init();
        this.jpMonitorAPI.check();
        if (!this.skillWheel.isEnabled) this.loading = false;
        this.addGlobalListeners();
        if (this.userInfo.guest) {
            this.guestPopup.init();
        } else {
            this.site.showIOSAddToHomescreenBlockIfPossible();
        }
        this.games.onFetchGames(msg, act);
        if (this.appSettings.loadBonusesOnInit) {
            this.userBonusesStore.fetchBonuses();
            this.finalBonuses.fetchData();
            this.dailyBonus.fetchData(this.dailyBonus.openModalOnInit);
            this.wagerBonus.fetchData(this.wagerBonus.openModalOnInit);
            //this.balanceWager.fetchData(this.balanceWager.openModalOnInit);
        }

        if (this.appSettings.registerForm && this.appSettings.registerForm.formVersion === 'v3' && !this.isGameRoute() && window.location.pathname.search('/auth/') === -1) {
            this.router.push(this.l('/login'));
        }

        this.firebaseApp.init(this.siteConfig.firebaseConfig);

        if (this.user.isKioskLobbyEnabled) {
            this.lobby.startIdleDetection();
        }

        onFinish();
    }

    processInitialSiteConfig(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        if (act.siteConfig) {
            if (act.siteConfig.currencies) {
                currencies.setCurrenciesInfo(act.siteConfig.currencies, appSettings.currencies);
            }

            if (act.siteConfig.translations) {
                localization.setTranslations(act.siteConfig.translations);
            }

            if (act.siteConfig.lowBalancePopup) {
                this.lowBalancePopup.setConfig(act.siteConfig.lowBalancePopup);
            }
        } else {
            console.error('Missing siteConfig in initial request');
        }
    }

    addGlobalListeners() {
        document.body.addEventListener('keydown', (event: KeyboardEvent) => {
            // is esc
            if (event.keyCode === 27) {
                this.onEscHandler();
            }
            if  ((event.ctrlKey || event.metaKey) && event.keyCode === 70) {
                this.search.proceedCtrlF(event);
            }
        });
    }

    onEscHandler() {
        this.closeUserHeaderDialog();
    }

    @action
    onImageLoaded(image: string, error: string, size: Size) {
        if (error) {
            size = {width: 0, height: 0};
            console.error('Failed to load image: ', image);
        } else {
            this.imagesLoadingMap[image] = size;
            console.debug('Image loaded: ', image, size);
        }
    }

    processApi(message: messages.IServerResponse, act: messages.IClientActionResponse, isAfterLogin?: boolean): void {
        if (message.language) {
            this.language = message.language;
            localization.setCurrentLanguage(message.language);
        }

        if (act.siteConfig) {
            let isShouldLoadBonuses = !!this.appSettings.mainMenu?.showAvailableBonusesCount;
            this.siteConfig = act.siteConfig;
            this.games.gamesList = act.siteConfig.games!;
            this.games.providers = new Set(act.siteConfig.games!.map(game => game.provider!));
            this.currencySelectState.setCurrencies(act.siteConfig.currencies);
            this.phoneInputState.setCountries(act.siteConfig.countries);

            if (this.siteConfig.lobby) {
                this.vlobbyActions.domain = this.siteConfig.lobby;
            }

            if (this.siteConfig.loginType) {
                this.loginType = this.siteConfig.loginType;
            }

            if (this.siteConfig.billingSettings) {
                this.processBillingSettings(this.siteConfig.billingSettings);
            }

            if (this.siteConfig.socialProviders) {
                this.social.processProviders(this.siteConfig.socialProviders);
            }

            if (this.siteConfig.lowBalancePopup) {
                this.lowBalancePopup.setConfig(this.siteConfig.lowBalancePopup)
            }

            this.gameMenu.upgrade();
            this.mainMenu.upgrade();

            if (act.siteConfig.userProfile?.refSystem && this.site.showInvitationBonusesInfoInBonuses) {
                this.ref.loadBonusInfo();
            }

            if (isAfterLogin) {
                this.finalBonuses.fetchData(this.finalBonuses.openModalOnLogin);
                this.dailyBonus.fetchData(this.dailyBonus.openModalOnLogin);
                this.wagerBonus.fetchData(this.wagerBonus.openModalOnLogin);
                this.user.redirectToIncomingUrl();
                if (this.page === SitePages.BONUSES) {
                    isShouldLoadBonuses = true;
                }
            }
            this.userInfo.checkRedirectUrl();
            if (isShouldLoadBonuses) {
                this.profileBonuses.tryLoadBonusesData();
            }
        }

        this.fatal = message.configError ?? null;

        if (message.sessionId || message.user) {
            this.processUserInfo(message.sessionId, message.user, message.currencyId);
        }

        if (act.siteConfig) {
            this.confirmationCodeMethodState.upgrade();
        }
    }

    @computed
    get hasUserFields(): boolean {
        const userFields = this.siteConfig.userFields;
        return !!(userFields ? userFields.length > 0 : false);
    }

    @action
    onSSENotification(message: messages.SSENotifcation) {
        this.initialSSELoading = false;

        if (message.personal) {
            this.processPersonalNotification(message.personal);
        }
    }

    @action
    processPersonalNotification(message: messages.IPersonalNotification) {
        // if (message.language) {
        //     this.language = message.language;
        // }

        if (message.logout) {
            this.modal.showSessionStartedOnDifferentDevice();
        }

        // if (message.autoLogout) {
        //     this.modal.showAutoLogoutAfterIdle();
        // }

        // if (message.keepAlivePing) {
        //     this.modal.showKeepAlivePing();
        // }

        if (message.sessionId || message.user) {
            this.processUserInfo(message.sessionId, message.user, message.currencyId);
        }

        if (message?.user?.tags) {
            this.processUserTagsOnSSE(message.user.tags);
        }

        if (message.updatedBalance) {
            this.processUserBalances(message.currencyId ? message.currencyId : Long.ZERO, [message.updatedBalance], true);
            if (message.updatedBalance.updateBillingHistory) {
                this.billingHistory.loadHistory();
            }
            this.lowBalancePopup.showIfNeeded();
        }

        if (message.billing && message.billing.length) {
            this.processBilling(message.billing);
            //this.balanceWager.showIfNeededAfterRefill();
        }

        if (message.withdrawalNotification && message.withdrawalNotification.length) {
            this.processWithdrawalNotifications(message.withdrawalNotification)
        }

        if (message.bonusesState) {
            const played = message.bonusesState.played;
            const total = message.bonusesState.total;
            if (played && total) {
                this.bonuses.processBonusesState(played, total);
            }
        }

        if (message.bonusCodes && message.bonusCodes.length) {
            this.bonuses.processBonusesNotifications(message.bonusCodes);
            this.profileBonuses.onBonusNotification();
        }

        if (message.updatedBonusCodes && message.updatedBonusCodes.length) {
            this.bonuses.processUpdatedBonusCodes(message.updatedBonusCodes);
        }

        if (message.notifications) {
            for (let notification of message.notifications) {
                switch (notification.type) {
                    case messages.PersonalNotification.NotificationType.WECASHUP_SUCCESS:
                        this.notificator!.addNotification({
                            level: NotificationItemLevel.SUCCESS,
                            message: this.t('ui-refill-created')
                        });
                        break;
                    case messages.PersonalNotification.NotificationType.WECASHUP_FAIL:
                        this.notificator!.addNotification({
                            level: NotificationItemLevel.SUCCESS,
                            message: this.t('ui-refill-failed')
                        });
                        break;
                    default:
                        console.warn('Unknown personal notification with type: ', notification.type);
                        break;
                }
            }
        }

        if (message.cryptoAddress) {
            this.refillStore.cryptoAddressForm.updateCryptoAddressFromSSE(message.cryptoAddress);
        }

        if (message.billing && message.billing.length) {
            window.setTimeout(() => {
                this.finalBonuses.fetchData();
            }, 1000);
        }

        if (message.resetToSite) {
            window.location.href = message.resetToSite;
        }

        if (message.refSystemStatusChanged) {
            this.ref.handleStatusChangeEvent();
        }

        if (message.withdrawalLink) {
            this.withdrawalStore.withdrawalLinkForModal = message.withdrawalLink;
            this.modal.showWithdrawalSkycryptoLinkModal();
        }
    }

    @action
    processUserInfo(sessionId: NString, user: users.IUserInfo | null | undefined, currencyId: NLong) {
        if (sessionId) {
            if (this.userInfo.sid && this.userInfo.sid !== sessionId) {
                this.notifications.reconnect(true);
                this.webPushAPI.reconnect(true);
                this.site.reloadStateForNewSession();
                this.site.showIOSAddToHomescreenBlockIfPossible();
            }
            if (this.userInfo.sid !== sessionId && this.site.isFavoritesGamesAllowed) {
                this.games.fetchFavoriteGames();
            }
            this.userInfo.sid = sessionId;
        }

        if (user) {
            // process user info
            // const userInfo = new UserInfo(this, sessionId);
            let login: string;
            this.userInfo.userId = user.userId!;
            this.userInfo.guest = user.registrationLevel === users.UserInfo.UserRegistrationLevel.GUEST;
            if (this.userInfo.guest) {
                login = this.t('ui-guest', this.userInfo.userId.toString());
            } else {
                if (user.phone) {
                    login = user.phone;
                } else if (user.email) {
                    login = user.email;
                } else if (user.login) {
                    login = this.t('ui-user', user.login);
                } else {
                    login = this.t('ui-user', this.userInfo.userId);
                }
            }
            this.userInfo.login = login;

            if (user.restrictions) {
                this.userInfo.restrictions = user.restrictions;
            }

            if (user.providerRestrictions) {
                this.userInfo.providerRestrictions = user.providerRestrictions;
            }

            this.rawUserInfo = user;
            //this.userInfo = userInfo;

            // process user balances
            if (user.balances) {
                this.processUserBalances(currencyId ? currencyId : Long.ZERO, user.balances);
            }

            this.chat.updateInfo();

            if (this.screenSaverMode && !this.userInfo.guest && this.modal.activeModal === Modal.LOGIN) {
                this.modal.activeModal = null;
            } else if (this.appSettings.registerForm && this.appSettings.registerForm.formVersion === 'v3' && !this.userInfo.guest && this.modal.activeModal === Modal.LOGIN && !this.siteConfig.entryConfig?.entryByPhone?.tiePinCodeToPhone) {
                this.modal.activeModal = null;
            }
        }
    }

    @action
    processUserBalances(currency: Long, balances: users.IBalanceInfo[], isUpdate: boolean = false) {
        const newBalances = balances!.map(balance => {
            const balanceCurrency = currencies.findCurrencyById(balance.currencyId);

            if (balanceCurrency == null) {
                return null;
            }

            return {
                id: balance.id,
                currency: balanceCurrency,
                currencyId: balanceCurrency.id,
                currencyCode: balanceCurrency.code,
                availableValue: balance.available,
                available: formatCurrencyAmount(balance.available, balanceCurrency.id, true),
                isFiat: balanceCurrency.isFiat,
                formatted: formatCurrencyAmount(balance.available, balanceCurrency)
            };
        }).filter(balance => balance) as UserBalance[];

        if (isUpdate) {
            const result = this.userInfo.balances.slice();
            for (let b of newBalances) {
                let cb = result.find(balance => b.id.eq(balance.id)) as UserBalance;
                let index = result.indexOf(cb);
                result[index] = b;
            }
            this.userInfo.balances = result;
        } else {
            this.userInfo.balances = newBalances;
        }

        this.userInfo.balance =
            this.userInfo.balances.find(balance => balance.currencyId!.eq(currency)) ||
            this.userInfo.balances.find(balance => balance.currencyId!.eq(Long.ZERO)) ||
            {available: '', formatted: '', currencyCode: '', currency: null, currencyId: Long.ZERO, availableValue: Long.ZERO, id: Long.ZERO, isFiat: true};

        this.userInfo.realBalance = this.userInfo.balances.find(balance => balance.currencyId!.greaterThan(Long.ZERO));
        if (
            this.user.isFromKiosk && !!this.userInfo.realBalance?.availableValue?.isZero()
        ) {
            this.user.backToLobby();
        }
        this.userInfo.balance.active = true;
        this.userInfo.currency = this.userInfo.balance.currency;
        this.userInfo.balanceString = this.userInfo.balance.formatted;
        this.userInfo.balanceStringWithoutCurrency = this.userInfo.balance.available;
        this.userInfo.currencyString = this.userInfo.balance.currencyCode;
    }

    @action
    processUserTagsOnSSE(tags: string[]) {
        if (
            tags.includes("withdrawals-actions-only") &&
            tags.includes("wager-bonus-by-reg-force-redeem") &&
            this.modal.activeModal !== Modal.POSITIVE_REG_WAGER_BONUS_CLOSING
        ) {
            setTimeout(() => {
                if (this.page !== SitePages.WITHDRAW && !this.bonusesInfo.bonusModalNotification) {
                    this.userBonusesStore.fetchBonuses((bonusCodes) => {
                        for (const bonusCode of bonusCodes) {
                            if (bonusCode.meta?.bonus_type === BonusType.REG_WAGER_FORCE_REDEEM) {
                                this.bonuses.processPositiveRegWagerBonusClosing(bonusCode.meta);
                            }
                        }
                    });
                }
            }, 500);
        }
    }

    @observable lastBillingNotificationIds: string[] = [];

    @action
    processBilling(billing: users.IBillingNotification[]) {
        const notificationsIds: string[] = [];
        billing.forEach(n => {
            if (this.notificator && (!n.id || !this.lastBillingNotificationIds.includes(n.id))) {
                const amount = formatCurrencyAmount(n.amount, n.currency, true);
                const currencyCode = formatCurrencyCode(n.currency);
                if (n.id) notificationsIds.push(n.id);
                let notification = null;

                switch (n.type) {
                    case users.BillingNotification.NotificationType.refill_success:
                        notification = {
                            level: NotificationItemLevel.SUCCESS,
                            message: this.t('ui-refill-successful', {amount, currency: currencyCode})
                        };
                        break;

                    case users.BillingNotification.NotificationType.withdrawal_success:
                        notification = {
                            level: NotificationItemLevel.SUCCESS,
                            message: this.t('ui-withdrawal-created')
                        };
                        break;

                    case users.BillingNotification.NotificationType.withdrawal_failed:
                        notification = {
                            level: NotificationItemLevel.ERROR,
                            message: this.t('ui-withdrawal-failed')
                        };
                        break;
                    default:
                        console.error('Unknown billing notification type: ', n.type);
                }
                if (notification) {
                    this.notificator.addNotification(notification);
                }
            }
        });
        this.lastBillingNotificationIds = notificationsIds;
    }

    @action.bound
    processWithdrawalNotifications(notifications: users.IWithdrawalBillingNotification[]) {
        if (this.notificator) {
            const notificator = this.notificator;
            notifications.forEach(n => {
                const amount = formatCurrencyAmount(n.amount, n.currency, true);
                const currencyCode = formatCurrencyCode(n.currency);

                let notification = null;

                const payway = this.withdrawalStore.payways.find(p => p.getProvider().canProcessWithdrawalNotification(n));
                if (payway) {
                    notification = payway.getProvider().processWithdrawalNotification(n, amount, currencyCode);
                }

                if (notification) {
                    notificator.addNotification(notification);
                }

            });

        }
    }

    processBillingSettings(settings: site.IBillingSettings) {
        if (settings.payseraVerification) {
            Paysera.processBillingSettings(settings.payseraVerification);
        }
    }

    showRestore() {
        let {code, email} = extractLocationArgs();

        this.restoreEmailForm.email = email;
        this.restoreEmailForm.code = code;
    }

    changeCurrency(currencyId: Long): ReactEventHandler<HTMLElement> {
        return (event) => {
            event.preventDefault();
            event.stopPropagation();
            this.loading = true;
            this.api.changeCurrency(currencyId, (msg, act) => {
                this.loading = false;
                this.processApi(msg, act);
                this.changeUserHeaderVisible = false;
            });
        };
    }

    @action
    toggleChangeUserHeaderDialog(event?: React.SyntheticEvent<HTMLElement>) {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (this.userInfo && this.userInfo.balances) {
            if (this.userInfo.balances.length > 1) {
                this.changeUserHeaderVisible = !this.changeUserHeaderVisible;
                if (this.changeUserHeaderVisible) {
                    this.userBonusesStore.isBonusBalanceInfoVisible = false;
                    if (this.userBonusesStore.timeToLeftWageringInterval) clearInterval(this.userBonusesStore.timeToLeftWageringInterval)
                }
            } else if (this.profileMenu.profileMenuItems.length) {
                this.router.push(this.l('/profile/edit'));
            }
        }
    }

    onPageClick() {
        this.closeUserHeaderDialog();
    }

    closeUserHeaderDialog() {
        if (this.changeUserHeaderVisible) {
            this.toggleChangeUserHeaderDialog();
        }
        if (this.userBonusesStore.isBonusBalanceInfoVisible) {
            this.userBonusesStore.toggleBonusBalanceInfo();
        }
    }

    @action
    resetForms() {
        this.signInActions.resetForm();
        this.signUpActions.resetForm();
        this.loginForm.reset();
        this.registerForm.reset();
        this.restoreForm.reset();
        this.confirmForm.reset();
        this.confirmEmailForm.reset();
        this.confirmEmailSentForm.reset();
        this.confirmRestorePasswordByMailForm.reset();
        this.phoneInputState.resetForm();
    }

    @computed
    get referrer(): string | null {
        const currentReferer = cookie('wlref');
        const browserReferer = document.referrer;
        const urlReferer = window.location.search.match(/ref=([a-z0-9-_]+)/i);
        let ref: string | null = null;
        if (currentReferer) {
            ref = currentReferer.replace('ref:', '');
        } else if (urlReferer) {
            ref = urlReferer[1];
        } else if (browserReferer && browserReferer !== window.location.origin) {
            ref = browserReferer;
        }
        return ref || null;
    }

    @computed
    get isApk(): boolean {
        return this.siteConfig?.applicationType === site.SiteConfigResponse.ApplicationType.MOBILE_APP;
    }

    setRefHit() {
        // new refs flow
        const currentReferer = cookie('wlref');
        const browserReferer = document.referrer;
        const urlReferer = window.location.search.match(/ref=([a-z0-9-_]+)/i);
        if (currentReferer) {
            this.api.sendHit(currentReferer);
        } else if (urlReferer) {
            this.api.sendHit('ref:' + urlReferer[1]);
        } else if (browserReferer && browserReferer !== window.location.origin) {
            this.api.sendHit(browserReferer);
        } else {
            this.api.sendHit('direct');
        }
    }

    @computed
    get localesGroupsRe(): string {
        let languagesRe = '[a-z]{2}';
        if (this.siteConfig) {
            if (this.siteConfig.languages) {
                languagesRe = this.siteConfig.languages.map(function (lang: site.ILanguageInfo): string {
                    return lang.code || '';
                }).join('|');
            } else if (this.siteConfig.defaultLanguage) {
                languagesRe = this.siteConfig.defaultLanguage;
            }
        }
        return '(' + languagesRe + ')';
    }

    getRef(ref: string): HTMLElement | undefined {
        return this.refs[ref];
    }

    makeRef(ref: string): (el: HTMLElement) => void {
        return (el) => {
            this.refs[ref] = el;
        };
    }

    updateViewportSize = debounce(() => {
        const size = getViewportSize();
        if (this.viewport.width !== size.width || this.viewport.height !== size.height) {
            runInAction(() => {
                this.viewport = size;
            });
        }
    }, 50);

    // calculate modal transform scale
    updateModalWrapperScale(wrapper: HTMLElement) {
        const viewport = this.viewport;
        let verticalOffset = 0;
        let horizontalOffset = 0;
        const minWidthContainerToScale = ScreenSize.MOBILE;

        let maxWrapperWidth = wrapper.clientWidth;
        let maxWrapperHeight = wrapper.clientHeight;

        // walk by all elements and get maxWidth/maxHeight
        const wrapperChildren = wrapper.querySelectorAll('*');
        for (let i = 0; i < wrapperChildren.length; i++) {
            const childrenEl = wrapperChildren[i] as HTMLElement;

            if (childrenEl.clientWidth > maxWrapperWidth) {
                maxWrapperWidth = childrenEl.clientWidth;
            }

            if (childrenEl.clientHeight > maxWrapperHeight) {
                maxWrapperHeight = childrenEl.clientHeight;
            }
        }

        if (viewport.width >= minWidthContainerToScale && !isMobile()) {
            verticalOffset = 100;
            horizontalOffset = 150;
        } else {
            verticalOffset = 25;
            horizontalOffset = 25;
        }

        if (wrapper) {
            const deltaByWidth = (viewport.width - horizontalOffset) / maxWrapperWidth;
            const deltaByHeight = (viewport.height - verticalOffset) / maxWrapperHeight;
            const scale = Math.min(deltaByHeight, deltaByWidth);

            wrapper.style.top = '0px';
            wrapper.style.left = '0px';
            wrapper.style.webkitTransform = 'scale(' + scale + ')';
            wrapper.style.transform = 'scale(' + scale + ')';

            const bounding = wrapper.getBoundingClientRect();
            wrapper.style.top = -bounding.top + (viewport.height - bounding.height) / 2 + 'px';
            wrapper.style.left = -bounding.left + (viewport.width - bounding.width) / 2 + 'px';
        }
    }

    // endregion

    @computed
    get pilotUrl(): string {
        const pilotSettings = this.siteConfig.pilotSettings;
        if (pilotSettings) {
            return pilotSettings.url || "not_found";
        } else {
            return "/not_found";
        }
    }

    isGameRoute() {
        if (
            window.location.pathname.indexOf('/game/') >= 0 ||
            POSSIBLE_LOBBY_GAMES.some((g) => window.location.pathname.indexOf('/' + g) >= 0) ||
            window.location.pathname.indexOf('/tvbet') >= 0 ||
            window.location.pathname.indexOf('/pilot') >= 0 ||
            window.location.pathname.indexOf('/lobby') >= 0 ||
            window.location.pathname.indexOf('/sp2-page') >= 0
        ) {
            return true;
        }
        return false;
    }
}
