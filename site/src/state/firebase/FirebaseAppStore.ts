import {initializeApp, FirebaseOptions} from 'firebase/app';
import {getMessaging, getToken, deleteToken, Messaging} from 'firebase/messaging';
import axios from 'axios';
import AppState from '../../state/AppState';
import {site} from '../../api/proto';

export default class FirebaseAppStore { 
    private firebaseMessaging: Messaging | null = null;
    firebaseConfig: FirebaseOptions;

    constructor(private appState: AppState) {}

    init = async (config?: site.IFirebaseConfig | null, withoutPermReq?: boolean) => {
        if (!config || !config?.messagingSenderId || this.appState.isApk) {
            return;
        }

        this.firebaseConfig = {
            apiKey: config?.apiKey ?? undefined,
            projectId: config?.projectId ?? undefined,
            messagingSenderId: config?.messagingSenderId ?? undefined,
            appId: config?.appId ?? undefined,
        };

        initializeApp(this.firebaseConfig);

        if ('Notification' in window) {
            this.firebaseMessaging = getMessaging();
            if (Notification.permission === 'granted' && withoutPermReq !== true) {
                this.requestPermission();
            }
        } else {
            console.log('This browser does not support desktop notification.');
        }
    };

    initWithRequestPermission = async (config?: site.IFirebaseConfig | null) => {
        try {
            this.init(config, true);
            this.requestPermission();
        } catch (e) {
            console.log(e);
        }
    };

    requestPermission = async () => {
        if (this.appState.isApk || !this.firebaseMessaging) {
            return;
        }
        getToken(this.firebaseMessaging)
            .then((currentToken) => {
                if (currentToken) {
                    this.sendTokenToServer(currentToken);
                } else {
                    console.warn(
                        'No Instance ID token available. Request permission to generate one',
                    );
                }
            })
            .catch((error) => {
                console.warn('An error occurred while retrieving token', error);
            });
    };

    deleteToken = () => {
        if (this.firebaseMessaging) {
            deleteToken(this.firebaseMessaging)
                .then(() => {})
                .catch((error) => {
                    console.warn('Unable to delete token', error);
                });
        }
    };

    sendTokenToServer = (token: string) => {
        axios
            .post(
                '/save-token',
                JSON.stringify({
                    appSessId: this.appState.userInfo.sid,
                    token: token,
                    tokenType: 'browser',
                    userId: this.appState.userInfo.userId.toString(),
                    tokenProjectId: this.firebaseConfig.projectId,
                }),
            )
            .then((response) => {
                console.log(`Token ${token} sent to server`);
            })
            .catch((error) => {
                console.log(error);
            });
    };
}
