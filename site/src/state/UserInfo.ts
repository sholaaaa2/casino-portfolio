import { commons } from '../api/proto';
import Long from 'long';
import {observable, computed, observe} from 'mobx';
import {isDef} from '../api/utils';
import AppState from './AppState';
import { users } from "../api/proto";
import {BonusForPage} from './meta';
import { redirectToUrlIfNotTheSame } from './utils/redirectToUrlIfNotTheSame';
import { LOGIN_VIA_TOKEN_QUERY_PARAMS } from './pages/LoginViaToken';

const STORAGE_KEY_USER_REDIRECT_URL = 'wl:user-redirect-url';

export type UserBalance = {
    currencyCode: string;
    currency: (commons.ICurrency | null);
    available: string;
    formatted: string;
    isFiat: boolean;
    currencyId: (Long | null);
    availableValue: (Long | null);
    active?: boolean;
    id: Long;
};

export default class UserInfo {
    @observable sid: string;
    @observable balances: UserBalance[];
    @observable balance: UserBalance;
    @observable balanceString: string;
    @observable balanceStringWithoutCurrency: string;
    @observable currencyString: string;
    @observable realBalance: (UserBalance | undefined);
    @observable currency: commons.ICurrency | null;
    @observable guest: boolean;
    @observable login: string;
    @observable userId: Long;
    @observable restrictions: users.UserInfo.UserRestrictions[] = [];
    @observable providerRestrictions: users.UserInfo.IProviderRestrictions[] = [];
    incomingUrl: string | null = window.location.href;

    @computed
    get isBmUser(): boolean {
        return !this.guest && (this.appState.rawUserInfo?.ref?.startsWith('bm:') ?? false);
    }

    @computed
    get showLoginBM(): boolean {
        return ((this.appState.rawUserInfo?.ref?.startsWith('bm:') ?? false) && !!this.appState.rawUserInfo?.phone && !!this.appState.appSettings.showBMLoginWhenUserHasPhone) || (!!this.appState.rawUserInfo?.phone && !!this.appState.rawUserInfo?.displayName);
    }

    @computed
    get showLoginBMWithoutPhone(): boolean {
        return ((this.appState.rawUserInfo?.ref?.startsWith('bm:') ?? false)) || (!!this.appState.rawUserInfo?.displayName);
    }

    @computed
    get loginBM(): string {
        if (this.appState.rawUserInfo?.displayName) {
            return this.appState.t('ui-user', this.appState.rawUserInfo.displayName);
        }
        return this.appState.rawUserInfo?.login ?
            this.appState.t('ui-user', this.appState.rawUserInfo?.login) :
            this.appState.t('ui-user', this.appState.userInfo.userId);
    }

    @computed
    get loginBMRaw() {
        if (this.appState.rawUserInfo?.displayName) {
            return this.appState.rawUserInfo.displayName;
        }
        return this.appState.rawUserInfo?.login ?
            this.appState.rawUserInfo?.login :
            this.appState.userInfo.userId;
    }

    constructor(private appState: AppState, sid?: string | null) {
        if (isDef(sid)) {
            this.sid = sid!;
        }
        if (appState.appSettings.skillWheel.enabled) {
            observe(this, 'guest', (change) => {
              const isRealUser = change.newValue !== true;
              const appState = this.appState;

              setTimeout(() => {
                if (isRealUser) {
                    if (appState.skillWheel.isEnabled) {
                        appState.skillWheel.fetchState(`${BonusForPage.SKILLWHEEL}|${BonusForPage.INFO}`);
                    }
                } else {
                    // reset skillWheel state if guest user and skillWheel is enabled
                    if (appState.skillWheel.show) {
                        appState.skillWheel.resetState();
                    }
                }
              }, 1000)
          });
      }
    }

    @computed
    get realCurrency(): commons.ICurrency | null {
        const { currencies } = this.appState;
        const realBalance = this.balances.filter(b => b.currencyId && !currencies.isFunCurrency(b.currencyId));
        return realBalance.length > 0 ? realBalance[0].currency : null;
    }

    @computed
    get supportedBalances(): UserBalance[] {
        const { userInfo, currencies, site } = this.appState;
        return site.isFunAllowed ? userInfo.balances : userInfo.balances.filter(b => !currencies.isFunCurrency(b.currencyId!));
    }

    get requiredCurrencyId(): Long | null {
        const url = new URL(window.location.href);
        const currencyId = url.searchParams.get("requiredCurrencyId");
        if (currencyId) {
            return Long.fromString(currencyId);
        }
        return null;
    }

    get isRequiredCurrencyIdSupported(): boolean {
        return this.supportedBalances.some(_ => this.requiredCurrencyId !== null && _.currencyId?.eq(this.requiredCurrencyId))
    }

    get requiredCurrencyCode(): string {
        return this.appState.siteConfig.currencies?.supported?.find(_ => this.requiredCurrencyId !== null && _.id?.eq(this.requiredCurrencyId))?.code ?? "";
    }

    checkRedirectUrl = async () => {
        try {
            const storedUrls = await window.localStorage.getItem(STORAGE_KEY_USER_REDIRECT_URL) ?? '{}';
            const urls: Record<string, string> = JSON.parse(storedUrls);
            const userIdString = this.userId.toString();
            if (userIdString in urls && this.appState.siteConfig.autoLogin) {
                redirectToUrlIfNotTheSame(urls[userIdString]);
            }
        } catch (e) {
            console.error(e);
        }
    }

    storeAndRedirectToUrl = async (url?: string | null): Promise<boolean> => {
        if (url) {
            try {
                const storedUrls = await window.localStorage.getItem(STORAGE_KEY_USER_REDIRECT_URL) ?? '{}';
                const urls: Record<string, string> = JSON.parse(storedUrls);
                const userIdString = this.userId.toString();
                urls[userIdString] = url;
                await window.localStorage.setItem(STORAGE_KEY_USER_REDIRECT_URL, JSON.stringify(urls));
                const urlToRedirect = new URL(url);
                if (this.appState.refillStore.promoCodeForm.value) {
                    urlToRedirect.searchParams.append(
                        LOGIN_VIA_TOKEN_QUERY_PARAMS.PROMO_CODE,
                        this.appState.refillStore.promoCodeForm.value,
                    );
                }
                return redirectToUrlIfNotTheSame(urlToRedirect.toString());
            } catch (e) {
                console.error(e);
            }
        }
        return false;
    }
}
