import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class Bonuses extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.BONUSES;
            appState.site.onPageOpen('bonuses');
        });

        appState.profileBonuses.tryLoadBonusesData();

        appState.finishInitialize();
    }

    componentWillUnmount() {
        this.props.appState.profileBonuses.resetStateOnUnmount();
    }

    render() {
        return template(this.props.appState);
    }
}