import * as React from 'react';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import template from '../../templates';
import {PageProps} from '../../App';
import { SitePages } from '../meta';

@observer
export default class MainPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.MAIN;
            appState.site.onPageOpen('index');

            if (appState.appSettings.mainPage && appState.appSettings.mainPage.showTournaments) {
                appState.tournamentsActions.fetchTournaments();
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}
