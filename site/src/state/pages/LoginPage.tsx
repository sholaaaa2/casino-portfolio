import * as React from 'react';
import {observer} from 'mobx-react';
import template from '../../templates';
import {PageProps} from '../../App';
import {action} from 'mobx';
import {ComponentNames, SitePages} from '../../state/meta';
import { HomePage } from './index';
import { AUTH_GET_PARAMS } from '../enums/AUTH_GET_PARAMS';
import { getParamsFromUrl } from '../utils/BrowserUtils';

@observer
export default class LoginPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            if (appState.site.isWeblimaMode) {
                this.prepareGeneratedLoginPage();
            } else {
                appState.modal.showLogin();
                appState.page = appState.site.homePageName;
                appState.site.onPageOpen('login');
            }
        }, ComponentNames.LOGIN_PAGE);

        appState.finishInitialize();
    }

    componentWillReceiveProps(newProps: PageProps) {
        const appState = newProps.appState;
        if (!appState.site.isWeblimaMode) {
            appState.routeParams = newProps.match.params;
            appState.router = newProps.history;
        } else {
            this.prepareGeneratedLoginPage();
        }
    }

    componentDidUpdate() {
        const appState = this.props.appState;
        if (appState.site.isWeblimaMode) {
            appState.ui.scale.calcWrapperScale();
        }
    }

    @action
    componentWillUnmount() {
        const appState = this.props.appState;
        if (appState.site.isWeblimaMode) {
            appState.ui.scale.page = null;
        }
        this.props.appState.modal.hideModal();
    }

    @action
    prepareGeneratedLoginPage() {
        const { appState } = this.props;
        appState.page = SitePages.GENERATED_LOGIN;
        appState.site.onPageOpen('generated-login');
        window.setTimeout(() => {
            const loginRef = appState.getRef('generatedLogin') as HTMLInputElement;
            const [login] = getParamsFromUrl([AUTH_GET_PARAMS.LOGIN]);
            if (loginRef && login) {
                loginRef.value = login;
            }
        }, 50);
    }

    render() {
        if (this.props.appState.site.isWeblimaMode) {
            return template(this.props.appState);
        } else {
            return <HomePage {...this.props} />;
        }
    }
}