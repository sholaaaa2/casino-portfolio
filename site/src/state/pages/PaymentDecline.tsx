import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class PaymentDecline extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PAYMENT_DECLINE;
            appState.site.onPageOpen('payment-decline');
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}