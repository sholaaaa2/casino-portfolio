import {observer} from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import {PageProps} from '../../App';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class VLobbyPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.LOBBY;
            appState.site.onPageOpen('lobby');
            document.body.classList.add('v-lobby-page');
            const frame = this.getFrame();
            if (frame) {
                frame.addEventListener('load', this.handleFrameLoad);
            }
            window.addEventListener('message', this.handleMessage);
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    getFrame() {
        const frameContainer = document.getElementById('lobby_iframe');
        if (frameContainer) {
            return frameContainer.getElementsByTagName('iframe')[0] || null;
        }
        return null;
    }

    handleFrameLoad = () => {
        const frame = this.getFrame();
        if (frame && frame.contentWindow) {
            frame.contentWindow.postMessage({ type: 'getHeight' }, '*');
        }
    };

    setFrameHeight = (height: number) => {
        const frame = this.getFrame();
        if (frame) {
            frame.height = '';
            frame.height = height + 'px';
        }
    };

    handleMessage = (event: MessageEvent) => {
        switch (event.data.type) {
            case 'height':
                this.setFrameHeight(event.data.value);
                break;
            default:
                console.debug('Unknown lobby iframe message: ', event.data);
        }
    };

    componentDidUpdate() {
        const frame = this.getFrame();
        if (frame) {
            frame.addEventListener('load', this.handleFrameLoad);
        }
    }

    componentWillUnmount() {
        document.body.classList.remove('v-lobby-page');
        window.removeEventListener('message', this.handleMessage);
    }

    render() {
        return template(this.props.appState);
    }
}