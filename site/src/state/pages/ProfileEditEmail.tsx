import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class ProfileEditEmail extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PROFILE_EDIT_EMAIL;
            appState.site.onPageOpen('profile/edit/email');
            this.props.appState.profileEdit.setCurrentValues();
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    componentWillUnmount() {
        this.props.appState.profileEdit.resetFormErrors();
    }

    render() {
        return template(this.props.appState);
    }
}
