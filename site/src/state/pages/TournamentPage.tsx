import {PageProps} from '../../App';
import {observer} from 'mobx-react';
import * as React from 'react';
import {action} from 'mobx';
import template from '../../templates';
import { SitePages } from '../meta';

@observer
export default class TournamentPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        const tournamentId = this.props.match.params.tournamentId;
        appState.startInitialize(this.props, () => {
            appState.tournamentsActions.tournamentId = tournamentId ?? "";
            appState.page = SitePages.TOURNAMENT;
            this.updateTournamentMeta();

            window.scrollTo(0, 0);
        });

        // here you may add additional API calls to be executed
        // during app initialization call
        appState.tournamentsActions.fetchTournaments();

        appState.finishInitialize();
    }

    @action
    componentDidUpdate() {
        const appState = this.props.appState;
        appState.routeParams = this.props.match.params;
        appState.tournamentsActions.tournamentId = this.props.match.params.tournamentId ?? "";
        this.updateTournamentMeta();
    }

    updateTournamentMeta() {
        const appState = this.props.appState;
        const tournamentId = this.props.match.params.tournamentId;

        // set page canonical url
        appState.site.onPageOpen(`tournaments/${tournamentId}`);

        // set page meta by type
        const tournament = appState.tournamentsActions.findTournament(tournamentId);
        const tournamentType = tournament ? tournament.type : null;
        appState.site.setPageMetaByPath(`tournaments/${tournamentType}`);
    }

    @action
    componentWillUnmount() {
        this.props.appState.tournamentsActions.tournamentId = null;
    }

    render() {
        return template(this.props.appState);
    }
}