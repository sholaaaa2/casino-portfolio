import * as React from 'react';
import {PageProps} from '../../App';
import template from '../../templates';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
class JackpotPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.JACKPOT;
            appState.site.onPageOpen('jackpot');
        });
        appState.pages.load('jackpot', SitePages.JACKPOT, true);

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}

export default JackpotPage;
