import {action} from 'mobx';
import {observer} from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import {PageProps} from '../../App';
import { HomePage } from './index';

@observer
export default class ResetPasswordPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            if (appState.site.isWeblimaMode) {
                appState.user.hideModalAndOpenMainPage();
            } else {
                appState.modal.showRestore();
                appState.page = appState.site.homePageName;
                appState.site.onPageOpen('reset-password');
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    componentDidUpdate(prevProps: PageProps) {
        const appState = this.props.appState;
        if (appState.site.isWeblimaMode) {
            appState.user.hideModalAndOpenMainPage();
        }
    }

    render() {
        if (this.props.appState.site.isWeblimaMode) {
            return template(this.props.appState);
        } else {
            return <HomePage {...this.props} />;
        }
    }
}
