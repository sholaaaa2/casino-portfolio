import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

export enum PAYMENT_SUCCESS_QUERY_PARAMS {
    PROVIDER = 'provider',
}

@observer
export default class PaymentSuccess extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PAYMENT_SUCCESS;
            appState.site.onPageOpen('payment-success');
            const query = new URLSearchParams(window.location.search);
            if (query.has(PAYMENT_SUCCESS_QUERY_PARAMS.PROVIDER)) {
                appState.successPaymentProvider = query.get(PAYMENT_SUCCESS_QUERY_PARAMS.PROVIDER) as string;
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}