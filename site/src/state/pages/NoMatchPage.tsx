import { action } from 'mobx';
import { observer } from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import {PageProps} from '../../App';

@observer
export default class NoMatchPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            if (appState.appSettings.redirectFrom404ToMainPage) {
                const route = appState.userInfo.guest ? '/login' : '/';
                appState.router.push(appState.l(route));
            } else {
                appState.pages.triggerNoMatchPage();
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}
