import {PageProps} from '../../App';
import {action} from 'mobx';
import * as React from 'react';
import template from '../../templates';
import {observer} from 'mobx-react';
import { SitePages } from '../meta';

@observer
export default class TournamentsListPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.TOURNAMENT_LIST;
            appState.site.onPageOpen('tournaments');
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.tournamentsActions.fetchTournaments();

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}