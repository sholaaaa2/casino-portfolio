import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class ProfileEmail extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PROFILE_EMAIL;
            appState.site.onPageOpen('profile/email');
            this.props.appState.profileEdit.setCurrentValues();
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    componentWillUnmount() {
        this.props.appState.profileEdit.resetFormErrors();
    }

    render() {
        return template(this.props.appState);
    }
}
