import * as React from 'react';
import {observer} from 'mobx-react';
import template from '../../templates';
import {PageProps, SitePages} from '../meta';

interface SkillWheelProps extends PageProps {
    wheelTranslateY?: string | null;
}

@observer
export default class SkillWheelPage extends React.Component<SkillWheelProps> {
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.SKILL_WHEEL;
            appState.site.onPageOpen('skill-wheel');
        });
        setTimeout(() => {
            appState.notifications.connect();
            appState.ui.scale.calcWrapperScale(appState.getRef('wrapper'), true, true);
            appState.ui.skillWheel.calcFontSizeForWheelItems();
        }, 0);
        //appState.skillWheel.fetchState(`${BonusForPage.SKILLWHEEL}|${BonusForPage.INFO}`);
        appState.finishInitialize();
    }

    componentDidUpdate() {
        const appState = this.props.appState;
        appState.ui.scale.calcWrapperScale(appState.getRef('wrapper'), true, true);
        appState.notifications.connect();
    }

    componentWillUnmount() {
        this.props.appState.skillWheel.resetTimerId();
        this.props.appState.skillWheel.resetWheelTimers();
    }

    render() {
        return template(this.props.appState);
    }
}
