import {PageProps} from '../../App';
import {observer} from 'mobx-react';
import * as React from 'react';
import {action} from 'mobx';
import template from '../../templates';
import { SitePages } from '../meta';

@observer
export default class WithdrawalPaywayPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.WITHDRAW;
            this.updatePageMeta();
            if (appState.withdrawalStore.isPageDataLoaded()) {
                appState.withdrawalStore.selectPaywayByUrl();
            }
            window.scrollTo(0, 0);
        });

        // here you may add additional API calls to be executed
        // during app initialization call
        appState.billingHistory.loadHistory();
        appState.withdrawalStore.loadPayways();
        if (appState.appSettings.loadBonusesInWithdrawals && !appState.withdrawalStore.withdrawalInspector) {
            appState.userBonusesStore.fetchBonuses(appState.withdrawalStore.onBonusesLoadedWithPage);
        }

        appState.finishInitialize();
    }

    @action
    componentWillUnmount() {
        this.props.appState.withdrawalStore.resetPayway();
    }

    @action
    componentDidUpdate() {
        const appState = this.props.appState;
        appState.routeParams = this.props.match.params;
        const withdrawalStore = appState.withdrawalStore;

        if (withdrawalStore.isPageDataLoaded()) {
            withdrawalStore.selectPaywayByUrl();
        }
        this.updatePageMeta();
    }

    updatePageMeta() {
        const appState = this.props.appState;
        const payway = this.props.match.params.payway;
        appState.site.onPageOpen(`withdraw/${payway}`);
        appState.site.setPageMetaByPath('withdraw');
    }

    render() {
        const appState = this.props.appState;
        return template(appState);
    }
}