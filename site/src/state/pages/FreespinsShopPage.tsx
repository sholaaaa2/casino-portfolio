import {PageProps} from "../../App";
import {SitePages} from "../meta";
import template from "../../templates";
import {observer} from "mobx-react";
import React from "react";

@observer
export default class FreespinsShopPage extends React.Component<PageProps> {
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.FREESPINS_SHOP;
            appState.site.onPageOpen('freespins-shop');
            appState.freespinsShopActions.init(this.props.match?.params?.shopItemId);
        });
        appState.finishInitialize();
    }

    componentWillUnmount() {
        this.props.appState.freespinsShopActions.resetState();
    }

    render() {
        const appState = this.props.appState;
        return template(appState);
    }

}
