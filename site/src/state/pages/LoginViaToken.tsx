import * as React from 'react';
import { action } from 'mobx';
import { observer } from 'mobx-react';
import template from '../../templates';
import { PageProps } from '../../App';
import { SitePages } from '../meta';
import { users } from '../../api/proto';

export enum LOGIN_VIA_TOKEN_QUERY_PARAMS {
    PROMO_CODE = 'promoCode',
}

@observer
export default class LoginViaToken extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.LOGIN_VIA_TOKEN;
            appState.site.onPageOpen('login');
        });

        this.tryLoginViaToken();

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    tryLoginViaToken() {
        const appState = this.props.appState;
        const token = this.props.match.params.token;
        if (token) {
            const query = new URLSearchParams(window.location.search);
            if (query.has(LOGIN_VIA_TOKEN_QUERY_PARAMS.PROMO_CODE)) {
                appState.refillStore.promoCodeForm.updateValue(query.get(LOGIN_VIA_TOKEN_QUERY_PARAMS.PROMO_CODE)!);
            }
            appState.api.signInUsingToken(token, (msg, act) => {
                appState.processApi(msg, act);
                if (act.login && act.login.ok) {
                    appState.router.push(appState.l('/'));
                    appState.signInActions.processSignInSuccess(act.login);
                } else if (act.login && act.login.error === users.LoginResponse.LoginError.use_alternative_credentials) {
                    if (act.login.alternativeCredentials) {
                        if (act.login.alternativeCredentials.email) {
                            appState.router.push(appState.l(`/login?email=${encodeURIComponent(act.login.alternativeCredentials.email)}`));
                        } else if (act.login.alternativeCredentials.login) {
                            appState.router.push(appState.l(`/login?login=${encodeURIComponent(act.login.alternativeCredentials.login)}`));
                        } else if (act.login.alternativeCredentials.phone) {
                            const { phone, countryCode } = act.login.alternativeCredentials.phone;
                            appState.router.push(appState.l(`/login?phone=${encodeURIComponent(phone || '')}&country=${encodeURIComponent(countryCode || '')}`));
                        }
                    } else {
                        appState.router.push(appState.l('/login'));
                    }
                } else {
                    appState.loginForm.tokenError = appState.t(
                        'error-login-via-token'
                    );
                }
            });
        }
    }

    render() {
        return template(this.props.appState);
    }
}
