import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class ProfileDocumentsUpload extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const { appState } = this.props;
        appState.profileDocuments.setCurrentDocType(this.props.match.params?.type);
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PROFILE_DOCUMENTS_UPLOAD;
            appState.site.onPageOpen('profile/documents/upload');
        });

        appState.profileDocuments.loadDocumentsAndRestrictions();
        window.addEventListener('offline', appState.profileDocuments.handleOfflineEvent);

        appState.finishInitialize();
    }

    componentWillUnmount() {
        const { appState } = this.props;
        window.removeEventListener('offline', appState.profileDocuments.handleOfflineEvent);
    }

    render() {
        return template(this.props.appState);
    }
}
