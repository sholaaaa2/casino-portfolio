import * as React from 'react';
import {Redirect, Route, RouteProps} from 'react-router';

interface ProtectedRouteProps extends RouteProps {
    isAllowed: boolean;
    redirectTo: string;
}

const ProtectedRoute: React.SFC<ProtectedRouteProps> = function (props: ProtectedRouteProps) {
    return props.isAllowed ? (
        <Route {...props}/>
    ) : (
        <Redirect to={props.redirectTo}/>
    );
};

export default ProtectedRoute;