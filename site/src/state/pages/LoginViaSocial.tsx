import * as React from 'react';
import { action } from 'mobx';
import { observer } from 'mobx-react';
import template from '../../templates';
import {extractLocationArgs} from '../utils/BrowserUtils';
import { PageProps } from '../../App';
import { SitePages } from '../meta';
import {NotificationItemLevel} from '../components/Notificator';
import {site} from '../../api/proto';

@observer
export default class LoginViaSocial extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;

        appState.startInitialize(this.props, () => {
            appState.page = SitePages.LOGIN_VIA_SOCIAL;
            appState.site.onPageOpen('login');
        });

        this.tryLoginViaToken();

        appState.finishInitialize();
    }

    @action
    tryLoginViaToken() {
        const appState = this.props.appState;
        const socialProvider = this.props.match.params.socialProvider;
        const queryParams = extractLocationArgs();
        if (socialProvider && queryParams?.code) {
            const socialProviderNumber = site.Social.SocialProvider[socialProvider];
            appState.api.signInUsingSocial({
                provider: socialProviderNumber,
                redirectUrl: `${window.location.origin}${`/social/${socialProvider}`}`,
                authCode: queryParams?.code,
            }, (msg, act) => {
                if (act?.login?.error && act?.login?.alternativeCredentials?.facebookId) {
                    if (appState.appSettings.registerForm?.socialRegistrationWithDefaultCurrency) {
                        appState.social.facebookId = act?.login?.alternativeCredentials?.facebookId;
                        appState.social.providerForRegistration = Number(socialProviderNumber);
                        appState.social.tryRegister(true);
                    } else {
                        window.location.replace(appState.l(`/register?provider=${socialProvider}&facebookId=${act?.login?.alternativeCredentials?.facebookId}`))
                    }
                } else if (act?.login?.ok) {
                    appState.processApi(msg, act);
                    appState.signInActions.processSignInSuccess(act.login);
                    appState.router.push(appState.l('/'));
                } else {
                    appState.notificator!.addNotification({
                        message: appState.t('ui-social-login-error'),
                        level: NotificationItemLevel.ERROR
                    });
                    appState.router.push(appState.l('/'));
                }
            });
        } else if (queryParams?.error_message) {
            appState.social.socialAuthError = queryParams?.error_message?.replace(/\+/g, ' ')
        } else {
            appState.social.socialAuthError = appState.t('ui-social-auth-error')
        }
    }

    render() {
        return template(this.props.appState);
    }
}
