import { action } from 'mobx';
import { observer } from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import { PageProps } from '../../App';
import { SitePages } from '../meta';

@observer
export default class RestorePage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            if (appState.site.isWeblimaMode) {
                appState.user.hideModalAndOpenMainPage();
            } else {
                appState.modal.showRestorePasswordByMail();
                appState.page = SitePages.RESTORE;
                appState.site.onPageOpen('restore');
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}
