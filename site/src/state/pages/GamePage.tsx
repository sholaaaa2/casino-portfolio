import * as React from 'react';
import {addGamePageClass, embedScriptCode, getGameSeoDescription, getGameSeoTitle, removeGamePageClass} from '../utils/GameUtils';
import {observer} from 'mobx-react';
import {getCapabilities, getDeviceType, getParamsFromUrl, setPageMeta} from '../utils/BrowserUtils';
import template from '../../templates';
import {action, autorun, observable, IReactionDisposer} from 'mobx';
import {PageProps} from '../../App';
import {getLaunchGameError, getBonusActivationErrorTokenForGame} from '../../api/utils';
import {games, messages, bonuses} from '../../api/proto';
import {GameItem} from '../actions/GamesActions';
import {GAME_CONTENT_ID} from '../../ui/GameScreen/GameScreen';
import { SitePages } from '../meta';
import GamePageProcessor from "../commonProcessors/GamePageProcessor";
import VendorGamePageProcessor from "../vendorProcessors/VendorGamePageProcessor";
import { getUrlParamsAsMap } from '../utils/getUrlParamsAsMap';

@observer
class GamePage<P> extends React.Component<P & PageProps> {
    isEmbedded = false;
    gameId: string = '';
    autorunDisposer?: IReactionDisposer;
    gamePageProcessor: GamePageProcessor = new GamePageProcessor();
    vendorGamePageProcessor: VendorGamePageProcessor = new VendorGamePageProcessor();
    @observable isGameStartInitialization: boolean = false;

    @action
    componentDidMount() {
        const appState = this.props.appState;
        this.gameId = this.props.match.params.id ?? "";
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.GAME;
            appState.site.onPageOpen(`game/${this.gameId}`);
            window.addEventListener("message", this.handleMessage);

            if (
                appState.userInfo.requiredCurrencyId !== null &&
                appState.userInfo.currency?.id &&
                appState.userInfo.requiredCurrencyId.notEquals(appState.userInfo.currency.id)
            ) {
                if (appState.userInfo.isRequiredCurrencyIdSupported) {
                    appState.site.changeUserCurrency(appState.userInfo.requiredCurrencyId, () => {
                        this.tryEmbedGame();
                    });
                } else {
                    if (appState.userInfo.guest) {
                        appState.modal.showGameRequiresLogin();
                    } else {
                        appState.modal.showGameRequiresAnotherCurrency(this.hideModalAndTryEmbedGame);
                    }
                }
            } else if (!appState.site.isAccessRestricted) {
                this.tryEmbedGame();
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();

        this.autorunDisposer = autorun(() => {
           if (this.isGameStartInitialization && appState.bonusesInfo.needPlay) {
               // region ---- update game page if new options for running game
               const runningGameStore = appState.runningGameStore;
               if (runningGameStore.runningGame && runningGameStore.runningGame.id) {
                   const activeGame = appState.games.findGameById(runningGameStore.runningGame.id);
                   let reloadPage = false;
                   if (activeGame && activeGame.disableIfActiveBonus) {
                       reloadPage = true;
                   }

                   if (reloadPage) {
                       window.location.reload();
                   }
               }
               // ---- endregion
           }
        });
    }

    componentWillUnmount() {
        removeGamePageClass();
        this.resetGameInfo();
        window.removeEventListener('message', this.handleMessage);
        if (this.autorunDisposer) {
            this.autorunDisposer();
        }
    }

    handleMessage = (event: MessageEvent) => {
        //for yorg
        if (
            event.data === 'closeGame' ||
            event.data === 'close' ||
            event.data === 'notifyCloseContainer' ||
            (event.data.indexOf && event.data.indexOf('GAME_MODE:LOBBY') >= 0)
        ) {
           document.location.href = this.props.appState.l('/');
        }
    };

    @action.bound
    hideModalAndTryEmbedGame() {
        this.props.appState.modal.hideModal();
        this.tryEmbedGame();
    }

    @action
    tryEmbedGame() {
        const appState = this.props.appState;

        if (!this.isEmbedded) {
            this.isEmbedded = true;
            addGamePageClass();
            const params = getParamsFromUrl(['bonusCode']);
            if (params[0]) {
                this.tryAcceptBonus(params[0], (message: string) => {
                    appState.api.getGameLaunchMethod(this.gameId, getDeviceType(this.gameId), getCapabilities(), this.onLaunchGameResponse.bind(this), getUrlParamsAsMap(), message);
                })
            } else {
                appState.api.getGameLaunchMethod(this.gameId, getDeviceType(this.gameId), getCapabilities(), this.onLaunchGameResponse.bind(this), getUrlParamsAsMap());
            }
        }
    }

    @action
    onLaunchGameResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const appState = this.props.appState;
        const gameContainer = document.getElementById(GAME_CONTENT_ID);

        // wait until game page rendered
        if (!gameContainer) {
            setTimeout(this.onLaunchGameResponse.bind(this, msg, act), 1);
            return;
        }

        if (act.gameEmbed) {
            const game = appState.games.findGameById(this.gameId);
            const error = act.gameEmbed.error;
            this.updatePageMeta(game);

            if (error) {
                if (error === games.GameEmbedResponse.LaunchError.PROVIDER_ERROR) {
                    appState.games.launchGameProviderError = getLaunchGameError(error);
                } else {
                    if (error === games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED && appState.userInfo.balance.currencyId!.toNumber() === 0) {
                        appState.games.launchGameError = 'error-demo-mode-not-supported';
                    } else if (error === games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED) {
                        appState.games.launchGameError = getLaunchGameError(games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED);
                        appState.games.showSupportedCurrenciesForGame = true;
                    } else {
                        appState.games.launchGameError = getLaunchGameError(error);
                    }
                }

                appState.modal.showGameEmbedError();
            } else {
                this.isGameStartInitialization = true;

                let embed = act.gameEmbed;

                if (embed.launchMethod!.screenWidth && embed.launchMethod!.screenHeight) {
                    appState.runningGameStore.gameScreenSize = {
                        width: embed.launchMethod!.screenWidth!,
                        height: embed.launchMethod!.screenHeight!
                    };
                } else {
                    appState.runningGameStore.gameScreenSize = null;
                }

                if (embed.launchMethod!.div && embed.launchMethod!.div!.scriptPath!.length > 0) {
                    let scripts = embed.launchMethod!.div!.scriptPath!;

                    let checkOnLoad: () => void;
                    let embedScript = () => {
                        const scriptEmbed = document.createElement('script');
                        scriptEmbed.type = 'text/javascript';
                        scriptEmbed.onload = checkOnLoad;
                        scriptEmbed.src = scripts[0];
                        scripts = scripts.slice(1);

                        gameContainer!.appendChild(scriptEmbed);
                    };

                    checkOnLoad = () => {
                        if (scripts.length) {
                            embedScript();
                        } else {
                            // eslint-disable-next-line no-eval
                            eval(embedScriptCode(appState, embed, game));
                        }
                    };

                    embedScript();
                } else if (embed.launchMethod!.div && embed.launchMethod!.div!.rawHTML) {
                    appState.runningGameStore.rawHTML = embed.launchMethod!.div!.rawHTML!;
                } else if (embed.launchMethod?.iframe?.src) {
                    let iframeSrc = embed.launchMethod.iframe.src;
                    if (embed.launchMethod.iframe.setLocation && !appState.appSettings.games.alwaysOpenInFrame) {
                        window.location.replace(iframeSrc);
                        return;
                    } else {
                        appState.runningGameStore.iframe = iframeSrc;
                        this.processFrameCallbacks();
                    }
                } else {
                    console.error('Unknown launch method', embed);
                }
            }
        }
    }

    updatePageMeta(game: GameItem | null) {
        const appState = this.props.appState;
        appState.runningGameStore.runningGame = game;

        if (game) {
            const gameId = game.id!;
            const seoMeta = {
                'game-name': appState.games.getGameName(game),
                'site-name': appState.site.siteName,
                year: new Date().getFullYear(),
            }; 
            const title = appState.t(
                getGameSeoTitle(appState, game, 'seo-game-default-title'),
                seoMeta
            );
            const description = appState.t(
                getGameSeoDescription(appState, game, 'seo-game-default-description'),
                seoMeta
            );
            const ogImage = `${window.location.protocol}//${window.location.host}/media/thumb/300x300/${gameId}.jpeg`;

            setPageMeta(this.props.appState, title, description, ogImage);
        }
    }

    @action
    tryAcceptBonus(code: string, cb: Function, bonusValue?: string) {
        this.props.appState.api.activatePromoCode(code, this.onTryAcceptBonus.bind(this, {code, bonusValue, cb}));
    }

    @action
    onTryAcceptBonus(params: {code: string, bonusValue?: string, cb: Function}, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.props.appState.processApi(msg, act);
        if (act.bonusAction && act.bonusAction.activateCode) {
            const result = act.bonusAction.activateCode.result;
            if (result === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                params.cb(this.props.appState.t('bonus-successfully-activated'));
            } else {
                const bonusActivationErrorToken = getBonusActivationErrorTokenForGame(result);
                params.cb(this.props.appState.t(bonusActivationErrorToken));

            }
        } else {
            const bonusActivationErrorToken = getBonusActivationErrorTokenForGame(bonuses.BonusCodeActivateResponse.Result.SERVER_ERROR);
            params.cb(this.props.appState.t(bonusActivationErrorToken));
        }
    }

    @action
    resetGameInfo() {
        const appState = this.props.appState;
        appState.runningGameStore.runningGame = null;
        appState.runningGameStore.rawHTML = null;
        appState.runningGameStore.iframe = null;
        appState.games.showSupportedCurrenciesForGame = false;
        this.isEmbedded = false;
    }

    processFrameCallbacks() {
        const { appState } = this.props;
        this.gamePageProcessor.processCommonFrameCallbacks(appState);
        this.vendorGamePageProcessor.processCommonFrameCallbacks(appState);
    }

    render() {
        return template(this.props.appState);
    }
}

export default GamePage;