import {PageProps} from '../../App';
import {observer} from 'mobx-react';
import * as React from 'react';
import {action} from 'mobx';
import template from '../../templates';
import { SitePages } from '../meta';

@observer
export default class RefillPaywayPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.REFILL;
            this.updatePageMeta();

            if (appState.refillStore.isPageDataLoaded()) {
                appState.refillStore.selectPaywayByUrl();
            }
            window.scrollTo(0, 0);
        });

        // here you may add additional API calls to be executed
        // during app initialization call
        if (!appState.userInfo.guest) {
            appState.billingHistory.loadHistory();
            appState.refillStore.loadPayways();
        }

        appState.finishInitialize();
    }

    @action
    componentWillUnmount() {
        this.props.appState.refillStore.resetPayway();
    }

    @action
    componentDidUpdate() {
        const appState = this.props.appState;
        appState.routeParams = this.props.match.params;
        const refillStore = appState.refillStore;

        if (refillStore.isPageDataLoaded()) {
            refillStore.selectPaywayByUrl();
        }
        this.updatePageMeta();
    }

    updatePageMeta() {
        const appState = this.props.appState;
        const payway = this.props.match.params.payway;
        appState.site.onPageOpen(`refill/${payway}`);
        appState.site.setPageMetaByPath('refill');
    }

    render() {
        return template(this.props.appState);
    }
}