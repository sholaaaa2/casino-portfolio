import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class AppInfoPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.APP_INFO;
            appState.site.onPageOpen('app-info');
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
        document.documentElement.style.scrollBehavior = 'smooth';
    }

    componentWillUnmount() {
        document.documentElement.style.scrollBehavior = 'unset';
    }

    render() {
        return template(this.props.appState);
    }
}