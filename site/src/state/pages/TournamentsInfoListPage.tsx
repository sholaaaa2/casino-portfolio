import {PageProps} from '../../App';
import {action} from 'mobx';
import * as React from 'react';
import template from '../../templates';
import {observer} from 'mobx-react';
import { SitePages } from '../meta';

@observer
export default class TournamentsInfoListPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.page = SitePages.TOURNAMENTS_INFO_LIST_PAGE;
        appState.startInitialize(this.props, () => {
            appState.site.onPageOpen('tournaments-info-list');
        });

        // here you may add additional API calls to be executed
        // during app initialization call


        appState.finishInitialize();
    }

    componentWillUnmount() {
        this.props.appState.tournamentsInfoActions.tournamentUuid = undefined;
    }

    render() {
        return template(this.props.appState);
    }
}