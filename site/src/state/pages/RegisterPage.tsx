import { observer } from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import { PageProps } from '../../App';
import {action} from 'mobx';
import { HomePage } from './index';
import { AuthorizationType } from '../actions/authorization/AuthorizationTypeState';

@observer
export default class RegisterPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.social.checkRegistrationIsSocial();
        appState.startInitialize(this.props, () => {
            if (appState.site.isWeblimaMode) {
                appState.user.hideModalAndOpenMainPage();
            } else {
                appState.modal.showRegister();
                appState.page = appState.site.homePageName;
                appState.site.onPageOpen('register');
                if (this.props.match.url.includes("/anonymous")) {
                    appState.authorizationTypeState.type = AuthorizationType.ANONYMOUS;
                }
            }
        });
        appState.finishInitialize();
    }

    componentDidUpdate(prevProps: PageProps) {
        const appState = this.props.appState;
        if (appState.site.isWeblimaMode) {
            appState.user.hideModalAndOpenMainPage();
        }
    }

    @action
    componentWillUnmount() {
        this.props.appState.modal.hideModal();
    }

    render() {
        if (this.props.appState.site.isWeblimaMode) {
            return template(this.props.appState);
        } else {
            return <HomePage {...this.props} />;
        }
    }
}
