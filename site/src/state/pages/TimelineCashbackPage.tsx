import * as React from 'react';
import {PageProps} from '../../App';
import template from '../../templates';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
class TimelineCashbackPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.TIMELINE_CASHBACK;
            appState.site.onPageOpen('timeline-cashback');
        });
        appState.pages.load('timeline-cashback', SitePages.TIMELINE_CASHBACK, true);

        // here you may add additional API calls to be executed
        // during app initialization call
        if (!appState.appSettings.loadBonusesOnInit && !appState.timelineCashbackBonus.settings) {
            appState.userBonusesStore.fetchBonuses();
        }

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}

export default TimelineCashbackPage;
