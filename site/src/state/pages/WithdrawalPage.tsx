import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class WithdrawPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.WITHDRAW;
            appState.withdrawalStore.resetPayway();
            appState.site.onPageOpen('withdraw');
            appState.withdrawalStore.tryPreselectPayway();

            // this api calls require user info
            if (!appState.userInfo.guest) {
                appState.api.startBatch();
                appState.billingHistory.loadHistory();
                appState.withdrawalStore.loadPayways(!appState.refillStore.hasPayways);
                appState.api.commit();
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call
        if (appState.appSettings.loadBonusesInWithdrawals) {
            appState.userBonusesStore.fetchBonuses(appState.withdrawalStore.onBonusesLoadedWithPage);
        }

        appState.finishInitialize();
    }

    componentDidUpdate() {
        const appState = this.props.appState;
        appState.withdrawalStore.tryPreselectPayway();
    }

    componentWillUnmount() {
        const appState = this.props.appState;
        // multi
        if (appState.withdrawalStore.payways.length === 1) {
            appState.withdrawalStore.resetPayway();
        }
    }

    render() {
        return template(this.props.appState);
    }
}