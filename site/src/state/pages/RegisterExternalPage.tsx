import {observer} from 'mobx-react';
import template from '../../templates';
import * as React from 'react';
import {PageProps} from '../../App';
import {action} from 'mobx';
import {cookie} from '../utils/BrowserUtils';
import { SitePages } from '../meta';

@observer
export default class RegisterExternalPage extends React.Component<PageProps> {
    externalRegistrationInit: boolean = false;

    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            if (appState.site.isWeblimaMode) {
                appState.user.hideModalAndOpenMainPage();
            } else {
                appState.modal.showRegister();
                appState.site.onPageOpen('register');
                appState.page = SitePages.MAIN;
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();

        // clean url
        window.history.pushState(null, '', '/');
    }

    componentDidUpdate() {
        const appState = this.props.appState;
        if (!appState.userInfo.guest) {
            appState.user.hideAuthWithoutRedirect();
        } else {
            if (!this.externalRegistrationInit) {
                this.externalRegistrationInit = true;
                this.tryRegisterFromCookie();
            }
        }
    }

    render() {
        return template(this.props.appState);
    }

    tryRegisterFromCookie = () => {
        const appState = this.props.appState;
        const siteName = appState.site.siteName;

        const externalEmail = cookie(`${siteName}-external-email`);
        const externalPassword = cookie(`${siteName}-external-password`);

        const emailInputEl = appState.getRef('registerEmail') as HTMLInputElement;
        const passwordInputEl = appState.getRef('registerPassword') as HTMLInputElement;

        if (emailInputEl) {
            emailInputEl.value = externalEmail;
        }

        if (passwordInputEl) {
            passwordInputEl.value = externalPassword;
        }

        if (externalEmail && externalPassword) {
            appState.user.tryRegister();
        }
    }
}
