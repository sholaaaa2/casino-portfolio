import * as React from 'react';
import {PageProps} from '../../App';
import {action} from 'mobx';
import template from '../../templates';
import {observer} from 'mobx-react';
import {SitePages} from '../meta';

@observer
export class ForcedPhoneConfirmationPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.FORCED_PHONE_CONFIRMATION;
            appState.site.onPageOpen(SitePages.FORCED_PHONE_CONFIRMATION);
            appState.ref.onForcedConfirmationOpen();
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}
