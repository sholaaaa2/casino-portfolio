import { observer } from 'mobx-react';
import { isMobile } from '../utils/BrowserUtils';
import template from '../../templates';
import * as React from 'react';
import { PageProps } from '../../App';
import {action, computed} from 'mobx';
import { SitePages, SportBookOddsFormats } from '../meta';

@observer
export default class SportPage extends React.Component<PageProps> {
    constructor(props: PageProps) {
        super(props);
        this.onSportLogin = this.onSportLogin.bind(this);
        this.onSportLoaded = this.onSportLoaded.bind(this);
        this.onSportRegister = this.onSportRegister.bind(this);
        this.onSportScriptLoaded = this.onSportScriptLoaded.bind(this);
    }

    @computed
    get sportSettings() {
        return this.props.appState.siteConfig ? this.props.appState.siteConfig.sportSettings : undefined;
    }

    @computed
    get sportIsAvailable() {
        return this.sportSettings && this.sportSettings.digitain && !this.sportSettings.digitain.disabled;
    }

    showPopupUnavailability  = () => {
        this.props.appState.games.launchGameError = 'error-game-disable-by-active-bonus';
        this.props.appState.modal.showGameEmbedError();
        this.removeSportFrame();
    };

    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.SPORT;
            appState.sport.setIsLoading(true);
            appState.site.onPageOpen('sport');
            appState.sport.mobileFrameLoaded = false;

            if (!appState.site.isAccessRestricted) {
                appState.site.hideFunCurrency(() => {
                    if (!this.isSportScriptAdded() && appState.sport.isCurrencySupported()) {
                        this.addSportScriptTag();
                    }

                    if (!this.sportIsAvailable) {
                        this.showPopupUnavailability();
                    }
                });
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    componentDidUpdate(prevProps: PageProps) {
        if (!this.props.appState.site.isAccessRestricted) {
            if (!this.sportIsAvailable) {
                this.showPopupUnavailability();
                return;
            }
            if (this.props.appState.loading === false) {
                this.loadSportFrameIfNotLoaded();
            }
            if (this.props.match.params.category !== prevProps.match.params.category && window.SportFrame) {
                const currentPageName = this.getCurrentPageName(this.props.match.params.category);
                window.SportFrame.openPage(currentPageName);
            }
        }
    }

    componentWillUnmount() {
        this.props.appState.site.showFunCurrency();
    }

    render() {
        const appState = this.props.appState;
        return template(appState);
    }

    loadSportFrameIfNotLoaded() {
        const appState = this.props.appState;
        const sportFrame = this.getSportFrame();
        if (!sportFrame && appState.sport.scriptLoaded) {
            if (this.props.appState.isMobileOnly && this.props.appState.appSettings.mobileSportNew) {
                this.createSportFrameMobile();
            } else {
                this.createSportFrame();
            }
        }
    }

    getCurrentPageName(category?: string): string {
        let currentPage = '';
        switch (category) {
            case 'live':
                currentPage = 'Overview';
                break;
            case 'football':
                currentPage = 'Upcoming/4584';
                break;
            case 'tennis':
                currentPage = 'Upcoming/20419';
                break;
            case 'cricket':
                currentPage = 'Upcoming/20100';
                break;
            case 'hockey':
                currentPage = 'Upcoming/2439';
                break;
            case 'american-football':
                currentPage = 'Upcoming/14194';
                break;
            case 'esports':
                currentPage = 'Upcoming/21804';
                break;
            default:
                currentPage = 'Home';
                break;
        }
        return currentPage;
    }

    createSportFrame() {
        const appState = this.props.appState;
        const sportSettings = appState.siteConfig.sportSettings;
        const { category } = this.props.match.params;
        const currentPageName = this.getCurrentPageName(category);
        // create sport iframe
        const server =
            sportSettings &&
            sportSettings.digitain ? sportSettings.digitain.server || '' : '';
        const _sp = window._sp || [];
        _sp.push(['server', server]);
        _sp.push(['token', appState.userInfo.sid]);
        _sp.push(['language', window.appState.language]);
        _sp.push(['login', 'onSportLogin']);
        _sp.push(['registration', 'onSportRegistration']);
        _sp.push(['currentPage', currentPageName]);
        _sp.push(['device', isMobile() ? 'm' : 'd']);
        _sp.push(['unHideOverflow', true]);
        _sp.push(['fixedHeight', true]);
        if (appState.appSettings.digitainSportSettings) {
            const {digitainSportSettings} = appState.appSettings;
            const view = digitainSportSettings.sportsBookView ? digitainSportSettings.sportsBookView : 'europeanView';
            const clearSiteStyles = !!digitainSportSettings.clearDefaultStyles;
            _sp.push(['sportsBookView', view]);
            _sp.push(['clearSiteStyles', clearSiteStyles]);
            if (digitainSportSettings.customCssUrl) {
                _sp.push(['customCssUrl', encodeURIComponent(digitainSportSettings.customCssUrl)]);
            }
        }
        const oddsFormat = this.getSportBookOddsFormatIfExist();
        if (oddsFormat) {
            _sp.push(['oddsFormat', oddsFormat]);
        }

        window.SportFrame.frame(_sp);
        // listen for load event
        const sportFrame = this.getSportFrame();
        if (sportFrame) {
            sportFrame.addEventListener('load', this.onSportLoaded);
        }
    }

    createSportFrameMobile() {
        const appState = this.props.appState;
        const sportSettings = appState.siteConfig.sportSettings;
        if (!appState.sport.mobileFrameLoaded) {
            // create sport iframe
            const server =
                sportSettings &&
                sportSettings.digitain ? sportSettings.digitain.server || '' : '';
            const oddsFormat = this.getSportBookOddsFormatIfExist();

            const params = {
                server: server.slice(0, -1),
                containerId: "sport_div_iframe",
                token: appState.userInfo.sid ? appState.userInfo.sid : '-',
                defaultLanguage: window.appState.language,
                loginTrigger: this.onSportLogin,
                oddsFormat: oddsFormat ? oddsFormat : 0
            };
            console.log("bootIframe")
            window.Bootstrapper.boot(params, { name: "Mobile" });

            // listen for load event
            const sportFrame = this.getSportFrame();
            if (sportFrame) {
                sportFrame.addEventListener('load', this.onSportLoaded);
            }
            appState.sport.mobileFrameLoaded = true;
        }
    }

    addSportScriptTag() {
        if (!this.sportIsAvailable) {
            this.showPopupUnavailability();
            return;
        }

        const { sportSettings } = this.props.appState.siteConfig;
        if (sportSettings && sportSettings.digitain && sportSettings.digitain.server) {
            const script = document.createElement('script');
            const scriptUrl = this.props.appState.isMobileOnly && this.props.appState.appSettings.mobileSportNew
              ? `${sportSettings.digitain.server}js/partner/bootstrapper.min.js`
              : `${sportSettings.digitain.server}js/Partner/IntegrationLoader.js`
            script.setAttribute('id', 'digitain-sport-script');
            script.setAttribute('src', scriptUrl);
            script.onload = this.onSportScriptLoaded;
            document.head.appendChild(script);
            window.onSportLogin = this.onSportLogin;
            window.onSportRegister = this.onSportRegister;
        } else {
            console.error("No embedment address for Sport Page");
        }
    }

    isSportScriptAdded(): boolean {
        return !!document.getElementById('digitain-sport-script');
    }

    getSportFrame() {
        const sportContainer = document.getElementById('sport_div_iframe');
        if (sportContainer) {
            return sportContainer.getElementsByTagName('iframe')[0] || null;
        }
        return null;
    }

    removeSportFrame = () => {
        const sportContainer = document.getElementById('sport_div_iframe');
        if (sportContainer && sportContainer.parentNode) {
            sportContainer.parentNode.removeChild(sportContainer);
        }
    };

    onSportLogin() {
        const appState = this.props.appState;
        this.props.history.push(appState.l('/login'));
    }

    onSportRegister() {
        const appState = this.props.appState;
        this.props.history.push(appState.l('/register'));
    }

    onSportLoaded() {
        const appState = this.props.appState;
        appState.sport.setIsLoading(false);
    }

    onSportScriptLoaded() {
        const appState = this.props.appState;
        appState.sport.scriptLoaded = true;
        this.loadSportFrameIfNotLoaded();
    }

    getSportBookOddsFormatIfExist() {
        if (this.props.appState.appSettings.digitainSportSettings?.oddsFormat) {
            switch (this.props.appState.appSettings.digitainSportSettings.oddsFormat) {
              case SportBookOddsFormats.DECIMAL: return 0;
              case SportBookOddsFormats.FRACTIONAL: return 1;
              case SportBookOddsFormats.AMERICAN: return 2;
              case SportBookOddsFormats.HONG_KONG: return 3;
              case SportBookOddsFormats.MALAY: return 4;
              case SportBookOddsFormats.INDO: return 5;
              default:
                return undefined;
            }
        }
        return undefined;
    }
}
