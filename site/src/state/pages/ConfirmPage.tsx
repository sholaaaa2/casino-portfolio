import * as React from 'react';
import {PageProps} from '../../App';
import {action} from 'mobx';
import template from '../../templates';
import {extractLocationArgs} from '../utils/BrowserUtils';
import {observer} from 'mobx-react';
import {SitePages} from '../meta';

@observer
class ConfirmPage extends React.Component<PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            const {code, email} = extractLocationArgs();
            appState.confirmEmailForm.code = code;
            appState.confirmEmailForm.email = email;
            appState.modal.showDoConfirmEmail();
            appState.page = SitePages.CONFIRM;
            appState.site.onPageOpen('confirm');
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}

export default ConfirmPage;