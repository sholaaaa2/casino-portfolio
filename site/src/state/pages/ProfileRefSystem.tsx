import {useEffect} from 'react';
import {observer} from 'mobx-react';
import template from '../../templates';
import {runInAction} from 'mobx';
import {PageProps, SitePages} from '../meta';

function _ProfileRefSystem(props: PageProps) {
    const {appState} = props;

    useEffect(() => {
        appState.startInitialize(props, () => {
            runInAction(() => {
                appState.page = SitePages.PROFILE_REF_SYSTEM;
                appState.site.onPageOpen(SitePages.PROFILE_REF_SYSTEM);
            });

            // this api calls require user info, so should be executed only then site config is loaded
            appState.api.startBatch();
            appState.ref.loadBonusInfo();
            appState.ref.loadInvites();
            appState.api.commit();
        });

        appState.finishInitialize();

        return appState.ref.clearState;
    }, []);

    return template(appState);
}

export const ProfileRefSystem = observer(_ProfileRefSystem);
