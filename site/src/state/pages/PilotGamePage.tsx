import {PageProps} from "../../App";
import {SitePages} from "../meta";
import template from "../../templates";
import {observer} from "mobx-react";
import React from "react";
import {action} from "mobx";

@observer
export default class PilotGamePage extends React.Component<PageProps> {


    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PILOT;
            appState.site.onPageOpen('pilot');
            document.body.classList.add('v-lobby-page');

            // here you may add additional API calls to be executed
            // during app initialization call
        });
        appState.finishInitialize();
    }

    componentWillUnmount() {
        document.body.classList.remove('v-lobby-page');
    }

    render() {
        const appState = this.props.appState;
        return template(appState);
    }

}
