import * as React from 'react';
import {observer} from 'mobx-react';
import template from '../../templates';
import {PageProps, SitePages} from '../meta';

@observer
export class KioskLobbyPage extends React.Component<PageProps> {
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.KIOSK_LOBBY;
            appState.site.onPageOpen('kiosk-lobby');
        });

        // here you may add additional API calls to be executed
        // during app initialization call
        appState.site.isKioskLobbyShown = true;
        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}
