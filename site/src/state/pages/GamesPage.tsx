import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import {GameGroupId, SitePages} from '../meta';

@observer
export default class GamesPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        const groupId = this.props.match.params.id || "all";

        appState.startInitialize(this.props, () => {
            appState.page = SitePages.MAIN;
            appState.games.setGroupId(groupId);
            appState.site.onPageOpen('games/' + groupId);
        });


        this.loadRecommendedGames();
        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    @action
    componentDidUpdate() {
        const appState = this.props.appState;
        const groupId = this.props.match.params.id || "all";

        if (!appState.routeParams || (!appState.routeParams.id && groupId !== 'all') || (appState.routeParams.id && appState.routeParams.id !== groupId)) {
            appState.games.setGroupId(groupId);
            appState.routeParams = this.props.match.params;
            appState.router = this.props.history;
            appState.site.onPageOpen('games/' + groupId);
        }
    }

    @action
    componentWillUnmount() {
        this.props.appState.games.setGroupId(null);
    }

    loadRecommendedGames() {
        const appState = this.props.appState;
        if (appState.games.groupId === GameGroupId.FAVORITES && appState.games.topGames === null && !appState.games.isTopGamesLoading) {
            appState.games.fetchPlayerTopGames();
        }
    }

    render() {
        return template(this.props.appState);
    }
}
