import {observer} from "mobx-react";
import * as React from "react";
import {PageProps} from "../../App";
import {action} from "mobx";
import template from "../../templates";
import { SitePages } from "../meta";

@observer
export default class LiveCasinoPage extends React.Component<PageProps> {

    readonly loadingScriptName = 'livecasino-loading-script';
    readonly iframeContainerName = 'live_casino_div_iframe';

    constructor(props: PageProps) {
        super(props);
        this.onLoadingScriptAdded = this.onLoadingScriptAdded.bind(this);
    }

    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.LIVE_CASINO;
            appState.liveCasino.setIsLoading(true);
            appState.site.onPageOpen('liveCasino');
            document.body.classList.add('hide-balance');

            if (!appState.site.isAccessRestricted && appState.liveCasino.isCurrencySupported()) {
                if (!this.isLoadingScriptAdded()) {
                    this.addScriptTag();
                } else {
                    this.props.appState.liveCasino.setIsLoading(false);
                }
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    componentDidUpdate() {
        if (!this.props.appState.site.isAccessRestricted && this.props.appState.liveCasino.isCurrencySupported()) {
            this.loadFrameIfNotLoaded();
        }
    }

    isLoadingScriptAdded(): boolean {
        return !!document.getElementById(this.loadingScriptName);
    }

    addScriptTag() {
        const script = document.createElement('script');
        const url = `https://${this.props.appState.siteConfig.liveCasinoSettings!!.iframe}/assets/frame.js`;
        script.setAttribute('id', this.loadingScriptName);
        script.setAttribute(
            'src',
            url
        );
        script.onload = this.onLoadingScriptAdded;
        document.head.appendChild(script);
    }

    getFrame() {
        const container = document.getElementById(this.iframeContainerName);
        if (container) {
            return container.getElementsByTagName('iframe')[0] || null;
        }
        return null;
    }

    onLoadingScriptAdded() {
        this.props.appState.liveCasino.setIsLoading(false);
        this.props.appState.liveCasino.scriptLoaded = true;
        this.loadFrame();
    }

    loadFrame() {
        const liveCasinoSettings = this.props.appState.siteConfig.liveCasinoSettings!!;
        const userInfo = this.props.appState.userInfo;
        const server = `https://${liveCasinoSettings.iframe}`;
        const sessionId = userInfo.sid.replace(/-/gi, "");
        // @ts-ignore
        new TvbetFrame({
            'lng'        : liveCasinoSettings.lang,
            'clientId'   : liveCasinoSettings.clientId,
            'tokenAuth'  : sessionId,
            'server'     : server,
            'containerId': this.iframeContainerName

        });
    }

    loadFrameIfNotLoaded() {
        const appState = this.props.appState;
        const frame = this.getFrame();
        if (!frame && appState.liveCasino.scriptLoaded) {
            this.loadFrame();
        }
    }

    componentWillUnmount() {
        document.body.classList.remove('hide-balance');
    }

    render() {
        const appState = this.props.appState;
        return template(appState);
    }

}
