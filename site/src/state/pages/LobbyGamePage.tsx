import * as React from 'react';
import {
    addGamePageClass,
    embedScriptCode,
    getGameSeoDescription,
    getGameSeoTitle,
    removeGamePageClass,
} from '../utils/GameUtils';
import {observer} from 'mobx-react';
import {getCapabilities, getDeviceType, setPageMeta} from '../utils/BrowserUtils';
import template from '../../templates';
import {action, autorun, observable, IReactionDisposer} from 'mobx';
import {PageProps} from '../../App';
import {getLaunchGameError} from '../../api/utils';
import {games, messages} from '../../api/proto';
import {GameItem} from '../actions/GamesActions';
import {GAME_CONTENT_ID} from '../../ui/GameScreen/GameScreen';
import { LobbyGame, SitePages } from '../meta';
import { getUrlParamsAsMap } from '../utils/getUrlParamsAsMap';
import { isFunCurrency } from '../utils/Currencies';

interface IProps {
    gameId: LobbyGame;
}

@observer
export default class LobbyGamePage extends React.Component<IProps & PageProps> {
    isEmbedded = false;
    autorunDisposer?: IReactionDisposer;
    @observable isGameStartInitialization: boolean = false;

    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.LOBBY_GAME;
            appState.site.onPageOpen(this.props.gameId);
            document.body.classList.add('game-lobby-page');
            window.addEventListener('message', this.handleMessage);
            if (
                this.props.gameId === 'sports2win' &&
                !appState.userInfo.guest &&
                !!appState.userInfo.currency?.id &&
                !!appState.userInfo.realCurrency?.id &&
                isFunCurrency(appState.userInfo.currency.id) &&
                !!appState.userInfo.realCurrency?.id
            ) {
                appState.site.changeUserCurrency(appState.userInfo.realCurrency.id, () => {
                    this.tryEmbedGame();
                });
            } else if (
                appState.userInfo.requiredCurrencyId !== null &&
                appState.userInfo.currency?.id &&
                appState.userInfo.requiredCurrencyId.notEquals(appState.userInfo.currency.id)
            ) {
                if (appState.userInfo.isRequiredCurrencyIdSupported) {
                    appState.site.changeUserCurrency(appState.userInfo.requiredCurrencyId, () => {
                        this.tryEmbedGame();
                    });
                } else {
                    if (appState.userInfo.guest) {
                        appState.modal.showGameRequiresLogin();
                    } else {
                        appState.modal.showGameRequiresAnotherCurrency(this.hideModalAndTryEmbedGame);
                    }
                }
            } else if (!appState.site.isAccessRestricted) {
                this.tryEmbedGame();
            }

        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();

        this.autorunDisposer = autorun(() => {
            if (this.isGameStartInitialization && appState.bonusesInfo.needPlay) {
                // region ---- update game page if new options for running game
                const runningGameStore = appState.runningGameStore;
                if (runningGameStore.runningGame && runningGameStore.runningGame.id) {
                    const activeGame = appState.games.findGameById(runningGameStore.runningGame.id);
                    let reloadPage = false;
                    if (activeGame && activeGame.disableIfActiveBonus) {
                        reloadPage = true;
                    }

                    if (reloadPage) {
                        window.location.reload();
                    }
                }
                // ---- endregion
            }
        });
    }

    @action
    handleMessage = (event: MessageEvent) => {
        if (event.data.type === 'event' && event.data.event === 'exit') {
            this.props.appState.router.push(this.props.appState.l('/'));
        }
        if (this.props.gameId === 'sports2win') {
            const action = event.data?.action ?? undefined;
            if (action) {
                switch (action) {
                case "ui:signin":
                    this.props.appState.modal.showLogin();
                    break;
                case "ui:signup":
                    this.props.appState.modal.showRegister();
                    break;
                }
            }
        }
    };

    componentWillUnmount() {
        removeGamePageClass();
        this.resetGameInfo();
        document.body.classList.remove('game-lobby-page');
        window.removeEventListener('message', this.handleMessage);
        if (this.autorunDisposer) {
            this.autorunDisposer();
        }
    }

    @action
    resetGameInfo() {
        const appState = this.props.appState;
        appState.runningGameStore.runningGame = null;
        appState.runningGameStore.rawHTML = null;
        appState.runningGameStore.iframe = null;
        appState.games.showSupportedCurrenciesForGame = false;
        this.isEmbedded = false;
        appState.lobbyGameActions.gameId = undefined;
    }

    @action.bound
    hideModalAndTryEmbedGame() {
        this.props.appState.modal.hideModal();
        this.tryEmbedGame();
    }

    @action
    tryEmbedGame() {
        const appState = this.props.appState;

        if (!this.isEmbedded) {
            this.isEmbedded = true;
            addGamePageClass();
            appState.lobbyGameActions.gameId = this.props.gameId;
            appState.api.getGameLaunchMethod(
                this.props.gameId,
                getDeviceType(this.props.gameId),
                getCapabilities(),
                this.onLaunchGameResponse.bind(this),
                getUrlParamsAsMap()
            );
        }
    }

    @action
    onLaunchGameResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        const appState = this.props.appState;
        const gameContainer = document.getElementById(GAME_CONTENT_ID);

        // wait until game page rendered
        if (!gameContainer) {
            setTimeout(this.onLaunchGameResponse.bind(this, msg, act), 1);
            return;
        }

        if (act.gameEmbed) {
            const game = appState.games.findGameById(this.props.gameId);
            const error = act.gameEmbed.error;
            this.updatePageMeta(game);

            if (error) {
                if (error === games.GameEmbedResponse.LaunchError.PROVIDER_ERROR) {
                    appState.games.launchGameProviderError = getLaunchGameError(error);
                } else {
                    if (
                        error === games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED &&
                        appState.userInfo.balance.currencyId!.toNumber() === 0
                    ) {
                        appState.games.launchGameError = 'error-demo-mode-not-supported';
                    } else if (
                        error === games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED
                    ) {
                        appState.games.launchGameError = getLaunchGameError(
                            games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED,
                        );
                        appState.games.showSupportedCurrenciesForGame = true;
                    } else {
                        appState.games.launchGameError = getLaunchGameError(error);
                    }
                }

                appState.modal.showGameEmbedError();
            } else {
                this.isGameStartInitialization = true;

                let embed = act.gameEmbed;

                if (embed.launchMethod!.screenWidth && embed.launchMethod!.screenHeight) {
                    appState.runningGameStore.gameScreenSize = {
                        width: embed.launchMethod!.screenWidth!,
                        height: embed.launchMethod!.screenHeight!,
                    };
                } else {
                    appState.runningGameStore.gameScreenSize = null;
                }

                if (embed.launchMethod!.div && embed.launchMethod!.div!.scriptPath!.length > 0) {
                    let scripts = embed.launchMethod!.div!.scriptPath!;

                    let checkOnLoad: () => void;
                    let embedScript = () => {
                        const scriptEmbed = document.createElement('script');
                        scriptEmbed.type = 'text/javascript';
                        scriptEmbed.onload = checkOnLoad;
                        scriptEmbed.src = scripts[0];
                        if (this.props.gameId === 'bc-sport') {
                            scriptEmbed.id = 'bcsportsbook';
                        }
                        scripts = scripts.slice(1);

                        gameContainer!.appendChild(scriptEmbed);
                    };

                    checkOnLoad = () => {
                        if (scripts.length) {
                            embedScript();
                        } else {
                            // eslint-disable-next-line no-eval
                            eval(embedScriptCode(appState, embed, game));
                        }
                    };

                    embedScript();
                } else if (embed.launchMethod!.div && embed.launchMethod!.div!.rawHTML) {
                    appState.runningGameStore.rawHTML = embed.launchMethod!.div!.rawHTML!;
                } else if (embed.launchMethod?.iframe?.src) {
                    let iframeSrc = embed.launchMethod.iframe.src;
                    if (embed.launchMethod.iframe.setLocation && !appState.appSettings.games.alwaysOpenInFrame) {
                        window.location.replace(iframeSrc + '&homeUrl=/');
                        return;
                    } else {
                        appState.runningGameStore.iframe = iframeSrc;
                    }
                } else {
                    console.error('Unknown launch method', embed);
                }
            }
        }
    }

    updatePageMeta(game: GameItem | null) {
        const appState = this.props.appState;
        appState.runningGameStore.runningGame = game;

        if (game) {
            const gameId = game.id!;
            const seoMeta = {
                'game-name': appState.games.getGameName(game),
                'site-name': appState.site.siteName,
                year: new Date().getFullYear(),
            };
            const title = appState.t(
                getGameSeoTitle(appState, game, 'seo-game-default-title'),
                seoMeta,
            );
            const description = appState.t(
                getGameSeoDescription(appState, game, 'seo-game-default-description'),
                seoMeta,
            );
            const ogImage = `${window.location.protocol}//${
                window.location.host
            }/media/thumb/300x300/${gameId}.jpeg`;

            setPageMeta(this.props.appState, title, description, ogImage);
        }
    }

    render() {
        return template(this.props.appState);
    }
}
