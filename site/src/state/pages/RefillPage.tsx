import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import 'qrcodejs/qrcode.min.js';
import { SitePages } from '../meta';

@observer
export default class RefillPage extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.REFILL;
            appState.site.onPageOpen('refill');
            appState.refillStore.resetPayway();

            // this api calls require user info
            if (!appState.userInfo.guest) {
                appState.api.startBatch();
                appState.billingHistory.loadHistory();
                appState.refillStore.loadPayways(!appState.refillStore.hasPayways);
                if (appState.appSettings.billing?.showBonusInfoBlock) {
                    appState.profileBonuses.tryLoadBonusesData();
                }
                appState.api.commit();
            }
        });

        // here you may add additional API calls to be executed
        // during app initialization call

        appState.finishInitialize();
    }

    @action
    componentDidUpdate() {
        const appState = this.props.appState;
        appState.refillStore.tryPreselectPayway();
    }

    componentWillUnmount() {
        const appState = this.props.appState;
        appState.refillStore.promoCodeForm.reset();
        if (appState.refillStore.payways.length === 1) {
            appState.refillStore.resetPayway();
        }
    }

    render() {
        return template(this.props.appState);
    }
}