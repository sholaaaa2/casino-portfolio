import * as React from 'react';
import {PageProps} from '../../App';
import template from '../../templates';
import {observer} from 'mobx-react';
import {action} from 'mobx';
import {NString} from '../meta';

type StaticPageState = {
    page?: string
};

function addSpecificPageClasses(page: NString) {
    if (page === 'sport-terms-and-conditions') {
        document.body.classList.add('sport-terms-and-conditions-page');
    }
}

function removeSpecificPageClasses() {
    document.body.classList.remove('sport-terms-and-conditions-page');
}

@observer
class StaticPage extends React.Component<PageProps, StaticPageState> {
    constructor(props: PageProps) {
        super(props);
        this.state = {};
    }

    @action
    componentDidMount() {
        const appState = this.props.appState;
        const renderPage = this.renderPage.bind(this);
        const page = this.props.match.params.id;

        appState.startInitialize(this.props, () => null);
        // here you may add additional API calls to be executed
        // during app initialization call

        if (page) {
            addSpecificPageClasses(page);
            renderPage(page);
        } else {
            console.error("Page without ID");
        }

        appState.finishInitialize();
    }

    componentDidUpdate(prevProps: Readonly<PageProps>) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            const page = this.props.match.params.id;
            if (page) {
                this.renderPage(page);
                addSpecificPageClasses(page);
            } else {
                console.error("Page without ID");
            }
        }
    }

    componentWillUnmount() {
        removeSpecificPageClasses();
    }

    render() {
        return template(this.props.appState);
    }

    private renderPage(page: string) {
        if (page !== this.state.page) {
            this.setState({page});

            this.props.appState.pages.load(page);
        }
        this.props.appState.site.onPageOpen('p/' + page);
    }
}

export default StaticPage;
