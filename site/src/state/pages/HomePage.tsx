import * as React from "react";
import { observer } from "mobx-react";
import { PageProps } from "../../App";
import { MainPage, SportPage, Sport2Page } from "./index";
import PilotGamePage from "./PilotGamePage";
import { site } from "../../api/proto";
import LobbyGamePage from './LobbyGamePage';

@observer
export default class HomePage extends React.Component<PageProps> {
    render() {
        const { siteConfig } = this.props.appState;
        if (siteConfig?.mainPageType === site.MainPageType.SPORT_PAGE) {
            return <SportPage {...this.props} />;
        }
        if (siteConfig?.mainPageType === site.MainPageType.SP2_PAGE) {
            return <Sport2Page {...this.props} />;
        }
        if (siteConfig?.mainPageType === site.MainPageType.PILOT_PAGE) {
            return <PilotGamePage {...this.props} />;
        }
        if (siteConfig?.mainPageType === site.MainPageType.BC_PAGE) {
            return <LobbyGamePage gameId="bc-sport" {...this.props} />;
        }
        if (siteConfig?.mainPageType === site.MainPageType.SPORTS2WIN_PAGE) {
            return <LobbyGamePage gameId="sports2win" {...this.props} />;
        }
        return <MainPage {...this.props} />;
    }
}
