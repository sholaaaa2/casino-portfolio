import { useEffect, useRef, ReactElement } from "react";
import { observer } from "mobx-react";
import { PageProps } from "../../App";
import template from "../../templates";
import { SitePages } from "../meta";

const _BlogPage = (props: PageProps): ReactElement => {
    const page = useRef(props.match.params.id);
    const { match } = props;

    const renderPage = (newPage = "index") => {
        page.current = newPage;
        props.appState.pages.load(
            newPage === "index" ? `blog/index` : `blog/posts/${newPage}`,
            newPage === "index" ? SitePages.BLOG_INDEX : SitePages.BLOG
        );
        props.appState.site.onPageOpen(
            newPage === "index" ? "blog" : "blog/" + newPage
        );
    };

    useEffect(() => {
        props.appState.startInitialize(props, () => {
            renderPage(page.current);
        });
        props.appState.finishInitialize();
    }, []);

    useEffect(() => {
        if (page.current !== match.params.id) {
            renderPage(match.params.id);
        }
    }, [match.params.id]);

    return template(props.appState);
};

export const BlogPage = observer(_BlogPage);
