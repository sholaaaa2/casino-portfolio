import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class AffiliateProgram extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.AFFILIATE_PROGRAM;
            appState.site.onPageOpen('profile/affiliate-program');

            // this api calls require user info, so should be executed only then site config is loaded
            appState.affiliateProgram.tryLoadPromoLinks();
            appState.affiliateProgram.loadRefInfo();
        });

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}