import * as React from 'react';
import {observer} from 'mobx-react';
import {PageProps} from '../../App';
import template from '../../templates';
import {action} from 'mobx';
import { SitePages } from '../meta';

@observer
export default class ProfileBonusBalances extends React.Component <PageProps> {
    @action
    componentDidMount() {
        const appState = this.props.appState;
        appState.startInitialize(this.props, () => {
            appState.page = SitePages.PROFILE_BONUS_BALANCES;
            appState.site.onPageOpen('profile/bonus-balance');
        });

        appState.userBonusesStore.fetchBonusBalances();

        appState.finishInitialize();
    }

    render() {
        return template(this.props.appState);
    }
}