type Options = {
    theme: string;
    labels: {
        days: string;
        hours: string;
        minutes: string;
        seconds: string;
    };
};

export default class FlipDown {
    version: string = '0.2.0';
    opts: Options;
    daysRemaining: number = 0;
    // FlipDown version
    public countdownEnded: boolean = false;
    // Initialised?
    private initialised: boolean = false;
    // Time at instantiation in seconds
    private now: number = this.getTime();
    // User defined callback for countdown end
    private hasEndedCallback: Function | null = null;
    // FlipDown DOM element
    private element = document.getElementById(this.elementId)!;
    // Rotor DOM elements
    private rotors: HTMLDivElement[] = [];
    private rotorLeafFront: HTMLDivElement[] = [];
    private rotorLeafRear: HTMLDivElement[] = [];
    private rotorTop: HTMLDivElement[] = [];
    private rotorBottom: HTMLDivElement[] = [];
    // interval
    private countdown: NodeJS.Timer | null = null;
    private clockValues: {
        d: number;
        h: number;
        m: number;
        s: number;
    } = {d: 0, h: 0, m: 0, s: 0};
    private clockStrings: {
        d: string;
        h: string;
        m: string;
        s: string;
    } = {d: '', h: '', m: '', s: ''};

    private clockValuesAsString: string[];
    private prevClockValuesAsString: string[];
    private daysremaining: number;

    constructor(// UTS to count down to
                private epoch: number,
                private elementId: string = 'flipdown',
                private opt: Options) {
        // Number of days remaining
        this.daysRemaining = 0;

        // Clock values as array
        this.clockValuesAsString = [];
        this.prevClockValuesAsString = [];

        // Parse options
        this.opts = this.parseOptions(this.opt);

        // Set options
        this.setOptions();
    }

    public start = () => {
        // Initialise the clock
        if (!this.initialised) {
            this.init();
        }

        // Set up the countdown interval
        this.countdown = setInterval(this.tick, 1000);

        // Chainable
        return this;
    };

    public stop = () => clearInterval(this.countdown!);

    public ifEnded = (cb: Function) => {
        this.hasEndedCallback = function () {
            cb();
            this.hasEndedCallback = null;
        };

        // Chainable
        return this;
    };

    private getTime() {
        return new Date().getTime() / 1000;
    }

    private hasCountdownEnded = () => {
        // Countdown has ended
        if (this.epoch - this.now < 0) {
            this.countdownEnded = true;

            // Fire the ifEnded callback once if it was set
            if (this.hasEndedCallback !== null) {
                // Call ifEnded callback
                this.hasEndedCallback();

                // Remove the callback
                this.hasEndedCallback = null;
            }

            return true;

            // Countdown has not ended
        } else {
            this.countdownEnded = false;
            return false;
        }
    };

    private parseOptions(opt: Options) {
        return {
            // Theme
            theme: opt.hasOwnProperty('theme') ? opt.theme : 'dark',
            // Labels
            labels: opt.labels
        };
    }

    private setOptions = () => {
        // Apply theme
        this.element!.classList.add(`flipdown__theme-${this.opts.theme}`);
    };

    private init = () => {
        this.initialised = true;

        // Check whether countdown has ended and calculate how many digits the day counter needs
        if (this.hasCountdownEnded()) {
            this.daysremaining = 0;
        } else {
            this.daysremaining = Math.floor(
                (this.epoch - this.now) / 86400
            ).toString().length;
        }
        const dayRotorCount = this.daysremaining <= 2 ? 2 : this.daysremaining;

        // Create and store rotors
        for (let i = 0; i < dayRotorCount + 6; i++) {
            this.rotors.push(this.createRotor(0));
        }

        const rotorLabels = ['days', 'hours', 'minutes', 'seconds'];

        // Create day rotor group
        const dayRotors = [];
        for (let i = 0; i < dayRotorCount; i++) {
            dayRotors.push(this.rotors[i]);
        }
        this.element.appendChild(
            this.createRotorGroup(dayRotors, rotorLabels[0])
        );

        // Create other rotor groups
        let count = dayRotorCount;
        for (let i = 0; i < 3; i++) {
            let otherRotors = [];
            for (let j = 0; j < 2; j++) {
                otherRotors.push(this.rotors[count]);
                count++;
            }
            this.element.appendChild(
                this.createRotorGroup(otherRotors, rotorLabels[i + 1])
            );
        }

        const getRotors = (name: string) => {
            const elements = this.element.getElementsByClassName(`rotor-${name}`);
            const result: HTMLDivElement[] = [];
            for (let i = 0, count = elements.length; i < count; i++) {
                result.push(elements[i] as HTMLDivElement);
            }
            return result;
        };


        // Store and convert rotor nodelists to arrays
        this.rotorLeafFront = getRotors('leaf-front');
        this.rotorLeafRear = getRotors('leaf-rear');
        this.rotorTop = getRotors('top');
        this.rotorBottom = getRotors('bottom');

        // Set initial values;
        this.tick();
        this.updateClockValues(true);

        return this;
    };

    private createRotorGroup(rotors: HTMLDivElement[], label: string) {
        var rotorGroup = document.createElement('div');
        rotorGroup.className = 'rotor-group';
        var dayRotorGroupHeading = document.createElement('div');
        dayRotorGroupHeading.className = 'rotor-group-heading';
        dayRotorGroupHeading.innerText = this.opts.labels[label];
        appendChildren(rotorGroup, rotors);
        rotorGroup.appendChild(dayRotorGroupHeading);
        return rotorGroup;
    }

    private createRotor(v: number = 0) {
        var rotor = document.createElement('div');
        var rotorLeaf = document.createElement('div');
        var rotorLeafRear = document.createElement('figure');
        var rotorLeafFront = document.createElement('figure');
        var rotorTop = document.createElement('div');
        var rotorBottom = document.createElement('div');
        rotor.className = 'rotor';
        rotorLeaf.className = 'rotor-leaf';
        rotorLeafRear.className = 'rotor-leaf-rear';
        rotorLeafFront.className = 'rotor-leaf-front';
        rotorTop.className = 'rotor-top';
        rotorBottom.className = 'rotor-bottom';
        rotorLeafRear.textContent = v.toString();
        rotorTop.textContent = v.toString();
        rotorBottom.textContent = v.toString();
        appendChildren(rotor, [rotorLeaf, rotorTop, rotorBottom]);
        appendChildren(rotorLeaf, [rotorLeafRear, rotorLeafFront]);
        return rotor;
    }

    private tick = () => {
        // Get time now
        this.now = this.getTime();

        // Between now and epoch
        var diff = this.epoch - this.now <= 0 ? 0 : this.epoch - this.now;

        // Days remaining
        this.clockValues.d = Math.floor(diff / 86400);
        diff -= this.clockValues.d * 86400;

        // Hours remaining
        this.clockValues.h = Math.floor(diff / 3600);
        diff -= this.clockValues.h * 3600;

        // Minutes remaining
        this.clockValues.m = Math.floor(diff / 60);
        diff -= this.clockValues.m * 60;

        // Seconds remaining
        this.clockValues.s = Math.floor(diff);

        // Update clock values
        this.updateClockValues();

        // Has the countdown ended?
        this.hasCountdownEnded();
    };

    private updateClockValues(init: boolean = false) {
        // Build clock value strings
        this.clockStrings.d = pad(this.clockValues.d.toString(), 2);
        this.clockStrings.h = pad(this.clockValues.h.toString(), 2);
        this.clockStrings.m = pad(this.clockValues.m.toString(), 2);
        this.clockStrings.s = pad(this.clockValues.s.toString(), 2);

        // Concat clock value strings
        this.clockValuesAsString = (
            this.clockStrings.d +
            this.clockStrings.h +
            this.clockStrings.m +
            this.clockStrings.s
        ).split('');

        // Update rotor values
        // Note that the faces which are initially visible are:
        // - rotorLeafFront (top half of current rotor)
        // - rotorBottom (bottom half of current rotor)
        // Note that the faces which are initially hidden are:
        // - rotorTop (top half of next rotor)
        // - rotorLeafRear (bottom half of next rotor)
        this.rotorLeafFront.forEach((el, i) => {
            el.textContent = this.prevClockValuesAsString[i];
        });

        this.rotorBottom.forEach((el, i) => {
            el.textContent = this.prevClockValuesAsString[i];
        });

        const rotorTopFlip = () => {
            this.rotorTop.forEach((el, i) => {
                if (el.textContent !== this.clockValuesAsString[i]) {
                    el.textContent = this.clockValuesAsString[i];
                }
            });
        };

        const rotorLeafRearFlip = () => {
            this.rotorLeafRear.forEach((el, i) => {
                if (el.textContent !== this.clockValuesAsString[i]) {
                    el.textContent = this.clockValuesAsString[i];
                    el.parentElement!.classList.add('flipped');
                    var flip = setInterval(() => {
                        el.parentElement!.classList.remove('flipped');
                        clearInterval(flip);
                    }, 500);
                }
            });
        };

        // Init
        if (!init) {
            setTimeout(rotorTopFlip, 500);
            setTimeout(rotorLeafRearFlip, 500);
        } else {
            rotorTopFlip();
            rotorLeafRearFlip();
        }

        // Save a copy of clock values for next tick
        this.prevClockValuesAsString = this.clockValuesAsString;
    }
}

function pad(n: string, len: number): string {
    return n.length < len ? pad(`0${n}`, len) : n;
}

function appendChildren(parent: HTMLElement, children: HTMLElement[]) {
    children.forEach(el => {
        parent.appendChild(el);
    });
}
