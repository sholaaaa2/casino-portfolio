import * as React from 'react';
// import './assets/flipdown.min';
import FlipDown from './assets/flipdown';
import './assets/flipdown.css';
import { appState } from '../../..';

interface CountdownProps {
    to: number;
    id: string;
}

export default class Countdown extends React.Component<CountdownProps> {
    clock: FlipDown;
    componentDidMount() {
        const { id, to } = this.props;

        this.clock = new FlipDown(to, id, {
            theme: 'wl',
            labels: {
                days: appState.t('ui-days'),
                hours: appState.t('ui-hours'),
                minutes: appState.t('ui-minutes'),
                seconds: appState.t('ui-seconds')
            }
        }).start();
    }

    componentWillUnmount() {
        this.clock.stop();
    }

    render() {
        return <div className="flipdown countdown" id={this.props.id} />;
    }
}
