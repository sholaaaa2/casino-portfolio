export interface CorrectLevel {
    L: number;
    M: number;
    Q: number;
    H: number;
}

export interface QRCodeConfig {
    text?: string;
    width?: number;
    height?: number;
    colorDark?: string;
    colorLight?: string;
    correctLevel?: number;
}

export abstract class QRCode {
    static CorrectLevel: CorrectLevel;

    constructor(containingElement: HTMLElement, config?: QRCodeConfig) {
    };

    clear() {
    };

    makeCode(text: string) {
    };
}