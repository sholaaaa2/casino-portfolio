import * as React from 'react';

interface CuracaoLicenseProps {
    secretKey: string;
    url: string;
}

export default class CuracaoLicense extends React.Component<CuracaoLicenseProps> {
    componentDidMount(): void {
        const {url, secretKey} = this.props;
        if (secretKey && url) {
            const s = document.createElement('script');
            s.src = `https://${secretKey}.${url}`;
            s.type = 'text/javascript';
            document.body.appendChild(s);
        }
    }

    shouldComponentUpdate(): boolean {
        return false;
    }

    render() {
        const {url, secretKey} = this.props;
        return url && secretKey ? (
            <div id={`apg-${secretKey}`}
                 data-apg-seal-id={secretKey}
                 data-apg-image-size="128"
                 data-apg-image-type="basic-small">
            </div>
        ) : null;
    }
}
