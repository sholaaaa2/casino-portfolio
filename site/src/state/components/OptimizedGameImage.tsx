import * as React from 'react';
import GameListState from '../actions/GameListState';
import { useImagePreload } from './../../hooks/useImagePreload';

type GameImageProps = {
    src: string;
    gameId: string;
    gamesList: GameListState;
    loadImage?: boolean;
    gameName: string;
};

const Preloader = () => (
    <div className="loading">
        <svg className="game-item__img" width="300" height="188" />
        <div className="loading-spinner_nobg">
            <div className="loading-spinner_inner">
                <div className="loading-spinner_indicator loading-spinner_indicator-first" />
                <div className="loading-spinner_indicator loading-spinner_indicator-second" />
                <div className="loading-spinner_indicator loading-spinner_indicator-third" />
                <div className="loading-spinner_indicator loading-spinner_indicator-fourth" />
                <div className="loading-spinner_indicator loading-spinner_indicator-fifth" />
            </div>
        </div>
    </div>
);

export const GameImageWrapper: React.FC<GameImageProps> = React.memo(props => {
    const { src, loadImage, gameName } = props;
    const [srcLoaded, loading] = useImagePreload(src);
    const showPreloader = loadImage === false || loading === true || !srcLoaded;

    return showPreloader ? (
        <Preloader />
    ) : (
        <img alt={gameName} className="game-item__img loaded" src={srcLoaded as any} />
    );
});
