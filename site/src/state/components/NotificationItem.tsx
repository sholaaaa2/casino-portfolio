import * as React from 'react';
import * as ReactDOM from 'react-dom';

const whichTransitionEvent = (): (string | undefined) => {
    let el = document.createElement('fakeelement');
    let transition;
    let transitions = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'MozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
    };

    Object.keys(transitions).forEach((transitionKey) => {
        if (el.style[transitionKey] !== undefined) {
            transition = transitions[transitionKey];
        }
    });

    return transition;
};

type Notification = {
    message: string;
    level: string;
    gid: number;
    lifetime: number;
};

type NotifcationItemProps = {
    notification: Notification;
    onRemove: (gid: number) => void;
    allowHTML: boolean;
    animationDisabled?: boolean;
    key: number;
};

export class NotificationItem extends React.Component<NotifcationItemProps> {
    notificationTimer: NodeJS.Timer | null = null;
    animationDisabled: boolean = false;
    removeCount: number = 0;
    mounted: boolean = false;

    state = {
        visible: undefined,
        removed: false
    };

    constructor(props: NotifcationItemProps) {
        super(props);

        this.hide = this.hide.bind(this);
        this.onTransitionEnd = this.onTransitionEnd.bind(this);
    }

    onTransitionEnd(e: Event) {
        if (this.removeCount > 0) {
            return;
        }
        if (this.state.removed) {
            this.removeCount += 1;
            this.props.onRemove(this.props.notification.gid);
        }
    }

    componentDidMount() {
        this.mounted = true;

        const {notification} = this.props;
        const element = ReactDOM.findDOMNode(this) as HTMLElement;
        const transitionEvent = whichTransitionEvent();

        if (transitionEvent) {
            element.addEventListener(transitionEvent, this.onTransitionEnd);
        } else {
            this.animationDisabled = true;
        }

        if (notification.lifetime) {
            this.notificationTimer = setTimeout(this.hide, notification.lifetime * 1000);
        }

        this.show();
    }

    componentWillUnmount() {
        const element = ReactDOM.findDOMNode(this) as HTMLElement;
        const transitionEvent = whichTransitionEvent();
        if (transitionEvent) {
            element.removeEventListener(transitionEvent, this.onTransitionEnd);
        }
        this.mounted = false;
    }

    render() {
        const {notification, allowHTML} = this.props;
        let className =
            `notificator__item notification notification--level-${notification.level}`;
        if (this.state.visible) {
            className += ' notification--visible';
        } else {
            className += ' notification--hidden';
        }

        let message;
        if (allowHTML) {
            message = (
                <div
                    className="notification__message"
                    dangerouslySetInnerHTML={{__html: notification.message}}
                />
            );
        } else {
            message = (
                <div className="notification__message">
                    {notification.message}
                </div>
            );
        }
        const dismiss = <span className="notification__dismiss">&times;</span>;
        const icon = (
            <div className="notification__icon-container">
                <i className={`icon-icon-${notification.level}`}/>
            </div>
        );

        return (
            <div className={className} onClick={this.hide}>
                {icon}
                {message}
                {dismiss}
            </div>
        );
    }

    private show() {
        setTimeout(() => {
                if (this.mounted) {
                    this.setState({
                        visible: true
                    });
                }
            }, 50);
    }

    private hide() {
        if (this.notificationTimer) {
            this.notificationTimer = null;
        }
        if (this.mounted) {
            this.setState({visible: false, removed: true});
        }

        if (this.animationDisabled) {
            this.props.onRemove(this.props.notification.gid);
        }
    }
}
