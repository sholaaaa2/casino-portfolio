import { NotificationItem } from './NotificationItem';
import * as React from 'react';
import AppState from '../AppState';

type NotificatorProps = {
    allowHTML: boolean;
    appState: AppState
};

type NotificatorState = {
    notifications: Notification[];
};

export type SimpleNotification = {
    message: string;
    level: NotificationItemLevel;
    gid?: number;
    lifetime?: number;
    dupMessages?: boolean;
};

type Notification = {
    message: string;
    level: NotificationItemLevel;
    gid: number;
    lifetime: number;
    dupMessages?: boolean;
};

export enum NotificationItemLevel {
  SUCCESS = 'success',
  ERROR = 'error'
}

export class Notificator extends React.Component<NotificatorProps, NotificatorState> {
    gid = 42;
    mounted = false;
    lifetime = 10;

    constructor(props: NotificatorProps) {
        super(props);

        props.appState.notificator = this;

        let notifications: Notification[] = [];
        this.state = {
            notifications
        };
        this.onRemoveNotification = this.onRemoveNotification.bind(this);
    }

    addNotification(notification: SimpleNotification) {
        let notifications = this.state.notifications;
        let newNotification: Notification = {
            message: notification.message,
            level: notification.level,
            gid: notification.gid || this.gid,
            lifetime: notification.lifetime || this.lifetime,
            dupMessages: notification.dupMessages,
        };

        newNotification.lifetime = parseInt(newNotification.lifetime.toString(), 10);
        newNotification.gid = this.gid;
        this.gid += 1;

        // don't add existing notification
        if (
            -1 !== notifications.findIndex(n => n.gid === newNotification.gid)
        ) {
            return false;
        }

        if (newNotification.dupMessages === false && notifications.findIndex(n => n.message === newNotification.message) >= 0) {
            return false;
        }

        notifications.push(newNotification);
        this.setState({ notifications: notifications });

        return newNotification;
    }

    showTestNotification(message: string) {
        this.addNotification({
            message: message,
            level: NotificationItemLevel.SUCCESS,
            lifetime: 50
        });
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const notifications = this.state.notifications.map(n => (
            <NotificationItem
                key={n.gid}
                notification={n}
                onRemove={this.onRemoveNotification}
                allowHTML={this.props.allowHTML}
            />
        ));
        return <div className="notificator">{notifications}</div>;
    }

    private onRemoveNotification(gid: number) {
        const notifications = (this.state as NotificatorState).notifications.filter(
            n => n.gid !== gid
        );
        if (this.mounted) {
            this.setState({ notifications });
        }
    }
}
