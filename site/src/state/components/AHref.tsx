import React from "react";

interface AHrefProps {
    href: string;
    className?: string;
    text: string;
    onClick?: () => void;
}

export const AHref = React.forwardRef<HTMLAnchorElement, AHrefProps>(
    (
        props,
        ref
    ): React.ReactElement => {
        const { href, className, text, onClick } = props;
        return (
            <a className={className} href={href} onClick={onClick} ref={ref}>
                {text}
            </a>
        );
    }
);
