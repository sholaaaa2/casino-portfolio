import * as React from 'react';
import AppState from '../AppState';
import { bonuses } from '../../api/proto';
import { processBonusPercentValue } from '../../api/utils';

type BonusProps = {
    key: string;
    appState: AppState;
    bonus: bonuses.UserBonusesListResponse.IPossibleBonusCode;
    index: number;
};

export const Bonus: React.FC<BonusProps> = props => {
    const { appState, bonus, index } = props;
    const wager = appState.profileBonuses.processBonusWager(
        bonus as bonuses.IBonusCode
    );
    const bonusNumber = index % 4;
    return (
        <div className={`bonus bonus--${bonusNumber}`}>
            <div
                className="bonus-block"
                onClick={appState.userBonusesStore.bonusClick}
            >
                {!appState.isMobileOnly && (
                    <img
                        alt={bonus.id!}
                        className="bonus-block__img"
                        src={`/images/bonuses/block/${bonusNumber}/1x.png`}
                        srcSet={`/images/bonuses/block/${bonusNumber}/2x.png 2x, /images/bonuses/block/${bonusNumber}/3x.png 3x`}
                    />
                )}
                <div className="bonus-block__text">
                    <div className="bonus-block__title">
                        {appState.t(bonus.meta!.bonus_title)}
                    </div>
                    <div className="bonus-block__amount">
                        {bonus.meta!.balance_bonus_amount_exact_percent ||
                            processBonusPercentValue(bonus.meta, appState)}
                    </div>
                </div>
            </div>
            <div className="bonus-get">
                <div
                    className="bonus-get__button"
                    onClick={appState.userBonusesStore.bonusClick}
                >
                    {appState.t('ui-bonuses-get')}
                </div>
                {wager && (
                    <>
                        <span className="bonus-get__wager">
                            {appState.t('ui-wager-colon')}
                        </span>
                        <span className="bonus-get__multiply">
                            {wager.multiplier}
                        </span>
                    </>
                )}
            </div>
        </div>
    );
};
