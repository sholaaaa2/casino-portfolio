import * as React from 'react';
import {proto} from '../../api/proto';
import {AutoSizer, Size, Grid} from 'react-virtualized';
import AppState from '../AppState';
import {GameItem} from '../actions/GamesActions';
import gameItemTemplate from '../../templates/components/gameItem/index';
import {OptimizedGameItem} from './OptimizedGameItem';
import {ViewOption} from '../actions/GamesActions';

interface GameListProps {
    content: AppState;
    games: GameItem[];
    gamesMeta?: {
      [gameId: string]: proto.icon.IMetaDataEntry[];
    } | {};
    view?: ViewOption;
}

export default class OptimizedGamesGrid extends React.Component<GameListProps> {
    _columnCount: number;
    _width: number;
    _height: number;
    _scrollTop?: number;
    _autoSizer: React.RefObject<AutoSizer> = React.createRef<AutoSizer>();
    _grid: React.RefObject<Grid> = React.createRef<Grid>();
    _windowWidth = window.innerWidth;

    isMobile: boolean;
    scrollHandlerMounted: boolean;
    vScrollable: React.RefObject<HTMLDivElement> = React.createRef<HTMLDivElement>();
    scrollTop: number = 0;
    visibleGames: string[];
    measuredElement: HTMLDivElement;

    raf: (callback: FrameRequestCallback) => number;
    caf: (handle: number) => void;
    timer: number;

    state: {
        columnWidth: number;
        rowHeight: number;
        height?: number;
        scrollTop: number;
        scrolling: boolean;
        measuring: boolean;
        needsMeasuring: boolean;
    };

    constructor(props: GameListProps) {
        super(props);

        this.raf = window.requestAnimationFrame || function (callback: any) {
            return window.setTimeout(callback, 1000 / 60);
        };

        this.caf = window.cancelAnimationFrame || function (id: number) {
            window.clearTimeout(id);
        };

        this.state = {
            columnWidth: 200,
            rowHeight: 200,
            scrollTop: 0,
            scrolling: false,
            measuring: true,
            needsMeasuring: true
        };

        this.visibleGames = [];

        this._cellRenderer = this._cellRenderer.bind(this);
        this._renderAutoSizer = this._renderAutoSizer.bind(this);
        this._renderGrid = this._renderGrid.bind(this);
        this._scrollHandler = this._scrollHandler.bind(this);
        this._onScrollEndHandler = this._onScrollEndHandler.bind(this);
        this._onWindowResize = this._onWindowResize.bind(this);
        this._onImageLoad = this._onImageLoad.bind(this);
        this._gameItemRef = this._gameItemRef.bind(this);
        this.raf = this.raf.bind(window);
        this.caf = this.caf.bind(window);
    }

    componentDidMount() {
        window.addEventListener('resize', this._onWindowResize);
        window.addEventListener('scroll', this._scrollHandler);
        this._onImageLoad();
    }

    componentWillReceiveProps(nextProps: GameListProps) {
        if (this.props.view !== nextProps.view) {
            this.setState(prevState => ({...prevState, needsMeasuring: true}));
        }
    }

    componentDidUpdate(prevProps: GameListProps) {
        if (this._grid.current && this._autoSizer.current && (this.props.games !== prevProps.games || this.props.gamesMeta !== prevProps.gamesMeta)) {
            this._autoSizer.current.forceUpdate();
            this._grid.current.forceUpdate();
        }
        this._onImageLoad();
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this._scrollHandler);
        window.removeEventListener('resize', this._onWindowResize);
    }

    render() {
        if (this.props.games.length === 0) {
            return null;
        }
        this.props.content.gameList.setVisibleState(this.visibleGames);

        if (this.state.needsMeasuring) {
            let game = this.props.games[0];
            return gameItemTemplate({content: this.props.content, game: game, loadImage: false, ref: this._gameItemRef});
        } else {
            this.props.content.gameList.setScrollingState(this.state.scrolling);

            return (
                <div ref={this.vScrollable}>
                    {this._renderAutoSizer({height: document.body.offsetHeight, scrollTop: this.state.scrollTop})}
                </div>
            );
        }
    }

    _gameItemRef(e: HTMLDivElement) {
        this.measuredElement = e;
    }

    _onImageLoad() {
        if (this.measuredElement && this.state.needsMeasuring) {
            this._width = this.measuredElement.parentElement!.offsetWidth;
            const width = this.measuredElement.offsetWidth;
            const height = this.measuredElement.offsetHeight;

            this.setState(prevState => ({
                ...prevState,
                columnWidth: width,
                rowHeight: height,
                needsMeasuring: false
            }));
        }
    }

    _onScrollEndHandler() {
        this.setState(prevState => ({...prevState, scrollTop: this.scrollTop, scrolling: false}));
    }

    _scrollHandler() {
        if (this.vScrollable.current) {
            if (this.timer) {
                this.caf(this.timer);
            }

            let scrollTop = 'scrollY' in window ? window.scrollY : document.documentElement.scrollTop;
            const bodyRect = document.body.getBoundingClientRect();
            const elemRect = this.vScrollable.current.getBoundingClientRect();
            const offsetTop = elemRect.top - bodyRect.top;
            scrollTop = Math.max(0, scrollTop - offsetTop);

            this.scrollTop = scrollTop;

            this.timer = this.raf(this._onScrollEndHandler);

            this.setState(prevState => ({...prevState, scrollTop: scrollTop, scrolling: true}));
        }
    }

    _onWindowResize() {
        if (this._windowWidth !== window.innerWidth) {
            this._windowWidth = window.innerWidth;
            this.setState(prevState => ({...prevState, needsMeasuring: true}));
        }
    }

    _cellRenderer(props: { columnIndex: number; rowIndex: number; key: React.Key; style: React.CSSProperties; isScrolling: boolean; }): React.ReactNode {
        let {columnIndex, rowIndex, key, style} = props;

        const content = this.props.content;
        const game = this.props.games[rowIndex * this._columnCount + columnIndex];
        let gameMeta = undefined;
        if (game) {
            this.visibleGames.push(game.id!);
            if (this.props.gamesMeta && this.props.gamesMeta[game.id!]) {
                gameMeta = this.props.gamesMeta[game.id!];
            }
        }

        return game ? (
            <div key={key} style={{
                ...style,
                position: 'absolute',
            }}>
                <OptimizedGameItem appState={content} game={game} gameMeta={gameMeta} />
            </div>
        ) : null;
    }

    _renderAutoSizer(params: { height: number; scrollTop?: number; }) {
        this._height = params.height;
        this._scrollTop = params.scrollTop;

        return (
            <>
                <AutoSizer
                    disableHeight={true}
                    height={params.height}
                    width={this._width}
                    scrollTop={this._scrollTop}
                    ref={this._autoSizer}>
                    {this._renderGrid}
                </AutoSizer>
            </>
        );
    }

    _renderGrid(props: Size) {
        this._columnCount = Math.round(this._width / this.state.columnWidth) || 5;

        const games = this.props.games;
        this.visibleGames.length = 0;

        return (
            <>
                <Grid
                    cellRenderer={this._cellRenderer}
                    columnWidth={this.state.columnWidth}
                    columnCount={this._columnCount}
                    autoHeight={true}
                    height={this._height}
                    overscanRowCount={2}
                    rowHeight={this.state.rowHeight}
                    rowCount={Math.ceil(games.length / this._columnCount)}
                    scrollTop={this._scrollTop}
                    width={this._width}
                    style={{overflow: 'inherit', outline: 'none'}}
                    containerStyle={{overflow: 'inherit'}}
                    ref={this._grid}
                />
            </>
        );
    }
}
