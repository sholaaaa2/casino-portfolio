import * as React from 'react';
import { default as classNames } from 'classnames';
import CountUp from 'react-countup';

type CounterProps = {
    end: number;
    duration: number;
    formattingFn: (value: number) => string;
};

interface CounterState {
    startValue: number;
    endValue: number;
    isAnimatingCounter: boolean;
}

export default class JackpotCounter extends React.Component<
    CounterProps,
    CounterState
> {
    constructor(props: CounterProps) {
        super(props);

        this.state = {
            startValue: 0,
            endValue: this.props.end,
            isAnimatingCounter: false
        };

        this.onStart = this.onStart.bind(this);
        this.onComplete = this.onComplete.bind(this);
    }

    shouldComponentUpdate(nextProps: CounterProps, nextState: CounterState) {
        return nextProps.end !== this.state.startValue;
    }

    componentDidUpdate(prevProps: CounterProps, prevState: CounterState) {
        if ((!prevState.isAnimatingCounter || !prevProps.end)) {
            if (prevProps.end !== this.props.end && prevState.startValue !== this.props.end) {
                this.setState({
                    startValue: prevProps.end,
                    endValue: this.props.end
                });
            }
        }
    }

    render() {
        const { startValue, endValue, isAnimatingCounter } = this.state;
        const duration = this.props.duration / 2000;
        const shouldRunAnimationForUpdate =
            startValue !== endValue && !isAnimatingCounter;
        const shouldRunAnimationForInit =
            startValue === 0 && !isAnimatingCounter;

        const counterClasses = classNames(
            'money__amount__counter',
            {
                'money__amount__counter-init--animate': shouldRunAnimationForInit
            },
            {
                'money__amount__counter-update--animate':
                    !isAnimatingCounter &&
                    shouldRunAnimationForUpdate &&
                    !shouldRunAnimationForInit
            }
        );

        return (
            <span className={counterClasses}>
                <CountUp
                    start={startValue}
                    end={endValue}
                    redraw={shouldRunAnimationForUpdate}
                    duration={duration}
                    useEasing={true}
                    onEnd={this.onComplete}
                    onStart={this.onStart}
                    formattingFn={this.props.formattingFn}
                />
            </span>
        );
    }

    onComplete() {
        this.setState(prevState => {
            return {
                ...prevState,
                isAnimatingCounter: false,
                startValue: prevState.endValue
            };
        });
    }

    onStart() {
        this.setState(prevState => {
            return { ...prevState, isAnimatingCounter: true };
        });
    }
}
