import * as React from 'react';
import {observer} from 'mobx-react';
import AppState from '../AppState';
import {Size} from '../utils/BrowserUtils';
import {JackpotWinnerData} from '../actions/JackpotWinner';
import {AnimationModes, Modal} from '../meta';
import {updateFadeClasses} from '../../utils/common';
import {runJackpotConfettiForWinner} from '../../utils/effects';

interface ModalJackpotProps {
    appState: AppState;
    viewport: Size;
    winner: JackpotWinnerData | null;
}

@observer
class ModalJackpot extends React.Component<ModalJackpotProps> {
    prevViewportWidth: number | null;
    prevViewportHeight: number | null;
    modal: Modal | null;

    componentDidMount() {
        this.init();
    }

    componentDidUpdate() {
        this.init();
    }

    render() {
        return null;
    }

    init() {
        const {appState, viewport, winner} = this.props;
        const modalJackpot = appState.getRef('modal-jackpot') as HTMLElement;
        const modalContent = appState.getRef('modal-jackpot-content') as HTMLElement;
        if (winner) {
            if (appState.modal.activeModal === Modal.JACKPOT_WINNER && !this.modal) {
                const animationMode = appState.appSettings.animations && appState.appSettings.animations.youWinJP;
                const confettiFn = animationMode === AnimationModes.CONFETTI ? runJackpotConfettiForWinner : null;
                updateFadeClasses(true, modalJackpot, confettiFn);
                this.modal = appState.modal.activeModal;
            }

            if (this.prevViewportWidth !== viewport.width || this.prevViewportHeight !== viewport.height) {
                this.prevViewportWidth = viewport.width;
                this.prevViewportHeight = viewport.height;
                appState.updateModalWrapperScale(modalContent);
            }
        } else {
            this.prevViewportWidth = null;
            this.prevViewportHeight = null;
            this.modal = null;
        }
    }
}

export default ModalJackpot;