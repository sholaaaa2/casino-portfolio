import * as React from 'react';
import LazyLoad from 'react-lazyload';

enum Status {
    LOADING,
    LOADED,
    ERROR
}

type GameImageProps = {
    isVisible?: boolean;
    src: string;
    gameName: string;
    loadingImgHeight?: number;
};

type GameImageState = {
    state: Status;
    loadAttempt: number;
    src: string;
};

const maxLoadAttempt = 3;
const timeoutBetweenAttempt = 1500;
const defaultImageSrc = '/images/default-game.png';

class GameImage extends React.Component<GameImageProps, GameImageState> {

    private mounted: boolean = false;

    constructor(props: GameImageProps) {
        super(props);
        this.state = {
            state: Status.LOADING,
            loadAttempt: 1,
            src: props.src,
        };

        this.handleImageLoaded = this.handleImageLoaded.bind(this);
    }

    componentWillReceiveProps(props: GameImageProps) {
        this.setState({
            src: props.src,
        })
    }

    handleImageLoaded() {
        if (this.mounted && this.state.state === Status.LOADING) {
            this.setState(() => {
                return {state: Status.LOADED};
            });
        }
    }

    handleImageError = () => {
        const {loadAttempt} = this.state;
        const {src} = this.props;
        if (loadAttempt > maxLoadAttempt) {
            this.setState({src: defaultImageSrc});
        } else {
            setTimeout(() => {
                this.setState({loadAttempt: loadAttempt + 1, src: `${src}?${loadAttempt}`, state: Status.LOADING });
            }, timeoutBetweenAttempt);
        }
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const img = new Image();
        img.onload = this.handleImageLoaded;
        img.onerror = this.handleImageError;
        img.src = this.state.src;
        return (
          this.state.state === Status.LOADED ? (
                  <img className="game-item__img loaded" src={this.state.src} alt={this.props.gameName} />) :
            (
              <div className="loading">
                  <svg className="game-item__img" width="300" height={this.props.loadingImgHeight ? this.props.loadingImgHeight : 188}/>
                  <div className="loading-spinner_nobg">
                      <div className="loading-spinner_inner">
                          <div className="loading-spinner_indicator loading-spinner_indicator-first"/>
                          <div className="loading-spinner_indicator loading-spinner_indicator-second"/>
                          <div className="loading-spinner_indicator loading-spinner_indicator-third"/>
                          <div className="loading-spinner_indicator loading-spinner_indicator-fourth"/>
                          <div className="loading-spinner_indicator loading-spinner_indicator-fifth"/>
                      </div>
                  </div>
              </div>)
        );
    }
}

type GameImageWrapProps = {
    src: string;
    lazyLoad?: boolean;
    gameName: string;
    loadingImgHeight?: number;
};

export default class GameImageWrap extends React.Component<GameImageWrapProps> {
    static defaultProps = {
        lazyLoad: true
    };

    render() {
        const {src, lazyLoad, gameName, loadingImgHeight} = this.props;

        return (
            lazyLoad ? (
                <LazyLoad once={true} height="100%" offset={150}>
                    <GameImage src={src} gameName={gameName} loadingImgHeight={loadingImgHeight} />
                </LazyLoad>) : (
                <GameImage src={src} gameName={gameName} />
            )
        );
    }
}
