import * as React from 'react';
import AppState from '../AppState';
import {proto} from '../../api/proto';

type GameMetaIconProps = {
    content: AppState;
    gameMeta?: proto.icon.IMetaDataEntry[];
};

type GameMetaIconState = {
    gameMeta?: proto.icon.IMetaDataEntry[];
    indicator: boolean;
};

export default class GameMetaIcon extends React.Component<GameMetaIconProps, GameMetaIconState> {
    mounted = false;
    indicatorTimeout: number | undefined;

    indicatorActivate = () => {
        if (this.mounted) {
            this.setState((prevState) => ({
                ...prevState,
                indicator: true,
            }));
            this.indicatorTimeout = window.setTimeout(() => {
                if (this.mounted) {
                    this.setState((prevState) => ({...prevState, indicator: false}));
                }
            }, 500);
        }
    };

    constructor(props: GameMetaIconProps) {
        super(props);
        this.state = {
            gameMeta: props.gameMeta ? props.gameMeta : undefined,
            indicator: false,
        };
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
        window.clearTimeout(this.indicatorTimeout);
    }

    shouldComponentUpdate(nextProps: GameMetaIconProps, nextState: GameMetaIconState) {
        if (
            nextProps.gameMeta !== this.props.gameMeta &&
            nextProps.gameMeta![0] !== this.props.gameMeta![0]
        ) {
            this.indicatorActivate();
            return true;
        }
        if (nextState.indicator !== this.state.indicator) {
            return true;
        }
        return false;
    }

    render() {
        const {gameMeta} = this.props;
        return (
            <div className="game-meta-container">
                <div className="game-meta">
                    {gameMeta?.map((metaEntry) => (
                        <div key={metaEntry?.key ?? ''} className="game-meta-block">
                            <div className="game-meta-first-line">
                                <div className={`key ${this.state.indicator ? 'active' : ''}`}>
                                    {metaEntry?.key ?? ''}
                                </div>
                                <div
                                    className={`indicator ${this.state.indicator ? 'active' : ''}`}
                                />
                            </div>
                            <div
                                className={`game-meta-second-line ${
                                    this.state.indicator ? 'active' : ''
                                }`}
                            >
                                {metaEntry?.value ?? ''}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
