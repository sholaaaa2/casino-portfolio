import * as React from "react";
import { default as ReactSlick, Settings } from "react-slick";
import SliderArrow from "./SliderArrow";
import { Direction } from "../../../state/actions/final-bonus/TextSlider";

interface ReactSlickWithCustomNavProps extends Settings {
    isDisabledPrevIcon: boolean;
    isDisabledNextIcon: boolean;
    activeIndex?: number;
}

export default class ReactSlickWithCustomNav extends React.Component<
    ReactSlickWithCustomNavProps
> {
    private sliderRef: React.RefObject<ReactSlick>;

    constructor(props: ReactSlickWithCustomNavProps) {
        super(props);
        this.sliderRef = React.createRef<ReactSlick>();
    }

    componentDidUpdate() {
        const { activeIndex } = this.props;
        const currentSliderRef = this.sliderRef.current;
        if (activeIndex !== undefined && currentSliderRef) {
            currentSliderRef.slickGoTo(activeIndex, false);
        }
    }

    render() {
        return (
            <ReactSlick
                {...this.props}
                ref={this.sliderRef}
                children={this.props.children}
                prevArrow={
                    <SliderArrow
                        direction={Direction.LEFT}
                        disabled={this.props.isDisabledPrevIcon}
                    />
                }
                nextArrow={
                    <SliderArrow
                        direction={Direction.RIGHT}
                        disabled={this.props.isDisabledNextIcon}
                    />
                }
            />
        );
    }
}
