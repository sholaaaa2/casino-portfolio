import React from "react";
import { default as classNames } from "classnames";
import { Direction } from "../../../state/actions/final-bonus/TextSlider";

export default function SliderArrow(props: {
    direction: string;
    disabled: boolean;
    onClick?: () => void;
}) {
    const classes = classNames(
        "pagination__item",
        "pagination__arrow",
        "pagination__arrow--withOutActiveStyles",
        { pagination__arrow_right: props.direction === Direction.RIGHT },
        { pagination__arrow_left: props.direction === Direction.LEFT },
        { "pagination__arrow--disabled": props.disabled }
    );

    return (
        <div onClick={props.onClick}>
            <div className={classes}>
                <div className="pagination__arrow_icon" />
            </div>
        </div>
    );
}
