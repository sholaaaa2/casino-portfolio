import React, {useEffect} from 'react';
import {useMutationObserver} from '../../hooks/useMutationObserver';

interface IProps {
    value: string;
    className?: string;
    children?: React.ReactNode;
}

// NOTE: It prevents situation when players change balance in devtools and send fake screenshots
export const Balance: React.FC<IProps> = ({value, className, children}) => {
    const balanceRef = React.useRef<HTMLSpanElement>(null);
    const checkForValueCorrectness = () => {
        if (balanceRef.current && balanceRef.current.innerText !== value) {
            balanceRef.current.innerText = value;
        }
    };

    useEffect(() => {
        checkForValueCorrectness();
    }, [value]);

    useMutationObserver(balanceRef, checkForValueCorrectness);

    return (
        <div className={className}>
            <span ref={balanceRef}>{value}</span>
            {children}
        </div>
    );
};
