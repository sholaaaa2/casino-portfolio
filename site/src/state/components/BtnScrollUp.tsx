import * as React from 'react';
// import './BtnScrollUp.scss';

export class BtnScrollUp extends React.Component{
  constructor(props: {}){
    super(props);
    this.state = { display: "none" };
  }
  componentDidMount(): void {
    window.addEventListener('scroll', this.showScrollUp);
  }
  handlerScrollUp = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth',
        });
  }

  showScrollUp = () =>{
    if(window.pageYOffset > 400 || window.scrollY > 400){
        this.setState({ display: "flex"  });
    } else {
      this.setState({ display: "none"  });
    }
  }
  render() {
      return (
        <div className={'btn-scroll-up'} style={this.state}  onClick={this.handlerScrollUp}>&#8686;</div>
      )
  }
}

export default BtnScrollUp;
