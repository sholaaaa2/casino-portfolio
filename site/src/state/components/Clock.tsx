import * as React from 'react';
// import { useClockTimer } from './../../hooks/useClockTimer';
import AppState from '../AppState';

interface ClockProps {
    appState: AppState;
}

export const Clock: React.FC<ClockProps> = React.memo(props => {
    // const currentDate = useClockTimer();
    return <span id="current-date"></span>;
});
