import * as React from 'react';
import AppState from '../AppState';
import {Size} from '../utils/BrowserUtils';

interface CardNotificationScaleProps {
    appState: AppState;
    viewport: Size;
}

export class CardNotificationScale extends React.Component<CardNotificationScaleProps> {
    componentDidUpdate() {
        this.props.appState.ui.scale.calcNotificationsScale();
    }

    render() {
        return null;
    }
}