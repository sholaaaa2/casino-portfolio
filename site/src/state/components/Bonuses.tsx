import * as React from 'react';
import AppState from '../AppState';
import { observer } from 'mobx-react';
import { Bonus } from './Bonus';

interface BonusesProps {
    appState: AppState;
}

@observer
export class Bonuses extends React.Component<BonusesProps> {

    componentDidMount(): void {
        if (!this.props.appState.userBonusesStore.displayedBonuses) {
            this.props.appState.userBonusesStore.fetchBonuses();
        }
    }

    render() {
        const { appState } = this.props;
        const { displayedBonuses } = appState.userBonusesStore;

        return displayedBonuses && displayedBonuses.length ? (
            <>
                <h2 className="bonuses-title">
                    {appState.t('ui-bonuses-title')}
                </h2>
                <div className="bonuses">
                    {displayedBonuses.map((bonus, index) => {
                        return (
                            <Bonus
                                key={bonus.id!}
                                appState={appState}
                                bonus={bonus}
                                index={index}
                            />
                        );
                    })}
                </div>
            </>
        ) : null;
    }
}
