import * as React from 'react';
import AppState from '../AppState';
import { GameItem } from '../actions/GamesActions';
import gameItemTemplate from '../../templates/components/gameItem/index';
import {proto} from '../../api/proto';

type GameItemProps = {
    appState: AppState;
    game: GameItem;
    loadImage?: boolean;
    useOptimizedGamesGrid?: boolean;
    lazyLoad?: boolean;
    gameMeta?: proto.icon.IMetaDataEntry[];
};

type GameItemState = {
    isGameFavorite: boolean;
    isFavoriteActionLoading: boolean;
    disableIfActiveBonus: boolean
    gameMeta?: proto.icon.IMetaDataEntry[];
};

export class OptimizedGameItem extends React.Component<
    GameItemProps,
    GameItemState
> {
    shouldComponentUpdate(nextProps: GameItemProps, nextState: GameItemState) {
        if (
            nextProps.game.id !== this.props.game.id ||
            nextProps.appState.site.isFavoritesGamesAllowed !==
                this.props.appState.site.isFavoritesGamesAllowed ||
            (nextProps.game.isFavorite !== undefined &&
                nextProps.game.isFavorite !== this.state.isGameFavorite) ||
            (nextProps.game.isFavoriteActionLoading !== undefined &&
                nextProps.game.isFavoriteActionLoading !==
                    this.state.isFavoriteActionLoading) ||
            (nextProps.game.disableIfActiveBonus !== undefined &&
                nextProps.game.disableIfActiveBonus !==
                    this.state.disableIfActiveBonus) ||
            (nextProps.gameMeta !==
                    this.state.gameMeta) ||
            nextProps.game.tags?.join('-') !== this.props.game.tags?.join('-')
        ) {
            this.setState(prevState => {
                return {
                    isGameFavorite: !!nextProps.game.isFavorite,
                    isFavoriteActionLoading: !!nextProps.game
                        .isFavoriteActionLoading,
                    disableIfActiveBonus: !!nextProps.game.disableIfActiveBonus,
                    gameMeta: nextProps.gameMeta,
                };
            });
            return true;
        } else {
            return false;
        }
    }

    constructor(props: GameItemProps) {
        super(props);
        this.state = {
            isGameFavorite: !!props.game.isFavorite,
            isFavoriteActionLoading: !!props.game.isFavoriteActionLoading,
            disableIfActiveBonus: !!props.game.disableIfActiveBonus,
            gameMeta: props.gameMeta,
        };
    }

    componentDidMount() {
        if (this.props.gameMeta && this.props.game?.id!) {
            this.props.appState.gameMetaIconActions.startFetchingForGame(this.props.game?.id!);
        }
    }

    componentWillUnmount() {
        if (this.props.gameMeta && this.props.game?.id!) {
            this.props.appState.gameMetaIconActions.stopFetchingForGame(this.props.game?.id!);
        }
    }

    render() {
        const {
            appState,
            game,
        } = this.props;
        return gameItemTemplate({content: appState, game: game});
    }
}
