import AppState from '../AppState';

export default class ExpandableContainer {
    appState: AppState;

    state: { [key: string]: boolean } = {};
    height: { [key: string]: number } = {};

    constructor(appState: AppState) {
        this.appState = appState;
        this.toggleState = this.toggleState.bind(this);
    }

    toggleState(e: React.MouseEvent<HTMLElement>) {
        const headerId = e.currentTarget.getAttribute('id');
        if (headerId) {
            const id = headerId.replace('expandable-container-header-', '');
            const expanded = this.state[id];
            const content = this.getContent(id);
            const icon = this.getIcon(id);
            if (content && icon) {
                if (expanded) {
                    this.setDOMState(content, icon, 0, '/images/icons/icon-expand.svg');
                } else {
                    this.setDOMState(content, icon, this.getHeight(id), '/images/icons/icon-collapse.svg');
                }
                this.state[id] = !expanded;
            }
        }
    }

    getContent(id: string): HTMLElement | null {
        return document.getElementById('expandable-container-content-' + id);
    }

    getIcon(id: string): HTMLElement | null {
        return document.getElementById('expandable-container-icon-' + id);
    }

    getHeight(id: string) {
        let height = this.height[id];
        if (!height) {
            const content = this.getContent(id);
            if (content) {
                const curHeight = content.style.height;
                content.style.height = 'auto';
                height = content.clientHeight;
                content.style.height = curHeight;
                this.height[id] = height;
            } else {
                height = 0;
            }
        }
        return height;
    }

    setDOMState(content: HTMLElement, icon: HTMLElement, height: number, iconSrc: string) {
        setTimeout(() => {
            icon.setAttribute('src', iconSrc);
            content.style.height = height + 'px';
        }, 50);
    }
}