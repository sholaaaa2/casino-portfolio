import * as React from 'react';
import AppState from '../AppState';
import { WebPushNotification } from '../actions/WebPushActions';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';


interface IWebpushNotifications {
    appState: AppState;
}

@observer
export default class WebpushNotifications extends React.Component<IWebpushNotifications> {

  render() {
    const {webPushActions} = this.props.appState;
    const {items} = webPushActions;
    return (
      <div className="new-notifications-container">
        {items.map(item => <NotificationWebpush key={item.id} notification={item} />)}
      </div>
    )
  }
}

const WebpushIcon = React.memo(function Icon(props: {iconUrl?: string | null}) {
  return props.iconUrl ? <img className="notification-card-icon" alt="" src={props.iconUrl} /> : null;
});

interface IProps {
  notification?: WebPushNotification;
};

@observer
class NotificationWebpush extends React.Component<IProps> {

    render() {
        const {notification} = this.props;
        if (!notification) return null;
        const isExternalLink = notification.message?.actionUrl && notification.message?.actionUrl.startsWith('http')

        return (
          <div className="new-notification-card" >
            <WebpushIcon iconUrl={notification.message?.iconUrl} />
            <div className="main-block">
              <div className="text">{notification.message?.text}</div>
              {
                !!notification.message?.actionText && !!notification.message?.actionUrl
                  ? (
                      <Link
                        className="action-button"
                        to={{pathname: notification.message.actionUrl}}
                        onClick={() => notification.close()}
                        target={isExternalLink ? '_blank' : undefined}
                      >
                        {notification.buttonText}
                      </Link>
                    )
                  : null
              }
            </div>
            <img
              className="notification-card-close-icon"
              src="/images/icons/icon-close.svg"
              alt=""
              onClick={() => notification.close()}
            />
          </div>
        );
    }
}
