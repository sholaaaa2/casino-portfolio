import * as React from 'react';
import {default as classNames} from 'classnames';
import AppState from '../../AppState';

interface ResendConfirmCodePropsState {
    resendInterval: number;
}

interface ResendConfirmCodeProps {
    appState: AppState;
    onClick?: React.MouseEventHandler<HTMLElement>;
    caption?: string;
}

class ResendConfirmCode extends React.Component<ResendConfirmCodeProps, ResendConfirmCodePropsState> {
    timeoutId?: (NodeJS.Timer | null) = null;

    componentWillUnmount() {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
        }
    }

    constructor(props: ResendConfirmCodeProps) {
        super(props);
        this.state = {
            resendInterval: props.appState.appSettings.registerForm.resendConfirmationCodeTimeout
        };
        this.onClick = this.onClick.bind(this);
        this.onResendTimeout = this.onResendTimeout.bind(this);
    }

    render() {
        const classes = classNames(
            'resend-confirm-code',
            {'resend-confirm-code-disabled': this.timeoutId !== null}
        );
        return (
            <a href="#resend" className={classes} onClick={this.onClick}>{this.props.caption}{this.renderTimer()}</a>
        );
    }

    renderTimer() {
        const resendInterval = this.state.resendInterval;
        return this.timeoutId ? (
            '(' + resendInterval.toString() + ')'
        ) : null;
    }

    onResendTimeout() {
        const interval = this.state.resendInterval - 1;
        if (interval === 0) {
            clearTimeout(this.timeoutId!);
            this.timeoutId = null;
        } else {
            this.timeoutId = setTimeout(this.onResendTimeout, 1000);
        }
        this.setState({resendInterval: interval});
    }

    onClick(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();

        if (this.timeoutId === null) {
            this.setState({
                resendInterval:
                    this.props.appState.appSettings.registerForm.resendConfirmationCodeTimeout,
            });
            this.timeoutId = setTimeout(this.onResendTimeout, 1000);
            if (this.props.onClick) {
                this.props.onClick(e);
            }
        }
    }
}

export default ResendConfirmCode;