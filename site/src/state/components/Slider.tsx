import * as React from 'react';
import {default as SlickSlider, LazyLoadTypes} from 'react-slick';
import 'slick-carousel/slick/slick.css';
import {Link} from 'react-router-dom';
import AppState from '../../state/AppState';

export interface Slide {
    // url props
    href?: string;
    target?: string;
    onClick?: Function;

    // image banner props
    image?: string;
    imageForMobile?: string;
    alt?: string;

    // iframe banner props
    iframe?: string;
    iframeWidth?: number;
    iframeHeight?: number;

    // video banner props
    mp4Path?: string;
    webmPath?: string;
    oggPath?: string;
    poster?: string;

    // other props
    displayForLanguages?: string[];
}

interface SliderProps {
    appState: AppState;
    slides: Slide[];
}

class Slider extends React.Component<SliderProps> {

    constructor(props: SliderProps) {
        super(props);
        this.onSliderInit = this.onSliderInit.bind(this);
        this.onSlideClick = this.onSlideClick.bind(this);
    }

    componentDidMount() {
        this.onSliderInit();
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const slides = this.props.slides.filter(slide => {
            return !slide.displayForLanguages || slide.displayForLanguages.indexOf(this.props.appState.language) !== -1;
        });

        // see full set of slider settings at https://github.com/kenwheeler/slick/#settings
        const settings = {
            slidesToShow: 1, // number of slides to show at a time
            slidesToScroll: 1, // of slides to scroll at a time

            dots: false,
            arrows: false,

            // animation settings
            speed: 500,

            // auto play settings
            autoplay: true,
            autoplaySpeed: 6000,
            infinite: true,
            lazyLoad: 'progressive' as LazyLoadTypes
        };

        return (
            <div className="slider">
                <SlickSlider {...settings}>
                    {slides.map(function (this: Slider, slide: Slide, index: number) {
                        const content = this.renderSlideContent(slide);
                        const isExternalLink = (slide.href && slide.href.startsWith('http')) || slide.target === '_blank';
                        return !!content ? (
                            isExternalLink ? (
                                <a key={index} href={slide.href} target={slide.target} className="slider_slide_link">
                                    {content}
                                </a>
                            ) : (
                                <Link key={index} to={slide.href ? slide.href : ''} className="slider_slide_link">
                                    {content}
                                </Link>
                            )
                        ) : null;
                    }, this)}
                </SlickSlider>
            </div>
        );
    }

    renderSlideContent(slide: Slide) {
        if (slide.image) {
            let imageSrc = slide.image;
            if (this.props.appState.isMobileOnly && slide.imageForMobile) {
                imageSrc = slide.imageForMobile;
            }
            return <img alt={slide.alt} src={imageSrc} className="slider_slide" />;
        } else if (slide.iframe) {
            return (
                <div className="slider_slide_iframe">
                    <svg width={slide.iframeWidth || 800} height={slide.iframeHeight || 400}/>
                    <iframe title={slide.alt} src={slide.iframe} className="slider_slide_iframe" frameBorder="0" scrolling="no"/>
                </div>
            );
        } else if (slide.webmPath || slide.mp4Path) {
            return (
                <video autoPlay={true} loop={true} muted={true} poster={slide.poster} className="slider_slide_video">
                    {!!slide.webmPath && (
                        <source src={slide.webmPath} type="video/webm"/>
                    )}
                    {!!slide.mp4Path && (
                        <source src={slide.mp4Path} type="video/mp4"/>
                    )}
                    {!!slide.oggPath && (
                        <source src={slide.oggPath} type="video/ogg"/>
                    )}
                </video>
            );
        } else {
            return null;
        }
    }

    onSliderInit() {
        const slides = document.getElementsByClassName('slick-slide');

        for (let i = 0, count = slides.length; i < count; i++) {
            slides[i].addEventListener('click', this.onSlideClick);
        }
    }

    onSlideClick(e: Event) {
        const element = e.currentTarget as HTMLElement;
        const index = element.getAttribute('data-index');
        const slides = this.props.slides;

        if (index !== null && index !== undefined && slides && slides.length) {
            const slide = slides[index];
            if (slide && slide.onClick) {
                slide.onClick();
            }
        }
    }
}

export default Slider;
