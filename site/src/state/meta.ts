import Long from 'long';
import {wallet, bonuses} from '../api/proto';
import {ReferralResource} from './actions/AffiliateProgramActions';
import {RouteComponentProps, StaticContext} from 'react-router';
import AppState from './AppState';

export class WithdrawalType {
    hint: string;
    id: string;
    name: ({ [k: string]: string } | null);
    minAmount: Long;
    maxAmount: Long;
    format: string;
    provider: wallet.WithdrawProvider;
    cardProvider?: string;
    cardLastDigits?: string;
    placeholder: ({ [k: string]: string } | null);
    fields?: (wallet.IPaywayField[] | null);
}

export type PageRenderProps = RouteComponentProps<{ [x: string]: string | undefined; }, StaticContext, any>;

export interface PageProps extends PageRenderProps {
    appState: AppState;
}

export type BonusMetaObj = { [k: string]: string } | null | undefined;

export enum RefillMethod {
    FIAT = 'fiat',
    CRYPTO = 'crypto'
}

export interface PhoneInfo {
    phoneCode: string;
    phoneNumber: string;
    phoneNumberWithCode: string;
    countryCode: string;
}

export interface PhoneParams {
    cleanPhoneNumber: string;
    phoneNumber: string;
    phoneCode: string;
    phoneCountry: string | null;
}

export interface CredentialsSettings {
}

export interface GamesSortingSettings {
    byGameId: { [key: string]: number };
    byGameTag: { [key: string]: number };
    inCategory: { [key: string]: { [key: string]: number } };
    providersCategoryAlphabetically: boolean;
    sortByWeightInNewCategory: boolean;
}

export interface GamesSetSettings {
    byGameId: string[];
    byGameTag: string[];
}

export interface InbetGameSettings {
    kf_list: string; // list of coefficients, like "[1, 100, 200, 300]"
    lang: string; // possible values are: fr_FR, pt_PT, es_ES, ru_RU, en_US
}

export interface InbetGamesLocaleSettings {
    defaultLanguage: string;
    matchingMap: { [key: string]: string };
}

export enum GamesSizeOptionsButtonPosition {
    LEFT = 'game-menu-left',
    RIGHT = 'game-menu-right',
}

export interface GamesSettings {
    alwaysOpenInFrame?: boolean;
    useOptimizedGamesGrid: boolean;
    openGameInTheSameTabOnMobile?: boolean;
    doNotShowGameCloseButtonOnMobile?: boolean;
    showMoreGames?: {
      enabled: boolean,
      gamesForLoad: number,
    }
    enabled: GamesSetSettings;
    disabled: GamesSetSettings;
    sorting: GamesSortingSettings;
    gameIconAnimationTimeMs?: number;
    randomizeIndexes?: number[];
    defaultGroup?: string;
    showGameItemsSizeOptions?: boolean;
    gamesSizeOptionsButtonPosition?: GamesSizeOptionsButtonPosition;
}

export interface IProvidersCustomizations {
    [provider: string]: {
        [field: string]: {
            regex?: string;
            error?: string;
            enabled?: boolean;
        }
    }
}

export type CurrencySettings = { [key: string]: { code?: string, format?: string } };

export interface MainMenu {
    newEntries?: string[];
    showAvailableBonusesCount?: boolean;
}

export enum BonusForPage {
    SKILLWHEEL = 'skillwheel',
    INFO = 'info',
}

export enum BonusType {
    REGISTRAION = 'registration_bonus',
    WELCOME = 'welcome_bonus',
    FOLOWING_DEPOSITS = 'following_deposits_bonus',
    FINAL_BONUS = 'final',
    SKILL_WHEEL = 'skillwheel',
    WITHDRAWALS_INSPECTOR = 'withdrawals-inspector',
}

export enum PlayState {
    PAUSE = 'pause',
    PLAY = 'play',
    STOP = 'stop'
}

export type IPossibleBonusCode = bonuses.UserBonusesListResponse.IPossibleBonusCode;

export enum SkillWheelState {
    STARTED = '1',
    ACTIVE = '2',
    ENDED = '3',
}

export enum Routes {
    ROOT = '/',
    MAIN = '/:ref(z[a-zA-Z0-9-_]+/?)?',
    GAMES = '/games/:id/:paginationIndex?',
    GAME = '/game/:id',
    SKILL_WHEEL = '/skill-wheel',
    TOKEN_AUTH = '/auth/:token',
    LOGIN = '/login',
    LOGOUT = '/logout',
    FATAL_ERROR = '/fatal-error',
    KIOSK_LOBBY = '/kiosk-lobby',
}

export enum Page {
    MAIN = 'main',
    GAMES = 'games',
    LOGIN = 'login',
    TOKEN_AUTH = 'token-auth',
    GAME = 'game',
    FATAL_ERROR = 'fatal-error',
    LOGOUT = 'logout',
    SKILL_WHEEL = 'skill-wheel'
}

export enum TimelineCashbackVisibility {
    MAIN_PAGE_ONLY = 'main-page-only',
    PROFILE_ONLY = 'profile-only',
    ALL = 'all',
}

export interface AppSettings {
    globalVars: { [key: string]: string };
    credentials?: CredentialsSettings;
    currencies?: CurrencySettings;
    mainMenu?: MainMenu;
    registerForm: {
        desktopAuthMode?: boolean;
        desktopAuthLogin?: boolean;
        desktopAuthRegistration?: boolean;
        desktopAuthPIN?: boolean;
        screenSaverMode?: boolean;
        formVersion?: FormVersion,
        ruleApprovalMode: RegisterFormRuleApprovalMode;
        resendConfirmationCodeTimeout: number;
        isOneClickRegistrationEnabled: boolean;
        isAnonymousRegistrationWithCryptoEnabled: boolean;
        socialRegistrationWithDefaultCurrency?: boolean;
        additionalLoginByPhoneButton?: boolean;
        includePromoCodeInput?: boolean;
    };
    skillWheel: {
        enabled: boolean;
        showBtn: boolean;
        animationDurationForOneItem: number; // ms
    };
    animations?: {
        bonusReceived: AnimationModes;
        someoneWinJP: AnimationModes;
        youWinJP: AnimationModes;
    };
    winners: {
        showTabBar: boolean;
        defaultTab: ActiveBlockWinners;
    };
    guestPopups: {
        enabled: boolean;
        list: GuestPopupItem[];
        mode: GuestPopupShowMode;
        excludeUrls?: string[];
    };
    games: GamesSettings;
    preloadImages: string[];
    promoCodeInput?: {
        newViewEnabled?: boolean;
        promoCodeLength?: number;
        includePasteButton?: boolean;
        seeMoreButton?: {
            enabled?: boolean;
            link?: string;
        }
    };
    promoMaterials: {
        promoBanners: ReferralResource[]
    };
    formatDatesForUS: boolean;
    mainPage?: { showTournaments?: boolean };
    suppliersMenu: {
        useProvidersIcons: boolean,
        useHorizontalMenu: boolean
    };
    jackpots?: {
        theme?: boolean | string;
        title?: string;
        maxNumber?: number;
        themeColors?: string[];
    };
    mutePhoneCodes: boolean;
    showBonusBalances: boolean;
    billing?: {
        redirectMobile?: boolean;
        showCommission?: boolean;
        autoFillFields?: boolean;
        newFormProviders?: string[];
        refillProvidersCustomizations?: IProvidersCustomizations;
        showBonusInfoBlock?: boolean;
        btcManualViaCashApp?: boolean;
        btcManualBanner?: boolean;
        btcWithdrawalAddressCheck?: boolean;
    };
    displayIntegerCurrencies?: boolean;
    loadBonusesOnInit?: boolean;
    finalBonusOpenModalOnLogin?: boolean;
    dailyBonusOpenModalOnLogin?: boolean;
    dailyBonusOpenModalOnInit?: boolean;
    loadBonusesInWithdrawals?: boolean;
    dailyBonusOpenModalOnGamePage?: boolean;
    wagerBonusOpenModalOnLogin?: boolean;
    wagerBonusOpenModalOnInit?: boolean;
    wagerBonusOpenModalOnGamePage?: boolean;
    wagerBonusRequireActivationConfirmation?: boolean;
    disabledFrameProcessors?: string[];
    sportPageFullWidth?: boolean;
    guestGamePage?: boolean;
    onlyAuthUserAccess?: boolean;
    kioskLobby: {
        enabled: boolean;
        electronOnly: boolean;
        idleTimeoutSeconds: number; // not less then 60
        longPressTimeoutSeconds: number;
    };
    showPinsalePaymentWarning?: boolean;
    hidePhoneCodeSelectWhenSingleCountry: boolean;
    showInvitationBonusesInfoInBonuses: boolean;
    sendSMSInvitesFromClientOnAndroid: boolean;
    sendAllSMSInvitesFromClient: boolean;
    sendSMSInvitesWithCard: boolean;
    showBMLoginWhenUserHasPhone: boolean;
    refillAndRedeemInstructionModal?: boolean;
    apkRef?: string | string[];
    showLoginInMobileHeader?: boolean;
    bonusButtonInvite?: boolean;
    mobileSportNew?: boolean;
    hideBonusPolicyInNotification?: boolean;
    newMobileJackpots?: boolean;
    redirectFrom404ToMainPage?: boolean;
    iOSSafariAddToHomeScreen?: boolean;
    digitainSportSettings?: {
        sportsBookView?: SportsBookViewType;
        clearDefaultStyles?: boolean;
        customCssUrl?: string;
        oddsFormat?: SportBookOddsFormats;
    },
    showFooterInLobbyGamePage?: boolean;
    showUserIdInProfile?: boolean;
    timelineCashbackVisibility?: TimelineCashbackVisibility;
    timelineCashbackCurrentCashbackCurrencyCustomFormat?: boolean;
    timelineCashbackCustomTimeFormat?: boolean;
    freespinsShop?: {
        theme: "default" | "minimal",
    };
}

export enum SportsBookViewType {
    EUROPEAN = 'europeanView',
    PAPER = 'paperView',
    AFRICA = 'africanView',
}

export enum SportBookOddsFormats {
    DECIMAL = 'decimal',
    FRACTIONAL = 'fractional',
    AMERICAN = 'american',
    HONG_KONG = 'hong_kong',
    MALAY = 'malay',
    INDO = 'indo',
}

export enum NotificationAnimationClass {
    ACTIVE = 'card-popup-container--visible',
    HIDE = 'card-popup-container--hidden',
    HIDDEN = 'hidden',
    QUEUED = 'card-popup-container--queued'
}

export enum Modal {
    LOGIN = 'login',
    REGISTER = 'register',
    CONFIRM_PHONE = 'confirmPhone',
    CONFIRM_EMAIL = 'confirmEmail',
    REGISTRATION_SECOND_STEP = 'registration-second-step',
    RESTORE = 'restore',
    AUTHORIZED_CONFIRMATION = 'authorizedConfirmation',
    GENERATED_LOGIN = 'generated-login',
    ERROR_500 = 'error-500',
    REFILL_WECASHUP = 'refill-wecashup',
    PAYWAY_WECASHUP_REFILL_FORM = 'payway-wecashup-refill-form',
    DO_RESTORE_PASSWORD_BY_MAIL = 'doRestorePasswordByMail',
    DO_CONFIRM_EMAIL = 'doConfirmEmail',
    JACKPOT_WINNER = 'jackpot-winner',
    FINAL_BONUSES = 'final-bonuses',
    FINAL_BONUS = 'final-bonus',
    CONFIRM_REFUSE_BONUS = 'confirm-refuse-bonus',
    CONFIRM_REFUSE_WAGER_BONUS = 'confirtm-refuse-wager-bonus',
    GAME_EMBED_ERROR = 'game-embed-error',
    REFILL_WALLETTEC_INFO = 'refill-wallettec-info',
    REFILL_SWITCH_INFO = 'refill-switch-info',
    ONE_CLICK_REGISTRATION_SUCCESS = 'one-click-registration-success',
    ANONYMOUS_REGISTRATION_SUCCESS = 'anonymous-registration-success',
    SESSION_STARTED_ON_DIFFERENT_DEVICE = 'session-started-on-different-device',
    AUTO_LOGOUT_AFTER_IDLE = 'auto-logout-after-idle',
    KEEP_ALIVE_PING = 'keep-alive-ping',
    DAILY_BONUS = 'daily-bonus',
    WAGER_BONUS = 'wager-bonus',
    WAGER_BONUS_CONFIRMATION = 'wager-bonus-confirmation',
    FREESPIN_BUY = 'freespin-buy',
    APK_ANDROID_BONUS = 'apk-android-bonus',
    REF_INVITE_WAS_SENT = 'ref-invite-was-sent',
    REF_INVITE_FORM = 'ref-invite-form',
    GAME_REQUIRES_ANOTHER_CURRENCY = 'game-requires-another-currency',
    GAME_REQUIRES_LOGIN = 'game-requires-login',
    PAYWAY_COIN_PAYMENTS_CRYPTO_REFILL_FORM = 'payway-coin-payments-crypto-refill',
    POSITIVE_REG_WAGER_BONUS_CLOSING = 'positive-reg-wager-bonus-closing',
    LOGIN_BY_PHONE = 'login-by-phone',
    REFILL_REDEEM = 'refill-redeem',
    LOW_BALANCE_POPUP = 'low-balance-popup',
    REVOKE_WITHDRAWAL_CONFIRMATION = 'revoke_withdrawal_confirmationl',
    WITHDRAWAL_LINK_MODAL_SKYCRYPTO = 'withdrawal-link-modal-skycrypto',
    BALANCE_WAGER_BONUS = 'balance-wager',
    WRONG_PROMO_CODE = 'wrong-promo-code',
    CONFIRM_CASHAPP_PHONE = 'confirm-cashapp-phone',
    BONUS_BY_CODE_WITH_REQ_DEPS = 'bonus-by-code-with-req-deps',
    NON_TAKEN_SPINS_GAMES = 'non-taken-spins-games',
}

export enum UserProfileGroupIconStatusClass {
    SUCCESS = 'icon-status-success',
    WARNING = 'icon-status-warning',
    LOADING = 'icon-status-loading'
}

export enum UserProfileGroup {
    PHONE = 'phone',
    EMAIL = 'email',
    USER_FIELDS = 'user-fields',
    PASSWORD = 'password'
}

export enum UserFieldType {
    DATE = 'datetime',
    STRING = 'string',
    UINT = 'uint'
}

export enum ActiveBlockWinners {
    LAST = 'last',
    TOP = 'top',
    JACKPOT = 'jackpot'
}

export type NLong = Long | null | undefined;

export type NString = string | null | undefined;
export type NNumber = number | null | undefined;
export type SelectOption = { value: string, caption: string, disabled?: boolean };

export enum GameGroupId {
    ALL = 'all',
    NEW = 'new',
    POPULAR = 'popular',
    FAVORITES = 'favorites'
}

export enum WallettecInvoiceData {
    PAYMENT_INSTRUCTION = 'paymentInstructions',
    SHORT_ID = 'shortId',
    BUSINESS_NO = 'businessNo',
    INVOICE_URL = 'invoiceURL',
    INVOICE_DETAILS = 'invoiceDetails',
    MULTITIER = 'multitier'
}

export interface WecashupRefillForm {
    phone: string;
    amount: string;
    callbackUrl: string;
    merchantUid: string;
    merchantPublicKey: string;
}

export interface GuestPopupItem {
    id: GuestPopupId;
    timeout: number;
    // default, true
    enabled?: boolean;
}

export enum GuestPopupId {
    BONUSES = 'guest-bonuses-popup',
    WINNERS = 'guest-winners-popup',
    JACKPOT = 'guest-jackpot-popup'
}

export enum GuestPopupShowMode {
    BY_INACTIVITY = 'by-inactivity',
    BY_TIMEOUT = 'by-timeout'
}

export enum AgeOfTheRegisteredUser {
    OVER_18_YEARS_OLD,
    OVER_21_YEARS_OLD
}

export enum RegisterFormRuleApprovalMode {
    SINGLE = 'single',
    MULTIPLE = 'multiple'
}

export enum ActiveTournamentState {
    LAST_ACTIVE = 'last-active'
}

export enum FormVersion {
    V1 = 'v1',
    V2 = 'v2',
    V3 = 'v3'
}

export enum ComponentNames {
    LOGIN_PAGE = 'LoginPage'
}

export enum SitePages {
    MAIN = 'main',
    SPORT = 'sport',
    GAME = 'game',
    LIVE_CASINO = 'liveCasino',
    LOBBY = 'lobby',
    WITHDRAW = 'withdraw-v2',
    REFILL = 'refill-v2',
    PAYMENT_DECLINE = 'payment-decline',
    PAYMENT_FAIL = 'payment-fail',
    PAYMENT_SUCCESS = 'payment-success',
    GENERATED_LOGIN = 'generated-login',
    LOGIN_VIA_TOKEN = 'login-via-token',
    RESTORE = 'restore',
    CONFIRM = 'confirm',
    STATIC = 'static',
    BLOG_INDEX = 'blog-index',
    BLOG = 'blog',
    AFFILIATE_PROGRAM = 'affiliate-program',
    TOURNAMENT = 'tournament',
    TOURNAMENT_LIST = 'tournaments-list',
    JACKPOT = 'jackpot',
    BONUSES = 'bonuses',
    PROFILE_BONUSES = 'profile-bonuses',
    PROFILE_BONUS_BALANCES = 'profile-bonus-balances',
    PROFILE_DOCUMENTS = 'profile-documents',
    PROFILE_EDIT = 'profile-edit',
    PROFILE_REF_SYSTEM = 'profile/ref-system',
    ERROR_404 = '404',
    PILOT = 'pilot',
    LOBBY_GAME = "game-lobby",
    SKILL_WHEEL = 'skill-wheel',
    FREESPINS_SHOP = 'freespins-shop',
    SP2_PAGE = 'sp2-page',
    FORCED_PHONE_CONFIRMATION = 'forced-phone-confirmation',
    LOGIN_VIA_SOCIAL = 'login-via-social',
    PROFILE_PHONE = 'profile-phone',
    PROFILE_EMAIL = 'profile-email',
    PROFILE_USER_ID = 'profile-user-id',
    PROFILE_EDIT_PHONE = 'profile-edit-phone',
    PROFILE_EDIT_EMAIL = 'profile-edit-email',
    PROFILE_MENU_MOBILE = 'profile-menu-mobile',
    PROFILE_EDIT_PASSWORD = 'profile-edit-password',
    PROFILE_DOCUMENTS_UPLOAD = 'profile-documents-upload',
    PROFILE_EDIT_PERSONAL_DATA = 'profile-edit-personal-data',
    PAYMENT_IN_PROGRESS = 'payment-in-progress',
    TIMELINE_CASHBACK = 'timeline-cashback',
    KIOSK_LOBBY = 'kiosk-lobby',
    TOURNAMENTS_INFO_LIST_PAGE = 'tournaments-info-list-page',
    TOURNAMENTS_INFO_LIST_PAGE_GAMES = 'tournaments-info-list-page-games',
    APP_INFO = 'app-info',
}

export enum AnimationModes {
    NONE = 'none',
    CONFETTI = 'confetti',
}

// games id's list
export const POSSIBLE_LOBBY_GAMES = [
    'fishing-dragnet',
    'fishtable',
    'pescador-de-oro',
    'bc-sport',
    'sports2win',
] as const;

export type LobbyGame = typeof POSSIBLE_LOBBY_GAMES[number];
