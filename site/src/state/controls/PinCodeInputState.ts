import {action, observable, computed} from 'mobx';
import {messages, users} from '../../api/proto';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {NotificationItemLevel} from '../components/Notificator';

const PIN_CODE_LENGTH = 5;

export default class PinCodeInputState extends ControlsState {
    @observable pinCode: string = '';
    @observable pinCodeForRepeat: string = '';
    @observable repeatError: boolean = false;
    @observable repeatPinErrorCount: number = 0;
    @observable signInError: boolean = false;
    isPasswordChanging = false;
    tokenLoginUrl: string | null = null;

    constructor(private appState: AppState) {
        super();
    }

    @action
    numPadTriggerKey = (event: React.SyntheticEvent<HTMLElement>) => {
        event.preventDefault();
        const maxPinCodeTries = this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries
          ?  this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries : 4;

        const key = event.currentTarget.getAttribute('data-key')!;
        if (this.pinCode?.length < PIN_CODE_LENGTH) {
            this.pinCode += key;
            this.signInError = false;
        }
        if (this.appState.phoneInputState.isPinSet && this.pinCode?.length === PIN_CODE_LENGTH) {
            this.signInByPinCode();
        } else if (this.appState.phoneInputState.isSetNewPin) {
            if (this.pinCode?.length === PIN_CODE_LENGTH && !this.pinCodeForRepeat) {
                this.pinCodeForRepeat = this.pinCode;
                this.pinCode = '';
                this.repeatError = false;
            }
            if (this.pinCode?.length === PIN_CODE_LENGTH && this.pinCodeForRepeat) {
                if (this.pinCode === this.pinCodeForRepeat) {
                    if (!this.isPasswordChanging) {
                        this.isPasswordChanging = true;
                        this.appState.api.changePassword(this.pinCode, this.onChangePassword);
                    }
                } else {
                    this.repeatError = true;
                    if (this.repeatPinErrorCount >= maxPinCodeTries) {
                        this.pinCodeForRepeat = '';
                        this.pinCode = '';
                        this.repeatPinErrorCount = 0;
                    } else {
                        this.pinCode = '';
                        this.repeatPinErrorCount ++;
                    }
                }
            }
        }
    }

    @computed
    get attemptsLeft() {
        const maxPinCodeTries = this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries
          ?  this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries : 4;
        return maxPinCodeTries - this.repeatPinErrorCount;
    }

    @action.bound
    resetAll() {
        this.pinCode = '';
        this.pinCodeForRepeat = '';
        this.repeatError = false;
        this.repeatPinErrorCount = 0;
        this.signInError = false;
    }

    @action.bound
    signInByPinCode() {
        const phone = this.appState.phoneInputState.getValueOrSetError();
        const password = this.pinCode;
        if (phone && password) {
            this.appState.api.signIn('', phone!.countryCode, phone!.phoneNumber, password!, this.onSignInResponse);
        }
    }

    @action.bound
    onSignInResponse(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        if (act.login && act.login.ok) {
            this.appState.signInActions.processSignInSuccess(act.login);
        } else {
            const maxPinCodeTries = this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries
              ?  this.appState.siteConfig?.entryConfig?.entryByPhone?.maxPinCodeTries : 4;
            this.signInError = true;
            this.pinCode = '';
            this.repeatPinErrorCount ++;
            if (this.repeatPinErrorCount >= maxPinCodeTries) {
                this.resetAll();
                this.appState.phoneInputState.onSendSmsClick(undefined, true);
            }
        }
    }

    @action.bound
    forgotPinClick() {
        this.resetAll();
        this.appState.phoneInputState.onSendSmsClick(undefined, true);
    }

    @action.bound
    onChangePassword(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        if (act.changePassword) {
            if (act.changePassword.ok) {
                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-pin-code-is-set'),
                    level: NotificationItemLevel.SUCCESS
                });
                this.appState.signInActions.processSignInSuccess(new users.LoginResponse({
                    tokenLoginUrl: this.tokenLoginUrl,
                }));
                this.tokenLoginUrl = null;
            } else {
                this.appState.notificator!.addNotification({
                    message: this.appState.t('ui-pin-code-is-not-set'),
                    level: NotificationItemLevel.ERROR
                });
            }
        } else {
            this.appState.notificator!.addNotification({
                message: this.appState.t('ui-pin-code-is-not-set'),
                level: NotificationItemLevel.ERROR
            });
        }
        this.isPasswordChanging = false;
    }

    @action
    numPadBackspace = (event: React.SyntheticEvent<HTMLElement>) => {
        event.preventDefault();

        if (this.pinCode?.length > 0) {
            this.pinCode = this.pinCode.slice(0,-1);
        }
    }

    @action
    numPadCancel = (event: React.SyntheticEvent<HTMLElement>) => {
        event.preventDefault();

        this.pinCode = '';
    }

    getTitle = () => {
        if (this.appState.phoneInputState.isPinSet) {
            return this.appState.t('ui-title-pin-code');
        } else {
            if (this.pinCodeForRepeat) {
                return this.appState.t('ui-repeat-pin-code');
            }
            return this.appState.t('ui-title-new-pin-code');
        }
    }
}
