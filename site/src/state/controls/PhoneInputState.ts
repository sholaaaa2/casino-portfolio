import {action, computed, observable} from 'mobx';
import {messages, site, users} from '../../api/proto';
import {NString, SelectOption} from '../meta';
import {BonusType} from '../../api/utils';
import {isNumberString} from '../utils/Validation';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {getParamsFromUrl} from '../utils/BrowserUtils';
import {AUTH_GET_PARAMS} from '../enums/AUTH_GET_PARAMS';
import { formatCurrencyAmount } from '../utils/Money';
import Long from 'long';

export type PhoneInputValue = {phoneCode: string; phoneNumber: string; countryCode: string};
export enum PhoneAuthState {
    PHONE_SET,
    SET_NEW_PIN,
    SET_PIN,
    SMS_CODE,
}

export default class PhoneInputState extends ControlsState {
    @observable formatedPhoneNumber: string = '';
    @observable phoneNumber: string = '';
    @observable phoneCodeValue?: NString;
    @observable countryValue?: NString;
    @observable selectedCountry: site.ICountryInfo | null = null;
    @observable countries: site.ICountriesInfo | null = null;
    @observable phoneAuthState: PhoneAuthState = PhoneAuthState.PHONE_SET;
    isUserWithPin = false;

    constructor(private appState: AppState) {
        super();
        this.onChangeCountryCode = this.onChangeCountryCode.bind(this);
        this.onPhoneNumberChange = this.onPhoneNumberChange.bind(this);
    }

    @computed
    get isPhoneSet() {
        return this.phoneAuthState === PhoneAuthState.PHONE_SET;
    }

    @computed
    get isSetNewPin() {
        return this.phoneAuthState === PhoneAuthState.SET_NEW_PIN;
    }

    @computed
    get isPinSet() {
        return this.phoneAuthState === PhoneAuthState.SET_PIN;
    }

    @computed
    get isSmsCode() {
        return this.phoneAuthState === PhoneAuthState.SMS_CODE;
    }

    @computed
    get phoneCode(): string {
        const country = this.country;
        return country && country.phoneCode ? country.phoneCode.toString() : '';
    }

    @computed
    get countryCode(): string {
        const country = this.country;
        return country && country.code ? country.code.toString() : 'undefined';
    }

    @computed
    get country(): site.ICountryInfo | null {
        return this.selectedCountry || this.defaultCountry;
    }

    @computed
    get defaultCountry(): site.ICountryInfo | null {
        let result = null;
        if (this.countries && this.countries.defaultCountry) {
            result = this.findCountryByCode(this.countries.defaultCountry);
        } else {
            console.error('Missing site config countries or defaultCountry');
        }
        return result;
    }

    @computed
    get phoneMask(): string {
        return this.country && this.country.mask ? this.country.mask.replace(/\d/gim, '#') : '';
    }

    @computed
    get phonePlaceholder(): string {
        return this.phoneMask.replace(/\d/gim, 'X').replace(/#/gim, 'X');
    }

    @computed
    get phoneCodeOptions(): SelectOption[] {
        const countries = this.countries ? this.countries.countries || [] : [];
        const result: SelectOption[] = countries.map(function(country: site.ICountryInfo) {
            return {value: country.code!, caption: `+${country.phoneCode} (${country.code})`};
        });
        result.unshift({value: 'undefined', caption: 'ui-phone-code-short', disabled: true});
        return result;
    }

    @computed
    get regWagerBonusAmount(): string | null {
        const bonus = this.appState.profileBonuses.possibleBonusesList.find(
            (_) =>
                _.meta.bonus_type === BonusType.REG_WAGER_BONUS &&
                _.meta.enabled === 'true' &&
                _.meta['should_show_in_possible_bonuses'] === 'true',
        );
        if (!!bonus && !!bonus.meta.bonus_amount && !!bonus.meta.amount_currency) {
            return formatCurrencyAmount(
                Long.fromString(bonus.meta.bonus_amount),
                Long.fromString(bonus.meta.amount_currency),
            );
        }
        return null;
    }

    @action.bound
    getStateFromUrl(): void {
        const [countryCode, phone] = getParamsFromUrl([
            AUTH_GET_PARAMS.COUNTRY,
            AUTH_GET_PARAMS.PHONE,
        ]);
        if (countryCode && phone) {
            this.selectedCountry = new site.CountryInfo({code: countryCode.toUpperCase()});
            this.phoneNumber = phone;
        }
    }

    @action
    onChangeCountryCode(e: React.ChangeEvent<HTMLSelectElement>) {
        this.selectedCountry = this.findCountryByCode(e.currentTarget.value);
        this.phoneNumber = '';
        this.formatedPhoneNumber = '';
        this.resetError();
    }

    @action
    onPhoneNumberChange(e: React.ChangeEvent<HTMLInputElement>) {
        if (isNumberString(e.currentTarget.value)) {
            this.phoneNumber = e.currentTarget.value;
            this.resetError();
        }
    }

    @action
    setCountries(countries: site.ICountriesInfo | null | undefined) {
        if (countries) {
            this.countries = countries;
        } else {
            console.error('Invalid countries value, check site config');
        }
    }

    findCountryByCode(code: string) {
        return this.countries && this.countries.countries
            ? this.countries.countries.find((c: site.ICountryInfo) => c.code === code) || null
            : null;
    }

    @action
    getValueOrSetError(): PhoneInputValue | null {
        if (!this.phoneNumber || this.countryCode === 'undefined' || !this.phoneCode) {
            this.error = this.appState.t('error-reset-password-phone-required');
            return null;
        } else {
            return {
                phoneCode: this.phoneCode,
                phoneNumber: this.phoneNumber,
                countryCode: this.countryCode,
            };
        }
    }

    @action
    resetForm() {
        super.resetForm();
        this.phoneNumber = '';
        this.formatedPhoneNumber = '';
        this.selectedCountry = null;
        this.stopResendTime();
        this.smsSend = false;
        this.isUserWithPin = false;
    }

    @computed
    get fullPhone(): string {
        return `+${this.phoneCode} ${this.phoneNumber}`;
    }

    @observable isLoading: boolean = false;

    @action
    setLoading = (isLoading: boolean) => {
        this.isLoading = isLoading;
    };

    @observable smsSend: boolean = false;

    @action
    setSmsSend = (isLoading: boolean) => {
        this.smsSend = isLoading;
    };

    @action
    setPhoneAuthState = (state: PhoneAuthState) => {
        this.phoneAuthState = state;
    }

    @action
    onBackToPhone = () => {
        this.setPhoneAuthState(PhoneAuthState.PHONE_SET);
        this.stopResendTime();
        this.appState.smsNumberInputState.resetForm();
    };

    @action
    onAuthorizationPhoneNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const val = e.currentTarget.value.replace(/x|-| |\(|\)+/gim, '');
        if (isNumberString(val)) {
            this.phoneNumber = val;
            this.formatedPhoneNumber = `+${this.phoneCode} ${e.currentTarget.value.replace(/x+/gim, '')}`;
            this.resetError();
        }
    };

    @action
    onAuthPhoneNumberChange = (values: any) => {
        const val = values.value;
        if (isNumberString(val)) {
            this.phoneNumber = val;
            this.formatedPhoneNumber = `+${this.phoneCode} ${values.formattedValue.replace(/x+/gim, '')}`;
            this.resetError();
        }
    };

    @action
    onSendSmsClick = (e?: React.MouseEvent<HTMLElement>, forceSend?: boolean) => {
        if (this.resendDisabled) {
            return;
        }
        this.setLoading(true);
        this.error = undefined;
        this.appState.api.getExternalEntryCode(
            this.phoneNumber,
            this.countryCode,
            this.isUserWithPin ? true : forceSend,
            this.appState.confirmationCodeMethodState.isChoiseAvailable ?
                 this.phoneAuthState === PhoneAuthState.SMS_CODE :
                 this.appState.confirmationCodeMethodState.isVoiceSelected,
            this.onExternalEntryCodeResponse,
        );
    };

    onExternalEntryCodeResponse = (
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) => {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.externalCode && act.externalCode.ok) {
            this.appState.confirmationCodeMethodState.isFirstRetry = this.phoneAuthState !== PhoneAuthState.SMS_CODE;
            this.setPhoneAuthState(PhoneAuthState.SMS_CODE);
            this.runResendTime();
        } else {
            if (
                !act.externalCode ||
                act.externalCode.error === users.ExternalEntryCodeResponse.CodeError.internal_error
            ) {
                this.error = this.appState.t('error-register-unknown-error');
            } else if (
                act.externalCode.error === users.ExternalEntryCodeResponse.CodeError.too_many_tries
            ) {
                this.error = this.appState.t('error-too-many-tries');
            } else if (
                act.externalCode.error === users.ExternalEntryCodeResponse.CodeError.pin_code_required
            ) {
                this.isUserWithPin = true;
                this.setPhoneAuthState(PhoneAuthState.SET_PIN);
            } else {
                this.error = this.appState.t('error-register-phone-invalid');
            }
        }
    };

    @observable resendTime: number = 0;
    resendTimer: NodeJS.Timeout | null = null;

    @action
    setResendTime = (time: number) => {
        this.resendTime = time;
    };

    @computed
    get resendDisabled(): boolean {
        return this.isLoading || this.resendTime > 0;
    }

    decreaseResendTime = () => {
        if (this.resendTime > 0) {
            this.setResendTime(this.resendTime - 1);

            if (this.resendTimer) {
                clearTimeout(this.resendTimer);
            }
            this.resendTimer = setTimeout(this.decreaseResendTime, 1000);
        }
    };

    runResendTime = () => {
        this.setResendTime(this.appState.appSettings.registerForm.resendConfirmationCodeTimeout);
        this.decreaseResendTime();
    };

    stopResendTime = () => {
        if (this.resendTimer) {
            clearTimeout(this.resendTimer);
        }
        this.setResendTime(0);
    };

    @computed
    get resendTitle(): string {
        const isResendTimerActive = this.resendTime > 0;
        const mins = Math.floor(this.resendTime / 60);
        const secs = this.resendTime - mins * 60;
        return `${
            this.appState.confirmationCodeMethodState.isChoiseAvailable
                ? this.appState.confirmationCodeMethodState.isFirstRetry
                    ? this.appState.t('ui-send-confirmation-by-call')
                    : this.appState.t('ui-resend-confirmation-by-call')
                : this.appState.t('ui-resend-confirmation')
        }${isResendTimerActive ? ' (' + mins + ":" + (secs < 10 ? '0' + secs : secs) + ')' : ''}`;
    }
}
