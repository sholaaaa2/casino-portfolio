import {observable, computed, action} from 'mobx';
import {ChangeEvent} from 'react';
import {site} from '../../api/proto';
import AppState from '../AppState';
import {Modal, SitePages} from '../meta';

export class ConfirmationCodeMethodState {
    constructor(private appState: AppState) {}

    @observable selectedMethod?: site.ConfirmationMethod = undefined;
    @observable codeMask = 'X';
    @observable isFirstRetry = false;

    @computed
    get isVoiceEnabled(): boolean {
        return (
            !!this.appState.siteConfig?.confirmationMethods &&
            this.appState.siteConfig.confirmationMethods.some(
                (_) => _ === site.ConfirmationMethod.VOICE,
            )
        );
    }

    @computed
    get isSmsEnabled(): boolean {
        return (
            !!this.appState.siteConfig?.confirmationMethods &&
            this.appState.siteConfig.confirmationMethods.some(
                (_) => _ === site.ConfirmationMethod.SMS,
            )
        );
    }

    @computed
    get isChoiseAvailable(): boolean {
        return this.isSmsEnabled && this.isVoiceEnabled;
    }

    @computed
    get isVoiceSelected(): boolean {
        return (
            this.appState.confirmationCodeMethodState.selectedMethod ===
            site.ConfirmationMethod.VOICE
        );
    }

    @computed
    get codeLength(): number {
        if (this.appState.siteConfig?.confirmationCodeLength) {
            return this.appState.siteConfig.confirmationCodeLength;
        }
        if (this.appState.page === SitePages.FORCED_PHONE_CONFIRMATION) {
            return 4;
        }
        if (
            this.appState.modal.activeModal === Modal.CONFIRM_CASHAPP_PHONE ||
            ((this.appState.modal.activeModal === Modal.LOGIN ||
                this.appState.modal.activeModal === Modal.REGISTER) &&
                this.appState.appSettings.registerForm.formVersion === 'v3')
        ) {
            return 6;
        }
        return 5;
    }

    @computed
    get codeIndexes(): number[] {
        return Array.from({length: this.codeLength}, (v, i) => i + 1);
    }

    @computed
    get codeFormat(): string {
        const formatWithoutDashes = '#'.repeat(this.codeLength);
        const formatWithDashes = formatWithoutDashes.match(/.{2}/g)?.join('-');
        if (this.codeLength % 2 === 0) {
            return formatWithDashes ?? formatWithoutDashes;
        }
        return formatWithDashes ? `#-${formatWithDashes}` : formatWithoutDashes;
    }

    @computed
    get codePlaceholder(): string {
        return this.codeFormat.replace(/#/g, this.codeMask);
    }

    @action.bound
    handleChange(event: ChangeEvent<HTMLSelectElement>) {
        this.selectedMethod = Number(event.currentTarget.value) as site.ConfirmationMethod;
    }

    @action.bound
    upgrade() {
        if (
            !!this.appState.siteConfig.confirmationMethods &&
            this.appState.siteConfig.confirmationMethods.length > 0 &&
            (this.selectedMethod === undefined ||
                !this.appState.siteConfig.confirmationMethods.includes(this.selectedMethod))
        ) {
            this.selectedMethod = this.appState.siteConfig.confirmationMethods[0];
        }
    }
}
