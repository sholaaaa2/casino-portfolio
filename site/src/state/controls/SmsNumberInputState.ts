import {action, computed, observable} from 'mobx';
import {isNumberString} from '../utils/Validation';
import {NString} from '../meta';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {messages} from '../../api/proto';
import {PhoneAuthState} from './PhoneInputState';

export default class SmsNumberInputState extends ControlsState {
    @observable value: string = '';

    constructor(private appState: AppState) {
        super();
    }

    @action.bound
    onValueChange(e: React.ChangeEvent<HTMLInputElement>) {
        const val = e.currentTarget.value.replace(/x|-| |\(|\)+/gim, '');
        if (isNumberString(val)) {
            this.value = val;
            this.resetError();

            if (this.value.length === this.appState.confirmationCodeMethodState.codeLength) {
                this.authorize();
            }
        }
    }

    @action
    onChangeNumber = (values: any) => {
        const val = values.value;
        if (isNumberString(val)) {
            this.value = val;
            this.resetError();

            if (this.value.length === this.appState.confirmationCodeMethodState.codeLength) {
                this.authorize();
            }
        }
    };

    @action
    resetForm() {
        super.resetForm();
        this.value = '';
    }

    @computed
    get minLength(): number | undefined {
        return this.appState.confirmationCodeMethodState.codeLength;
    }

    @computed
    get maxLength(): number | undefined {
        return this.appState.confirmationCodeMethodState.codeLength;
    }

    @computed
    get allowOnlyNumbers() {
        return true;
    }

    @computed
    get pattern(): NString {
        return this.allowOnlyNumbers ? '[0-9]*' : undefined;
    }

    @observable isLoading: boolean = false;

    @action
    setLoading = (isLoading: boolean) => {
        this.isLoading = isLoading;
    };

    @action
    authorize = () => {
        this.setLoading(true);
        this.appState.api.authorizeByExternalAuthCode(
            this.value,
            this.appState.phoneInputState.phoneNumber,
            this.appState.phoneInputState.countryCode,
            this.onAuthorizeByExternalAuthCodeResponse,
        );
        this.setLoading(false);
    };

    onAuthorizeByExternalAuthCodeResponse = (
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) => {
        this.appState.processApi(msg, act);
        this.setLoading(false);

        if (act.login?.ok) {
            this.resetForm();
            if (this.appState.siteConfig.entryConfig?.entryByPhone?.tiePinCodeToPhone) {
                window.setTimeout(() => {
                  if (this.appState.appSettings.registerForm?.formVersion === 'v3') {
                      this.appState.modal.showLogin();
                  } else {
                      this.appState.modal.showLoginByPhone();
                  }
                  this.appState.pinCodeInputState.tokenLoginUrl = act.login?.tokenLoginUrl ?? null;
                  this.appState.phoneInputState.setPhoneAuthState(PhoneAuthState.SET_NEW_PIN);
                }, 0);
            } else {
                this.appState.signInActions.processSignInSuccess(act.login);
            }
        } else {
            this.error = this.appState.t('error-wrong-sms-code');
        }
    };
}
