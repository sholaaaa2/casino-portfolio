import {action, observable} from 'mobx';
import {isValidEmail} from '../utils/Validation';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import { getParamsFromUrl } from '../utils/BrowserUtils';
import { AUTH_GET_PARAMS } from '../enums/AUTH_GET_PARAMS';

export default class EmailInputState extends ControlsState {
    @observable value: string = '';

    constructor(private appState: AppState) {
        super();
        this.onValueChange = this.onValueChange.bind(this);
    }

    @action.bound
    getStateFromUrl(): void {
        const [email] = getParamsFromUrl([AUTH_GET_PARAMS.EMAIL]);
        if (email) {
            this.value = email;
        }
    }

    @action
    onValueChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.value = e.currentTarget.value;
        this.resetError();
    }

    @action
    getValueOrSetError(): string | null {
        const email = this.value ? this.value.trim() : undefined;
        if (!email) {
            this.error = this.appState.t('error-login-email-required');
            return null;
        } else if (!isValidEmail(email)) {
            this.error = this.appState.t('error-invalid-email');
            return null;
        } else {
            return email;
        }
    }

    @action
    resetForm() {
        super.resetForm();
        this.value = '';
    }
}