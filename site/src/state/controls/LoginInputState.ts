import {action, computed, observable} from 'mobx';
import {isMobile, getParamsFromUrl} from '../utils/BrowserUtils';
import {isNumberString} from '../utils/Validation';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {NString} from '../meta';
import { AUTH_GET_PARAMS } from '../enums/AUTH_GET_PARAMS';

export default class LoginInputState extends ControlsState {
    @observable value: string = '';

    constructor(private appState: AppState) {
        super();
        this.onValueChange = this.onValueChange.bind(this);
    }

    @action.bound
    getStateFromUrl(): void {
        const [login] = getParamsFromUrl([AUTH_GET_PARAMS.LOGIN]);
        if (login) {
            this.value = login;
        }
    }

    @action
    onValueChange(e: React.ChangeEvent<HTMLInputElement>) {
        const noSpaces = e.currentTarget.value.indexOf(' ') === -1;
        const isValid = noSpaces && (!this.allowOnlyNumbers || isNumberString(e.currentTarget.value));
        if (isValid) {
            this.value = e.currentTarget.value;
            this.resetError();
        }
    }

    @action
    getValueOrSetError(): string | null {
        if (!this.value) {
            this.error = this.appState.t('error-generated-login-required');
            return null;
        } else if (this.minLength && this.value.length < this.minLength) {
            this.error = this.appState.t('error-login-too-short', {'length': this.minLength});
            return null;
        } else if (this.maxLength && this.value.length > this.maxLength) {
            this.error = this.appState.t('error-login-too-long', {'length': this.maxLength});
            return null;
        } else {
            return this.value;
        }
    }

    @action
    resetForm() {
        super.resetForm();
        this.value = '';
    }

    @computed
    get minLength(): number | undefined {
        // default 8 symbols
        // one click registration - 8 symbols
        // api registration - 6 symbols
        return this.appState.siteConfig ?
            (this.allowOnlyNumbers || this.appState.site.isWlAndWeblimaMode ? 6 : 8) :
            undefined;
    }

    @computed
    get maxLength(): number | undefined {
        // default 8 symbols
        // one click registration - 8 symbols
        // api registration - 6 symbols
        return this.appState.siteConfig ?
            (this.allowOnlyNumbers ? 6 : 8) :
            undefined;
    }

    @computed
    get allowOnlyNumbers() {
        return this.appState.site.isWeblimaMode;
    }

    @computed
    get pattern(): NString {
        return this.allowOnlyNumbers ? '[0-9]*' : undefined;
    }

    @computed
    get inputType(): string {
        return this.allowOnlyNumbers && isMobile() ? 'number' : 'text';
    }
}