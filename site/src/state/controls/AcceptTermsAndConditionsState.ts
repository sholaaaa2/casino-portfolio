import ControlsState from './ControlsState';
import {AgeOfTheRegisteredUser} from '../meta';
import {action, observable} from 'mobx';
import AppState from '../AppState';

export default class AcceptTermsAndConditionsState extends ControlsState {
    @observable termsAccepted: boolean = true;
    @observable selectedAge: AgeOfTheRegisteredUser | null = null;

    constructor(private appState: AppState) {
        super();
        this.toggleTermsAccepted = this.toggleTermsAccepted.bind(this);
        this.onChangeTermsAge = this.onChangeTermsAge.bind(this);
    }

    @action
    toggleTermsAccepted() {
        this.termsAccepted = !this.termsAccepted;
    }

    @action
    onChangeTermsAge(e: React.ChangeEvent<HTMLInputElement>) {
        const value = e.currentTarget.getAttribute('data-value') as string;
        const intValue = parseInt(value, 10);
        if (isNaN(intValue)) {
            console.error('Unknown player age enum value: ', value);
        } else {
            this.selectedAge = intValue;
        }
    }

    isChecked(value: AgeOfTheRegisteredUser): boolean {
        return this.selectedAge === value;
    }

    @action
    getValueOrSetError(): boolean | null {
        if (!this.termsAccepted && this.selectedAge === null) {
            this.error = this.appState.t('error-register-user-age-and-rule-approval-required');
            return false;
        } else {
            return true;
        }
    }
}