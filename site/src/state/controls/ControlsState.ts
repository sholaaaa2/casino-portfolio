import {action, observable} from 'mobx';

export default class ControlsState {
    @observable error?: string;
    @observable disabled: boolean = false;

    @action
    resetError() {
        this.error = undefined;
    }

    @action
    setDisabled(value: boolean) {
        this.disabled = value;
    }

    @action
    resetForm() {
        this.resetError();
        this.setDisabled(false);
    }
}