import {action, computed, observable} from 'mobx';
import {isNumberString} from '../utils/Validation';
import {NString} from '../meta';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {AuthorizationType} from '../actions/authorization/AuthorizationTypeState';

const EMAIL_OR_PHONE_MIN_LENGTH = 6;
const EMAIL_OR_PHONE_MAX_LENGTH = undefined;

const ONE_CLICK_MIN_LENGTH = 6;

export const BM_PASSWORD_LENGTH = 6;

export default class PasswordInputState extends ControlsState {
    @observable value: string = '';

    constructor(private appState: AppState) {
        super();
    }

    @action.bound
    onValueChange(e: React.ChangeEvent<HTMLInputElement>) {
        const noSpaces = e.currentTarget.value.indexOf(' ') === -1;
        const isValid = noSpaces && (!this.allowOnlyNumbers || isNumberString(e.currentTarget.value));
        if (isValid) {
            this.value = e.currentTarget.value;
            this.resetError();
        }
    }

    @action
    getValueOrSetError(): string | null {
        if (!this.value) {
            this.error = this.appState.t('error-login-password-required');
            return null;
        } else if (this.minLength && this.value.length < this.minLength) {
            this.error = this.appState.t('error-password-length-too-short', {'length': this.minLength});
            return null;
        } else if (this.maxLength && this.value.length > this.maxLength) {
            this.error = this.appState.t('error-password-length-too-long', {'length': this.maxLength});
            return null;
        } else {
            return this.value;
        }
    }

    @action
    resetForm() {
        super.resetForm();
        this.value = '';
    }

    @computed
    get minLength(): number | undefined {
        if (this.appState.authorizationTypeState.type === AuthorizationType.BY_LOGIN) {
            return this.appState.siteConfig ?
                (this.allowOnlyNumbers ? BM_PASSWORD_LENGTH : ONE_CLICK_MIN_LENGTH) :
                undefined;
        } else {
            return EMAIL_OR_PHONE_MIN_LENGTH;
        }
    }

    @computed
    get maxLength(): number | undefined {
        if (this.appState.authorizationTypeState.type === AuthorizationType.BY_LOGIN) {
            return this.appState.siteConfig && this.allowOnlyNumbers ?
                BM_PASSWORD_LENGTH :
                undefined;
        } else {
            return EMAIL_OR_PHONE_MAX_LENGTH;
        }
    }

    @computed
    get allowOnlyNumbers() {
        return this.appState.authorizationTypeState.type === AuthorizationType.BY_LOGIN && this.appState.site.isWeblimaMode;
    }

    @computed
    get pattern(): NString {
        return this.allowOnlyNumbers ? '[0-9]*' : undefined;
    }
}