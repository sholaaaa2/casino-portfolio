import {action, computed, observable} from 'mobx';
import {NLong, NString} from '../meta';
import {commons, site} from '../../api/proto';
import Long from 'long';
import ControlsState from './ControlsState';
import AppState from '../AppState';
import {isCryptoCurrencyId} from  '../utils/Currencies';
import { AuthorizationType } from '../actions/authorization/AuthorizationTypeState';

export default class CurrencySelectState extends ControlsState {
    @observable currencies: site.ISiteCurrencies | null = null;
    @observable selectedCurrency: NLong;

    constructor(private appState: AppState) {
        super();
        this.onValueChange = this.onValueChange.bind(this);
    }

    @computed
    get value(): string {
        if (this.appState.authorizationTypeState.type === AuthorizationType.ANONYMOUS && this.cryptoOptions.length > 0 && this.cryptoOptions[0].id) {
            return this.selectedCurrency ? this.selectedCurrency.toString() : this.cryptoOptions[0].id.toString();
        }
        return this.selectedCurrency ? this.selectedCurrency.toString() : this.defaultValue || '';
    }

    @computed
    get options(): commons.ICurrency[] {
        let result: commons.ICurrency[] = [];
        if (this.currencies && this.currencies.supported) {
            result = this.currencies.supported.filter((c: commons.ICurrency) => !c.blockRefill && c.id && c.id.greaterThan(0));
        } else {
            console.error('Missing site config currencies or supported currencies');
        }
        return result;
    }

    @computed
    get cryptoOptions(): commons.ICurrency[] {
        let result: commons.ICurrency[] = [];
        if (this.currencies && this.currencies.supported) {
            result = this.currencies.supported.filter((c: commons.ICurrency) => !c.blockRefill && c.id && c.id.greaterThan(0) && isCryptoCurrencyId(c.id));
        }
        return result;
    }

    @computed
    get defaultValue(): NString {
        if (this.currencies && this.currencies.defaultCurrencyId) {
            return this.currencies.defaultCurrencyId.toString();
        } else {
            console.error('Missing site config currencies or defaultCurrencyId');
            return null;
        }
    }

    @action
    onValueChange(e: React.ChangeEvent<HTMLSelectElement>) {
        if (e.currentTarget.value) {
            this.selectedCurrency = Long.fromString(e.currentTarget.value);
            this.resetError();
        } else {
            console.error('Invalid selected currency value: ', e.currentTarget.value);
        }
    }

    @action
    setCurrencies(currencies: site.ISiteCurrencies | null | undefined) {
        if (currencies) {
            this.currencies = currencies;
        } else {
            console.error('Invalid currencies value, check site config');
        }
    }

    @action
    getValueOrSetError(): Long | null {
        const value = this.value;
        if (value) {
            return Long.fromString(value);
        } else {
            this.error = this.appState.t('error-invalid-currency');
            return null;
        }
    }

    @action
    resetForm() {
        super.resetForm();
        this.selectedCurrency = undefined;
    }
}
