import PaywayForm from './forms/PaywayForm';
import Payway, {PaywayCategory} from './Payway';
import AppState from '../AppState';
import {action, computed, observable} from 'mobx';
import Long from 'long';
import PaywaysGroup from './PaywaysGroup';
import CryptoAddressForm from './forms/CryptoAddressForm';
import PaywayField, {PaywayFieldType} from './fields/PaywayField';
import {commons, wallet} from '../../api/proto';
import BillingTilesGroup from './BillingTilesGroup';
import BillingTile, { TilePayway } from './BillingTile';
import {IProvidersCustomizations} from '../meta';

export class BaseStore {
    appState: AppState;

    // form props
    @observable paywayForm: PaywayForm;
    @observable cryptoAddressForm: CryptoAddressForm;
    @observable selectedPayway: Payway | null = null;
    @observable selectedCurrency: Long | null = null;

    // payways props
    @observable isPaywaysLoading: boolean | null = null;
    @observable payways: Payway[] = [];
    @observable paywaysByGroup: PaywaysGroup[] = [];
    @observable paywaysByCategory: PaywayCategory[] = [];
    @observable error?: string;
    @observable useTiles: boolean = false;
    @observable tilesGroups: BillingTilesGroup[] = [];
    basePaywayUrl: string = '';

    constructor(appState: AppState) {
        this.appState = appState;
        this.paywayForm = new PaywayForm(this, appState);
        this.cryptoAddressForm = new CryptoAddressForm(appState);
    }

    getAmountLabelToken(): string {
        return '';
    }

    @computed
    get refillPageAllowed(): boolean {
        return this.appState.site.isAuthorized && this.appState.site.isElectronicPaymentsAllowed;
    }

    @computed
    get withdrawalsPageAllowed(): boolean {
        return this.appState.site.isAuthorized && this.appState.site.isElectronicPaymentsAllowed;
    }

    @computed
    get hasPayways(): boolean {
        return this.useTiles ? this.tilesGroups.length > 0 : this.payways.length > 0;
    }

    hasCryptoAddressWallet(): boolean {
        for (let payway of this.payways) {
            if (payway.fields.find((p: PaywayField) => p.type === PaywayFieldType.CRYPTO_ADDRESS)) {
                return true;
            }
        }
        return false;
    }

    getCryptoWallets(): string[] {
        const addresses: string[] = [];
        for (let payway of this.payways) {
            const paywayAddresses = payway.fields
                .filter((p: PaywayField) => (p.type === PaywayFieldType.CRYPTO_ADDRESS || p.type === PaywayFieldType.CRYPTO_ADDRESS_WITHOUT_QR) && p.value)
                .map((p) => p.value);
            addresses.push(...paywayAddresses) 
        }
        return addresses;
    }

    isPageDataLoaded(): boolean {
        return this.isPaywaysLoading === false;
    }

    @computed
    get isPageDataLoading(): boolean {
        return !!this.isPaywaysLoading;
    }

    @computed
    get isActionLoading(): boolean {
        return !!(this.paywayForm.isLoading);
    }

    @action
    selectPaywayById(id: string | null | undefined) {
        const payway = this.payways.find((p: Payway) => p.getId() === id);

        if (this.selectedPayway) {
            this.paywayForm.reset(this.selectedPayway);
        }

        if (payway && (!this.selectedPayway || this.selectedPayway.getId() !== payway.getId())) {
            this.prefillFields(payway);

            this.selectedPayway = payway;
            const currencies = payway.currencies;
            if (currencies.length === 1 && currencies[0].id) {
                this.selectedCurrency = currencies[0].id;
            } else if (currencies.length > 0 && this.appState.userInfo.balance.currencyId) {
                const balanceCurrencyId = this.appState.userInfo.balance.currencyId;
                if (currencies.find((c: commons.ICurrency) => c.id && c.id.equals(balanceCurrencyId))) {
                    this.selectedCurrency = balanceCurrencyId;
                }
            }
            this.onSelectedPaywayChange(payway);
        } else {
            this.selectedPayway = null;
            console.error('Unknown payway with id: ', id);
        }
    }

    @action
    prefillFields = (payway: Payway) => {
        if (this.selectedPayway && this.selectedPayway?.getProvider().type === payway.getProvider().type) {
            payway.fill(this.selectedPayway.fields)
        }
        const billingSettings = this.appState.appSettings.billing;
        if (billingSettings?.autoFillFields) {
            if (this.appState.rawUserInfo?.email) {
                payway.setEmail(this.appState.rawUserInfo?.email);
            }
            const phone = this.appState.rawUserInfo?.phone || this.appState.rawUserInfo?.newPhone;
            if (phone) {
                payway.setPhone(phone);
            }
        } else {
            const phoneField = payway.fields.find((f) => f.type === PaywayFieldType.PHONE);
            if (phoneField && phoneField.defaultValue) {
                payway.setPhone(phoneField.defaultValue);
            }
        }
    };

    onSelectedPaywayChange(payway: Payway) {
    }

    @action
    selectPaywayByUrl() {
        if (this.appState.routeParams && this.appState.routeParams.payway) {
            if (!this.selectedPayway || !(this.selectedPayway.getId() === this.appState.routeParams.payway)) {
                this.selectPaywayById(this.appState.routeParams.payway);
            }
        }
    }

    @action
    resetPayway() {
        if (this.selectedPayway) {
            this.paywayForm.reset(this.selectedPayway);
        }
        this.error = undefined;
        this.selectedPayway = null;
        this.selectedCurrency = null;
    }

    isLastFieldInSelectedPayway(payway: Payway | null | undefined, field?: PaywayField): boolean {
        const fields = payway?.fields ?? null;
        if (field === undefined) {
            return true;
        }
        if (payway?.provider.type === wallet.WithdrawProvider.CASHAPP && payway?.isRefill) {
            return false;
        }
        return !!(fields && fields.findIndex((f: PaywayField) => f.id === field.id) === fields.length - 1);
    }

    findTile = (paywayId: string): BillingTile | null | undefined => {
        let tile: BillingTile | null | undefined = null;

        this.tilesGroups.forEach((tg) => {
            if (!tile) {
                tile = tg.tiles.find((t) => t.paywayIds.join('_').toLowerCase().search(paywayId.toLowerCase()) !== -1);
            }
        });

        return tile;
    }

    @computed
    get selectedTile(): BillingTile | null | undefined {
        if (this.appState.routeParams && this.appState.routeParams.payway) {
            return this.findTile(this.possiblePaywayIds);
        }
        return null;
    }

    @computed
    get possiblePayways(): TilePayway[] {
        if (this.selectedTile) {
            return this.selectedTile.payways;
        }
        return [];
    }

    @computed
    get possiblePaywayIds(): string {
        if (this.appState.routeParams && this.appState.routeParams.payway) {
            return this.appState.routeParams.payway;
        }
        return '';
    }

    get providersCustomizations(): IProvidersCustomizations | null {
        return null;
    }

    @computed
    get activeBonusWarning() {
        return undefined;
    }
}
