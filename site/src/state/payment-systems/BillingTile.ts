import { observable, computed } from 'mobx';
import { commons, wallet } from '../../api/proto';
import AppState from '../AppState';
import { BADGE_TAGS } from './PaywaysGroup';
import Payway from './Payway';

export interface TilePayway {
  paywayId: string;
  tile: BillingTile;
  currency?: commons.ICurrency | null;
}

export default class BillingTile {
    appState: AppState;

    @observable id: string;
    @observable paywayIds: string[];
    @observable title: string;
    @observable icon: string;
    @observable fee: string;
    @observable tags: string[];
    @observable basePaywayUrl: string;

    @observable allPayways: Payway[] = [];

    get paywayId(): string {
        return this.paywayIds.length ? this.paywayIds[0] : this.id;
    }

    constructor(
        appState: AppState,
        billingTile: wallet.IBillingTile,
        basePaywayUrl: string,
        allPayways: Payway[]
    ) {
        this.appState = appState;

        this.id = billingTile.id || '';
        this.paywayIds = billingTile.paywayId || [];
        this.title = billingTile.title || '';
        this.icon = billingTile.icon || '';
        this.fee = billingTile.fee || '';
        this.tags = billingTile.tags || [];
        this.basePaywayUrl = basePaywayUrl;

        this.allPayways = allPayways;
    }

    get payways(): TilePayway[] {
        const tile = this;
        return this.paywayIds
            .map(paywayId => {
                const payway = this.allPayways.find(p => p.getId() === paywayId);
                return {
                    paywayId: paywayId,
                    tile,
                    currency:
                        payway && payway.currencies.length ? payway.currencies[0] : null
                };
            });
    }

    get paywayUrl() {
        return this.getPaywayUrl(this.paywayId);
    }

    getPaywayUrl(paywayId: string) {
        return this.appState.l(
            `${this.basePaywayUrl}/${paywayId}`
        );
        /*return this.appState.l(
            `${this.basePaywayUrl}/${paywayId}/${this.paywayIds
                .join('__')
                .toLowerCase()}`
        );*/
    }

    @computed
    get badgeTag(): string | undefined {
        return this.tags.find(tag => BADGE_TAGS.indexOf(tag) >= 0);
    }
}
