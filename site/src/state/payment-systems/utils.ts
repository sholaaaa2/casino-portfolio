import Payway, {PaywayCategory} from './Payway';
import PaywaysGroup from './PaywaysGroup';
import {v4 as uuid} from 'uuid';
import AppState from '../AppState';
import {wallet} from '../../api/proto';
import {WindowData} from './RefillStore';

export enum PaywayLogo {
    WALLET_ONE = '/images/payments-system/wallet-one.png',
    INTERKASSA = '/images/payments-system/interkassa.png',
    MIR = '/images/payments-system/mir.png',
    YANDEX_MONEY_CASH = '/images/payments-system/yandex_cash.png',
    PSB_RETAIL = '/images/payments-system/psbretail.png',
    EUROSET = '/images/payments-system/euroset.png',
    MASTERCARD = '/images/payments-system/mastercard.png',
    VISA = '/images/payments-system/visa.png',
    SVYAZNOY = '/images/payments-system/svyaznoy.png',
    NIXMONEY = '/images/payments-system/nixmoney.png',
    EXMO = '/images/payments-system/exmo.png',
    UKR_TERMINAL = '/images/payments-system/ukr-terminal.png',
    PAYEER = '/images/payments-system/payeer.png',
    QIWI = '/images/payments-system/qiwi.png',
    QIWI_TERMINAL = '/images/payments-system/qiwi-terminal.png',
    WORLD_TERMINAL = '/images/payments-system/world-terminal.png',
    CASH_TO_CODE = '/images/payments-system/cashtocode.png',
    SKRILL = '/images/payments-system/skrill.png',
    VISA_MC = '/images/payments-system/visa-mc.png',
    WECASHUP = '/images/payments-system/wecashup.png',
    YANDEX_MONEY = '/images/payments-system/yandex-money.png',
    NETELLER = '/images/payments-system/neteller.png',
    PAY_SAFE = '/images/payments-system/paysafe.png',
    HEXAPAY = '/images/payments-system/hexapay.png',
    DEFAULT = '/images/payments-system/default.png',
    BEELINE = '/images/payments-system/beeline.png',
    ALFA_BANK = '/images/payments-system/alfabank.png',
    MEGAFON = '/images/payments-system/megafon.png',
    TELE2 = '/images/payments-system/tele2.png',
    WEX = '/images/payments-system/wex.png',
    ADVCASH = '/images/payments-system/advcash.png',
    MTS = '/images/payments-system/mts.png',
    WEBMONEY = '/images/payments-system/webmoney.png',
    PRIVATE_BANK = '/images/payments-system/privat_bank.png',
    AIRTEL = '/images/payments-system/airtel.png',
    MPESA = '/images/payments-system/mpesa.png',
    MTN = '/images/payments-system/mtn.png',
    TIGO = '/images/payments-system/tigo.png',
    PERFECT_MONEY = '/images/payments-system/perfect-money.png',
    CASHLIB = '/images/payments-system/cashlib.png',
    PIASTRIX = '/images/payments-system/piastrix.png',
    TERMINALS_RF = '/images/payments-system/term_rf.png',
    VODACOM = '/images/payments-system/vodacom.png',
    CONNECTUM = '/images/payments-system/connectum.png',
    CASHAPP = '/images/payments-system/cashapp.svg',
    BTCPAYSERVER = '/images/payments-system/btcpayserver.png',
}

export enum AccountPlaceholder {
    CRYPTO_WALLET_ADDRESS = 'ui-placeholder-crypto-address',
    RU_PHONE_PLACEHOLDER = 'ui-withdrawal-direction-hint-phone',
    YANDEX_MONEY_ACCOUNT = 'ui-withdrawal-direction-hint-yamoney',
    CARD_NUMBER = 'ui-withdrawal-direction-hint-card',
    QIWI_WALLET = 'ui-withdrawal-direction-hint-qiwi',
    PAYEER_WALLET = 'ui-withdrawal-direction-hint-payeer',
    WEBMONEY_WALLET = 'ui-withdrawal-direction-hint-wmr',
    CASHLIB_WALLET = 'ui-withdrawal-direction-hint-cashlib',
    PIASTRIX_WALLET = 'ui-withdrawal-direction-hint-payeer',
}

export function mapToFormFields(map: { [k: string]: string } | null | undefined): wallet.RefillResponse.Form.IField[] {
    const result: wallet.RefillResponse.Form.IField[] = [];
    if (map) {
        Object.keys(map).forEach((key) => {
            result.push({
                type: 'hidden',
                name: key,
                id: key,
                value: map[key]
            });
        });
    }
    return result;
}

export function sumbitPaywayForm(windowData: WindowData, action: string, method: string, fields: wallet.RefillResponse.Form.IField[]) {
    let form = document.getElementById('payway-hidden-form') as HTMLFormElement;
    if (!form) {
        form = document.createElement('form')!;
        form.setAttribute('id', 'payway-hidden-form');
        document.body.appendChild(form);
    }

    form.innerHTML = '';
    form.setAttribute('action', action);
    form.setAttribute('method', method);
    if (windowData.redirect) {
        form.setAttribute('target', '_self');
    } else {
        form.setAttribute('target', windowData!.id);
    }
    form.setAttribute('accept-charset', 'UTF-8');

    for (let field of fields) {
        const input = document.createElement('input');
        if (field.type) {
            input.type = field.type;
        }

        if (field.name) {
            input.name = field.name;
        }

        if (field.id) {
            input.id = field.id;
        }

        if (field.value) {
            input.value = field.value;
        }
        form.appendChild(input);
    }
    form.submit();
    if (windowData && windowData.instance) {
        setTimeout(() => windowData!.instance!.focus(), 0);
    }
}

export function redirectByPayway(url: string, windowData: WindowData) {
    if (windowData.redirect) {
        window.location.href = url;
    } else if (windowData && windowData.instance) {
        windowData!.instance!.location.href = url;
        setTimeout(() => windowData!.instance!.focus(), 0);
    }
}

export function groupPaywaysByGroupId(appState: AppState, payways: Payway[], basePaywayUrl: string): PaywaysGroup[] {
    const map: { [key: string]: PaywaysGroup } = {};

    for (let payway of payways) {
        let groupId = payway.getGroup() || uuid();
        let group = map[groupId];
        if (group) {
            group.payways.push(payway);
        } else {
            map[groupId] = new PaywaysGroup(appState, basePaywayUrl);
            map[groupId].payways.push(payway);
        }
    }

    return Object.keys(map).map((k: string) => map[k]);
}

export function groupPaywaysByCategory(paywayGroups: PaywaysGroup[]): PaywayCategory[] {
    // 12 payways is a two rows with the default markup
    if (paywayGroups.length > 12) {
        // build groups
        const map: { [key: string]: PaywayCategory } = {};
        for (let group of paywayGroups) {
            let categoryName = group.getCategory();
            let category = map[categoryName];
            if (category) {
                category.payways.push(group);
            } else {
                map[categoryName] = {name: categoryName, payways: [group]};
            }
        }

        // sort groups order
        const result = Object.keys(map).map((k: string) => map[k]);
        result.sort((a: PaywayCategory, b: PaywayCategory) => b.payways.length - a.payways.length);

        // sort items order inside each group
        for (let category of result) {
            category.payways.sort((a: PaywaysGroup, b: PaywaysGroup): number => {
                if (a.getTranslatedName() < b.getTranslatedName()) {
                    return -1;
                }
                if (a.getTranslatedName() > b.getTranslatedName()) {
                    return 1;
                }
                return 0;
            });
        }
        return result;
    } else {
        return [{name: '', payways: paywayGroups}];
    }
}