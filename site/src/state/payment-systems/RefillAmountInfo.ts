import {action, computed, observable} from 'mobx';
import AppState from '../AppState';
import Long from 'long';
import {commons, messages} from '../../api/proto';
import {convertAmountToApi} from '../utils/Money';

export default class RefillAmountInfo {

    @observable isLoading: boolean = false;

    @observable amount: commons.IMoneyAmount | null = null;
    @observable amountRoyalty: commons.IMoneyAmount | null = null;

    @computed
    get totalAmount(): commons.IMoneyAmount | null {
        if (!this.amount || !this.amountRoyalty) {
            return null;
        }
        return {
            amount: this.amount.amount?.add(this.amountRoyalty?.amount || Long.fromNumber(0)),
            currency: this.amount.currency, 
        };
    }

    @computed
    get amountFormatted(): string {
        if (!this.amount) {
            return '';
        }
        return this.appState.money.formatCurrencyAmount(this.amount.amount, this.amount.currency);
    }

    @computed
    get amountRoyaltyFormatted(): string {
        if (!this.amountRoyalty || this.amountRoyalty.amount?.equals(Long.ZERO)) {
            return '';
        }
        return this.appState.money.formatCurrencyAmount(this.amountRoyalty.amount, this.amountRoyalty.currency);
    }

    @computed
    get totalAmountFormatted(): string {
        if (!this.totalAmount) {
            return '';
        }
        return this.appState.money.formatCurrencyAmount(this.totalAmount.amount, this.totalAmount.currency);
    }

    constructor(private appState: AppState) {
    }

    @action
    reset() {
        this.isLoading = false;
        this.amount = null;
        this.amountRoyalty = null;
        if (this.amountRoyaltyTimer) {
            clearTimeout(this.amountRoyaltyTimer);
        }
    }

    amountRoyaltyTimer: NodeJS.Timeout | null = null;

    processAmount = (amount: string, selectedCurrency: Long | null) => {
        if (this.amountRoyaltyTimer) {
            clearTimeout(this.amountRoyaltyTimer);
        }

        this.amountRoyaltyTimer = setTimeout(() => {
            this.loadDepositsRoyalty(amount, selectedCurrency);
        }, 800);
    };

    @action
    loadDepositsRoyalty = (sourceAmount: string, selectedCurrency: Long | null) => {
        this.reset();

        const currency = this.appState.currencies.findCurrencyById(selectedCurrency);
        if (!sourceAmount || !currency) {
            return;
        }
        const convertedAmount = convertAmountToApi(sourceAmount, currency);
        if (!convertedAmount) {
            return;
        }
        this.isLoading = true;

        this.amount = { amount: convertedAmount, currency: currency.id};
        this.appState.api.defineDepositsRoyalty(convertedAmount, currency.id, this.processDepositsRoyalty);
    };

    @action
    processDepositsRoyalty = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
        this.isLoading = false;
        if (act.defineDepositsRoyaltyResponse?.wlDepositRoyalty) {
            this.amountRoyalty = act.defineDepositsRoyaltyResponse?.wlDepositRoyalty;
        } else {
            this.amountRoyalty = null;
        }
    };
}
