import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export type SwitchInvoiceHTML = { title: string, body: string };

export default class Switch extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const invoiceHTMLData = response && response.invoiceData && Object.keys(response.invoiceData).length ?
            this.processInvoiceData(response.invoiceData) :
            null;

        if (invoiceHTMLData) {
            this.appState.modal.switchModalData = invoiceHTMLData;
            this.appState.modal.showRefillSwitchInfo();
        } else {
            console.error('No payment data to display to user');
        }
    }

    processInvoiceData(invoiceData: { [k: string]: string }): SwitchInvoiceHTML[] | null {
        const result: SwitchInvoiceHTML[] = [];

        for (let key in invoiceData) {
            let value = invoiceData[key];
            if (value) {
                if (key === "Value") {
                    result.push({title: key, body: (value + " EUR")});
                } else {
                    result.push({title: key, body: value});
                }
            }
        }

        return result.length ? result : null;
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }
}