import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NotificationItemLevel} from '../../components/Notificator';
import {WallettecInvoiceData} from '../../meta';
import {convertCamelCase} from '../../utils/BrowserUtils';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export type WallettecInvoiceHTML = { title: string, body: string };

export default class Walletec extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const invoiseHTMLData = response && response.invoiceData && Object.keys(response.invoiceData).length ?
            this.processInvoiceData(response.invoiceData) :
            null;

        if (invoiseHTMLData) {
            this.appState.modal.walletecModalData = invoiseHTMLData;
            this.appState.modal.showRefillWallettecInfo();
        } else {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-refill-created')
            });

            this.appState.billingHistory.loadHistory();
        }
    }

    processInvoiceData(invoiceData: { [k: string]: string }): WallettecInvoiceHTML[] | null {
        const result: WallettecInvoiceHTML[] = [];

        for (let key in invoiceData) {
            if (key !== WallettecInvoiceData.MULTITIER) {
                let value = invoiceData[key];
                if (value) {
                    result.push({title: convertCamelCase(key), body: invoiceData[key]});
                }
            }
        }

        return result.length ? result : null;
    }

    getGroupName(paywayId: string): string {
        return '';
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }
}