import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {redirectByPayway, sumbitPaywayForm} from '../utils';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export default class Hexopay extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const missingFormData = !response ||
            !response.hexopayForm ||
            !response.hexopayForm.action ||
            !response.hexopayForm.fields ||
            !response.hexopayForm.method;

        const url = response ? response.redirectUrl || response.fiatFormUrl : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (missingFormData && !(response!.redirectUrl)) {
            console.error('Unable to process hexopay response', response);
        } else if (url) {
            redirectByPayway(url, windowData);
        } else {
            sumbitPaywayForm(
                windowData,
                response!.hexopayForm!.action!,
                response!.hexopayForm!.method!,
                response!.hexopayForm!.fields!
            );
        }
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        const paywayId = payway.id ? payway.id.toLowerCase() : '';
        if (paywayId.startsWith('qiwi-')) {
            return 'payway-qiwi';
        } else if (paywayId.startsWith('skrill-')) {
            return 'payway-skrill';
        } else if (paywayId.startsWith('neteller-')) {
            return 'payway-neteller';
        } else if (paywayId.startsWith('upay-safe-')) {
            return 'payway-u-pay-safe';
        } else if (paywayId.startsWith('cash-to-code')) {
            return 'payway-cash-to-code';
        } else if (paywayId.startsWith('hexopay-card-')) {
            return 'payway-hexopay-card';
        } else {
            return super.getPaywayName(payway, appState);
        }
    }
}