import BaseProvider from "./BaseProvider";
import Payway from "../Payway";
import {default as PaywayForm, PaywayFormData} from "../forms/PaywayForm";
import {NWindowData} from "../RefillStore";
import {wallet} from "../../../api/proto";
//import {redirectByPayway} from "../utils";
import {BaseStore} from '../BaseStore';
import _PaymentIQCashier, { IPiqCashierConfig, IPiqCashierApiMethods } from 'paymentiq-cashier-bootstrapper'
import {action, observable} from 'mobx';
import WithdrawalStore from '../WithdrawalStore';

export default class Devcode extends BaseProvider {

    @observable isLoading: boolean = false;

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (response?.ok && payway) {
            payway.showFields = false;
            this.devCodeIframeShow(paywayForm, payway);
        }
    }

    onRefillSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
        payway.showFields = true;
        this.isLoading = false;
    }

    onWithdrawalSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
        payway.showFields = true;
        this.isLoading = false;
    }

    onSuccessWithdrawalResponse(store: WithdrawalStore, payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, response: wallet.IWithdrawResponse | null) {
        if (response?.ok && payway) {
            payway.showFields = false;
            this.devCodeIframeShow(paywayForm, payway);
        }
    }

    @action
    devCodeIframeShow(paywayForm: PaywayForm, payway: Payway) {
        this.isLoading = true;
        setTimeout(() => {
          if (document.getElementById('iframe-block') && payway.config) {
              const config: IPiqCashierConfig = JSON.parse(payway.config);
              console.log(config)
              const css = payway.css || "";
              new _PaymentIQCashier('#iframe-block',
                  config,
                  (api: IPiqCashierApiMethods) => {
                       // register callbacks
                       api.on({
                          cashierInitLoad: () => {
                              this.isLoading = false;
                          },
                          success: data => console.log('Successful transaction completed', data)
                       });
                       api.css(`${css}`);
                  }
              )
          }
        }, 0);
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }

    iframeBlockEnabled() {
        return true;
    }

    showSuccessWithdrawNotification() {
        return false;
    }
}
