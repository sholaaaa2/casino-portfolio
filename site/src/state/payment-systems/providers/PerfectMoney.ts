import {wallet} from '../../../api/proto';
import BaseProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {mapToFormFields, sumbitPaywayForm} from '../utils';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export default class PerfectMoney extends BaseProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const url = response ? response.fiatFormUrl : null;
        const method = response && response.fiatFormMethod ? response.fiatFormMethod : 'get';
        const formData = response ? response.fiatForm : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!url || !formData) {
            console.error('Missing perfectmoney required param(s) in response:  ', response);
        } else {
            sumbitPaywayForm(windowData, url, method, mapToFormFields(formData));
        }
    }
}