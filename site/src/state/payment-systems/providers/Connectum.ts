import PSProvider from './BaseProvider';
import {wallet} from '../../../api/proto';
import AppState from '../../AppState';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {PaywayLogo, redirectByPayway} from '../utils';
import Payway from '../Payway';

export default class Connectum extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const url = response ? response.redirectUrl : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!url) {
            console.error('Missing Connectum required param in response:  ', response);
        } else {
            redirectByPayway(url, windowData);
        }
    }

    getPaywayLogo(payway: wallet.IPayway, appState: AppState): string {
        return PaywayLogo.VISA_MC;
    }

    disablePaywayAfterSuccess(): boolean {
        return true;
    }
}