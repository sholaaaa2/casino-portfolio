import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {formatCurrencyCode} from '../../utils/Money';
import {AccountPlaceholder, PaywayLogo} from '../utils';
import {NWindowData} from '../RefillStore';
import {PaywayFieldType} from '../fields/PaywayField';
import Payway from '../Payway';
import {NotificationItemLevel} from '../../components/Notificator';

export type CoinPaymentsFiatForm = {
    amount1: string,
    amount2: string,
    currency1: string,
    currency2: string,
    qrCodeUrl: string,
    walletAddress: string,
};

export default class CoinPayments extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (response && response.fiatForm && Object.keys(response.fiatForm).length) {
            this.appState.modal.coinPaymentsFiatFormData = this.processInvoiceData(response.fiatForm);
            this.appState.modal.showCoinPaymentsCryptoForm();
        } else {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-refill-created')
            });

            this.appState.billingHistory.loadHistory();
        };
    }

    processInvoiceData(fiatForm: { [k: string]: string }): CoinPaymentsFiatForm | null {
        const result: CoinPaymentsFiatForm = {
            amount1: fiatForm.amount1,
            amount2: fiatForm.amount2,
            currency1: fiatForm.currency1,
            currency2: fiatForm.currency2,
            qrCodeUrl: fiatForm.qr_code_url,
            walletAddress: fiatForm.wallet_address,
        };

        return result;
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        const lang = appState.language!;
        const defaultLang = appState.siteConfig.defaultLanguage!;
        const paywayName = payway.name![lang] || payway.name![defaultLang] || payway.name!['en'];
        if (paywayName) {
            return paywayName;
        } else {
            return formatCurrencyCode(appState.userInfo.balance.currencyId);
        }
    }

    getPaywayLogo(payway: wallet.IPayway, appState: AppState): string {
        const lang = appState.language!;
        const defaultLang = appState.siteConfig.defaultLanguage!;
        const logo = payway.logoUrl ? (payway.logoUrl[lang] || payway.logoUrl[defaultLang] || payway.logoUrl['en']) : null;
        if (logo) {
            return logo;
        } else {
            const currency = appState.user.getRealBalanceCurrency();
            if (currency && currency.code) {
                return '/images/payments/crypto/' + currency.code.toUpperCase() + '.png';
            } else {
                console.error('Unable to get image for payway: ', payway);
                return PaywayLogo.DEFAULT;
            }
        }
    }

    getPaywayFieldTitle(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        if (field.id === PaywayFieldType.ACCOUNT) {
            return 'ui-wallet-address';
        } else {
            return super.getPaywayFieldTitle(field, payway, appState);
        }
    }

    getPaywayFieldPlaceholder(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        if (field.id === PaywayFieldType.ACCOUNT) {
            return AccountPlaceholder.CASHLIB_WALLET;
        } else {
            return super.getPaywayFieldPlaceholder(field, payway, appState);
        }
    }
}
