import BaseProvider from "./BaseProvider";
import Payway from "../Payway";
import PaywayForm, {PaywayFormData} from "../forms/PaywayForm";
import {WindowData} from "../RefillStore";
import {wallet} from "../../../api/proto";
import {NotificationItemLevel} from "../../components/Notificator";

export default class Muchbetter extends BaseProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: WindowData | null, response: wallet.IRefillResponse | null) {
        const isOk = response ? response.ok || false : false;
        if (isOk) {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-refill-request-sent-to-phone')
            });
        } else {
            console.error('Missing muchbetter required param in response:  ', response);
        }
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }

}