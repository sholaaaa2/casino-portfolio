import BaseProvider from "./BaseProvider";
import Payway from "../Payway";
import {default as PaywayForm, PaywayFormData} from "../forms/PaywayForm";
import {NWindowData} from "../RefillStore";
import {wallet} from "../../../api/proto";
import {redirectByPayway} from "../utils";

export default class Simpay extends BaseProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const url = response ? response.redirectUrl : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!url) {
            console.error('Missing simpay required param in response:  ', response);
        } else {
            redirectByPayway(url, windowData);
        }
    }
}