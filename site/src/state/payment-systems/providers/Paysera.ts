import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {redirectByPayway} from '../utils';
import Payway from '../Payway';
import {NString} from '../../meta';

export default class Paysera extends PSProvider {

    static processBillingSettings(payseraVerification: NString) {
        if (payseraVerification) {
            let meta = document.getElementById('verify-paysera');
            if (!meta) {
                meta = document.createElement('meta');
                meta.setAttribute('id', 'verify-paysera');
                meta.setAttribute('name', 'verify-paysera');
                document.head.appendChild(meta);
            }

            if (meta.getAttribute('content') !== payseraVerification) {
                meta.setAttribute('content', payseraVerification);
            }
        }
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!response || !response.redirectUrl) {
            console.error('Missing paysera required param in response:  ', response);
        } else {
            redirectByPayway(response.redirectUrl, windowData);
        }
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        const paywayId = payway.id ? payway.id.toLowerCase() : '';
        switch (paywayId) {
            case 'worldhand':
                return 'payway-worldhand';
            case 'webmoney':
                return 'payway-webmoney';
            case 'safetypay':
                return 'payway-safetypay';
            default:
                return super.getPaywayName(payway, appState);
        }
    }
}