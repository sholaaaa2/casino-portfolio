import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {AccountPlaceholder, mapToFormFields, PaywayLogo, sumbitPaywayForm} from '../utils';
import {NotificationItemLevel} from '../../components/Notificator';
import {NWindowData} from '../RefillStore';
import {PaywayFieldType} from '../fields/PaywayField';
import Payway from '../Payway';

export default class Cashlib extends PSProvider {

    refillType?: wallet.RefillProvidersResponse.RefillType | null | undefined;

    constructor(provider: wallet.WithdrawProvider, refillType: wallet.RefillProvidersResponse.RefillType | null | undefined, appState: AppState) {
        super(provider, appState);
        this.refillType = refillType;
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (this.refillType === wallet.RefillProvidersResponse.RefillType.direct) {
            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-refill-created')
            });
            this.appState.billingHistory.loadHistory();
        } else {
            if (response) {
                const url = response.fiatFormUrl || response.iframeUrl;
                const formData = response.fiatForm;
                if (!windowData) {
                    console.error('Unable to open new window');
                } else if (!url || !formData) {
                    console.error('Missing Cashlib required param in response: ', response);
                } else {
                    sumbitPaywayForm(windowData, url, 'get', mapToFormFields(formData));
                }
            } else {
                console.error('Unable to process empty response');
            }
        }
    }

    willRedirectToPaymentSystemPage(): boolean {
        return this.refillType === wallet.RefillProvidersResponse.RefillType.iframe;
    }

    getPaywayLogo(payway: wallet.IPayway, appState: AppState): string {
        return PaywayLogo.CASHLIB;
    }

    getPaywayFieldPlaceholder(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        if (field.id === PaywayFieldType.ACCOUNT) {
            return AccountPlaceholder.CASHLIB_WALLET;
        } else {
            return super.getPaywayFieldPlaceholder(field, payway, appState);
        }
    }
}