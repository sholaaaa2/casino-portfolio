import {messages, wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {mapToFormFields, redirectByPayway, sumbitPaywayForm} from '../utils';
import Payway from '../Payway';
import {getRefillError} from '../../../api/utils';
import {action, computed, observable} from 'mobx';
import Long from 'long';
import {PaywayFieldType} from '../fields/PaywayField';
import PhoneField from '../fields/PhoneField';
import parsePhoneNumberFromString, {CountryCode} from 'libphonenumber-js';

export class CashApp extends PSProvider {
    @observable isAdditionalInfoSending = false;

    @computed
    get isCashappPhoneConfirmed() {
        const confirmedPhone = this.appState.refillStore.selectedPayway?.fields.find(
            (_) => _.id === 'cashapp-phone-confirmed',
        );
        const phoneField = this.appState.refillStore.selectedPayway?.fields.find(
            (_) => _.type === PaywayFieldType.PHONE,
        ) as PhoneField | undefined;
        const parsedConfirmedPhone = parsePhoneNumberFromString(confirmedPhone?.value ?? '');
        const parsedPhone = parsePhoneNumberFromString(
            phoneField?.phoneNumber ?? '',
            phoneField?.countryCode as CountryCode,
        );
        const parsedUserPhone = parsePhoneNumberFromString(this.appState.rawUserInfo?.phone ?? '');
        if (!!parsedPhone && (!!parsedConfirmedPhone || !!parsedUserPhone)) {
            return parsedConfirmedPhone?.isEqual(parsedPhone) || parsedUserPhone?.isEqual(parsedPhone);
        }
        return false;
    }

    onSuccessRefillResponse(
        payway: Payway,
        paywayFormData: PaywayFormData,
        paywayForm: PaywayForm,
        windowData: NWindowData,
        response: wallet.IRefillResponse | null,
    ) {
        if (!windowData || !response) {
            console.error('Unable to open new window or null response: ', windowData, response);
        } else if (response.redirectUrl) {
            redirectByPayway(response.redirectUrl, windowData);
        } else if (response.fiatFormUrl) {
            const method = response?.fiatFormMethod ?? 'get';
            sumbitPaywayForm(
                windowData,
                response.fiatFormUrl,
                method,
                mapToFormFields(response.fiatForm),
            );
        } else {
            console.error('Unimplemented method onSuccessRefillResponse');
        }
        window.location.reload();
    }

    @action.bound
    saveAdditionalInfo() {
        const payway = this.appState.refillStore.selectedPayway;
        if (payway) {
            this.isAdditionalInfoSending = true;
            let resultFields = {};
            const phoneField = this.appState.refillStore.selectedPayway?.fields.find(
                (_) => _.type === PaywayFieldType.PHONE,
            );
            const confirmField = this.appState.refillStore.selectedPayway?.fields.find(
                (_) => _.type === PaywayFieldType.BOOLEAN,
            );
            const isShouldConfirmPhone = confirmField?.value === 'true';
            let phone = '';
            let country = '';
            if (phoneField) {
                phoneField.setError(null);
                phoneField.fillInFieldsOrSetError(this.appState.refillStore);
                resultFields = {...resultFields, ...this.appState.refillStore.paywayForm.fields};
                phone = resultFields['cashapp-phone-with-code'];
                country = resultFields['cashapp-phone-country-code'];
            }
            const balanceId = this.appState.user.getRealBalanceId();
            this.appState.api.refillByPayway(
                payway,
                payway.getId(),
                this.type,
                undefined,
                balanceId ?? Long.ZERO,
                null,
                {...resultFields, 'cashapp-save': '1'},
                this.processSaveAdditionalInfoResult.bind(
                    this,
                    country,
                    phone,
                    isShouldConfirmPhone && !this.isCashappPhoneConfirmed,
                ),
            );
        }
    }

    @action.bound
    processSaveAdditionalInfoResult(
        country: string,
        phone: string,
        isShouldConfirmPhone: boolean,
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) {
        const cashAppTagField = this.appState.refillStore.selectedPayway?.fields.find((_) => _.id === 'cashapp-tag');
        if (act.refill?.error) {
            const error = this.appState.t(getRefillError(act.refill.error, this.type));
            if (act.refill.error === wallet.RefillResponse.RefillError.invalid_cashtag) {
                cashAppTagField?.setError(error);
            } else {
                this.appState.refillStore.selectedPayway?.fields
                    .find((_) => _.type === PaywayFieldType.PHONE)
                    ?.setError(error);
            }
        } else {
            if (cashAppTagField) {
                cashAppTagField.defaultValue = cashAppTagField.value;
            }
            if (!!country && !!phone && isShouldConfirmPhone) {
                this.appState.refillStore.paywayForm.fields['confirm_phone'] = 'true';
                this.appState.confirmCashappPhone.init(country, phone);
            }
        }
        this.isAdditionalInfoSending = false;
    }
}
