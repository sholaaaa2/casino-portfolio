import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {mapToFormFields, sumbitPaywayForm} from '../utils';
import Payway from '../Payway';

export default class Payzen extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!response || !response.invoiceData) {
            console.error('Unable to process payzen refill response');
        } else if (!windowData) {
            console.error('Unable to open new window');
        } else if (!response || !response.redirectUrl) {
            console.error('Missing payzen required param in response:  ', response);
        } else {
            const formData = response.invoiceData;
            sumbitPaywayForm(windowData, response.redirectUrl, 'post', mapToFormFields(formData));
        }
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        const paywayId = payway.id ? payway.id.toLowerCase() : '';
        switch (paywayId) {
            case 'payzen-onlinebanking':
                return 'payway-payzen-onlinebanking';
            case 'payzen-quickpay':
                return 'payway-payzen-quickpay';
            case 'payzen-qrcode':
                return 'payway-payzen-qrcode';
            case 'payzen-p2p':
            case 'payzen-p2p-direct':
            case 'payzen-banktransfer':
                return 'payway-payzen-p2p';
            default:
                return super.getPaywayName(payway, appState);
        }
    }
}