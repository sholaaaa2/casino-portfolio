import BaseProvider from "./BaseProvider";
import Payway from "../Payway";
import {default as PaywayForm, PaywayFormData} from "../forms/PaywayForm";
import {NWindowData} from "../RefillStore";
import {wallet} from "../../../api/proto";
import {redirectByPayway} from "../utils";
import {BaseStore} from '../BaseStore';

export default class SwiftPayP2P extends BaseProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const url = response ? response.redirectUrl : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!url) {
            console.error('Missing simpay required param in response:  ', response);
        } else {
            redirectByPayway(url, windowData);
        }
    }

    onRefillSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
        payway.showFields = false;
    }

    willRedirectToPaymentSystemPage(): boolean {
        return true;
    }

    isSwiftPayP2P() {
        return true;
    }
}
