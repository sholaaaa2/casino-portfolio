import {users, wallet} from '../../../api/proto';
import AppState from '../../AppState';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {mapToFormFields, PaywayLogo, redirectByPayway, sumbitPaywayForm} from '../utils';
import {NWindowData} from '../RefillStore';
import {BaseStore} from '../BaseStore';
import Payway from '../Payway';
import WithdrawalStore from '../WithdrawalStore';
import {SimpleNotification} from "../../components/Notificator";

export default class PSProvider {
    appState: AppState;
    type: wallet.WithdrawProvider;

    constructor(type: wallet.WithdrawProvider, appState: AppState) {
        this.type = type;
        this.appState = appState;
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!windowData || !response) {
            console.error('Unable to open new window or null response: ', windowData, response);
        } else if (response.redirectUrl) {
            redirectByPayway(response.redirectUrl, windowData);
        } else if (response.fiatFormUrl) {
            const method = response?.fiatFormMethod ?? 'get';
            sumbitPaywayForm(windowData, response.fiatFormUrl, method, mapToFormFields(response.fiatForm));
        } else {
            console.error('Unimplemented method onSuccessRefillResponse');
        }
    }

    onSuccessWithdrawalResponse(store: WithdrawalStore, payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, response: wallet.IWithdrawResponse | null) {
        // withdrawal success handler is used only for some of the payment systems
        // print nothing if unimplemented
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        const paywayId = payway.id ? payway.id.toLowerCase() : '';
        if (paywayId.startsWith('card_privat_') || paywayId.startsWith('privat24_')) {
            return 'payway-privat-24';
        } else if (paywayId.startsWith('alfaclick_')) {
            return 'payway-alfa-click';
        } else {
            const lang = appState.language!;
            const defaultLang = appState.siteConfig.defaultLanguage!;
            return payway.name![lang] || payway.name![defaultLang] || payway.name!['en'] || payway.id!;
        }
    }

    getGroupName(paywayId: string): string {
        return '';
    }

    getPaywayLogo(payway: wallet.IPayway, appState: AppState): string {
        const lang = appState.language!;
        const defaultLang = appState.siteConfig.defaultLanguage!;
        const logo = payway.logoUrl ? (payway.logoUrl[lang] || payway.logoUrl[defaultLang] || payway.logoUrl['en']) : null;
        return logo || this.getPaywayLogoById(payway) || PaywayLogo.DEFAULT;
    }

    getPaywayLogoById(payway: wallet.IPayway): string | null {
        const paywayId = payway.id ? payway.id.toLowerCase() : '';
        if (paywayId.startsWith('card_privat_') || paywayId.startsWith('privat24_') || paywayId.endsWith('privat_uah')) {
            return PaywayLogo.PRIVATE_BANK;
        } else if (paywayId.startsWith('webmoney_')) {
            return PaywayLogo.WEBMONEY;
        } else if (paywayId.startsWith('yamoney_') || paywayId.startsWith('yandexmoney_')) {
            return PaywayLogo.YANDEX_MONEY;
        } else if (paywayId.startsWith('payeer_')) {
            return PaywayLogo.PAYEER;
        } else if (paywayId.startsWith('qiwi_') || paywayId.startsWith('qiwi-')) {
            return PaywayLogo.QIWI;
        } else if (paywayId.startsWith('mts_')) {
            return PaywayLogo.MTS;
        } else if (paywayId.startsWith('tele2_')) {
            return PaywayLogo.TELE2;
        } else if (paywayId.startsWith('megafon_') || paywayId.startsWith('megafone_')) {
            return PaywayLogo.MEGAFON;
        } else if (paywayId.startsWith('beeline_')) {
            return PaywayLogo.BEELINE;
        } else if (paywayId.startsWith('advcash_')) {
            return PaywayLogo.ADVCASH;
        } else if (paywayId.startsWith('wex_')) {
            return PaywayLogo.WEX;
        } else if (paywayId.startsWith('piastrix_')) {
            return PaywayLogo.PIASTRIX;
        } else if (paywayId.startsWith('terminal_')) {
            return PaywayLogo.TERMINALS_RF;
        } else if (paywayId.startsWith('alfaclick_')) {
            return PaywayLogo.ALFA_BANK;
        } else if (paywayId.startsWith('perfectmoney_')) {
            return PaywayLogo.PERFECT_MONEY;
        } else if (paywayId.startsWith('nixmoney_')) {
            return PaywayLogo.NIXMONEY;
        } else if (paywayId.startsWith('worldterminal_')) {
            return PaywayLogo.WORLD_TERMINAL;
        } else if (paywayId.startsWith('ukrterminal_')) {
            return PaywayLogo.UKR_TERMINAL;
        } else if (paywayId.startsWith('svyaznoy_')) {
            return PaywayLogo.SVYAZNOY;
        } else if (paywayId.startsWith('exmo_')) {
            return PaywayLogo.EXMO;
        } else if (paywayId.startsWith('visa_')) {
            return PaywayLogo.VISA;
        } else if (paywayId.startsWith('cash_yandexmoney_')) {
            return PaywayLogo.YANDEX_MONEY_CASH;
        } else if (paywayId.startsWith('psbretail_')) {
            return PaywayLogo.PSB_RETAIL;
        } else if (paywayId.startsWith('mastercard_')) {
            return PaywayLogo.MASTERCARD;
        } else if (paywayId.startsWith('euroset_')) {
            return PaywayLogo.EUROSET;
        } else if (paywayId.startsWith('qiwiterminal_')) {
            return PaywayLogo.QIWI_TERMINAL;
        } else if (paywayId.startsWith('w1_')) {
            return PaywayLogo.WALLET_ONE;
        } else if (paywayId.startsWith('interkassa_')) {
            return PaywayLogo.INTERKASSA;
        } else if (paywayId.startsWith('mir_')) {
            return PaywayLogo.MIR;
        } else if (paywayId.startsWith('card_')) {
            return PaywayLogo.VISA_MC;
        } else if (paywayId.startsWith('skrill-')) {
            return PaywayLogo.SKRILL;
        } else if (paywayId.startsWith('neteller-')) {
            return PaywayLogo.NETELLER;
        } else if (paywayId.startsWith('upay-safe-')) {
            return PaywayLogo.PAY_SAFE;
        } else if (paywayId.startsWith('cash-to-code')) {
            return PaywayLogo.CASH_TO_CODE;
        } else if (paywayId.startsWith('hexopay-card-')) {
            return PaywayLogo.HEXAPAY;
        } else if (paywayId.startsWith('mtn-')) {
            return PaywayLogo.MTN;
        } else if (paywayId.startsWith('airtel-')) {
            return PaywayLogo.AIRTEL;
        } else if (paywayId.startsWith('tigo-')) {
            return PaywayLogo.TIGO;
        } else if (paywayId.startsWith('m-')) {
            return PaywayLogo.MPESA;
        } else if (paywayId.startsWith('vodacom-')) {
            return PaywayLogo.VODACOM;
        } else {
            return null;
        }
    }

    getPaywayFieldTitle(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        const lang = appState.language!;
        const defaultLang = appState.siteConfig.defaultLanguage!;
        return field.title ? (field.title[lang] || field.title[defaultLang] || field.title['en'] || '') : '';
    }

    getPaywayFieldPlaceholder(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        return field.placeholder || '';
    }

    willRedirectToPaymentSystemPage(): boolean {
        return true;
    }

    needBackendCall(): boolean {
        return true;
    }

    disablePaywayAfterSuccess(): boolean {
        return false;
    }

    isPromoCodeFormEnabled(): boolean {
        return true;
    }

    onRefillSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
    }
    onWithdrawalSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
    }

    canProcessWithdrawalNotification(notification: users.IWithdrawalBillingNotification) {
        return false;
    }

    processWithdrawalNotification(notification: users.IWithdrawalBillingNotification, formattedAmount: String, formattedCurrency: String): SimpleNotification | null {
        return null;
    }

    iframeBlockEnabled() {
        return false;
    }

    showSuccessWithdrawNotification() {
        return true;
    }
}
