import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {AccountPlaceholder, mapToFormFields, sumbitPaywayForm} from '../utils';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export default class Piastrix extends PSProvider {

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        const url = response ? response.fiatFormUrl : null;
        const formData = response ? response.fiatForm : null;

        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!url || !formData) {
            console.error('Missing piastrix required param in response:  ', response);
        } else {
            sumbitPaywayForm(windowData, url, 'get', mapToFormFields(formData));
        }
    }

    getPaywayName(payway: wallet.IPayway, appState: AppState): string {
        return super.getPaywayName(payway, appState);
    }

    getPaywayFieldTitle(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState) {
        if (field.id !== 'payeer_account') {
            return 'ui-refill-account';
        } else {
            return super.getPaywayFieldPlaceholder(field, payway, appState);
        }
    }

    getPaywayFieldPlaceholder(field: wallet.IPaywayField, payway: wallet.IPayway, appState: AppState): string {
        if (field.id !== 'amount') {
            const paywayId = payway.id ? payway.id.toLowerCase() : '';
            if (paywayId.startsWith('card_privat_') || paywayId.startsWith('privat24_') || paywayId.startsWith('card_')) {
                return AccountPlaceholder.CARD_NUMBER;
            } else if (paywayId.startsWith('webmoney_')) {
                return AccountPlaceholder.WEBMONEY_WALLET;
            } else if (paywayId.startsWith('yamoney_')) {
                return AccountPlaceholder.YANDEX_MONEY_ACCOUNT;
            } else if (paywayId.startsWith('payeer_')) {
                return AccountPlaceholder.PAYEER_WALLET;
            } else if (paywayId.startsWith('qiwi_')) {
                return AccountPlaceholder.QIWI_WALLET;
            } else if (paywayId.startsWith('mts_') || paywayId.startsWith('tele2_') || paywayId.startsWith('megafon_') || paywayId.startsWith('beeline_')) {
                return AccountPlaceholder.RU_PHONE_PLACEHOLDER;
            } else if (paywayId.startsWith('piastrix_')) {
                return AccountPlaceholder.PIASTRIX_WALLET;
            } else {
                return super.getPaywayFieldPlaceholder(field, payway, appState);
            }

        } else {
            return super.getPaywayFieldPlaceholder(field, payway, appState);
        }
    }
}