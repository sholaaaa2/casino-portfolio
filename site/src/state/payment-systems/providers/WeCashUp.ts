import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {PaywayLogo} from '../utils';
import {NWindowData} from '../RefillStore';
import Payway from '../Payway';

export type WecashupModalData = {
    phone: string;
    amount: string;
    currencyCode: string;
};

export default class WeCashUp extends PSProvider {
    settings: wallet.RefillProvidersResponse.Provider.IWecashupSettings | null | undefined;

    constructor(provider: wallet.WithdrawProvider, appState: AppState, settings: wallet.RefillProvidersResponse.Provider.IWecashupSettings | null | undefined) {
        super(provider, appState);
        this.settings = settings;
        this.onWecashupBtnClick = this.onWecashupBtnClick.bind(this);
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (this.settings) {
            this.appState.modal.showPaywayWecashupRefillForm();
            this.appState.modal.wecashupModalData = {
                phone: paywayFormData.phone!,
                amount: paywayFormData.sourceAmount!,
                currencyCode: this.appState.userInfo.realBalance!.currencyCode
            };
            setTimeout(this.onModalDialogRendered.bind(this, paywayFormData), 100);
        } else {
            console.error('Unable to proceed, missing IWecashupSettings field');
        }
    }

    onModalDialogRendered(paywayFormData: PaywayFormData) {
        const appState = this.appState;
        const payway = appState.refillStore.selectedPayway;
        const refillWecashupFormContainer = appState.getRef('refillWecashupFormContainer') as HTMLElement;

        if (refillWecashupFormContainer && payway) {
            const wecashup = payway.getProvider()  as WeCashUp;
            const wecashupSettings = wecashup.settings!;
            const userInfo = appState.userInfo;
            const siteName = appState.site.siteName;
            const userId = userInfo.userId.toNumber().toString();
            const sessionId = userInfo.sid;
            const siteLogo = window.location.origin + appState.site.siteLogo;
            const currencyCode = appState.userInfo.realBalance!.currencyCode;

            const jqueryScript = document.createElement('script');
            jqueryScript.setAttribute('src', 'https://code.jquery.com/jquery-3.2.1.min.js');
            jqueryScript.setAttribute('type', 'text/javascript');
            jqueryScript.setAttribute('defer', 'true');

            const script = document.createElement('script');
            script.setAttribute('defer', 'true');
            script.setAttribute('src', 'https://www.wecashup.com/library/MobileMoney.js');
            script.setAttribute('class', 'wecashup_button');
            script.setAttribute('data-sender-phonenumber', paywayFormData.phone!);
            script.setAttribute('data-receiver-uid', wecashupSettings.merchantUid!);
            script.setAttribute('data-receiver-public-key', wecashupSettings.merchantPublicKey!);
            script.setAttribute('data-transaction-parent-uid', '');
            script.setAttribute('data-transaction-receiver-currency', currencyCode);
            script.setAttribute('data-transaction-receiver-total-amount', paywayFormData.sourceAmount!);
            script.setAttribute('data-transaction-receiver-reference',  sessionId);
            script.setAttribute('data-transaction-sender-reference', userId);
            script.setAttribute('data-transaction-method', 'pull');
            script.setAttribute('data-image', siteLogo);
            script.setAttribute('data-name', siteName);
            script.setAttribute('data-crypto', 'true');
            script.setAttribute('data-cash', 'true');
            script.setAttribute('data-telecom', 'true');
            script.setAttribute('data-m-wallet', 'true');
            script.setAttribute('data-split', 'true');
            script.setAttribute('configuration-id', '3');
            script.setAttribute('data-marketplace-mode', 'false');
            script.setAttribute('data-crypto', wecashupSettings.crypto!.toString());
            script.setAttribute('data-cash', wecashupSettings.cash!.toString());
            script.setAttribute('data-telecom', wecashupSettings.telecom!.toString());
            script.setAttribute('data-m-wallet', wecashupSettings.mWallet!.toString());
            script.setAttribute('data-split', wecashupSettings.split!.toString());

            const form = document.createElement('form');
            form.setAttribute('action', wecashupSettings.callbackUrl!);
            form.setAttribute('method', 'POST');
            form.setAttribute('id', 'wecashup');

            form.appendChild(jqueryScript);
            form.appendChild(script);
            refillWecashupFormContainer.appendChild(form);
        }
    }

    getPaywayLogo(payway: wallet.IPayway, appState: AppState): string {
        return PaywayLogo.WECASHUP;
    }

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }

    needBackendCall(): boolean {
        return false;
    }

    onWecashupBtnClick(e: React.MouseEvent<HTMLElement>) {
        const WCUpaymentButton = document.getElementById('WCUpaymentButton') as HTMLButtonElement;
        WCUpaymentButton.click();
    }
}