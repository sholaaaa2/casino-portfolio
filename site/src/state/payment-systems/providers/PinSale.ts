import {users, wallet} from "../../../api/proto";
import Payway from "../Payway";
import {default as PaywayForm, PaywayFormData} from "../forms/PaywayForm";
import {NWindowData} from "../RefillStore";
import BaseProvider from "./BaseProvider";
import {NotificationItemLevel, SimpleNotification} from "../../components/Notificator";

export default class PinSale extends BaseProvider {

    willRedirectToPaymentSystemPage(): boolean {
        return false;
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!response) {
            console.error('No response: ', response);
        } else if (response.ok) {
            this.appState.refillStore.resetPayway();
            this.appState.withdrawalStore.resetPayway();

            this.appState.refillStore.loadPayways(true);
            this.appState.withdrawalStore.loadPayways(true);

            this.appState.notificator!.addNotification({
                level: NotificationItemLevel.SUCCESS,
                message: this.appState.t('ui-refill-created')
            });
            this.appState.router.push(this.appState.l('/'));
        }
    }

    isPromoCodeFormEnabled(): boolean {
        return false;
    }

    canProcessWithdrawalNotification(notification: users.IWithdrawalBillingNotification): boolean {
        return notification.provider === 'pinsale' && notification.type === users.WithdrawalBillingNotification.NotificationType.SUCCESS;
    }

    processWithdrawalNotification(notification: users.IWithdrawalBillingNotification, formattedAmount: String, formattedCurrency: String): SimpleNotification | null {
        this.appState.billingHistory.loadHistory();
        return {
            level: NotificationItemLevel.SUCCESS,
            message: this.appState.t('ui-withdrawal-successful', {amount: formattedAmount, currency: formattedCurrency})
        };
    }
}