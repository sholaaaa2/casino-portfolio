import {messages, wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {BaseStore} from '../BaseStore';
import Payway from '../Payway';
import PaywayField, {PaywayFieldOption} from '../fields/PaywayField';
import {formatCurrencyAmount} from '../../utils/Money';
import {redirectByPayway} from '../utils';
import WithdrawalStore from '../WithdrawalStore';

const amountIdKey = 'id';

export default class Paygigaa extends PSProvider {

    onRefillSelectedPaywayChange(store: BaseStore, paywayForm: PaywayForm, payway: Payway) {
        const sessionField = payway.fields.find((f: PaywayField) => f.id === 'sessionId');
        const sessionId = sessionField ? sessionField.value : null;
        const bankId = payway.getId();
        if (sessionId && bankId) {
            store.isPaywaysLoading = true;
            this.appState.api.getAvailableAmounts(
                sessionId,
                bankId,
                this.onAvailableAmountsResponse.bind(this, store, paywayForm, payway)
            );
        } else {
            console.error('Unable to get available amounts, missing session or bank id: ', sessionId, bankId);
        }
    }

    onAvailableAmountsResponse(store: BaseStore, paywayForm: PaywayForm, payway: Payway, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        store.isPaywaysLoading = false;
        this.appState.processApi(msg, act);

        if (act.availableAmounts && act.availableAmounts.amounts && act.availableAmounts.amounts.length) {
            const idField = payway.fields.find((f: PaywayField) => f.id === amountIdKey);
            if (idField) {
                idField.options = act.availableAmounts.amounts.map(function (this: Paygigaa, amount: wallet.PaygigaAvailableAmountsResponse.IDepositAmount) {
                    const value = formatCurrencyAmount(amount.amount, amount.currency, false);
                    const count = amount.count ? amount.count.toString() : '0';
                    return {
                        key: amount.id ? amount.id.toString() : '',
                        value: value + ' x ' + count
                    };
                }, this);
            } else {
                console.error('Unable to find id field in payway fields list: ', payway);
            }
        } else {
            store.error = 'error-no-available-amounts';
        }
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!windowData) {
            console.error('Unable to open new window');
        } else if (!response || !response.redirectUrl) {
            console.error('Missing paygigaa required param in response:  ', response);
        } else {
            const id = paywayFormData.fields[amountIdKey];
            const idField = payway.fields.find((f: PaywayField) => f.id === amountIdKey);
            if (id && idField) {
                const optionIndex = idField.options ? idField.options.findIndex((f: PaywayFieldOption) => f.key === id) : -1;
                if (optionIndex !== -1) {
                    const result = idField.options!.slice();
                    result.splice(optionIndex, 1);
                    idField.options = result;
                    if (idField.options.length === 0) {
                        window.location.reload();
                    }
                }
            }
            redirectByPayway(response.redirectUrl, windowData);
        }
    }

    onSuccessWithdrawalResponse(store: WithdrawalStore, payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, response: wallet.IWithdrawResponse | null) {
        this.appState.api.getWithdrawalTypes(store.onPaywaysLoaded.bind(store));
        this.appState.router.push(this.appState.l('/'));
    }
}