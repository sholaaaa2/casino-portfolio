import AppState from '../../AppState';
import {wallet} from '../../../api/proto';
import PSProvider from './BaseProvider';
import {default as PaywayForm, PaywayFormData} from '../forms/PaywayForm';
import {NWindowData} from '../RefillStore';
import {mapToFormFields, sumbitPaywayForm} from '../utils';
import Payway from '../Payway';

export default class Interkassa extends PSProvider {
    settings: wallet.RefillProvidersResponse.Provider.IInterkassaSettings | null | undefined;

    constructor(provider: wallet.WithdrawProvider, appState: AppState, settings: wallet.RefillProvidersResponse.Provider.IInterkassaSettings | null | undefined) {
        super(provider, appState);
        this.settings = settings;
    }

    onSuccessRefillResponse(payway: Payway, paywayFormData: PaywayFormData, paywayForm: PaywayForm, windowData: NWindowData, response: wallet.IRefillResponse | null) {
        if (!response || !response.invoiceData) {
            console.error('Unable to process interkassa refill response');
        } else if (!windowData) {
            console.error('Unable to open new window');
        } else {
            const formData = response.invoiceData;
            sumbitPaywayForm(windowData, 'https://sci.interkassa.com/', 'post', mapToFormFields(formData));
        }
    }
}