import {action, computed, observable} from 'mobx';
import Payway from './Payway';
import AppState from '../AppState';
import * as React from 'react';
import {PaywayFormData, ApiErrorLinkType} from './forms/PaywayForm';
import PromoCodeForm from './forms/PromoCodeForm';
import {messages, wallet, users} from '../../api/proto';
import {BaseStore} from './BaseStore';
import {groupPaywaysByCategory, groupPaywaysByGroupId} from './utils';
import {getRefillError, isDef} from '../../api/utils';
import {v4 as uuid} from 'uuid';
import { SitePages } from '../meta';
import {isMobile} from "../utils/BrowserUtils";
import PaywayField, { PaywayFieldType } from './fields/PaywayField';
import BillingTilesGroup from './BillingTilesGroup';

const MobileDetect = require('mobile-detect');

export type WindowData = { instance: Window | null, id: string, redirect: Boolean };
export type NWindowData = WindowData | null;

export default class RefillStore extends BaseStore {

    @observable promoCodeForm: PromoCodeForm;

    constructor(appState: AppState) {
        super(appState);
        this.promoCodeForm = new PromoCodeForm(appState);

        this.loadPayways = this.loadPayways.bind(this);
        this.onFormAction = this.onFormAction.bind(this);
        this.onActivatePromoCodeClick = this.onActivatePromoCodeClick.bind(this);
        this.basePaywayUrl = '/refill';
    }

    // region ---- page state

    @computed
    get isActionLoading(): boolean {
        return this.promoCodeForm.isLoading || this.paywayForm.isLoading;
    }

    @computed
    get isDocumentsRequired(): boolean {
        if (this.selectedPayway?.provider?.type && this.appState.userInfo.providerRestrictions?.length) {
            const currentProvider = this.appState.userInfo.providerRestrictions
                .find((p) => p.provider && p.provider === this.selectedPayway?.provider.type);
            if (currentProvider) {
                const anyActionRestriction = currentProvider.restrictions?.indexOf(
                    users.UserInfo.UserRestrictions.DOCUMENTS_REQUIRED_FOR_ANY_ACTION
                );
                const refillRestriction = currentProvider.restrictions?.indexOf(
                    users.UserInfo.UserRestrictions.DOCUMENT_REQUIRED_FOR_REFILL
                );
                if (this.isDocumentsRequiredByGeneralRestrictions) {
                    return !(anyActionRestriction === -1 && refillRestriction === -1)
                } else {
                    return anyActionRestriction !== -1 || refillRestriction !== -1;
                }
            }
        }
        return this.isDocumentsRequiredByGeneralRestrictions;
    }

    @computed
    get isDocumentsRequiredByGeneralRestrictions() {
        return (
            this.appState.userInfo.restrictions.indexOf(
                users.UserInfo.UserRestrictions
                    .DOCUMENTS_REQUIRED_FOR_ANY_ACTION
            ) !== -1 ||
            this.appState.userInfo.restrictions.indexOf(
                users.UserInfo.UserRestrictions.DOCUMENT_REQUIRED_FOR_REFILL
            ) !== -1
        );
    }

    selectPaywayById(id: string | null | undefined) {
        super.selectPaywayById(id);

        if (this.selectedPayway) {
            this.promoCodeForm.reset(this.selectedPayway.getProvider().isPromoCodeFormEnabled());
        }
    }

    getFormActionToken(): string {
        return 'ui-refill-button';
    }

    getAmountLabelToken(): string {
        return 'ui-refill-amount';
    }

    reloadPayways() {
        if (this.appState.page === SitePages.REFILL) {
            this.loadPayways(true);
            this.appState.billingHistory.loadHistory();
        }
    }

    @computed
    get refillBanner() {
        let bannerUrl = undefined;
        if (!this.appState.appSettings.billing?.btcManualBanner) {
            return bannerUrl;
        }
        let lang = 'ru';
        if (this.appState.language && this.appState.language === 'ru') {
            lang = 'ru';
        }
        if (this.useTiles) {
            if (this.findTile('btcpayserver')) {
                bannerUrl = `/images/payments-system/banners/btcpayserver-banner_${lang}.png`;
            }
        } else {
            if (this.payways.find((payway) => payway.getId()?.search('btcpayserver') !== -1)) {
                bannerUrl = `/images/payments-system/banners/btcpayserver-banner_${lang}.png`;
            }
        }
        return bannerUrl;
    }

    // endregion

    // region ---- load payways
    @action
    loadPayways(forceLoad?: boolean) {
        if (!this.isPageDataLoaded() || forceLoad) {
            this.isPaywaysLoading = true;
            this.appState.api.getRefillProviders(this.onPaywaysLoaded.bind(this));
        }
    }

    onPaywaysLoaded(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isPaywaysLoading = false;
        this.appState.processApi(msg, act);

        const mobileDetect = new MobileDetect(window.navigator.userAgent);
        const isMobile = !!mobileDetect.mobile();
        const payways: Payway[] = [];

        if (act.refillProviders && act.refillProviders.providers) {
            for (let provider of act.refillProviders.providers) {
                if (isDef(provider.provider) && provider.payways) {
                    const providerPayways = isMobile ? provider.payways : provider.payways.filter((p: wallet.IPayway) => !p.mobileOnly);
                    for (let payway of providerPayways) {
                        if (payway.id && payway.fields && payway.fields.length) {
                            payways.push(new Payway(this.appState, provider, payway, this, true));
                        } else {
                            console.error('Unable to process payway with empty id or fields: ', payway);
                        }
                    }
                } else {
                    console.error('Unable to process refill provider: ', provider.provider, ' with payways: ', provider.payways);
                }
            }
        } else {
            console.error('Unable to process refill providers: ', act.refillProviders);
        }

        if (act.refillProviders) {
            this.useTiles = !!act.refillProviders.useTiles;
            this.tilesGroups = act.refillProviders.tiles ?
                act.refillProviders.tiles.map((t) => new BillingTilesGroup(
                    this.appState,
                    t,
                    this.basePaywayUrl,
                    payways,
                )) : [];
        }

        this.payways = payways;
        this.paywaysByGroup = groupPaywaysByGroupId(this.appState, payways, this.basePaywayUrl);
        this.paywaysByCategory = groupPaywaysByCategory(this.paywaysByGroup);

        this.tryPreselectPayway();
    }

    // endregion

    // region ---- UI event handlers
    @action
    resetPayway() {
        super.resetPayway();
        this.promoCodeForm.reset();
        this.cryptoAddressForm.reset();
    }

    @action
    tryPreselectPayway() {
        if (!this.selectedPayway) {
            this.selectPaywayByUrl();
        }
    }

    onSelectedPaywayChange(payway: Payway) {
        payway.getProvider().onRefillSelectedPaywayChange(this, this.paywayForm, payway);
        const cryptoField =
            payway.fields.find((p: PaywayField) => p.type === PaywayFieldType.CRYPTO_ADDRESS) ??
            payway.fields.find(
                (p: PaywayField) => p.type === PaywayFieldType.CRYPTO_ADDRESS_WITHOUT_QR,
            );
        if (cryptoField) {
            if (cryptoField.value) {
                this.cryptoAddressForm.updateCryptoAddress(cryptoField.value);
            } else {
                this.appState.refillStore.tryLoadCryptoAddress();
            }
        }
    }

    @action
    tryLoadCryptoAddress() {
        if (this.hasCryptoAddressWallet() && !this.cryptoAddressForm.address) {
            this.cryptoAddressForm.loadCryptoAddress();
        }
    }

    @action
    onFormAction(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        e.stopPropagation();
        if (this.selectedPayway?.isFinalStep) {
            this.tryRefill();
        } else if (
            this.selectedPayway?.isMultiStep &&
            !this.selectedPayway.isGoToNextStepButtonDisabled
        ) {
            this.selectedPayway.goToNextStep();
        }
    }

    onActivatePromoCodeClick(event: React.SyntheticEvent<HTMLElement>) {
        event.preventDefault();
        event.stopPropagation();
        this.promoCodeForm.tryActivatePromoCode();
    }

    // endregion

    // ---- refill
    tryRefill() {
        const appState = this.appState;
        const payway = this.selectedPayway;

        if (payway) {
            const provider = payway.getProvider();
            const paywayForm = this.paywayForm;
            paywayForm.clearErrors(payway);

            const paywayFormData = paywayForm.getFormDataOrSetError(payway, this);

            if (paywayFormData) {
                const promoCodeForm = this.promoCodeForm;
                const promoCode = promoCodeForm.getValue();

                let windowData: NWindowData = null;
                if (payway.getProvider().willRedirectToPaymentSystemPage()) {
                    windowData = this.openRefillWindowOrShowError();
                    if (!windowData) {
                        return;
                    }
                }

                paywayForm.isLoading = true;

                if (promoCode) {
                    promoCodeForm.tryActivatePromoCode((result: boolean) => {
                        if (result) {
                            if (provider.needBackendCall()) {
                                appState.api.refillByPayway(
                                    payway,
                                    payway.getId(),
                                    payway.getProvider().type,
                                    paywayFormData.amount,
                                    paywayFormData.balanceId,
                                    paywayFormData.vaucher,
                                    paywayFormData.fields,
                                    this.onTryRefill.bind(this, paywayFormData, payway, windowData)
                                );
                            } else {
                                payway.getProvider().onSuccessRefillResponse(payway, paywayFormData, paywayForm, windowData, null);
                            }
                        } else {
                            paywayForm.isLoading = false;
                        }
                    });
                } else {
                    if (provider.needBackendCall()) {
                        appState.api.refillByPayway(
                            payway,
                            payway.getId(),
                            payway.getProvider().type,
                            paywayFormData.amount,
                            paywayFormData.balanceId,
                            paywayFormData.vaucher,
                            paywayFormData.fields,
                            this.onTryRefill.bind(this, paywayFormData, payway, windowData)
                        );
                    } else {
                        payway.getProvider().onSuccessRefillResponse(payway, paywayFormData, paywayForm, windowData, null);
                    }
                }
            }
        }
    }

    onTryRefill(paywayFormData: PaywayFormData, payway: Payway, windowData: NWindowData, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        const paywayForm = this.paywayForm;
        paywayForm.isLoading = false;

        if (act.refill && act.refill.ok) {
            paywayForm.reset(payway);
            this.appState.billingHistory.loadHistory();
            payway.getProvider().onSuccessRefillResponse(payway, paywayFormData, paywayForm, windowData, act.refill);
        } else {
            if (windowData && windowData.instance) {
                windowData.instance!.close();
                window.focus();
            }
            const error = act.refill ? act.refill.error : null;
            paywayForm.apiError = this.appState.t(getRefillError(error, payway.getProvider().type));
            paywayForm.apiErrorLinkType = error === wallet.RefillResponse.RefillError.blocked_by_the_terms_of_bonus
              ? ApiErrorLinkType.BONUSES
              : ApiErrorLinkType.SUPPORT
        }
    }

    openRefillWindowOrShowError(): NWindowData | null {
        const id = uuid();
        const redirect = this.appState.appSettings.billing != null && this.appState.appSettings.billing.redirectMobile === true &&
            isMobile();
        if (redirect) {
            return {instance: null, id: id, redirect: true};
        } else {
            const instance = window.open('', id);
            const refillForm = this.paywayForm;

            if (instance) {
                setTimeout(() => instance.blur(), 0);
                instance.document.write(this.appState.t('ui-refill-redirect-info'));
                return {instance: instance, id: id, redirect: false};
            } else {
                refillForm.apiError = this.appState.t('error-refill-open-window');
                refillForm.apiErrorLinkType = ApiErrorLinkType.SUPPORT;
                return null;
            }
        }
    }

    // endregion

    get providersCustomizations() {
        return this.appState.appSettings.billing?.refillProvidersCustomizations || null;
    }
}
