import {bonuses, messages} from '../../../api/proto';
import {getBonusActivationErrorToken} from '../../../api/utils';
import {action, computed, observable, runInAction} from 'mobx';
import AppState from '../../AppState';
import {clearInputValue, getInputValueWithoutSpaces} from '../../utils/BrowserUtils';
import {createRef} from 'react';
import {Modal} from '../../meta';
import {pasteFromClipboard} from '../../utils/pasteFromClipboard';

const PROMO_CODE_PLACEHOLDER = '-';
const PROMO_CODE_SQUARE_ID = 'promo-code-square-';

export type PromoCodeActivationCallback = (result: boolean) => void;

export default class PromoCodeForm {
    inputRef = createRef<HTMLInputElement | null>();
    INPUT_ID: string = 'refill-promo-code-input';
    appState: AppState;

    @observable value: string = '';

    @observable isLoading: boolean = false;
    @observable error: string | null = null;
    @observable errorWithSeeMoreButton?: boolean = false;
    @observable success: boolean = false;
    @observable isEnabled: boolean = true;
    @observable isInputInFocus = false;
    @observable successMessage: string | null | undefined = undefined;

    constructor(appState: AppState) {
        this.appState = appState;
        this.onValueChange = this.onValueChange.bind(this);
    }

    @computed
    get isNewForm(): boolean {
        return this.appState.appSettings.promoCodeInput?.newViewEnabled ?? false;
    }

    @computed
    get length(): number {
        return this.appState.appSettings.promoCodeInput?.promoCodeLength ?? 6;
    }

    @computed
    get squares(): {
        value: string;
        isFocused: boolean;
        index: number;
        id: string;
        placeholder: string;
    }[] {
        let emptySquares: string[] = PROMO_CODE_PLACEHOLDER.repeat(this.length).split('');
        emptySquares.forEach((_, index) => {
            if (this.value.length > index) {
                emptySquares[index] = this.value[index];
            } else {
                emptySquares[index] = '';
            }
        });
        let isFocusedAlready = !this.isInputInFocus;
        return emptySquares.map((_, index) => {
            const isFocused = !isFocusedAlready && _ === '';
            if (isFocused) {
                isFocusedAlready = true;
            }
            return {
                value: _,
                isFocused,
                index,
                id: PROMO_CODE_SQUARE_ID + index,
                placeholder: PROMO_CODE_PLACEHOLDER,
            };
        });
    }

    @computed
    get isShownInV3Auth(): boolean {
        return (
            (this.appState.appSettings.registerForm?.includePromoCodeInput ?? false) &&
            this.appState.appSettings.registerForm?.formVersion === 'v3' &&
            (this.appState.modal.activeModal === Modal.LOGIN ||
                this.appState.modal.activeModal === Modal.REGISTER ||
                this.appState.modal.activeModal === Modal.LOGIN_BY_PHONE)
        );
    }

    @computed
    get isShownInRegisterForm(): boolean {
        return (
            (this.appState.appSettings.registerForm?.includePromoCodeInput ?? false) &&
            (this.appState.modal.activeModal === Modal.LOGIN ||
                this.appState.modal.activeModal === Modal.REGISTER ||
                this.appState.modal.activeModal === Modal.LOGIN_BY_PHONE)
        );
    }

    @computed
    get isPasteButtonShown(): boolean {
        return this.appState.appSettings.promoCodeInput?.includePasteButton ?? false;
    }

    @action
    reset(isEnabled: boolean = true) {
        this.value = '';
        this.isLoading = false;
        this.success = false;
        this.isEnabled = isEnabled;
        this.successMessage = undefined;
        this.clearError();
    }

    @action
    clearError() {
        this.error = null;
        this.errorWithSeeMoreButton = false;
    }

    @action
    onValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.value = e.target.value.toUpperCase();
        this.clearError();
    };

    @action.bound
    updateValue(value: string) {
        this.value = value;
        this.clearError();
    }

    @action
    tryActivatePromoCode(resultCallback?: PromoCodeActivationCallback) {
        const appState = this.appState;
        const promoCode = this.getValue();

        this.clearError();

        if (promoCode) {
            this.isLoading = true;
            appState.api.activatePromoCode(
                promoCode,
                this.onTryActivatePromoCode.bind(this, resultCallback),
            );
        } else {
            this.error = appState.t(getBonusActivationErrorToken());
            if (resultCallback) {
                resultCallback(!this.error);
            }
        }
    }

    onTryActivatePromoCode(
        resultCallback: PromoCodeActivationCallback | null | undefined,
        msg: messages.IServerResponse,
        act: messages.IClientActionResponse,
    ) {
        const appState = this.appState;

        appState.processApi(msg, act);
        this.isLoading = false;

        if (act.bonusAction && act.bonusAction.activateCode) {
            const result = act.bonusAction.activateCode.result;
            const meta = act.bonusAction.activateCode.meta as {error: string, type?: string, message?: string, percent?: string, wager?: string, should_redirect_to_rules?: string};
            if (result === bonuses.BonusCodeActivateResponse.Result.SUCCESS) {
                this.success = true;
                this.successMessage = undefined;
                this.appState.profileBonuses.onPromoCodeActivation();
                this.clearValue();
                if (meta?.type && meta.type === 'final-promo-success' && meta?.message) {
                    this.successMessage = meta.message;
                }
                if (this.appState.modal.activeModal === Modal.WRONG_PROMO_CODE) {
                    this.appState.modal.hideModal();
                }
            } else {
                if (meta && meta.error) {
                    this.error = appState.t('error-promo-code-conditions-not-met', {
                        reason: appState.t(meta.error),
                    });
                } else {
                    this.error = appState.t(getBonusActivationErrorToken(result));
                }
                if (meta.should_redirect_to_rules && meta.should_redirect_to_rules === 'true' && 
                    this.appState.appSettings.promoCodeInput?.seeMoreButton?.enabled &&
                    this.appState.appSettings.promoCodeInput?.seeMoreButton?.link) {
                    this.errorWithSeeMoreButton = true;
                    this.error = appState.t('error-promo-code-conditions-not-met-2')
                }
                if (this.isNewForm) {
                    this.clearValue();
                }
            }
        } else {
            this.error = appState.t(getBonusActivationErrorToken());
        }

        if (resultCallback) {
            resultCallback(!this.error);
        }
    }

    getValue(): string {
        if (this.isNewForm) {
            return this.value;
        }
        return getInputValueWithoutSpaces(this.appState, this.INPUT_ID);
    }

    @action
    clearValue(): void {
        this.value = '';
        this.isInputInFocus = false;
        clearInputValue(this.appState, this.INPUT_ID);
    }

    @action.bound
    onSquareBlur() {
        this.isInputInFocus = false;
    }

    @action.bound
    onSquareFocus(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        if (this.isNewForm) {
            const squareToFocus = this.squares.findIndex((_) => _.value === '');
            if (squareToFocus >= 0) {
                document.getElementById(PROMO_CODE_SQUARE_ID + squareToFocus)?.focus();
            }
            this.isInputInFocus = true;
        }
    }

    @action.bound
    onSquareChange(e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
        if (e.target.id && e.target.value) {
            const id = e.target.id;
            const squareIndex = Number(id.replace(PROMO_CODE_SQUARE_ID, '') || '0');
            const userInput = e.target.value.toUpperCase();
            if (userInput.length > 0) {
                this.value =
                    this.value.slice(0, squareIndex) + userInput[0] + this.value.slice(squareIndex);
                if (squareIndex === this.length - 1) {
                    if (!this.isShownInV3Auth && !this.isShownInRegisterForm) {
                        this.tryActivatePromoCode();
                    }
                    document.getElementById(PROMO_CODE_SQUARE_ID + squareIndex)?.blur();
                } else {
                    document.getElementById(PROMO_CODE_SQUARE_ID + (squareIndex + 1))?.focus();
                }
            }
        }
    }

    @action.bound
    onSquareKeyDown(e: React.KeyboardEvent<HTMLDivElement>) {
        if (e.keyCode === 8 && (e.target as HTMLElement).id) {
            const id = (e.target as HTMLElement).id;
            const squareIndex = Number(id.replace(PROMO_CODE_SQUARE_ID, '') || '0');
            const squareToFocus = squareIndex > 0 ? squareIndex - 1 : 0;
            this.value = this.value
                ? this.value.slice(0, squareIndex - 1) + this.value.slice(squareIndex)
                : '';
            document.getElementById(PROMO_CODE_SQUARE_ID + squareToFocus)?.focus();
        }
    }

    pasteFromClipboard = async () => {
        try {
            const text = await pasteFromClipboard(this.appState);
            runInAction(() => {
                if (text) {
                    this.value = text.slice(0, this.length).toUpperCase();
                    if (
                        this.isNewForm &&
                        this.value.length === this.length &&
                        (!this.isShownInV3Auth && !this.isShownInRegisterForm)
                    ) {
                        this.tryActivatePromoCode();
                    }
                }
            });
        } catch (error) {
            console.error(error);
        }
    };

    @action.bound
    closeWrongPromoCodeModal() {
        this.reset();
        this.appState.modal.hideModal();
    }
}
