import {action, observable} from 'mobx';
import AppState from '../../AppState';
import {getCryptoAddressLoadingError} from '../../../api/utils';
import {messages, wallet} from '../../../api/proto';
import * as React from 'react';
import {QRCode} from '../../components/QRCode';
import copyToClipboard from "copy-to-clipboard";

const CRYPTO_ADDRESS_CHECK_TIMEOUT = 2000;

export default class CryptoAddressForm {
    INPUT_ID = 'refillCryptoInput';
    qrCodeRef: React.RefObject<HTMLDivElement> = React.createRef();

    appState: AppState;
    qrCode: QRCode | null = null;

    @observable loading: boolean = false;
    @observable address: string | null = null;
    @observable addressCopied: boolean = false;
    @observable addressError: string | null = null;
    checkingTimeout: number | null = null;

    constructor(appState: AppState) {
        this.appState = appState;
        this.tryCopyCryptoWallet = this.tryCopyCryptoWallet.bind(this);
    }

    @action
    tryCopyCryptoWallet(event: React.SyntheticEvent<HTMLElement>) {
        event.preventDefault();

        const input = this.appState.getRef(this.INPUT_ID) as HTMLInputElement;
        const walletValue = input ? input.value : '';
        if (walletValue) {
            copyToClipboard(walletValue);
            this.setWalletAddressCopied(true);
            setTimeout(this.setWalletAddressCopied.bind(this, false), 1000);
        }
    }

    @action
    setWalletAddressCopied(value: boolean) {
        this.addressCopied = value;
    }

    @action
    loadCryptoAddress() {
        if (!this.loading) {
            this.loading = true;
            this.appState.api.getCryptoAddresses(this.onCryptoAddressLoaded.bind(this));
        }
    }

    @action.bound
    addCheckingTimeout() {
        this.checkingTimeout = window.setTimeout(() => {
            this.checkCryptoAddress();
        }, CRYPTO_ADDRESS_CHECK_TIMEOUT);
    }

    @action.bound
    clearCheckingTimeout() {
        if (this.checkingTimeout) {
            window.clearTimeout(this.checkingTimeout);
        }
    }

    @action.bound
    checkCryptoAddress() {
        this.appState.api.getCryptoAddresses(
            (msg: messages.IServerResponse, act: messages.IClientActionResponse) => {
                if (!this.address) {
                    if (
                        act.cryptoAddresses?.addresses?.length &&
                        act.cryptoAddresses.addresses[0].state === wallet.CryptoAddress.State.GENERATED
                    ) {
                        this.onCryptoAddressLoaded(msg, act);
                    } else {
                        this.addCheckingTimeout();
                    }
                }
            },
        );
    }

    onCryptoAddressLoaded(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.loading = false;
        this.appState.processApi(msg, act);

        if (act.cryptoAddresses && act.cryptoAddresses.addresses && act.cryptoAddresses.addresses.length) {
            const address = act.cryptoAddresses.addresses[0];
            if (address.state === wallet.CryptoAddress.State.GENERATED && address.address) {
                this.address = address.address;
                setTimeout(this.initializeQRCode.bind(this), 100);
            } else {
                this.addressError = this.appState.t(getCryptoAddressLoadingError());
            }
        } else {
            this.addressError = this.appState.t(getCryptoAddressLoadingError());
        }
    }

    @action.bound
    updateCryptoAddressFromSSE(address: string): void {
        this.clearCheckingTimeout();
        this.reset();
        this.address = address;
        setTimeout(this.initializeQRCode.bind(this), 100);
    }

    @action.bound
    updateCryptoAddress(address: string): void {
        this.clearCheckingTimeout();
        this.reset();
        this.address = address;
        setTimeout(this.initializeQRCode.bind(this), 100);
    }

    @action
    reset() {
        this.loading = false;
        this.address = null;
        this.addressCopied = false;
        this.addressError = '';
        this.destroyQRCode();
    }

    @action.bound
    initializeQRCode() {
        const address = this.address;
        if (address) {
            const qrElement = this.qrCodeRef.current;
            if (qrElement) {
                if (!this.qrCode) {
                    const QRCodeCtl: any = window.QRCode;
                    this.qrCode = new QRCodeCtl(qrElement, {
                        width: 210,
                        height: 210,
                        colorDark: '#000000',
                        colorLight: '#ffffff'
                    }) as QRCode;
                }
                this.qrCode!.makeCode(address);
            } else {
                console.error('Unable to initialize QR code, element with ref qrCodeRef does not exist');
            }
        } else {
            console.error('Unable to initialize QR code, address is not set: ', address);
        }
    }

    destroyQRCode() {
        if (this.qrCode) {
            this.qrCode.clear();
            this.qrCode = null;
        }
    }
}
