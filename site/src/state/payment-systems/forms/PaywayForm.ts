import {action, computed, observable} from 'mobx';
import AppState from '../../AppState';
import {NLong, NString} from '../../meta';
import Long from 'long';
import Payway, { FIRST_STEP_NUMBER } from '../Payway';
import {default as PaywayField, PaywayFieldType} from '../fields/PaywayField';
import {BaseStore} from '../BaseStore';
import RefillAmountInfo from '../RefillAmountInfo';
import { wallet } from '../../../api/proto';
import { PaywayFileUpload } from '../PaywayFileUpload';
import {formatCurrencyAmount} from '../../utils/Money';
import {isAndroid} from '../../utils/BrowserUtils';
import getScrollOffset from '../../utils/getScrollOffset';
import {formatBitcoinUrl} from '../../utils/formatBitcoinUrl';

export type PaywayFormDataFields = {[key: string]: any};

export enum ApiErrorLinkType {
    SUPPORT,
    BONUSES,
}

export interface PaywayFormData {
    balanceId: Long;
    amount: NLong;
    sourceAmount: NString;
    phone: NString;
    vaucher: NString;
    currency: NLong;
    fields: PaywayFormDataFields;
}

export default class PaywayForm {
    store: BaseStore;
    appState: AppState;

    @observable _isLoading: boolean = false;

    @action
    setLoading = (value: boolean) => {
        this._isLoading = value;
    };

    @computed
    get isLoading(): boolean {
        return this._isLoading || this.amountInfo.isLoading;
    }

    set isLoading(value: boolean) {
        this.setLoading(value);
    }

    @observable apiError: string | null = null;
    @observable apiErrorLinkType: ApiErrorLinkType | null = ApiErrorLinkType.SUPPORT;
    @observable royaltiesAmountStr: string | null = null;
    @observable pendingBonusesCount: number | null = null;
    @observable bonusError: boolean | null = null;
    @observable lastscrollTargetId: string = '';

    @observable amountInfo: RefillAmountInfo;

    @observable fields: PaywayFormDataFields = {};

    constructor(store: BaseStore, appState: AppState) {
        this.store = store;
        this.appState = appState;
        this.amountInfo = new RefillAmountInfo(appState);
    }

    @action
    setField = (id: string, value: any) => {
        this.fields = { ...this.fields, [id]: value };
    };

    @action
    reset(payway: Payway) {
        this._isLoading = false;
        this.apiError = null;
        this.royaltiesAmountStr = null;
        this.pendingBonusesCount = null;
        this.bonusError = null;
        payway.currentStep = FIRST_STEP_NUMBER;

        this.amountInfo.reset();

        if (payway.fields.some((_) => _.field.type === wallet.PaywayFieldType.IMAGE_UPLOAD)) {
            payway.fileUpload = new PaywayFileUpload();
        }

        for (let field of payway.fields) {
            field.clearValue();
            field.clearError();
        }
    }

    @action
    clearErrors(payway: Payway) {
        this.apiError = null;
        this.bonusError = null;
        for (let field of payway.fields) {
            field.clearError();
        }
    }

    @action
    onFieldValueChange(field: PaywayField) {
        this.apiError = null;
        this.royaltiesAmountStr = null;
        this.bonusError = null;
        this.pendingBonusesCount = null;

        field.clearError();
    }

    getFormDataOrSetError(payway: Payway, store: BaseStore, confirmedAccountRequired?: boolean): PaywayFormData | null {
        let resultFields = {};
        const paywayFields = payway.fields;
        let isValid = true;
        let accountConfirmed = true;

        // check if account confirmed
        if (confirmedAccountRequired) {
            const rawUserInfo = this.appState.rawUserInfo;
            accountConfirmed = rawUserInfo ? !!(
                rawUserInfo.phoneConfirmed ||
                rawUserInfo.emailConfirmed ||
                rawUserInfo.apiConfirmed) : false;
            if (!accountConfirmed) {
                this.apiError = this.appState.t('error-withdrawal-account-not-confirmed');
            }
        }

        // balance id is always required
        const balanceId = this.appState.user.getRealBalanceId();
        if (balanceId) {
            resultFields[PaywayFieldType.BALANCE_ID] = balanceId;
        } else {
            this.apiError = this.appState.t('error-refill-unknown-error');
            isValid = false;
        }

        // currency field is always required
        const currency = payway.currencies.length ? payway.currencies[0].id : this.appState.userInfo.balance.currencyId;
        if (currency) {
            resultFields[PaywayFieldType.CURRENCY] = currency;
        } else {
            this.apiError = this.appState.t('error-refill-unknown-error');
            isValid = false;
        }

        for (let field of paywayFields) {
            // it is important to call fillInFieldsOrSetError func to see all form errors
            let result = field.fillInFieldsOrSetError(store);
            resultFields = { ...resultFields, ...store.paywayForm.fields};
            isValid = isValid && result;
        }

        return isValid && accountConfirmed && balanceId ? {
            balanceId: balanceId,
            fields: resultFields,

            // legacy
            amount: resultFields[PaywayFieldType.AMOUNT] || null,
            sourceAmount: resultFields[PaywayFieldType.SOURCE_AMOUNT] || null,
            currency: resultFields[PaywayFieldType.CURRENCY] || null,
            phone: resultFields[PaywayFieldType.PHONE] || null,
            vaucher: resultFields[PaywayFieldType.VOUCHER] || null
        } : null;
    }

    @computed
    get amount(): string {
        const currencyId = this.appState.user.getRealBalanceCurrencyId();
        let amount = Long.fromNumber(0);
        if (this.fields[PaywayFieldType.AMOUNT] instanceof Long) {
            amount = this.fields[PaywayFieldType.AMOUNT];
        }
        return amount && currencyId ? formatCurrencyAmount(amount, currencyId) : '';
    }

    @computed
    get cardNumberPreview(): string {
        let cardNumber = '';
        const field = this.store.selectedPayway?.fields.find((field) => PaywayFieldType.CARD_NUMBER === field.type);
        if (field && this.fields[field.id]) {
            cardNumber = this.fields[field.id];
        }
        return `**** ${cardNumber.substr(cardNumber.length - 4)}`;
    }

    @computed
    get wallet(): string | null {
        const field = this.store.selectedPayway?.fields.find((field) => field.id === 'wallet');
        if (field) {
            return field.value;
        }
        return null;
    }

    @computed
    get btcCurrencyRate(): string | null {
        const field = this.store.selectedPayway?.fields.find((field) => field.id === 'currencyRate');
        if (field) {
            return field.value;
        }
        return null;
    }

    @computed
    get btcRefillAddress(): string | null {
        const amount = this.store.selectedPayway?.fields.find(
            (field) => field.type === PaywayFieldType.AMOUNT,
        );
        if (!!this.wallet) {
            if (!!amount?.value && !!this.btcCurrencyRate) {
                let amountInSatoshi = Math.ceil(
                    parseFloat(amount.value) * parseFloat(this.btcCurrencyRate) * 100,
                ).toString();
                const denomination = 8;
                if (amountInSatoshi.length <= denomination) {
                    const zerosCount = denomination - amountInSatoshi.length + 1;
                    for (let i = 0; i < zerosCount; i++) {
                        amountInSatoshi = '0' + amountInSatoshi;
                    }
                }
                const amountInBitcoin =
                    amountInSatoshi.substr(0, amountInSatoshi.length - denomination) +
                    '.' +
                    amountInSatoshi.substr(amountInSatoshi.length - denomination);
                return formatBitcoinUrl(this.wallet, amountInBitcoin);
            } else {
                return formatBitcoinUrl(this.wallet);
            }
        }
        return null;
    }

    @action
    scrollToTarget = () => {
        const targetId = `autoscroll-target-${this.store.selectedPayway?.currentStep}`;
        const scrollTarget = this.appState.getRef(targetId) ? this.appState.getRef(targetId) as HTMLElement : null;
        if (scrollTarget && (this.lastscrollTargetId !== targetId || isAndroid())) {
            this.lastscrollTargetId = targetId;

            if (isAndroid()) {
                const scrollTopOffset = getScrollOffset(scrollTarget);
                window.scrollTo({
                    top: scrollTopOffset,
                    behavior: 'smooth'
                });
            } else {
                scrollTarget.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
            }
        }
    }

    handleFocus = () => {
        if (this.appState.isMobile) {
            this.scrollToTarget();
        }
    };

    handleKeyDown = (e: React.KeyboardEvent, field: PaywayField) => {
        if (e.key === 'Enter' && field.type === PaywayFieldType.CARD_HOLDER_NAME) {
            e.preventDefault();
            this.store.selectedPayway?.goToNextStep();
        }
    }
}
