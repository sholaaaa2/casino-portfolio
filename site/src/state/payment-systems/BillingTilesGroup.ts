import { observable, computed } from 'mobx';
import { wallet } from '../../api/proto';
import AppState from '../AppState';
import BillingTile from './BillingTile';
import Payway from './Payway';

export default class BillingTilesGroup {
  appState: AppState;

  @observable title?: string | null = null;
  @observable tiles: BillingTile[] = [];
  @observable allPayways: Payway[] = [];

  constructor(
    appState: AppState,
    billingTilesGroup: wallet.IBillingTilesGroup,
    basePaywayUrl: string,
    allPayways: Payway[],
  ) {
    this.appState = appState;

    this.title = billingTilesGroup.title;
    this.tiles = billingTilesGroup.tiles
      ? billingTilesGroup.tiles.map(
          t => new BillingTile(this.appState, t, basePaywayUrl, allPayways)
        )
      : [];
    this.allPayways = allPayways;
  }

  @computed
  get hasBadgeTags(): boolean {
    return this.tiles.some(t => t.badgeTag);
  }
}
