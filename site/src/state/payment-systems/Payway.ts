import Long from 'long';
import {action, computed, observable} from 'mobx';
import {commons, wallet} from '../../api/proto';
import {formatPaywayType} from '../../api/utils';
import AppState from '../AppState';
import {NString} from '../meta';
import {formatCurrencyAmount, formatCurrencyCode, NASymbol} from '../utils/Money';
import {BaseStore} from './BaseStore';
import AccountField from './fields/AccountField';
import AmountField from './fields/AmountField';
import {CardNumberField} from './fields/CardNumberField';
import {CryptoAddressField} from './fields/CryptoAddressField';
import {CryptoAddressWithoutQRField} from './fields/CryptoAddressWithoutQRField';
import {ImageUploadField} from './fields/ImageUploadField';
import MobileDetectField from './fields/MobileDetectField';
import PasswordField from './fields/PasswordField';
import SelectField from './fields/SelectField';
import PaywayField, {PaywayFieldType} from './fields/PaywayField';
import PhoneField from './fields/PhoneField';
import VoucherField from './fields/VoucherField';
import {CardHolderField} from './fields/CardHolderField';
import {CardExpirationMonthYearField} from './fields/CardExpirationMonthYearField';
import {CardCVVField} from './fields/CardCVVField';
import {EmailField} from './fields/EmailField';
import {BooleanField} from './fields/BooleanField';
import PaywaysGroup from './PaywaysGroup';
import PSProvider from './providers/BaseProvider';
import Cashlib from './providers/Cashlib';
import CoinPayments from './providers/CoinPayments';
import Connectum from './providers/Connectum';
import Hexopay from './providers/Hexopay';
import Interkassa from './providers/Interkassa';
import Paysera from './providers/Paysera';
import Piastrix from './providers/Piastrix';
import Walletec from './providers/Walletec';
import WeCashUp from './providers/WeCashUp';
import Payzen from './providers/Payzen';
import Paygigaa from './providers/Paygigaa';
import HiddenField from './fields/HiddenField';
import Paystack from './providers/Paystack';
import Royalpay from './providers/Royalpay';
import PinSale from './providers/PinSale';
import Redpay from './providers/Redpay';
// import TjTerminal from "./providers/TjTerminal";
import Simpay from "./providers/Simpay";
import Muchbetter from "./providers/Muchbetter";
import PerfectMoney from "./providers/PerfectMoney";
import PremierPay from "./providers/PremierPay";
import InfernoPay from "./providers/InfernoPay";
import Devcode from "./providers/Devcode";
import SwiftPayP2P from './providers/SwiftPayP2P';
import * as currencies from '../utils/Currencies';
import WgPayways from './providers/WgPayways';
import { CashApp } from './providers/CashApp';
import { PaywayFileUpload } from './PaywayFileUpload';
import React from 'react';
import copyToClipboard from "copy-to-clipboard";

export const FIRST_STEP_NUMBER = 1;

export interface PaywayCategory {
    name: string;
    payways: PaywaysGroup[];
}

export interface IWithdrawalLink extends wallet.ISkyCryptoLinkRow {
    formattedAmount?: string;
    expirationNumber?: number;
}

export default class Payway {
    appState: AppState;
    store: BaseStore;

    @observable private id: string;
    @observable private name: string;
    @observable private translatedName: string;
    @observable private category: string;
    @observable private logo: string;
    @observable private group: NString = null;
    @observable minAmount: Long | null = null;
    @observable maxAmount: Long | null = null;
    @observable public provider: PSProvider;
    @observable public fields: PaywayField[];
    @observable private disabled: boolean;
    @observable private delayed: boolean;
    @observable public currencies: commons.ICurrency[];
    @observable public uiSettings: wallet.PaywayUISettings;
    @observable public disclaimer: String;
    @observable public config?: string | null;
    @observable public links?: IWithdrawalLink[];
    @observable instructions?: string;
    @observable params: {[key: string]: string} = {};
    @observable totalSteps = FIRST_STEP_NUMBER;
    @observable currentStep = FIRST_STEP_NUMBER;
    @observable copyButtons: {[key: string]: boolean} = {};
    @observable isAdditionalInfoOpened = false;
    fileUpload?: PaywayFileUpload;
    @observable public css?: string | null;

    @observable showFields?: boolean = true;

    constructor(
        appState: AppState,
        provider: wallet.RefillProvidersResponse.IProvider,
        payway: wallet.IPayway,
        store: BaseStore,
        public isRefill: boolean,
    ) {
        this.appState = appState;
        this.store = store;

        this.id = payway.id!;
        this.provider = this.getProviderInstance(provider);
        this.name = this.provider.getPaywayName(payway, appState);
        this.translatedName = this.appState.t(this.name);
        this.logo = this.provider.getPaywayLogo(payway, appState);
        this.category = formatPaywayType(payway.type);
        this.group = payway.groupId;
        this.disabled = !!payway.disabled;
        this.delayed = !!payway.delayed;
        this.disclaimer = payway.disclaimer!!;
        this.config = payway.config;
        this.instructions = payway.wgPaywaysProps?.instructions ?? "";
        this.params = payway.wgPaywaysProps?.params ?? {};
        this.css = payway.css;

        if (payway.currencies && payway.currencies.length) {
            const currency = payway.currencies[0]!;
            this.currencies = [currency.currency!];
            this.minAmount = currency.minUserAmount || null;
            this.maxAmount = currency.maxUserAmount || null;
        } else {
            this.currencies = [];
            this.minAmount = null;
            this.maxAmount = null;
        }
        this.fields = [];

        if (payway.fields) {
            this.totalSteps = Math.max(...payway.fields.map(_ => _.shownOnStep || FIRST_STEP_NUMBER));
            for (let field of payway.fields) {
                const fieldInstance = this.getPaywayFieldInstance(field, payway, this.provider);
                if (fieldInstance) {
                    this.fields.push(fieldInstance);
                }
            }
        }

        this.uiSettings = payway.uiSettings ? new wallet.PaywayUISettings(payway.uiSettings) : new wallet.PaywayUISettings();

        if (payway.links) {
            this.links = payway.links.map(link => {
                if (link.currency && link.amount) {
                    let currencyInfo = currencies.findCurrencyById(link.currency);
                    let formattedAmount = formatCurrencyAmount(link.amount, currencyInfo);
                    return {
                      ...link,
                      formattedAmount,
                      expirationNumber: link.expiration?.toNumber()
                    }
                } else {
                    return {
                      ...link,
                      expirationNumber: link.expiration?.toNumber()
                    };
                }
            })
        }
    }

    @action.bound
    pressCopyButton(buttonId: string, value: string) {
        copyToClipboard(value);
        this.copyButtons[buttonId] = true
    }

    @computed
    get isSubmitButtonDisabled(): boolean {
        return this.fields.some(_ => _.isValid === false);
    }

    @computed
    get isGoToNextStepButtonDisabled(): boolean {
        const currentStepFields = this.fields.filter(_ => _.shownOnStep === this.currentStep);
        return currentStepFields.some(_ => _.isValid === false);
    }

    @computed
    get isMultiStep(): boolean {
        return this.totalSteps > FIRST_STEP_NUMBER;
    }

    @computed
    get isMultiStepAndNotTheFirst(): boolean {
        return this.isMultiStep && this.currentStep > FIRST_STEP_NUMBER;
    }

    @computed
    get isMultiStepAndNotTheFinal(): boolean {
        return this.isMultiStep && this.currentStep < FIRST_STEP_NUMBER;
    }

    @computed
    get isFinalStep(): boolean {
        return this.totalSteps === this.currentStep;
    }

    @computed
    get isMultiStepAndFinal(): boolean {
        return this.isMultiStep && this.isFinalStep;
    }

    getId(): string {
        return this.id;
    }

    getName(): string {
        if (this.store.useTiles && this.store.selectedTile) {
            return this.store.selectedTile.title;
        }
        return this.name;
    }

    getTranslatedName(): string {
        return this.translatedName;
    }

    getLogo(): string {
        return this.logo;
    }

    getCategory(): string {
        return this.category;
    }

    getGroup(): NString {
        return this.group;
    }

    amountFieldsTypes: PaywayFieldType[] = [PaywayFieldType.AMOUNT];

    @computed
    get amountDataFields(): PaywayField[] {
        const fields: PaywayField[] = [];

        this.amountFieldsTypes.forEach((type) => {
            const field = this.fields.find((field) => type === field.type);
            if (field) {
                fields.push(field);
            }
        });

        return fields;
    }

    cardFieldsTypes: PaywayFieldType[] = [
        PaywayFieldType.CARD_NUMBER,
        PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR,
        PaywayFieldType.CARD_CVV,
        PaywayFieldType.CARD_HOLDER_NAME,
    ];

    @computed
    get cardDataFields(): PaywayField[] {
        let fields: PaywayField[] = [];
        this.cardFieldsTypes.forEach((type) => {
            const field = this.fields.filter((field) => type === field.type);
            if (field) {
                fields = fields.concat(field);
            }
        });

        return fields;
    }

    @computed
    get anotherDataFields(): PaywayField[] {
        return this.fields.filter(
            (field) =>
                this.amountFieldsTypes.indexOf(field.type) < 0 &&
                this.cardFieldsTypes.indexOf(field.type) < 0,
        );
    }

    @computed
    get isCurrencySelectShown() {
        return this.provider.type !== wallet.WithdrawProvider.CASHAPP;
    }

    getNextFieldByType(fields: PaywayField[], fieldType: PaywayFieldType): PaywayField | null | undefined {
        const idx = fields.findIndex((f) => f.type === fieldType);
        const nextIdx = idx + 1;
        return idx >= 0 && fields.length > nextIdx ? fields[nextIdx] : null;
    }

    getNextField(fieldType: PaywayFieldType): PaywayField | null | undefined {
        return this.getNextFieldByType(this.amountDataFields, fieldType) || this.getNextFieldByType(this.cardDataFields, fieldType) ||this.getNextFieldByType(this.anotherDataFields, fieldType);
    }

    getMinAmount(): Long | null | undefined {
        return this.minAmount;
    }

    getMaxAmount(): Long | null | undefined {
        return this.maxAmount;
    }

    getProvider(): PSProvider {
        return this.provider;
    }

    getDelayed(): boolean {
        return this.delayed;
    }

    hasMinAmount(): boolean {
        return !!(this.minAmount && this.minAmount.greaterThan(Long.ZERO));
    }

    hasMaxAmount(): boolean {
        return !!(this.maxAmount && this.maxAmount.greaterThan(Long.ZERO));
    }

    formatMinAmount(withCurrency?: boolean): string {
        const isWithCurrency = withCurrency ?? this.provider.type === wallet.WithdrawProvider.CASHAPP;
        const currencyId = this.appState.user.getRealBalanceCurrencyId();
        return this.hasMinAmount() && currencyId
            ? formatCurrencyAmount(this.minAmount, currencyId, !isWithCurrency)
            : NASymbol;
    }

    formatMaxAmount(withCurrency?: boolean): string {
        const isWithCurrency = withCurrency ?? this.provider.type === wallet.WithdrawProvider.CASHAPP;
        const currencyId = this.appState.user.getRealBalanceCurrencyId();
        return this.hasMaxAmount() && currencyId
            ? formatCurrencyAmount(this.maxAmount, currencyId, !isWithCurrency)
            : NASymbol;
    }

    formatCurrencyCode(): string {
        const currencyId = this.appState.user.getRealBalanceCurrencyId();
        return currencyId ? formatCurrencyCode(currencyId) : NASymbol;
    }

    findRelatedGroup(store: BaseStore): PaywaysGroup | null {
        const id = this.id;
        return (
            store.paywaysByGroup.find(
                (g: PaywaysGroup) => !!g.payways.find((p: Payway) => p.id === id),
            ) || null
        );
    }

    isDisabled(): boolean {
        return this.disabled;
    }

    @action
    setDisabled(value: boolean) {
        this.disabled = value;
    }

    private getPaywayFieldInstance(
        field: wallet.IPaywayField,
        payway: wallet.IPayway,
        provider: PSProvider,
    ): PaywayField | null {
        const type = this.processFieldType(field.type, field.id);
        const title = provider.getPaywayFieldTitle(field, payway, this.appState);
        const placeholder = provider.getPaywayFieldPlaceholder(field, payway, this.appState);
        const format = field.format;
        let options = null;
        let Cls = null;

        if (field.select) {
            const keys = Object.keys(field.select);
            if (keys.length) {
                options = [];
                for (let key of keys) {
                    if (field.select[key]) {
                        options.push({key, value: field.select[key]});
                    }
                }
            }
        }

        switch (type) {
            case PaywayFieldType.AMOUNT:
                Cls = AmountField;
                break;
            case PaywayFieldType.VOUCHER:
                Cls = VoucherField;
                break;
            case PaywayFieldType.PHONE:
                Cls = PhoneField;
                break;
            case PaywayFieldType.ACCOUNT:
                Cls = AccountField;
                break;
            case PaywayFieldType.PASSWORD:
                Cls = PasswordField;
                break;
            case PaywayFieldType.MOBILE_DETECT:
                Cls = MobileDetectField;
                break;
            case PaywayFieldType.SELECT:
                Cls = SelectField;
                break;
            case PaywayFieldType.HIDDEN:
                Cls = HiddenField;
                break;
            case PaywayFieldType.CRYPTO_ADDRESS:
                Cls = CryptoAddressField;
                break;
            case PaywayFieldType.CRYPTO_ADDRESS_WITHOUT_QR:
                Cls = CryptoAddressWithoutQRField;
                break;
            case PaywayFieldType.IMAGE_UPLOAD:
                Cls = ImageUploadField;
                break;
            case PaywayFieldType.CARD_NUMBER:
                Cls = CardNumberField;
                break;
            case PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR:
                Cls = CardExpirationMonthYearField;
                break;
            case PaywayFieldType.CARD_CVV:
                Cls = CardCVVField;
                break;
            case PaywayFieldType.CARD_HOLDER_NAME:
                Cls = CardHolderField;
                break;
            case PaywayFieldType.EMAIL:
                Cls = EmailField;
                break;
            case PaywayFieldType.BOOLEAN:
                Cls = BooleanField;
                break;
            default:
                Cls = PaywayField;
                break;
        }

        if (type && Cls) {
            return new Cls(
                this.appState,
                this,
                field.id!,
                type,
                format,
                title,
                placeholder,
                options,
                field.value,
                field.ibanCode,
                field
            );
        } else {
            return null;
        }
    }

    private getProviderInstance(provider: wallet.RefillProvidersResponse.IProvider): PSProvider {
        switch (provider.provider) {
            case wallet.WithdrawProvider.PIASTRIX:
                return new Piastrix(provider.provider, this.appState);
            case wallet.WithdrawProvider.COINPAYMENTS:
                return new CoinPayments(provider.provider, this.appState);
            case wallet.WithdrawProvider.HEXOPAY:
                return new Hexopay(provider.provider, this.appState);
            case wallet.WithdrawProvider.WECASHUP:
                return new WeCashUp(provider.provider, this.appState, provider.wecashup);
            case wallet.WithdrawProvider.CASHLIB:
                return new Cashlib(provider.provider, provider.refillType, this.appState);
            case wallet.WithdrawProvider.WALLETTEC:
                return new Walletec(provider.provider, this.appState);
            case wallet.WithdrawProvider.PAYSERA:
                return new Paysera(provider.provider, this.appState);
            case wallet.WithdrawProvider.INTERKASSA:
                return new Interkassa(provider.provider, this.appState, provider.interkassa);
            case wallet.WithdrawProvider.CONNECTUM:
                return new Connectum(provider.provider, this.appState);
            case wallet.WithdrawProvider.PAYZEN:
                return new Payzen(provider.provider, this.appState);
            case wallet.WithdrawProvider.PAYGIGA:
                return new Paygigaa(provider.provider, this.appState);
            case wallet.WithdrawProvider.PAYSTACK:
                return new Paystack(provider.provider, this.appState);
            case wallet.WithdrawProvider.ROYALPAY:
                return new Royalpay(provider.provider, this.appState);
            case wallet.WithdrawProvider.PINSALE:
                return new PinSale(provider.provider, this.appState);
            case wallet.WithdrawProvider.REDPAY:
                return new Redpay(provider.provider, this.appState);
            // case wallet.WithdrawProvider.TJTERMINALS:
            //     return new TjTerminal(provider.provider, this.appState);
            case wallet.WithdrawProvider.SIMPAY:
                return new Simpay(provider.provider, this.appState);
            case wallet.WithdrawProvider.MUCHBETTER:
                return new Muchbetter(provider.provider, this.appState);
            case wallet.WithdrawProvider.PERFECTMONEY:
                return new PerfectMoney(provider.provider, this.appState);
            case wallet.WithdrawProvider.PREMIERPAY:
                return new PremierPay(provider.provider, this.appState);
            case wallet.WithdrawProvider.INFERNOPAY:
                return new InfernoPay(provider.provider, this.appState);
            case wallet.WithdrawProvider.DEVCODE:
                return new Devcode(provider.provider, this.appState);
            case wallet.WithdrawProvider.SWIFTPAYP2P:
                return new SwiftPayP2P(provider.provider, this.appState);
            case wallet.WithdrawProvider.WG_PAYWAYS:
                return new WgPayways(provider.provider, this.appState);
            case wallet.WithdrawProvider.CASHAPP:
                return new CashApp(provider.provider, this.appState);
            default:
                console.log('Used default provider for:', provider);
                return new PSProvider(provider.provider!, this.appState);
        }
    }

    private processFieldType(
        type: wallet.PaywayFieldType | null | undefined,
        id: NString,
    ): PaywayFieldType | null {
        switch (type) {
            default:
            case wallet.PaywayFieldType.DEFAULT:
            case wallet.PaywayFieldType.STRING:
                return this.processFieldTypeById(id);
            case wallet.PaywayFieldType.AMOUNT:
                return PaywayFieldType.AMOUNT;
            case wallet.PaywayFieldType.PHONE:
                return PaywayFieldType.PHONE;
            case wallet.PaywayFieldType.ACCOUNT:
                return PaywayFieldType.ACCOUNT;
            case wallet.PaywayFieldType.CARD_NUMBER:
                return PaywayFieldType.CARD_NUMBER;
            case wallet.PaywayFieldType.PASSWORD:
                return PaywayFieldType.PASSWORD;
            case wallet.PaywayFieldType.CRYPTO_ADDRESS:
                return PaywayFieldType.CRYPTO_ADDRESS;
            case wallet.PaywayFieldType.MOBILE_DETECT:
                return PaywayFieldType.MOBILE_DETECT;
            case wallet.PaywayFieldType.SELECT:
                return PaywayFieldType.SELECT;
            case wallet.PaywayFieldType.IBAN:
                return PaywayFieldType.IBAN;
            case wallet.PaywayFieldType.HIDDEN:
                return PaywayFieldType.HIDDEN;
            case wallet.PaywayFieldType.IMAGE_UPLOAD:
                return PaywayFieldType.IMAGE_UPLOAD;
            case wallet.PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR:
                return PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR;
            case wallet.PaywayFieldType.CARD_CVV:
                return PaywayFieldType.CARD_CVV;
            case wallet.PaywayFieldType.CARD_HOLDER_NAME:
                return PaywayFieldType.CARD_HOLDER_NAME;
            case wallet.PaywayFieldType.EMAIL:
                return PaywayFieldType.EMAIL;
            case wallet.PaywayFieldType.BOOLEAN:
                return PaywayFieldType.BOOLEAN;
            case wallet.PaywayFieldType.CRYPTO_ADDRESS_WITHOUT_QR:
                return PaywayFieldType.CRYPTO_ADDRESS_WITHOUT_QR;
        }
    }

    private processFieldTypeById(id: NString): PaywayFieldType | null {
        switch (id) {
            case 'country_code':
                return null;
            case 'amount':
                return PaywayFieldType.AMOUNT;
            case 'voucher':
                return PaywayFieldType.VOUCHER;
            case 'phone':
            case 'phone_number':
                return PaywayFieldType.PHONE;
            case 'account':
                return PaywayFieldType.ACCOUNT;
            case 'card_number':
            case 'pan':
                return PaywayFieldType.CARD_NUMBER;
            case 'password':
                return PaywayFieldType.PASSWORD;
            case 'crypto_address':
                return PaywayFieldType.CRYPTO_ADDRESS;
            case 'mobile':
                return PaywayFieldType.MOBILE_DETECT;
            case 'exp_month':
                return PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR;
            case 'exp_year':
                return PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR;
            case 'cvv':
                return PaywayFieldType.CARD_CVV;
            case 'name':
                return PaywayFieldType.CARD_HOLDER_NAME;
            case 'email':
                return PaywayFieldType.EMAIL;
            case 'expiration_date':
                return PaywayFieldType.CARD_EXPIRATION_MONTH_YEAR;
            default:
                return PaywayFieldType.UNIFIED;
        }
    }

    get warning(): string | null {
        if (
            this.appState.appSettings.showPinsalePaymentWarning &&
            this.store.selectedPayway &&
            this.store.selectedPayway.getId() === 'pinsale'
        ) {
            return this.appState.t('ui-pinsale-payment-warning');
        }
        return null;
    }

    @action.bound
    goToNextStep(event?: React.MouseEvent<HTMLElement>): void {
        event?.preventDefault();
        this.currentStep = this.currentStep + 1;
    }

    @action.bound
    goToPrevStep(event?: React.MouseEvent<HTMLElement>): void {
        event?.preventDefault();
        this.currentStep = this.currentStep - 1;
    }

    get newFormAvailable(): boolean {
        const newFormProviders = this.appState.appSettings.billing?.newFormProviders;
        if (
            newFormProviders &&
            newFormProviders.some(
                (p) => this.provider.type === wallet.WithdrawProvider[p.toUpperCase()],
            )
        ) {
            return true;
        }
        return false;
    }

    get providerCustomizations() {
        const customizations = this.store.providersCustomizations;
        const providerName = wallet.WithdrawProvider[this.provider.type]
        if (providerName) {
            if (customizations && customizations[providerName]) {
                return customizations[providerName];
            } else if (customizations && customizations[providerName.toLowerCase()]) {
                return customizations[providerName.toLowerCase()];
            }
        }

        return null;
    }

    fill(fields: PaywayField[]) {
        this.fields.forEach((f) => {
            const f2 = fields.find((f2) => f2.id === f.id);
            if (f2) {
                f.setValue(f2.value);
            }
        });
    }

    setEmail(email: string) {
        this.fields.forEach((f) => {
            if (f.type === PaywayFieldType.EMAIL) {
                f.setValue(email);
            }
        });
    }

    setPhone(phone: string) {
        this.fields.forEach((f) => {
            if (f.type === PaywayFieldType.PHONE) {
                f.setValue(phone);
            }
        });
    }

    @action.bound
    toggleAdditionalInfo() {
        this.isAdditionalInfoOpened = !this.isAdditionalInfoOpened;
    }
}
