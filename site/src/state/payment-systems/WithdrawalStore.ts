import AppState from '../AppState';
import {action, computed, observable} from 'mobx';
import {bonuses, messages, users, wallet} from '../../api/proto';
import Payway from './Payway';
import * as React from 'react';
import {BaseStore} from './BaseStore';
import {PaywayFormData} from './forms/PaywayForm';
import {NotificationItemLevel} from '../components/Notificator';
import {groupPaywaysByCategory, groupPaywaysByGroupId} from './utils';
import {PaywayFieldType} from './fields/PaywayField';
import {
    BonusMeta,
    BonusType,
    getWithdrawalError,
    processBoolean,
    processNumber,
    processLong,
    millisecondsToHoursString,
    isDef,
} from '../../api/utils';
import Long from 'long';
import {formatCurrencyAmount} from '../utils/Money';
import {NLong, SitePages} from '../meta';
import BillingTilesGroup from './BillingTilesGroup';

const MobileDetect = require('mobile-detect');

export interface WithdrawalRequestData {
    id: string;
    balanceId: Long;
    amount: NLong;
    credentials?: string | null;
    provider?: wallet.WithdrawProvider;
    fields?: { [key: string]: string };
    tileId?: string | null;
}

export interface WithdrawalInspector {
    spinsLeft: number;
    minWithdrawal: string | null;
    maxWithdrawal: string | null;
    timeUntilNextWithdrawal: string;
}

export default class WithdrawalStore extends BaseStore {
    @observable withdrawalInspector: WithdrawalInspector | null = null;
    @observable withdrawalLinkForModal?: string;

    constructor(appState: AppState) {
        super(appState);
        this.onFormAction = this.onFormAction.bind(this);
        this.basePaywayUrl = '/withdraw';
    }

    @computed
    get isDocumentsRequired(): boolean {
        if (this.selectedPayway?.provider?.type && this.appState.userInfo.providerRestrictions?.length) {
            const currentProvider = this.appState.userInfo.providerRestrictions
                .find((p) => p.provider && p.provider === this.selectedPayway?.provider.type);
            if (currentProvider) {
                const anyActionRestriction = currentProvider.restrictions?.indexOf(
                    users.UserInfo.UserRestrictions.DOCUMENTS_REQUIRED_FOR_ANY_ACTION
                );
                const withrawRestriction = currentProvider.restrictions?.indexOf(
                    users.UserInfo.UserRestrictions.DOCUMENT_REQUIRED_FOR_WITHDRAW
                );
                if (this.isDocumentsRequiredByGeneralRestrictions) {
                    return !(anyActionRestriction === -1 && withrawRestriction === -1)
                } else {
                    return anyActionRestriction !== -1 || withrawRestriction !== -1;
                }
            }
        }
        return this.isDocumentsRequiredByGeneralRestrictions;
    }

    @computed
    get isDocumentsRequiredByGeneralRestrictions() {
        return (
            this.appState.userInfo.restrictions.indexOf(
                users.UserInfo.UserRestrictions
                    .DOCUMENTS_REQUIRED_FOR_ANY_ACTION
            ) !== -1 ||
            this.appState.userInfo.restrictions.indexOf(
                users.UserInfo.UserRestrictions.DOCUMENT_REQUIRED_FOR_WITHDRAW
            ) !== -1
        );
    }

    // region ---- page state
    getFormActionToken(): string {
        if (this.paywayForm.royaltiesAmountStr) {
            return 'ui-withdrawal-with-royalties-button';
        } else if (this.paywayForm.pendingBonusesCount) {
            return 'ui-withdrawal-and-close-bonuses';
        } else {
            return 'ui-withdrawal-button';
        }
    }

    getAmountLabelToken(): string {
        return 'ui-withdrawal-amount';
    }

    // endregion

    // region ---- load payways
    @action
    loadPayways(forceLoad?: boolean) {
        if (!this.isPageDataLoaded() || forceLoad) {
            this.isPaywaysLoading = true;
            this.appState.api.getWithdrawalTypes(this.onPaywaysLoaded.bind(this));
        }
    }

    onPaywaysLoaded(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.isPaywaysLoading = false;
        this.appState.processApi(msg, act);

        const mobileDetect = new MobileDetect(window.navigator.userAgent);
        const isMobile = !!mobileDetect.mobile();
        const payways: Payway[] = [];

        if (act.withdrawTypes && act.withdrawTypes.types) {
            const types = isMobile ? act.withdrawTypes.types : act.withdrawTypes.types.filter((p: wallet.IPayway) => !p.mobileOnly);
            for (let payway of types) {
                if (payway.id && isDef(payway.provider) && payway.fields && payway.fields.length) {
                    payways.push(new Payway(this.appState, {provider: payway.provider}, payway, this, false));
                } else {
                    console.error('Unable to process payway with empty id, provider or fields: ', payway);
                }
            }
        } else {
            console.error('Unable to process WithdrawTypesResponse, missing withdrawal types in action: ', act);
        }

        if (act.withdrawTypes) {
            this.useTiles = !!act.withdrawTypes.useTiles;
            this.tilesGroups = act.withdrawTypes.tiles ?
                act.withdrawTypes.tiles.map((t) => new BillingTilesGroup(
                    this.appState,
                    t,
                    this.basePaywayUrl,
                    payways,
                )) : [];
        }

        this.payways = payways;
        this.paywaysByGroup = groupPaywaysByGroupId(this.appState, payways, this.basePaywayUrl);
        this.paywaysByCategory = groupPaywaysByCategory(this.paywaysByGroup);

        const isForced = this.selectedPayway?.provider.type === wallet.WithdrawProvider.WG_PAYWAYS;
        this.tryPreselectPayway(isForced);
        if (this.appState.appSettings.billing?.btcWithdrawalAddressCheck) {
            this.appState.refillStore.loadPayways(!this.appState.refillStore.hasPayways)
        }
    }

    @action
    tryPreselectPayway(isForced = false) {
        if (!this.selectedPayway || isForced) {
            if (this.payways.length === 1) {
                this.selectPaywayById(this.payways[0].getId());
            } else {
                this.selectPaywayByUrl();
            }
        }
    }

    onSelectedPaywayChange(payway: Payway) {
        payway.getProvider().onWithdrawalSelectedPaywayChange(this, this.paywayForm, payway);
    }

    reloadPayways() {
        if (this.appState.page === SitePages.WITHDRAW) {
            this.loadPayways(true);
            this.appState.billingHistory.loadHistory();
        }
    }

    // endregion

    // region ---- UI event handlers
    @action
    onFormAction(e: React.MouseEvent<HTMLElement>) {
        e.preventDefault();
        e.stopPropagation();
        if (this.selectedPayway?.isFinalStep) {
            this.tryWithdraw();
        } else if (
            this.selectedPayway?.isMultiStep &&
            !this.selectedPayway.isGoToNextStepButtonDisabled
        ) {
            this.selectedPayway.goToNextStep();
        }
    }

    // endregion

    // ---- withdraw
    tryWithdraw() {
        const payway = this.selectedPayway;

        if (payway) {
            const paywayForm = this.paywayForm;
            paywayForm.clearErrors(payway);

            //skip client check for pinsale. All checks will be executed on the server side
            const confirmationRequired = payway.getProvider().type !== wallet.WithdrawProvider.PINSALE;
            const paywayFormData = paywayForm.getFormDataOrSetError(payway, this, confirmationRequired);

            if (paywayFormData) {
                const data = {
                    id: payway.getId(),
                    credentials: paywayFormData.fields ? paywayFormData.fields[PaywayFieldType.ACCOUNT] : null,
                    balanceId: paywayFormData.balanceId,
                    amount: paywayFormData.amount,
                    provider: payway.getProvider().type,
                    fields: paywayFormData.fields || {},
                    tileId: this.appState.withdrawalStore.selectedTile?.id ?? ""
                };

                paywayForm.isLoading = true;
                this.appState.api.getWithdrawalRoyalties(data, this.onWithdrawalRoyalties.bind(this, data, paywayFormData, payway));
            }
        }
    }

    @action
    onWithdrawalRoyalties(data: WithdrawalRequestData, paywayFormData: PaywayFormData, payway: Payway, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        const paywayForm = this.paywayForm;

        if (act.withdrawCalcRoyalty && act.withdrawCalcRoyalty.ok) {
            if (!act.withdrawCalcRoyalty.amount || act.withdrawCalcRoyalty.amount.eq(Long.ZERO)) {
                this.appState.profileBonuses.tryLoadBonusesData(this.onBonusesLoaded.bind(this, data, paywayFormData, payway));
            } else {
                const royaltiesStrAmount = formatCurrencyAmount(act.withdrawCalcRoyalty.amount, act.withdrawCalcRoyalty.currency, true);
                if (paywayForm.royaltiesAmountStr === royaltiesStrAmount) {
                    this.appState.profileBonuses.tryLoadBonusesData(this.onBonusesLoaded.bind(this, data, paywayFormData, payway));
                } else {
                    paywayForm.isLoading = false;
                    paywayForm.royaltiesAmountStr = royaltiesStrAmount;
                }
            }
        } else {
            const error = act.withdrawCalcRoyalty ? act.withdrawCalcRoyalty.error : null;
            paywayForm.apiError = this.appState.t(getWithdrawalError(error, payway.getProvider().type));
        }
    }

    @action

    onBonusesLoaded(data: WithdrawalRequestData, paywayFormData: PaywayFormData, payway: Payway) {
        const pendingBonuses = this.appState.profileBonuses.pendingBonuses;

        if (pendingBonuses.length) {
            if (pendingBonuses.length === this.paywayForm.pendingBonusesCount) {
                this.refuseAllPendingBonuses(data, paywayFormData, payway);
            } else {
                this.paywayForm.isLoading = false;
                this.paywayForm.pendingBonusesCount = pendingBonuses.length;
            }
        } else {
            this.appState.api.withdraw(payway, data, this.onTryWithdraw.bind(this, paywayFormData, payway));
        }
    }

    @action.bound
    onBonusesLoadedWithPage(codes: bonuses.IBonusCode[]) {
        var withdrawalsInspector = codes.find(code => code.meta && code.meta.bonus_type === BonusType.WITHDRAWALS_INSPECTOR);
        if (withdrawalsInspector && withdrawalsInspector.meta && processBoolean(BonusMeta.WI_ACTIVATED, withdrawalsInspector.meta)) {
            const minSpins = processNumber(BonusMeta.WI_MIN_SPINS, withdrawalsInspector.meta) || 0;
            const currentSpins = processNumber(BonusMeta.WI_CURRENT_SPINS, withdrawalsInspector.meta) || 0;
            const minWithdrawal = processLong(BonusMeta.WI_MIN_WITHDRAWAL, withdrawalsInspector.meta) || Long.ZERO;
            const maxWithdrawal = processLong(BonusMeta.WI_MAX_WITHDRAWAL, withdrawalsInspector.meta) || Long.ZERO;
            const timeUntilNextWithdrawal = processNumber(BonusMeta.WI_TIME_UNTIL_NEXT_WITHDRAWAL, withdrawalsInspector.meta) || 0;

            const minWithdrawalFormatted = minWithdrawal.isZero()
                ? null
                : formatCurrencyAmount(minWithdrawal, this.appState.user.getRealBalanceCurrency());
            const maxWithdrawalFormatted = maxWithdrawal.isZero()
                ? null
                : formatCurrencyAmount(maxWithdrawal, this.appState.user.getRealBalanceCurrency());
            const formattedTimeUntilNextWithdrawal = millisecondsToHoursString(this.appState, timeUntilNextWithdrawal);
            this.withdrawalInspector = {
                spinsLeft: Math.max(0, minSpins - currentSpins),
                minWithdrawal: minWithdrawalFormatted,
                maxWithdrawal: maxWithdrawalFormatted,
                timeUntilNextWithdrawal: formattedTimeUntilNextWithdrawal,
            };
        }
    }

    refuseAllPendingBonuses(data: WithdrawalRequestData, paywayFormData: PaywayFormData, payway: Payway) {
        const pending = this.appState.profileBonuses.pendingBonuses;
        if (pending.length) {
            this.appState.api.cancelBonus(pending[0].promoCode, this.onCancelBonus.bind(this, data, paywayFormData, payway));
        } else {
            this.appState.api.withdraw(payway, data, this.onTryWithdraw.bind(this, paywayFormData, payway));
        }
    }

    onCancelBonus(data: WithdrawalRequestData, paywayFormData: PaywayFormData, payway: Payway) {
        this.appState.profileBonuses.tryLoadBonusesData(this.refuseAllPendingBonuses.bind(this, data, paywayFormData, payway));
    }

    onTryWithdraw(paywayFormData: PaywayFormData, payway: Payway, msg: messages.IServerResponse, act: messages.IClientActionResponse) {
        this.appState.processApi(msg, act);

        const paywayForm = this.paywayForm;
        paywayForm.isLoading = false;

        if (act.withdraw && act.withdraw.ok) {
            const provider = payway.getProvider();
            paywayForm.reset(payway);
            if (provider.disablePaywayAfterSuccess()) {
                payway.setDisabled(true);
                this.appState.router.push(this.appState.l('/withdraw'));
            }
            this.appState.billingHistory.loadHistory();
            
            if (provider.showSuccessWithdrawNotification()) {
                this.appState.notificator!.addNotification({
                    level: NotificationItemLevel.SUCCESS,
                    message: this.appState.t('ui-withdrawal-created')
                });
            }
            provider.onSuccessWithdrawalResponse(this, payway, paywayFormData, paywayForm, act.withdraw);
        } else {
            const error = act.withdraw ? act.withdraw.error : null;
            if (error && error === wallet.WithdrawResponse.WithdrawError.bonuses_unplayed) {
                paywayForm.bonusError = true;
            }
            paywayForm.apiError = this.appState.t(getWithdrawalError(error, payway.getProvider().type));
        }
    }

    @computed
    get activeBonusWarning() {
        let warning = undefined;
        this.appState.userBonusesStore.codes.forEach(bonus => {
            if (bonus.meta?.bonus_type && bonus.meta?.bonus_type === BonusType.LEVER_BONUS_BY_CODE &&
                bonus.meta?.allowed_withdrawal && bonus.currency) {
                const allowedWithdrawal = formatCurrencyAmount(Long.fromNumber(Number(bonus.meta.allowed_withdrawal)), bonus.currency);
                if (allowedWithdrawal) {
                    warning = this.appState.t('allowed-withdrawal-bonus-disclaimer', {allowedWithdrawal: allowedWithdrawal});
                }
            }
        })
        return warning;
    }
    // endregion
}
