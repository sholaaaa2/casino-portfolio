import Payway from './Payway';
import AppState from '../AppState';
import Long from 'long';
import PSProvider from './providers/BaseProvider';
import {NString} from '../meta';
import PaywayField from './fields/PaywayField';
import {commons} from '../../api/proto';
import {computed} from 'mobx';

export const BADGE_TAGS = ['popular', 'new', 'recommended'];

export interface GroupSelectValue {
    currencyId: Long;
    paywayName: string;
    currencyInfo: commons.ICurrency;
}

export default class PaywaysGroup {
    appState: AppState;
    payways: Payway[] = [];
    basePaywayUrl: string;

    constructor(appState: AppState, basePaywayUrl: string) {
        this.appState = appState;
        this.basePaywayUrl = basePaywayUrl;
    }

    @computed
    get defaultPayway(): Payway {
        const paywayByCurrency = this.payways.find(
            (p: Payway) =>
                !!p.currencies.find(
                    (c: commons.ICurrency) =>
                        c.id && c.id.equals(this.appState.userInfo.balance.currencyId!),
                ),
        );
        return paywayByCurrency ? paywayByCurrency : this.payways[0];
    }

    @computed
    get className(): string {
        const classNames = [];
        if (this.defaultPayway.isDisabled()) {
            classNames.push('payways-list-item-disabled');
        }
        if ((this.defaultPayway.getId() === "btcpayserver" || this.defaultPayway.getGroup() === "btcpayserver_manual" || this.defaultPayway.getId() === "btcpayserver_paypal") && this.defaultPayway.isRefill) {
            classNames.push('payways-list-item--btcpayserver');
        }
        return classNames.join(' ');
    }

    getId(): string {
        return this.getDefaultPayway().getId();
    }

    getName(): string {
        return this.getDefaultPayway().getName();
    }

    getGroupName(): string {
        const defaultPayway = this.getDefaultPayway();
        return defaultPayway.getProvider().getGroupName(defaultPayway.getId()) || this.getName();
    }

    getTranslatedName(): string {
        return this.getDefaultPayway().getTranslatedName();
    }

    getLogo(): string {
        return this.getDefaultPayway().getLogo();
    }

    getCategory(): string {
        return this.getDefaultPayway().getCategory();
    }

    getGroup(): NString {
        return this.getDefaultPayway().getGroup();
    }

    getFields(): PaywayField[] {
        return this.getDefaultPayway().fields;
    }

    getMinAmount(): Long | null | undefined {
        return this.getDefaultPayway().getMinAmount();
    }

    getMaxAmount(): Long | null | undefined {
        return this.getDefaultPayway().getMaxAmount();
    }

    getProvider(): PSProvider {
        return this.getDefaultPayway().getProvider();
    }

    hasMinAmount(): boolean {
        return this.getDefaultPayway().hasMinAmount();
    }

    hasMaxAmount(): boolean {
        return this.getDefaultPayway().hasMaxAmount();
    }

    formatMinAmount(withCurrency?: boolean): string {
        return this.getDefaultPayway().formatMinAmount(withCurrency);
    }

    formatMaxAmount(withCurrency?: boolean): string {
        return this.getDefaultPayway().formatMaxAmount(withCurrency);
    }

    isDisabled(): boolean {
        return this.getDefaultPayway().isDisabled();
    }

    formatCurrencyCode(): string {
        return this.getDefaultPayway().formatCurrencyCode();
    }

    isCurrencySelectRequired(): boolean {
        return !!this.getCurrencySelectValues().length;
    }

    getCurrencySelectValues(): GroupSelectValue[] {
        const result: GroupSelectValue[] = [];
        for (let payway of this.payways) {
            for (let currency of payway.currencies) {
                if (currency.id && !result.find((c: GroupSelectValue) => c.currencyId.equals(currency.id!))) {
                    result.push({
                        currencyId: currency.id,
                        paywayName: payway.getTranslatedName(),
                        currencyInfo: currency
                    });
                }
            }
        }
        return result;
    }

    isPlasticCardSelectRequired(): boolean {
        return this.getPlasticCardSelectValues().length > 0;
    }

    getPlasticCardSelectValues(): Payway[] {
        const result: Payway[] = [];
        for (let payway of this.payways) {
            if (payway.getId().startsWith('hexopay-card-')) {
                result.push(payway);
            }
        }
        return result;
    }

    getPaywayUrl(currency: Long) {
        const payway = this.getDefaultPaywayByCurrency(currency);
        if (payway) {
            return this.getPaywayUrlById(payway.getId());
        } else {
            return this.appState.l(this.basePaywayUrl);
        }
    }

    getPaywayUrlById(id: string) {
        return this.appState.l(this.basePaywayUrl + '/' + id);
    }

    getDefaultPaywayUrl() {
        const payway = this.getDefaultPayway();
        return this.appState.l(this.basePaywayUrl + '/' + payway.getId());
    }

    getDefaultPayway(): Payway {
        return this.getDefaultPaywayByCurrency(this.appState.userInfo.balance.currencyId!);
    }

    getDefaultPaywayByCurrency(currency: Long): Payway {
        const paywayByCurrency = this.payways.find(
            (p: Payway) => !!(p.currencies.find((c: commons.ICurrency) => c.id && c.id.equals(currency)))
        );
        return paywayByCurrency ? paywayByCurrency : this.payways[0]!;
    }

    get feeTitle(): string {
        return this.getDefaultPayway().uiSettings.feeTitle;
    }

    get tags(): string[] {
        return this.getDefaultPayway().uiSettings.tags;
    }

    get badgeTag(): string | undefined {
        return this.tags.find((tag) => BADGE_TAGS.indexOf(tag) >= 0);
    }
}