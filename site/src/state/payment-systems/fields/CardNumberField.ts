import PaywayField from './PaywayField';
import {action} from 'mobx';
import {NumberFormatValues} from 'react-number-format';
import {BaseStore} from '../BaseStore';
import {validateByFormat} from '../../utils/BrowserUtils';
import {validateCreditCardLuhn} from '../../utils/validateCreditCardLuhn';

export class CardNumberField extends PaywayField {
    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.value;

        if (!value) {
            this.setError(this.appState.t('error-card-number-required'));
        } else if (!validateByFormat(value, this.format)) {
            this.setError(this.appState.t('error-card-number-invalid'));
        } else if (!validateCreditCardLuhn(value)) {
            this.setError(this.appState.t('error-card-number-incorrect'));
        } else if (this.getCustomErrors(value)) {
            this.setError(this.getCustomErrors(value));
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    @action.bound
    onNumberValueChange(values: NumberFormatValues) {
        this.paywayForm.onFieldValueChange(this);
        this.value = values.value;
        if (this.value.length === 16) {
            const valid = this.fillInFieldsOrSetError();
            if (valid) {
                setTimeout(() => {
                    this.focusToNext();
                }, 200);
            }
        }
    }
}
