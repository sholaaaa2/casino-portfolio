import PaywayField, {PaywayFieldOption, PaywayFieldType} from './PaywayField';
import AppState from '../../AppState';
import Payway from '../Payway';
import {NString} from '../../meta';
import {wallet} from '../../../api/proto';
import {PaywayFileUpload} from '../PaywayFileUpload';
import {action, computed} from 'mobx';
import {BaseStore} from '../BaseStore';

export class ImageUploadField extends PaywayField {
    constructor(
        public appState: AppState,
        public payway: Payway,
        public id: string,
        public type: PaywayFieldType,
        public format: NString,
        public title: NString,
        public placeholder: NString,
        public options: PaywayFieldOption[] | null,
        value: NString,
        public ibanCode: NString,
        field: wallet.IPaywayField,
    ) {
        super(
            appState,
            payway,
            id,
            type,
            format,
            title,
            placeholder,
            options,
            value,
            ibanCode,
            field,
        );
        this.payway.fileUpload = new PaywayFileUpload();
    }

    @computed
    get isValid() {
        return (
            !!this.field.isOptional ||
            (!!this.payway.fileUpload?.preparedFiles &&
                this.payway.fileUpload.preparedFiles.length > 0)
        );
    }

    @action.bound
    fillInFieldsOrSetError(store: BaseStore): boolean {
        return true;
    }
}
