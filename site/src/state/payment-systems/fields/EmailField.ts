import PaywayField from './PaywayField';
import {action} from 'mobx';
import {validateByFormat} from '../../utils/BrowserUtils';
import {BaseStore} from '../BaseStore';

export class EmailField extends PaywayField {
    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const email = this.getValue();
        if (!email) {
            this.setError(this.appState.t('error-email-required'));
        } else if (!validateByFormat(email, this.format)) {
            this.setError(this.appState.t('error-email-invalid'));
        } else if (store) {
            this.setField(this.id, email, store);
        }

        return !this.error;
    }

    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.value = e.target.value;
        this.payway.store.paywayForm.onFieldValueChange(this);
    }
}
