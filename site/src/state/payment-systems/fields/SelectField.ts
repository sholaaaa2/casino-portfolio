import PaywayField from './PaywayField';
import {action, computed} from 'mobx';
import {BaseStore} from '../BaseStore';
import {getSelectValue} from '../../utils/BrowserUtils';

export default class SelectField extends PaywayField {
    @computed
    get isValid(): boolean {
        return true;
    }

    @action
    fillInFieldsOrSetError(store: BaseStore): boolean {
        const value = getSelectValue(this.appState, this.getInputId());

        if (value === 'undefined' && !this.field.isOptional) {
            this.error = this.appState.t('error-user-fields-error');
        } else {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }
}