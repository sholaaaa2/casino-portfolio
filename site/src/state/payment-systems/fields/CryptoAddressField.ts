import PaywayField from './PaywayField';
import {computed} from 'mobx';

export class CryptoAddressField extends PaywayField {
    @computed
    get isValid(): boolean {
        return true;
    }
}
