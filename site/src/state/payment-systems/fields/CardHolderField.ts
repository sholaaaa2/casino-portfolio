import PaywayField from './PaywayField';
import { action, observable } from 'mobx';
import { validateByFormat } from '../../utils/BrowserUtils';
import { BaseStore } from '../BaseStore';

export class CardHolderField extends PaywayField {
    @observable inputValue = '';

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.value;

        if (!value) {
            this.setError(this.appState.t('error-card-holder-required'));
        } else if (!validateByFormat(value, this.format)) {
            this.setError(this.appState.t('error-card-holder-invalid'));
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        const value = e.target.value;
        if (!value || /^[a-zA-Z ]+$/.test(value)) {
            this.inputValue = value;
            this.value = value.toUpperCase();
            this.setField(this.id, value, this.payway.store);
            this.payway.store.paywayForm.onFieldValueChange(this);
        }
    }

    @action
    setValue = (value: any) => {
        this.value = value
        this.inputValue = value.toUpperCase();
    };
}
