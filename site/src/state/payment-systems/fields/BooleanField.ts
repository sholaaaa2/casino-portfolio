import PaywayField from './PaywayField';
import {action} from 'mobx';

export class BooleanField extends PaywayField {
    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.value = this.value === 'true' ? 'false' : 'true';
        this.payway.store.paywayForm.onFieldValueChange(this);
    }
}
