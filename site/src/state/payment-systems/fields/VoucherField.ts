import PaywayField, {PaywayFieldType} from './PaywayField';
import {action} from 'mobx';
import {validateByFormat} from '../../utils/BrowserUtils';
import {BaseStore} from '../BaseStore';

export default class VoucherField extends PaywayField {
    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const voucher = this.getValue();

        if (this.field.isOptional) {
            this.setField(this.id, voucher, store);
            return true;
        }

        if (!voucher) {
            this.setError(this.appState.t('error-refill-vaucher-number-required'));
        } else if (!validateByFormat(voucher, this.format)) {
            this.setError(this.appState.t('error-invalid-format'));
        } else if (store) {
            this.setField(this.id, voucher, store);
            this.setField(PaywayFieldType.VOUCHER, voucher, store);
        }
        return !this.error;
    }
}