import PaywayField, {PaywayFieldOption, PaywayFieldType} from './PaywayField';
import {formatCurrencyAmount, convertAmountToApi} from '../../utils/Money';
import Long from 'long';
import {action, computed} from 'mobx';
import {validateByFormat} from '../../utils/BrowserUtils';
import {BaseStore} from '../BaseStore';
import WithdrawalStore from '../WithdrawalStore';
import RefillStore from '../RefillStore';
import {wallet} from '../../../api/proto';
import AppState from '../../AppState';
import Payway from '../Payway';
import { NString } from '../../meta';

export default class AmountField extends PaywayField {
    constructor(
        public appState: AppState,
        public payway: Payway,
        public id: string,
        public type: PaywayFieldType,
        public format: NString,
        public title: NString,
        public placeholder: NString,
        public options: PaywayFieldOption[] | null,
        value: NString,
        public ibanCode: NString,
        field: wallet.PaywayField
    ) {
        super(appState, payway, id, type, format, title, placeholder, options, value, ibanCode, field);
        if (
            payway.provider.type === wallet.WithdrawProvider.BTCPAYSERVER &&
            !payway.isRefill &&
            !!payway.minAmount
        ) {
            this.placeholder = appState.t('enter-amount-min', {
                amount: appState.money.formatCurrencyAmount(
                    payway.minAmount,
                    appState.user.getRealBalanceCurrencyId(),
                ),
            });
        }
    }

    @computed
    get isValid(): boolean {
        return !this.validate(this.value);
    }

    get showCommission() {
        return this.appState.appSettings.billing?.showCommission === true && this.store.basePaywayUrl === '/refill';
    }

    get convertedAmount() {
        const sourceAmount = this.getValue();
        return this.appState.userInfo.currency ? convertAmountToApi(sourceAmount, this.appState.userInfo.currency) : null;
    }

    validate = (value?: string): string | null => {
        let error = null;
        const sourceAmount = value ?? this.getValue();
        const isWithdrawal = this.store instanceof WithdrawalStore;

        if (this.store.selectedPayway?.getProvider()?.type && this.store.selectedPayway?.getProvider()?.type === wallet.WithdrawProvider.SWIFTPAYP2P && this.store instanceof RefillStore) {
            return error;
        }

        if (this.convertedAmount == null) {
            error = this.appState.t('error-invalid-format');
        } else if (!sourceAmount && (!this.convertedAmount || this.convertedAmount.lessThanOrEqual(Long.ZERO))) {
            error = this.appState.t('error-refill-incorrect-amount');
        } else if (!validateByFormat(sourceAmount, this.format)) {
            error = this.appState.t('error-invalid-format');
        } else if (this.payway.hasMinAmount() && this.payway.minAmount && this.convertedAmount?.lessThan(this.payway.minAmount)) {
            error = this.appState.t('error-min-amount', { amount: formatCurrencyAmount(this.payway.minAmount, this.appState.user.getRealBalanceCurrency()) });
        } else if (this.payway.hasMaxAmount() && this.payway.maxAmount && this.convertedAmount?.greaterThan(this.payway.maxAmount)) {
            error = this.appState.t(isWithdrawal ? 'error-max-amount-withdrawal' : 'error-max-amount-refill', { amount: formatCurrencyAmount(this.payway.maxAmount, this.appState.user.getRealBalanceCurrency()) });
        } else if (Long.fromString(sourceAmount).isZero()) {
            error = this.appState.t('error-zero-amount');
        } else if (isWithdrawal) {
            const balance = this.appState.user.getFirstRealBalance();
            const available = balance && balance.availableValue && balance.availableValue.greaterThan(Long.ZERO) ? balance.availableValue : null;
            if (!available || this.convertedAmount?.greaterThan(available)) {
                error = this.appState.t('error-withdrawal-balance-too-low');
            }
        }

        return error;
    };

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        if (store?.selectedPayway?.getProvider()?.type && store.selectedPayway?.getProvider()?.type === wallet.WithdrawProvider.SWIFTPAYP2P && store instanceof RefillStore) {
            this.setField(this.id, 0, store);
            this.setField(PaywayFieldType.AMOUNT, 0, store);
            this.setField(PaywayFieldType.SOURCE_AMOUNT, 0, store);
            return true;
        }

        const error = this.validate();
        this.setError(error);

        if (!error) {
            this.setField(this.id, this.convertedAmount, store);
            this.setField(PaywayFieldType.AMOUNT, this.convertedAmount, store);
            this.setField(PaywayFieldType.SOURCE_AMOUNT, this.getValue(), store);
        }

        return !error;
    }

    @action
    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.paywayForm.onFieldValueChange(this);
        this.value = e.currentTarget.value;

        if (this.showCommission) {
            if (!this.validate()) {
                const sourceAmount = this.getValue();
                this.paywayForm.amountInfo.processAmount(sourceAmount, this.store.selectedCurrency);
            } else {
                this.paywayForm.amountInfo.reset();
            }
        }
        if (this.appState.mobileOSType === 'iOS' && this.inputRef.value.search(',')) {
            this.inputRef.value = this.inputRef.value.replace(',','.');
        }
    };
}
