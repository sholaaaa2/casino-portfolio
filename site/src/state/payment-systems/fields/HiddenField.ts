import PaywayField from './PaywayField';
import {BaseStore} from '../BaseStore';
import {clearInputValue} from '../../utils/BrowserUtils';
import {action, computed} from 'mobx';

export default class HiddenField extends PaywayField {
    @computed
    get isValid(): boolean {
        return true;
    }

    getValue(): any {
        return this.defaultValue
    }

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.getValue();

        if (!value && !this.field.isOptional) {
            this.error = this.appState.t('error-user-fields-error');
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    clearValue() {
        clearInputValue(this.appState, this.getInputId());
    }

    @action
    clearError() {
        this.error = null;
    }
}