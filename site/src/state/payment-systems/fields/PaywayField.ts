import {NString} from '../../meta';
import {clearInputValue, getInputValue, validateByFormat} from '../../utils/BrowserUtils';
import AppState from '../../AppState';
import {action, computed, observable} from 'mobx';
import Payway, { FIRST_STEP_NUMBER } from '../Payway';
import {BaseStore} from '../BaseStore';
import { wallet } from '../../../api/proto';
import { NotificationItemLevel } from '../../components/Notificator';

export enum ErrorStyle {
    DEFAULT,
    RED_AND_UPPERCASE,
}

export enum PaywayFieldType {
    BALANCE_ID = 'balance_id',
    CURRENCY = 'currency',
    AMOUNT = 'amount',
    SOURCE_AMOUNT = 'source_amount',
    PHONE = 'phone',
    ACCOUNT = 'account',
    CARD_NUMBER = 'card_number',
    PASSWORD = 'password',
    VOUCHER = 'voucher',
    CRYPTO_ADDRESS = 'crypto_address',
    COUNTRY_CODE = 'country_code',
    PHONE_CODE = 'phone_code',
    PHONE_WITH_CODE = 'phone_with_code',
    SELECT = 'select',
    IBAN = 'iban',
    HIDDEN = 'hidden',
    MOBILE_DETECT = 'mobile_detect',
    UNIFIED = 'unified',
    IMAGE_UPLOAD = 'image_upload',
    CARD_EXPIRATION_MONTH_YEAR = 'card_expiration_month_year',
    CARD_CVV = 'card_cvv',
    CARD_HOLDER_NAME = 'card_holder_name',
    EMAIL = 'email',
    BOOLEAN = 'boolean',
    CRYPTO_ADDRESS_WITHOUT_QR = 'crypto_address_without_qr',
}

export type PaywayFieldOption = { key: string, value: string };

export default class PaywayField {
    @observable value = "";
    @observable defaultValue = "";
    @observable error: string | null = null;
    @observable errorStyle: ErrorStyle = ErrorStyle.DEFAULT;
    shownOnStep = FIRST_STEP_NUMBER;

    constructor(public appState: AppState,
                public payway: Payway,
                public id: string,
                public type: PaywayFieldType,
                public format: NString,
                public title: NString,
                public placeholder: NString,
                public options: PaywayFieldOption[] | null,
                value: NString,
                public ibanCode: NString,
                public field: wallet.IPaywayField) {
        if (value) {
            this.defaultValue = value;
            this.value = this.defaultValue;
        }
        this.shownOnStep = field.shownOnStep || FIRST_STEP_NUMBER;
    }

    @computed
    get isOptional() {
        return this.field.isOptional;
    }

    @computed
    get isValid(): boolean {
        return !!this.field.isOptional || (!!this.value && validateByFormat(this.value, this.format));
    }

    get store() {
        return this.payway.store;
    }

    get paywayForm() {
        return this.store.paywayForm;
    }

    getInputId(): string {
        return 'payway-field-' + this.id;
    }

    get inputRef(): HTMLInputElement {
        return this.appState.getRef(this.getInputId()) as HTMLInputElement;
    }

    getValue(): any {
        return getInputValue(this.appState, this.getInputId());
    }

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.getValue();

        if (this.field.isOptional) {
            this.setField(this.id, value, store);
            return true;
        }

        if (!value) {
            this.setError(this.appState.t('error-user-fields-error'));
        } else if (!validateByFormat(value, this.format)) {
            this.setError(this.appState.t('error-invalid-format'));
        } else if (this.getCustomErrors(value)) {
            this.setError(this.getCustomErrors(value));
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    @action
    setValue = (value: any) => {
        this.value = value
    };

    @action
    setError = (error: string | null, errorStyle: ErrorStyle = ErrorStyle.DEFAULT) => {
        this.error = error;
        this.errorStyle = errorStyle;
        if (error && this.payway.isMultiStep) {
            this.payway.currentStep = this.shownOnStep;
        }
        
        if (error && this.payway.newFormAvailable && !this.appState.isMobile && this.store.selectedPayway?.getId() === this.payway.getId()) {
            setTimeout(() => {
                if (this.store.selectedPayway?.getId() === this.payway.getId()) {
                    this.appState.notificator?.addNotification({
                        level: NotificationItemLevel.ERROR,
                        message: error,
                        dupMessages: false,
                    });
                }
            }, 200);
        }
    };

    @action
    setField = (id: string, value: any, store?: BaseStore) => {
        if (store instanceof BaseStore) {
            store.paywayForm.setField(id, value);
        }
    };

    clearValue() {
        clearInputValue(this.appState, this.getInputId());
    }

    @action
    clearError() {
        this.error = null;
    }

    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.paywayForm.onFieldValueChange(this);
        this.value = e.currentTarget.value;
    };

    @action.bound
    onBlur() {
        this.fillInFieldsOrSetError(this.payway.store);
    }

    handleFocus = () => {
        this.store.paywayForm.handleFocus();
    }

    focus = () => {
        if (this.inputRef) {
            this.inputRef.focus()
        }
    };

    focusToNext = () => {
        const field = this.store.selectedPayway?.getNextField(this.type);
        if (field) {
            field.focus();
        }
    };

    handleKeyDown = (e: React.KeyboardEvent) => {
        this.store.paywayForm.handleKeyDown(e, this);
    }

    get customizations() {
        return this.payway.providerCustomizations && this.payway.providerCustomizations[this.type] ? this.payway.providerCustomizations[this.type] : null;
    }

    getCustomErrors = (value: string): string | null => {
        if (
            this.customizations &&
            this.customizations.regex &&
            this.customizations.error &&
            !validateByFormat(value, this.customizations.regex)
        ) {
            return this.appState.t(this.customizations.error);
        }
        return null
    };

    @computed
    get errorClass() {
        if (this.errorStyle === ErrorStyle.RED_AND_UPPERCASE) {
            return 'red-and-uppercase';
        }
        return '';
    }
}