import PaywayField from './PaywayField';
import {action} from 'mobx';
import {NumberFormatProps} from 'react-number-format';
import {BaseStore} from '../BaseStore';
import {validateByFormat} from '../../utils/BrowserUtils';

export class CardExpirationMonthYearField extends PaywayField {

    validate = (value: string) => {
        if (!validateByFormat(value, this.format)) {
            return false;
        }

        const month = parseInt(value.substring(0, 2));
        const fullYear = 2000 + parseInt(value.substring(2, 4));

        if (isNaN(month) || isNaN(fullYear) || (fullYear === new Date().getFullYear() && month < (new Date().getMonth() + 1)) || fullYear < new Date().getFullYear()) {
            return false;
        }

        return true;
    };

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.value;

        if (!value) {
            this.setError(this.appState.t('error-card-expiration-required'));
        } else if (!this.validate(value)) {
            this.setError(this.appState.t('error-card-expiration-invalid'));
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    @action.bound
    onValueChange(e: NumberFormatProps) {
        let val = e.value ? e.value.toString() : '';
        if (val.length > 0 && !(val[0] === '0' || val[0] === '1')) {
            val = '0' + val;
        }
        this.value = val;
        this.payway.store.paywayForm.onFieldValueChange(this);

        if (this.value.length === 4) {
            const valid = this.fillInFieldsOrSetError();
            if (valid) {
                setTimeout(() => {
                    this.focusToNext();
                }, 200);
            }
        }
    }

    limit = (val: string, max: string) => {
        if (val.length === 1 && val[0] > max[0]) {
            val = '0' + val;
        }

        if (val.length === 2) {
            if (Number(val) === 0) {
                val = '01';
                //this can happen when user paste number
            } else if (val > max) {
                val = max;
            }
        }

        return val;
    };

    cardExpiry = (val: string) => {
        let month = this.limit(val.substring(0, 2), '12');
        let year = val.substring(2, 4);

        const res = month + (year.length ? ' / ' + year : '');
        return res.length >= 4 ? res : res + this.placeholder?.substr(res.length);
    };
}
