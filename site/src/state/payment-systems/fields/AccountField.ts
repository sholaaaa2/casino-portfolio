import PaywayField from './PaywayField';
import {action} from 'mobx';
import {validateByFormat} from '../../utils/BrowserUtils';
import {BaseStore} from '../BaseStore';

export default class AccountField extends PaywayField {
    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const account = this.getValue();

        if (this.field.isOptional) {
            this.setField(this.id, account, store);
            return true;
        }

        if (!account) {
            this.setError(this.appState.t('error-empty-refill-account'));
        } else if (!validateByFormat(account, this.format)) {
            this.setError(this.appState.t('error-invalid-format'));
        } else if (store) {
            this.setField(this.id, account, store);
        }

        return !this.error;
    }
}