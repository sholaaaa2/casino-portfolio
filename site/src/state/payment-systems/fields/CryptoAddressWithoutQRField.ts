import PaywayField, { ErrorStyle } from './PaywayField';
import {action, computed, observable, runInAction} from 'mobx';
import copyToClipboard from 'copy-to-clipboard';
import {pasteFromClipboard} from '../../utils/pasteFromClipboard';
import {BaseStore} from '../BaseStore';
import {validateByFormat} from '../../utils/BrowserUtils';
import { wallet } from '../../../api/proto';

export class CryptoAddressWithoutQRField extends PaywayField {
    @observable isCopied = false;

    @computed
    get isValid(): boolean {
        return (
            !!this.field.isOptional || (!!this.value && validateByFormat(this.value, this.format) && !this.addrressIsEqualToRefillWallet)
        );
    }

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        if (this.field.isOptional) {
            this.setField(this.id, this.value, store);
            return true;
        }
        if (!this.value) {
            this.setError(this.appState.t('error-user-fields-error'));
        } else if (!validateByFormat(this.value, this.format)) {
            this.setError(this.appState.t('error-invalid-format'));
        } else if (this.addrressIsEqualToRefillWallet) {
            this.setError(this.appState.t('error-this-address-uses-for-refill'), ErrorStyle.RED_AND_UPPERCASE);
        } else if (store) {
            this.setError(null);
            this.setField(this.id, this.value, store);
        }
        return !this.error;
    }

    @computed
    get addrressIsEqualToRefillWallet() {
        return this.payway?.provider?.type &&
            this.payway.provider.type === wallet.WithdrawProvider.BTCPAYSERVER &&
            this.appState.appSettings.billing?.btcWithdrawalAddressCheck &&
            !this.payway.isRefill &&
            this.appState.refillStore.getCryptoWallets().includes(this.value)
    }

    @action.bound
    copy() {
        if (this.value) {
            copyToClipboard(this.value);
            this.isCopied = true;
            window.setTimeout(() => {
                runInAction(() => {
                    this.isCopied = false;
                });
            }, 1000);
        }
    }

    paste = async () => {
        const value = await pasteFromClipboard(this.appState);
        if (value) {
            runInAction(() => {
                this.value = value;
                this.fillInFieldsOrSetError(this.payway.store);
            });
        }
    };

    @action.bound
    clear() {
        this.value = '';
        this.fillInFieldsOrSetError(this.payway.store);
    }
}
