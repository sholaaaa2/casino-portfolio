import PaywayField, {PaywayFieldType} from './PaywayField';
import {action, computed} from 'mobx';
import {validateByFormat} from '../../utils/BrowserUtils';
import {BaseStore} from '../BaseStore';

export default class PasswordField extends PaywayField {
    @computed
    get isValid(): boolean {
        return true;
    }

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const password = this.getValue();

        if (this.field.isOptional) {
            this.setField(this.id, password, store);
            return true;
        }

        if (!password) {
            this.setError(this.appState.t('error-login-password-required'));
        } else if (!validateByFormat(password, this.format)) {
            this.setError(this.appState.t('error-invalid-format'));
        } else if (store) {
            this.setField(this.id, password, store);
            this.setField(PaywayFieldType.PASSWORD, password, store);
        }
        return !this.error;
    }
}