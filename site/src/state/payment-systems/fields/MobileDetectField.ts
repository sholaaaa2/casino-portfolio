import PaywayField from './PaywayField';
import {BaseStore} from '../BaseStore';
import { computed } from 'mobx';

const MobileDetect = require('mobile-detect');
const mobileDetect = new MobileDetect(window.navigator.userAgent);

export default class MobileDetectField extends PaywayField {
    @computed
    get isValid(): boolean {
        return true;
    }

    fillInFieldsOrSetError(store?: BaseStore): boolean {
        if(store) {
            this.setField(this.id, mobileDetect.mobile() ? '1' : '0', store);
        }
        return true;
    }
}