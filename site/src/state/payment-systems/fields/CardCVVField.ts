import PaywayField from './PaywayField';
import {action} from 'mobx';
import {BaseStore} from '../BaseStore';
import {validateByFormat} from '../../utils/BrowserUtils';

export class CardCVVField extends PaywayField {
    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const value = this.value;

        if (!value) {
            this.setError(this.appState.t('error-card-cvv-required'));
        } else if (!validateByFormat(value, this.format)) {
            this.setError(this.appState.t('error-card-cvv-invalid'));
        } else if (store) {
            this.setField(this.id, value, store);
        }
        return !this.error;
    }

    @action.bound
    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        const cvv = e.target.value;
        if (!cvv || /^\d+$/.test(cvv)) {
            this.value = cvv;
            this.setField(this.id, cvv, this.payway.store);
            this.payway.store.paywayForm.onFieldValueChange(this);

            const valid = validateByFormat(cvv, this.format);
            if (valid) {
                setTimeout(() => {
                    this.focusToNext();
                }, 200);
            }
        }
    }
}
