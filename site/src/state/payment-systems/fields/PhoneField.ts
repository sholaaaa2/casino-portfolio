import PaywayField, {PaywayFieldOption, PaywayFieldType} from './PaywayField';
import {clearInputValue, getCleanedInputValue, validateByFormat} from '../../utils/BrowserUtils';
import {NString, PhoneInfo} from '../../meta';
import {getPhoneCodeByCountry} from '../../../api/utils';
import {action, computed, observable} from 'mobx';
import {BaseStore} from '../BaseStore';
import {getPhoneMask} from '../../utils/getPhoneMask';
import {NumberFormatProps} from 'react-number-format';
import {parsePhoneNumberFromString} from 'libphonenumber-js';
import {site, wallet} from '../../../api/proto';
import AppState from '../../AppState';
import Payway from '../Payway';

export default class PhoneField extends PaywayField {
    @observable phoneCode = '';
    @observable phoneNumber = '';
    @observable _countryCode = '';

    constructor(
        public appState: AppState,
        public payway: Payway,
        public id: string,
        public type: PaywayFieldType,
        public format: NString,
        public title: NString,
        public placeholder: NString,
        public options: PaywayFieldOption[] | null,
        value: NString,
        public ibanCode: NString,
        public field: wallet.IPaywayField,
    ) {
        super(
            appState,
            payway,
            id,
            type,
            format,
            title,
            placeholder,
            options,
            value,
            ibanCode,
            field,
        );
        if (value) {
            const parsedPhone = parsePhoneNumberFromString(value);
            if (parsedPhone) {
                this.phoneNumber = parsedPhone.nationalNumber.toString();
                this._countryCode = parsedPhone.country as string;
            }
        }
    }

    @computed
    get isValid(): boolean {
        return true;
    }

    getPhoneNumberInputId(): string {
        return 'payway-field-phone-' + this.id;
    }

    getCountryCodeInputId(): string {
        return 'payway-field-country-code-' + this.id;
    }

    getPhoneNumberValue(): string {
        return getCleanedInputValue(this.appState, this.getPhoneNumberInputId());
    }

    clearPhoneNumberValue(): void {
        clearInputValue(this.appState, this.getPhoneNumberInputId());
    }

    getCountryCodeValue(): string {
        return getCleanedInputValue(this.appState, this.getCountryCodeInputId());
    }

    clearCountryCodeValue(): void {
        // todo: find out how to clear
    }

    @computed
    get countries() {
        if (
            this.countryCode &&
            this.appState.site.countries.some((c) => c.code === this.countryCode)
        ) {
            return this.appState.site.countries;
        }
        if (this.countryCode) {
            return [
                new site.CountryInfo({
                    code: this.countryCode,
                    phoneCode: this.phoneCode ? parseFloat(this.phoneCode) : undefined,
                }),
                ...this.appState.site.countries,
            ];
        }
        return this.appState.site.countries;
    }

    @computed
    get countryCode(): string | null {
        return this._countryCode || this.appState.site.getDefaultCountry();
    }

    @action
    setCountryCode = (code: string) => {
        this._countryCode = code;
        this.setField(this.id + '-country-code', code, this.store);
        this.setField(PaywayFieldType.COUNTRY_CODE, code, this.store);
    };

    @action
    setPhoneNumber = (phoneNumber: string) => {
        this.phoneNumber = phoneNumber;
        this.setField(this.id, phoneNumber, this.store);
        this.setField(PaywayFieldType.PHONE, phoneNumber, this.store);
    };

    @action
    setPhoneCode = (phoneCode: string) => {
        this.phoneCode = phoneCode;
        this.setField(this.id + '-code', phoneCode, this.store);
        this.setField(PaywayFieldType.PHONE_CODE, phoneCode, this.store);
    };

    @action
    setPhoneWithCode = (phoneNumberWithCode: string) => {
        this.value = phoneNumberWithCode;
        this.setField(this.id + '-with-code', phoneNumberWithCode, this.store);
        this.setField(PaywayFieldType.PHONE_WITH_CODE, phoneNumberWithCode, this.store);
    };

    @computed
    get phoneMask(): string | undefined {
        return (
            (this.countryCode ? getPhoneMask(this.countryCode, this.countries) : undefined) ||
            undefined
        );
    }

    @computed
    get phoneLength(): number {
        return this.phoneMask ? this.phoneMask.split('#').length - 1 : 0;
    }

    @computed
    get phonePlaceholder(): string | undefined {
        return this.phoneMask
            ? this.phoneMask.split('#').join('_')
            : this.appState.t('ui-phone-number');
    }

    getValue(): PhoneInfo | null {
        const phoneCode = this.options
            ? this.countryCode
            : getPhoneCodeByCountry(this.countryCode ?? '', this.appState.siteConfig!.countries!);

        return !!this.phoneNumber && !!this.countryCode && !!phoneCode
            ? {
                  phoneCode: phoneCode.toString(),
                  phoneNumber: this.phoneNumber,
                  phoneNumberWithCode:
                      phoneCode[0] !== '+'
                          ? '+' + phoneCode + this.phoneNumber
                          : phoneCode + this.phoneNumber,
                  countryCode: this.countryCode,
              }
            : null;
    }

    @action.bound
    fillInFieldsOrSetError(store?: BaseStore): boolean {
        const phoneInfo = this.getValue();
        let error: string | undefined = undefined;

        if (!phoneInfo) {
            error = this.appState.t('error-refill-phone-number-required');
        } else if (
            !validateByFormat(phoneInfo.phoneNumber, this.format) &&
            !this.field.isOptional
        ) {
            error = this.appState.t('error-invalid-format');
        } else {
            this.fillPhoneInfoIntoFields(phoneInfo);
        }

        if (!!error && !this.field.isOptional) {
            this.setError(error);
            return false;
        }

        return true;
    }

    fillPhoneInfoIntoFields = (phoneInfo: PhoneInfo): void => {
        this.setPhoneNumber(phoneInfo.phoneNumber);
        this.setPhoneCode(phoneInfo.phoneCode);
        this.setCountryCode(phoneInfo.countryCode);
        this.setPhoneWithCode(phoneInfo.phoneNumberWithCode);
    };

    clearValue() {
        this.clearPhoneNumberValue();
        this.clearCountryCodeValue();
    }

    @action
    onChangeCountryCode = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.store.paywayForm.onFieldValueChange(this);

        const countryCode = e.target.value;
        this.setCountryCode(countryCode);

        const country = this.countries.find((c) => c.code === countryCode);
        if (country) {
            this.setPhoneCode(country.phoneCode?.toString() || '');
        }
    };

    @action
    onChangePhone = (e: NumberFormatProps) => {
        this.store.paywayForm.onFieldValueChange(this);
        this.setPhoneNumber(e.value?.toString() || '');
    };

    @action
    setValue = (phone: string) => {
        const parsedPhone = parsePhoneNumberFromString(phone);
        if (parsedPhone) {
            this.setPhoneNumber(parsedPhone.nationalNumber.toString());
            this.setPhoneCode(parsedPhone.countryCallingCode.toString());
            this.setCountryCode(parsedPhone.country || '-');
            this.setPhoneWithCode(phone);
        }
    };

    @action
    onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.paywayForm.onFieldValueChange(this);
        this.value = e.currentTarget.value;
    };
}
