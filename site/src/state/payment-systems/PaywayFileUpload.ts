import {action, observable} from 'mobx';
import React from 'react';
import {users, wallet} from '../../api/proto';

interface IFileError {
    fileName?: string;
    status: users.UserDocumentsResponse.Status;
}

export class PreparedFile implements wallet.IAttachDataRequest {
    constructor(
        public name: string,
        public bytes: Uint8Array,
        private fileUpload: PaywayFileUpload,
    ) {}

    @action.bound
    remove() {
        this.fileUpload.preparedFiles.remove(this);
    }
}

export class PaywayFileUpload {
    readonly ACCEPTED_TYPES = ['image/png', 'image/jpeg'];
    readonly FILE_LIMIT = 5;
    readonly FILE_SIZE_LIMIT = 10000000;
    fileInputRef = React.createRef();
    @observable filesToUpload?: FileList;
    @observable isUploading = false;
    @observable isDraggingFiles = false;
    @observable errors: IFileError[] = [];
    preparedFiles = observable<PreparedFile>([], {deep: false});

    @action.bound
    showFileSelectionDialog() {
        if (this.isUploading === false) {
            const fileInput = this.fileInputRef.current as HTMLInputElement;
            if (fileInput) {
                fileInput.value = '';
                fileInput.click();
            }
        }
    }

    @action.bound
    onDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
    };

    @action.bound
    onDragEnter = (event: React.DragEvent<HTMLDivElement>) => {
        this.isDraggingFiles = true;
    };

    @action.bound
    onDragLeave = (event: React.DragEvent<HTMLDivElement>) => {
        this.isDraggingFiles = false;
    };

    @action.bound
    fileDropHandler(event: React.DragEvent<HTMLDivElement>) {
        event.preventDefault();
        this.processFiles(event.dataTransfer.files);
    }

    @action.bound
    handleSelectedFile(event: React.ChangeEvent<HTMLInputElement>) {
        this.processFiles(event.target.files);
    }

    @action
    processFiles(files: FileList | null) {
        if (files && files.length) {
            this.clearErrors();
            this.filesToUpload = files;
            this.prepareFilesForUpload();
        }
    }

    @action
    validateSelectedFile(file: File): boolean {
        if (file.size > this.FILE_SIZE_LIMIT) {
            this.errors.push({
                fileName: file.name,
                status: users.UserDocumentsResponse.Status.FILE_TOO_BIG,
            });
            return false;
        } else if (!this.ACCEPTED_TYPES.includes(file.type)) {
            this.errors.push({
                fileName: file.name,
                status: users.UserDocumentsResponse.Status.FAILED,
            });
            return false;
        }
        return true;
    }

    @action
    clearErrors() {
        this.errors = [];
    }

    @action.bound
    errorUploadingFailed() {
        this.isUploading = false;
        this.errors.push({status: users.UserDocumentsResponse.Status.FAILED});
    }

    @action.bound
    prepareFilesForUpload() {
        if (this.filesToUpload) {
            [].forEach.call(this.filesToUpload, (file: File) => {
                const reader = new FileReader();
                reader.onload = this.addPreparedFile.bind(this, file);
                reader.onerror = this.errorUploadingFailed;
                reader.readAsArrayBuffer(file);
            });
        } else {
            this.errorUploadingFailed();
        }
    }

    @action.bound
    addPreparedFile(file: File, event: ProgressEvent<FileReader>) {
        if (event.target && event.target.result) {
            const fileContent = new Uint8Array(event.target.result as ArrayBuffer);
            if (
                this.validateSelectedFile(file) &&
                this.preparedFiles.every((preparedFile) => preparedFile.name !== file.name)
            ) {
                const preparedFile = new PreparedFile(file.name, fileContent, this);
                const numberOfFiles = this.preparedFiles.length;
                if (numberOfFiles < this.FILE_LIMIT) {
                    this.preparedFiles.push(preparedFile);
                }
            }
        } else {
            this.errorUploadingFailed();
        }
    }
}
