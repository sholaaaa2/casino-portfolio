export function updateFadeClasses(flag: boolean, el: HTMLElement, callbackFnAfterShow?: (() => void) | null, callbackFnAfterHide?: (() => void) | null) {
    const html = document.getElementsByTagName('html')[0] as HTMLElement;

    if (flag) {
        html.classList.add('lock-position');
        el.classList.add('in');
        setTimeout(function () {
            el.classList.add('show');
            if (callbackFnAfterShow) {
                callbackFnAfterShow();
            }
        }, 0);
    } else if (el.classList.contains('show') && el.classList.contains('in')) {
        html.classList.remove('lock-position');
        el.classList.remove('show');
        setTimeout(function () {
            el.classList.remove('in');
            if (callbackFnAfterHide) {
                callbackFnAfterHide();
            }
        }, 250);
    }
}