import {PageProps} from '../App';
import {messages} from '../api/proto';


function onActivityUpdate(msg: messages.IServerResponse, act: messages.IClientActionResponse) {
    if (act.activityUpdate?.ok) {
        console.debug('activityUpdate success')
    }
}

export function initUrlTracking(props: PageProps) {
    if (props.history?.location?.pathname) {
        props.appState.api.activityUpdateRequest(props.history.location.pathname, onActivityUpdate);
    }
    props.history.listen((location, action) => {
        if (location.pathname) {
            props.appState.api.activityUpdateRequest(location.pathname, onActivityUpdate);
        }
    });
}
