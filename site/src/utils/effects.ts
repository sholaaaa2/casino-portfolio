import {getViewportSize, isMobile} from '../state/utils/BrowserUtils';

const confetti = require('canvas-confetti');

const colors = ['#193f6a', '#da8afb'];
const bonusNotificationConfettiColors = ['#193f6a', '#da8afb', '#fff'];
const jackpotNotificationConfettiColors = ['#f5de73', '#de0f12', '#fff'];

const volleyConfetti = 3;
const durations = volleyConfetti * 1000;
const CONTENT_MAX_WIDTH = 1365;
const NOTIFICATION_CARD_WIDTH = 320;

export const runConfetti = function () {
    const end = Date.now() + durations;

    (function frame() {
        confetti({
            particleCount: 4,
            angle: 60,
            spread: 65,
            origin: {
                x: 0
            },
            colors: colors
        });
        confetti({
            particleCount: 4,
            angle: 120,
            spread: 65,
            origin: {
                x: 1
            },
            colors: colors
        });

        if (Date.now() < end) {
            requestAnimationFrame(frame);
        }
    }());
};

export const runFireworks = function () {
    const end = Date.now() + durations;

    const interval: NodeJS.Timer = setInterval(function () {
        if (Date.now() > end) {
            return clearInterval(interval);
        }

        confetti({
            startVelocity: 30,
            spread: 360,
            ticks: 60,
            origin: {
                x: Math.random(),
                // since they fall down, start a bit higher than random
                y: Math.random() - 0.2
            },
            colors: colors
        });
    }, 150);
};

export const runRandomEffect = function () {
    const rand = Math.floor(Math.random() * 2);
    switch (rand) {
        case 0:
            runConfetti();
            break;
        case 1:
            runFireworks();
            break;
        default:
            runConfetti();
    }
};

export const runBonusConfetti = function () {
    const viewport = getViewportSize();
    const pixOffset = Math.max(viewport.width - CONTENT_MAX_WIDTH, 0) / 2;
    const ratioOffset = pixOffset / viewport.width;
    const cardRatioOffset = NOTIFICATION_CARD_WIDTH / viewport.width;
    for (let i = 1; i <= volleyConfetti; i++) {
        setTimeout(() => {
            confetti({
                angle: r(155, 175),
                spread: r(50, 70),
                particleCount: r(75, 120),
                origin: {
                    x: 1 - ratioOffset + cardRatioOffset,
                    y: 0.1
                },
                colors: [bonusNotificationConfettiColors[i - 1]]
            });
        }, i * 200);
    }
};

export const runJackpotConfettiForNotification = function () {
    if (!isMobile()) {
        const viewport = getViewportSize();
        const pixOffset = Math.max(viewport.width - CONTENT_MAX_WIDTH, 0) / 2;
        const ratioOffset = pixOffset / viewport.width;
        const cardRatioOffset = NOTIFICATION_CARD_WIDTH / viewport.width;
        for (let i = 1; i <= volleyConfetti; i++) {
            setTimeout(() => {
                confetti({
                    angle: r(155, 175),
                    spread: r(50, 70),
                    particleCount: r(75, 120),
                    origin: {
                        x: 1 - ratioOffset + cardRatioOffset,
                        y: 0.1
                    },
                    colors: [jackpotNotificationConfettiColors[i - 1]]
                });
            }, i * 100);
        }
    }
};

export const runJackpotConfettiForWinner = function () {
    if (!isMobile()) {
        const end = Date.now() + durations;
        const viewport = getViewportSize();
        const isLessMaxContentWidth = viewport.width < CONTENT_MAX_WIDTH;
        const decay = isLessMaxContentWidth ? 0.9 : 0.96;

        (function frame() {
            confetti({
                particleCount: 4,
                angle: 40,
                spread: 50,
                origin: {
                    x: 0
                },
                decay: decay,
                colors: jackpotNotificationConfettiColors
            });
            confetti({
                particleCount: 4,
                angle: 140,
                spread: 50,
                origin: {
                    x: 1
                },
                decay: decay,
                colors: jackpotNotificationConfettiColors
            });

            if (!isLessMaxContentWidth) {
                confetti({
                    particleCount: 4,
                    angle: 40,
                    spread: 50,
                    origin: {
                        x: 0
                    },
                    decay: decay,
                    colors: jackpotNotificationConfettiColors
                });
                confetti({
                    particleCount: 4,
                    angle: 140,
                    spread: 50,
                    origin: {
                        x: 1
                    },
                    decay: decay,
                    colors: jackpotNotificationConfettiColors
                });
            }

            if (Date.now() < end) {
                requestAnimationFrame(frame);
            }
        }());
    }
};

function r(min: number, max: number) {
    return Math.random() * (max - min) + min;
}