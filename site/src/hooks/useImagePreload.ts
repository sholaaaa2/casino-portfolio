import { useState, useEffect } from 'react';

const cache = new Map();

//TODO: retry
export const useImagePreload = (url: string): [string | null, boolean] => {
  const isLoaded = url && cache.has(url);

  const [state, setSate] = useState<{ src: string | null; loading: boolean }>({
    src: isLoaded ? url : null,
    loading: !isLoaded,
  });

  useEffect(() => {
    if (!url) {
      return;
    }

    if (!isLoaded) {
      setSate({ loading: true, src: null });

      let img = new Image();

      const onLoad = () => {
        setSate({ loading: false, src: url });
        cache.set(url, true);

        img.removeEventListener('load', onLoad);
        img.remove();
      };

      img.addEventListener('load', onLoad);
      img.src = url;

      return () => {
        setSate({ loading: false, src: null });
        img.remove();
        img.removeEventListener('load', onLoad);
      };
    } else {
      setSate({ loading: false, src: url });
      return () => {};
    }
  }, [url, isLoaded]);

  return [state.src, state.loading];
};
