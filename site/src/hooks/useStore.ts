import {MobXProviderContext} from 'mobx-react';
import React from 'react';
import AppState from '../state/AppState';

export function useStore(): AppState {
    return React.useContext(MobXProviderContext).appState;
}
