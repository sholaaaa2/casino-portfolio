import { useState, useEffect } from 'react';

export const useClockTimer = (): Date => {
    const [date, setDate] = useState(new Date());

    useEffect(() => {
        const timerId = window.setInterval(() => {
            setDate(new Date());
        }, 1000);
        return () => {
            window.clearInterval(timerId);
        };
    }, []);

    return date;
};
