import * as React from 'react';
import classNames from 'classnames';
import {Size} from '../../state/utils/BrowserUtils';
import {GameImageSize, GameItem} from '../../state/actions/GamesActions';
import './GameScreen.scss';
import {calcProportionSize, getGameImage} from '../../state/utils/GameUtils';
import {NString} from '../../state/meta';
import AppState from '../../state/AppState';

type ProportionConfig = { byProvider: { [key: string]: string }, byGameId: { [key: string]: string } };
const proportionConfig = require('./GameScreenProportion.json') as ProportionConfig;

interface GameScreenState {
    isLoading?: boolean;
}

interface GameScreenProps {
    // app props
    appState: AppState;
    viewport: Size;

    // game props
    runningGame?: GameItem;
    proportionSize?: Size;
    iframe?: string;
    rawHTML?: string;

    sport2?: boolean;
    iframeRef?: React.RefObject<HTMLIFrameElement>;
    height?: string;
}

// DO NOT CHANGE this id, it is used inside games scripts
export const GAME_CONTENT_ID = 'game-content';

class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
    element?: HTMLDivElement;

    constructor(props: GameScreenProps) {
        super(props);
        this.state = {};
    }

    componentDidUpdate(prevProps: GameScreenProps, prevState: GameScreenState) {
        if (this.props.iframe && prevState.isLoading === undefined) {
            this.setState({isLoading: true});
        }
    }

    render() {
        const {runningGame, appState} = this.props;
        const {isLoading} = this.state;
        const {gameId} = appState.lobbyGameActions;

        let providerClass = undefined;
        let gameClass = undefined;
        let backgroundStyle = undefined;
        if (runningGame) {
            providerClass = `GameScreen-${runningGame.provider}`;
            gameClass = `GameScreen-${runningGame.id}`;
            backgroundStyle = {backgroundImage: `url(${getGameImage(runningGame, GameImageSize.SIZE_300x188)})`};
        } else if (gameId) {
            gameClass = `GameScreen-${gameId}`;
        }
        const className = classNames(
            'GameScreen',
            providerClass,
            gameClass
        );
        return (
            <div className={className} ref={(e: HTMLDivElement) => this.element = e}>
                {backgroundStyle && (
                    <div className="GameScreen__background" style={backgroundStyle}/>
                )}
                {isLoading && (
                    // To provide backward compatibility with jade, we assume that children contains a loader element.
                    // When the loader will also be reworked as react component, just replace this with loader usage.
                    this.props.children
                )}
                {this.renderGameScreen()}
            </div>
        );
    }

    renderGameScreen() {
        const {iframe, rawHTML, runningGame, appState, sport2} = this.props;
        const provider = runningGame ? runningGame.provider : null;
        const {isLoading} = this.state;
        const gameSize = this.calcGameSize();
        const style = gameSize ?
            {width: `${gameSize.width}px`, height: `${gameSize.height}px`} :
            {width: undefined, height: undefined};
        const className = classNames(
            'GameScreen__content',
            {
                'GameScreen__content--iframe': !!iframe,
                'GameScreen__content--rawHtml': !!rawHTML,
                'GameScreen__content--script': !rawHTML && !iframe,
                'GameScreen__content--hidden': !!isLoading
            }
        );
        const gameName = runningGame ? runningGame.name![appState.language] : '';

        if (provider === 'tomhorn') {
            return (
                <div className={className} style={style}>
                    <div id={GAME_CONTENT_ID}/>
                </div>
            );
        } else if (iframe) {
            return (
                <div id={GAME_CONTENT_ID} className={className} style={style}>
                    <iframe
                        title={gameName}
                        src={iframe}
                        frameBorder="0"
                        allowFullScreen={true}
                        scrolling={sport2 ? "" : "no"}
                        onLoad={this.onIFrameLoaded}
                        height={this.props.height}
                        ref={this.props.iframeRef}
                    />
                </div>
            );
        } else if (rawHTML) {
            return (
                <div id={GAME_CONTENT_ID} style={style} className={className} dangerouslySetInnerHTML={{__html: rawHTML}}/>
            );
        } else {
            return <div id={GAME_CONTENT_ID} style={style} className={className}/>;
        }
    }

    onIFrameLoaded = () => {
        this.setState({isLoading: false});
    };

    calcGameSize() {
        if (this.element && this.props.runningGame) {
            const proportion = this.getGameProportion(this.props.runningGame);
            if (proportion) {
                const baseSize = {width: this.element.clientWidth, height: this.element.clientHeight};
                return calcProportionSize(baseSize, proportion);
            }
        }

        return null;
    }

    getGameProportion(game: GameItem): Size | null {
        if (
            this.isRunningOldArrowedgeGame(game.id) ||
            (this.props.appState.isMobile && game.provider === 'yorg')
        ) {
            return null;
        } else {
            return this.props.proportionSize || this.getPredefinedProportionsSize(game) || null;
        }
    }

    getPredefinedProportionsSize(game: GameItem): Size | null {
        const proportions = game.id && game.provider ?
            proportionConfig.byGameId[game.id] || proportionConfig.byProvider[game.provider] || null :
            null;
        if (proportions) {
            if (proportions === 'auto') {
                return null;
            } else {
                const split = proportions.split(':');
                return {width: parseFloat(split[0]), height: parseFloat(split[1])};
            }
        } else {
            return null;
        }
    }

    isRunningOldArrowedgeGame(gameId: NString): boolean {
        return !!(gameId && [
            'arrowsedge-bluebeardsgold',
            'arrowsedge-aroundtheworld',
            'arrowsedge-yellowbrickreels'
        ].indexOf(gameId) >= 0);
    }
}

export default GameScreen;
