import {bonuses, commons, games, messages, site, users, wallet, shop, usersLocations, goapi} from './proto';
import {v4 as uuid} from 'uuid';
import axios, {AxiosError, AxiosInstance} from 'axios';
import {runInAction} from 'mobx';
import {cookie, addCryptoDenominationToParams, isGPSE, getMobileOSType} from '../state/utils/BrowserUtils';
import {WithdrawalRequestData} from '../state/payment-systems/WithdrawalStore';
import {NLong, NString} from '../state/meta';
import {forceMapToString} from './utils';
import Long from 'long';
import AppState from '../state/AppState';
import { initGoogleAnalytics, initYandexMetrika } from '../state/utils/ThirdParty';
import { ProfileDocumentsPreparedFile } from '../state/actions/ProfileDocumentsActions';
import Payway from '../state/payment-systems/Payway';

let Fingerprint2 = require('fingerprintjs2');

export type Callback = (msg: messages.IServerResponse, act: messages.IClientActionResponse) => void;
export type ErrorCallback = (code: number, message: string) => void;
export type RegisterCustomFields = { [k: string]: users.RegisterRequest.IFieldValue };
export type CallbackApi = (msg: messages.IServerResponse, act: goapi.IServerActionResponse) => void;

export const REQUEST_TIMEOUT = 30000;
const ERROR_TIMEOUT = 500;

export default class APIConnector {

    private batchMode: boolean = false;
    private skipOnErrors: boolean = false;
    private apiIsDown: boolean = false;
    private packetsInFlight: number = 0;

    private unsentActions: messages.ClientActionRequest[] = [];
    private handlers: { [k: string]: Callback } = {};
    private errorCallback: ErrorCallback;
    private appState: AppState;

    private unsentApiActions: goapi.ClientActionRequest[] = [];
    private apiHandlers: { [k: string]: CallbackApi } = {};


    private requester: AxiosInstance = axios.create({
        baseURL: window.location.origin,
        responseType: 'arraybuffer',
        headers: {'Content-Type': 'application/x-protobuf'},
        timeout: REQUEST_TIMEOUT
    });

    constructor(appState: AppState) {
        this.appState = appState;
        this.errorCallback = appState.modal.showError500;
    }

    init(cb: Callback) {
        const siteConfig = new site.SiteConfigRequest();
        siteConfig.queryString = window.location.search;

        const action = new messages.ClientActionRequest();
        action.siteConfig = siteConfig;

        this.enqueueAction(action, cb);
    }

    initialize(referrer: string, params: { [k: string]: string } | null, cb: Callback) {
        const siteConfig = new site.SiteConfigRequest();
        siteConfig.queryString = window.location.search;
        siteConfig.referrer = referrer;

        if (params) {
            siteConfig.params = params;
        }

        const action = new messages.ClientActionRequest();
        action.siteConfig = siteConfig;

        this.enqueueAction(action, cb);
    }

    signIn(email: string, country: string, phone: string, password: string, cb: Callback) {
        const loginRequest = new users.LoginRequest();
        loginRequest.email = email;
        loginRequest.country = country;
        loginRequest.phone = phone;
        loginRequest.password = password;

        const action = new messages.ClientActionRequest();
        action.login = loginRequest;

        this.enqueueAction(action, cb);
    }

    signInUsingToken(token: string, cb: Callback) {
        const loginRequest = new users.LoginRequest();
        loginRequest.token = token;

        const action = new messages.ClientActionRequest();
        action.login = loginRequest;

        this.enqueueAction(action, cb);
    }

    signInUsingGeneratedLogin(login: string, password: string, cb: Callback) {
        const loginRequest = new users.LoginRequest();
        loginRequest.login = login;
        loginRequest.password = password;

        const action = new messages.ClientActionRequest();
        action.login = loginRequest;

        this.enqueueAction(action, cb);
    }

    signInUsingSocial(params: {provider: number, redirectUrl: string, authCode?: string}, cb: Callback) {
        const loginRequest = new users.LoginRequest();
        const {provider, redirectUrl, authCode} = params;

        loginRequest.social = {
            provider,
            redirectUrl,
            authCode,
        }

        const action = new messages.ClientActionRequest();
        action.login = loginRequest;

        this.enqueueAction(action, cb);
    }

    authorizeByExternalAuthCode(token: string, phone: string, country: string, cb: Callback) {
        const loginRequest = new users.LoginRequest({
            externalAuthCode: token,
            phone,
            country,
        });

        const action = new messages.ClientActionRequest();
        action.login = loginRequest;

        this.enqueueAction(action, cb);
    }

    getExternalEntryCode(phone: string, countryCode: string, forceSendCode: boolean = false, voiceMessageConfirmation: boolean, cb: Callback) {
        const externalCodeRequest = new users.ExternalEntryCodeRequest({
            countryCode,
            phone,
            forceSendCode,
            voiceMessageConfirmation,
        });

        const action = new messages.ClientActionRequest();
        action.externalCode = externalCodeRequest;

        this.enqueueAction(action, cb);
    }

    signUp(email: string, password: string, phone: string, countryCode: string, currencyId: Long,
           shouldSubscribe: boolean, fields: RegisterCustomFields | null, voiceMessageConfirmation: boolean, cb: Callback) {
        const registerRequest = new users.RegisterRequest({
            email,
            password,
            phone,
            countryCode,
            currencyId,
            subscribe: shouldSubscribe,
            fields: fields ?? undefined,
            voiceMessageConfirmation,
        }); 

        const action = new messages.ClientActionRequest();
        action.register = registerRequest;

        this.enqueueAction(action, cb);
    }

    signUpSocial(currencyId: Long, provider: number, facebookId: string, cb: Callback) {
        const registerRequest = new users.RegisterRequest();
        registerRequest.currencyId = currencyId;
        registerRequest.social = {
            provider,
            facebookId,
        };

        const action = new messages.ClientActionRequest();
        action.register = registerRequest;

        this.enqueueAction(action, cb);
    }

    signUpOneClick(currencyId: Long, cb: Callback) {
        const request = new users.RegisterOneClickRequest();
        request.currencyId = currencyId;

        const action = new messages.ClientActionRequest();
        action.registerOneClick = request;

        this.enqueueAction(action, cb);
    }

    signOut(cb: Callback) {
        const logoutRequest = new users.LogoutRequest();
        logoutRequest.closeAllSessions = true;

        const action = new messages.ClientActionRequest();
        action.logout = logoutRequest;

        this.enqueueAction(action, cb);
    }

    changeLanguage(language: string, cb: Callback) {
        const languageRequest = new users.ChangeCurrentLanguageRequest();
        languageRequest.language = language;

        const action = new messages.ClientActionRequest();
        action.changeLanguage = languageRequest;

        this.enqueueAction(action, cb);
    }

    changePassword(password: string, cb: Callback) {
        const changePasswordRequest = new users.ChangePasswordRequest();
        changePasswordRequest.password = password;

        const action = new messages.ClientActionRequest();
        action.changePassword = changePasswordRequest;

        this.enqueueAction(action, cb);
    }

    changeEmail(email: string, cb: Callback) {
        const changeEmailRequest = new users.ChangeEmailRequest();
        changeEmailRequest.email = email;

        const action = new messages.ClientActionRequest();
        action.changeEmail = changeEmailRequest;

        this.enqueueAction(action, cb);
    }

    changePhone(phone: string, phoneCountryCode: string, voiceMessageConfirmation: boolean, cb: Callback) {
        const changePhoneRequest = new users.ChangePhoneRequest({
            phone,
            countryCode: phoneCountryCode,
            voiceMessageConfirmation,
        });

        const action = new messages.ClientActionRequest();
        action.changePhone = changePhoneRequest;

        this.enqueueAction(action, cb);
    }

    changeUserFields(fields: { [k: string]: users.RegisterRequest.IFieldValue }, cb: Callback) {
        const changeFieldsRequest = new users.ChangeFieldsRequest();
        changeFieldsRequest.fields = fields;

        const action = new messages.ClientActionRequest();
        action.changeFields = changeFieldsRequest;

        this.enqueueAction(action, cb);
    }

    changeCurrency(currencyId: Long, cb: Callback) {
        const currencyRequest = new users.ChangeCurrentCurrencyRequest();
        currencyRequest.currencyId = currencyId;

        const action = new messages.ClientActionRequest();
        action.changeCurrency = currencyRequest;

        this.enqueueAction(action, cb);
    }

    loadStaticPage(id: string, cb: Callback) {
        const pageRequest = new site.StaticPageRequest();
        pageRequest.url = id;

        const action = new messages.ClientActionRequest();
        action.staticPage = pageRequest;

        this.enqueueAction(action, cb);
    }

    restorePasswordRequest(email: string, phone: string, countryCode: string, cb: Callback) {
        const restorePasswordRequest = new users.RestorePasswordRequest();
        restorePasswordRequest.email = email;
        restorePasswordRequest.phone = phone;
        restorePasswordRequest.countryCode = countryCode;

        const action = new messages.ClientActionRequest();
        action.restorePassword = restorePasswordRequest;

        this.enqueueAction(action, cb);
    }

    restorePassword(email: string,
                    phone: string,
                    countryCode: string,
                    restoreCode: string,
                    newPassword: string,
                    cb: Callback) {
        const restorePasswordRequest = new users.RestorePasswordRequest();
        restorePasswordRequest.email = email;
        restorePasswordRequest.phone = phone;
        restorePasswordRequest.countryCode = countryCode;
        restorePasswordRequest.code = restoreCode;
        restorePasswordRequest.newPassword = newPassword;

        const action = new messages.ClientActionRequest();
        action.restorePassword = restorePasswordRequest;

        this.enqueueAction(action, cb);
    }

    refillPiastrix(balanceId: Long, amount: Long, cb: Callback) {
        const refill = new wallet.RefillRequest();
        refill.amount = amount;
        refill.balanceId = balanceId;
        refill.provider = wallet.WithdrawProvider.PIASTRIX;

        const action = new messages.ClientActionRequest();
        action.refill = refill;

        this.enqueueAction(action, cb);
    }

    refillWallettec(balanceId: Long, amount: Long, fields: { [key: string]: string }, cb: Callback) {
        const refill = new wallet.RefillRequest();
        refill.amount = amount;
        refill.balanceId = balanceId;
        refill.provider = wallet.WithdrawProvider.WALLETTEC;
        refill.fields = fields;

        const action = new messages.ClientActionRequest();
        action.refill = refill;

        this.enqueueAction(action, cb);
    }

    refillCashlib(balanceId: Long, amount: Long, voucher: string | null, cb: Callback) {
        const refill = new wallet.RefillRequest();
        refill.amount = amount;
        refill.balanceId = balanceId;
        refill.provider = wallet.WithdrawProvider.CASHLIB;
        if (voucher) {
            refill.voucher = voucher;
        }

        const action = new messages.ClientActionRequest();
        action.refill = refill;

        this.enqueueAction(action, cb);
    }

    refillPaysera(balanceId: Long, amount: Long, type: string, cb: Callback) {
        const refill = new wallet.RefillRequest();
        refill.provider = wallet.WithdrawProvider.PAYSERA;
        refill.amount = amount;
        refill.balanceId = balanceId;
        refill.typeId = type;

        const action = new messages.ClientActionRequest();
        action.refill = refill;

        this.enqueueAction(action, cb);
    }

    refillByPayway(payway: Payway, paywayId: NString, provider: wallet.WithdrawProvider, amount: NLong, balanceId: Long, vaucher: NString, fields: { [key: string]: string } | null | undefined, cb: Callback) {
        const refill = new wallet.RefillRequest();
        refill.provider = provider;
        refill.balanceId = balanceId;

        // cashlib dosn't require amount
        if (amount) {
            refill.amount = amount;
        }

        // can be null in old refill implementation
        if (paywayId) {
            refill.typeId = paywayId;
        }


        if (this.appState.refillStore.selectedTile?.id) {
            refill.tileId = this.appState.refillStore.selectedTile?.id;
        }

        // currently used in cashlib only
        if (vaucher) {
            refill.voucher = vaucher;
        }

        // used in old implementation of wallettec
        if (fields) {
            refill.fields = forceMapToString(fields);
        }

        if (payway.fileUpload) {
            refill.attaches = payway.fileUpload.preparedFiles.map(file => ({
               bytes: file.bytes,
               fileExtension: file.name.substring(file.name.lastIndexOf('.') + 1),
            }));
        }

        const action = new messages.ClientActionRequest();
        action.refill = refill;

        this.enqueueAction(action, cb);
    }

    defineDepositsRoyalty(amount: Long, currency: Long, cb: Callback) {
        const action = new messages.ClientActionRequest({
            defineDepositsRoyaltyRequest: {
                depositAmount: {
                    amount,
                    currency,
                },
            }
        });

        this.enqueueAction(action, cb);
    }

    activatePromoCode(code: string, cb: Callback, retryOnAnyError: boolean = false) {
        const activateCode = new bonuses.BonusCodeActivateRequest();
        activateCode.code = code;

        const bonusAction = new bonuses.BonusCodeActionRequest();
        bonusAction.activateCode = activateCode;

        const action = new messages.ClientActionRequest();
        action.bonusAction = bonusAction;

        this.enqueueAction(action, cb, false, retryOnAnyError);
    }

    getUserBonuses(cb: Callback, page?: string) {
        const request = new bonuses.UserBonusesListRequest();
        request.addMeta = true;
        request.addPossible = true;
        request.page = page ? page : window.location.pathname;

        const msg = new messages.ClientActionRequest();
        msg.userBonuses = request;

        this.enqueueAction(msg, cb);
    }

    cancelBonus(code: string, cb: Callback) {
        const cancelBonus = new bonuses.BonusCancelRequest();
        cancelBonus.code = code;

        const bonusAction = new bonuses.BonusCodeActionRequest();
        bonusAction.cancelCode = cancelBonus;

        const action = new messages.ClientActionRequest();
        action.bonusAction = bonusAction;

        this.enqueueAction(action, cb);
    }

    cancelBonusPreview(code: string, cb: Callback) {
        const previewCancelCode = new bonuses.BonusCancelPreviewRequest({ code });
        const bonusAction = new bonuses.BonusCodeActionRequest({ previewCancelCode });
        const action = new messages.ClientActionRequest({ bonusAction });

        this.enqueueAction(action, cb);
    }

    closePromoCodeNotification(id: string, action: bonuses.BonusCodeNotificationActionRequest.Action, cb: Callback, retryOnAnyError: boolean = false) {
        const notificationAction = new bonuses.BonusCodeNotificationActionRequest();
        notificationAction.id = id;
        notificationAction.action = action;

        const msg = new messages.ClientActionRequest();
        msg.bonusNotificationAction = notificationAction;

        this.enqueueAction(msg, cb, false, retryOnAnyError);
    }

    getBonusBalance(cb: Callback) {
        const request = new users.UserBonusBalancesRequest();

        const msg = new messages.ClientActionRequest();
        msg.userBonusBalances = request;

        this.enqueueAction(msg, cb);
    }

    getRefillProviders(cb: Callback) {
        const action = new messages.ClientActionRequest();
        action.refillProviders = new wallet.RefillProvidersRequest();

        this.enqueueAction(action, cb);
    }

    getWithdrawalTypes(cb: Callback) {
        const action = new messages.ClientActionRequest();
        action.withdrawTypes = new wallet.WithdrawTypesRequest();

        this.enqueueAction(action, cb);
    }

    withdraw(payway: Payway, data: WithdrawalRequestData, cb: Callback) {
        const request = new wallet.WithdrawRequest();
        request.typeId = data.id;
        request.balanceId = data.balanceId;

        if (data.provider) {
            request.provider = data.provider;
        }

        if (data.amount) {
            request.amount = data.amount;
        }

        if (data.credentials) {
            request.credentials = data.credentials;
        }

        if (data.fields) {
            request.fields = forceMapToString(data.fields);
        }

        if (data.tileId) {
            request.tileId = data.tileId;
        }

        if (payway.fileUpload) {
            request.attaches = payway.fileUpload.preparedFiles.map(file => ({
               bytes: file.bytes,
               fileExtension: file.name.substring(file.name.lastIndexOf('.') + 1),
            }));
        }

        const action = new messages.ClientActionRequest();
        action.withdraw = request;

        this.enqueueAction(action, cb);
    }

    getWithdrawalRoyalties(data: WithdrawalRequestData, cb: Callback) {
        const request = new wallet.WithdrawalRoyaltyRequest();
        request.typeId = data.id;
        request.balanceId = data.balanceId;

        if (data.provider) {
            request.provider = data.provider;
        }

        if (data.amount) {
            request.amount = data.amount;
        }

        if (data.credentials) {
            request.credentials = data.credentials;
        }

        const action = new messages.ClientActionRequest();
        action.withdrawCalcRoyalty = request;

        this.enqueueAction(action, cb);
    }

    getBillingHistory(cb: Callback) {
        const billingHistoryRequest = new wallet.UserBillingHistoryRequest();

        const action = new messages.ClientActionRequest();
        action.billingHistory = billingHistoryRequest;

        this.enqueueAction(action, cb);
    }

    revokeWithdrawal(billingOperationId: string, cb: Callback) {
        const action = new messages.ClientActionRequest({
            revokeWithdrawalRequest: new wallet.RevokeWithdrawalRequest({ billingOperationId })
        });

        this.enqueueAction(action, cb);
    }

    getCryptoAddresses(cb: Callback) {
        const cryptoAddressesRequest = new wallet.CryptoAddressesRequest();
        const action = new messages.ClientActionRequest();
        action.getCryptoAddresses = cryptoAddressesRequest;

        this.enqueueAction(action, cb);
    }

    confirmPhone(code: string, cb: Callback) {
        const request = new users.ConfirmPhoneRequest();
        request.code = code;

        const action = new messages.ClientActionRequest();
        action.confirmPhone = request;

        this.enqueueAction(action, cb);
    }

    confirmNewPhone(code: string, cb: Callback) {
        const request = new users.ConfirmPhoneRequest();
        request.code = code;

        const action = new messages.ClientActionRequest();
        action.confirmNewPhone = request;

        this.enqueueAction(action, cb);
    }

    confirmEmail(email: string, code: string, cb: Callback) {
        const request = new users.ConfirmEmailRequest();
        request.email = email;
        request.code = code;

        const action = new messages.ClientActionRequest();
        action.confirmEmail = request;

        this.enqueueAction(action, cb);
    }

    confirmNewEmail(email: string, code: string, cb: Callback) {
        const request = new users.ConfirmEmailRequest();
        request.email = email;
        request.code = code;

        const action = new messages.ClientActionRequest();
        action.confirmNewEmail = request;

        this.enqueueAction(action, cb);
    }

    resendPhoneConfirmation(cb: Callback) {
        const request = new users.ResendPhoneConfirmationRequest();

        const action = new messages.ClientActionRequest();
        action.resendPhoneConfirmation = request;

        this.enqueueAction(action, cb);
    }

    resendNewPhoneConfirmation(cb: Callback) {
        const request = new users.ResendPhoneConfirmationRequest();

        const action = new messages.ClientActionRequest();
        action.resendNewPhoneConfirmation = request;

        this.enqueueAction(action, cb);
    }

    resendEmailConfirmation(cb: Callback) {
        const request = new users.ResendEmailConfirmationRequest();

        const action = new messages.ClientActionRequest();
        action.resendEmailConfirmation = request;

        this.enqueueAction(action, cb);
    }

    resendNewEmailConfirmation(cb: Callback) {
        const request = new users.ResendEmailConfirmationRequest();

        const action = new messages.ClientActionRequest();
        action.resendNewEmailConfirmation = request;

        this.enqueueAction(action, cb);
    }

    getGameLaunchMethod(gameId: string, deviceType: commons.DeviceType, deviceCaps: commons.DeviceCapabilities[],
                        cb: Callback, extraParams: { [k: string]: string } = {}, message?: string) {
        const msg = new games.GameEmbedRequest();
        msg.id = gameId;
        msg.device = deviceType;
        msg.capabilities = deviceCaps;
        msg.extraParams = extraParams;
        if (message) {
            msg.message = message;
        }

        const action = new messages.ClientActionRequest();
        action.gameEmbed = msg;

        this.enqueueAction(action, cb);
    }

    fetchGames(cb: Callback) {
        const siteConfig = new site.SiteConfigRequest();
        const action = new messages.ClientActionRequest();
        siteConfig.params = addCryptoDenominationToParams(null);
        action.siteConfig = siteConfig;

        this.enqueueAction(action, cb);
    }

    fetchFavoriteGames(cb: Callback) {
        const favorites = new users.UserGamesFavRequest();

        const action = new messages.ClientActionRequest();
        action.favGames = favorites;

        this.enqueueAction(action, cb);
    }

    addGameToFavorites(gameId: string, cb: Callback) {
        const addToFav = new users.UserGamesFavRequest.GameIdAct();
        addToFav.gameId = gameId;

        const favorites = new users.UserGamesFavRequest();
        favorites.addToFav = addToFav;

        const action = new messages.ClientActionRequest();
        action.favGames = favorites;

        this.enqueueAction(action, cb);
    }

    getAvailableAmounts(sessionId: string, bankCode: string, cb: Callback) {
        const req = new wallet.PaygigaAvailableAmountsRequest();
        req.sessionId = sessionId;
        req.bankCode = bankCode;

        const action = new messages.ClientActionRequest();
        action.availableAmounts = req;

        this.enqueueAction(action, cb);
    }

    removeGameFromFavorites(gameId: string, cb: Callback) {
        const addToFav = new users.UserGamesFavRequest.GameIdAct();
        addToFav.gameId = gameId;

        const favorites = new users.UserGamesFavRequest();
        favorites.removeFromFav = addToFav;

        const action = new messages.ClientActionRequest();
        action.favGames = favorites;

        this.enqueueAction(action, cb);
    }

    fetchPlayerLastGames(gamesCount: number = 25, cb: Callback) {
        const stats = new users.UserGamesStatsRequest.StatsRequest();
        stats.gamesCount = Long.fromNumber(gamesCount);

        const statsRequest = new users.UserGamesStatsRequest();
        statsRequest.last = stats;

        const action = new messages.ClientActionRequest();
        action.gamesStats = statsRequest;

        this.enqueueAction(action, cb);
    }

    fetchPlayerTopGames(gamesCount: number = 25, cb: Callback) {
        const stats = new users.UserGamesStatsRequest.StatsRequest();
        stats.gamesCount = Long.fromNumber(gamesCount);

        const statsRequest = new users.UserGamesStatsRequest();
        statsRequest.top = stats;

        const action = new messages.ClientActionRequest();
        action.gamesStats = statsRequest;

        this.enqueueAction(action, cb);
    }

    fetchPlayerSuggestedGames(gamesCount: number = 25, cb: Callback) {
        const stats = new users.UserGamesStatsRequest.StatsRequest();
        stats.gamesCount = Long.fromNumber(gamesCount);

        const statsRequest = new users.UserGamesStatsRequest();
        statsRequest.suggest = stats;

        const action = new messages.ClientActionRequest();
        action.gamesStats = statsRequest;

        this.enqueueAction(action, cb);
    }

    createRefCode(cb: Callback) {
        const request = new users.CreateRefCodeRequest();

        const action = new messages.ClientActionRequest();
        action.createRefCode = request;

        this.enqueueAction(action, cb);
    }

    getRefInfo(cb: Callback) {
        const request = new users.GetRefInfoRequest();

        const action = new messages.ClientActionRequest();
        action.getRefInfo = request;

        this.enqueueAction(action, cb);
    }

    // keepAlive(cb: Callback) {
    //     const request = new users.KeepAliveRequest();

    //     const action = new messages.ClientActionRequest();
    //     action.keepAlive = request;

    //     this.enqueueAction(action, cb);
    // }

    fetchUserDocuments(cb: Callback) {
        const request = new users.UserDocumentsRequest();

        const action = new messages.ClientActionRequest();
        action.userDocuments = request;

        this.enqueueAction(action, cb);
    }

    uploadDocuments(preparedFiles: ProfileDocumentsPreparedFile[], cb: Callback) {
        const documents = preparedFiles.map(file => file.info);
        const request = new users.UserDocumentsRequest({
            newDocuments: documents,
        });

        const action = new messages.ClientActionRequest();
        action.userDocuments = request;

        this.enqueueAction(action, cb);
    }

    removeDocument(id: Long, cb: Callback) {
        const request = new users.UserDocumentsRequest({
            documentsForRemoval: [id],
        });

        const action = new messages.ClientActionRequest();
        action.userDocuments = request;

        this.enqueueAction(action, cb);
    }

    fetchFreespinsForShop(cb: Callback) {
        const shopItems = new shop.SiteShopItemsRequest();

        const action = new messages.ClientActionRequest();
        action.shopItems = shopItems;

        this.enqueueAction(action, cb, true, true);
    }

    buyFreespin(id: Long, cb: Callback, promoCode?: string) {
        const shopPayment = new shop.SiteShopPaymentRequest({
            itemId: id,
            coupon: promoCode,
        });

        const action = new messages.ClientActionRequest();
        action.shopPayment = shopPayment;

        this.enqueueAction(action, cb);
    }

    freeSpinPromocodeRequest(itemId: Long, promoCode: string, cb: Callback) {
        const shopCoupon = new shop.SiteShopCouponDiscountRequest({
            itemId,
            couponCode: promoCode,
        });

        const action = new messages.ClientActionRequest();
        action.shopCoupon = shopCoupon;

        this.enqueueAction(action, cb);
    }

    activityUpdateRequest(path: string, cb: Callback) {
        const activityUpdate = new site.ActivityUpdateRequest({path});

        const action = new messages.ClientActionRequest();
        action.activityUpdate = activityUpdate;

        this.enqueueAction(action, cb, true);
    }

    refInvitationBonusesInfoRequest(cb: Callback) {
        const invitationBonusesInfoRequest = new users.invitations.InvitationBonusesInfoRequest();

        const action = new messages.ClientActionRequest();
        action.invitationBonusesInfoRequest = invitationBonusesInfoRequest;

        this.enqueueAction(action, cb);
    }

    refSendInviteRequest(params: users.invitations.ISendInvitationRequest, cb: Callback) {
        const invitationRequest = new users.invitations.SendInvitationRequest(params);

        const action = new messages.ClientActionRequest();
        action.sendInvitation = invitationRequest;

        this.enqueueAction(action, cb);
    }

    refResendInviteRequest(invitationId: Long, cb: Callback) {
        const resendInvitationRequest = new users.invitations.ResendInvitationRequest({invitationId});

        const action = new messages.ClientActionRequest();
        action.resendInvitation = resendInvitationRequest;

        this.enqueueAction(action, cb);
    }

    refListInvitationsRequest(limit: number, offset: Long, cb: Callback) {
        const listInvitationsRequest = new users.invitations.ListInvitationStatisticsRequest({limit, offset});

        const action = new messages.ClientActionRequest();
        action.listInvitations = listInvitationsRequest;

        this.enqueueAction(action, cb);
    }

    sendUserGeolocation(coords: {lat: number, long: number, accuracy: number}, cb: Callback) {
        const geoRequest = new usersLocations.AddUserGeoLocationRequest();
        geoRequest.lat = coords.lat;
        geoRequest.long = coords.long;
        geoRequest.accuracy = coords.accuracy;

        const action = new messages.ClientActionRequest();
        action.addUserLocationRequest = geoRequest;

        this.enqueueAction(action, cb);
    }

    uploadAttachmentsForPayway(billingId: string, attaches: wallet.IAttachDataRequest[], cb: Callback) {
        const attachBillingData = new wallet.PaywayAttachDataRequest({
            billingId,
            attaches,
        });
        const action = new messages.ClientActionRequest({
            attachBillingData
        });

        this.enqueueAction(action, cb);
    }

    cashAppConfirmPhone(phone: string, country: string, confirmWithVoice: boolean, confirmationCode: string | undefined = undefined, cb: Callback) {
        const cashAppConfirmPhoneRequest = new wallet.CashAppConfirmPhoneRequest({
            country,
            phone,
            confirmWithVoice,
            confirmationCode,
        });
        const action = new messages.ClientActionRequest({
            cashAppConfirmPhone: cashAppConfirmPhoneRequest,
        });

        this.enqueueAction(action, cb);
    }

    checkNonTakenSpinsGames(cb: CallbackApi) {
        const games = new goapi.GamesActionRequest({
            checkNonTaken: {
                check: true,
            }
        })
        const action = new goapi.ClientActionRequest({
            games: games,
        });

        this.enqueueApiAction(action, cb, true);
    }

    dumpContacts(userContacts: Uint8Array, cb: Callback) {
        const dumpContacts = new users.DumpContactsRequest({
            userContacts
        });
        const action = new messages.ClientActionRequest({
            dumpContacts
        });
        this.enqueueAction(action, cb, true, true);
    }

    startBatch() {
        this.batchMode = true;
    }

    startBatchWithSkip() {
        this.batchMode = true;
        this.skipOnErrors = true;
    }

    commit() {
        this.batchMode = false;
        this.skipOnErrors = false;
        this.sendPacket(this.unsentActions);
        this.unsentActions = [];
    }

    sendHit(ref?: string) {
        if (window.location.hostname !== 'localhost' && !isGPSE()) {
            const COUNTER_ID_GOOGLE_ANALYTICS = this.appState.g('%COUNTER_ID_GOOGLE_ANALYTICS%');
            const COUNTER_ID_YANDEX_METRIKA = this.appState.g('%COUNTER_ID_YANDEX_METRIKA%');
            if (COUNTER_ID_GOOGLE_ANALYTICS) {
                initGoogleAnalytics(COUNTER_ID_GOOGLE_ANALYTICS);
            }
            if (COUNTER_ID_YANDEX_METRIKA) {
                initYandexMetrika(COUNTER_ID_YANDEX_METRIKA);
            }
            cookie('wlref', ref || 'direct', 30 * 60 * 60 * 365, '/');
            if (getMobileOSType() === 'iOS') {
                // skip fingerprint detections on iOS since it's useless
                this.postHit();
            } else {
                new Fingerprint2().get((result: string, components: [{ key: string, value: string }]) => {
                    cookie('wlf', result, 30 * 60 * 60 * 365, '/');
                    this.postHit();
                });
            }
        }
    }

    private postHit(): void {
        axios.post(this.appState.g('%ENDPOINT_HIT%'))
            .then(function (response: object) {
                console.debug('hit response: ', response);
            })
            .catch(function (error: object) {
                console.debug('hit error: ', error);
            });
    }

    private sendPacket(unsentActions: messages.ClientActionRequest[], ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        this.packetsInFlight += 1;

        let msg = new messages.ClientRequest();
        msg.actions = unsentActions;
        console.debug('API request - ', msg);

        const buffer = messages.ClientRequest.encode(msg).finish();
        const buffer2send = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);

        this.requester
            .post(this.appState.g('%ENDPOINT_EXEC%'), buffer2send)
            .then(resp => {
                this.packetsInFlight--;
                this.handleSuccessfulSendPacket(new Uint8Array(resp.data));
            })
            .catch(err => {
                this.packetsInFlight -= 1;
                this.handleFailedSendPacket(err, unsentActions, ignoreErrors, retryOnAnyError);
            });
    }

    private enqueueAction(action: messages.ClientActionRequest, callback: Callback, ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        if (this.apiIsDown === true && this.skipOnErrors === true) {
            return;
        }

        let trx = uuid();
        action.trx = trx;
        this.handlers[trx] = callback;

        this.unsentActions = this.unsentActions.concat(action);
        if (this.batchMode === false) {
            this.sendPacket(this.unsentActions, ignoreErrors, retryOnAnyError);
            this.unsentActions = [];
        }
    }

    private handleFailedSendPacket(err: AxiosError, actions: messages.ClientActionRequest[], ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        this.apiIsDown = true;
        console.error(err.message);

        if (ignoreErrors) return;

        if (!retryOnAnyError && (err.code === '500' || (err.request && err.request.status === 500))) {
            this.errorCallback(500, 'Internal server error');
        } else {
            if (this.packetsInFlight === 0) {
                setTimeout(() => this.sendPacket(actions), ERROR_TIMEOUT);
            }
        }
    }

    private handleSuccessfulSendPacket(response: Uint8Array, ignoreErrors?: boolean) {
        this.apiIsDown = false;

        const resp = messages.ServerResponse.decode(response);
        console.debug('API response - ', resp);

        resp.actions.forEach(action => {
            const trx = action.trx!;
            const handler = this.handlers[trx];

            if (handler) {
                delete this.handlers[trx];
                runInAction(() => {
                    try {
                        if (
                            !action.login &&
                            !action.register &&
                            !action.registerOneClick &&
                            !action.logout &&
                            !action.restorePassword &&
                            !action.confirmNewEmail &&
                            !action.confirmEmail &&
                            !!this.appState.rawUserInfo?.userId &&
                            !!resp.user?.userId &&
                            this.appState.rawUserInfo.userId.notEquals(resp.user.userId)
                        ) {
                            window.location.reload();
                        }
                        handler(resp, action!);
                    } catch (e) {
                        if (!ignoreErrors) {
                            this.errorCallback(500, 'Fail to execute API handler.');
                        }
                        console.error('handler error:', e);
                    }
                });
            } else {
                console.error(`Skip ${trx}: no handler`);
            }
        });
    }

    async iconMetaUrlRequest(url: string) {
        try {
            const response = await axios({
                method: 'get',
                responseType: 'arraybuffer',
                url: `${window.location.origin}${url}`
            });
            return new Uint8Array(response.data);
        } catch (e) {
            console.error('iconMetaUrlRequest error:', e);
            return null;
        }
    }

    private sendPacketApi(unsentActions: goapi.ClientActionRequest[], ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        this.packetsInFlight += 1;

        let msg = new messages.ClientRequest();
        msg.apiActions = unsentActions;
        console.debug('API request - ', msg);

        const buffer = messages.ClientRequest.encode(msg).finish();
        const buffer2send = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);

        this.requester
            .post(this.appState.g('%ENDPOINT_EXEC%'), buffer2send)
            .then(resp => {
                this.packetsInFlight--;
                this.handleSuccessfulApiSendPacket(new Uint8Array(resp.data));
            })
            .catch(err => {
                this.packetsInFlight -= 1;
                this.handleFailedApiSendPacket(err, unsentActions, ignoreErrors, retryOnAnyError);
            });
    }

    private enqueueApiAction(action: goapi.ClientActionRequest, callback: CallbackApi, ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        if (this.apiIsDown === true && this.skipOnErrors === true) {
            return;
        }

        let trx = uuid();
        action.trx = new TextEncoder().encode(trx);
        this.apiHandlers[trx] = callback;

        this.unsentApiActions = this.unsentApiActions.concat(action);
        if (this.batchMode === false) {
            this.sendPacketApi(this.unsentApiActions, ignoreErrors, retryOnAnyError);
            this.unsentApiActions = [];
        }
    }

    private handleFailedApiSendPacket(err: AxiosError, actions: goapi.ClientActionRequest[], ignoreErrors?: boolean, retryOnAnyError?: boolean) {
        this.apiIsDown = true;
        console.error(err.message);

        if (ignoreErrors) return;

        if (!retryOnAnyError && (err.code === '500' || (err.request && err.request.status === 500))) {
            this.errorCallback(500, 'Internal server error');
        } else {
            if (this.packetsInFlight === 0) {
                setTimeout(() => this.sendPacketApi(actions), ERROR_TIMEOUT);
            }
        }
    }

    private handleSuccessfulApiSendPacket(response: Uint8Array, ignoreErrors?: boolean) {
        this.apiIsDown = false;

        const resp = messages.ServerResponse.decode(response);
        console.debug('API response - ', resp);

        resp.apiActions.forEach(action => {
            const trx = new TextDecoder().decode(action.trx!);
            const handler = this.apiHandlers[trx];

            if (handler) {
                delete this.apiHandlers[trx];
                runInAction(() => {
                    try {
                        handler(resp, action!);
                    } catch (e) {
                        if (!ignoreErrors) {
                            this.errorCallback(500, 'Fail to execute API handler.');
                        }
                        console.error('handler error:', e);
                    }
                });
            } else {
                console.error(`Skip ${trx}: no handler`);
            }
        });
    }
}
