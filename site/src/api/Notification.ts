import {messages} from './proto';
import AppState from '../state/AppState';
import {v4 as uuid} from 'uuid';
import {base64ToArrayBuffer} from '../state/utils/base64ToArrayBuffer';
import visibility from 'visibilityjs';

export type NotificationHandler = (message: messages.SSENotifcation) => void;

export enum PageVisibilityState {
    VISIBLE = 'visible',
    HIDDEN = 'hidden',
    PRERENDER = 'prerender',
}

const isEnabled = true;

export default class Notifications {
    isEnabled: boolean = true;
    eventSource: EventSource | null;
    connected: boolean = false;
    sseUuid = uuid();

    constructor(private appState: AppState, private notificationHandler: NotificationHandler) {}

    init = () => {
        if (visibility.isSupported()) {
            visibility.change(this.onChangePageVisibilityState);
        }
        this.reconnect();
    };

    connect = () => {
        if (isEnabled && !this.eventSource) {
            console.debug('SSE: connect');
            try {
                const eventSource = new EventSource(
                    `${this.appState.g('%ENDPOINT_LISTEN%')}?id=${this.sseUuid}`,
                    {withCredentials: true},
                );
                eventSource?.addEventListener('open', this.onOpen, false);
                eventSource?.addEventListener('error', this.onError, false);
                eventSource?.addEventListener('message', this.onMessage, false);
                this.eventSource = eventSource;
                this.connected = true;
            } catch (e) {
                console.error('Unable to create EventSource');
            }
        }
    };

    disconnect = () => {
        this.disconnectInternal();
        this.connected = false;
    };

    disconnectInternal = () => {
        if (this.eventSource) {
            console.debug('SSE: disconnect');
            this.eventSource.close();
            this.eventSource = null;
        }
    };

    reconnect = (isSessionChanged = false) => {
        if (isSessionChanged) {
            this.sseUuid = uuid();
        }
        this.disconnectInternal();
        this.connect();
    };

    autoReconnect = () => {
        // 0 — connecting, 1 — open, 2 — closed
        if (this.connected && (!this.eventSource || this.eventSource.readyState === 2)) {
            this.reconnect();

            // check if reconnection proceed successfully
            setTimeout(this.autoReconnect, 5000);
        }
    };

    private onChangePageVisibilityState = (e: Event, state: string) => {
        if (state === PageVisibilityState.VISIBLE) {
            this.connect();
        }
        if (this.connected && state === PageVisibilityState.HIDDEN && this.appState.isMobileOnly) {
            this.disconnectInternal();
        }
    };

    private onMessage = (e: Event) => {
        const data = (e as MessageEvent).data as string;
        const arrayBuffer = base64ToArrayBuffer(data);
        const message = messages.SSENotifcation.decode(arrayBuffer);
        console.debug('(' + new Date().toString() + ') SSE: ', message);
        this.notificationHandler(message);
    };

    private onOpen = (e: Event) => {
        // console.debug('Open: ', e);
    };

    private onError = (e: Event) => {
        // console.debug('Error: ', e, this.siteSource.readyState);
        setTimeout(this.autoReconnect, 5000);
    };
}
