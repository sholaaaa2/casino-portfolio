import {webpush} from './proto';
import AppState from '../state/AppState';
import {v4 as uuid} from 'uuid';
import {base64ToArrayBuffer} from '../state/utils/base64ToArrayBuffer';
import wait from '../state/utils/wait';
import {PageVisibilityState} from './Notification';
import visibility from 'visibilityjs';
import axios from 'axios';
import { REQUEST_TIMEOUT } from './APIConnector';

const RETRY_TIMEOUT = 1000;

export type WebPushHandler = (message: webpush.ServerEvent) => void;

export class WebPushAPI {
    isEnabled: boolean = true;
    eventSource: EventSource | null;
    connected: boolean = false;
    sseUuid = uuid();

    constructor(private appState: AppState, private webPushHandler: WebPushHandler) {}

    init = () => {
        if (visibility.isSupported()) {
            visibility.change(this.onChangePageVisibilityState);
        }
        this.reconnect();
    };

    connect = () => {
        if (!this.eventSource) {
            console.debug('WebPush: connect');
            try {
                const eventSource = new EventSource(
                    `${this.appState.g('%ENDPOINT_WEBPUSH_SSE%')}?id=${this.sseUuid}`,
                    {withCredentials: true},
                );
                console.debug('eventSource', eventSource);
                eventSource.addEventListener('open', this.onOpen, false);
                eventSource.addEventListener('error', this.onError, false);
                eventSource.addEventListener('message', this.onMessage, false);
                this.eventSource = eventSource;
                this.connected = true;
            } catch (e) {
                console.error('Unable to create EventSource');
            }
        }
    };

    disconnect = () => {
        this.disconnectInternal();
        this.connected = false;
    };

    disconnectInternal = () => {
        if (this.eventSource) {
            console.debug('WebPush: disconnect');
            this.eventSource.close();
            this.eventSource.removeEventListener('open', this.onOpen);
            this.eventSource.removeEventListener('error', this.onError);
            this.eventSource.removeEventListener('message', this.onMessage);
            this.eventSource = null;
        }
    };

    reconnect = (isSessionChanged = false) => {
        if (isSessionChanged) {
            this.sseUuid = uuid();
        }
        this.disconnectInternal();
        this.connect();
    };

    autoReconnect = () => {
        // 0 — connecting, 1 — open, 2 — closed
        if (this.connected && (!this.eventSource || this.eventSource.readyState === 2)) {
            this.reconnect();

            // check if reconnection proceed successfully
            setTimeout(this.autoReconnect, 5000);
        }
    };

    private onChangePageVisibilityState = (e: Event, state: string) => {
        if (state === PageVisibilityState.VISIBLE) {
            this.connect();
        }
        if (this.connected && state === PageVisibilityState.HIDDEN && this.appState.isMobileOnly) {
            this.disconnectInternal();
        }
    }

    onMessage = (e: Event) => {
        const data = (e as MessageEvent).data as string;
        const arrayBuffer = base64ToArrayBuffer(data);
        const message = webpush.ServerEvent.decode(arrayBuffer);
        console.debug('(' + new Date().toString() + ') WebPush: ', message);
        this.webPushHandler(message);
    };

    onOpen = (e: Event) => {
        console.debug('Webpush Open: ', e);
    };

    onError = (e: Event) => {
        console.debug('Webpush Error: ', e as MessageEvent);
        setTimeout(this.autoReconnect, 5000);
    };

    post = async (data: {
        webPushReceived?: webpush.IWebPushReceived;
        webPushClosed?: webpush.IWebPushClose;
    }): Promise<webpush.IServerResponse | null> => {
        const textEncoder = new TextEncoder();
        const trx = textEncoder.encode(uuid());
        const request = new webpush.ClientEvent({
            trx,
            ...data,
        });
        const result = await this.executeWithRetry(request);
        return result ?? null;
    };

    executeWithRetry = async (
        request: webpush.ClientEvent,
    ): Promise<webpush.IServerResponse | null> => {
        console.log('->', request);
        const buffer = webpush.ClientEvent.encode(request).finish();
        const body = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);
        let result: webpush.IServerResponse | undefined;
        let retryNumber = 0;
        while (!result) {
            try {
                if (retryNumber > 0) {
                    await wait(RETRY_TIMEOUT);
                }
                const response = await this.sendRequest(body);
                if (response.status === 200) {
                    result = webpush.ServerResponse.decode(new Uint8Array(response.data));
                }
            } catch (error) {
                console.error(error);
            } finally {
                retryNumber++;
            }
        }
        console.log('<-', result);
        return result ?? null;
    };

    sendRequest = async (body: ArrayBuffer) => {
        return axios({
            responseType: 'arraybuffer',
            baseURL: window.location.origin,
            url: this.appState.g('%ENDPOINT_WEBPUSH_POST%'),
            headers: {
                Cache: 'no-cache',
                'Content-Type': 'application/x-protobuf'
            },
            method: 'POST',
            data: body,
            timeout: REQUEST_TIMEOUT,
        });
    };
}
