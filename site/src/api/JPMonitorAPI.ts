import axios from 'axios';
import {jpmonitor} from './proto';
import AppState from '../state/AppState';
import wait from '../state/utils/wait';
import {logDev} from '../state/utils/logDev';
import {REQUEST_TIMEOUT} from './APIConnector';

const RETRY_TIMEOUT = 1000;

export type JPMonitorResponseHandler = (response: jpmonitor.GameExtrasServerResponse) => void;

export class JPMonitorAPI {
    isChecking = false;
    checkTimeout?: number;
    queryDelayInMilliseconds?: number;

    constructor(private appState: AppState, private handler: JPMonitorResponseHandler) {}

    check = async () => {
        if (!this.isChecking) {
            this.isChecking = true;
            window.clearTimeout(this.checkTimeout);
            const response = await this.post({});
            const retryInMs = response?.nextQueryDelayInMilliseconds.toNumber() ?? RETRY_TIMEOUT;
            this.queryDelayInMilliseconds = retryInMs;
            if (response) {
                this.handler(response);
            }
            this.isChecking = false;
            this.checkTimeout = window.setTimeout(() => {
                this.check();
            }, retryInMs);
        }
    };

    post = async (
        data: jpmonitor.IGameExtrasClientRequest,
    ): Promise<jpmonitor.GameExtrasServerResponse | null> => {
        const request = new jpmonitor.GameExtrasClientRequest(data);
        const result = await this.executeWithRetry(request);
        return result ?? null;
    };

    executeWithRetry = async (
        request: jpmonitor.GameExtrasClientRequest,
    ): Promise<jpmonitor.GameExtrasServerResponse | null> => {
        logDev('->', request);
        const buffer = jpmonitor.GameExtrasClientRequest.encode(request).finish();
        const body = buffer.buffer.slice(buffer.byteOffset, buffer.byteOffset + buffer.byteLength);
        let result: jpmonitor.GameExtrasServerResponse | undefined;
        let retryNumber = 0;
        while (!result) {
            try {
                if (retryNumber > 0) {
                    await wait(RETRY_TIMEOUT);
                }
                const response = await this.sendRequest(body);
                if (response.status === 200) {
                    result = jpmonitor.GameExtrasServerResponse.decode(
                        new Uint8Array(response.data),
                    );
                }
            } catch (error) {
                console.error(error);
            } finally {
                retryNumber++;
            }
        }
        logDev('<-', result);
        return result ?? null;
    };

    sendRequest = async (body: ArrayBuffer) => {
        return axios({
            responseType: 'arraybuffer',
            baseURL: window.location.origin,
            url: this.appState.g('%ENDPOINT_JP_MONITOR%'),
            headers: {
                Cache: 'no-cache',
                'Content-Type': 'application/x-protobuf',
            },
            method: 'POST',
            data: body,
            timeout: REQUEST_TIMEOUT,
        });
    };
}
