import {NLong, NString, PhoneParams} from '../state/meta';
import {bonuses, games, site, users, wallet} from './proto';
import {formatPercent, NASymbol, getFreeSpinsToken} from '../state/utils/Money';
import {AccountPlaceholder} from '../state/payment-systems/utils';
import AppState from '../state/AppState';
import Long from 'long';

export const getCryptoAddressLoadingError = function () {
    return 'error-crypto-address-failed';
};

export const getWithdrawalMethodHint = function (methodId: NString) {
    switch (methodId) {
        case 'COINPAYMENTS':
            return AccountPlaceholder.CRYPTO_WALLET_ADDRESS;
        case 'beeline_rub':
        case 'megafon_rub':
        case 'mts_rub':
        case 'tele2_rub':
        case 'mobile_phone_number':
            return AccountPlaceholder.RU_PHONE_PLACEHOLDER;
        case 'yamoney_rub':
            return AccountPlaceholder.YANDEX_MONEY_ACCOUNT;
        case 'card_rub':
        case 'card_uah':
        case 'card_usd':
        case 'card_eur':
        case 'card_privat_uah':
            return AccountPlaceholder.CARD_NUMBER;
        case 'qiwi_usd':
        case 'qiwi_eur':
        case 'qiwi_rub':
            return AccountPlaceholder.QIWI_WALLET;
        case 'payeer_usd':
        case 'payeer_eur':
        case 'payeer_rub':
            return AccountPlaceholder.PAYEER_WALLET;
        case 'webmoney_usd':
        case 'webmoney_rub':
            return AccountPlaceholder.WEBMONEY_WALLET;
        case 'cashlib':
            return AccountPlaceholder.CASHLIB_WALLET;
        default:
            return '';
    }
};

export const getEmailChangeError = function (error?: users.ChangeEmailResponse.ChangeEmailError | null): string {
    switch (error) {
        case users.ChangeEmailResponse.ChangeEmailError.invalid_email:
            return 'error-email-invalid';
        case users.ChangeEmailResponse.ChangeEmailError.email_used:
            return 'error-register-email-used';
        case users.ChangeEmailResponse.ChangeEmailError.forbidden_email:
            return 'error-email-forbidden';
        default:
            return 'error-email-change-failed';
    }
};

export const getPhoneChangeError = function (error?: users.ChangePhoneResponse.ChangePhoneError | null): string {
    switch (error) {
        case users.ChangePhoneResponse.ChangePhoneError.invalid_phone:
            return 'error-register-phone-invalid';
        case users.ChangePhoneResponse.ChangePhoneError.invalid_country:
            return 'error-phone-code-invalid';
        case users.ChangePhoneResponse.ChangePhoneError.phone_used:
            return 'error-register-phone-used';
        case users.ChangePhoneResponse.ChangePhoneError.retry_too_fast:
            return 'error-change-phone-retry-too-fast';
        default:
            return 'error-phone-change-failed';
    }
};

export const getPhoneConfirmationError = function (error?: users.ConfirmPhoneResponse.ConfirmPhoneError | null): string {
    switch (error) {
        case users.ConfirmPhoneResponse.ConfirmPhoneError.unknown_code:
            return 'error-register-confirm-code-invalid';
        case users.ConfirmPhoneResponse.ConfirmPhoneError.already_confirmed:
            return 'error-register-confirm-code-already-confirmed';
        default:
            return 'error-phone-confirmation-failed';
    }
};

export const getLaunchGameError = function (error?: games.GameEmbedResponse.LaunchError | null): string {
    switch (error) {
        case games.GameEmbedResponse.LaunchError.DEVICE_NOT_SUPPORTED:
            return 'error-device-not-supported';
        case games.GameEmbedResponse.LaunchError.CURRENCY_NOT_SUPPORTED:
            return 'error-currency-not-supported';
        case games.GameEmbedResponse.LaunchError.GAME_NOT_FOUND:
            return 'error-game-not-found';
        case games.GameEmbedResponse.LaunchError.ACTIVE_BONUS_NOT_SUPPORTED:
            return 'error-game-disable-by-active-bonus';
        case games.GameEmbedResponse.LaunchError.PROVIDER_ERROR:
        default:
            return 'error-server-desc-with-support';
    }
};

export const getWithdrawalError = function (error: wallet.WithdrawResponse.WithdrawError | wallet.WithdrawalRoyaltyResponse.WithdrawalRoyaltyError | null | undefined, provider: wallet.WithdrawProvider): string {
    switch (error) {
        case wallet.WithdrawResponse.WithdrawError.invalid_iban:
            return 'error-withdrawal-invalid-iban';
        case wallet.WithdrawResponse.WithdrawError.bonuses_unplayed:
            return 'error-withdrawal-bonuses-unplayed';
        case wallet.WithdrawResponse.WithdrawError.billing_failed:
        case wallet.WithdrawResponse.WithdrawError.unknown_payway:
            return 'error-withdrawal-billing-rejected';
        case wallet.WithdrawalRoyaltyResponse.WithdrawalRoyaltyError.invalid_balance:
        case wallet.WithdrawResponse.WithdrawError.invalid_balance:
            return 'error-withdrawal-balance-too-low';
        case wallet.WithdrawResponse.WithdrawError.invalid_credentials:
            if (provider === wallet.WithdrawProvider.WALLETTEC) {
                return 'error-phone-carrier';
            } else {
                return 'error-withdrawal-invalid-credentials-format';
            }
        case wallet.WithdrawalRoyaltyResponse.WithdrawalRoyaltyError.invalid_currency:
        case wallet.WithdrawResponse.WithdrawError.invalid_currency:
            return 'error-withdrawal-invalid-currency';
        case wallet.WithdrawResponse.WithdrawError.account_not_confirmed:
            return 'error-withdrawal-account-not-confirmed';
        case wallet.WithdrawResponse.WithdrawError.email_not_confirmed:
            return 'ui-email-not-confirmed';
        case wallet.WithdrawResponse.WithdrawError.amount_too_low:
            return 'error-withdrawal-amount-to-low';
        case wallet.WithdrawResponse.WithdrawError.amount_too_high:
            return 'error-withdrawal-amount-to-high';
        case wallet.WithdrawResponse.WithdrawError.deposit_not_wagered:
            return 'error-withdrawal-bonuses-unplayed';
        case wallet.WithdrawResponse.WithdrawError.payway_disabled:
            return 'error-withdrawal-payway-disabled';
        case wallet.WithdrawResponse.WithdrawError.has_unprocessed_withdrawals:
            return 'error-withdrawal-has-unprocessed-withdrawals';
        case wallet.WithdrawResponse.WithdrawError.documents_required:
            return 'error-withdrawal-documents-required';
        case wallet.WithdrawResponse.WithdrawError.attached_data_required:
            return 'error-attached-data-required';
        case wallet.WithdrawResponse.WithdrawError.invalid_cashtag:
            return 'error-invalid-cashtag';
        case wallet.WithdrawResponse.WithdrawError.refill_not_allowed:
            return 'error-refill-not-allowed';
        case wallet.WithdrawalRoyaltyResponse.WithdrawalRoyaltyError.user_not_found:
        case wallet.WithdrawResponse.WithdrawError.no_deposits_found:
        default:
            return 'error-register-unknown-error';
    }
};

export const getRefillError = function (error: wallet.RefillResponse.RefillError | null | undefined, provider: wallet.WithdrawProvider): string {
    switch (error) {
        case wallet.RefillResponse.RefillError.order_rejected:
            return 'error-refill-order-rejected';
        case wallet.RefillResponse.RefillError.amount_unavailable:
            return 'error-refill-amount-in-not-available';
        case wallet.RefillResponse.RefillError.invalid_currency:
        case wallet.RefillResponse.RefillError.unknown_provider:
            return 'error-refill-invalid-currency';
        case wallet.RefillResponse.RefillError.invalid_amount:
            return 'error-refill-invalid-amount';
        case wallet.RefillResponse.RefillError.amount_too_low:
            return 'error-refill-amount-to-low';
        case wallet.RefillResponse.RefillError.amount_too_high:
            return 'error-refill-amount-to-high';
        case wallet.RefillResponse.RefillError.wrong_voucher:
            return 'error-refill-incorrect-vaucher';
        case wallet.RefillResponse.RefillError.not_enough_balance:
            return 'error-refill-not-enough-balance';
        case wallet.RefillResponse.RefillError.invalid_credentials:
            return 'error-refill-invalid-credentials';
        case wallet.RefillResponse.RefillError.bonuses_failed:
            return 'error-refill-bonuses-failed';
        case wallet.RefillResponse.RefillError.email_not_confirmed:
            return 'ui-email-not-confirmed';
        case wallet.RefillResponse.RefillError.api_user:
            return 'error-refill-api-user';
        case wallet.RefillResponse.RefillError.pin_not_found:
            return 'error-refill-pin-not-found';
        case wallet.RefillResponse.RefillError.documents_required:
            return 'error-refill-documents-required';
        case wallet.RefillResponse.RefillError.blocked_by_the_terms_of_bonus:
            return 'error-refill-blocked-by-the-terms-of-bonus';
        case wallet.RefillResponse.RefillError.attached_data_required:
            return 'error-attached-data-required';
        case wallet.RefillResponse.RefillError.phone_already_used:
            return 'error-register-phone-used';
        case wallet.RefillResponse.RefillError.invalid_phone:
            return 'error-register-phone-invalid';
        case wallet.RefillResponse.RefillError.invalid_cashtag:
            return 'error-invalid-cashtag';
        case wallet.RefillResponse.RefillError.can_not_refill_with_this_pin:
            return 'error-can-not-refill-with-this-pin';
        case wallet.RefillResponse.RefillError.unknown_payway:
        case wallet.RefillResponse.RefillError.internal_error:
        default:
            return 'error-refill-unknown-error';
    }
};

export const getBonusActivationErrorToken = function (error?: bonuses.BonusCodeActivateResponse.Result | undefined | null) {
    switch (error) {
        case bonuses.BonusCodeActivateResponse.Result.CODE_NOT_FOUND:
            return 'error-refill-promo-code-not-found';
        case bonuses.BonusCodeActivateResponse.Result.CANT_BE_ACTIVATED_AT_THE_MOMENT:
            return 'error-refill-promo-code-cant-be-activated-at-the-moment';
        case bonuses.BonusCodeActivateResponse.Result.CODE_ALREADY_ACTIVE:
            return 'error-refill-promo-code-already-activated';
        case bonuses.BonusCodeActivateResponse.Result.SERVER_ERROR:
        default:
            return 'error-refill-invalid-promo-code';
    }
};

export const getBonusActivationErrorTokenForGame = function (error?: bonuses.BonusCodeActivateResponse.Result | undefined | null) {
    switch (error) {
        case bonuses.BonusCodeActivateResponse.Result.CODE_NOT_FOUND:
            return 'error-refill-promo-code-not-found';
        case bonuses.BonusCodeActivateResponse.Result.CANT_BE_ACTIVATED_AT_THE_MOMENT:
            return 'error-refill-promo-code-cant-be-activated-at-the-moment-in-game';
        case bonuses.BonusCodeActivateResponse.Result.CODE_ALREADY_ACTIVE:
            return 'error-refill-promo-code-already-activated';
        case bonuses.BonusCodeActivateResponse.Result.SERVER_ERROR:
        default:
            return 'error-refill-invalid-promo-code';
    }
};

export const getBonusCancelErrorToken = function (error?: bonuses.BonusCancelResponse.Result | undefined | null) {
    switch (error) {
        case bonuses.BonusCancelResponse.Result.CODE_NOT_FOUND:
            return 'error-refill-promo-code-not-found';
        case bonuses.BonusCancelResponse.Result.CANNOT_CANCEL:
            return 'error-refill-promo-code-can-not-cancel';
        case bonuses.BonusCancelResponse.Result.SERVER_ERROR:
        default:
            return 'error-refill-invalid-promo-code';
    }
};

export const getSignInError = function (error?: users.LoginResponse.LoginError | null | undefined): string {
    switch (error) {
        case users.LoginResponse.LoginError.invalid_credentials:
            return 'error-login-failed';
        case users.LoginResponse.LoginError.blocked:
            return 'error-login-blocked';
        case users.LoginResponse.LoginError.not_found:
            return 'error-login-not-found';
        default:
            return 'error-login-unknown';
    }
};

export const getSignUpError = function (error?: users.RegisterResponse.RegisterError | null | undefined): string {
    switch (error) {
        case users.RegisterResponse.RegisterError.country_code_required_for_phone:
            return 'error-register-phone-required';
        case users.RegisterResponse.RegisterError.email_used:
            return 'error-register-email-used';
        case users.RegisterResponse.RegisterError.forbidden_email:
            return 'error-email-forbidden';
        case users.RegisterResponse.RegisterError.phone_used:
            return 'error-register-phone-used';
        case users.RegisterResponse.RegisterError.invalid_email:
            return 'error-register-email-invalid';
        case users.RegisterResponse.RegisterError.invalid_phone:
            return 'error-register-phone-invalid';
        case users.RegisterResponse.RegisterError.password_too_short:
            return 'error-register-password-too-short';
        case users.RegisterResponse.RegisterError.unknown_country_code:
            return 'error-register-phone-country-not-supported';
        case users.RegisterResponse.RegisterError.unknown_currency:
            return 'error-register-currency-not-supported';
        case users.RegisterResponse.RegisterError.invalid_fields:
            return 'error-user-fields-error';
        case users.RegisterResponse.RegisterError.unknown_error:
        default:
            return 'error-register-unknown-error';
    }
};

export const clearPhoneNumber = function (phone: string | null | undefined) {
    if (phone) {
        return phone.replace(/[ -]|[(]|[)]|[_]|\+/g, '');
    } else {
        return '';
    }
};

export const formatPaywayType = function (type: wallet.PaywayType | null | undefined): string {
    switch (type) {
        default:
        case wallet.PaywayType.MULTI:
        case wallet.PaywayType.OTHER:
            return 'payway-category-other';
        case wallet.PaywayType.EMONEY:
            return 'payway-category-emoney';
        case wallet.PaywayType.BANK:
        case wallet.PaywayType.CARD:
            return 'payway-category-card-and-banks';
        case wallet.PaywayType.MOBILE:
            return 'payway-category-mobile';
        case wallet.PaywayType.CRYPTO:
            return 'payway-category-crypto';
    }
};

export const getPhoneParams = function (phone: NString, countries: site.ICountryInfo[]): PhoneParams | null {
    if (phone && phone !== 'none' && phone !== NASymbol) {
        const split = phone.split(' ');
        const phoneCode = split.splice(0, 1)[0];
        const phoneCodeNum = parseInt(phoneCode, 10);
        const phoneNumber = split.join(' ');
        const country = countries.find(function (c: site.ICountryInfo) {
            return c.phoneCode === phoneCodeNum;
        }) || null;

        return {
            cleanPhoneNumber: clearPhoneNumber(phoneNumber),
            phoneNumber: phoneNumber,
            phoneCode: clearPhoneNumber(phoneCode),
            phoneCountry: country ? country.code! : null
        };
    } else {
        return null;
    }
};

export const muteString = function (value: NString): string {
    if (value) {
        let start = value.length / 3;
        let end = start * 2;

        if (end - start < 1) {
            start = 1;
            end = value.length - 1;
        }

        for (let i = start; i <= end; i++) {
            value = value.substr(0, i) + '*' + value.substr(i + 1);
        }
        return value;
    } else {
        return '';
    }
};

export const mutePhoneCode = function (value: NString) {
    if (value && /^\+[\s?\d]{2,4}[\d|\\*]{3,}/.test(value)) {
        return value.replace(/^\+[\s?\d]{2,4}/, '+***');
    } else {
        return value;
    }
};

export const formatInputDate = function (date: Date) {
    let day: number | string = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    // +1, because jan === 0
    let month: number | string = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    const year = date.getFullYear();

    return day + '.' + month + '.' + year;
};

export const isValidDate = function (date: Date) {
    const now = new Date();
    const deltaYear = 110;

    return date.getFullYear() + deltaYear >= now.getFullYear() && date.getTime() <= now.getTime();
};

export const getPhoneCodeByCountry = function (countryCode: string, countriesInfo: site.ICountriesInfo): number | null {
    const info = (
        countriesInfo.countries!.find((c: site.ICountryInfo) => c.code === countryCode) ||
        countriesInfo.countries!.find((c: site.ICountryInfo) => c.code === countriesInfo.defaultCountry) ||
        null
    );
    return info ? info.phoneCode! : null;
};

export function forceMapToString(map: { [key: string]: any }): { [key: string]: any } {
    for (let key in map) {
        if (map[key]) {
            map[key] = map[key].toString();
        }
    }
    return map;
}

export const formatBonusState = function (state: bonuses.BonusCode.State | null | undefined | -1, isCanceled: boolean): string {
    switch (state) {
        case bonuses.BonusCode.State.BS_PENDING:
            return 'ui-bonus-state-pending';
        case bonuses.BonusCode.State.BS_WAGERING:
            return 'ui-bonus-state-wagering';
        case bonuses.BonusCode.State.BS_WAGERED:
            return 'ui-bonus-state-wagered';
        case bonuses.BonusCode.State.BS_CLOSED:
            if (isCanceled) {
                return 'ui-bonus-state-canceled';
            } else {
                return 'ui-bonus-state-closed';
            }
        case bonuses.BonusCode.State.BS_DECLINED:
            return 'ui-bonus-state-declined';
        case bonuses.BonusCode.State.BS_MANUALLY_CLOSED:
            return 'ui-bonus-state-manually-closed';
        case -1: // for possible bonuses
            return '';
        default:
            console.error('Unknown bonus state: ', state);
            return 'ui-bonus-state-unknown';
    }
};

export const nullDateValue = 315532801;

export const isNullDate = function (date: NLong): boolean {
    return !date || date.equals(nullDateValue);
};

export const isDef = function (value: any): boolean {
    return value !== null && value !== undefined;
};

export type BonusMetaObj = { [k: string]: string } | null | undefined;

export enum BonusType {
    CASHBACK = 'cashback',
    FINAL_BONUS = 'final_bonus',
    TOURNAMENT = 'tournament',
    REGULAR_TOURNAMENT = 'regular_tournament',
    MANUAL_ACTIVATION_REGULAR_TOURNAMENT = 'manual_activation_regular_tournament',
    FOLLOW_UP = 'follow_up_notification',
    SKILLWHEEL = 'skillwheel',
    WITHDRAWALS_INSPECTOR = 'withdrawals-inspector',
    WAGER_BONUS = 'wager_bonus',
    APK_ANDROID_BONUS = 'apk-android-bonus',
    USA_REF_INVITEE = 'usa-ref-invitee',
    USA_REF_INVITER = 'usa-ref-inviter',
    USA_REF_INVITEE_BY_CASHIER = 'usa-ref-invitee-by-cashier',
    EXTERNAL_REG_BONUS = 'external-reg-bonus',
    REG_WAGER_BONUS = 'reg-wager-bonus',
    REG_WAGER_FORCE_REDEEM = 'reg-wager-force-redeem',
    FIRST_DEPOSIT_BONUS = 'first-deposit-bonus',
    FOLLOWING_DEPOSITS_BONUS = 'following-deposits-bonus',
    DEPOSIT_BY_CODE = 'deposit-by-code',
    NO_DEPOSIT_BY_CODE = 'no-deposit-by-code',
    REGULAR_CASHBACK = 'regular-cashback',
    DAILY_BONUS = 'daily-bonus',
    UPDATED_FINAL_BONUS = 'updated_final_bonus',
    BALANCE_WAGER_BONUS = 'balance-wager-refills',
    PIN_WAGER = 'pin-bonus',
    BETS_WAGER_REFILLS_BONUS = 'bets-wager-refills',
    FS_BETS_WAGER_REFILLS_BONUS = 'fs-bets-wager-refills',
    FS_BALANCE_WAGER_REFILLS_BONUS = 'fs-balance-wager-refills',
    NO_DEPOSIT_BY_CODE_BALANCE_WAGER = 'no-deposit-by-code-balance-wager',
    APK_BONUS = 'apk_bonus',
    EXTERNAL_PROMO = 'external-promo-wager-bonus',
    BONUS_BY_CODE_WITH_REQ_DEPS = 'by-code-with-req-deps',
    SAFE_REGISTRATION_BONUS = 'safe-registration-bonus',
    TIMELINE_CASHBACK = 'timeline-cashback',
    LEVER_BONUS_BY_CODE = 'lever-bonus-by-code',
}

export enum BonusFamily {
    REFILLS = 'refills',
}

export enum BonusGroups {
    ACTIVE = 'ACTIVE',
    PENDING = 'PENDING',
    POSSIBLE = 'POSSIBLE',
    ALREADY_RECEIVED = 'ALREADY_RECEIVED',
}

export enum BonusMeta {
    // common info
    BONUS_TYPE = 'bonus_type',
    ENABLED = 'enabled',
    BONUS_TITLE = 'bonus_title',
    PROMO_CODE = 'promo_code',
    PUBLIC_PROMO_CODE = 'public_promo_code',
    CANT_CANCEL_BONUS = 'cant_cancel_bonus',
    SHOULD_SHOW_IN_POSSIBLE_BONUSES = 'should_show_in_possible_bonuses',
    CAN_BE_REACTIVATED = 'can_be_reactivated',
    BONUS_GRADIENT = 'bonus_gradient',
    BONUS_IMAGE = 'bonus_image',
    BONUS_REDEEM_AMOUNT = 'bonus_redeem_amount',
    BONUSES_PAGE_DESCRIPTION = 'bonuses_page_description',
    BONUS_FAMILY = 'bonus_family',
    BANNER_URL = 'bannerUrl',

    // free spins info
    FREE_SPINS_BONUS_AMOUNT = 'free_spins_bonus_amount',
    FREE_SPINS_BONUS_WAGER_MULTIPLIER = 'free_spins_bonus_wager_multiplier',

    // balance bonus info
    BALANCE_BONUS_AMOUNT = 'balance_bonus_amount',
    BALANCE_BONUS_CURRENCY = 'balance_bonus_currency',
    BALANCE_BONUS_WAGER_MULTIPLIER = 'balance_bonus_wager_multiplier',
    BALANCE_BONUS_AMOUNT_EXACT_PERCENT = 'balance_bonus_amount_exact_percent',
    BALANCE_BONUS_AMOUNT_UP_TO_PERCENT = 'balance_bonus_amount_up_to_percent',
    BALANCE_BONUS_AMOUNT_UP_TO_PERCENT_PLAIN = 'balance_bonus_amount_up_to_percent_plain',

    // bonus spins wagering conditions
    WAGERING_SPINS_ALLOWED = 'wagering_spins_allowed',
    WAGERING_SPINS_MIN_SPIN_AMOUNT = 'wagering_spins_min_spin_amount',
    WAGERING_SPINS_MAX_SPIN_AMOUNT = 'wagering_spins_max_spin_amount',
    WAGERING_SPINS_AMOUNT_MULTIPLIER = 'wagering_spins_amount_multiplier',
    WAGERING_SPINS_CURRENCY = 'wagering_spins_currency',

    // bonus bets wagering conditions
    WAGERING_BETS_ALLOWED = 'wagering_bets_allowed',
    WAGERING_BETS_MIN_BET_AMOUNT = 'wagering_bets_min_bet_amount',
    WAGERING_BETS_MAX_BET_AMOUNT = 'wagering_bets_max_bet_amount',
    WAGERING_BETS_AMOUNT_MULTIPLIER = 'wagering_bets_amount_multiplier',
    WAGERING_BETS_CURRENCY = 'wagering_bets_currency',

    // bonus withdrawal conditions
    WITHDRAWAL_CONDITIONS_MAX_WIN_LIMIT = 'withdrawal_conditions_max_win_limit',
    WITHDRAWAL_CONDITIONS_MIN_DEPOSITS_SUM = 'withdrawal_conditions_min_deposits_sum',
    WITHDRAWAL_CONDITIONS_CURRENCY = 'withdrawal_conditions_currency',

    // bonus receive conditions
    RECEIVE_CONDITIONS_PHONE_CONFIRMATION_REQUIRED = 'receive_conditions_phone_confirmation_required',
    RECEIVE_CONDITIONS_EMAIL_CONFIRMATION_REQUIRED = 'receive_conditions_email_confirmation_required',
    RECEIVE_CONDITIONS_PROMO_CODES = 'receive_conditions_promo_codes',
    RECEIVE_CONDITIONS_MIN_DEPOSIT = 'receive_conditions_min_deposit',
    RECEIVE_CONDITIONS_MIN_DEPOSIT_SUM = 'receive_conditions_min_deposit_sum',
    RECEIVE_CONDITIONS_MAX_DEPOSIT = 'receive_conditions_max_deposit',
    RECEIVE_CONDITIONS_CURRENCY = 'receive_conditions_currency',
    RECEIVE_CONDITIONS_PROMO_CODE_REQUIRED = 'receive_conditions_promo_code_required',
    RECEIVE_CONDITIONS_ISSUE_PERIOD = 'cashback_period',
    RECEIVE_CONDITIONS_ISSUE_PERIOD_START = 'cashback_start_date',

    // receive notification
    RECEIVE_NOTIFICATION_CAN_ACTIVATE = 'receive_notification_can_activate',

    // withdrawals inspector
    WI_ACTIVATED = 'activated',
    WI_MIN_SPINS = 'minSpins',
    WI_CURRENT_SPINS = 'currentSpins',
    WI_MIN_WITHDRAWAL = 'minWithdrawal',
    WI_MAX_WITHDRAWAL = 'maxWithdrawal',
    WI_TIME_UNTIL_NEXT_WITHDRAWAL = 'timeUntilNextWithdrawal',

    // wager bonus
    WAGER_BONUS_AMOUNT = 'amount',
    WAGER_BONUS_REDEEM = 'redeem',
    WAGER_BONUS_REDEEM_AMOUNT = 'redeem_amount',
    WAGER_BONUS_FREE_SPINS_REDEEM = 'free_spins_redeem',
    WAGER_BONUS_FREE_SPINS_MULTIPLIER = 'free_spins_multiplier',

    // deposit by code bonus
    PROMOCODE_ACTIVATED = 'promocode_activated',

    GAME_ID = 'gameId',
    POSSIBLE_FREE_SPINS = 'possible_freespins',
    MAX_POSSIBLE_FIXED_BONUS_AMOUNT = 'max_possible_fixed_bonus_amount',
    BONUS_MAX_POSSIBLE_PERCENT = 'max_possible_percent',
    MAX_POSSIBLE_CASHBACK_PERCENT = 'max_possible_cashback_percent',
    AMOUNT_WAGER = 'amount_wager',
    POSSIBLE_FREESPINS_COUNT = 'possible_freespins_cnt',
    FREESPINS_WAGER = 'freespins_wager',
    BONUS_TAGS = 'bonus_tags',
    CURRENT_REDEEM = 'current_redeem',

    WAGERING_FINISH_TIME = 'wagering_time_expiration_stamp',
    BALANCE_AFTER_TIME_EXPIRED = 'balance_after_wagering_time_expired',

    DISPLAY_PRIORITY = 'display_priority',
}

export function processText(valueKey: string, paramsKey: string, meta: BonusMetaObj, defaultVal: string | undefined, appState: AppState): string | undefined {
    if (meta && isDef(meta[valueKey]) && meta[valueKey] !== 'null') {
        const params = meta[paramsKey] ? meta[paramsKey].split('|') : '';
        return appState.t(meta[valueKey]!, params);
    } else {
        return defaultVal;
    }
}

export function processString(key: string, meta: BonusMetaObj, defaultVal: string | null): string | null {
    if (meta && isDef(meta[key]) && meta[key] !== 'null') {
        return meta[key];
    } else {
        return defaultVal;
    }
}

export function processBoolean(key: BonusMeta, meta: BonusMetaObj, defaultVal?: boolean): boolean {
    if (meta && isDef(meta[key])) {
        return meta[key] === 'true';
    } else {
        return isDef(defaultVal) ? defaultVal! : false;
    }
}

export function processLong(key: BonusMeta, meta: BonusMetaObj): NLong {
    if (meta && meta[key]) {
        return Long.fromString(meta[key]);
    } else {
        return null;
    }
}

export function processNumber(key: BonusMeta, meta: BonusMetaObj): number | null {
    if (meta && meta[key]) {
        const value = parseFloat(meta[key]);
        return isNaN(value) ? null : value;
    } else {
        return null;
    }
}

export function processBonusPercentValue(meta: BonusMetaObj, appState: AppState): string | null {
    const exactPercent = processLong(BonusMeta.BALANCE_BONUS_AMOUNT_EXACT_PERCENT, meta);
    const upToPercent = processLong(BonusMeta.BALANCE_BONUS_AMOUNT_UP_TO_PERCENT, meta);
    const upToPercentPlain = processLong(BonusMeta.BALANCE_BONUS_AMOUNT_UP_TO_PERCENT_PLAIN, meta);

    if (exactPercent) {
        return appState.t('ui-amount-range-percent-of-deposit', {value: formatPercent(exactPercent.toNumber())});
    } else if (upToPercent) {
        return appState.t('promo-up-to') + ' ' + appState.t('ui-amount-range-percent-of-deposit', {value: formatPercent(upToPercent.toNumber())});
    } else if (upToPercentPlain) {
        return appState.t('promo-up-to') + ' ' + formatPercent(upToPercentPlain.toNumber());
    } else {
        return null;
    }
}

export function processFreeSpinsAmount(key: BonusMeta, meta: BonusMetaObj): bonuses.IFreeSpins[] | null {
    if (meta && meta[key]) {
        const result: bonuses.IFreeSpins[] = [];
        const freeSpinsMap = JSON.parse(meta[key]) as any;
        const keys = Object.keys(freeSpinsMap);
        for (let mKey of keys) {
            let freeSpins = freeSpinsMap[mKey];
            result.push({
                gameId: freeSpins.game,
                line: Long.fromNumber(freeSpins.lines),
                bet: null,
                count: Long.fromNumber(freeSpins.count),
                countInitial: Long.fromNumber(freeSpins.count_initial)
            });
        }
        return result.length ? result : null;
    } else {
        return null;
    }
}

export function processBonusFreeSpinsTotalAmountString(appState: AppState, freeSpins: bonuses.IFreeSpins[]): string {
    const freeSpinsTotalAmount = freeSpins.reduce((acc, freeSpin) => acc + freeSpin.count!.toInt(), 0);
    return `${freeSpinsTotalAmount} ${appState.t(getFreeSpinsToken(freeSpinsTotalAmount))}`;
}

export function processMetaList(str: NString): any[] {
    if (str) {
        try {
            const obj = JSON.parse(str);
            const keys = Object.keys(obj);
            return keys.map(function (k: string) {
                return obj[k];
            });
        } catch (e) {
            console.error('Unable to process bonus meta list', e);
            return [];
        }
    } else {
        return [];
    }
}

export function processParsedMetaList(list: any[]): any[] {
    const keys = Object.keys(list);
    return keys.map(function (k: string) {
        return list[k];
    });
}

export function checkRequiredFieldsPresent(fields: string[], meta: any) {
    if (meta) {
        const result = [];
        for (let field of fields) {
            if (!meta[field]) {
                result.push(field);
            }
        }
        return result;
    } else {
        return fields;
    }
}

export function millisecondsToHoursString(appState: AppState, milliseconds: number) {
    if (milliseconds === 0) return '';
    const hours = Math.floor(milliseconds / 1000 / 60 / 60) || "0";
    const minutes = (Math.floor(milliseconds / 1000 / 60) - +hours * 60) || "0";
    return appState.t('ui-hours-and-minutes', {hours, minutes})
}
