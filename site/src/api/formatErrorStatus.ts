import {errors} from './proto';
import { translate } from '../state/utils/Localization';

export const formatErrorStatus = (errorStatus: errors.WlError) => {
    return translate(`api-error-status-${errorStatus}`);
};
