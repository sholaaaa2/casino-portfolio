import {observer} from 'mobx-react';
import * as React from 'react';
import cn from 'classnames';
import {Redirect} from 'react-router';
import { Route, Switch, withRouter} from 'react-router-dom';
import AppState from './state/AppState';
import {Notificator} from './state/components/Notificator';
import {BtnScrollUp} from './state/components/BtnScrollUp';
import {
    PaymentSuccess,
    PaymentDecline,
    PaymentFail,
    RefillPage,
    WithdrawPage,
    GamesPage,
    ProfileEdit,
    ProtectedRoute,
    AffiliateProgram,
    ConfirmPage,
    GamePage,
    JackpotPage,
    StaticPage,
    BlogPage,
    ProfileBonuses,
    ProfileBonusBalances,
    RefillPaywayPage,
    WithdrawalPaywayPage,
    SportPage,
    LoginPage,
    RegisterPage,
    RestorePage,
    NoMatchPage,
    LoginViaToken,
    TournamentsListPage,
    TournamentPage,
    VLobbyPage,
    HomePage,
    ResetPasswordPage,
    RegisterExternalPage,
    LiveCasinoPage,
    BonusesPage,
    Sport2Page,
    ProfileDocuments,
    SkillWheelPage,
    FreespinsShopPage,
    LoginViaSocial,
    ProfileEditPhone,
    ProfileMenuMobile,
    ProfilePhone,
    ProfileEditEmail,
    ProfileEmail,
    ProfileEditPassword,
    ProfileDocumentsUpload,
    ProfileEditPersonalData,
    PaymentInProgress,
    ProfileUserId,
    TimelineCashbackPage,
    TournamentsInfoListPage,
    AppInfoPage,
} from './state/pages';
import { ProfileRefSystem } from './state/pages/ProfileRefSystem';
import { CardNotificationScale } from './state/components/CardNotificationScale';
import PilotGamePage from './state/pages/PilotGamePage';
import LobbyGamePage from './state/pages/LobbyGamePage';
import { PageRenderProps, POSSIBLE_LOBBY_GAMES, Routes, SitePages } from './state/meta';
import { ForcedPhoneConfirmationPage } from './state/pages/ForcedPhoneConfirmationPage';
import { KioskLobbyPage } from './state/pages/KioskLobbyPage';

export interface PageProps extends PageRenderProps {
    appState: AppState;
}

@observer
class App extends React.Component<PageProps> {

    componentDidMount() {
        const appState = this.props.appState;
        window.addEventListener('resize', appState.updateViewportSize);
    }

    componentDidUpdate(prevProps: PageProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            this.props.appState.site.updateCurrentPathName(this.props.location.pathname);
        }
    }

    render() {
        const appState = this.props.appState;
        const { initialized } = appState;
        const langRule = '/:lang' + appState.localesGroupsRe + '?';
        const isProfileEditAllowed = appState.site.isProfileEditAllowed;
        const isAffiliateProgramAllowed = appState.site.isAffiliateProgramAllowed;
        const isSiteRegistered = appState.user.isSiteRegistered;
        const isLobbyEnabled = appState.site.isLobbyEnabled;
        const isSportEnabled = appState.site.isSportEnabled;
        const { isAuthorized } = appState.site;
        const { isAnonymousRegistartionEnabled } = appState.signUpActions;
        const showSkillWheel = appState.skillWheel.show;
        return (
                <div
                    className={cn('root-container', {
                        [appState.language]: true,
                    })}
                    onClick={appState.onPageClick}
                >
                    <Notificator appState={appState} allowHTML={true}/>
                    <BtnScrollUp/>
                    <CardNotificationScale appState={appState} viewport={appState.viewport}/>
                    <Switch>
                        <Route exact={true} path={langRule + '/:ref(z[a-zA-Z0-9-_]+/?)?'} render={(props: PageRenderProps) => <HomePage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/confirm'} render={(props: PageRenderProps) => <ConfirmPage appState={appState} {...props}/>}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + Routes.KIOSK_LOBBY}
                                        isAllowed={appState.appSettings.kioskLobby.enabled}
                                        redirectTo={appState.l('/')}
                                        render={(props: PageRenderProps) => <KioskLobbyPage appState={appState} {...props}/>}
                        />

                        <Route exact={true} path={langRule + '/p/jackpot'} render={() => (<Redirect to={appState.l('/jackpot')} />)} />
                        <Route exact={true} path={langRule + '/jackpot'} render={(props: PageRenderProps) => <JackpotPage appState={appState} {...props}/>}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + '/sport/:category?'}
                                        isAllowed={isSportEnabled}
                                        redirectTo={appState.l('/')}
                                        render={(props: PageRenderProps) => <SportPage appState={appState} {...props}/>}
                        />
                        <Route exact={true} path={langRule + '/tvbet'} render={(props: PageRenderProps) => <LiveCasinoPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/pilot'} render={(props: PageRenderProps) => <PilotGamePage appState={appState} {...props}/>}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + '/lobby'}
                                        render={(props: PageRenderProps) => <VLobbyPage appState={appState} {...props}/>}
                                        isAllowed={isLobbyEnabled}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/refill'}
                                        render={(props: PageRenderProps) => <RefillPage appState={appState} {...props}/>}
                                        isAllowed={isSiteRegistered}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/login'}
                                        render={(props: PageRenderProps) => <LoginPage appState={appState} {...props} />}
                                        isAllowed={!initialized ||  !isAuthorized}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                            path={langRule + '/forced-phone-confirmation'}
                            render={(props: PageRenderProps) => <ForcedPhoneConfirmationPage appState={appState} {...props}/>}
                            isAllowed={isAuthorized && appState.site.forcedPhoneConfirmation}
                            redirectTo={appState.l('/')}
                        />
                        <Route exact={true} path={langRule + '/auth/:token'} render={(props: PageRenderProps) => <LoginViaToken appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/social/:socialProvider'} render={(props: PageRenderProps) => <LoginViaSocial appState={appState} {...props}/>}/>
                        <Redirect exact={true} path={langRule + '/login-cashier'} to={appState.l('/login')}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + '/register'}
                                        render={(props: PageRenderProps) => <RegisterPage appState={appState} {...props} />}
                                        isAllowed={!initialized ||  !isAuthorized}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/anonymous'}
                                        render={(props: PageRenderProps) => <RegisterPage appState={appState} {...props} />}
                                        isAllowed={!initialized || (!isAuthorized && isAnonymousRegistartionEnabled)}
                                        redirectTo={appState.l('/')}
                        />
                        <Route exact={true} path={langRule + '/register-external'} render={(props: PageRenderProps) => <RegisterExternalPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/restore'} render={(props: PageRenderProps) => <RestorePage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/reset-password'} render={(props: PageRenderProps) => <ResetPasswordPage appState={appState} {...props}/>}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + '/withdraw'}
                                        render={(props: PageRenderProps) => <WithdrawPage appState={appState} {...props}/>}
                                        isAllowed={isSiteRegistered}
                                        redirectTo={appState.l('/')}
                        />
                        <Route exact={true} path={langRule + '/games/:id?'} render={(props: PageRenderProps) => <GamesPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/game/:id'} render={(props: PageRenderProps) => <GamePage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/p/:id'} render={(props: PageRenderProps) => <StaticPage appState={appState} {...props}/>}/>
                        <Route
                            exact={true}
                            path={langRule + '/blog/:id?'}
                            render={(props: PageRenderProps) => (
                                <BlogPage appState={appState} {...props} />
                            )}
                        />

                        <Route exact={true} path={langRule + '/payments/:provider/success'} render={(props: PageRenderProps) => {
                            return <Redirect to={appState.l('/payment-succeed') + '?provider=' + props.match.params.provider}/>;
                        }}/>
                        <Redirect exact={true} path={langRule + '/success'} to={appState.l('/payment-succeed')}/>
                        <Redirect exact={true} path={langRule + '/payment-success'} to={appState.l('/payment-succeed')}/>
                        <Redirect exact={true} path={langRule + '/payment-decline'} to={appState.l('/payment-declined')}/>
                        <Redirect exact={true} path={langRule + '/payment-fail'} to={appState.l('/payment-failed')}/>

                        <Route exact={true} path={langRule + '/payment-succeed'} render={(props: PageRenderProps) => <PaymentSuccess appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/payment-declined'} render={(props: PageRenderProps) => <PaymentDecline appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/payment-failed'} render={(props: PageRenderProps) => <PaymentFail appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/payment-in-progress'} render={(props: PageRenderProps) => <PaymentInProgress appState={appState} {...props}/>}/>
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit'}
                                        render={(props: PageRenderProps) => <ProfileEdit appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit'}
                                        render={(props: PageRenderProps) => <ProfileMenuMobile appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/user-id'}
                                        render={(props: PageRenderProps) => <ProfileUserId appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/phone'}
                                        render={(props: PageRenderProps) => <ProfilePhone appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit/phone'}
                                        render={(props: PageRenderProps) => <ProfileEditPhone appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/email'}
                                        render={(props: PageRenderProps) => <ProfileEmail appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit/email'}
                                        render={(props: PageRenderProps) => <ProfileEditEmail appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit/password'}
                                        render={(props: PageRenderProps) => <ProfileEditPassword appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/edit/personal-data'}
                                        render={(props: PageRenderProps) => <ProfileEditPersonalData appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/affiliate-program'}
                                        render={(props: PageRenderProps) => <AffiliateProgram appState={appState} {...props}/>}
                                        isAllowed={isAffiliateProgramAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={`${langRule}/${SitePages.PROFILE_REF_SYSTEM}`}
                                        render={(props: PageRenderProps) => <ProfileRefSystem appState={appState} {...props}/>}
                                        isAllowed={appState.site.isRefSystemAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/bonuses'}
                                        render={(props: PageRenderProps) => <ProfileBonuses appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/bonus-balances'}
                                        render={(props: PageRenderProps) => <ProfileBonusBalances appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed && appState.appSettings.showBonusBalances}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/documents'}
                                        render={(props: PageRenderProps) => <ProfileDocuments appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/profile/documents/upload/:type'}
                                        render={(props: PageRenderProps) => <ProfileDocumentsUpload appState={appState} {...props}/>}
                                        isAllowed={isProfileEditAllowed}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={[langRule + '/refill/:payway', langRule + '/refill/:payway/:payways']}
                                        render={(props: PageRenderProps) => <RefillPaywayPage appState={appState} {...props}/>}
                                        isAllowed={isSiteRegistered}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={[langRule + '/withdraw/:payway', langRule + '/withdraw/:payway/:payways']}
                                        render={(props: PageRenderProps) => <WithdrawalPaywayPage appState={appState} {...props}/>}
                                        isAllowed={isSiteRegistered}
                                        redirectTo={appState.l('/')}
                        />
                        <ProtectedRoute exact={true}
                                        path={langRule + '/skill-wheel'}
                                        render={(props: PageRenderProps) => <SkillWheelPage appState={appState} {...props}/>}
                                        isAllowed={showSkillWheel}
                                        redirectTo={appState.l('/')}
                        />
                        <Route exact={true} path={langRule + '/tournaments'} render={(props: PageRenderProps) => <TournamentsListPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/tournaments/:tournamentId'} render={(props: PageRenderProps) => <TournamentPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/bonuses'} render={(props: PageRenderProps) => <BonusesPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/freespins-shop'} render={(props: PageRenderProps) => <FreespinsShopPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/freespins-shop/:shopItemId'} render={(props: PageRenderProps) => <FreespinsShopPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/sp2-page'} render={(props: PageRenderProps) => <Sport2Page appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/timeline-cashback'} render={(props: PageRenderProps) => <TimelineCashbackPage appState={appState} {...props}/>}/>
                        <Route exact={true} path={langRule + '/tournaments-list'} render={(props: PageRenderProps) => <TournamentsInfoListPage appState={appState} {...props}/>}/>
                        <ProtectedRoute
                            exact={true}
                            isAllowed={!initialized || appState.site.appInfoPageIsAllowed}
                            path={langRule + '/app-info'}
                            render={(props: PageRenderProps) => <AppInfoPage appState={appState} {...props}/>}
                            redirectTo={appState.l('/')}
                        />

                        {POSSIBLE_LOBBY_GAMES.map((gameId) =>
                            <Route
                                key={gameId}
                                exact={true}
                                path={langRule + '/' + gameId}
                                render={(props: PageRenderProps) =>
                                    <LobbyGamePage gameId={gameId} appState={appState} {...props} />
                                }
                            />
                        )}

                        <Route render={(props: PageRenderProps) => <NoMatchPage appState={appState} {...props}/>}/>

                    </Switch>
                </div>
        );
    }
}

export default withRouter(App);
