# Casino portfolio





Online Casino

This project is an online casino that I revamped. Within the project, I worked on the following tasks:

- Markup: creating HTML markup and CSS styles for the main casino pages, including the home page, games page, bonuses page, and payment page. I used the Jade template language and Sass preprocessor to speed up and simplify the markup process.
- Design: updating the design of the main casino pages to improve the user interface and brand recognition.
- Language switching: adding language switching functionality to the casino so that users can choose their preferred interface language.
- Redesigning main pages: redesigning the main casino pages to improve their functionality and overall user interactivity. I used React and other modern web development technologies to implement new features and improvements.

The technologies I used for this project include Jade, Sass, and React. I also used Git for version control and managing changes in the project.
