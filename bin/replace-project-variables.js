const path = require('path');
const fs = require('fs');
const basePath = path.join(__dirname, '..');
const variables = require(path.join(basePath, 'site', 'src', 'AppSettings.json'))['globalVars'];

// replace variables in variables values
for (var sourceKey in variables) {
    var sourceValue = variables[sourceKey];
    if (typeof sourceValue === 'string') {
        if (sourceValue === 'auto_year') {
            var date = new Date().getFullYear().toString();
            sourceValue = date;
            variables[sourceKey] = date;
        }

        var re = new RegExp(sourceKey, 'g');
        for (var targetKey in variables) {
            variables[targetKey] = variables[targetKey].replace(re, sourceValue);
        }
    }
}

// replace variables in project
walk(basePath, ['.git', 'node_modules'], function (filePath, stats) {
    var content = fs.readFileSync(filePath, "utf8");
    var shouldWrite = false;

    for (var key in variables) {
        var varValue = variables[key];
        if (typeof varValue === 'string') {
            if (content.indexOf(key) !== -1) {
                var re = new RegExp(key, 'g');
                var value = variables[key] === 'auto_year' ? new Date().getFullYear() : variables[key];
                content = content.replace(re, value);
                shouldWrite = shouldWrite || true;
            }
        }
    }

    if (shouldWrite) {
        fs.writeFileSync(filePath, content);
    }
});


function walk(dir, exclude, callback) {
    fs.readdir(dir, function (err, files) {
        if (err) throw err;

        files.forEach(function (file) {
            if (exclude.indexOf(file) !== -1) return;

            const filepath = path.join(dir, file);
            fs.stat(filepath, function (err, stats) {
                if (stats) {
                    if (stats.isDirectory()) {
                        walk(filepath, exclude, callback);
                    } else if (stats.isFile()) {
                        callback(filepath, stats);
                    }
                }
            });
        });
    });
}