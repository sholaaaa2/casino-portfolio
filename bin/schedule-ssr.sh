#!/usr/bin/env bash
echo "Call SSR with params"
echo $DOMAIN
echo $CI_PIPELINE_ID
curl -d "{\"domain\":\"$DOMAIN\",\"trx\":\"$CI_PIPELINE_ID\"}" -H "Content-Type: application/json" -X POST http://$SSR_SERVER/wg-ssr/process --trace-ascii /dev/stdout