const fs = require('fs');
const path = require('path');

const notificationPath = path.join(__dirname, '../../notifications');

const SHORT_LINK_VAR = '%NOTIFICATION_SHORT_LINK%';
const DOMAIN_SHORT_LINK_VAR = '%DOMAIN_SHORT_LINK%';
let mapOfShortLinks = {};
try {
    mapOfShortLinks = require('./map-of-short-links.json');
} catch (err) {
}

init();

function init() {
    console.log('replace short-links in notifications');
    // region ---- replace short-links in notifications
    walk(notificationPath, ['.git'], function (filePath) {
        replaceShortLinks(filePath);
    });
    // ----endregion
}

function replaceShortLinks(filePath) {
    var content = fs.readFileSync(filePath, 'utf8');
    var shouldWrite = false;

    for (key in mapOfShortLinks) {
        if (filePath.indexOf(key) !== -1) {
            var shortLink = mapOfShortLinks[key];
            var shortLinkRe = new RegExp(SHORT_LINK_VAR, 'g');
            var domainShortLinkRe = new RegExp(DOMAIN_SHORT_LINK_VAR, 'g');
            content = content.replace(shortLinkRe, shortLink).replace(domainShortLinkRe, shortLink);
            shouldWrite = shouldWrite || true;
        }
    }

    if (shouldWrite) {
        fs.writeFileSync(filePath, content);
    }
}

// region ---- utils
function walk(dir, exclude, callback) {
    fs.readdir(dir, function (err, files) {
        if (err) throw err;

        files.forEach(function (file) {
            if (exclude.indexOf(file) !== -1) return;

            const filepath = path.join(dir, file);
            fs.stat(filepath, function (err, stats) {
                if (stats) {
                    if (stats.isDirectory()) {
                        walk(filepath, exclude, callback);
                    } else if (stats.isFile()) {
                        callback(filepath, stats);
                    }
                }
            });
        });
    });
}

// ---- endregion
