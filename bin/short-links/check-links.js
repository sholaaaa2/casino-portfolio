const fs = require('fs');
const path = require('path');

const notificationPath = path.join(__dirname, '../../notifications');

const SHORT_LINK_VAR = '%NOTIFICATION_SHORT_LINK%';
const DOMAIN_SHORT_LINK_VAR = '%DOMAIN_SHORT_LINK%';

init();

function init() {
    checkShortLinksInNotifications();
}

function checkShortLinksInNotifications() {
    console.log('Check short links in notifications');
    walk(notificationPath, ['.git'], function (filePath) {
        var content = fs.readFileSync(filePath, 'utf8');
        if (content.indexOf(SHORT_LINK_VAR) !== -1 || content.indexOf(DOMAIN_SHORT_LINK_VAR) !== -1) {
            console.log(`${filePath} --- Not all short links have been replaced. Please generate map-of-short-links.json`);
        }
    });
}

// region ---- utils
function walk(dir, exclude, callback) {
    fs.readdir(dir, function (err, files) {
        if (err) throw err;

        files.forEach(function (file) {
            if (exclude.indexOf(file) !== -1) return;

            const filepath = path.join(dir, file);
            fs.stat(filepath, function (err, stats) {
                if (stats) {
                    if (stats.isDirectory()) {
                        walk(filepath, exclude, callback);
                    } else if (stats.isFile()) {
                        callback(filepath, stats);
                    }
                }
            });
        });
    });
}

// ---- endregion
