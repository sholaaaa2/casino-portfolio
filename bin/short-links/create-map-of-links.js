const fs = require('fs');
const path = require('path');
const child_process = require('child_process');

const basePath = path.join(__dirname, '../..');
const mailPath = path.join(__dirname, '../../site/public/mail');
const scriptPath = path.join(__dirname, '../../bin/short-links');
const notificationPath = path.join(__dirname, '../../notifications');

const variables = require(path.join(basePath, 'site', 'src', 'AppSettings.json'))['globalVars'];

const DOMAIN_VAR = '%DOMAIN%';
const DOMAIN_SHORT_LINK_VAR = '%DOMAIN_SHORT_LINK%';
const MAP_OF_SHORT_LINKS_FILE = 'map-of-short-links.json';

var mapOfShortLinksByPath = {};
var ATTEMPTS = 3;
var attemptsToGenerateShortUrlByAPI = 0;

init();

function init() {
    loadMapOfShortLinks();

    updateMapForShortLinks(mailPath);
    updateMapForDomainShortLinks(notificationPath);

    fs.writeFileSync(path.join(scriptPath, MAP_OF_SHORT_LINKS_FILE), JSON.stringify(mapOfShortLinksByPath), 'utf8');
}

function loadMapOfShortLinks() {
    try {
        mapOfShortLinksByPath = require(path.join(scriptPath, MAP_OF_SHORT_LINKS_FILE));
    } catch (e) {

    }
}

function updateMapForShortLinks(dir) {
    var files = fs.readdirSync(dir);
    files.forEach(function (file) {
        const filepath = path.join(dir, file);
        var fileDir = fs.readdirSync(filepath);

        for (var i = 0; i < fileDir.length; i++) {
            attemptsToGenerateShortUrlByAPI = 0;
            var filePathForGenerateUrl = path.join(file, fileDir[i]);
            var url = `https://%DOMAIN%/mail/${filePathForGenerateUrl}/index.html`;
            tryGenerateShortUrl(url, filePathForGenerateUrl);
        }
    });
}

function updateMapForDomainShortLinks(dir) {
    walkSync(dir, ['.git'], function (filePath) {
        var content = fs.readFileSync(filePath, 'utf8');
        if (content.indexOf(DOMAIN_SHORT_LINK_VAR) !== -1) {
            var url = `https://${variables[DOMAIN_VAR]}/`;
            var filePathForGenerateUrl = filePath.split('/').slice(-2).join('/');
            tryGenerateShortUrl(url, filePathForGenerateUrl);
        }
    });
}

// region ---- generate short url
function tryGenerateShortUrl(url, filePath) {
    // skip link generation if it is already generated
    if (mapOfShortLinksByPath[filePath]) {
        return;
    }

    var result = generateShortUrl(url);

    attemptsToGenerateShortUrlByAPI++;

    if (result && result.id) {
        mapOfShortLinksByPath[filePath] = result.id;
    } else if (attemptsToGenerateShortUrlByAPI <= ATTEMPTS) {
        tryGenerateShortUrl(url, filePath);
    } else {
        throw 'error tryGenerateShortUrl';
    }
}

function generateShortUrl(url) {
    var urlShortener = `curl -d {\\"longUrl\\":\\"${url}\\"} -H "Content-Type: application/json" -X POST "https://uri8.cc/urlshortener/v1/url"`;
    // replace global value in urlShortener string
    for (var key in variables) {
        var varValue = variables[key];
        if (typeof varValue === 'string') {
            if (urlShortener.indexOf(key) !== -1) {
                var re = new RegExp(key, 'g');
                var value = variables[key] === 'auto_year' ? new Date().getFullYear() : variables[key];
                urlShortener = urlShortener.replace(re, value);
            }
        }
    }
    // run command for generate short link
    var result = runCmd(urlShortener);
    return result ? JSON.parse(result) : result;
}

// ---- endregion

// region ---- utils
function walkSync(dir, exclude, callback) {
    var files = fs.readdirSync(dir);

    files.forEach(function (file) {
        if (exclude.indexOf(file) !== -1) return;

        const filepath = path.join(dir, file);
        var stats = fs.statSync(filepath);
        if (stats) {
            if (stats.isDirectory()) {
                walkSync(filepath, exclude, callback);
            } else if (stats.isFile()) {
                callback(filepath, stats);
            }
        }
    });
}


function runCmd(cmd) {
    var resp = child_process.execSync(cmd);
    var result = resp.toString('UTF8');
    return result;
}

// ---- endregion
