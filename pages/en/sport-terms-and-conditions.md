# RULES
The present Rules of bets accepting, funds deposit into and funds withdrawal from a gaming account of the betting company "%SITE_NAME%" (hereinafter referred to as “Rules”) set out an order of betting in terms of bets accepting, winning payout, adjustment of disputes, details of betting on special types of bets and sports. The present Rules adjust all other relations between the bet parties — the betting company "%SITE_NAME%" and clients.


1. [General notions and terms](#general-notions)
2. [General provisions](#general-provisions)
3. [General betting rules](#general-betting-rules)
4. [Types of bets](#types-of-bets)
5. [Limitations on including some event outcomes](#limitations)
6. [Results of matches, event start date and time, adjustment of disputes](#results-of-matches)
7. [Sport betting](#sport-betting)
    1. [American Football](#american-football)
    2. [Australian football](#australian-football)
    3. [Basketball](#basketball)
    4. [Baseball](#baseball)
    5. [Bandy](#bandy)
    6. [Beach soccer](#beach-soccer)
    7. [Curling](#curling)
    8. [Darts](#darts)
    9. [Football](#football)
    10. [Floorball](#floorball)
    11. [Field hockey](#field-hockey)
    12. [Formula1](#formula1)
    13. [Futsal](#futsal)
    14. [Handball](#handball)
    15. [Ice Hockey](#ice-hockey)
    16. [Rugby](#rugby)
    17. [Snooker](#snooker)
    18. [Tennis](#tennis)
    19. [Volleyball](#volleyball)
    20. [Water Polo](#water-polo)
    
## <a name="general-notions"></a>GENERAL NOTIONS AND TERMS
**Bet** — an agreement on winning based on risk and concluded between a client and the betting company according to the regulations, whereby a result of the given agreement depends on the event relative to which it is not known whether it takes place or not. Betting has a form of stakes taken from clients under the terms proposed by the betting company.

**Client** — a person who bets with the bookmakers company on an event outcome.

**Outcome** — a result of the event (events), on which a bet is placed.

**Pre-match** (Line, Early market) — a combination of events, possible outcomes of these events, odds of possible outcomes of these events, their closing date and time when the company quits to take bets on outcomes of these events.

Bets cancellation — a void bet excluded from any bet calculations and winning payout. Under the terms of the present Rules in case a bet is cancelled, an agreement between the betting company and a client is considered invalid and a refund is to be paid.

**Regular time** — the duration of the match in accordance with the rules of competition excluding extra time/overtime, penalty shoots etc.

**Insurance** - The Insurance feature gives you the opportunity to return the amount of the rate less the cost of insurance in case you change your mind. It is only necessary to choose the rate for which you wish to make insurance, and you can immediately receive the funds deposited to the account.

The Insurance option is valid for all Single and Multi bets made in Live and Pre-match mode. The Insurance function is available for all bets against which you will see a special Insurance symbol.

**How is the amount of Insurance calculated?**

For example, you are making an single bet of 1,000 rubles for the victory of England in the football match "England - Denmark" (W1), with a coefficient of 1.5. In case of victory, the win will be 1500 rubles - 1,000 x 1.5. After the bet is completed, the coefficient on W1 rises from 1.5 to 2.2. In this case, the program calculates the ratio of the coefficients (1.5 / 2.2), which is multiplied by the bet amount, then the determined commission fee is deducted from the result obtained, This example is 15%: (2.2 / 1.5 x 1 000) -15% = 898. Thus, you have the opportunity to take the amount received by the Insurance, 898 rubles, before the outcome of the event becomes known. If the coefficient is lowered, the program calculates the amount of the Insurance in the same way.

If you make an multi bet in the amount of 1,000 rubles for 3 different events, with a total odds of 4.78

1st event (Total O (2.5)) - 1.6
2nd event (W2) - 1.3
3rd event (W1) - 2.3
In case of victory, the winnings will be 4780 rubles - 1,000 x 1.6 x 1.3 x 2.3.

If two of the three events of your multi bet have already won and you have doubts about the outcome of the 3rd event or you simply do not want to wait for the 3rd event to end, you can take advantage of the Insurance option and return a part of the bet before the outcome of the 3- th event.

If during this time the coefficient for "W1" of the 3rd event rises from 2.3 to 2.8, then the winnings are calculated as follows: The total ratio is divided by the changed coefficient of the 3rd event (4.78 / 2.8), the resulting ratio is multiplied by the bet amount, then the determined commission fee is subtracted from the total result, in this example equal to 15%: (4.78 / 2.8 x 1 000) - 15% = 744. Thus, you have the opportunity to take the amount received by the Insured, 744 rubles, before the outcome of the event becomes known.

In case of increasing the coefficient, the program calculates the amount of the topic Insurance If the odds remain unchanged, only the commission fee set by the organizer is deducted from the amount of your bet.

The Organizer has the right, at its own discretion, to limit, terminate the Insurance, temporarily or indefinitely, and also has the right to change the amount of the commission.

## <a name="general-provisions"></a>GENERAL PROVISIONS
1. The betting company accepts bets on the outcome of sporting and other events with a deliberately unknown result.
2. The present Rules came into force on Dec 31, 2015.
3. The betting company "%SITE_NAME%" accepts bets on sports and other events, which take place in any country of the world.
4. Bets are accepted from individuals over 18 years old or the legal age of majority in his/her jurisdiction, whichever is greater, who agree with the Rules proposed by the betting company. Clients are responsible for breach of this regulation.
5. Bets are not accepted from persons who has relation to the event on which he/she places bets (athletes, coaches, referees, etc.), persons whose participation in the agreement with the betting company is prohibited by applicable law. A client is responsible for breach of present rules. In the case of violation of these regulations, the company reserves the right to refuse payment of any winnings and already invested stakes as well as to cancel any bets. The company takes no responsibility for knowing if the user is a member of one of the mentioned groups. This means that the company is entitled to undertake any necessary measures at any time after the user has become known as being a member of one of the mentioned groups.
6. Terms of bets accepting (odds, handicaps, totals, limits on maximum stake, etc.) can be changed after any bet, provided that terms of the bets, which had been accepted before, remain unchanged. Before betting a client should find out all changes in the current line.
7. The betting company reserves the right to decline a bet without giving a reason. In case of staff’s errors or software failures (obvious typos in the program, odds, totals, handicaps, mismatch of odds in different positions, not typical, wrong odds, etc.) and other facts confirming incorrectness of the bets (including online bets) which are either: a. materially different from those available in the general market at the time the bet was made, or b. clearly incorrect as opposed to the ones available in the general market at the time the bet was made, we will calculate them with the odds ″1″ (stake refunded). Examples of circumstances where this would apply are: a. the odds are recorded as 40 when the odds offered in the general market are 4. b. the margins for handicap betting have been reversed. c. the margins for totals are explicitly wrong, e.g. the odds are recorded as 685 when the odds offered in the general market are 68.5. d. the scores of the games have been reversed, e.g. the score is recorded as 3:0 when the score is 0:3. In this case the correspondence of odds is required. e. betting effected after the event has started.
8. In case of any amendments to the present Rules, clients are notified by corresponding announcements. Bets which are accepted from a specified date are subject to the Rules changed. Bets which had been accepted before remain unchanged.
9. A client is responsible for the privacy of his/her login and password of his/her gaming account. In case a client’s login or password becomes known to a third party, this client should immediately contact the administration of the betting company. All bets registered at the betting company are valid. Bets cancellation is possible only in accordance with the present Rules.
10. If an Internet connection failed while accepting a bet confirmation by a client, it does not constitute grounds for a cancellation of this bet.
11. Any bet is a confirmation of the fact that a client agrees to the present Rules and accepts them.
12. The event results declared by the betting company are the only grounds for calculations of bets and winnings. All claims to event results, date and actual time of an event beginning are considered together with the package of official protocols of corresponding sports federations.
13. All disputes are adjusted within 5 days as from the date of a bet.
14. Administration of the betting company reserves right to block the opportunity of client’s account to place bets, cancel all bets or address to law enforcement bodies in case when the client is suspected in fraud.
15. The betting company is not responsible and doesn’t accept any claims concerning correctness of translation from foreign languages of team names, player surnames, places where competitions take place. The information given in a tournament headline has an auxiliary character. Possible mistakes in such information do not constitute grounds for bets refund.
16. A client can register only one account. Only one account per person, family, household, computer or IP address or credit/debit card.
17. Once registered a client is not allowed to create another account (under a new name, a new e-mail, etc.). If this rule is violated, the administration of the company reserves the right to block the newly registered account for a period of investigation (up to 2 months). If this incident of multiple registration is confirmed, the company has a right to cancel all bets of this new account (winnings). The Company reserves the right to ask the client for more personal information (passport data, registration etc.) as well as to undergo a videoconference. The administration can make an exception to this rule on client’s request.
17. In connection with the cases: different types of fraud, multiple accounts by a single user, repeated violations of rules betting company has the right to carry out the identification process by video conference.
18. The betting company does not recommend two or more clients to place bets from one IP-address (from the same computer or the same local area network) in order to avoid suspicion of collusion (cheating). In such cases it is necessary to reconcile the possibility of the game with "%SITE_NAME%" by explaining the circumstances. The betting company has the right to block the account of such clients and ask them to send the documents verifying their registered identity.
20. The betting company reserves the right to block the accounts of clients who have reported false data, as well as to deny them the payment of any winnings. At the request of the company the client must present an official document with a photograph, identifying his identity (a passport copy), and also proof of authenticity of the indicated address data and telephone.
21. Clients are required to bet only on their own behalf. Recurring bets containing the same selections from one or more clients can be recognized as invalid and get canceled by the company. Even after the official outcome of such bets is known, in cases when the company is confident that Users have acted in collusion or as a syndicate, or suspicious bets have been made by one or more clients in a short period of time, such bets may be considered invalid and get canceled by the betting company.
22. The client is responsible for maintaining the confidentiality of any information related to their account, and shall take all measures to prevent the use of personal accounts by unauthorized persons. Any operations confirmed by the username and password of the account holder shall be valid and have legal force.
23. The client is responsible for maintaining the confidentiality of any information related to their e-mail, and shall take all measures to prevent the use of e-mail address by third parties. "%SITE_NAME%" is not responsible for any loss or damage resulting from the connection between the bookmaker office and the client using a registered e-mail address.
24. All operations on playing accounts are made in the currency chosen by the User during registration.
25. Payout might be checked within 20 (twenty) calendar days by the Security Department.
26. The betting company reserves the right to inform clients about special promotions and new products by telephone and e-mail.

## <a name="general-betting-rules"></a>GENERAL BETTING RULES
1. The betting company accepts bets given in a list of events with corresponding winning odds.
2. The minimum bet on any single event is 10RUR/1USD/1EUR/5UAH/30KZT/1TJS.
3. A maximum bet is determined for each event by the betting company and depends on sports and an event. For several events with different limits on a maximum bet included in an multi or a system bet a maximum bet is set equal to a minimum rate.
4. The maximum winning for one bet is 5 000 000 RUR(or its currency equivalent).
5. The betting company reserves the right to limit a maximum or minimum bet on special events as well as to limit or increase a maximum bet of a certain client without any notification and giving reasons.
6. Acceptance of multiple bets on one outcome or a combination of outcomes from one player can be limited by a decision of the betting company.
7. A bet is considered to be accepted after its registration on the server and its on-line confirmation. Registered bets are irreversible and irrevocable.
8. Bets are accepted only in the amount that doesn’t exceed a current balance of a client’s gaming account. Once a bet has been confirmed, its amount is withdrawn from a gaming account. Once a bet has been calculated, a winning amount is deposited into a client’s gaming account.
9. Bets are accepted before an official event beginning; event date, time and corresponding comments, given in the Early market, are approximate. Any bet placed after an event has started will be cancelled, except for Live-bets (i.e. bets placed in the course of a match). Such bets are considered to be valid till the end of a match.
10. Should a bet be cancelled, then a single bet payment will be refunded. If a bet on one or several events included in multi and system bets is cancelled, no winning calculations will be made.
11. If winnings are calculated incorrectly (e.g. event results were entered by mistake), such winnings will be recalculated. Provided that the bets placed during the period between incorrect calculation and recalculation are considered to be valid. In case the balance of a gaming account is negative after recalculation, a client can’t bet until he deposits into his gaming account.
12. All sports events are considered to be postponed or cancelled only in accordance to official documents provided by championship organizer, official internet sites of sports federations, sports clubs and other official sources of sports information.
13. The betting company reserves the right to cancel the bets even after the end of the event if these bets are won by the User as a result of a technical failure or an obvious error.does not accept any bets or provide services to customers on credit.
14. The Company does not accept any bets or provide services to customers on credit. All bets must be confirmed with funds existing in the account of the User.
15. A bet is to be cancelled in case a client deliberately misinformed the stuff of the bookmakers company by means of representing false information and claims in respect of bets, winning payout, event results and other information and claims of similar nature. The above mentioned incidents are valid in respect of minors under 18 and their parents as well.

## <a name="types-of-bets"></a>TYPES OF BETS
“Single” bet is a bet on a particular event outcome. A winning amount on a single bet is equal to the product of a bet amount and odd on the given outcome.

“Multi ”you need to predict the results of more than one independent events at the same time (the maximum number of events in the accumulator bet is 30). If at least one event in the multi bet has been predicted wrong, the whole bet is considered to be lost. Any combination of any independent events can be included in the multi bet if nothing else is intended by the rules of the games organized by regulations. Besides, multi bets may not be accepted on certain events by the discretion of the organizer. Dependent events cannot be included in the multi bet, - the same event cannot participate in the multi-bet more than once. If there were related or dependent events in the multi bet, the event with the highest odds remains for calculation. The odds of a multi bet are equal to the product of the odds of all the included events. The winning amount on a multi bet is equal to the product of the bet amount and odds of the multi bet.

“System” is a set of Multi bets of a particular set of events. Each combination of the system is calculated as a separate multi bet. To win in the system it is sufficient that the indicated number of predictions of the total number be correct (it is defined by name, that is, for example, in the system “3 of 4” it is necessary that 3 of the 4 predictions prove correct, for “5 of 7” it is necessary properly to predict the outcome of the five events of 7 options, etc.). It must be taken into account that because of the fact that only a part of the total bet amount is specified on each of the multi of the system bet (proportional to the number of options) then the winnings by such a system bet will be less than the possible general winnings by the multi bet. In some cases, when not all outcomes are predicted, the amount of winnings may be less than the amount of the bet, but in contrast to multi bets, a part of the amount will be offset on account of correctly predicted outcomes.The maximum number of variants in the system bet is equal to 1000.

When defining the final winning odds for all types of bets, the peculiarities of payment odds for some types of outcomes are taken into account (for example bets made on the victory of a participant in the competition, when more than one participant is winner, bets with handicaps made on the victory of participant in the competition, when the size of handicap coincides with the result).

The main types of outcomes for betting:
The bet “Home wins” is designated in the line as “1”.
The bet “Draw” is designated in the line as “X”.
The bet “Away wins” is designated in the line as “2”.
The bet “Home wins or a draw” is designated in the line as “1X”. To win this bet it is necessary to predict if the first team wins or there is a draw.
The bet “Home wins or away wins” is designated in the line as “12”. To win this bet it is necessary to predict if the first or the second team wins, i.e. a draw outcome is excluded.
The bet “Away wins or a draw” is designated in the line as “X2”. To win this bet it is necessary to predict if the second team wins or there is a draw.
The bet “A team (a player, a racer, etc.) wins with allowance for handicap” is designated in the line as “handicap” (odds are coefficients offered for each handicap).

Handicap is an advantage or disadvantage of a team (a player, a racer, etc.) expressed in goals, points, sets, seconds, etc. offered by the bookmakers company on a specified bet.

The outcome of an event with allowance for handicap is determined by the addition of handicap to an actual result. If the received result is in favour of a chosen team (a player, a racer, etc.), the bet is considered to be won. If not, the bet is considered to be lost. If the received result with allowance for handicap is a draw, the winning coefficient is equal to “1”.

The bet “Under, over” (on total)" is designated in the line as "Total“.This bet is on total of goals, points, games, etc. scored, collected, played by teams (players, etc.). To win this bet it is necessary to predict how many goals or points are scored and games played over/under “Total” given in the line. At defining a result the company takes into account playing time of a match, which is regulated by the present Rules separately for each kind of sport, unless otherwise stated in the line.

When individual total is determined, goals scored in an opponent’s gate are only included.A bet on individual total includes number of goals, points, games, etc. scored, played, etc. by a team (a player, etc.).The betting company offers two ways to bet on total — on two (“under” or “over”). If your result coincides with that offered by the bookmakers company, the winning coefficient on bets on “under” or “over” is to be equal to “1”.

The bet “Correct score”. It is necessary to predict a score of a match (on regular time).

The bet “Team comparison” (periods, quarters, sets, games, innings)“.
To win this bet it is necessary to predict which of both halves, periods, quarters and etc. in a match is the highest scoring one or which of them have similar results.

The bet “Players comparison by score” on tournament results.
To win this bet it is necessary to predict which of the players scores more on the results of a tournament. At comparison of players scoring (number of scored goals, pucks, points and so on) on the results of a tournament, in case of equality of these parameters, the winning coefficient is equal to “1”. Shootouts are not included. Number of matches played by a competitor is not included. If a player does not take part in any match, the winning coefficient is equal to “1”.

The bet "Head-to-head on the results of a championship".To win this bet it is necessary to predict which team has the best final position in a tournament table of a championship. If number of points is equal, the winning odd is equal to "1“.In case a team does not take part in any match of a tournament, the winning odd is equal to “1”.

“Home team — away team”- it is necessary to predict, which team scores more goals, pucks, including handicap and total. If at least one match is cancelled, postponed, not played to its end and is considered to be failed, all bets are calculated with the coefficient equal to “1”.

In case of betting on the “Odd number/Even number of Total”, regardless of the sports and the proposed position (including total goals/cards/fouls etc.) if the result is “0”, the bets are calculated as “Even number”.

In case if any market has the potential outcome “Any other”, this outcome includes all participants of the event, not listed in the options of that market separately.

In case of game on any event in the “additional markets” of tours (total number of goals, wins, losses, draws, angular, yellow cards etc.), only those matches of the tour are taken into account which are included in the daily line.

The betting company can also offer other types of bets.

## <a name="limitations"></a>LIMITATIONS ON INCLUDING EVENT OUTCOMES
It is forbidden to include different events that relate to the same match In Multi , not even the ones which are interdependent directly. If any events that relate to the given match were included in such bet this bet is refundable, even if the computer program of bet accepting did not block this bet during its submission. The company reserves the exclusive right to determine whether the events are connected.

## <a name="results-of-matches"></a>RESULTS OF MATCHES, EVENT START DATE AND TIME, ADJUSTMENT OF DISPUTES.
1. If a result of the completed event is for some reason cancelled or changed after some time (disqualification of a team or players, referees, carrying conditions, etc.), bet calculations are made on the basis of an initial (actual) result.
2. While calculating bets an actual start time of an event is taken into account which is approximately defined under documents of organizing officials; if there are no such approved documents, then it is defined on the basis of official sites of sports federations, sites of sports clubs and other sources of sports information. Valid results are those that are taken from official protocols and other official information sources immediately after an event has been completed.
3. When settling results company will do its utmost to attain itself to information obtained first hand (during or exactly after the event has been concluded), through TV transmissions, streaming (web-based and through other sources) as well as official sites. Should this information be omitted from first hand viewing and/or official sources and/or there is an obvious error in the information included in the sources above, the settlement of the bet offer will be based on other public sources. In this case the disputed bets can not be appealed.
4. No responsibility is assumed by the company for the mismatch of date and time to an actual beginning of an event. Date and time of an event beginning represented in the line are approximate. At bet calculations an actual start time of an event is accepted. It is determined under official protocols of organizing officials.
5. Claims to event results are accepted within 5 calendar days from the moment of event completion under official protocols on event results of organizing officials.
6. Calculation of bets is considered to be final in the case the calculation is correct at the time it has been made and/or does not contradict any of the rules. Calculation of bets will not include any changes deriving from and/or attributable, but not limited, to disqualifications, penalisations, protests, sub judice results and/or successive changes to the official result after the event has been completed and a result has been announced, even preliminarily.
7. Bets placed after an event had begun are calculated with the coefficient equal to "1“(except for Live-bets); the winning coefficient on them in multi bets is equal to “1”. If a client places a bet on the event the result which is known to him, such bet will be cancelled. In this case the bookmakers company makes a decision after a special internal investigation. All actions related to this bet will be temporary suspended.
8. An event is considered interrupted if there was no play for an indispensable amount of playing time specified according the Rules of the company, even if the result at which the event was interrupted is later confirmed to be final.
9. In case the event which has not yet started is postponed for no more than 72 hours the bets remain, and in case 72 hours pass the bets are refunded. If during 72 hours there is information about postponing the event for more than 72 hours the final decision about the bets being valid or refunded is made by the bookmakers’ office if nothing else is intended by regulations. If the matches of NBA, NHL, NFL, baseball (MLB) do not take place on the appointed day, the bet amounts are refunded immediately the next day, except for the cases when the specified date was wrong.
10. The started event may be interrupted, i.e. may not play up to the end for some reason. The event which has been interrupted and has not been finished during 24 hours is considered completed if played for no less than:
    * Soccer — 70 min
    * Futsal — 30 min
    * Basketball NBA (National Basketball Association) — 40 min
    * Eurobasketball — 35 min
    * Hockey NHL (National Hockey League) — 54 min
    * Bandy — 80 min
    * Grass Hockey - 60 m
    * Eurohockey — 50 min
    * American football — 50 min
    * Baseball — 5 periods (innings)
in these cases the outcome (the actual result) of the event considered to be completed is the result recorded at the moment of interruption of the event (except tennis, table tennis, beach volleyball, badminton). In all other cases the event is considered to be not completed, including interruption of the match at the time of tie in those sports where tie is not allowed (basketball, baseball, NHL hockey playoffs, etc.), and the bets (including online bets) must be refunded.
11. If the event has been interrupted and is considered to be not completed, those outcomes which are determined at the time of its interruption and do not depend on the final result of the event (for example, the team will score, who will score the first goal the outcome of the first half, etc.) are taken as a basis for calculating the bets (including online bets).
12. If a bet has been made on any asset before the expiration date chosen by the participant, the bet is no longer circulated on the world market, the bet is payable with odds 1 only at the end of the expiry period.
13. While calculating the bets on the statistics of one round (tour) or one game day, if one or several events (matches) are not considered to be completed the bets are calculated by odds “1”, except the bets the results of which are definitely clear regardless of whether other events (matches) have taken place or not.
14. If a match is not completed and is considered as incomplete, then all bets on it are refundable, (except what is mentioned in point 6.6-6.8) even if the result, at which the event was suspended, later is confirmed as final.
15. If it does not contradict the special rules of individual sports, the result announced on the day of the match is taken for the result of the match. Any possible changes in the result, conditioned by the revision of the results of the event by governing bodies and disciplinary penalties of teams, for the purpose of betting, are not taken into account, and the initial results of the calculation of bets remain in force.
16. If it does not contradict the special rules of individual sports, when an event participant for whatever reason cannot take part in the event which was bet on, the bet is considered valid and lost by the client. The exceptions are the following cases:
    * The event/tournament is canceled;
    * The event /tournament is considered invalid;
    * The location of the event was changed;
    * The event/tournament is suspended.
17. In case of bets “Who is taller” meaning bets with two outcomes, if one of the participants refuses to take part before the event/tournament, the bet is refundable.
18. If a match is not played to its end and is considered to be incomplete, then those outcomes, which are definitely determined by the time of its interruption (e.g. outcome of the first half, 1st goal and its time, etc.) are accepted for bet calculations. The winning coefficient on other bets are equal to “1”.
19. In competitions, where such terms as “home team” and “away team” are used, in case a match is moved to a neutral field, bets are valid; at rearranging of a competition to a contestant field (except for the case when both teams are from one city), all bets are refundable. Only If an international match is moved to another country, all bets are refundable.
20. If more than one player or a team is declared to be a winner of a competition, odds on these players are divided by the number of winners. E.g. if two players are declared to be winners, odds on these bets are divided by two.
21. If a player, who is a member of a team (a soccer player, an ice hockey player, a basketball player, etc.) does not take part in a match, the winning coefficient on bets on him is equal to “1”, unless otherwise specified.
22. In disputable matters without precedent a final decision is taken by the bookmakers company.
23. If data do not correspond to each other in several information sources (date, time, a result, a name of a team), the bookmakers company suspends winning payouts until a complete investigation into data authenticity is made. If a result of the completed event given at an official site differs from TV-broadcasting data, then the company reserves its right to make bet calculations according to TV-broadcasting data.
24. If in the indicated event/match more than one member with the same surname is present and during bet acceptance the full name of the athlete or other features identifying them uniquely have not been indicated, all bets are refundable.
25. If in the name of the match or in the name of the position one or more entrants are indicated in the line incorrectly (except for errors in translation), that is if the other team/entrant of represented tournament is indicated instead of the necessary one, all the bets on that event are recognized as invalid and a return is made. Only "%SITE_NAME%" has the right to determine the events belonging to this category.
26. In cases when the specific details of the team such as a particular age group (for example 18 years youth teams), gender (women) or the status of the team (reserve) are not indicated in the line resulting in the marking of the other team in the line instead of the declared one, all bets on this match are refundable. In other cases when incomplete name or possible grammar errors are indicated in the line, all bets remain in force. If the gender of teams is not indicated in the line, it is considered that the gender of teams is male. In case women’s teams are participating in a match it is necessary to have a detailed indication about it in the line otherwise bets on this match are a subject to return.
27. If the place of conducting a meeting is not indicated in the line, in the list of events the host of the field is indicated firstly, except for competitions of American types of sports. All the information contained in the “cap” to a particular sport, carries an ancillary character. Possible errors in the given information are not a ground for the return of bets.
28. If the match began from the moment at which it was previously interrupted, and in the line that was not stipulated, the bets are refundable.
29. Bet offers which originally require from the participant(s) to compete in two or more stages/legs to advance into a subsequent phase/round of a competition will remain valid regardless of any postponement/movement of the actual match dates, given that the said match(es) actually takes place within the competition.
30. A bet on "To Qualify" market originally requiring just one stage/leg to advance to a subsequent phase/round of a competition (including any eventual prolongations/additional matches, e.g. replays) will be declared void if the outcome of the said match is not determined within more than 72 hours after its supposed start time.

## <a name="sport-betting"></a>SPORT BETTING


### <a name="american-football"></a>AMERICAN FOOTBALL
A match consists of 4 periods of 15 minutes each, with an interval after the second period. Each aforementioned period is called «quarter». If the score is tied at the end of regular playing time in a NFL (the National Football League) match, 15 minutes overtime is appointed. In this case the first team to earn points is the winner. In the games of the regular season if the score is tied after overtime, a draw is appointed, and in play-offs extra overtimes are appointed until there is a winner.

### <a name="australian-football"></a>AUSTRALIAN FOOTBALL
A match consists of 4 quarters of 20 minutes each, only regular time counts.

### <a name="basketball"></a>BASKETBALL
A game of basketball consist of four quarters, each 10 (European basketball) or 12 (American basketball, NBA) minutes long. Only "actual playing time" is calculated. There are intervals of 2 minutes each between the periods. There is a 15 minutes interval between the 3rd and 4th periods (second half). If the score is tied at the end of playing time, 5 minutes long overtime is assigned. In case the score is tied at the end of the overtime, extra quarters are assigned until the winner is determined. All bets on basketball are identical to bets on football matches, except If selections for the event include Draw (X), bets on the winner, on halves or on quarters are settled in accordance with the regular time only

### <a name="baseball"></a>BASEBALL
Each game is played in innings, in every inning both teams play in turns on offense and defense. Usually the game consists of 9 innings. If there is a tie at the end of the last inning, play continues into extra innings. Baseball doesn’t allow a draw, extra innings are appointed until there is a winner.

Bets are accepted on the final result of a match with all the probable additional periods (innings) taken into account. If the match has been postponed or moved to another day the bet amount is refunded. If two similar matches included in the program of the day have taken place on the same day, the result of the first one is taken into account. If the match has started and has not been completed the bets are calculated according to the paragraph 6.6-6.8 of these regulations.

The following kinds of bets can be placed on baseball:

* Win of the first team (participant) of the event (column “W1” in the line).
* Win of the second team (participant) of the event (column “W2” in the line).
* Win of the first team (participant) of the event with the handicap taken into account (the handicap appears in the column “H1” of the line, and the odds - in the column “Odds1”).
* Win of the second team (participant) of the event with the handicap taken into account the handicap appears in the column “H2” of the line, and the odds - in the column “Odds2”).
* The number of match points is over the specified number (the total number of points appears in the column “Total” of the line, and the odds - in the column “Over”).
* The number of match points is under the specified number (the total number of points appears in the column “Total” of the line, and the odds - in the column “Under”).
* Bets are accepted on if the total points of the match will be even or odd.
* Bets are also accepted on other probable events in baseball, which will be offered in the program.



### <a name="bandy"></a>BANDY
Is a winter team sport played on the ice rink between 2 teams of 11 players per side, including the goaltender. The objective of the game is to throw the ball into the opposing team’s goal using sticks (to score a goal). The winner is the team which scores more goals. A match consists of 2 halves of 45 minutes each, with an interval of up to 20 minutes. In case a winner is required overtime of 2 halves of 15 minutes each is appointed and the play continues till the 1st scored goal.

The extra time and penalties are taken into account only for bets on entering the next round, tournament winner, etc.

### <a name="beach-soccer"></a>BEACH SOCCER
is a team sport based on the traditional football rules. The game is held on sand beaches. Each team consists of 5 players, including the goalkeeper. The substitutions are unlimited. The game consists of 3 periods each 12 minutes long, with 3 minutes intervals between the periods. Beach football doe son allow a draw. Overtime of 3 minutes is assigned until “golden goal” is scored; if no team scores, 9-meter shootout is assigned.
All bets are calculated on the basis of results obtained by the end of the match’s main time (36 minutes play time, 3 periods, 12 minutes each), if not stated otherwise.
The added time and penalty shootout are taken into account only for bets on match winner, entering the next round, tournament winner, etc.

### <a name="curling"></a>CURLING
A game consists of 10 separate periods called ends. Points are awarded in each end. The winner is the team which gets the highest number of accumulated points at the completion of ten ends. If the score is tied at the completion of the scheduled ends, play continues with extra end(s) and the team to scores first wins the game. Each team uses a set of 8 stones per end, 16 stones in total are played during an end. Points are counted after all 16 stones have been played. The team that puts a stone closest to the centre is the winner. That team gets a point for each stone that’s closer to the centre than that stone of the opponent’s which is the closest to the centre.

### <a name="darts"></a>DARTS
Darts combines several variations of games (301/501, Round, Big Round, 27, 1000, 5 lives, sector 20 etc.) where players throw darts to the dartboard. The standard dartboard is divided into numbered sections from 1 to 20, usually of white and black colours. The central circle is divided into a green outer ring worth 25 points and a red or black inner circle worth 50 points. Hitting the outer portions of the sections doubles the point value of that section, hitting the inner portions of the sections triples the point value of that section. The outer and inner portions are traditionally coloured red and green. Hitting outside the outer wire scores nothing. Any dart that does not remain in the board also scores nothing. Usually player’s points are counted after 3 shots. After that it’s the opponent’s turn to play. The highest score possible with three darts is 180 (when all three darts land in the triple 20).

### <a name="football"></a>FOOTBALL
Football Betting is accepted during regular time, excluding extra time and penalty shoot-outs, except special stipulated cases indicated on "%SITE_NAME%" In these rules the regular time should be counted taking into account the added time in the match, the rates on the events in each half take into account the added time of each half determined by the referee.

Bets on the actual result of the event

Win of the first team (participant) of the event (“W1” column in the line).
Draw (column “X” in the line).
Win of the second team (participant) of the event (column “W2” in the line).
The first team (participant) of the event will not lose (column “1X” in the line).
Any of the teams (participants) of the event will win (column “12” in the line)
The second team (participant) of the event will not lose (column “X2” in the line).
Bets on win of the participant of the event with the handicap taken into account

Win of the first team (participant) of the event with the handicap taken into account (the handicap appears in the column “H1” of the line, and the odds - in the column “Odds1”).
Win of the second team (participant) of the event with the handicap taken into account (the handicap appears in the column “H2” of the line, and the odds – in the column “Odds2”).
Bets on the total number of goals scored, points earned, games played, etc. by the participant(s) of the event.
The total number of match goals is over the specified number (the total number of goals appears in the column “Total” of the line, and the odds – in the column “Over”).
The total number of match goals is under the specified number (the total number of goals appears in the column “Total” of the line, and the odds – in the column “Under”).
The total number of match goals is even or odd (score 0:0 is considered even).
Bets on the scored goals in the halves (in which half more goals will be scored) 
The following 3 variants are offered:
In the second half more goals will be scored than in the first half: 1 < 2
The same number of goals in both halves: 1 = 2 (e.g. when the match ends with a score 0:0).
In the first half more goals will be scored than in the second half 1>2
Bets on the score of the match
It is offered to choose the correct score of the match according to the probable variants included in the program. In case of choosing all the other probable scores not included in the program outcome “any other score” of the event is offered.

Bets on the result of the half-time and full-time

It is offered to predict the result of the half-time and full-time at the same time. The following 9 variants are possible:

* W1W1 – win of first team in half-time and full-time,
* W1X - win of first team in half-time and draw in full-time,
* W1W2 - win of first team in half-time and win of second team in full-time
* XW1 – Draw in half-time and win of first team in full-time
* XX – Draw in in half-time and full-time,
* XW2 – Draw in half-time and win of second team in full-time,
* W2W1 - win of second team in half-time and win of first team in full-time,
* W2X - win of second team in half-time and draw in full-time,
* W2W2 - win of second team in half-time and full-time.
It is offered to predict if the first team will score in the match or not, if the second team will score or not, if both teams will score, or if at least one of them will not score or at least one of them will score, if any goals will be scored or not in the first half, if any goals will be scored or not in the second half

It is offered to predict which team will score the first goal in the match, in which offered period of time the first goal will be scored (the interval of minutes is indicated inclusive), how the first goal will be scored - from game not with head, with head, from a free kick, from a penalty spot, or own goal). If the match ends with a score 0:0, the bets are considered to be lost.

it is offered to predict which team will score the second goal in the match, in which offered period of time the second goal will be scored (the interval of minutes is indicated inclusive). If the match ends with a score 0:0, 0:1 or 1:0, the bets are considered to be lost.

it is offered to predict which team will score the last goal in the match, in which offered period of time the last goal will be scored (the interval of minutes is indicated inclusive): If the match ends with a score 0:0, 0:1 or 1:0, the bets are considered to be lost.

The only goal scored is not considered to be the last goal.

Own goal is the goal scored into the own goal. The own goal is considered to be on behalf of the team in favour of which the goal was recorded:

It is offered to predict if there will be a penalty kick in the match or not.
It is offered to predict if there will be a send-off (red card) in the match or not.
It is offered to predict if the number of cautions (yellow cards) will be over or under the offered total, which team’s player will be cautioned first in the match (in case the event in both teams is recorded at the same minute according to the report of the match, the bet is calculated by odds “1”), if there will be no caution, in which offered period of time a player of any team will be cautioned first (the interval of minutes is indicated inclusive). If a player was sent off after two cautions, during calculating the bets associated with cautions only one of them is taken into account.
When calculating bets associated with sending off and cautions only those cards are taken into account which were shown to the players directly involved in the match at that moment.

It is offered to predict if the number of substitutions in the match will be over or under the offered total.

It is offered to predict which team will make the first substitution in the match (in case according to the report of the match the event is recorded at the same minute for both teams, the bet amount is refunded), in which offered period of time one of the teams will make the first substitution (the interval of minutes is indicated inclusive), if the substitute will score in the match or not. If there is no bet offered on no substitutions in the match, in case of no substitutions made by the teams the bet is calculated by odds “1”.

It is offered to predict if the number of corner kicks in the match will be over or under the offered total.

It is offered to predict which team will take the first corner in the match, in which offered period of time the first corner will be taken. If there is no bet offered on no corners in the match, in case of no corners taken by the teams the bet is calculated by odds “1”.

It is offered to predict if the number of free kicks in the match will be over or under the offered total.

It is offered to predict if the number of offsides in the match will be over or under the offered total.

It is offered to predict if the time of ball possession by the first team, expressed in percentage, will be over or under the offered number (percentage).

It is offered to predict if the specified player will score into the rival’s goal or not. If the specified player does not play at all (does not participate in the match) the bet is calculated by odds “1”

The own goal isn’t taken into account.
It is offered to predict if there will be:

A double – two goals scored by the same player,
A hat-trick – three goals scored by the same player,
A poker - four goals scored by the same player The outcomes of these events are calculated in the following way: the prediction of the bet on hat-trick does not also mean prediction of the bet on double, or the prediction of the bet on poker does not also mean prediction of the bets on double and/ or hat-trick.
It is offered to predict if the team will win having been behind, or not. The bet is considered won if the team had the first goal scored in its goal but in the end won the match.

t is offered to predict which team will kick off the match. When calculating bets (including online bets) on the outcomes of this event the following information sources are taken as a basis:

The TV channels indicated by the organizer,
The websites indicated by the organizer,
The information provided by the official organizer or organizers of the match. If the outcome of this event is not determined based on the above-mentioned information sources the bets are calculated by odds “1” (one).
It is offered to predict how many minutes the referee will add to the regular time of the match.

The bets placed on this event are calculated in the following way:

According to the time (minutes) shown by reserve referee on the light board.
According to the information about the added time shown on TV.
If at least one of the above-mentioned variants does not appear on the screen, the bets on this event are calculated by odds “1”.
If the added time shown differs from the actually played added time, the added shown time is taken into account.
If the information about the added time shown on TV or by reserve referee on the light board is changed afterwards for some reason (increased or decreased), the bets are calculated according to the initially (first) shown information.
Bets are also accepted on other probable events in football matches, which will be offered in the program.


### <a name="floorball"></a>FLOORBALL
A floorball match is officially played in 3 periods lasting 20 minutes each, with 2 intervals of 10 minutes each between the periods, only regular time counts. In case of a tied score at the end of a match, if a winner is required play continues into an overtime of 10 minutes. Should the overtime not decide the game either, penalty shots are assigned.


### <a name="field-hockey"></a>FIELD HOCKEY
Is a team sport played between 2 teams of 11 players per side, including the goaltender. The game is held on a natural or artificial grass field. Like in any other kind of hockey, players use hockey sticks. The objective of the game is to score a goal against the opposing team using the sticks. The winner is the team which scores more goals against the opposing team. A match consists of 2 halves of 35 minutes each, with an interval of 10 minutes. If the score is tied at the end of play a draw is assigned (in some tournaments play continues into additional time (overtime) and bullets). Players with the exception of the goaltender are not allowed to touch the ball with hands and feet. A goal is scored when an attacker strikes the ball from within the striking circle.


### <a name="formula1"></a>FORMULA 1
Is auto circle racing with special cars. Formula 1 is held annually and consists of a series of races known as Grands Prix. The world champion is determined at the end of the year. Grands Prix consists of free series, qualification series, and race series. Points are awarded to the top ten finishers as follows: 25-18-15-12-10-8-6-4-2-1. The finishers who take top 3 places stand on the podium.

The warm-up lap is included in the race.
If both racers retire, the racer who completed more laps pursuant to the official protocol, is considered winner.
If the driver is classified, it means he finished the race

### <a name="futsal"></a>FUTSAL
A match consists of 2 halves of 20 minutes each, with 15 minutes interval between the halves. Only regular (actual playing) time counts. If a winner is required the play continues into 2 overtimes of 2 halves of 5 minutes each. Should the overtimes not decide the game either, 6-meter shoot-out is assigned.


### <a name="handball"></a>HANDBALL
A handball match consists of two periods of 30 minutes, with 10 minutes intervals between the periods. A tied score is possible, however if a winner is required 2 overtime halves of 5 minutes each are appointed (with a 1-minute break). Should the first overtime not decide the game either, the second overtime with the same conditions is appointed after 5 minutes. Handball rules provide 3 types of personal sanctions:

Warning (yellow card),
2-minute suspension,
Disqualification (red card, suspension till the end of the match).
Handball allows a 7-meter line throw, in which case player has a right to throw the ball from the 7-meter line.


### <a name="american-football"></a>BEACH FOOTBALL
The game consists of 3 periods each 12 minutes long, with 3 minutes intervals between the periods. Beach football doe son allow a draw. Overtime of 3 minutes is assigned until “golden goal” is scored; if no team scores, 9-meter shootout is assigned.

### <a name="ice-hockey"></a>ICE HOCKEY
A hockey game consists of 3 periods, each 20 minutes long. Only "actual playing time" is calculated. The intervals between periods last 15 minutes each. If the score is tied at the end of 3 periods, additional time (overtime) is appointed. In case the tie is not broken after overtime, penalty shots (bullets) are appointed. The need of overtime, as well as its duration and the necessity and number of penalty shots is agreed individually and must comply with the given competition regulations.

In matches of Russian Championship and NHL bets are accepted with additional time (overtime) taken into account, except for the cases specified in the offered line. In NHL playoff matches or other championships that do not allow a draw, the participant is warned beforehand that only bets on regular playing time are accepted. Bets on hockey matches in the championships of other countries or world championships and bets on bandy are only accepted with regular taken into account (without overtimes), except for the cases specified in the program.

The following kinds of bets can be placed on the outcome of hockey match:

Win of the first team (participant) of the event (column“W1” in the line).
Draw (column “X” in the line).
Win of the second team (participant) of the event (column “W2” in the line).
The first team (participant) of the event will not lose (column “1X” in the line).
Any of the teams (participants) of the event will win (column “12” in the line)
The second team (participant) of the event will not lose (column “X2” in the line).
Bets on win of the participant of the event with the handicap taken into account:

Win of the first team (participant) of the event with the handicap taken into account (the handicap appears in the column “H1” of the line, and the odds – in the column “Odds1”).
Win of the second team (participant) of the event with the handicap taken into account (the handicap appears in the column “H2” of the line, and the odds - in the column “Odds2”).
In hockey matches buying of handicap with half a goal is also offered. In this case the odds are reduced accordingly.

Bets on the total number of goals scored, points earned, games played, etc. by the participant(s) of the event:

The total number goals in matches is over the specified number (the total number of goals appears in the column “Total” of the line, and the odds - in the column “Over”).
The total number of goals in matches is under the specified number the total number of goals appears in the column “Total” of the line, and the odds - in the column “Under”). In hockey matches buying of total is offered as well. In this case the odds are reduced accordingly.
The total number of match goals is even or odd (score 0:0 is considered even). Bets on the result of the first period and entire match.
Bets on the result of the first period and entire match.

It is offered to predict the result of the first period and entire match at the same time. The following 9 variants are possible:

* W1W1 - win of first team in the first period and entire match,
* W1X - win of first team in the first period and draw in the entire match,
* W1W2 - win of first team in the first period and win of second team the entire match,
* XW1 - draw in the first period and win of first team in the entire match,
* XX - draw in the first period and entire match,
* XW2 - draw in the first period and win of second team in the entire match,
* W2W1 - win of second team in the first period and win of first team the entire match,
* W2X - win of second team in the first period and draw in the entire match,
* W2W2 - win of second team in the first period and entire match.
It is offered to predict which team will score the first goal, in which offered period of time the first goal will be scored (the interval of minutes is indicated inclusive). 
Bets are also accepted on other probable events in hockey matches, which will be offered in the program.


### <a name="rugby"></a>RUGBY
The match consists of 2 halves of 40 minutes each (overtime and compensated time not included) with intervals of 5 to 10 minutes. Overtimes are appointed if a winner is required.
Rugby has several variations, the most widespread and famous of them is the Rugby Union (often referred to simply as rugby or rugby 15). The second well-known variation is the Rugby League (rugby13).The main difference between these variations is the number of players, 15 and 13 accordingly.

### <a name="snooker"></a>SNOOKER
Snooker is a type of billiards. The match consists of odd frames. The player who scorew most points wins the frame, and the winner of the match is the player who won so many frames that even if he/she loses the remaining ones that player won’t lose the match. For example, a 25 frames match is won by the player to first win 13 frames.
The match begins with 15 red balls set in a triangle and 6 balls of different colours set on certain places of the table. The white ball is used to strike the colour balls. The players must knock the red and colour balls into the pockets. As long as reds are on the table, the pocketed colour balls shall be taken out of the pockets and returned to their original spots on the table. In case of a tied score the match is finished by the black ball. There is a possibility of stalemate in snooker when striking red balls does not progress the frame. In this case the frame restarts on players’ and referee’s agreement.
Points awarded for balls:
Red – 1 point
Yellow – 2 points
Green – 3 points
Brown – 4 points
Blue – 5 points
Pink – 6 points
Black – 7 points

### <a name="tennis"></a>TENNIS
Tennis tournament consists of sets, which in their turn consists of games, and games consists of points. Players take up places on opposite sides of the net. One player is designated the server and puts the ball into play, in other words that player serves. The opposing player is the receiver. The object of players is to send the ball over the net and into the opponent’s court so that the opposing player cannot return the ball, meanwhile the ball must not get out of the court. The opposing player must return the ball before it touches the ground more than once. The ball can also be returned without touching the ground. If the server makes a mistake, his/her opponent gets a point, and the score is 15-0. In case of another mistake the score is 30-0, then 40-0, after which a won point means a won game: 1-0. Obviously, during a game the score can be 15-15, 30-15, 30-30, 40-30, etc. It must be noted that in case of a 40-40 tie in a game it continues until one of the players gets a 2 points advantage over the opponent. Score is not calculated in numbers after 40. The player to first win 6 games (only if the opposing player won no more than 4 games) wins the set (e.g. 6:1, 6:2 etc.). If the game score is 5:5 one of the players must win the next two consecutive games (i.e. 7:5). If the game score is 6:6 “tie break” is played until 7 points are won (but with an advantage of no less than 2 points, e.g. 7:5). If a player wins tie-break, he/she wins the set with 7:6 score.
To win a tennis match players must win 2 sets (in all women’s competitions and most of men’s competitions) or 3 sets (in the most prestigious tournaments like the Grand Slam, Davis Cup). At the end of the most prestigious tournaments the decisive set (i.e. if the set score is 1:1 (for women) or 2:2 (for men)) tie-break is not played and the match continues until one of the players gets a 2 games advantage.
Tennis is played on various courts, e.g. clay, grass courts.
A tennis match is considered to have started if at least one point has been played.
If one of the competitors is disqualified for any reason, refuses to play or is unable to continue the match (the match ends prematurely), then all bets (including live bets) are settled as follows:
Bets (including bets made on statistical data - total aces, total double errors, etc.), which are unequivocally considered valid at the time of the match termination, based on the actual recorded results (according to the format of the match) are subject to settlement. The remaining bets are refunded (settled with the odds of "1")
The bets placed on the outcome of the match - "W1", "W2", are settled if the first set of the match was fully played and completed, and the player refused to continue the game after the end of the first set. The loser is the player who for any reason is disqualified, refuses to play or is unable to continue the game. If the match ends (prematurely ends) before the end of the first set, then the bets placed on the outcome of the match - "W1", "W2", are returned (settled with the odds of 1).
Bets logically determined at the time that the match is stopped. For example, the player refuses to continue the game when the score is 4:4 (15:0). In this case, the bets placed on the total of the first set of 9.5, the odds for the first set of -3.5 (+3.5) are settled, and the bets made on "W1", "W2" of the first set, total of the first set of 10.5, total of the first set of 12.5, handicap of the first set of 1.5, the handicap of the first set is 1.5, the "W1", "W2" of the match, as well as the bets made on the handicap and the total of the whole match are returned (settled with the odds of 1).
In the case of incorrectly specified number of sets in a match, the bet made on total and odds is returned. The bet made on the outcome of the match remains intact. The incorrect indication of type of cover of a championship can not become the basis for the cancellation of bets.

If a match has been postponed or interrupted, then bets (including Live bets) remain in effect until the end or continuation of the tournament or one of the players no longer refuses to play.

If before the match, one of the tennis players refuses to play for any reason, is unable to participate in the match or is disqualified, then the bets placed for this match are eligible for a return (settled with the odds of 1).

The handicap and the total in the tennis match shall be indicated in the games. If in a tennis match in the decisive set the "Super tie-break" is played, then the handicap and total are settled as points.

A tennis match is considered started if at least one point has been played.In case one of the participants is disqualified for any reason in the started tennis match, or refuses to play, or is unable to continue the match (the match ends untimely) all bets (including online bets) are calculated as follows: bets (including bets placed on statistical data - total of the ace, total of double falls, etc.) which are definitely considered to have taken place at the moment of the termination of the match based on the actual registered result (according to the format of the match), are subject to calculation. The rest of the bets are refunded (the bets are calculated by odds “1”).

Bets placed on the result of the match to be “W1”, “W2” are subject to calculation if the first set was played in full and completed, and the participant has refused to continue the match after the termination of the first set. The participant is considered to have lost if he is disqualified for any reason, refuses to play or is unable to continue the match. If the match is stopped (terminated untimely) until the end of the first set, the bets placed on the result of the match to be “W1”, “W2” are refunded (calculated by odds “1”).

Suppose the participant refuses to continue the match when the score is 4:4 (15:0). In this case the “W1”, “W2” bets placed on first set, first set total 10.5, first set total 12.5, first set handicap 1.5, first set handicap -1.5, match “W1”, “W2”, match total 21.5, match handicap -3.5, match handicap 3.5, even or odd of the total of set and match are subject to refund (calculated by odds “1”).

All the bets placed on first set total 6.5, first set total 7.5, first set total 8.5, first set total 9.5, first set handicap 2.5, first set handicap 3.5, first set handicap 4.5, first set handicap 5.5, first set handicap 2.5, first set handicap -3.5, first set handicap -4.5, first set handicap -5.5, bets placed on the result of t 1-8 games of the first set, are subject to calculation. Bets placed on “W1”, “W2” of 9th game of the first set are subject to be refunded, bets placed on “W1”, “W2” of the first point of the 9th game are subject to calculation.

In case of incorrectly specified number of sets in the match the bet amount on handicap and total is refunded. The bet on the result of the match remains valid.

Wrong indication of court coverage in any championship cannot be basis for considering the bet void.

If a tennis match has been delayed or interrupted, the bets (including On-line bets) remain valid up to the end of the tournament to which it refers until the match continues or one of the players refuses to continue.

If one of the tennis players refuses to participate in the match before it starts, or is unable to participate, or is disqualified, the bets on that match are refunded (calculated by odds “1”). In tennis handicap and total are indicated in games.

If “super tie break” is played in the decisive set of the tennis match, then handicap and total are calculated by points.

The following kinds of bets can be placed on a tennis match:

Win of the first participant of the event (column “W1” in the line)
Win of the second participant of the event (column “W2” in the line)
Win of the first participant of the event with the handicap taken into account (the handicap appears in the column “H1” of the line, and the odds - in the column “Odds1”).
Win of the second participant of the event with the handicap taken into account (the handicap appears in the column “H2” of the line, and the odds - in the column “Odds2”).
The number of games is over the specified number (the total number of games appears in the column “Total” of the line, and the odds - in the column “Over”).
The number of games is under the specified number (the total number of games appears in the column “Total” of the line, and the odds - in the column “Under”).
Bets on the correct score in a tennis match (all the unfinished games and sets are in favor of the winner). The corresponding columns in the line are marked as 2:0, 2:1.
The Organizer at its sole discretion may set bets on other probable events in tennis, for example: total of the ace, total of double falls, total of tie-break.


### <a name="volleyball"></a>VOLLEYBALL
A volleyball match has no time limitation and is played in sets. Each set continues until one of the teams scores 25 points. In case the team doesn’t win by two points (25։24) by that time, the play continues until one team wins by two points or more. Teams play until one of them wins 3 sets, so the maximum amount of sets is 5 (3:2). In case of 2:2 score the defining 5th set continues until one of the teams scores 15 points while keeping the aforementioned mandatory condition of winning by two points (at least 15:13). In case of 15:14 score teams play until one of them gets a 2 points advantage over the opponent.

The handicap and total for a volleyball match are specified in points.

The following kinds of bets can be placed on volleyball:

Win of the first team (participant) of the event (column“W1” in the line).
Win of the second team (participant) of the event (column“W2” in the line).
Win of the first team (participant) of the event with the handicap taken into account (the handicap appears in the column “H1” of the line, and the odds - in the column “Odds1”).
Win of the second team (participant) of the event with the handicap taken into account ((the handicap appears in the column “H2” of the line, and the odds - in the column “Odds2”).
The number of match points is over the specified number (the total number of points appears in the column “Total” of the line, and the odds - in the column “Over”).
The number of match points is under the specified number (the total number of points appears in the column “Total” of the line, and the odds - in the column “Under”).
Bets on a certain score in a volleyball match. The corresponding columns in the line are accordingly marked as 3:0, 3:1, etc.
Bets are also accepted on other probable events in volleyball, which will be offered in the program.

### <a name="water-polo"></a>WATER POLO


The match is played in 4 quarters. In Olympic water polo tournaments each quarter is 8 minutes. Only “actual playing time” counts. Each team is allowed 30 seconds to shoot the ball. If there is a tie at the end of regulation time, 2 overtime periods are appointed, each 3 minutes long (total 6 minutes). Overtime is appointed until there is a winner.
