1. Time of the tournament: <b>from {start-date} to {end-date} ({timezone})</b>.
2. Players registered on the site {site-name} are allowed to participate in the tournament.
3. Users with the active bonus cannot take part in the tournament.
4. Replenishment and withdrawal of funds as a part of the competition should be carried out only by e-money.
5. Participants with any account currency are allowed to take part in the tournament.
6. The prize fund determined by of all participant bets of at least {spin-min-amount} and no more {spin-max-amount} for games with spins and at least {bet-min-amount} and not more than {bet-max-amount} in games with bets (or equivalent in the participant's currency) made by participants of the competition during the period of the competition in the following slots (games): {games-list}.
7. A participant can place bets both in one of these slots and in several ones simultaneously.
8. The minimum bet that will be taken into account for each participant in the competition is {spin-min-amount} in games with spins and {bet-min-amount} in games with bets, the maximum bet is {spin-max-amount} in games with spins and {bet-max-amount} in games with bets (or equivalent in the member's currency).
9. By betting lower {spin-min-amount} in games with spins and {bet-min-amount} in games with bets or higher {spin-max-amount} in games with spins and {bet-max-amount} in games with bets, the participant doesn't leave the competition, but such bets won't be counted in the rating calculation of the participant.
10. The prize pool will  be divided {prize-pool-distribution-length} of the most active participants in the following size:
{prize-pool-distribution}
11. Players rating is determined by the points they receive in games of the tournament. 100 points = 1 {currency}.
12. All prizes will be credited to the winners' accounts in their current game currency, if all conditions of the competition are subjected within 24 hours after the end of the competition.
13. The site administration has the right to change the contest rules without notifying the participants.
14. The site administration reserves the right to view transactions and participant logs at any time and for any reason. If the administration reveals a violation of the rules by the participant of this contest, as well as the general rules and conditions of the site, fraud using errors or software vulnerabilities, the participant's account will be blocked and all winnings will be canceled.
15. The participant is prohibited to register more than one login to take part in the tournament. If it is revealed that the participant has more than one login, all bets will be canceled and logins will be blocked.
16. There are general rules of the site for the participants in the contest.
17. By participating in the tournament, the user agrees with these rules.