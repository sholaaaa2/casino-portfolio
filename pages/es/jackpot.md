**Progressive jackpot** — it's a main prize for all slots and games. Win amount increases with each played spin. So the more players place bets, the faster jackpot grows at %SITE_NAME%.

Jackpot amount is shown in rubles. You just need to play slots and games to scoop a Jackpot. The more bets you place, the higher is the chance to win a Jackpot.  

Jackpot amount grows with each placed bet and each new spin. If main prize is won, it will start accumulating again from some initial amount.  