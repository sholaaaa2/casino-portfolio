## Active bonuses 		
	
### First deposit bonus	
	
* To get the bonus you need to make the deposit in the amount upwards of 100 RUB and not exceeding 20000 RUB or equivalent in the currency of registration.	
* Maximum bonus amount: 100% of the deposit amount.	
* For turnover of the bonus in full you need to place bets in the amount which exceeds 35 times the amount of the received bonus.
* Limit imposed on the maximum bet during bonus turnover: 300 RUB or equivalent in the currency of registration.	
	
### Bonuses on the second and subsequent deposits:	
* To get the bonus you need to make a deposit in the amount upwards of 100 RUB and not exceeding 60000 RUB or equivalent in the currency of registration.	
* The bonus amount depends on the deposited amount: 	
	* 5%  in case the top-up amount is in the range from 100 RUB (inclusive) to 600 RUB or equivalent in the currency of registration;
	* 10% in case the top-up amount is in the range from 600 RUB (inclusive) to 6000 RUB or equivalent in the currency of registration ;
	* 15% in case the top-up amount is in the range from 6000 RUB (inclusive) to 60000 RUB or equivalent in the currency of registration;
* Turnover coefficient for getting the bonus also depends on the deposited amount:	
	* х10  in case the top-up amount is in the range from 100 RUB (inclusive) to 600 RUB or equivalent in the currency of registration;
	* х15 in case the top-up amount is in the range from 600 RUB (inclusive) to 6000 RUB or equivalent in the currency of registration;
	* х20 in case the top-up amount is in the range from 6000 RUB (inclusive) to 60000 RUB or equivalent in the currency of registration;
* Limit imposed on the maximum bet during bonus turnover: 300 RUB or equivalent in the currency of registration.	
	
### Bum-bonus:	
* To get the bonus you need to make the deposit in the amount upwards of 100 RUB or equivalent in the currency of registration.	
* Maximum bonus amount: 33% of the deposit amount.	
* For turnover of the bonus in full you need to place bets for the amount which exceeds 50 times the amount of the received bonus.
* Limit imposed on the maximum bet during bonus turnover: 300 RUB or equivalent in the currency of registration.	
	
### Special bonuses and promotions:	
* Information about special bonuses and promotions will be posted on this page or will be sent as informational message to the e-mail address;	
* Validity period of special bonuses and promotions may be limited, information about the time-frame will be provided additionally	
	
## General terms	
	
* All bonus offers are strictly limited to one for: an individual and his/her family, home address, e-mail address, IP-address, telephone number, settlement account number, credit or debit card number, e-wallet number, payment system account (Neteller, Skrill, etc.), one electronic device (computer, mobile phone, tablet, etc.).	
* Bonuses may be used by all players, except the cases, when restrictions in respect of certain countries are imposed on the bonus, and the player is the citizen of such a country.	
* Every bonus provides for the requirement of a minimum deposit, necessary for receipt of the bonus, limit on the maximum possible bonus amount, bonus turnover terms, limit on the maximum bet amount during bonus turnover, limit on the duration, i.e. validity period of the bonus, unless specified otherwise (detailed information is contained in the description of every bonus).	
* The limit on the maximum bet amount during bonus turnover is applied to all granted bonuses, unless specified otherwise.	
* The win received by using the bonus becomes available for withdrawal only after fulfilment of all the rules of bonus turnover.
* If a Player requested a withdrawal of funds, but the amount of bets made since the last deposit is not more than the one-time amount of this deposit, then the administration has the right to reject the withdrawal until the player makes the required amount of bets.
* Validity period of a bonus — unlimited, unless specified otherwise.	
* Upon expiration of the term of the bonus and in case the bonus remains active and the terms of bonus turnover haven't been fulfilled, all bonus funds and wins shall be cancelled.	
* When using the bonus in the course of the game, the bets are at first placed by using the actual funds and only then - by using the bonus funds.	
* The player is entitled to cancel any bonus, without embarking on its turnover; no own funds of the player are lost in this case.
* The player is entitled to opt out of receiving any bonuses from the casino.	
* The maximum bet for bonus turnover amount is 300 RUB or equivalent in the currency of registration.	
* Please, pay attention to the fact that bet amount is factored in full during bonus turnover not in all the games:	
	* In case of a slot game 100% of the bet are factored in 
* The casino is entitled to cancel any bonuses and wins, as well as to seize all funds on the account or to block the profile in case the funds were received by fraud means or with violation of these rules.
* The casino reserves the right to enter amendments and additions into these rules at any time.	
* In case of detection of suspicious operations on the player's account, facts of violation of the bonus policy or the rules of the casino, the company is entitled to conduct the necessary inspection in respect of such an account. Upon request of the company the client shall present the official document with the photo, identifying his/her personality, as well as the evidence of authenticity of the specified data and telephone. In the course of the inspection the security services may request other documents, if necessary.	
* The company shall keep secret the personal data of the user. Any information received from the user may be used only for conducting of the necessary inspections.