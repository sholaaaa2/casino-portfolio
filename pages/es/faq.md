## Frequently asked questions
### How do you register with the casino?
Registration has never been so easy. Click «Log in» button on any page of the site, choose «Registration» tab and fill out all the needed fields in the form which opens. After filling it out click «Register» button. After that the confirmation message shall be sent to the e-mail you registered with. To confirm registration you just need to follow the link in the message.
### I forgot the password, what should I do?
To reset the password you need to open the authorization form and click «Forgot?» link. Then you need to enter your e-mail address or telephone number and click «Reset» button. After that follow the instruction which will be sent to you e-mail address or telephone.
### I forgot my e-mail, what should I do?
You'll have to go through the identification procedure. The procedure is not regulated and it is unique for each client. In any case we'll do our best to help you. Please, try not to forget your login.
### Should the personality be confirmed by documents?
Certainly not. We respect your privacy and don't require to confirm your personality by documents. It may be needed only in absolutely extraordinary, exceptional cases.
### How quickly are the funds credited to the account?
Almost instantly. It depends on the operation speed of the payment provider.
### How quickly are the funds withdrawn?
Almost instantly. It depends on the operation speed of the payment provider. We don't process requests for payment manually, it is done automatically.
### How do I get a bonus?
You may read the detailed terms of getting bonuses on the «Bonus policy» page.
### What is the bonus turnover?
The bonus turnover takes place in the course of the game in the games permitted by the terms of the bonus. Within the framework of turnover your bets are summarized until the value of the sum reaches the necessary figure. Usually this is the bonus amount multiplied by the wager value. For example: if the bonus amount is 100 rubles, and the wager equals to 10, in order to complete the turnover you need to place bets in the amount: 100 rubles × 10 = 1000 rubles.
### What should I do if the bonus turnover doesn't work out?
You should carefully read the terms of the bonus. Maybe you try to wager with violation of the terms. For example, you are wagering by using the bet which exceeds the allowed limit, or you are playing the game which doesn't take part in bonus turnover. If you haven't found any mistakes on your part, please contact the support service. We'll do our best to help.
### Are there any personal bonuses?
Yes, of course, we are very attentive to our players and we offer very interesting conditions.
### The game has hung up, it doesn't start, what should I do?
Try to reload the page. Don't be afraid that the data will be lost. The game will continue from the place it was disconnected. If it doesn't help and the game is hung up again, try to restart the browser (Google Chrome, Mozilla Firefox, Opera, Internet Explorer). If this doesn't help too, your browser cache memory may be in need of garbage collection. If nothing of the above-mentioned helps, contact our support service.
### Any questions left?
You may always contact the support service by using online chat. We'll consider your question as quickly as possible and we'll try to give the exhaustive reply.