## Agreement

The rules presented on the site %SITE_NAME% are regarded as the agreement between the site and the player. The rules form the basis for further cooperation, regulate the issues relating to registration of the account and the process of placing bets. In case of violation of the rules contained in this section, the player's account shall be blocked together with the money available on the account of the profile. Before proceeding to registration on the site and to participation in the games one should read the main rules and terms of the site %SITE_NAME%.

## Rules

Registration on the site is allowed for the individuals aged 18 and older. According to the laws minor individuals can't take part in gambling games.

Each player, irrespective of the country of residence, shall follow and rely on the laws relating to gambling games. Responsibility for non-compliance with the laws on gambling games is vested to the full extent in the player and is borne by the player.

Bonus games have their own rules and bonus policy terms. Before proceeding to the game with bonuses, the player should at first read the terms thereof for avoidance of any possible misunderstandings in the course or upon completion of the game.

When paying the win, the administration of the site %SITE_NAME% has the right to demand the player's age to be confirmed by a document. The document should contain the information which confirms the personality and the age of the player, as well as his/her photo. The scanned files of the documents are to be of high quality and not raise suspicions as to their authenticity. In case the player ignores the request for presentation of personal documents or refuses to present such documents, the site administration is entitled to suspend the activity of the player and the account of the player's profile.

The online casino strongly recommends the player not to disclose the information about the registration data of his/her account. The password and/or login shall not be disclosed to third parties. %SITE_NAME% also undertakes not to disclose the users accounts data, but doesn't bear responsibility for the cases of hack of the account or in case third parties get access to the account as the result of the player him/herself giving the entry information to such persons.

An individual may register one profile and one account. Placing bets by using several registered accounts is strictly forbidden. In case of detection of such cases the activity of these profiles/accounts shall be suspended.

Participation in online games and promotions is absolutely voluntary, coercive actions on the part of third parties shall be punished in accordance with the norms contained in the laws of the country of the player. The Casino %SITE_NAME% may terminate promotions early, without notifying the players accordingly by personal messages. In somes cases the promotion may be terminated only for those players, whose activity has arisen suspicions of the site administration.

The site employees, partner programs representatives and near acquaintances of the above-mentioned persons are not allowed to take part in the games and place bets on %SITE_NAME%. Placing bets by these categories of individuals constitutes the violation of rules and fraudulent act. Upon detection of the account which is registered in the person-participant of a partner program, such account shall be blocked.

It is not allowed to place bets by using specialized programs which give advantage to the player and violate the fair play principle of the game. Such acts are classified as fraudulent and provide for responsibility of the player. Bets on the site of the online casino %SITE_NAME% shall be placed only by using the program installed on the site.

The site security service officer has the right to withdraw from the personal accounts of the users the funds and bonuses which were entered to the account by dishonest means during the period of technical issues on the site.

No player is entitled to ignore the messages and inquiries, sent by the administration of the online casino %SITE_NAME% to the personal e-mail address of the player. That is why in the course of registration you should carefully check that the data have been entered correctly; you should also check the inbox of your mail account on a regular basis, some messages may authomatically end up in the the spam category.

The online casino %SITE_NAME% at its own discretion establishes the amount which may be daily withdrawn by the player from the personal account. For detailed information about the schedule and amount of withdrawal of the funds, please contact the support service on the site.

In case the player refuses to fulfil or intentionally ignores the administrator's request for presentation of documents, the online casino %SITE_NAME% is entitled to block the profile and the personal account of the player until all the circumstances have been completely clarified. The account may be reactivated to the full extent after sending of the authentic scanned documents,which verify the personality and lawful age of the player.

In case of doubts as to the authenticity of the documents and/or suspicion of fraud, the online casino site administration has the right to demand holding of the online video conference with the player. In case unlawful acts are confirmed, the profile and the cash account of the player are blocked by the site administration.

It is strictly forbidden to use swear and abusive words and expressions in relation to the players and employees of the online casino %SITE_NAME%. In case the fact of insult and/or degrading is detected, the user account and the cash account of the player shall be frozen for an undetermined period of time.

Before registration the player needs to read these rules, and is insistently required to visit the page on a regular basis. The rules may be amended without any prior notice.  The amendments relating to promotion offers and bonus programs may be sent to e-mail addresses of the players in the form of mailing campaign.

## Bonus receipt rules

<span class="marker">First deposit bonus</span> – the amount which has no strictly defined size and is accrued for each player on the first deposit of funds to the account after account registration on the site of the online casino %SITE_NAME%.

<span class="marker">Bonus</span> may be accrued only to new players of the game, and such bonus is provided only for the first game. In case of any subsequent stand-alone top-up of the account, the bonuses from the site %SITE_NAME% shall not be accrued.

To get a bonus deposit from the site  %SITE_NAME%, the player needs to deposit the initial amount in one payment. The account shall be topped up by electronic payment option. The bonus shall be accrued to the account in the amount of the sum which was deposited.

Bonus funds are accrued right after the first electronic payment to the own account has been made. Bonus funds may be valid for an unlimited period of time and may be spent during any period of the game.

The funds are withdrawn from the bonus account, firstly, upon placing a bet, as well as in case of losses. After all funds available on the bonus account have been used, the funds are withdrawn from the principal cash account of the individual.

The bonus amount constitutes charge-free bets and in order to perform the transfer into money the account holder needs to wager the bonus. It becomes possible on establishment of the bet with the wager of 20 of the initial bonus amount.

<span class="marker">Wager</span> is the amount, which indicates and establishes the exact number of occasions, needed to the individual for the full-scale withdrawal of bonus. For example, in case the player initially tops up his/her account for 1000 USD, the casino %SITE_NAME% awards this player with the special bonus of the same amount. In order to transfer the bonus and the win amount into the cash equivalent the game participant needs to place the bet which is 20 times higher. That is, the player has to multiply one thousand USD by 20 and the resulting amount allows to withdraw the bonus money and the wins, received thereon.

Depending on the games, different amounts of bets, which allow the bonus turnover, may be factored in.

In the games such as Race-3D Velotrack, New TV roulette, Poker, Bingo, Miniroulette, Turbokeno, Blackjack, Race-3D Trone and Keno 10% of the bet are factored in for the purposes of bonus turnover. Slot type games provide for bonus turnover the funds in full, for one hundred percent.




